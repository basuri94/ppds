<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/clear-cache', function() {
	$run = Artisan::call('config:clear');
	$run = Artisan::call('cache:clear');
	$run = Artisan::call('config:cache');
	$run = Artisan::call('view:clear');
	

	return 'FINISHED'; 
});
Route::get('/index', function () {
    return view('dashboard');
});
Route::get('/', function () {
    return view('dashboard');
});
Route::get('/dashboard', function () {
    return view('dashboard');
});

/**************Sidebar Report********/
Route::post('/personnelaReport','DashboardController@personnelaReport');
/**************Login*****************/
Route::get('/login', function () {
    return view('login');
});
Route::post('/login_action','LoginController@login');
Route::get('/logout','LoginController@logout');

/***************Change Password***********/
Route::get('/change_password', function () {
    return view('change_password');
});
Route::post('/change_password_create','ChangePasswordController@change_password_create');
/***************User******************/

Route::get('/user_list',function(){
    return view('user_list');
});
Route::any('/user_create','UserController@user_list_modify');
Route::post('/user_list_datatable','UserController@user_list_datatable');
Route::post('/select_subdivision','UserController@select_subdivision');
Route::post('/select_block','UserController@select_block');
Route::post('/select_assebly','UserController@select_assebly');
Route::post('/select_zone','UserController@select_zone');
Route::post('/add_user_details','UserController@add_user_details');

Route::post('/user_permission','UserController@create_permission');
Route::post('/user_permission_sub_menu_list','UserController@user_permission_sub_menu_list');
Route::post('/user_permission_sub_menu_list_create','UserController@user_permission_sub_menu_list_create');
Route::post('/create_user_permission','UserController@create_user_permission');
Route::post('/user_permission_alloted','UserController@user_permission_alloted');

Route::get('/permission_not_found',function(){
  return view('error.permission_not_found');   
});
/***********************user modify & delete*************************/
Route::post('/user_list_modify','UserController@user_list_modify');
Route::post('/add_user_update','UserController@add_user_update');
Route::post('/user_list_delete','UserController@user_list_delete');

//::::::::::::::::Order Entry:::::::::::::::::::::::::://
Route::get('/orderEntry', function () {
    return view('orderEntry');
});
Route::post('/order_entry_details','orderDetailsController@order_entry_details');
Route::post('/getOrderDateAndTimeRecords','orderDetailsController@getOrderDateAndTimeRecords');
Route::post('/editOrderDateTime','orderDetailsController@editOrderDateTime');
Route::post('/update_order_table','orderDetailsController@update_order_table');
Route::post('/deleteOrderDateTime','orderDetailsController@deleteOrderDateTime');

//::::::::::::::::Pre Exemption (Add):::::::::::://
Route::get('/pre_exemption',function(){
    return view('pre_exemption');
});
Route::post('/getPPDetailsForExempt','PreExemptController@getPPDetailsForExempt');
Route::post('/save_pre_exemption','PreExemptController@save_pre_exemption');

//::::::::::::::::Pre Exemption (Add):::::::::::://
Route::get('/pre_exemption_edit',function(){
    return view('pre_exemption_edit');
});
Route::post('/getCheckedPPDetailsForExempt','PreExemptController@getCheckedPPDetailsForExempt');
Route::post('/edit_pre_exemption','PreExemptController@edit_pre_exemption');
Route::post('/officeCodeSearch','PreExemptController@officeCodeSearch');
Route::post('/designationSearch','PreExemptController@designationSearch');

//::::::::::::::::Pre Exemption (Export):::::::::::://
Route::get('/pre_exemption_export','PreExemptController@pre_exemption_export');

//::::::::::::::::::Swapping (ED Maximise)::::::::::://
Route::get('/swapping_ed_maximise','SwappingController@getEDSubdivision');
Route::post('/swapping_personnel_ed_maximise','SwappingController@save_swapping_personnel_ed_maximise');

//::::::::::::::::::Swapping::::::::::::::::::::::::://
Route::get('/swapping','SwappingController@getSubdivision');
Route::post('/getSubdivisionData','SwappingController@getSubdivisionData');
Route::post('/getBlockMuni','SwappingController@getBlockMuni');
Route::post('/getOfficeName','SwappingController@getOfficeName');
Route::post('/getMemberDetails','SwappingController@getMemberDetails');
Route::post('/getZoneDetailsReqiured','SwappingController@getZoneDetailsReqiured');
Route::post('/swapping_personnel','SwappingController@saveSwappingDetails');
Route::post('/getZoneDetails','SwappingController@getZoneDetails');
Route::post('/getForDistDetails','SwappingController@getForDistDetails');

//::::::::::::::::Swapping (1st Randomisation)::::::://
Route::get('/swapping_firstrand','SwappingController@getSwappingRand');
Route::post('/swapping_firstrandpp','SwappingController@saveSwappingPP');

//::::::::::::::::Swapping Duplicate::::::::::::::::://
Route::get('/swapping_duplicate','SwappingController@getSubdivisionDup');
Route::post('/getSubdivZoneWiseData','SwappingController@getSubdivZoneWiseData');
Route::post('/getMemberDuplicateDetails','SwappingController@getMemberDuplicateDetails');
Route::post('/swapping_pp_duplicate','SwappingController@swapping_pp_duplicate');

//::::::::::::::::Reverse Swapping::::::::::::::::://
Route::get('/reverse_swapping','ReverseSwappingController@getDistrict');
Route::post('/reverse_swapping_personnel','ReverseSwappingController@saveReverseSwappingDetails');
Route::post('/all_reverse_swapping_personnel','ReverseSwappingController@saveAllReverseSwappingDetails');
Route::post('/getZoneDetailsReverseReqiured','ReverseSwappingController@getZoneDetailsReverseReqiured');
Route::post('/check_for_all_reverse_password','ReverseSwappingController@check_for_all_reverse_password');

//::::::::::::::::SET Priority PC:::::::::::://
Route::get('/set_priority_pc',function(){
    return view('set_priority_pc');
});
Route::post('/getDistrictWisePCdata','SetPriorityPCController@getDistrictWisePCdata');
Route::post('/getDistrictPCPriorityRecords','SetPriorityPCController@getDistrictPCPriorityRecords');
Route::post('/save_priority_pc','SetPriorityPCController@save_priority_pc');
Route::post('/delete_priority_pc','SetPriorityPCController@delete_priority_pc');

//::::::::::::::::SET Priority AC:::::::::::://
Route::get('/set_priority_ac',function(){
    return view('set_priority_ac');
});
Route::post('/getDistrictWisePCdata','SetPriorityACController@getDistrictWisePCdata');
Route::post('/getDistrictWisePCwiseACdata','SetPriorityACController@getDistrictWisePCwiseACdata');
Route::post('/getDistrictACPriorityRecords','SetPriorityACController@getDistrictACPriorityRecords');
Route::post('/save_priority_ac','SetPriorityACController@save_priority_ac');
Route::post('/delete_priority_ac','SetPriorityACController@delete_priority_ac');

//::::::::::::::::First Training Venue:::::::::::://
Route::get('/first_training_venue','FirstTrainingVenueController@getSubdivision');
Route::post('/firstTrVenue','FirstTrainingVenueController@firstTrVenue');
Route::get('/first_training_venue_list',function(){
    return view('first_training_venue_list');
});
Route::post('/first_training_venue_list_datatable','FirstTrainingVenueController@first_training_venue_list_datatable');
Route::post('/check_for_edit_delete_subvenue','FirstTrainingVenueController@check_for_edit_delete_subvenue');
Route::post('/edit_first_train_venue','FirstTrainingVenueController@edit_first_train_venue');
Route::post('/delete_first_training_venue','FirstTrainingVenueController@delete_first_training_venue');
Route::post('/check_for_delete_subvenue','FirstTrainingVenueController@check_for_delete_subvenue');
Route::post('/getAssemblyData','FirstTrainingVenueController@getAssemblyData');


//::::::::::::::::First Training Allocation (Room wise):::::::::::://
Route::get('/first_training_allocation','FirstTrainingAllocationController@getData');
Route::post('/getTrainingDateTime','FirstTrainingAllocationController@getTrainingDateTime');
Route::post('/getVenueName','FirstTrainingAllocationController@getVenueName');
Route::post('/getSubVenueName','FirstTrainingAllocationController@getSubVenueName');
Route::post('/getSubVenueRoomWise','FirstTrainingAllocationController@getSubVenueRoomWise');
Route::post('/getOtherSubdivisionData','FirstTrainingAllocationController@getOtherSubdivisionData');
Route::post('/firstTrAllocation','FirstTrainingAllocationController@firstTrAllocation');
Route::post('/getZonewiseSubdivisionData','FirstTrainingAllocationController@getZonewiseSubdivisionData');
Route::post('/getZonewiseSubdivisionall_Data','FirstTrainingAllocationController@getZonewiseSubdivisionall_Data');


Route::get('/first_training_venue_allocation_list','FirstTrainingAllocationController@getDatafor_list');
Route::post('/first_training_venue_allocation_datatable','FirstTrainingAllocationController@first_training_venue_allocation_datatable');
Route::post('/delete_first_training_venue_alloc','FirstTrainingAllocationController@delete_first_training_venue_alloc');
Route::post('/getOtheAssemblyData','FirstTrainingAllocationController@getOtheAssemblyData');

//::::::::::::::::First Training Allocation (max cp wise):::::::::::://
Route::get('/first_training_allocation_maxcp','FirstTrainingAllocationController@getMaxCPData');


//::::::::::From Other Block:::::::::::::://
Route::get('/first_training_allocation_maxcp_block','FirstTrainingAllocationControllerblock@getMaxCPData');
Route::post('/getOtherblockData','FirstTrainingAllocationControllerblock@getOtherblockData');
Route::post('/firstTrAllocation_block','FirstTrainingAllocationControllerblock@firstTrAllocation_block');



//::::::::::::::::First Training Assign:::::::::::://
Route::get('/first_training_assign','FirstTrainingAssignController@getTrData');
Route::post('/firstTrAssign','FirstTrainingAssignController@savefirstTrAssign');

//::::::::::::::::First Training Attendance::::::::::::::::::://
Route::get('/first_training_attendance','FirstTrAttendanceController@initVenueTrData');
Route::post('/getPPForTrAttendance','FirstTrAttendanceController@getPPForTrAttendance');
Route::post('/first_tr_attendance','FirstTrAttendanceController@first_tr_attendance');
Route::post('/getFirstTrAttenExcel','FirstTrAttendanceController@getFirstTrAttenExcel');

//::::::::::::::::Second Training Venue:::::::::::://
Route::get('/second_training_venue', function () {
    return view('second_training_venue');
});
Route::post('/SecondTrVenue','SecondTrainingVenueController@SecondTrVenue');
Route::get('/second_training_venue_list',function(){
    return view('second_training_venue_list');
});
Route::post('/second_training_venue_list_datatable','SecondTrainingVenueController@second_training_venue_list_datatable');
Route::post('/check_for_edit_delete_subvenue_second','SecondTrainingVenueController@check_for_edit_delete_subvenue_second');
Route::post('/edit_second_train_venue','SecondTrainingVenueController@edit_second_train_venue');
Route::post('/delete_second_training_venue','SecondTrainingVenueController@delete_second_training_venue');
Route::post('/getVenueName_second','SecondTrainingVenueController@getVenueName_second');

//::::::::::::::::Second Training Allocation:::::::::::://
Route::get('/second_training_allocation', function () {
    return view('second_training_allocation');
});
Route::post('/get2ndTrainingDateTime','SecondTrainingAllocationController@get2ndTrainingDateTime');
Route::post('/get2ndVenueName','SecondTrainingAllocationController@get2ndVenueName');
Route::post('/getSubAssemblyDetails','SecondTrainingAllocationController@getSubAssemblyDetails');
Route::post('/get2ndSubVenue','SecondTrainingAllocationController@get2ndSubVenue');
Route::post('/get2ndVenuePPTotal','SecondTrainingAllocationController@get2ndVenuePPTotal');
Route::post('/secondTrAllocation','SecondTrainingAllocationController@secondTrAllocation');
Route::get('/second_training_venue_allocation_list',function(){
 return view('second_training_venue_allocation_list');   
});
Route::post('/second_training_venue_allocation_datatable','SecondTrainingAllocationController@second_training_venue_allocation_datatable');
Route::post('/delete_second_training_venue_alloc','SecondTrainingAllocationController@delete_second_training_venue_alloc');

//::::::::::::::::Zone Entry::::::::::::::::::::::::::::::://
Route::get('/zone_entry', function () {
    return view('zone_entry');
});
Route::post('/getZoneRecords','ZoneEntryController@getZoneRecords');
Route::post('/su_zone_entry','ZoneEntryController@su_zone_entry');
Route::post('/zone_edit','ZoneEntryController@zone_edit');
Route::post('/update_zone','ZoneEntryController@update_zone');
Route::post('/zone_delete','ZoneEntryController@zone_delete');

//::::::::::::::::Assemblywise Zone creation::::::::::::::::::://
Route::get('/assemblyzone_creation', function () {
    return view('assemblyzone_creation');
});
Route::post('/getAssemblyZone','AssemblyZoneCreationController@getAssemblyZone');
Route::post('/createAssemblyZone','AssemblyZoneCreationController@createAssemblyZone');
Route::post('/deleteAssemblyZone','AssemblyZoneCreationController@deleteAssemblyZone');
Route::post('/updateAssemblyZone','AssemblyZoneCreationController@updateAssemblyZone');


//:::::::::::::::Assembly party and Reserve:::::::::::::::::://
Route::get('/assemblyparty_formation', function () {
    return view('assemblyparty_formation');
});
Route::post('/getAssemblyDetails','AssemblyPartyFormationController@getAssemblyDetails');
Route::post('/assemblyPartyForm','AssemblyPartyFormationController@assemblyPartyForm');

//:::::::::::::::Assembly party List::::::::::::::::::::::://
Route::get('/assembly_party_form_list',function(){
    return view('assembly_party_form_list');
});
Route::post('/assem_party_formation_list_datatable','AssemblyPartyFormationController@assem_party_formation_list_datatable');
Route::post('/check_for_edit_asmparty','AssemblyPartyFormationController@check_for_edit_asmparty');
Route::post('/check_for_delete_asmparty','AssemblyPartyFormationController@check_for_delete_asmparty');
Route::post('/edit_assembly_party_form','AssemblyPartyFormationController@edit_assembly_party_form');
Route::post('/delete_assembly_party_form','AssemblyPartyFormationController@delete_assembly_party_form');

//:::::::::::::::Reserve::::::::::::::::://
Route::get('/assemblyreserve_formation', function () {
    return view('assemblyreserve_formation');
});
Route::post('/getAssemblyPartyDetails','AssemblyReserveFormationController@getAssemblyPartyDetails');
Route::post('/assemblyReserveForm','AssemblyReserveFormationController@assemblyReserveForm');
Route::get('/assembly_reserve_list', function () {
    return view('assembly_reserve_list');
});
Route::post('/assem_party_reserve_list_datatable','AssemblyReserveFormationController@assem_party_reserve_list_datatable');
Route::post('/check_for_edit_reserve','AssemblyReserveFormationController@check_for_edit_reserve');
Route::post('/edit_reserve_party_form','AssemblyReserveFormationController@edit_reserve_party_form');

//:::::::::::::::DCRC Master:::::::::::::::::://
Route::get('/dcrc_master', function () {
    return view('dcrc_master');
});
Route::post('/DCRCMasterForm','DCRCMasterController@DCRCMasterForm');
Route::get('/dcrc_master_list', function () {
    return view('dcrc_master_list');
});
Route::post('/dcrc_master_list_datatable','DCRCMasterController@dcrc_master_list_datatable');
Route::post('/check_for_edit_delete_dcrc','DCRCMasterController@check_for_edit_delete_dcrc');
Route::post('/edit_drdc','DCRCMasterController@edit_drdc');
Route::post('/delete_dcrc','DCRCMasterController@delete_dcrc');

//:::::::::::::::DCRC Party :::::::::::::::::://
Route::get('/dcrc_party', function () {
    return view('dcrc_party');
});
Route::post('/getAssemblyPartyDetailsForDCRC','DCRCPartyController@getAssemblyPartyDetailsForDCRC');
Route::post('/getDCVenueDetails','DCRCPartyController@getDCVenueDetails');
Route::post('/DCRCParty','DCRCPartyController@DCRCParty');
Route::get('/dcrc_party_list', function () {
    return view('dcrc_party_list');
    
});
Route::post('/dcrc_party_list_datatable','DCRCPartyController@dcrc_party_list_datatable');
Route::post('/check_for_edit_delete_dcrc_party','DCRCPartyController@check_for_edit_delete_dcrc_party');
Route::post('/delete_dcrc_party','DCRCPartyController@delete_dcrc_party');

//:::::::::::::::DCRC Party Separate:::::::::::::::::://
Route::get('/dcrc_party_separate', function () {
    return view('dcrc_party_separate');
});
Route::post('/getPartyNumber','DCRCPartyController@getPartyNumber');
Route::post('/DCRCPartySeparate','DCRCPartyController@DCRCPartySeparate');

//::::::::::::::::First Training Date and Time:::::::::::::://
Route::get('/first_training_datetime', function () {
    return view('first_training_datetime');
});
Route::post('/first_training_date_entry','DateTimeController@first_training_date_entry');
Route::post('/getFirstTraningDateRecords','DateTimeController@getFirstTraningDateRecords');
Route::post('/first_date_time_edit','DateTimeController@first_date_time_edit');
Route::post('/first_date_time_delete','DateTimeController@first_date_time_delete');
Route::post('/update_first_date_time_delete','DateTimeController@update_first_date_time_delete');

//::::::::::::::::Second Training Date and Time:::::::::::::://
Route::get('/second_training_datetime', function () {
    return view('second_training_datetime');
});
Route::post('/second_training_date_entry','DateTimeController@second_training_date_entry');
Route::post('/getSecondTraningDateRecords','DateTimeController@getSecondTraningDateRecords');
Route::post('/second_date_time_edit','DateTimeController@second_date_time_edit');
Route::post('/update_second_date_time','DateTimeController@update_second_date_time');
Route::post('/second_date_time_delete','DateTimeController@second_date_time_delete');


//:::::::::::::::1st Randomisation(Lock/Unlock):::::::::::::://
Route::get('/first_random_lock_unlock', function () {
    return view('first_random_lock_unlock');
});
Route::post('/check_for_status_password','FirstRandController@check_for_status_password');
Route::post('/check_for_lu_rand_otp','FirstRandController@check_for_lu_rand_otp');
Route::post('/send_otp_password','FirstRandController@send_otp_password');
Route::post('/firstzone_wise_assembly','FirstRandController@firstzone_wise_assembly');
Route::post('/firstrand_lock_unlock','FirstRandController@firstrand_lock_unlock');
Route::post('/check_for_lu_password','FirstRandController@check_for_lu_password');
Route::post('/resend_otp_password','FirstRandController@resend_otp_password');
Route::post('/firstzone_phase_wise_assembly','FirstRandController@firstzone_phase_wise_assembly');
//::::::::::::::::1st Randomisation:::::::::::::://
Route::get('/first_random', function () {
    return view('first_random');
});
Route::post('/firstrand','FirstRandController@firstrand');
Route::post('/first_rand_zone_wise_assembly','FirstRandController@first_rand_zone_wise_assembly');
Route::post('/check_for_rand_password','FirstRandController@check_for_rand_password');
Route::post('/first_rand_zone_phase_wise_assembly','FirstRandController@first_rand_zone_phase_wise_assembly');


//::::::::::::::::1st Randomisation Populate:::::::::::::://
Route::get('/first_letter_populate','FirstRandController@getTrData');
Route::post('/first_appt_letter_populate','FirstRandController@first_appt_letter_populate');
Route::post('/getOrderDetails','FirstRandController@getOrderNoDate');

//:::::::::::::::1st Appointment Letter:::::::::::::://
Route::get('/first_appt_letter','FirstRandController@getApptTrData');
Route::post('/getSubdivisionDataUserwise','FirstRandController@getSubdivisionDataUserwise');
Route::post('/getBlockMuniUserwise','FirstRandController@getBlockMuniUserwise');
Route::post('/getOfficeNameUserwise','FirstRandController@getOfficeNameUserwise');
//Route::post('/getFirstAppointmentLetterPDF','FirstRandController@getFirstAppointmentLetterPDF');
Route::post('/getFirstAppointmentLetterPDF2019','FirstRandController@getFirstAppointmentLetterPDF2019');
Route::post('/getFirstRecordAvailable','FirstRandController@getFirstRecordAvailable');
Route::post('/getFirstApptExcel','FirstRandController@getFirstApptExcel');
Route::get('/hello','SwappingController@getFPDF');

//:::::::::::::::1st Subdivision Office PP List (Report):::::::::::://
Route::get('/first_sub_off_pp_report','FirstRandController@getOffTrainingData');
Route::post('/getFirstSubOffPPReport','FirstRandController@getFirstSubOffPPReport');

//:::::::::::::::1st Venue PP List (Report):::::::::::::::::://
Route::get('/first_venue_pp_report','FirstRandController@getVenueTrData');
Route::post('/getFirstVenuePPReport','FirstRandController@getFirstVenuePPReport');
Route::post('/getFirstVenuePPReportexcel','FirstRandController@getFirstVenuePPReportexcel');

//:::::::::::::::2nd Randomisation(Lock/Unlock):::::::::::::://
Route::get('/second_random_lock_unlock', function () {
    return view('second_random_lock_unlock');
});
Route::post('/secondzone_wise_assembly','SecondRandController@secondzone_wise_assembly');
Route::post('/secondrand_lock_unlock','SecondRandController@secondrand_lock_unlock');
Route::post('/secondzone_phase_wise_assembly','SecondRandController@secondzone_phase_wise_assembly');
//::::::::::::::::2nd Randomisation:::::::::::::://
Route::get('/second_random', function () {
    return view('second_random');
});
Route::post('/secondrand','SecondRandController@secondrand');
Route::post('/second_rand_zone_wise_assembly','SecondRandController@second_rand_zone_wise_assembly');
Route::post('/getZonePCdata','SecondRandController@getZonePCdata');
Route::post('/getZonePhasedata','SecondRandController@getZonePhasedata');
Route::post('/second_rand_zone_phase_wise_assembly','SecondRandController@second_rand_zone_phase_wise_assembly');
Route::post('/getSubdivisionPhasedata','SecondRandController@getSubdivisionPhasedata');

//:::::::::::::::2nd Appointment Letter Populate:::::://
Route::get('/second_letter_populate', function () {
    return view('second_letter_populate');
});
Route::post('/second_appt_letter_populate','SecondRandController@second_appt_letter_populate');

//:::::::::::::::2nd Appointment Letter (Party/Reserve):::::::::::::://
Route::get('/second_appt_letter', function () {
    return view('second_appt_letter');
});
Route::post('/getSecondRecordAvailable','SecondRandController@getSecondRecordAvailable');
//Route::post('/getSecondAppointmentLetterPDF45','SecondRandController@getSecondAppointmentLetterPDF45');
//Route::post('/getSecondAppointmentLetterPDF6','SecondRandController@getSecondAppointmentLetterPDF6');
//Route::post('/getSecondAppointmentLetterReservePDF','SecondRandController@getSecondAppointmentLetterReservePDF');
Route::post('/getSecondAppointmentLetterPDF452019','SecondRandController@getSecondAppointmentLetterPDF452019');
Route::post('/getSecondAppointmentLetterPDF62019','SecondRandController@getSecondAppointmentLetterPDF62019');
Route::post('/getSecondAppointmentLetterReservePDF2019','SecondRandController@getSecondAppointmentLetterReservePDF2019');
Route::post('/getSecondAppointmentLetterEXCEL','SecondRandController@getSecondAppointmentLetterEXCEL');
Route::post('/getSecondAppointmentLetterReserveEXCEL','SecondRandController@getSecondAppointmentLetterReserveEXCEL');

//::::::::::::::::2nd Master Roll/Scroll/Venue Report:::::::::://
Route::get('/second_master_scroll_venue_report', function () {
    return view('second_master_scroll_venue_report');
});
Route::post('/getSecondMasterRollPDF','SecondRandController@getSecondMasterRollPDF');
Route::post('/getSecondPartyScrollPDF','SecondRandController@getSecondPartyScrollPDF');
Route::post('/getSecondPartyVenuePDF','SecondRandController@getSecondPartyVenuePDF');
Route::post('/getSecondReserveMasterPDF','SecondRandController@getSecondReserveMasterPDF');
Route::post('/getSecondReserveVenuePDF','SecondRandController@getSecondReserveVenuePDF');
Route::post('/getSecondMasterRollEXCEL','SecondRandController@getSecondMasterRollEXCEL');
Route::post('/getSecondReserveScrollPDF','SecondRandController@getSecondReserveScrollPDF');


//:::::::::::::::3rd Randomisation(Lock/Unlock):::::::::::::://
Route::get('/third_random_lock_unlock', function () {
    return view('third_random_lock_unlock');
});
Route::post('/thirdzone_wise_assembly','ThirdRandController@thirdzone_wise_assembly');
Route::post('/thirdrand_lock_unlock','ThirdRandController@thirdrand_lock_unlock');
Route::post('/thirdzone_phase_wise_assembly','ThirdRandController@thirdzone_phase_wise_assembly');
//::::::::::::::::3rd Randomisation:::::::::::::://
Route::get('/third_random', function () {
    return view('third_random');
});
Route::post('/thirdrand','ThirdRandController@thirdrand');
Route::post('/third_rand_zone_wise_assembly','ThirdRandController@third_rand_zone_wise_assembly');
Route::post('/third_rand_phase_zone_wise_assembly','ThirdRandController@third_rand_phase_zone_wise_assembly');
//::::::::::::::::Polling Station:::::::::::::::://
Route::get('/polling_station_add', function () {
    return view('polling_station_add');
});
Route::post('/pollingStationAdd','PollingStationController@pollingStationAdd');
Route::get('/polling_station_dcrc', function () {
    return view('polling_station_dcrc');
});
Route::post('/getPollingStationDetailsForDCRC','PollingStationController@getPollingStationDetailsForDCRC');
Route::post('/get_assembly_details_phase_wise','PollingStationController@get_assembly_details_phase_wise');
Route::post('/PSDCRCUpdate','PollingStationController@PSDCRCUpdate');
Route::get('/pollin_station_list', function () {
    return view('pollin_station_list');
});
Route::post('/polling_list_datatable','PollingStationController@polling_list_datatable');
Route::post('/polling_modify','PollingStationController@polling_modify');
Route::post('/update_polling','PollingStationController@update_polling');
Route::post('/delete_polling_chking','PollingStationController@delete_polling_chking');
Route::post('/delete_polling','PollingStationController@delete_polling');
Route::post('/excelformat','PollingStationController@excelformat');
Route::post('/getDCAssemblyVenueDetails','PollingStationController@getDCAssemblyVenueDetails');

//::::::::::::::::Reports (Query Reports after 1st Rand):::::::::::::::::::::::::://
//Route::get('/queryreports_first_rand', function () {
//    return view('queryreports_first_rand');
//});
Route::get('/queryreports_first_rand','QueryFirstRandController@getQueryInit');
Route::post('/getFirstSubBlockQueryReport','QueryFirstRandController@getFirstSubBlockQueryReport');
Route::post('/getFirstSubCatQueryReport','QueryFirstRandController@getFirstSubCatQueryReport');
Route::post('/getFirstSubInstiQueryReport','QueryFirstRandController@getFirstSubInstiQueryReport');
Route::post('/getZonewiseSubdivisionBlockData','QueryFirstRandController@getZonewiseSubdivisionBlockData');
Route::post('/getFirstSubPoststatusQueryReport','QueryFirstRandController@getFirstSubPoststatusQueryReport');
Route::post('/getZoneBlockMuniUserwise','QueryFirstRandController@getZoneBlockMuniUserwise');
Route::post('/getZonewiseSubBlockDataQuery','QueryFirstRandController@getZonewiseSubBlockDataQuery');
Route::post('/getZoneBlockMuniUserwiseQuery','QueryFirstRandController@getZoneBlockMuniUserwiseQuery');
Route::post('/getCategoryDetails','QueryFirstRandController@getCategoryDetails');
Route::post('/getInstituteDetails','QueryFirstRandController@getInstituteDetails');




//::::::::::::::::Pre-group Cancellation:::::::::::::::::::::::::://
Route::get('/pre_group_cancellation', function () {
    return view('pre_group_cancellation');
});
Route::post('/preCancelPPData','PreGReplacementController@preCancelPPData');

//::::::::::::::::Pre Group Cancellation Excel Upload:::::::::::::::::::::::::://

Route::get('/preGroupCancellationExcelUpload', function () {
    return view('preGroupCancellationExcelUpload');
});
Route::post('/excelFormatForCancellation','PreGReplacementController@excelFormatForCancellation');
Route::post('/preGroupCancellationExcel','PreGReplacementController@preGroupCancellationExcel');

//:::::::::::::::Post-group Replacement(NEW PP):::::::::::::::::::::::::://
Route::get('/post_group_replacement_newpp','PostGReplacementController@getPostGData');
Route::post('/getSelectedPersonnelID','PostGReplacementController@getSelectedPersonnelID');
Route::post('/searchNewPPPostData','PostGReplacementController@searchNewPPPostData');
Route::post('/postReplaceNewPPData','PostGReplacementController@postReplaceNewPPData');

//:::::::::::::::Post-group Replacement(REVERSE PP):::::::::::::::::::::::::://
Route::get('/post_group_replacement_reserve','PostGReplacementController@getPostRData');
Route::post('/searchNewPPReverseData','PostGReplacementController@searchNewPPReverseData');

//::::::::::::::: Reserve replacement ::::::::::::::::::://
Route::get('/reserve_replacement', function () {
    return view('reserve_replacement');
});
Route::post('/getOldPersonnelReserveDetails','PostGReplacementController@getOldPersonnelReserveDetails');
Route::post('/searchNewReserveDataFR','PostGReplacementController@searchNewReserveDataFR');
Route::post('/reserveReplaceNewPPData','PostGReplacementController@reserveReplaceNewPPData');

//::::::::::::::: Reserve Cancellation :::::::::::::::::://
Route::get('/reserve_cancellation', function () {
    return view('reserve_cancellation');
});
Route::post('/reserveCancelPPData','PostGReplacementController@reserveCancelPPData');

//::::::::::::::::Search Polling Personnel:::::::::::::::::::::::::://
Route::get('/search_polling_personnel', function () {
    return view('search_polling_personnel');
});
Route::post('/searchPPDetails','PreGReplacementController@searchPPDetails');


//::::::::::::::::Booth Tagging List:::::::::::::::::::::::::://
Route::get('/booth_tagging_list', function () {
    return view('booth_tagging_list');
});
Route::post('/getBoothTaggingPDF','ThirdRandController@getBoothTaggingPDF');
Route::post('/get_boothtaging_AssemblyDetails','ThirdRandController@get_boothtaging_AssemblyDetails');

//::::::::::::::::Training Initialize::::::::::::::::::::::::::::::://
Route::get('/firstTrainingInitialize','TrainingInitialiseController@getTrData');
Route::post('/firstTrInitialize','TrainingInitialiseController@firstTrInitialize');
Route::post('/check_for_training_initialise_password','TrainingInitialiseController@check_for_training_initialise_password');
//::::::::::::::::Report:::::::::::::::::::::::::://
Route::get('/subdivisionWisePPReport','ReportControllerP@getQueryInit');
Route::post('/getSubDivisionPoststatusReport','ReportControllerP@getSubDivisionPoststatusReport');
Route::post('/getSubDivisionWisePPExcel','ReportControllerP@getSubDivisionWisePPExcel');

//::::::::::::::::SMS SAVE FOR GENERAL TRAINING:::::::::::::::::::::::::://
Route::get('/save_sms','smsController@getSmsRelatedData');
Route::post('/sms_entry_details','smsController@sms_entry_details');

Route::post('/getMessageShow','smsController@getMessageShow');

//::::::::::::::::SEND SMS FOR GENERAL TRAINING:::::::::::::::::::::::::://
// Route::get('/send_sms', function () {
//     return view('sendSmsForGeneralTraining');
// });
Route::get('/send_sms','smsController@getSendSmsRelatedData');
// Route::get('/save_sms','smsController@getSmsRelatedData');


Route::post('/send_sms_for_general','smsController@send_sms_for_general');
Route::post('/getTrainingRecordAvailable','smsController@getTrainingRecordAvailable');
Route::post('/send_sms_for_general','smsController@send_sms_for_general');
Route::post('/SMSRecordssend','smsController@SMSRecordssend');

////Personnel Edit Section by Hooghly 07_03_2019
/*Route::get('/getofficebydistrict','PersonnelController@getAllOffice');
Route::get('/su_personnel_list/{office_id}','PersonnelController@getPersonnelByOffice');
Route::post('/edit_personnel','PersonnelController@getPersonnelById');
Route::post('/save_personnel','PersonnelController@update');
Route::get('/personnel_add','PersonnelController@add');
Route::post('/store_personnel','PersonnelController@store');
Route::get('/searchpersonnel/{personnel_id}','PersonnelController@searchpersonnel');*/

//Personnel Edit Section by Hooghly 07_03_2019
Route::get('/getofficebydistrict','PersonnelController@getAllOffice');
Route::post('/su_personnel_list','PersonnelController@getPersonnelByOffice');
Route::post('/edit_personnel','PersonnelController@getPersonnelById');
Route::post('/save_personnel','PersonnelController@update');
Route::get('/personnel_add','PersonnelController@add');
Route::post('/store_personnel','PersonnelController@store');
Route::post('/officeSearch','PersonnelController@officeSearch');
Route::post('/personnelIdSearch','PersonnelController@personnelIdSearch');
Route::post('/officerSearch','PersonnelController@officerSearch');


/*************************First Traing Change************************/
Route::get('/first_training_change','TrainingChangeController@getDataForChange');
Route::post('/getOldPersonnelDetails_for_change','TrainingChangeController@getOldPersonnelDetails_for_change');
Route::post('/get_venue_with_not_fill','TrainingChangeController@get_venue_with_not_fill');
Route::post('/changeTrainingData','TrainingChangeController@changeTrainingData');


//::::::::::::::::Pre-group Replacement:::::::::::::::::::::::::://
Route::get('/pre_group_replacement','PreGReplacementController@pre_group_replacement');
Route::post('/getOldPersonnelDetails','PreGReplacementController@getOldPersonnelDetails');
Route::post('/searchNewPPData','PreGReplacementController@searchNewPPData');
Route::post('/preReplacePPData','PreGReplacementController@preReplacePPData');
Route::post('/get_venue_with_not_fill_for_replace','PreGReplacementController@get_venue_with_not_fill_for_replace');

//:::::::::::::::2nd Subdivision Office PP List (Report):::::::::::://
Route::get('/second_sub_off_pp_report', function () {
    return view('second_sub_off_pp_report');
});

Route::post('/getOfficeNameUserwiseForSecondRandomization','secondSubOfficeRecordController@getOfficeNameUserwiseForSecondRandomization');
Route::post('/getSecondSubOffPPReport','secondSubOfficeRecordController@getSecondSubOffPPReport');





//::::::::::::::::ED & PD Generate::::::::::::://
Route::get('/edPbExcelHomePcUpdate', function () {
    return view('edPbExcelHomePcUpdate');
});
Route::post('/getExcelOfPDED','edpdExcelController@getExcelOfPDED');
Route::post('/updateHomePC','edpdExcelController@updateHomePC');
Route::post('/edpdGen','edpdExcelController@edpdGen');

Route::get('/edPbReportVenueWise', function () {
    return view('edPbReportVenueWise');
});
Route::post('/get2ndSubVenueName','edpdExcelController@get2ndSubVenueName');
Route::post('/getEDPBEXCEL','edpdExcelController@getEDPBEXCEL');

//::::::::::::::::Assembly Wise Report::::::::::::://
Route::get('/assemblyWiseReportView', function () {
    return view('assemblyWiseReportView');
});

//:::::::::::::::Change At Last:::::::::::://
Route::post('/getOrderNoDateForSecondAppointment','SecondRandController@getOrderNoDateForSecondAppointment');
Route::post('/getBlockMuniForSecondAppt','SecondRandController@getBlockMuniForSecondAppt');
Route::post('/getBoothTaggingPDFpsvspolling','ThirdRandController@getBoothTaggingPDFpsvspolling');
Route::post('/getAssemblyDetailsForPsvspolling','ThirdRandController@getAssemblyDetailsForPsvspolling');

//:::::::::::::::Kalimpong:::::::::::://
Route::post('/getSecondVenueEXCEL','SecondRandController@getSecondVenueEXCEL');
Route::post('/getSecondVenueReserveEXCEL','SecondRandController@getSecondVenueReserveEXCEL');
Route::get('/save_sms_group', function () {
    return view('save_sms_group');
});
Route::post('/sms_entry_details_for_group','smsController@sms_entry_details_for_group');
Route::post('/getMessageShowForGroup','smsController@getMessageShowForGroup');


//::::::::::::::::personnel edit PM::::::::::::::::::://
Route::get('/personal_after_first_training','PersonalControllerAfterFirst@index_personal');
Route::post('/edit_personnel_new','PersonalControllerAfterFirst@getPersonnelById'); 
Route::post('/getPPForTrAttendanceForPersonnel','PersonalControllerAfterFirst@getPPForTrAttendance');
Route::post('/save_personnel_new','PersonalControllerAfterFirst@update');
Route::post('/get_epic_det','PersonalControllerAfterFirst@get_epic_det');


/*******Mo Report after 2nd Rand*************/
Route::get('/mo_details', function () {
    return view('mo_details');
});
Route::post('/mo_detailsCount','MoController@mo_detailsCount');
Route::post('/moDetailsEXCEL','MoController@moDetailsEXCEL');
Route::post('/mo_pdf','MoController@mo_pdf');


Route::get('/epicNoSearch', function () {
    return view('epicNoSearch');
});
Route::post('/getSecondPartyScrollRollEXCEL','SecondRandController@getSecondPartyScrollRollEXCEL');
Route::post('/getSecondReserveScrollRollEXCEL','SecondRandController@getSecondReserveScrollRollEXCEL');
/*******Personnel Edit After Group Training paschim*************/
Route::get('/editPersonnelAfterSecondtraining', function () {
    return view('editPersonnelAfterSecondtraining');
});
Route::post('/getPersonnelDetailsAfterGroupTraining','PersonnelControllerAfterGropTraining@getPersonnelDetailsAfterGroupTraining');
Route::post('/update_personnel_new','PersonnelControllerAfterGropTraining@update_personnel_new');
Route::post('/get_personnel_details','PersonnelControllerAfterGropTraining@get_personnel_details');

/*******First Training Active/Inactive*************/
Route::get('/first_trainingvenue_active_inactive','FirstTrainingAIController@getData');
Route::post('/getfirstTrainingActiveInactive','FirstTrainingAIController@getfirstTrainingActiveInactive');
Route::post('/changeFirstTrainingStatus','FirstTrainingAIController@changeFirstTrainingStatus');

Route::get('/trainingtype_active_inactive','FirstTrainingAIController@getTrainingTypeDetails');
Route::post('/getTrainingTypeActiveInactive','FirstTrainingAIController@getTrainingTypeActiveInactive');
Route::post('/changeTrainingTypeStatus','FirstTrainingAIController@changeTrainingTypeStatus');


/*******1st Randomisation Checking*************/
Route::get('/first_randomisation_checking', function () {
    return view('first_randomisation_checking');
});

Route::post('/getsubdivisionwisedata','FirstRandomisationCheckingController@getsubdivisionwisedata'); 
Route::post('/getBeforeRandomisationPPExcel','FirstRandomisationCheckingController@getBeforeRandomisationPPExcel'); 



 /********************************Assembly Wise randomisation Checking**************************************/
 Route::get('/assembly_wise_checking', function () {
    return view('assembly_wise_checking');
});
Route::post('/getAssemblyWiserecord','FirstRandomisationCheckingController@getAssemblyWiserecord'); 
Route::post('/checkingAssemblyWiseRPTEXcel','FirstRandomisationCheckingController@checkingAssemblyWiseRPTEXcel'); 

/********************************After Populate Checking**************************************/
Route::get('/after_populate_checking', function () {
    return view('after_populate_checking');
});
Route::post('/getAfterPopulateRecord','FirstRandomisationCheckingController@getAfterPopulateRecord'); 

/********************************General Traning Checking**************************************/
Route::get('/general_traning_checking','GeneralTraningController@general_traning_checking');
Route::post('/get_subdivison_wise_traning','GeneralTraningController@get_subdivison_wise_traning'); 
Route::post('/generaltraningallocationPPExcel','GeneralTraningController@generaltraningallocationPPExcel'); 

/********************************Venue Wise Traning Checking**************************************/
Route::get('/checking_venue_wise_populate','GeneralTraningController@checking_venue_wise_populate');
Route::post('/getVenueSubvenueWiseChecking','GeneralTraningController@getVenueSubvenueWiseChecking');
Route::post('/getFirstTraningPopulateCheckingEXcel','GeneralTraningController@getFirstTraningPopulateCheckingEXcel');


Route::get('/single_person_training_assign','SinglePnTrController@single_person_training_assign');
Route::post('/getSinglePersonnelDetails','SinglePnTrController@getSinglePersonnelDetails');
Route::post('/getDistrictWiseSubdivision','SinglePnTrController@getDistrictWiseSubdivision');
Route::post('/singlePPTrDtAllocation','SinglePnTrController@singlePPTrDtAllocation');
Route::get('/sendsms','SendSmsController@sendsms');
Route::post('/smssent','SendSmsController@smssent');
Route::post('/getSmsRecordAvailable','SendSmsController@getSmsRecordAvailable');
Route::post('/SMSRecordssendSpecial','SendSmsController@SMSRecordssendSpecial');

Route::post('/assemblysearch','PersonnelController@assemblysearch');
Route::post('/update_revoke_exemption','PersonnelController@update_revoke_exemption');

/********************************Assembly Subdivision Wise Priority**************************************/

Route::get('/assembly_subdivision_wise_priority', function () {
    return view('assembly_subdivision_wise_priority');
});



Route::post('/saveAssemblySubdivisionPriority','AssemblySubdivisionPriorityController@saveAssemblySubdivisionPriority');
Route::get('/assembly_subdivision_wise_priority_list',function(){
    return view('assembly_subdivision_wise_priority_list');
});
Route::post('/assemblySubdivisionWisePriorityDatatable','AssemblySubdivisionPriorityController@assemblySubdivisionWisePriorityDatatable');

Route::post('/editAssemblySubdivisionWisePriority','AssemblySubdivisionPriorityController@editAssemblySubdivisionWisePriority');
Route::post('/deleteAssemblysubdivisionWisPriority','AssemblySubdivisionPriorityController@deleteAssemblysubdivisionWisPriority');


Route::post('/update_assmbly_eise_prioriy','AssemblySubdivisionPriorityController@update_assmbly_eise_prioriy');



Route::post('/getAllAssembly','AssemblySubdivisionPriorityController@getAllAssembly');

/********************************Checking After Group Training Allolcation**************************************/
Route::get('/checking_after_group_tr_allocation',function(){
    return view('checking_after_group_tr_allocation');
});

Route::post('/get_subdivison_wise_grp_tr_allocation','GroupTrCheckingController@get_subdivison_wise_grp_tr_allocation');




Route::get('/checking_after_grp_tr_populate',function(){
    return view('checking_after_grp_tr_populate');
});

Route::post('/group_tr_after_populate','GroupTrCheckingController@group_tr_after_populate');
Route::post('/getAfterPopulateRecordExcel','GroupTrCheckingController@getAfterPopulateRecordExcel');









