<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Remark extends Model
{
    //
    protected $table ='remarks';
    
    protected $fillable = [
    'remarks_cd','remarks'
    ];
}
