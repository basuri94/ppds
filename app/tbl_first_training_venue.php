<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tbl_first_training_venue extends Model
{
    protected $table ='first_training_venue';
    public $timestamps = false;
    protected $fillable = [
    'venue_cd','subdivision','venuename','venueaddress','maximumcapacity','usercode','blockmunicd'
    ];
     public function subVenueDetailBrief(){
         $filtered="";
         $filtered=$this->hasMany('App\tbl_first_training_subvenue','venue_cd','venue_cd')
//                 ->leftjoin('tbl_subjectmasters', 'tbl_subjectmasters.code', '=', 'tbl_student_subject_details.SubjectID')
                 ->select(array('venue_cd','subvenue_cd','subvenue','maxcapacity'));
         return $filtered;
     }
}
