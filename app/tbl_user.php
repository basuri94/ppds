<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tbl_user extends Model
{
    protected $table = 'user';
     public $timestamps = false;
    protected $fillable = [
       'code','user_id','password','category','creation_date','districtcd','subdivisioncd','parliamentcd','assemblycd','blockmunicd','zonecd'
    ];
    public function user_permission_code() {
         return $this->hasMany('App\tbl_user_permission','user_cd','code')->select(array('user_permission.user_cd','user_permission.menu_cd'))->distinct()->orderby('user_permission.menu_cd');
    }
   
}
