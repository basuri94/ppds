<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Phase extends Model
{
    protected $table = 'phase';

    public $incrementing = false;
}
