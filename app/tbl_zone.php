<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tbl_zone extends Model
{
   protected $table ='zone';
    public $timestamps = false;
    protected $fillable = [
     'zone','districtcd','zonename','usercode','posted_date',
    ];
}
