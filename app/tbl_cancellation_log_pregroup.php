<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tbl_cancellation_log_pregroup extends Model
{
    protected $table ='cancellation_log_pregroup';
    public $timestamps = false;
}
