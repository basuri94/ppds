<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tbl_office extends Model
{
    protected $table ='office';
    protected $fillable = [
        'officecd','office'
    ];
     public function ppDetailBrief(){
         $filtered="";
         $session_personnela=session()->get('personnela_ppds');
         
         $filtered=$this->hasMany('App\tbl_personnela','officecd','officecd')
                 ->join('personnel','personnel.personcd','=',''.$session_personnela.'.personcd')
                 ->join('poststat','poststat.post_stat','=',''.$session_personnela.'.poststat')
                 ->where('selected','=',1)
                 ->select(array('personnel.officecd',''.$session_personnela.'.personcd','personnel.officer_name','personnel.off_desg as designation','poststat.poststatus'))
                 ->orderBy(''.$session_personnela.'.personcd');
         return $filtered;
     }
}
