<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    //
    protected $table ='language';
    
    protected $fillable = [
    'languagecd','languagename','districtcd','usercode','posted_date',
    ];
}