<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tbl_second_training_venue extends Model
{
    protected $table ='second_training_venue';
    public $timestamps = false;
    protected $fillable = [
    'venue_cd','subdivision','venuename','venueaddress','maximumcapacity','usercode'
    ];
     public function subVenueDetailBrief(){
         $filtered="";        
         $filtered=$this->hasMany('App\tbl_second_training_subvenue','venue_cd','venue_cd')
//                  ->leftjoin('second_training_schedule',function($join){
//                     $join->on('second_training_schedule.tr_venue_cd', '=', 'second_training_subvenue.venue_cd');
//                     $join->on('second_training_schedule.tr_subvenue_cd', '=', 'second_training_subvenue.subvenue_cd');
//                  })
                 // ->where('second_training_schedule.datetimecd','=','1802')
                  ->select(array('venue_cd','subvenue_cd','subvenue','maxcapacity'));                
         return $filtered;
     }
}
