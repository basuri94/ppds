<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tbl_assembly extends Model
{
    protected $table ='assembly';
    
    protected $fillable = [
    'assemblycd','assemblyname','districtcd','pccd','usercode','posted_date',
    ];
}
