<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
//use Session;

class tbl_personnela extends Model
{

    protected $table = ''; 
    public $timestamps = false;
    public function __construct()
    { 
         parent::__construct(); 
         $this->table = \Session::get("personnela_ppds"); 
         
    }
    protected $fillable = [
        'personcd','poststat'
    ];
}
