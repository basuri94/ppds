<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tbl_dcrc_party extends Model
{
    protected $table ='dcrc_party';
    public $timestamps = false;
}
