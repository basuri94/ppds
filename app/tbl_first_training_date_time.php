<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tbl_first_training_date_time extends Model
{
    protected $table ='first_training_date_time';
    public $timestamps = false;
}
