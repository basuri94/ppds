<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tbl_institute extends Model
{
   protected $table ='institute';
    
    protected $fillable = [
    'institutecd','institute','usercode','posted_date'
    ];
}
