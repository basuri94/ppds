<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tbl_replacement_log_pregroup extends Model
{
    protected $table ='replacement_log_pregroup';
    public $timestamps = false;
}
