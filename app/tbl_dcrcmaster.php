<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tbl_dcrcmaster extends Model
{
    protected $table ='dcrcmaster';
    public $timestamps = false;
}
