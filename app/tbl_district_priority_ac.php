<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tbl_district_priority_ac extends Model
{
    protected $table ='district_priority_ac';
    public $timestamps = false;
}
