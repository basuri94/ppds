<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tbl_personnel extends Model
{
    protected $table ='personnel';
    public $timestamps = false;
    protected $fillable = [
        'personcd','poststat'
    ];
}
