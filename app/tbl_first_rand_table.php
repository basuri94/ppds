<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tbl_first_rand_table extends Model
{
    protected $table ='first_rand_table';
    public $timestamps = false;
}
