<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tbl_sub_submenu extends Model
{
    protected $table ='sub_submenu';
    
    protected $fillable = [
     'sub_submenu_cd','submenu_cd','sub_submenu','link',
    ];
    
}
