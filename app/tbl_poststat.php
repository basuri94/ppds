<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tbl_poststat extends Model
{
    protected $table ='poststat';
    protected $fillable = [
        'post_stat','poststatus'
    ];
}
