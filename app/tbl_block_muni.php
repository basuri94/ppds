<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tbl_block_muni extends Model
{
    protected $table ='block_muni';
    
    protected $fillable = [
       'blockminicd','subdivisioncd','blockmuni','block_or_muni','districtcd','usercode','posted_date',
    ];
}
