<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tbl_reserve extends Model
{
    protected $table ='reserve';
    public $timestamps = false;
}
