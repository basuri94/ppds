<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tbl_assembly_district extends Model
{
    protected $table ='assembly_district';
    public $timestamps = false;
    protected $fillable = ['assemblycd','districtcd'];
}
