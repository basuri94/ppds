<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tbl_second_rand_table extends Model
{
     protected $table ='second_rand_table';
     public $timestamps = false;
}
