<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tbl_second_training_schedule extends Model {

    protected $table = 'second_training_schedule';
    public $timestamps = false;

    public function ppDetailGroupBrief() {
        $filtered = "";
        $session_personnela = session()->get('personnela_ppds');
        $filtered = $this->hasMany('App\tbl_personnela', '2ndTrainingSch', 'schedule_code')
//                ->join('personnel', 'personnel.personcd', '=', '' . $session_personnela . '.personcd')
//                ->join('poststat', 'poststat.post_stat', '=', '' . $session_personnela . '.poststat')
                ->where('selected', '=', 1)
                ->where('booked', '=', 'P')
                ->distinct()
                ->select(array('2ndTrainingSch','groupid','forassembly'))
//                ->select(array('2ndTrainingSch', '' . $session_personnela . '.personcd', 'personnel.officer_name', 'personnel.off_desg as designation', 'poststat.poststatus', 'personnel.acno', 'personnel.partno', 'personnel.slno', 'personnel.mob_no'))
                ->orderBy('' . $session_personnela . '.groupid');
        return $filtered;
    }
     public function ppDetailGroupReserveBrief() {
        $filtered = "";
        $session_personnela = session()->get('personnela_ppds');
        $filtered = $this->hasMany('App\tbl_personnela', '2ndTrainingSch', 'schedule_code')
//                ->join('personnel', 'personnel.personcd', '=', '' . $session_personnela . '.personcd')
//                ->join('poststat', 'poststat.post_stat', '=', '' . $session_personnela . '.poststat')
                ->where('selected', '=', 1)
                ->where('booked', '=', 'R')
                ///->distinct()
                ->select(array('2ndTrainingSch','groupid','forassembly','personcd'))
//                ->select(array('2ndTrainingSch', '' . $session_personnela . '.personcd', 'personnel.officer_name', 'personnel.off_desg as designation', 'poststat.poststatus', 'personnel.acno', 'personnel.partno', 'personnel.slno', 'personnel.mob_no'))
                ->orderBy('' . $session_personnela . '.groupid');
        return $filtered;
    }

       public function ppDetailGroupReserveBriefExcel() {
        $filtered = "";
        $session_personnela = session()->get('personnela_ppds');
        $filtered = $this->hasMany('App\tbl_personnela', '2ndTrainingSch', 'schedule_code')
//                ->join('personnel', 'personnel.personcd', '=', '' . $session_personnela . '.personcd')
//                ->join('poststat', 'poststat.post_stat', '=', '' . $session_personnela . '.poststat')
                ->where('selected', '=', 1)
                ->where('booked', '=', 'R')
                ->distinct()
                ->select(array('2ndTrainingSch','groupid','forassembly','personcd'))
//                ->select(array('2ndTrainingSch', '' . $session_personnela . '.personcd', 'personnel.officer_name', 'personnel.off_desg as designation', 'poststat.poststatus', 'personnel.acno', 'personnel.partno', 'personnel.slno', 'personnel.mob_no'))
                ->orderBy('' . $session_personnela . '.groupid');
        return $filtered;
    }
}
