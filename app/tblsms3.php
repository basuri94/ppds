<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tblsms3 extends Model
{
    protected $table ='tblsms3';
    public $timestamps =false;
protected $fillable = [
    'phone_no','message','code', 'smscount'
];
}
