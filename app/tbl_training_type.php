<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tbl_training_type extends Model
{
   protected $table ='training_type';
    public $timestamps = false;
}
