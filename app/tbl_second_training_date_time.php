<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tbl_second_training_date_time extends Model
{
   protected $table ='second_training_date_time';  
   public $timestamps = false;
}
