<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tbl_district extends Model
{
    protected $table ='district';
    
    protected $fillable = [
     'districtcd','district','usercode','posted_date',
    ];
    public function district_wise_sub() {
        return $this->hasMany('App\tbl_subdivision','districtcd','districtcd')->select(array('subdivisioncd','subdivision'));
    }
}
