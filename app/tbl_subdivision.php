<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tbl_subdivision extends Model
{
    protected $table ='subdivision';
    
    protected $fillable = [
     'subdivisioncd','districtcd','subdivision','usercode','posted_date',
    ];
    public function sudivision_wise_block() {
        return $this->hasMany('App\tbl_block_muni','subdivisioncd','subdivisioncd')->select(array('blockminicd','blockmuni'));
    }
}
