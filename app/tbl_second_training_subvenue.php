<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tbl_second_training_subvenue extends Model
{
    protected $table ='second_training_subvenue';
    public $timestamps = false;
    protected $fillable = [
    'venue_cd','subvenue_cd','subvenue','maxcapacity','usercode'
    ];
}
