<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tbl_submenu extends Model
{
    protected $table ='submenu';
    
    protected $fillable = [
     'submenu_cd','menu_cd','submenu','link','usercode','posted_date',
    ];
    public function user_permission_submenu_cd() {
         return $this->hasMany('App\tbl_user_permission','submenu_cd','submenu_cd')->select(array('user_permission.code','user_permission.menu_cd','user_permission.submenu_cd','user_permission.sub_submenu_cd','user_permission.user_cd','user_permission.show_status'));
    }
    public function sub_submenu_submenu_cd() {
         return $this->hasMany('App\tbl_sub_submenu','submenu_cd','submenu_cd')->select(array('sub_submenu.sub_submenu_cd','sub_submenu.submenu_cd','sub_submenu.sub_submenu','sub_submenu.link'));
    }
    public function menu_menu_cd() {
         return $this->belongsTo('App\tbl_menu','menu_cd','menu_cd')->select(array('menu.menu_cd','menu.menu','menu.link','menu.usercode'));
    }
}
