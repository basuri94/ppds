<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tbl_first_training_subvenue extends Model
{
    protected $table ='first_training_subvenue';
    public $timestamps = false;
    protected $fillable = [
    'venue_cd','subvenue_cd','subvenue','maxcapacity','usercode'
    ];
}
