<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\tbl_assembly_zone;
use App\tbl_assembly_party;
use App\tbl_personnela;
use App\tbl_reserve;
use DB;

class AssemblyPartyFormationController extends Controller
{
    public function getAssemblyDetails(Request $request){
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        }
        else
        {
            $this->validate($request, [            
            'forZone' => 'required|alpha_num|min:4|max:4'
            ], [
            
            'forZone.required' => 'For Zone is required',
            'forZone.alpha_num' => 'For Zone must be an alpha numeric'
            ]);
            try
            {
              $forZone=$request->forZone;
              $list = '';
              $asmData = tbl_assembly_zone::where('zone','=',$forZone)
                                               ->join('assembly','assembly.assemblycd','=','assembly_zone.assemblycd')
                                               ->select('assembly_zone.assemblycd','assembly.assemblyname')->get();
               $trArray=json_decode($asmData);
              $list .= "<option value>[Select]</option>";
               foreach($trArray as $trDate){
                  $list .= "<option value='" . $trDate->assemblycd . "'>" . $trDate->assemblycd." - ".$trDate->assemblyname . "</option>";
                }
              $response = array(
                   'options' => $list,
                   'status' => 1);          
            }catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
              $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
      
        }
    }
    
    public function assemblyPartyForm(Request $request){
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        }
        else
        {
           $myArray1 = explode(',', $request->assembly_array);
           $myArray2 = explode(',', $request->no_of_member_array);
           $myArray3 = explode(',', $request->gender_array);
           $myArray4 = explode(',', $request->no_of_party_array);
           
           $validator1 = \Validator::make(compact('myArray1'), [
            'myArray1' => 'required|array',
            'myArray1.*' => 'alpha_num|min:3|max:3'
            ],[
             'myArray1.*.alpha_num'=>'Assembly must be an alpha numeric characters',
             'myArray1.*.min'=>'Assembly code must be 3 characters' ,
             'myArray1.*.max'=>'Assembly code must be 3 characters'
            ]);
            $this->validateWith($validator1);
            
            $validator2 = \Validator::make(compact('myArray2'), [
            'myArray2' => 'required|array',
            'myArray2.*' => 'digits_between:1,1'
            ],[
             'myArray2.*.digits_between'=>'No of Member must be an integer'  
            ]);          
            $this->validateWith($validator2);
            
             $validator3 = \Validator::make(compact('myArray3'), [
            'myArray3' => 'required|array',
            'myArray3.*' => 'alpha|min:1|max:1'
            ],[
             'myArray3.*.alpha'=>'Gender must be an alpha characters' ,
             'myArray3.*.min'=>'Assembly code must be 1 character' ,
             'myArray3.*.max'=>'Assembly code must be 1 character'
            ]);            
            $this->validateWith($validator3);
            
             $validator4 = \Validator::make(compact('myArray4'), [
            'myArray4' => 'required|array',
            'myArray4.*' => 'digits_between:1,4'
            ],[
             'myArray4.*.digits_between'=>'No of Party must be an integer'  
            ]);
            $this->validateWith($validator4);
            
            $this->validate($request, [            
            'forZone' => 'required|alpha_num|min:4|max:4'
            ], [
            
            'forZone.required' => 'For Zone is required',
            'forZone.alpha_num' => 'For Zone must be an alpha numeric characters'
            ]);
            try
            {
              $user_code=session()->get('code_ppds');  
              $forZone=$request->forZone;
              $list = '';
               if(sizeof($myArray1) == sizeof($myArray2) && sizeof($myArray1) > 0) {
                $myArraylength1 = sizeof($myArray1);
                $count1 = 0;
                $countDU=0;
                $countSM=0;
                for ($i = 1; $i <= $myArraylength1; $i++) {
                    
                    $assembly = $myArray1[$count1];
                    $no_of_member = $myArray2[$count1];
                    $gender = $myArray3[$count1];
                    $no_of_party = $myArray4[$count1];
                    
                    $tbl_assembly_party= new tbl_assembly_party();
                    $duplicate_v_code=$tbl_assembly_party->where('assemblycd','=', $assembly)
                        ->where('no_of_member','=', $no_of_member)
                        ->where('gender','=', $gender)
                        ->select(DB::raw('count(*) as cnt'))->get();
                    $du_code=json_decode($duplicate_v_code);
                    if($du_code[0]->cnt=="0")  
                    { 
                        $tbl_assembly_party->assemblycd = $assembly;
                        $tbl_assembly_party->no_of_member = $no_of_member;
                        $tbl_assembly_party->gender = $gender;
                        $tbl_assembly_party->no_party = $no_of_party;
                        $tbl_assembly_party->usercode = $user_code;
                        $tbl_assembly_party->save(); 
                                               
                        $countSM++;
                    }else{                      
                      $countDU++;
                    }
                    
                   $count1++; 
                }
              
                
                    $asmData = tbl_assembly_zone::where('zone','=',$forZone)
                                               ->join('assembly','assembly.assemblycd','=','assembly_zone.assemblycd')
                                               ->Rightjoin('assembly_party','assembly_party.assemblycd','=','assembly_zone.assemblycd')
                                               ->select('assembly_party.assemblycd')
                                               ->groupBy('assembly_party.assemblycd')
                                               ->orderBy('no_of_member')
                                               ->orderBy('gender')
                                               ->get();
                    $trArray=json_decode($asmData);
                    foreach ($trArray as $asm_cd)
                    {
                        $tbl_assembly_party_array= new tbl_assembly_party();
                         $duasm_array=$tbl_assembly_party_array->where('assemblycd','=', $asm_cd->assemblycd)
                                     ->select('assemblycd','no_of_member','gender','start_sl','no_party')->get();
                         $asm_array=json_decode($duasm_array);
                         if(count($asm_array)>1)
                         {
                             $idx = 0;
                             $u_cnt=1;
                             $sum_pp=0;
                             
                             foreach ($asm_array as $key => $value)
                             {
                                if(count($asm_array)!=$u_cnt)
                                {
                                    $sum_pp=$sum_pp+$value->no_party;
                                    $idx = $key;
                                    $next_gender=$asm_array[$idx +1]->gender;
                                    $next_asmcd=$asm_array[$idx +1]->assemblycd;
                                    $next_no_m=$asm_array[$idx +1]->no_of_member;
                                      $u_cnt++;
                                  //  $filtered="";
                                   $tbl_assembly_party_loop= new tbl_assembly_party();
                                    $tbl_assembly_party_loop->where('assemblycd','=', $next_asmcd)
                                      ->where('no_of_member','=', $next_no_m)
                                      ->where('gender','=', $next_gender)
                                      ->update(['start_sl'=>$sum_pp]); 
                                   // break;
                                  
                                }
                                else
                                {
                                   break;
                                }

                            }
                        }
                    }
               }
                $response = array(
                   'options' => $countSM,
                   'status' => 1,
                   'duplicate' => $countDU);        
            }catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
              $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
      
        }
    }

    /*  * ******************Assembly party formation list*********************************** */
    public function assem_party_formation_list_datatable(Request $request) {
        $response = [];
        $perm = null;
        $statusCode = 200;
        $users = array(); //Should be changed #4
        $search_val = array();
        $this->validate($request, [
            'search.*' => 'nullable|regex:/^[A-Za-z0-9\s]+$/i',
            'draw' => 'required|integer',
            'start' => 'required|integer',
            'length' => 'required|integer',
            'zone' => 'nullable|alpha_num|min:4|max:4',
            'assembly' => 'nullable|alpha_num|min:3|max:3',
                ], [
             'search.*.regex' => 'Special Charecters not allowed',
            'draw.required' => 'Draw is required',
            'start.integer' => 'Start must be an integer',
            'start.required' => 'Start is required',
            'length.integer' => 'Length must be an integer',
            'length.required' => 'Length is required',
            'zone.alpha_num' => 'Zone code must be an alpha numeric',
            'assembly.alpha_num' => 'Assembly code must be an alpha numeric'              
        ]);
        try {
            $draw = $request->draw;
            $offset = $request->start;
            $length = $request->length;
            $search = $request->search ["value"];
            $order = $request->order;
            $zone = $request->zone;
            $assembly = $request->assembly;
            //print_r($order);die;
            $categ = \Session::get('category_ppds');
            $dist = \Session::get('districtcd_ppds');
            $asmData = tbl_assembly_zone::where('assembly_zone.districtcd', '=', $dist)->join('assembly', 'assembly.assemblycd', '=', 'assembly_zone.assemblycd')
                    ->join('zone', 'zone.zone', '=', 'assembly_zone.zone')
                    ->Rightjoin('assembly_party', 'assembly_party.assemblycd', '=', 'assembly_zone.assemblycd')
                    ->select('zone.zonename', 'assembly_party.assemblycd', 'assembly.assemblyname', 'assembly_party.no_of_member', 'assembly_party.gender', 'assembly_party.start_sl', 'assembly_party.no_party')
                    ->orderBy('zonename')
                    ->orderBy('assemblyname')
                    ->orderBy('no_of_member')
                    ->orderBy('gender')
                    ->get();

            $filtered = tbl_assembly_zone::where('assembly_zone.districtcd', '=', $dist)->join('assembly', 'assembly.assemblycd', '=', 'assembly_zone.assemblycd')
                    ->join('zone', 'zone.zone', '=', 'assembly_zone.zone')
                    ->Rightjoin('assembly_party', 'assembly_party.assemblycd', '=', 'assembly_zone.assemblycd')
                    ->select('zone.zonename', 'assembly_party.assemblycd', 'assembly.assemblyname', 'assembly_party.no_of_member', 'assembly_party.gender', 'assembly_party.start_sl', 'assembly_party.no_party')
                    ->where(function($q) use ($search) {
                        $q->orwhere('assembly_party.assemblycd', 'like', '%' . $search . '%')
                        ->orwhere('assembly_party.no_of_member', 'like', '%' . $search . '%')
                        ->orwhere('assembly_party.no_party', 'like', '%' . $search . '%');
                    });
            if ($zone != '') {
                $filtered = $filtered->where('assembly_zone.zone', '=', $zone);
            }
            if ($assembly != '') {
                $filtered = $filtered->where('assembly_party.assemblycd', '=', $assembly);
            }
            $ordered = $filtered;
            $filtered_count = $filtered->count();
            //echo count ( $order );die;
            for ($i = 0; $i < count($order); $i ++) {
                $ordered = $ordered->orderBy($request->columns [$order [$i] ['column']] ['data'], strtoupper($order [$i] ['dir']));
            }
            $page_displayed = $ordered->orderBy('zonename')
                    ->orderBy('assemblyname')
                    ->orderBy('no_of_member')
                    ->orderBy('gender')
                    ->get();
            $data = array();
            if (!empty($page_displayed)) {
                foreach ($page_displayed as $assm) {

                    $nestedData['zonename'] = $assm->zonename;
                    $nestedData['assemblycd'] = $assm->assemblycd;
                    $nestedData['assemblyname'] = $assm->assemblyname;
                    $nestedData['no_of_member'] = $assm->no_of_member;
                    $nestedData['gender'] = $assm->gender;
                    $nestedData['no_party'] = $assm->no_party;

                    $edit_button = $delete_button = $nestedData['assemblycd'] . '/' . $nestedData['no_of_member'] . '/' . $nestedData['gender'] . '/' . $nestedData['no_party'] . '/' . $nestedData['assemblyname'];

                    $nestedData['action'] = array('e' => $edit_button, 'd' => $delete_button);
                    $data[] = $nestedData;
                }
            }
            $response = array(
                "draw" => $draw,
                "recordsTotal" => $asmData->count(), //Should be changed #7
                "recordsFiltered" => $filtered_count,
                'assembly_form' => $data //Should be changed #8
            );
        } catch (\Exception $e) {
            $response = array(
                'exception' => true,
                'exception_message' => $e->getMessage()
            );
            $statusCode = 400;
        } finally {
            return response()->json($response, $statusCode);
        }
    }

    public function check_for_edit_asmparty(Request $request) {
        $statusCode = 200;
         $this->validate($request, [
            'data' => 'required|regex:/^[A-Za-z0-9\s\/\(\)]+$/i|min:9|max:12',
                ], [
            'data.required' => 'Data is required to delete',
            'data.regex' => 'Special Charecters not allow in Data',
        ]);
        $all_value = $request->data;
        $data_all = explode("/", $all_value);
        $assemblycd = $data_all[0];
        $no_of_member = $data_all[1];
        $gender = $data_all[2];
        $no_party = $data_all[3];
        $tbl_personnela = tbl_personnela::where('gender', '=', $gender)
                                        ->where(function($q) {
                                            $q->orwhereNotNull('dcrccd')
                                              ->orwhere('dcrccd','=','');})
                                          ->where('no_of_member', '=', $no_of_member)->where('forassembly', '=', $assemblycd)->get();
        $count = $tbl_personnela->count();
        $response = array(
            'count' => $count,
        );
        return response()->json($response, $statusCode);
    }

    public function edit_assembly_party_form(Request $request) {
        $statusCode = 200;
        $this->validate($request, [
            'data' => 'required|regex:/^[A-Za-z0-9\s\/\(\)]+$/i|min:9|max:12',
            'edit_party' => 'required|digits_between:1,4'
                ], [
            'data.required' => 'Data is required To Edit',
            'data.regex' => 'Special Charecter Not allowed in Data',
            'edit_party.required' => 'No of Party is required',
            'edit_party.digits_between' => 'No of Party must be an integer and length should not be greater than 4'
        ]);
        try {
            $all_val = $request->data;
            $edit_party = $request->edit_party;
            $each_val = explode("/", $all_val);
            
            $asm_code = $each_val[0];
            $no_member = $each_val[1];
            $gender = $each_val[2];
            $no_party = $each_val[3];
            $update_data = tbl_assembly_party::where('assemblycd', '=', $asm_code)->where('gender', '=', $gender)->where('no_of_member', '=', $no_member)->where('no_party', '=', $no_party)->update(['no_party' => $edit_party]);
            $asmData = tbl_assembly_zone::join('assembly', 'assembly.assemblycd', '=', 'assembly_zone.assemblycd')
                    ->Rightjoin('assembly_party', 'assembly_party.assemblycd', '=', 'assembly_zone.assemblycd')
                    ->select('assembly_party.assemblycd')
                    ->groupBy('assembly_party.assemblycd')
                    ->orderBy('no_of_member')
                    ->orderBy('gender')
                    ->get();
            $trArray = json_decode($asmData);
            foreach ($trArray as $asm_cd) {
                $tbl_assembly_party_array = new tbl_assembly_party();
                $duasm_array = $tbl_assembly_party_array->where('assemblycd', '=', $asm_cd->assemblycd)
                                ->select('assemblycd', 'no_of_member', 'gender', 'start_sl', 'no_party')->get();
                $asm_array = json_decode($duasm_array);
                if (count($asm_array) > 1) {
                    $idx = 0;
                    $u_cnt = 1;
                    $sum_pp = 0;

                    foreach ($asm_array as $key => $value) {
                        if (count($asm_array) != $u_cnt) {
                            $sum_pp = $sum_pp + $value->no_party;
                            $idx = $key;
                            $next_gender = $asm_array[$idx + 1]->gender;
                            $next_asmcd = $asm_array[$idx + 1]->assemblycd;
                            $next_no_m = $asm_array[$idx + 1]->no_of_member;
                            $u_cnt++;
                            //  $filtered="";
                            $tbl_assembly_party_loop = new tbl_assembly_party();
                            $tbl_assembly_party_loop->where('assemblycd', '=', $next_asmcd)
                                    ->where('no_of_member', '=', $next_no_m)
                                    ->where('gender', '=', $next_gender)
                                    ->update(['start_sl' => $sum_pp]);
                            // break;
                        } else {
                            break;
                        }
                    }
                }
            }
            $response = array(
                'status' => 1,
            );
        } catch (\Exception $e) {
            $response = array(
                'exception' => true,
                'exception_message' => $e->getMessage(),
            );
            $statusCode = 400;
        } finally {
            return response()->json($response, $statusCode);
        }
    }
    
    public function check_for_delete_asmparty(Request $request) {
        
        $statusCode = 200;
         $this->validate($request, [
            'data' => 'required|regex:/^[A-Za-z0-9\s\/\(\)]+$/i|min:9|max:12',
                ], [
            'data.required' => 'Data is required to delete',
            'data.regex' => 'Special Charecters not allow in Data',
        ]);
        $all_value = $request->data;
        $data_all = explode("/", $all_value);
        $assemblycd = $data_all[0];
        $no_of_member = $data_all[1];
        $gender = $data_all[2];
        $no_party = $data_all[3];
        $tbl_personnela = tbl_personnela::where('gender', '=', $gender)                                      
                                          ->where('no_of_member', '=', $no_of_member)->where('forassembly', '=', $assemblycd)->get();
        $count = $tbl_personnela->count();
        $response = array(
            'count' => $count,
        );
        return response()->json($response, $statusCode);
    }

    public function delete_assembly_party_form(Request $request) {
        $statusCode = 200;

        $this->validate($request, [
            'data' => 'required|regex:/^[A-Za-z0-9\s\/\(\)]+$/i|min:9|max:12',
                ], [
            'data.required' => 'Data is required to delete',
            'data.regex' => 'Special Charecters not allow in Data',
        ]);
        try {
            $all_val = $request->data;
            $each_val = explode("/", $all_val);
            $asm_code = $each_val[0];
            $no_member = $each_val[1];
            $gender = $each_val[2];
            $no_party = $each_val[3];
            $update_data = tbl_assembly_party::where('assemblycd', '=', $asm_code)->where('gender', '=', $gender)->where('no_of_member', '=', $no_member)->where('no_party', '=', $no_party);
            if (!empty($update_data)) {//Should be changed #30
                $update_data = $update_data->delete();
                $update_data_reserve = tbl_reserve::where('forassembly', '=', $asm_code)->where('gender', '=', $gender)->where('number_of_member', '=', $no_member);
                $update_data_reserve = $update_data_reserve->delete();
            }
//            $response = array(
//                'status' => 1 //Should be changed #32
//            );
            $asmData = tbl_assembly_zone::join('assembly', 'assembly.assemblycd', '=', 'assembly_zone.assemblycd')
                    ->Rightjoin('assembly_party', 'assembly_party.assemblycd', '=', 'assembly_zone.assemblycd')
                    ->select('assembly_party.assemblycd')
                    ->groupBy('assembly_party.assemblycd')
                    ->orderBy('no_of_member')
                    ->orderBy('gender')
                    ->get();
            $trArray = json_decode($asmData);
            foreach ($trArray as $asm_cd) {
                $tbl_assembly_party_array = new tbl_assembly_party();
                $duasm_array = $tbl_assembly_party_array->where('assemblycd', '=', $asm_cd->assemblycd)
                                ->select('assemblycd', 'no_of_member', 'gender', 'start_sl', 'no_party')->get();
                $asm_array = json_decode($duasm_array);
                $update_first = tbl_assembly_party::where('assemblycd', '=', $asm_array[0]->assemblycd)->where('gender', '=', $asm_array[0]->gender)->where('no_of_member', '=', $asm_array[0]->no_of_member)->where('no_party', '=', $asm_array[0]->no_party)->update(['start_sl' => 0]);
                if (count($asm_array) > 1) {
                    $idx = 0;
                    $u_cnt = 1;
                    $sum_pp = 0;

                    foreach ($asm_array as $key => $value) {
                        if (count($asm_array) != $u_cnt) {
                            $sum_pp = $sum_pp + $value->no_party;
                            $idx = $key;
                            $next_gender = $asm_array[$idx + 1]->gender;
                            $next_asmcd = $asm_array[$idx + 1]->assemblycd;
                            $next_no_m = $asm_array[$idx + 1]->no_of_member;
                            $u_cnt++;
                            //  $filtered="";
                            $tbl_assembly_party_loop = new tbl_assembly_party();
                            $tbl_assembly_party_loop->where('assemblycd', '=', $next_asmcd)
                                    ->where('no_of_member', '=', $next_no_m)
                                    ->where('gender', '=', $next_gender)
                                    ->update(['start_sl' => $sum_pp]);
                            // break;
                        } else {
                            break;
                        }
                    }
                }
            }
            $response = array(
                'status' => 1
            );
        } catch (\Exception $e) {
            $response = array(
                'exception' => true,
                'exception_message' => $e->getMessage(),
            );
            $statusCode = 400;
        } finally {
            return response()->json($response, $statusCode);
        }
    }
}
