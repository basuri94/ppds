<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\tbl_subdivision;
use App\tbl_block_muni;
use App\tbl_poststat;
use App\tbl_personnel;
use App\tbl_office;
use App\tbl_zone;
use App\tbl_district;
use App\tbl_personnela;
use DB;
use Illuminate\Support\Facades\DB as FacadesDB;

class SwappingController extends Controller
{
//    public function getSubdivision(){
//         $post= $this->getPostStatus();
//         $dist= $this->getForDistrictData();
//        return view('swapping', compact('dist'))->with('poststat',$post);
//    }
      public function getSubdivision(){
         $poststat= $this->getPostStatus();
        // $dist= $this->getForDistrictData();
        return view('swapping', compact('poststat'));
    }
    public function getSwappingRand(){
         $poststat= $this->getPostStatus();
        // $dist= $this->getForDistrictData();
        return view('swapping_firstrand', compact('poststat'));
    }
    public function getSubdivisionDup(){
         $poststat= $this->getPostStatus();
        // $dist= $this->getForDistrictData();
        return view('swapping_duplicate', compact('poststat'));
    }
    public function getEDSubdivision(){
         $poststat= $this->getPostStatus();
         //$dist= $this->getForDistrictData();
        return view('swapping_ed_maximise', compact('poststat'));
    }
    //::::::::::::::::::::: Swapping Duplicate::::::::::::::::::::::::://
    public function getSubdivZoneWiseData(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        }
        else
        {
           $this->validate($request, [            
            'fromZone' => 'required|alpha_num|min:4|max:4',
            ], [
            
            'fromZone.required' => 'From Zone is required',
            'fromZone.alpha_num' => 'From Zone must be an alpha numeric',
            ]);
            $fromZone=$request->fromZone;
            try
            {
                $filtered="";
                $session_personnela=session()->get('personnela_ppds');
                $tbl_personnela = new tbl_personnela();
                $filtered = $tbl_personnela
                            ->join('subdivision','subdivision.subdivisioncd','=',DB::raw("SUBSTRING(".$session_personnela.".personcd,1,4)"))
                            ->where('forzone','=', $fromZone)
                            ->where('selected','=',1)
                            ->groupBy('subdivision.subdivisioncd')
                            ->pluck('subdivision.subdivision', 'subdivision.subdivisioncd')->all();
                $response = array(
                   'options' => $filtered,
                   'status' => 1 
                );
            }catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
              $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }        
    }
    public function getMemberDuplicateDetails(Request $request) {   
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        }
        else
        {
            $this->validate($request, [            
            'fromZone' => 'required|alpha_num|min:4|max:4',
            'subdivision' => 'nullable|alpha_num|min:4|max:4',
            'gender' => 'nullable|alpha_num|min:1|max:1',
            'blockmuni' => 'nullable|alpha_num|min:6|max:6',
            'poststatus' => 'nullable|alpha_num|min:2|max:2',
            'officeName' => 'nullable|alpha_num|min:10|max:10',
            'ExAs1' => 'nullable|alpha_num|min:3|max:3',
            'ExAs2' => 'nullable|alpha_num|min:3|max:3',
            'ExAs3' => 'nullable|alpha_num|min:3|max:3',
            'Employee' => 'nullable|digits_between:1,4'
            ], [
            
            'fromZone.required' => 'From Zone is required',
            'fromZone.alpha_num' => 'From Zone must be an alpha numeric',
            'subdivision.alpha_num' => 'Subdivision must be an alpha numeric',
            'gender.alpha_num' => 'Gender must be an alpha numeric',
            'blockmuni.alpha_num' => 'Block/Municipality must be an alpha numeric',
            'officeName.alpha_num' => 'Office Name must be an alpha numeric',
            'ExAs1.alpha_num' => 'Assembly 1 must be an alpha numeric',
            'ExAs2.alpha_num' => 'Assembly 2 must be an alpha numeric',
            'ExAs3.alpha_num' => 'Assembly 3 must be an alpha numeric',
            'Employee.digits_between' => 'Number of employee must be numeric'
            ]);
            try
            {
                $subdivision=$request->subdivision;
                $fromZone=$request->fromZone;
                $blockmuni=$request->blockmuni;
                $officeName=$request->officeName;
                $poststatus=$request->poststatus;
                $gender=$request->gender;
                $ExAs1=$request->ExAs1;
                $ExAs2=$request->ExAs2;
                $ExAs3=$request->ExAs3;
                $session_districtcd=session()->get('districtcd_ppds');
                //$personnelCheck=DB::select('call PoststatPersonnel(?,?,?,?,?,?,?,?)',[$subdivision,$blockmuni,$officeName,$poststatus,$gender,$ExAs1,$ExAs2,$ExAs3]);
                //print_r($personnelCheck);
                $session_personnela=session()->get('personnela_ppds');
                $filtered="";
                $filtered = tbl_personnela::
                            leftjoin('personnel','personnel.personcd','=',$session_personnela.'.personcd')
                             ->leftjoin('office','personnel.officecd','=','office.officecd')
                            ->where('personnel.f_cd','=',1)
                            ->where('selected','=',1)
                            ->where('forzone','=',$fromZone)
                          //  ->whereNull('termination.personal_id')
                            ->select('personnel.poststat',DB::raw('Count(personnel.personcd) as total'));
//                 if($forDist!=''){
//                    $filtered=$filtered->where('personnel.districtcd','=',$forDist);
//                }
                if($subdivision!=''){
                    $filtered=$filtered->where('personnel.subdivisioncd','=',$subdivision);
                }
                if($blockmuni!=''){
                    $filtered=$filtered->where('office.blockormuni_cd','=',$blockmuni);
                }

                if($officeName!=''){
                    $filtered=$filtered->where('personnel.officecd','=',$officeName);
                }
                 if($gender!=''){
                    $filtered=$filtered->where('personnel.gender','=',$gender);
                }
                 if($ExAs1!=''){
                    $filtered=$filtered->where('personnel.assembly_temp','!=',$ExAs1)
                                       ->where('personnel.assembly_off','!=',$ExAs1)
                                       ->where('personnel.assembly_perm','!=',$ExAs1);
                }
                 if($ExAs2!=''){
                     $filtered=$filtered->where('personnel.assembly_temp','!=',$ExAs2)
                                       ->where('personnel.assembly_off','!=',$ExAs2)
                                       ->where('personnel.assembly_perm','!=',$ExAs2);
                }
                 if($ExAs3!=''){
                    $filtered=$filtered->where('personnel.assembly_temp','!=',$ExAs3)
                                       ->where('personnel.assembly_off','!=',$ExAs3)
                                       ->where('personnel.assembly_perm','!=',$ExAs3);
                }
                $filtered = $filtered->groupBy('personnel.poststat');
                $filtered = $filtered->orderBy('personnel.poststat')->get();
                $response = array(
                   'options' => $filtered,
                   'status' => 1 
                );          
            }catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
              $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
          }       
    }
    
    //:::::::::::::::::::: Save Swapping Duplicate ::::::::::://
     public function swapping_pp_duplicate(Request $request) {  
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        }
        else
        {
           $this->validate($request, [            
            'fromZone' => 'required|alpha_num|min:4|max:4|different:toZone',
            'subdivision' => 'required|alpha_num|min:4|max:4',
            'gender' => 'nullable|alpha_num|min:1|max:1',
            'blockmuni' => 'nullable|alpha_num|min:6|max:6',
            'poststatus' => 'nullable|alpha_num|min:2|max:2',
            'toZone' => 'required|alpha_num|min:4|max:4|different:fromZone',
            'officeName' => 'nullable|alpha_num|min:10|max:10',
            'ExAs1' => 'nullable|alpha_num|min:3|max:3',
            'ExAs2' => 'nullable|alpha_num|min:3|max:3',
            'ExAs3' => 'nullable|alpha_num|min:3|max:3',
            'Employee' => 'nullable|digits_between:1,4',
            'districtcd' => 'required|alpha_num|min:2|max:2'
            ], [
            
            'fromZone.required' => 'From Zone is required',
            'fromZone.alpha_num' => 'From Zone must be an alpha numeric',
            'subdivision.required' => 'Subdivision is required',
            'subdivision.alpha_num' => 'Subdivision code must be an alpha numeric',
            'toZone.required' => 'To Zone is required',
            'toZone.alpha_num' => 'To Zone must be an alpha numeric',
            'gender.alpha_num' => 'Gender must be an alpha numeric',
            'blockmuni.alpha_num' => 'Block/Municipality must be an alpha numeric',
            'officeName.alpha_num' => 'Office Name must be an alpha numeric',
            'ExAs1.alpha_num' => 'Assembly 1 must be an alpha numeric',
            'ExAs2.alpha_num' => 'Assembly 2 must be an alpha numeric',
            'ExAs3.alpha_num' => 'Assembly 3 must be an alpha numeric',
            'Employee.digits_between' => 'Number of employee must be numeric',
            'districtcd.required' => 'Distirct is required',
            'districtcd.alpha_num' => 'Distirct must be an alpha numeric',
            ]);
           
            $subdivision=$request->subdivision;
            $blockmuni=$request->blockmuni;
            $officeName=$request->officeName;
            $poststatus=$request->poststatus;
            $gender=$request->gender;
            $ExAs1=$request->ExAs1;
            $ExAs2=$request->ExAs2;
            $ExAs3=$request->ExAs3;
            $toZone=$request->toZone;
            $Employee=$request->Employee;
            $fromZone=$request->fromZone;
            $districtcd=$request->districtcd;
            
            try
            {
                $session_personnela=session()->get('personnela_ppds');  
                $user_code=session()->get("code_ppds");
                $personnelCheck=DB::select('call swapping_duplicate(?,?,?,?,?,?,?,?,?,?,?,?,?,?)',[$subdivision,$blockmuni,$officeName,$poststatus,$gender,$ExAs1,$ExAs2,$ExAs3,$fromZone,$Employee,$toZone,$districtcd,$session_personnela,$user_code]);
                $pcount=$personnelCheck[0]->t_Count;
                $response = array(
                   'options' => $pcount,
                   'status' => 1 
                );
            }catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
              $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
            // return response()->json(['options' => $pcount,'status' => 1]);
        }
    }
    
    //:::::::::::::::::::: Swapping ::::::::::::::::::://
    public function getSubdivisionData(Request $request) {
        /*$districtcd=session()->get('districtcd_ppds');       
        $subdivision = new tbl_subdivision();
        if($districtcd!=NULL)
        {
           $subdivision = $subdivision->where('districtcd','=', $districtcd);
        }
        $subdivision = $subdivision->pluck('subdivision', 'subdivisioncd')->all();
        $data = array('subdivision' => $subdivision);
        return $data;*/
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        }
        else
        {
           $this->validate($request, [            
            'forDist' => 'required|alpha_num|min:2|max:2',
            ], [
            
            'forDist.required' => 'Subdivision is required',
            'forDist.alpha_num' => 'Subdivision code must be an alpha numeric',
            ]);
            $forDist=$request->forDist;
            try
            {
                $filtered="";
                $subdivision = new tbl_subdivision();
                $filtered = $subdivision->where('districtcd','=', $forDist)
                                           ->pluck('subdivision', 'subdivisioncd')->all();
                $response = array(
                   'options' => $filtered,
                   'status' => 1 
                );
            }catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
              $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }        
    }
    public function getPostStatus() {
        $poststatus = tbl_poststat::pluck('poststatus', 'post_stat')->all();
        $data = array('poststatus' => $poststatus);
        return $data;
        
    }
    public function getDistData(){
        $forDist = tbl_district::pluck('district', 'districtcd')->all();
        $data = array('forDist' => $forDist);
        return $data;
    }
    public function getForDistrictData(){
        $session_districtcd=session()->get('districtcd_ppds');
        $forDist = tbl_personnel::join('district','personnel.districtcd','=','district.districtcd')
                               ->where('fordistrict','=',$session_districtcd)
                               ->groupBy('personnel.districtcd')
                               ->pluck('district.district', 'district.districtcd')
                               ->all();

        $data = array('forDist' => $forDist);
        return $data;
    }
    public function getForDistDetails(Request $request){
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        }
        else
        {
            try
            {
               $session_districtcd=session()->get('districtcd_ppds');
              $tbl_zone=new tbl_personnel();
              $forzone = $tbl_zone->join('district','personnel.districtcd','=','district.districtcd')
                               ->where('fordistrict','=',$session_districtcd)
                               ->groupBy('personnel.districtcd')
                               ->pluck('district.district', 'district.districtcd')->all();
              $response = array(
                   'options' => $forzone,
                   'status' => 1);          
            }catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
              $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
      
        }
    }
    public function getBlockMuni(Request $request){
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        }
        else
        {
            $this->validate($request, [            
            'subdivision' => 'required|alpha_num|min:4|max:4'
            ], [

            'subdivision.required' => 'Subdivision is required',
            'subdivision.alpha_num' => 'Subdivision must be an alpha numeric'
            ]);
            try
            {
                $subdivision=$request->subdivision;
                $tbl_block_muni = new tbl_block_muni();
                $list = '';
                $blockmuni = $tbl_block_muni->where('subdivisioncd','=', $subdivision)->pluck('blockmuni', 'blockminicd')->all();

                $list .= "<option value>[Select]</option>";
                foreach ($blockmuni as $key => $value) {
                  $list .= "<option value='" . $key . "'>" . $value . "</option>";
                }
                $response = array(
                       'options' => $list,
                       'status' => 1);          
            }catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
              $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }
    public function getOfficeName(Request $request){
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        }
        else
        {
           $this->validate($request, [   
            'subdivision' => 'required|alpha_num|min:4|max:4',
            'blockmuni' => 'nullable|alpha_num|min:6|max:6'
            ], [
            'subdivision.required' => 'Subdivision is required',
            'subdivision.alpha_num' => 'Subdivision must be an alpha numeric',
            'blockmuni.alpha_num' => 'Block/Municipality must be an alpha numeric'
            ]);
            try
            {
                $subdivision=$request->subdivision;
                $blockmuni=$request->blockmuni;
                $office = '';
                $office = tbl_office::where('subdivisioncd','=', $subdivision);
                if($blockmuni!=''){
                  $office=$office->where('office.blockormuni_cd','=', $blockmuni);
                }
                $office=$office->pluck('office', 'officecd')->all();               
                // $data_sub = view('ajax.stream_wise_subject', compact('subjects'))->render();
                $response = array(
                   'options' => $office,
                   'status' => 1);          
            }catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
              $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }
    public function getZoneDetails(Request $request){
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        }
        else
        {
            $this->validate($request, [            
            'forDist' => 'required|alpha_num|min:2|max:2'
            ], [
            
            'forDist.required' => 'District is required',
            'forDist.alpha_num' => 'District must be an alpha numeric'
            ]);
            try
            {
              $forDist=$request->forDist;
              $session_zonecd=session()->get('zonecd_ppds');
              $tbl_zone=new tbl_zone();
              if($session_zonecd!=NULL)
              {
                   $tbl_zone = $tbl_zone->where('zone','=', $session_zonecd);
              }
              $forzone = $tbl_zone->where('districtcd','=', $forDist)
                    ->pluck('zonename', 'zone')->all();
              $response = array(
                   'options' => $forzone,
                   'status' => 1);          
            }catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
              $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
      
        }
    }
    public function getMemberDetails(Request $request) {   
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        }
        else
        {
            $this->validate($request, [            
            'forDist' => 'required|alpha_num|min:2|max:2',
            'subdivision' => 'nullable|alpha_num|min:4|max:4',
            'gender' => 'nullable|alpha_num|min:1|max:1',
            'blockmuni' => 'nullable|alpha_num|min:6|max:6',
            'poststatus' => 'nullable|alpha_num|min:2|max:2',
            'officeName' => 'nullable|alpha_num|min:10|max:10',
            'ExAs1' => 'nullable|alpha_num|min:3|max:3',
            'ExAs2' => 'nullable|alpha_num|min:3|max:3',
            'ExAs3' => 'nullable|alpha_num|min:3|max:3',
            'Employee' => 'nullable|digits_between:1,4'
            ], [
            
            'forDist.required' => 'From District is required',
            'forDist.alpha_num' => 'From District must be an alpha numeric',
            'subdivision.alpha_num' => 'Subdivision must be an alpha numeric',
            'gender.alpha_num' => 'Gender must be an alpha numeric',
            'blockmuni.alpha_num' => 'Block/Municipality must be an alpha numeric',
            'officeName.alpha_num' => 'Office Name must be an alpha numeric',
            'ExAs1.alpha_num' => 'Assembly 1 must be an alpha numeric',
            'ExAs2.alpha_num' => 'Assembly 2 must be an alpha numeric',
            'ExAs3.alpha_num' => 'Assembly 3 must be an alpha numeric',
            'Employee.digits_between' => 'Number of employee must be numeric'
            ]);
            try
            {
                $subdivision=$request->subdivision;
                $forDist=$request->forDist;
                $blockmuni=$request->blockmuni;
                $officeName=$request->officeName;
                $poststatus=$request->poststatus;
                $gender=$request->gender;
                $ExAs1=$request->ExAs1;
                $ExAs2=$request->ExAs2;
                $ExAs3=$request->ExAs3;
                $session_districtcd=session()->get('districtcd_ppds');
                //$personnelCheck=DB::select('call PoststatPersonnel(?,?,?,?,?,?,?,?)',[$subdivision,$blockmuni,$officeName,$poststatus,$gender,$ExAs1,$ExAs2,$ExAs3]);
                //print_r($personnelCheck);
                $filtered="";
                // $search = "2021-03-17";
                $filtered = tbl_personnel::leftjoin('office','personnel.officecd','=','office.officecd')
                            // ->where(function($q) {
                            //     $q->orwhereNull('personnel.f_cd')
                            //     ->orwhere('personnel.f_cd', '=', '0');
                            // })
                             ->where(function($q) {
                                $q->where('personnel.phase_1','=','0')
                                ->where('personnel.phase_2','=','0')
                                ->where('personnel.phase_3','=','0')
                                ->where('personnel.phase_4','=','0')
                                ->where('personnel.phase_5','=','0')
                                ->where('personnel.phase_6','=','0');

                            })
                            ->where('personnel.fordistrict','=',$session_districtcd)
                            ->whereNull('personnel.preexempt')
                            // ->where('booked','=','R')
                            // ->where('personnel.last_updated', 'like', '%' . $search . '%')
                            ->select('personnel.poststat',DB::raw('Count(personnel.personcd) as total'));
                 if($forDist!=''){
                    $filtered=$filtered->where('personnel.districtcd','=',$forDist);
                }
                if($subdivision!=''){
                    $filtered=$filtered->where('personnel.subdivisioncd','=',$subdivision);
                }
                if($blockmuni!=''){
                    $filtered=$filtered->where('office.blockormuni_cd','=',$blockmuni);
                }

                if($officeName!=''){
                    $filtered=$filtered->where('personnel.officecd','=',$officeName);
                }
                 if($gender!=''){
                    $filtered=$filtered->where('personnel.gender','=',$gender);
                }
                 if($ExAs1!=''){
                    $filtered=$filtered->where('personnel.assembly_temp','!=',$ExAs1)
                                       ->where('personnel.assembly_off','!=',$ExAs1)
                                       ->where('personnel.assembly_perm','!=',$ExAs1);
                }
                 if($ExAs2!=''){
                     $filtered=$filtered->where('personnel.assembly_temp','!=',$ExAs2)
                                       ->where('personnel.assembly_off','!=',$ExAs2)
                                       ->where('personnel.assembly_perm','!=',$ExAs2);
                }
                 if($ExAs3!=''){
                    $filtered=$filtered->where('personnel.assembly_temp','!=',$ExAs3)
                                       ->where('personnel.assembly_off','!=',$ExAs3)
                                       ->where('personnel.assembly_perm','!=',$ExAs3);
                }
                $filtered = $filtered->groupBy('personnel.poststat');
                $filtered = $filtered->orderBy('personnel.poststat')->get();
                $response = array(
                   'options' => $filtered,
                   'status' => 1 
                );          
            }catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
              $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
          }       
    }
    public function getZoneDetailsReqiured(Request $request){
       // dd($request->all());
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        }
        else
        {
            $this->validate($request, [            
            'forZone' => 'required|alpha_num|min:4|max:4'
            ], [           
            'forZone.required' => 'For Zone is required',
            'forZone.alpha_num' => 'For Zone must be an alpha numeric'
            ]);
           
            try
            {
                $phase_arr="";
               
                $phase=$request->phase;
               
            
               
               
                      
                $forZone=$request->forZone;
               
                $session_personnela=session()->get('personnela_ppds');
                $ZoneRecord=DB::select('call ZoneDetailsReqiured(?,?,?)',[$forZone,'',$phase]);
                $p1_stat_count=0;
                $p2_stat_count=0;
                $p3_stat_count=0;
                $pr_stat_count=0;
                $pa_stat_count=0;
                $pb_stat_count=0;
                $thCount=0;
                $ptable="";
                $totres=0;
                foreach ($ZoneRecord as $ZoneDetail){
                    $membno=$ZoneDetail->memb;
                    $n_o_p=$ZoneDetail->npc;
                    $p_numb=$ZoneDetail->pnumb;
                    $pst=$ZoneDetail->pst;
                    $preqd=$ZoneDetail->ptyrqd;

                    if(strcmp($n_o_p,'N')==0)
                    {
                        $totres=$p_numb;
                    }
                    else
                    {
                        $totres=round($p_numb*$preqd/100,0);
                    }

                    if($pst=='P1')
                    {
                        $p1_stat_count=$preqd+$p1_stat_count+$totres;
                    }
                    else if($pst=='P2')
                    {
                        $p2_stat_count=$preqd+$p2_stat_count+$totres;
                    }
                    else if($pst=='P3')
                    {
                        $p3_stat_count=$preqd+$p3_stat_count+$totres;
                    }
                    else if($pst=='PR')
                    {
                         $pr_stat_count=$preqd+$pr_stat_count+$totres;
                    }
                    else if($pst=='PA')
                    {
                         $pa_stat_count=$preqd+$pa_stat_count+$totres;
                    }
                    else if($pst=='PB')
                    {
                          $pb_stat_count=$preqd+$pb_stat_count+$totres;
                    }
                //    echo", P1:-"; echo $p1_stat_count;
                 //   echo", P2:-"; echo $p2_stat_count;
                 //   echo", P3:-"; echo $p3_stat_count;
                  //  echo", PR:-"; echo $pr_stat_count;
                }
//die;
                if($p1_stat_count !=0)
                {
                  $thCount++;
                }
                if($p2_stat_count !=0)
                {
                    $thCount++;
                }
                if($p3_stat_count !=0)
                {
                    $thCount++;
                }
                if($pa_stat_count !=0)
                {
                    $thCount++; 
                }
                if($pb_stat_count !=0)
                {
                    $thCount++;
                }
                if($pr_stat_count !=0)
                {
                    $thCount++;
                }

                if(empty($phase)){
                    $phasemodify="ALL Phase";
                }
                else{
                    $phasemodify=$phase;
                }
                $ptable.="<table class='table table-bordered table-striped1' width='100%' style='border-top: 2px solid #009fe8;'> ";
                $ptable.="<tr style='background-color: #f5f8fa'>";
                $ptable.="<th colspan=".$thCount.">Total PP Required (including Reserve)(Phase:- ".$phasemodify.")</th>";
                $ptable.="</tr>";
                $ptable.="<tr>";
                if($p1_stat_count !=0)
                {
                  $ptable.="<td style='padding:0px;border-collapse: collapse;'>";
                  $ptable.="<table width='100%'>";
                  $ptable.="<tr><td> P1";
                  $ptable.="</td></tr>";
                  $ptable.="<tr><td>".$p1_stat_count;
                  $ptable.="</td></tr>";
                  $ptable.="</table>";
                  $ptable.="</td>";
                   // echo "P1: ".$p1_stat_count.";&nbsp;";
                }
                if($p2_stat_count !=0)
                {
                  $ptable.="<td style='padding:0px;border-collapse: collapse;'>";
                  $ptable.="<table width='100%'>";
                  $ptable.="<tr><td> P2";
                  $ptable.="</td></tr>";
                  $ptable.="<tr><td>".$p2_stat_count;
                  $ptable.="</td></tr>";
                  $ptable.="</table>";
                  $ptable.="</td>";
                }
                if($p3_stat_count !=0)
                {
                  $ptable.="<td style='padding:0px;border-collapse: collapse;'>";
                  $ptable.="<table width='100%'>";
                  $ptable.="<tr><td> P3";
                  $ptable.="</td></tr>";
                  $ptable.="<tr><td>".$p3_stat_count;
                  $ptable.="</td></tr>";
                  $ptable.="</table>";
                  $ptable.="</td>";
                }
                if($pa_stat_count !=0)
                {
                  $ptable.="<td style='padding:0px;border-collapse: collapse;'>";
                  $ptable.="<table width='100%' >";
                  $ptable.="<tr><td> PA";
                  $ptable.="</td></tr>";
                  $ptable.="<tr><td>".$pa_stat_count;
                  $ptable.="</td></tr>";
                  $ptable.="</table>";
                  $ptable.="</td>";
                }
                if($pb_stat_count !=0)
                {
                  $ptable.="<td style='padding:0px;border-collapse: collapse;'>";
                  $ptable.="<table width='100%'>";
                  $ptable.="<tr><td> PB";
                  $ptable.="</td></tr>";
                  $ptable.="<tr><td>".$pb_stat_count;
                  $ptable.="</td></tr>";
                  $ptable.="</table>";
                  $ptable.="</td>";
                }
                if($pr_stat_count !=0)
                {
                  $ptable.="<td style='padding:0px;border-collapse: collapse;'>";
                  $ptable.="<table width='100%'>";
                  $ptable.="<tr><td> PR";
                  $ptable.="</td></tr>";
                  $ptable.="<tr><td>".$pr_stat_count;
                  $ptable.="</td></tr>";
                  $ptable.="</table>";
                  $ptable.="</td>";
                }          
                $ptable.="<tr>";
                $ptable.="</table>";

                //$ZoneAvailable=DB::select('call ZoneDetailsAvailable(?,?)',[$forZone,$session_personnela]);
$getPhases=tbl_personnela::select('phase','forzone')->groupBy('phase','forzone');
if(!empty($phase)){
    $getPhases=$getPhases->where('phase',$phase);
}
if(!empty($forZone)){
    $getPhases=$getPhases->where('forzone',$forZone);
}
$getPhases=$getPhases->get();
//dd($getPhases);
foreach($getPhases as $getPhases){
    $ZoneAvailable=tbl_personnela::select('poststat',DB::raw('Count('.$session_personnela.'.personcd) As total'))->where('phase',$getPhases->phase)->where('forzone',$getPhases->forzone)->groupBy('poststat')->get();
    $ptable.="<table class='table table-bordered table-striped1' width='100%' style='border-top: 2px solid #009fe8;'> ";
    $ptable.="<tr style='background-color: #f5f8fa'>";
    $ptable.="<th colspan=".count($ZoneAvailable).">Total PP Assigned (Phase:- ".$getPhases->phase.")</th>";
    $ptable.="</tr>";
    $ptable.="<tr>";
    foreach($ZoneAvailable as $zv){
        $ptable.="<td style='padding:0px;border-collapse: collapse;'>";
        $ptable.="<table width='100%'>";
        $ptable.="<tr><td>".$zv->poststat."</td></tr>";
        $ptable.="<tr><td>".$zv->total."</td></tr>";
        $ptable.="</table>";
        $ptable.="</td>";
      }
      $ptable.="<tr>";
      $ptable.="</table>";
}

                
                
              
                
                $response = array(
                   'options' => $ptable,
                   'status' => 1 
                );
            }catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
              $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
      
        }
    }
    //:::::::::::::::::Save Swapping (ED Maximise):::::://
     public function save_swapping_personnel_ed_maximise(Request $request) {  
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        }
        else
        {
           $this->validate($request, [            
            'forDist' => 'required|alpha_num|min:2|max:2',
            'subdivision' => 'required|alpha_num|min:4|max:4',
            'gender' => 'nullable|alpha_num|min:1|max:1',
            'blockmuni' => 'nullable|alpha_num|min:6|max:6',
            'poststatus' => 'nullable|alpha_num|min:2|max:2',
            'forZone' => 'required|alpha_num|min:4|max:4',
            'officeName' => 'nullable|alpha_num|min:10|max:10',
            'ExAs1' => 'nullable|alpha_num|min:3|max:3',
            'ExAs2' => 'nullable|alpha_num|min:3|max:3',
            'ExAs3' => 'nullable|alpha_num|min:3|max:3',
            'Employee' => 'nullable|digits_between:1,4',
            'districtcd' => 'required|alpha_num|min:2|max:2'
            ], [
            
            'forDist.required' => 'Subdivision is required',
            'forDist.alpha_num' => 'Subdivision code must be an alpha numeric',
            'subdivision.required' => 'Subdivision is required',
            'subdivision.alpha_num' => 'Subdivision code must be an alpha numeric',
            'forZone.required' => 'For Zone is required',
            'forZone.alpha_num' => 'For Zone must be an alpha numeric',
            'gender.alpha_num' => 'Gender must be an alpha numeric',
            'blockmuni.alpha_num' => 'Block/Municipality must be an alpha numeric',
            'officeName.alpha_num' => 'Office Name must be an alpha numeric',
            'ExAs1.alpha_num' => 'Assembly 1 must be an alpha numeric',
            'ExAs2.alpha_num' => 'Assembly 2 must be an alpha numeric',
            'ExAs3.alpha_num' => 'Assembly 3 must be an alpha numeric',
            'Employee.digits_between' => 'Number of employee must be numeric',
            'districtcd.required' => 'Distirct is required',
            'districtcd.alpha_num' => 'Distirct must be an alpha numeric',
            ]);
           
            $subdivision=$request->subdivision;
            $blockmuni=$request->blockmuni;
            $officeName=$request->officeName;
            $poststatus=$request->poststatus;
            $gender=$request->gender;
            $ExAs1=$request->ExAs1;
            $ExAs2=$request->ExAs2;
            $ExAs3=$request->ExAs3;
            $forZone=$request->forZone;
            $Employee=$request->Employee;
            $forDist=$request->forDist;
            $districtcd=$request->districtcd;
            
            try
            {
                $session_personnela=session()->get('personnela_ppds');  
                $user_code=session()->get("code_ppds");
                $personnelCheck=DB::select('call swapping_ed_maximise(?,?,?,?,?,?,?,?,?,?,?,?,?,?)',[$subdivision,$blockmuni,$officeName,$poststatus,$gender,$ExAs1,$ExAs2,$ExAs3,$forZone,$Employee,$forDist,$districtcd,$session_personnela,$user_code]);
                $pcount=$personnelCheck[0]->t_Count;
                $response = array(
                   'options' => $pcount,
                   'status' => 1 
                );
            }catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
              $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }
    //:::::::::::::::::Save Swapping:::::::::::::::::::://
    public function saveSwappingDetails(Request $request) {  
      //  dd($request->all());
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        }
        else
        {
           $this->validate($request, [            
            'forDist' => 'required|alpha_num|min:2|max:2',
            'subdivision' => 'required|alpha_num|min:4|max:4',
            'gender' => 'nullable|alpha_num|min:1|max:1',
            'blockmuni' => 'nullable|alpha_num|min:6|max:6',
            'poststatus' => 'nullable|alpha_num|min:2|max:2',
            'forZone' => 'required|alpha_num|min:4|max:4',
            'officeName' => 'nullable|alpha_num|min:10|max:10',
            'ExAs1' => 'nullable|alpha_num|min:3|max:3',
            'ExAs2' => 'nullable|alpha_num|min:3|max:3',
            'ExAs3' => 'nullable|alpha_num|min:3|max:3',
            'Employee' => 'nullable|digits_between:1,4',
            'districtcd' => 'required|alpha_num|min:2|max:2'
            ], [
            
            'forDist.required' => 'Subdivision is required',
            'forDist.alpha_num' => 'Subdivision code must be an alpha numeric',
            'subdivision.required' => 'Subdivision is required',
            'subdivision.alpha_num' => 'Subdivision code must be an alpha numeric',
            'forZone.required' => 'For Zone is required',
            'forZone.alpha_num' => 'For Zone must be an alpha numeric',
            'gender.alpha_num' => 'Gender must be an alpha numeric',
            'blockmuni.alpha_num' => 'Block/Municipality must be an alpha numeric',
            'officeName.alpha_num' => 'Office Name must be an alpha numeric',
            'ExAs1.alpha_num' => 'Assembly 1 must be an alpha numeric',
            'ExAs2.alpha_num' => 'Assembly 2 must be an alpha numeric',
            'ExAs3.alpha_num' => 'Assembly 3 must be an alpha numeric',
            'Employee.digits_between' => 'Number of employee must be numeric',
            'districtcd.required' => 'Distirct is required',
            'districtcd.alpha_num' => 'Distirct must be an alpha numeric',
            ]);
           
            $subdivision=$request->subdivision;
            $blockmuni=$request->blockmuni;
            $officeName=$request->officeName;
            $poststatus=$request->poststatus;
            $gender=$request->gender;
            $ExAs1=$request->ExAs1;
            $ExAs2=$request->ExAs2;
            $ExAs3=$request->ExAs3;
            $forZone=$request->forZone;
            $Employee=$request->Employee;
            $forDist=$request->forDist;
            $districtcd=$request->districtcd;
            $phase=$request->phase;
            $used_notused=$request->used_notused;
           
         
           
           
            try
            {
                 $condition_q3="";
                 $condition_q="";
                 $update_q="";
                 $session_personnela=session()->get('personnela_ppds'); 
               
                 if( $used_notused!='null'){
                    $phase_arr=explode(",",$used_notused);
                    $condition_q3.=" and (";
                    foreach($phase_arr as $phase1){
                        $condition_q3.="  personnel.phase_".$phase1."='1' ,";
                        
                    }
                    $condition_q2 = rtrim($condition_q3, ','); 
                    $condition_q2 = $condition_q2." )";
                    
                  
                    $condition_q = $condition_q2." and personnel.phase_".$phase."='0' ";

                    $condition_q=  str_replace(",","or","$condition_q");  
                   
                } 
                else{
                    $condition_q.=" and personnel.phase_1=0 and personnel.phase_2=0 and personnel.phase_3=0 and personnel.phase_4=0 and personnel.phase_5=0
                    and personnel.phase_6=0";
                }
                
                //    echo $condition_q;die;
                   // $update_q.=" personnel.phase_".$phase. "= '1', a.phase = "."'$phase'";
                    $update_q.=" personnel.phase_".$phase. "= '1'";
              //  echo $update_q;die;
            
                $user_code=session()->get("code_ppds");
                $personnelCheck=DB::select('call swapping(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',[$subdivision,$blockmuni,$officeName,$poststatus,$gender,$ExAs1,$ExAs2,$ExAs3,$forZone,$Employee,$forDist,$districtcd,$session_personnela,$user_code, $condition_q,$update_q,$phase]);
                $pcount=$personnelCheck[0]->t_Count;
                
              //  if( $pcount>0){
                    $response = array(
                        'options' => $pcount,
                        'status' => 1 
                     );
             //   }
                // else{
                //     $response = array(
                //         'options' => $pcount,
                //         'status' => 2 
                //      );
                // }
            }catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
              $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
            // return response()->json(['options' => $pcount,'status' => 1]);
        }
    }
    //:::::::::::::::::Save Swapping (After 1st rand):::::::::::::::::::://
    public function saveSwappingPP(Request $request) {  
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        }
        else
        {
           $this->validate($request, [            
                'forDist' => 'required|alpha_num|min:2|max:2',
                'subdivision' => 'required|alpha_num|min:4|max:4',
                'gender' => 'nullable|alpha_num|min:1|max:1',
                'blockmuni' => 'nullable|alpha_num|min:6|max:6',
                'poststatus' => 'nullable|alpha_num|min:2|max:2',
                'forZone' => 'required|alpha_num|min:4|max:4',
                'officeName' => 'nullable|alpha_num|min:10|max:10',
                'ExAs1' => 'nullable|alpha_num|min:3|max:3',
                'ExAs2' => 'nullable|alpha_num|min:3|max:3',
                'ExAs3' => 'nullable|alpha_num|min:3|max:3',
                'Employee' => 'nullable|digits_between:1,4',
                'districtcd' => 'required|alpha_num|min:2|max:2'
            ], [
            
                'forDist.required' => 'Subdivision is required',
                'forDist.alpha_num' => 'Subdivision code must be an alpha numeric',
                'subdivision.required' => 'Subdivision is required',
                'subdivision.alpha_num' => 'Subdivision code must be an alpha numeric',
                'forZone.required' => 'For Zone is required',
                'forZone.alpha_num' => 'For Zone must be an alpha numeric',
                'gender.alpha_num' => 'Gender must be an alpha numeric',
                'blockmuni.alpha_num' => 'Block/Municipality must be an alpha numeric',
                'officeName.alpha_num' => 'Office Name must be an alpha numeric',
                'ExAs1.alpha_num' => 'Assembly 1 must be an alpha numeric',
                'ExAs2.alpha_num' => 'Assembly 2 must be an alpha numeric',
                'ExAs3.alpha_num' => 'Assembly 3 must be an alpha numeric',
                'Employee.digits_between' => 'Number of employee must be numeric',
                'districtcd.required' => 'Distirct is required',
                'districtcd.alpha_num' => 'Distirct must be an alpha numeric'
            ]);
           
            $subdivision=$request->subdivision;
            $blockmuni=$request->blockmuni;
            $officeName=$request->officeName;
            $poststatus=$request->poststatus;
            $gender=$request->gender;
            $ExAs1=$request->ExAs1;
            $ExAs2=$request->ExAs2;
            $ExAs3=$request->ExAs3;
            $forZone=$request->forZone;
            $Employee=$request->Employee;
            $forDist=$request->forDist;
            $districtcd=$request->districtcd;
            $phase=$request->phase;
            $used_notused=$request->used_notused;
            try
            {
                $condition_q3="";
                 $condition_q="";
                 $update_q="";
                 $session_personnela=session()->get('personnela_ppds'); 
               
                 if( $used_notused!='null'){
                    $phase_arr=explode(",",$used_notused);
                    $condition_q3.=" and (";
                    foreach($phase_arr as $phase1){
                        $condition_q3.="  personnel.phase_".$phase1."='1' ,";
                        
                    }
                    $condition_q2 = rtrim($condition_q3, ','); 
                    $condition_q2 = $condition_q2." )";
                  
                    $condition_q = $condition_q2." and personnel.phase_".$phase."='0' ";

                    $condition_q=  str_replace(",","or","$condition_q");  
                   
                } 
                else{
                    $condition_q.=" and personnel.phase_1=0 and personnel.phase_2=0 and personnel.phase_3=0 and personnel.phase_4=0 and personnel.phase_5=0
                    and personnel.phase_6=0";
                }
                
                  //  echo $condition_q;die;
                   // $update_q.=" personnel.phase_".$phase. "= '1', a.phase = "."'$phase'";
                    $update_q.=" personnel.phase_".$phase. "= '1'";
               //echo $update_q;die;
               
                $user_code=session()->get("code_ppds");
                $personnelCheck=DB::select('call swapping_firstrand(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',[$subdivision,$blockmuni,$officeName,$poststatus,$gender,$ExAs1,$ExAs2,$ExAs3,$forZone,$Employee,$forDist,$districtcd,$session_personnela,$user_code, $condition_q,$update_q,$phase]);
                $pcount=$personnelCheck[0]->t_Count;
                $response = array(
                   'options' => $pcount,
                   'status' => 1 
                );
            }catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
              $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
            // return response()->json(['options' => $pcount,'status' => 1]);
        }
    }
    public function getFPDF(Request $request) {
        //$pdf = new Fpdf();
        $pdf = app('FPDF');
        $pdf->AddPage();
        $pdf->SetFont('Arial','B',18);
        $pdf->Cell(0,10,"Title",0,"","C");
        $pdf->Ln();
        $pdf->Ln();
        $pdf->SetFont('Arial','B',12);
        $pdf->cell(25,8,"ID",1,"","C");
        $pdf->cell(45,8,"Name",1,"","L");
        $pdf->cell(35,8,"Address",1,"","L");
        $pdf->Ln();
        $pdf->SetFont("Arial","",10);
        $pdf->cell(25,8,"1",1,"","C");
        $pdf->cell(45,8,"John",1,"","L");
        $pdf->cell(35,8,"New York",1,"","L");
        $pdf->Ln();
        $pdf->Output();
        //exit;
    }

    
}

