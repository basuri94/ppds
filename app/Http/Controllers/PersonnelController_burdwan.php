<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Personnel;
use App\Office;
use App\tbl_assembly;
use App\Language;
use App\Qualification;
use App\Remark;
use App\Poststatus;
use Session;
use Validator;
use DB;
class PersonnelController extends Controller
{
    //
     
    public function getAllOffice(){
//echo 'hi';die;
        $data= Office::where('districtcd',session()->get('districtcd_ppds'))->orderBy('office', 'asc')->get();
        return view('Personnel_list',['ofc' => $data]);

    }
    public function getPersonnelByid(Request $request){
        
        $data=Personnel::where('personcd',$request->personnel_id)->get();
      //  echo json_encode($data);die;
        return view('Personnel_edit',['person' => $data,'ac'=> tbl_assembly::where('districtcd',session()->get('districtcd_ppds'))->get(),'language'=> Language::get(),'qualification'=> Qualification::get(),'remark'=> Remark::get(),'poststatus'=> Poststatus::get()]);
    }
    public function searchpersonnel(Request $request){
      //  echo 'hi';die;
    $data=Personnel::where('personcd',$request->personnel_id)->get();
    //return empty($data);
    if(!empty($data)){
    $print='';
     foreach($data as $d){
        $print.='<tr>
                <td>'.$d->personcd.'</td>
                <td>'.$d->officer_name.'</td>
                <td>'.$d->off_desg.'</td>
                <td>'.$d->mob_no.'</td>
                <td>'.$d->email.'</td>
                <td>'.$d->bank_acc_no.'</td>
                <td>'.$d->gender.'</td>
                <td style="cursor:pointer" id='.$d->personcd.' class=editpersonnelId>Edit</td>
                </tr>hi';

     }
     
     return $print;
    }else{
        $print='';
        $print.='<tr>
                <td colspan="8">Personnel not found by this ID</td>
                
                </tr>';
                return $print;
    }
    }
    public function getPersonnelByOffice(Request $request){
   
    $data=Personnel::where('officecd',$request->office_id)->get();
    $print='';
     foreach($data as $d){
        $print.='<tr>
                <td>'.$d->personcd.'</td>
                <td>'.$d->officer_name.'</td>
                <td>'.$d->off_desg.'</td>
                <td>'.$d->mob_no.'</td>
                <td>'.$d->email.'</td>
                <td>'.$d->bank_acc_no.'</td>
                <td>'.$d->gender.'</td>
                <td style="cursor:pointer" id='.$d->personcd.' class=editpersonnelId>Edit</td>
                </tr>';
     }
     
     return $print;
    }
    public function update(Request $request)
    {
    //echo '<pre>';
	    // print_r($request->all());
   $personnelId=$request->personnel_id;

       if($personnelId!='' ){

        $validator =Validator::make($request->all(),[
             'officer_name' => 'required|string|max:50',
             'off_desg' => 'required|string|max:50',
             'resi_no'=> 'max:15',
             'present_addr1' => 'required|string|max:100',
             'perm_addr1' => 'required|string|max:100',
             'dateofbirth' => 'required|date',
             'gender' => 'required',
             'scale' => 'required|max:15',
             'basic_pay' => 'required|numeric|max:9999999',
             'grade_pay' => 'required|numeric',
             'pgroup' => 'required',
             'workingstatus' => 'required',
             'mob_no' => 'required|digits:10',
             'email' => 'nullable|email',
             'qualificationcd' => 'required',
             'languagecd' => 'required',
             'epic' => 'required|max:20',
             'partno' => 'numeric|max:9999',
             'slno' => 'numeric|max:9999',
             'assembly_temp' => 'required|numeric',
             'assembly_off' => 'required|numeric',
             'assembly_perm' => 'required|numeric',
             'branch_ifsc' => 'required|max:11',
             //'bank_acc_no' => 'required|unique:personnel,bank_acc_no|max:16'



         ]);
if($validator->fails()) {
			return redirect()->back()->withErrors($validator)->withInput();
          }
         
        $personnel =array();

       
        $personnel['officer_name']= strip_tags($request->officer_name,'');
        $personnel['off_desg']= strip_tags($request->off_desg,'');
        $personnel['qualificationcd']= strip_tags($request->qualificationcd,'');
        $personnel['languagecd']= strip_tags($request->languagecd,'');
        $personnel['dateofbirth']= strip_tags($request->dateofbirth,'');
        $personnel['gender']= strip_tags($request->gender,'');

        $personnel['scale']= strip_tags($request->scale,'');
        $personnel['basic_pay']= strip_tags($request->basic_pay,'');
        $personnel['grade_pay']= strip_tags($request->grade_pay,'');
        $personnel['workingstatus']= strip_tags($request->workingstatus,'');
        $personnel['pgroup']= strip_tags($request->pgroup,'');

        $personnel['email']= strip_tags($request->email,'');
        $personnel['resi_no']= strip_tags($request->resi_no,'');
        $personnel['mob_no']= strip_tags($request->mob_no,'');
        $personnel['present_addr1']=  strip_tags($request->present_addr1,'');
        $personnel['perm_addr1']=strip_tags($request->perm_addr1,'');
        $personnel['present_addr2']=  strip_tags($request->present_addr2,'');
        $personnel['perm_addr2']=strip_tags($request->perm_addr2,'');

        $personnel['epic']= strip_tags($request->epic,'');
        $personnel['partno']= strip_tags($request->partno,'');
        $personnel['slno']= strip_tags($request->slno,'');
        $personnel['poststat']= strip_tags($request->poststat,'');
        $personnel['assembly_off']= strip_tags($request->assembly_off,'');
        $personnel['assembly_perm']= strip_tags($request->assembly_perm,'');
        $personnel['assembly_temp']= strip_tags($request->assembly_temp,'');

        $personnel['branch_ifsc']= strip_tags($request->branch_ifsc,'');
        $personnel['bank_acc_no']= strip_tags($request->bank_acc_no,'');
        $personnel['remarks']=strip_tags($request->remarks,'');
    
       
		
		$personnela =array();
        $personnela['gender']= strip_tags($request->gender,'');
        $personnela['poststat']= strip_tags($request->poststat,'');
        $personnela['assembly_off']= strip_tags($request->assembly_off,'');
        $personnela['assembly_perm']= strip_tags($request->assembly_perm,'');
        $personnela['assembly_temp']= strip_tags($request->assembly_temp,'');
        // echo '<hr>';
        // print_r($personnel);
        // die();
        //Update personnel
        DB::table('personnel')->where('personcd', $personnelId)->update($personnel);
		//Update personnela
		DB::table('personnela'.session()->get('districtcd_ppds'))->where('personcd', $personnelId)->update($personnela);
        //return redirect('edit_personnel/'.$personnelId.'?save=1');
        return redirect('/getofficebydistrict');
       }

    } 
    public function add(){
        return view('Personnel_add',['offices'=>Office::where('districtcd',session()->get('districtcd_ppds'))->orderBy('office', 'asc')->get(),'ac'=> tbl_assembly::where('districtcd',session()->get('districtcd_ppds'))->get(),'language'=> Language::get(),'qualification'=> Qualification::get(),'remark'=> Remark::get(),'poststatus'=> Poststatus::get()]);
    }
    public function store(Request $request)
    {
		
		//echo '<pre>';
	    //print_r($request->all());
	
        $id = DB::select('SELECT MAX(CAST(SUBSTR(personcd,-5) AS UNSIGNED)) AS MaxID FROM personnel WHERE subdivisioncd = ?',[substr($request->office_id,0,4)]);

        $id = $id[0]->MaxID;

        if(is_null($id)){
            $id = substr($request->office_id,0,6).'00001';
        }
        else{
            $id = substr($request->office_id,0,6).str_pad($id+1,5,"0",STR_PAD_LEFT);
        }


        

       if($id!='' ){

        $validator =Validator::make($request->all(),[
             'officer_name' => 'required|string|max:50',
             'off_desg' => 'required|string|max:50',
             'resi_no'=> 'max:15',
             'present_addr1' => 'required|string|max:100',
             'perm_addr1' => 'required|string|max:100',
             'dateofbirth' => 'required|date',
             'gender' => 'required',
             'scale' => 'required|max:15',
             'basic_pay' => 'required|numeric|max:9999999',
             'grade_pay' => 'required|numeric',
             'pgroup' => 'required',
             'workingstatus' => 'required',
             'mob_no' => 'required|digits:10',
             'email' => 'required|email',
             'qualificationcd' => 'required',
             'languagecd' => 'required',
             'epic' => 'required|max:20',
             'partno' => 'numeric|max:9999',
             'slno' => 'numeric|max:9999',
             'assembly_temp' => 'required|numeric',
             'assembly_off' => 'required|numeric',
             'assembly_perm' => 'required|numeric',
             'branch_ifsc' => 'required|max:11',
             'bank_acc_no' => 'required|unique:personnel,bank_acc_no|max:16'



         ]);
		 if($validator->fails()) {
			return redirect()->back()->withErrors($validator)->withInput();
		  }

        $personnel =array();

        $personnel['personcd'] = $id;
        $personnel['officecd'] = strip_tags($request->office_id,'');
        $personnel['officer_name']= strip_tags($request->officer_name,'');
        $personnel['off_desg']= strip_tags($request->off_desg,'');
        //$personnel['aadhaar']= strip_tags($request->aadhaar,'');
        $personnel['qualificationcd']= strip_tags($request->qualificationcd,'');
        $personnel['languagecd']= strip_tags($request->languagecd,'');
        $personnel['dateofbirth']= strip_tags($request->dateofbirth,'');
        $personnel['gender']= strip_tags($request->gender,'');

        $personnel['scale']= strip_tags($request->scale,'');
        $personnel['basic_pay']= strip_tags($request->basic_pay,'');
        $personnel['grade_pay']= strip_tags($request->grade_pay,'');
        $personnel['workingstatus']= strip_tags($request->workingstatus,'');
        $personnel['pgroup']= strip_tags($request->pgroup,'');

        $personnel['email']= strip_tags($request->email,'');
        $personnel['resi_no']= strip_tags($request->resi_no,'');
        $personnel['mob_no']= strip_tags($request->mob_no,'');
        $personnel['present_addr1']=  strip_tags($request->present_addr1,'');
        $personnel['perm_addr1']=strip_tags($request->perm_addr1,'');
        $personnel['present_addr2']=  strip_tags($request->present_addr2,'');
        $personnel['perm_addr1']=strip_tags($request->perm_addr2,'');
        // $personnel['block_muni_temp_id']= strip_tags($request->block_muni_temp_id,'');
        // $personnel['block_muni_perm_id']=  strip_tags($request->block_muni_perm_id,'');
        // $personnel['block_muni_off_id']=  strip_tags($request->block_muni_off_id,'');

        $personnel['epic']= strip_tags($request->epic,'');
        $personnel['partno']= strip_tags($request->partno,'');
        $personnel['slno']= strip_tags($request->slno,'');
        $personnel['poststat']= strip_tags($request->poststat,'');
        $personnel['assembly_off']= strip_tags($request->assembly_off,'');
        $personnel['assembly_perm']= strip_tags($request->assembly_perm,'');
        $personnel['assembly_temp']= strip_tags($request->assembly_temp,'');

        //$personnel['post_office_account']= strip_tags($request->post_office_account,'');
        $personnel['branch_ifsc']= strip_tags($request->branch_ifsc,'');
        $personnel['bank_acc_no']= strip_tags($request->bank_acc_no,'');
        $personnel['remarks']=strip_tags($request->remarks,'');
    
        $personnel['districtcd']= substr($request->office_id,0,2);
        $personnel['subdivisioncd']= substr($request->office_id,0,4);
      //  $personnel['remark_reason']= strip_tags($request->remark_reason,'');
       // $personnel['pay_level']= strip_tags($request->pay_level,'');
       // $personnel['updated_at']=date('Y-m-d H:i:s');
    //echo '=============================================';
	//print_r($personnel);
	//exit();

        DB::table('personnel')->insert($personnel);
        return redirect('personnel_add?id='.$id.'&save=1');
       }

    } 

}
