<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\tbl_second_training_venue;
use App\tbl_second_training_subvenue;
use App\tbl_second_training_schedule;
use DB;

class SecondTrainingVenueController extends Controller {

    public function SecondTrVenue(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {

            $myArray1 = json_decode($request->subvenue_array);
            $myArray2 = json_decode($request->capacity_array);

            $validator2 = \Validator::make(compact('myArray1'), [
                        'myArray1' => 'required|array',
                        'myArray1.*' => 'regex:/^[A-Za-z0-9\s,-.]+$/i|max:30'
                            ], [
                        'myArray1.*.regex' => 'Subvenue must be alpha numeric',
                        'myArray1.*.max' => 'Subvenue must not be greater than 30 characters'
            ]);
            $this->validateWith($validator2);

            $validator3 = \Validator::make(compact('myArray2'), [
                        'myArray2' => 'required|array',
                        'myArray2.*' => 'digits_between:1,4'
                            ], [
                        'myArray2.*.digits_between' => 'Maximum Capacity must be numeric'
            ]);
            $this->validateWith($validator3);

            $validate_array = ['subdivision' => 'required|alpha_num|min:4|max:4',
                'venuename' => 'required|regex:/^[A-Za-z0-9\s,-.]+$/i|max:100',
                'venueaddress' => 'required|regex:/^[A-Za-z0-9\s,-.]+$/i|max:100',
                'maxcapacity' => 'required|digits_between:1,5'
            ];

            $validate_array1 = ['subdivision.required' => 'Subdivision is required',
                'subdivision.alpha_num' => 'Subdivision must be an alpha numeric characters',
                'venuename.required' => 'Venuename is required',
                'venuename.regex' => 'Venuename must be alpha numeric',
                'venuename.max' => 'Venuename may not be greater than 100 characters',
                'venueaddress.required' => 'Venueaddress is required',
                'venueaddress.regex' => 'Venueaddress must be alpha numeric',
                'venueaddress.max' => 'Venueaddress may not be greater than 100 characters',
                'maxcapacity.required' => 'Maximum Capacity is required',
                'maxcapacity.digits_between' => 'Maximum Capacity must be numeric'
//                              'maxcapacity.max' => 'Maximum Capacity may not be greater than 4',
            ];
            $this->validate($request, $validate_array, $validate_array1);

            try {
                $user_code = session()->get('code_ppds');

                $subdivision = $request->subdivision;
                $venuename = $request->venuename;
                $venueaddress = $request->venueaddress;
                $maxcapacity = $request->maxcapacity;

                $tbl_second_training_venue = new tbl_second_training_venue();
                $max_venue_code = $tbl_second_training_venue->where('subdivision', '=', $subdivision)
                                ->select(DB::raw('max(venue_cd) as cnt'))->get();
                $max_code = json_decode($max_venue_code);
                if ($max_code[0]->cnt == "") {
                    $venuecode = $subdivision . "01";
                } else {
                    $tmp_code = 100 + substr($max_code[0]->cnt, -2) + 1;
                    $venuecode = $subdivision . substr($tmp_code, -2);
                }
                $tbl_second_training_venue->venue_cd = $venuecode;
                $tbl_second_training_venue->subdivision = $subdivision;
                $tbl_second_training_venue->venuename = $venuename;
                $tbl_second_training_venue->venueaddress = $venueaddress;
                $tbl_second_training_venue->maximumcapacity = $maxcapacity;
                $tbl_second_training_venue->usercode = $user_code;


                $tbl_second_training_venue->save(); //save Fees Details

                if (sizeof($myArray1) == sizeof($myArray2) && sizeof($myArray1) > 0) {
                    $myArraylength1 = sizeof($myArray1);
                    $count1 = 0;
                    for ($i = 1; $i <= $myArraylength1; $i++) {
                        $sub_venue = $myArray1[$count1];
                        $capacity = $myArray2[$count1];
                        $tbl_first_training_subvenue = new tbl_second_training_subvenue();
                        $max_subvenue_code = $tbl_first_training_subvenue->where('venue_cd', '=', $venuecode)
                                        ->select(DB::raw('max(subvenue_cd) as cnt'))->get();
                        $max_subcode = json_decode($max_subvenue_code);
                        if ($max_subcode[0]->cnt == "") {
                            $subvenuecode = $venuecode . "01";
                        } else {
                            $tmp_subcode = 100 + substr($max_subcode[0]->cnt, -2) + 1;
                            $subvenuecode = $venuecode . substr($tmp_subcode, -2);
                        }
                        $tbl_first_training_subvenue->subvenue_cd = $subvenuecode;
                        $tbl_first_training_subvenue->venue_cd = $venuecode;
                        $tbl_first_training_subvenue->subvenue = $sub_venue;
                        $tbl_first_training_subvenue->maxcapacity = $capacity;
                        $tbl_first_training_subvenue->usercode = $user_code;
                        $tbl_first_training_subvenue->save();
                        $count1++;
                    }
                }
                $response = array(
                    'options' => $count1,
                    'status' => 1
                );
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }

    public function second_training_venue_list_datatable(Request $request) {
        $response = [];
        $perm = null;
        $statusCode = 200;
        $users = array(); //Should be changed #4
        $search_val = array();
        $this->validate($request, [
            'search.*' => 'nullable|regex:/^[A-Za-z0-9\s]+$/i',
            'draw' => 'required|integer',
            'start' => 'required|integer',
            'length' => 'required|integer',
            'subdivision' => 'nullable|alpha_num|min:4|max:4',
            'venuename' => 'nullable|alpha_num|min:6|max:6',
                ], [
             'search.*.regex' => 'Special Charecters not allowed',
            'draw.required' => 'Draw is required',
            'start.integer' => 'Start must be an integer',
            'start.required' => 'Start is required',
            'length.integer' => 'Length must be an integer',
            'length.required' => 'Length is required',
            'subdivision.alpha_num' => 'Subdivision Code Must be an alpha numeric',
            'venuename.alpha_num' => 'Assembly Code Must be an alpha numeric',
        ]);
        try {
            $draw = $request->draw;
            $offset = $request->start;
            $length = $request->length;
            $search = $request->search ["value"];
            $order = $request->order;
            $subdivision = $request->subdivision;
            $venuename = $request->venuename;
            //print_r($order);die;
            $categ = \Session::get('category_ppds');
            $dist = \Session::get('districtcd_ppds');

            $all = tbl_second_training_subvenue::all();
            $filtered = tbl_second_training_subvenue::join('second_training_venue', 'second_training_venue.venue_cd', '=', 'second_training_subvenue.venue_cd')
                    ->join('subdivision', 'subdivision.subdivisioncd', '=', 'second_training_venue.subdivision')
                    ->select('second_training_venue.venue_cd','second_training_subvenue.subvenue_cd', 'subdivision.subdivision', 'second_training_venue.venuename', 'second_training_venue.venueaddress', 'second_training_subvenue.subvenue', 'second_training_subvenue.maxcapacity')
                    ->where(function($q) use ($search) {
                $q->orwhere('second_training_subvenue.subvenue', 'like', '%' . $search . '%')
                ->orwhere('second_training_subvenue.maxcapacity', 'like', '%' . $search . '%');
            });
            if ($dist != '') {
                $filtered = $filtered->where(DB::raw("substring(second_training_subvenue.subvenue_cd,1,2)"), '=', $dist);
            }
            if ($subdivision != '') {
                $filtered = $filtered->where('second_training_venue.subdivision', '=', $subdivision);
            }
            if ($venuename != '') {
                $filtered = $filtered->where('second_training_subvenue.venue_cd', '=', $venuename);
            }
            $ordered = $filtered;
            $filtered_count = $filtered->count();
            //echo count ( $order );die;
            for ($i = 0; $i < count($order); $i ++) {
                $ordered = $ordered->orderBy($request->columns [$order [$i] ['column']] ['data'], strtoupper($order [$i] ['dir']));
            }
            $page_displayed = $ordered->orderBy('subdivision.subdivision')
                    ->orderBy('second_training_venue.venuename')
                    ->orderBy('second_training_subvenue.subvenue')
                    ->offset($offset)->limit($length)
                    ->get();
            $data = array();
            $i = 1;
            if (!empty($page_displayed)) {
                foreach ($page_displayed as $assm) {
                    $nestedData['code'] = $i;
                    $nestedData['subdivision'] = $assm->subdivision;
                    $nestedData['venuename'] = $assm->venuename;
                    $nestedData['venueaddress'] = $assm->venueaddress;
                    $nestedData['subvenue'] = $assm->subvenue;
                    $nestedData['maxcapacity'] = $assm->maxcapacity;
                    $nestedData['subvenue_cd'] = $assm->subvenue_cd;

                    $edit_button = $nestedData['subvenue_cd'];
                    $delete_button = $nestedData['subvenue_cd'] . "/" . $assm->venue_cd;

                    $nestedData['action'] = array('e' => $edit_button, 'd' => $delete_button);
                    $i++;
                    $data[] = $nestedData;
                }
            }
            $response = array(
                "draw" => $draw,
                "recordsTotal" => $filtered_count, //Should be changed #7
                "recordsFiltered" => $filtered_count,
                'fisrt_training_venue' => $data //Should be changed #8
            );
        } catch (\Exception $e) {
            $response = array(
                'exception' => true,
                'exception_message' => $e->getMessage()
            );
            $statusCode = 400;
        } finally {
            return response()->json($response, $statusCode);
        }
    }

    public function check_for_edit_delete_subvenue_second(Request $request) {
        $statusCode = 200;
        $this->validate($request, [
            'data' => 'required|alpha_num|min:8|max:8'
                ], [
            'data.required' => 'code is required',
            'data.alpha_num' => 'code must be an alpha numeric',
        ]);
        $subvenue_cd = $request->data;
        $check_exist = tbl_second_training_schedule::where('tr_subvenue_cd', '=', $subvenue_cd)->get();
        $check_count = $check_exist->count();
        if ($check_count == 0) {
            $data = tbl_second_training_subvenue::join('second_training_venue', 'second_training_venue.venue_cd', '=', 'second_training_subvenue.venue_cd')
                            ->join('subdivision', 'subdivision.subdivisioncd', '=', 'second_training_venue.subdivision')
                            ->select('second_training_subvenue.venue_cd', 'second_training_subvenue.subvenue_cd', 'subdivision.subdivision', 'second_training_venue.venuename', 'second_training_venue.venueaddress', 'second_training_subvenue.subvenue', 'second_training_subvenue.maxcapacity')
                            ->where('second_training_subvenue.subvenue_cd', '=', $subvenue_cd)->get();
        } else {
            $data = '';
        }
        $response = [
            'count' => $check_count,
            'rec' => $data
        ];
        return response()->json($response, $statusCode);
    }

    public function edit_second_train_venue(Request $request) {
        $statusCode = 200;
        $update = null;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in form submit.');
            return response()->json($response, $statusCode);
        }
        $response = [
            'second_train' => [] //Should be changed #9
        ];

        $this->validate($request, [
            'venue' => 'required|regex:/^[A-Za-z0-9\s,-.]+$/i|max:100',
            'venue_add' => 'required|regex:/^[A-Za-z0-9\s,-.]+$/i|max:100',
            'subvenue' => 'required|regex:/^[A-Za-z0-9\s,-.]+$/i|max:30',
            'mxcapacity' => 'required|digits_between:1,4',
            'v_code' => 'required|alpha_num|min:6|max:6',
            'sub_v_code' => 'required|alpha_num|min:8|max:8'
                ], [
            'venue.required' => 'venue is required',
            'venue.regex' => 'Venue must be an alpha numeric',
            'venue.max' => 'Venue may not be greater than 100 characters',
            'venue_add.required' => 'Venue address is required',
            'venue_add.regex' => 'Venue address must be an alpha numeric',
            'venue_add.max' => 'Venue address may not be greater than 100 characters',
            'subvenue.required' => 'Sub Venue is required',
            'subvenue.regex' => 'Sub Venue must be an alpha numeric',
            'subvenue.max' => 'Sub Venue may not be greater than 30 characters',
            'mxcapacity.required' => 'Max Capacity is required',
            'mxcapacity.digits_between' => 'Max Capacity must be integer',
            'v_code.required' => 'Venue code is required',
            'v_code.alpha_num' => 'Venue code must be an alpha numeric',
            'sub_v_code.required' => 'Venue code is required',
            'sub_v_code.alpha_num' => 'Venue code must be an alpha numeric'
        ]);
        try {
            $v_code = $request->v_code;
            $venue = $request->venue;
            $venue_add = $request->venue_add;
            $sub_v_code = $request->sub_v_code;
            $subvenue = $request->subvenue;
            $mxcapacity = $request->mxcapacity;

            $update_sub_venue = tbl_second_training_subvenue::where('subvenue_cd', '=', $sub_v_code)->update(['subvenue' => $subvenue, 'maxcapacity' => $mxcapacity]);
            $maxVenueCap = tbl_second_training_subvenue::where('venue_cd', '=', $v_code)->value(DB::raw("SUM(maxcapacity)"));
            $update_venu = tbl_second_training_venue::where('venue_cd', '=', $v_code)->update(['venuename' => $venue, 'venueaddress' => $venue_add, 'maximumcapacity' => $maxVenueCap]);
            $response = array(
                'status' => 1,
            );
        } catch (\Exception $e) {
            $response = array(
                'exception' => true,
                'exception_message' => $e->getMessage(),
            );
            $statusCode = 400;
        } finally {
            return response()->json($response, $statusCode);
        }
    }

    public function delete_second_training_venue(Request $request) {
        $statusCode = 200;
        $record = null; //Should be changed #26
        $this->validate($request, [
            'data' => 'required|alpha_num|min:8|max:8',
            'v_code' => 'required|alpha_num|min:6|max:6'
                ], [
            'data.required' => 'Sub Venuecode is required',
            'data.alpha_num' => 'Sub Venuecode must be an alpha numeric',
            'v_code.required' => 'Venue code is required',
            'v_code.alpha_num' => 'Venue code must be an alpha numeric'
        ]);
        $subvenue_cd = $request->data;
        $v_code = $request->v_code;
        try {
            $record = tbl_second_training_subvenue::where('subvenue_cd', '=', $subvenue_cd); //Should be changed #27

            if (!empty($record)) {//Should be changed #30
                $record = $record->delete();
                $maxVenueCap = tbl_second_training_subvenue::where('venue_cd', '=', $v_code)->value(DB::raw("SUM(maxcapacity)"));
                $update_venu = tbl_second_training_venue::where('venue_cd', '=', $v_code)->update(['maximumcapacity' => $maxVenueCap]);
                $check_exist = tbl_second_training_subvenue::where('venue_cd', '=', $v_code)->get();
                $check_count = $check_exist->count();
                if($check_count == 0){
                    tbl_second_training_venue::where('venue_cd', '=', $v_code)->delete();
                }
            }

            $response = array(
                'record' => $record //Should be changed #32
            );
        } catch (\Exception $e) {
            $response = array(
                'exception' => true,
                'exception_message' => $e->getMessage()
            );
            $statusCode = 400;
        } finally {
            return response()->json($response, $statusCode);
        }
    }

    public function getVenueName_second(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            $this->validate($request, [
                'subdivision' => 'required|alpha_num|min:4|max:4'
                    ], [
                'subdivision.required' => 'Subdivision is required',
                'subdivision.alpha_num' => 'Subdivision must be an alpha numeric'
            ]);
            try {
                $subdivision = $request->subdivision;
                $list = '';
                $list = tbl_second_training_venue::where('subdivision', '=', $subdivision)
                                ->pluck('venuename', 'venue_cd')->all();

                $response = array(
                    'options' => $list,
                    'status' => 1);
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }

}
