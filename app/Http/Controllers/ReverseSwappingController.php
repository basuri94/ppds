<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\tbl_zone;
use App\tbl_district;
use App\Http\Controllers\SwappingController;
use DB;
use App\tbl_personnela;
class ReverseSwappingController extends Controller
{
    private $swapp_controller;
    
    public function getDistrict(SwappingController $swapp_controller) {
        $this->swapp_controller=$swapp_controller;
       // $dist=$this->swapp_controller->getDistData();
        $poststat= $this->swapp_controller->getPostStatus();
        return view('reverse_swapping', compact('poststat'));
    }
    //::::::::::::::::::::: Reverse(ALL) swapping password check :::::::::::::://
    public function check_for_all_reverse_password(Request $request){
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        }
        else
        {
             $this->validate($request, [  
                'password' => 'required|regex:/^[a-zA-Z0-9!$#%_@]+$/'
            ], [
                'password.required' => 'Password is required'
            ]);
            
            try
            {
                $password=$request->password;
                if($password=="B%E9@cL"){
                    $count = 1;
                }else{
                    $count = 0;
                }

                  $response = array(
                   'options' => $count,                     
                   'status' => 1 
                );
                
            }
            catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
              $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }
    //::::::::::::::::::::: Reverse(ALL) swapping :::::::::::://
     public function saveAllReverseSwappingDetails(Request $request) {  
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        }
        else
        {
           $this->validate($request, [            
            'districtcd' => 'required|alpha_num|min:2|max:2',
            'forZone' => 'nullable|alpha_num|min:4|max:4',
            'phase' => 'nullable|integer',
            'poststatus' => 'nullable|alpha_num|min:2|max:2',
            'ExAs1' => 'nullable|alpha_num|min:3|max:3',
            'Employee' => 'nullable|digits_between:1,4'
            ], [
            
            'districtcd.required' => 'District code is required',
            'districtcd.alpha_num' => 'District must be an alpha numeric',
            'forZone.alpha_num' => 'For Zone must be an alpha numeric',
            'poststatus.alpha_num' => 'Post Status must be an alpha numeric',
            'ExAs1.alpha_num' => 'Excluding Assembly must be an alpha numeric',           
            'Employee.digits_between' => 'Number of employee must be numeric', 
         
            'phase.integer'=>'Invalid phase select'
            ]);
           
            $poststatus=$request->poststatus;
            $ExAs1=$request->ExAs1;
            $forZone=$request->forZone;
            $Employee=$request->Employee;
            $forDist=$request->districtcd;
            $phase=$request->phase;
            if(empty( $phase)){
                $phase=0;
            }
        
            try
            {
                $session_personnela=session()->get('personnela_ppds');
                $personnelCheck=DB::select('call reverseswappingall(?,?,?)',[$forDist,$session_personnela,$phase]);
                $pcount=$personnelCheck[0]->reverseCount;
                $response = array(
                   'options' => $pcount,
                   'status' => 1 
                );
            }catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
              $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
            // return response()->json(['options' => $pcount,'status' => 1]);
        }
    }
    
    public function getZoneDetailsReverseReqiured(Request $request){
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        }
        else
        {
            $this->validate($request, [            
            'forZone' => 'required|alpha_num|min:4|max:4'
            ], [           
            'forZone.required' => 'For Zone is required',
            'forZone.alpha_num' => 'For Zone must be an alpha numeric'
            ]);
            try
            {
                $phase=$request->phase;
                $forZone=$request->forZone;
                $session_personnela=session()->get('personnela_ppds');
                $ZoneRecord=DB::select('call ZoneDetailsReqiured(?,?,?)',[$forZone,'',$phase]);
                $p1_stat_count=0;
                $p2_stat_count=0;
                $p3_stat_count=0;
                $pr_stat_count=0;
                $pa_stat_count=0;
                $pb_stat_count=0;
                $thCount=0;
                $ptable="";
                $totres=0;
                foreach ($ZoneRecord as $ZoneDetail){
                    $membno=$ZoneDetail->memb;
                    $n_o_p=$ZoneDetail->npc;
                    $p_numb=$ZoneDetail->pnumb;
                    $pst=$ZoneDetail->pst;
                    $preqd=$ZoneDetail->ptyrqd;

                    if(strcmp($n_o_p,'N')==0)
                    {
                        $totres=$p_numb;
                    }
                    else
                    {
                        $totres=round($p_numb*$preqd/100,0);
                    }

                    if($pst=='P1')
                    {
                        $p1_stat_count=$preqd+$p1_stat_count+$totres;
                    }
                    else if($pst=='P2')
                    {
                        $p2_stat_count=$preqd+$p2_stat_count+$totres;
                    }
                    else if($pst=='P3')
                    {
                        $p3_stat_count=$preqd+$p3_stat_count+$totres;
                    }
                    else if($pst=='PR')
                    {
                         $pr_stat_count=$preqd+$pr_stat_count+$totres;
                    }
                    else if($pst=='PA')
                    {
                         $pa_stat_count=$preqd+$pa_stat_count+$totres;
                    }
                    else if($pst=='PB')
                    {
                          $pb_stat_count=$preqd+$pb_stat_count+$totres;
                    }

                }

                if($p1_stat_count !=0)
                {
                  $thCount++;
                }
                if($p2_stat_count !=0)
                {
                    $thCount++;
                }
                if($p3_stat_count !=0)
                {
                    $thCount++;
                }
                if($pa_stat_count !=0)
                {
                    $thCount++; 
                }
                if($pb_stat_count !=0)
                {
                    $thCount++;
                }
                if($pr_stat_count !=0)
                {
                    $thCount++;
                }

                if(empty($phase)){
                    $phasemodify="ALL Phase";
                }
                else{
                    $phasemodify=$phase;
                }
                $ptable.="<table class='table table-bordered table-striped1' width='100%' style='border-top: 2px solid #009fe8;'> ";
                $ptable.="<tr style='background-color: #f5f8fa'>";
                $ptable.="<th colspan=".$thCount.">Total PP Required (including Reserve) (Phase:- ".$phasemodify.")</th>";
                $ptable.="</tr>";
                $ptable.="<tr>";
                if($p1_stat_count !=0)
                {
                  $ptable.="<td style='padding:0px;border-collapse: collapse;'>";
                  $ptable.="<table width='100%'>";
                  $ptable.="<tr><td> P1";
                  $ptable.="</td></tr>";
                  $ptable.="<tr><td>".$p1_stat_count;
                  $ptable.="</td></tr>";
                  $ptable.="</table>";
                  $ptable.="</td>";
                   // echo "P1: ".$p1_stat_count.";&nbsp;";
                }
                if($p2_stat_count !=0)
                {
                  $ptable.="<td style='padding:0px;border-collapse: collapse;'>";
                  $ptable.="<table width='100%'>";
                  $ptable.="<tr><td> P2";
                  $ptable.="</td></tr>";
                  $ptable.="<tr><td>".$p2_stat_count;
                  $ptable.="</td></tr>";
                  $ptable.="</table>";
                  $ptable.="</td>";
                }
                if($p3_stat_count !=0)
                {
                  $ptable.="<td style='padding:0px;border-collapse: collapse;'>";
                  $ptable.="<table width='100%'>";
                  $ptable.="<tr><td> P3";
                  $ptable.="</td></tr>";
                  $ptable.="<tr><td>".$p3_stat_count;
                  $ptable.="</td></tr>";
                  $ptable.="</table>";
                  $ptable.="</td>";
                }
                if($pa_stat_count !=0)
                {
                  $ptable.="<td style='padding:0px;border-collapse: collapse;'>";
                  $ptable.="<table width='100%' >";
                  $ptable.="<tr><td> PA";
                  $ptable.="</td></tr>";
                  $ptable.="<tr><td>".$pa_stat_count;
                  $ptable.="</td></tr>";
                  $ptable.="</table>";
                  $ptable.="</td>";
                }
                if($pb_stat_count !=0)
                {
                  $ptable.="<td style='padding:0px;border-collapse: collapse;'>";
                  $ptable.="<table width='100%'>";
                  $ptable.="<tr><td> PB";
                  $ptable.="</td></tr>";
                  $ptable.="<tr><td>".$pb_stat_count;
                  $ptable.="</td></tr>";
                  $ptable.="</table>";
                  $ptable.="</td>";
                }
                if($pr_stat_count !=0)
                {
                  $ptable.="<td style='padding:0px;border-collapse: collapse;'>";
                  $ptable.="<table width='100%'>";
                  $ptable.="<tr><td> PR";
                  $ptable.="</td></tr>";
                  $ptable.="<tr><td>".$pr_stat_count;
                  $ptable.="</td></tr>";
                  $ptable.="</table>";
                  $ptable.="</td>";
                }          
                $ptable.="<tr>";
                $ptable.="</table>";

               // $ZoneAvailable=DB::select('call ZoneDetailsAvailableReserve(?,?)',[$forZone,$session_personnela]);

                // $ptable.="<table class='table table-bordered table-striped1' width='100%' style='border-top: 2px solid #009fe8;'> ";
                // $ptable.="<tr style='background-color: #f5f8fa'>";
                // $ptable.="<th colspan=".count($ZoneAvailable).">Total PP Available(Not selected)</th>";
                // $ptable.="</tr>";
                // $ptable.="<tr>";
                // foreach($ZoneAvailable as $zv){
                //   $ptable.="<td style='padding:0px;border-collapse: collapse;'>";
                //   $ptable.="<table width='100%'>";
                //   $ptable.="<tr><td>".$zv->poststat."</td></tr>";
                //   $ptable.="<tr><td>".$zv->total."</td></tr>";
                //   $ptable.="</table>";
                //   $ptable.="</td>";
                // }
                // $ptable.="<tr>";
                // $ptable.="</table>";

                $getPhases=tbl_personnela::select('phase')->groupBy('phase');
if(!empty($phase)){
    $getPhases=$getPhases->where('phase',$phase);
}
$getPhases=$getPhases->get();
foreach($getPhases as $getPhases){
    $ZoneAvailable=tbl_personnela::select('poststat',DB::raw('Count('.$session_personnela.'.personcd) As total'))
    ->where('phase',$getPhases->phase)
    ->where('selected',0)
    ->groupBy('poststat')->get();
    $ptable.="<table class='table table-bordered table-striped1' width='100%' style='border-top: 2px solid #009fe8;'> ";
    $ptable.="<tr style='background-color: #f5f8fa'>";
    $ptable.="<th colspan=".count($ZoneAvailable).">Total PP Available(Not selected) (Phase:- ".$getPhases->phase.")</th>";
    $ptable.="</tr>";
    $ptable.="<tr>";
    foreach($ZoneAvailable as $zv){
        $ptable.="<td style='padding:0px;border-collapse: collapse;'>";
        $ptable.="<table width='100%'>";
        $ptable.="<tr><td>".$zv->poststat."</td></tr>";
        $ptable.="<tr><td>".$zv->total."</td></tr>";
        $ptable.="</table>";
        $ptable.="</td>";
      }
      $ptable.="<tr>";
      $ptable.="</table>";
}
                
                $response = array(
                   'options' => $ptable,
                   'status' => 1 
                );
            }catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
              $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
      
        }
    }
    public function saveReverseSwappingDetails(Request $request) {  
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        }
        else
        {
           $this->validate($request, [            
            'forDist' => 'required|alpha_num|min:2|max:2',
            'forZone' => 'required|alpha_num|min:4|max:4',
            'poststatus' => 'nullable|alpha_num|min:2|max:2',
            'ExAs1' => 'nullable|alpha_num|min:3|max:3',
            'Employee' => 'nullable|digits_between:1,4'
            ], [
            
            'forDist.required' => 'For District is required',
            'forDist.alpha_num' => 'For District must be an alpha numeric',
            'forZone.required' => 'For Zone is required',
            'forZone.alpha_num' => 'For Zone must be an alpha numeric',
            'poststatus.alpha_num' => 'Post Status must be an alpha numeric',
            'ExAs1.alpha_num' => 'Excluding Assembly must be an alpha numeric',           
            'Employee.digits_between' => 'Number of employee must be numeric', 
            ]);
           
            $poststatus=$request->poststatus;
            $ExAs1=$request->ExAs1;
            $forZone=$request->forZone;
            $Employee=$request->Employee;
            $forDist=$request->forDist;
            $phase=$request->phase;
            try
            {
                $session_personnela=session()->get('personnela_ppds');
                $pcount=tbl_personnela::where('forzone',$forZone);
if(!empty( $ExAs1)){
    $pcount=$pcount->where('assembly_temp','!=',$ExAs1)->where('assembly_off','!=',$ExAs1)->where('assembly_perm','!=',$ExAs1);
}
if(!empty( $ExAs1)){
    $pcount=$pcount->where('fordistrict','=',$forDist);
}
if(!empty( $phase)){
    $pcount=$pcount->where('phase','=',$phase);
}
if(!empty( $Employee)){
    $pcount=$pcount->limit($Employee);
}
$pcount=$pcount->select('personcd')->get();
                $personnelCheck=DB::select('call reverseswapping(?,?,?,?,?,?,?)',[$poststatus,$ExAs1,$forZone,$Employee,$forDist,$session_personnela,$phase]);
               // $pcount=$personnelCheck[0]->reverseCount;
                $response = array(
                   'options' => count($pcount),
                   'status' => 1 
                );
            }catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
              $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
            // return response()->json(['options' => $pcount,'status' => 1]);
        }
    }
}
