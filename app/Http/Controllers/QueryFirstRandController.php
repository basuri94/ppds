<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\tbl_poststat;
use App\tbl_personnela;
use App\Http\Controllers\SwappingController;
use DB;

class QueryFirstRandController extends Controller
{
    private $swapping;
    public function getQueryInit(SwappingController $swapping){
         $this->swapping=$swapping;
        $poststat= $this->swapping->getPostStatus();        
        return view('queryreports_first_rand', compact('poststat'));
    }
    public function getZonewiseSubdivisionBlockData(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        }
        else
        {
           $this->validate($request, [            
            'zone' => 'required|alpha_num|min:4|max:4',
            'forDist'=> 'required|alpha_num|min:2|max:2'
            ], [           
            'zone.required' => 'Zone is required',
            'zone.alpha_num' => 'Zone must be an alpha numeric characters',
            'forDist.required' => 'District is required',
            'forDist.alpha_num' => 'District must be an alpha numeric characters'
            ]);
            $forDist=$request->forDist;
            $zone=$request->zone;
            try
            {
                $filtered="";
                $subdiv_sessioncd=session()->get('subdivisioncd_ppds'); 
                $tbl_personnela = new tbl_personnela();
                if($subdiv_sessioncd!=NULL || $subdiv_sessioncd!="")
                {
                   $tbl_personnela = $tbl_personnela->where('subdivision.subdivisioncd','=', $subdiv_sessioncd);
                } 
                if($zone!="")
                {
                   $tbl_personnela = $tbl_personnela->where('forzone','=', $zone);
                } 
                $session_personnela=session()->get('personnela_ppds');
                $filtered = $tbl_personnela
                            ->join('subdivision','subdivision.subdivisioncd','=',DB::raw("SUBSTRING(".$session_personnela.".personcd,1,4)"))
                            ->where('fordistrict','=', $forDist)
                            ->where('selected','=',1)
                            ->groupBy('subdivision.subdivisioncd')
                            ->pluck('subdivision.subdivision', 'subdivision.subdivisioncd')->all();
//              $filtered = $subdivision->where('districtcd','=', $forDist)
//                           ->pluck('subdivision', 'subdivisioncd')->all();
                $response = array(
                   'options' => $filtered,
                   'status' => 1
                );
            }catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
              $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
        
    }
    public function getZoneBlockMuniUserwise(Request $request){
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        }
        else
        {
            $this->validate($request, [  
                'forDist' => 'required|alpha_num|min:2|max:2',
                'zone' => 'required|alpha_num|min:4|max:4',
                'subdivision' => 'required|alpha_num|min:2|max:2'
            ], [
                'forDist.required' => 'District is required',
                'forDist.alpha_num' => 'District code must be an alpha numeric',
                'zone.required' => 'Zone is required',
                'zone.alpha_num' => 'Zone must be an alpha numeric characters',
                'subdivision.required' => 'Subdivision is required',
                'subdivision.alpha_num' => 'Subdivision must be an alpha numeric'
            ]);
            try
            {
                $subdivision=$request->subdivision;
                $forDist=$request->forDist;
                $zone=$request->zone;
                $list = '';
                $blockmuni_sessioncd=session()->get('blockmunicd_ppds'); 
                $tbl_personnela = new tbl_personnela();
                if($blockmuni_sessioncd!=NULL || $blockmuni_sessioncd!="")
                {
                   $tbl_personnela = $tbl_personnela->where('block_muni.blockminicd','=', $blockmuni_sessioncd);
                }
                if($zone!="")
                {
                   $tbl_personnela = $tbl_personnela->where('forzone','=', $zone);
                } 
                $session_personnela=session()->get('personnela_ppds');
                $blockmuni = $tbl_personnela
                           ->join('office',''.$session_personnela.'.officecd','=','office.officecd')
                            ->join('block_muni','office.blockormuni_cd','=','block_muni.blockminicd')
                            ->where(''.$session_personnela.'.fordistrict','=', $forDist)
                            ->where('selected','=',1)
                            ->where(DB::raw("SUBSTRING(".$session_personnela.".personcd,1,4)"),'=', $subdivision)
                            ->groupBy('block_muni.blockminicd')
                            ->pluck('block_muni.blockmuni', 'block_muni.blockminicd')->all();
                
                
//                $blockmuni = $tbl_block_muni->where('subdivisioncd','=', $subdivision)->pluck('blockmuni', 'blockminicd')->all();

                $list .= "<option value>[Select]</option>";
                foreach ($blockmuni as $key => $value) {
                  $list .= "<option value='" . $key . "'>" . $value . "</option>";
                }
                $response = array(
                       'options' => $list,
                       'status' => 1);          
            }catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
              $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }
    
    //:::::::::::::::::::: FOR PP Query ::::::::::::::::::::::::://
    public function getZonewiseSubBlockDataQuery(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        }
        else
        {
           $this->validate($request, [            
            'zone' => 'required|alpha_num|min:4|max:4',
            'forDist'=> 'required|alpha_num|min:2|max:2'
            ], [           
            'zone.required' => 'Zone is required',
            'zone.alpha_num' => 'Zone must be an alpha numeric characters',
            'forDist.required' => 'District is required',
            'forDist.alpha_num' => 'District must be an alpha numeric characters'
            ]);
            $forDist=$request->forDist;
            $zone=$request->zone;
            try
            {
                $filtered="";
                $subdiv_sessioncd=session()->get('subdivisioncd_ppds'); 
                $tbl_personnela = new tbl_personnela();
                if($subdiv_sessioncd!=NULL || $subdiv_sessioncd!="")
                {
                   $tbl_personnela = $tbl_personnela->where('subdivision.subdivisioncd','=', $subdiv_sessioncd);
                } 
                if($zone!="")
                {
                   $tbl_personnela = $tbl_personnela->where('forzone','=', $zone);
                } 
                $session_personnela=session()->get('personnela_ppds');
                $filtered = $tbl_personnela
                            ->join('subdivision','subdivision.subdivisioncd','=',DB::raw("SUBSTRING(".$session_personnela.".personcd,1,4)"))
                            ->where('fordistrict','=', $forDist)
                            ->groupBy('subdivision.subdivisioncd')
                            ->pluck('subdivision.subdivision', 'subdivision.subdivisioncd')->all();
//              $filtered = $subdivision->where('districtcd','=', $forDist)
//                           ->pluck('subdivision', 'subdivisioncd')->all();
                $response = array(
                   'options' => $filtered,
                   'status' => 1
                );
            }catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
              $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
        
    }
    public function getZoneBlockMuniUserwiseQuery(Request $request){
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        }
        else
        {
            $this->validate($request, [  
                'forDist' => 'required|alpha_num|min:2|max:2',
                'zone' => 'required|alpha_num|min:4|max:4',
                'subdivision' => 'required|alpha_num|min:4|max:4'
            ], [
                'forDist.required' => 'District is required',
                'forDist.alpha_num' => 'District code must be an alpha numeric',
                'zone.required' => 'Zone is required',
                'zone.alpha_num' => 'Zone must be an alpha numeric characters',
                'subdivision.required' => 'Subdivision is required',
                'subdivision.alpha_num' => 'Subdivision must be an alpha numeric'
            ]);
            try
            {
                $subdivision=$request->subdivision;
                $forDist=$request->forDist;
                $zone=$request->zone;
                $list = '';
                $blockmuni_sessioncd=session()->get('blockmunicd_ppds'); 
                $tbl_personnela = new tbl_personnela();
                if($blockmuni_sessioncd!=NULL || $blockmuni_sessioncd!="")
                {
                   $tbl_personnela = $tbl_personnela->where('block_muni.blockminicd','=', $blockmuni_sessioncd);
                }
                if($zone!="")
                {
                   $tbl_personnela = $tbl_personnela->where('forzone','=', $zone);
                } 
                $session_personnela=session()->get('personnela_ppds');
                $blockmuni = $tbl_personnela
                           ->join('office',''.$session_personnela.'.officecd','=','office.officecd')
                            ->join('block_muni','office.blockormuni_cd','=','block_muni.blockminicd')
                            ->where(''.$session_personnela.'.fordistrict','=', $forDist)
                            ->where(DB::raw("SUBSTRING(".$session_personnela.".personcd,1,4)"),'=', $subdivision)
                            ->groupBy('block_muni.blockminicd')
                            ->pluck('block_muni.blockmuni', 'block_muni.blockminicd')->all();
                
                
//                $blockmuni = $tbl_block_muni->where('subdivisioncd','=', $subdivision)->pluck('blockmuni', 'blockminicd')->all();

                $list .= "<option value>[Select]</option>";
                foreach ($blockmuni as $key => $value) {
                  $list .= "<option value='" . $key . "'>" . $value . "</option>";
                }
                $response = array(
                       'options' => $list,
                       'status' => 1);          
            }catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
              $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }
    public function getCategoryDetails(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        }
        else
        {
           $this->validate($request, [            
            'forDist' => 'required|alpha_num|min:2|max:2',
            'subdivision' => 'required|alpha_num|min:4|max:4',
            'zone' => 'required|alpha_num|min:4|max:4'
            ], [
            
            'forDist.required' => 'District is required',
            'forDist.alpha_num' => 'District code must be an alpha numeric',
            'subdivision.required' => 'Subdivision is required',
            'subdivision.alpha_num' => 'Subdivision must be an alpha numeric',
            'zone.required' => 'Zone is required',
            'zone.alpha_num' => 'Zone must be an alpha numeric characters'
            ]);
            $forDist=$request->forDist;
            $subdivision=$request->subdivision;
            $zone=$request->zone;
            try
            {
                $filtered=""; 
                $tbl_personnela = new tbl_personnela();             
                $session_personnela=session()->get('personnela_ppds');
                $filtered = $tbl_personnela
                           ->join('office',''.$session_personnela.'.officecd','=','office.officecd')
                            ->join('govtcategory','office.govt','=','govtcategory.govt')
                            ->where(''.$session_personnela.'.fordistrict','=', $forDist)
                            ->where(''.$session_personnela.'.forzone','=', $zone)
                            ->where(DB::raw("SUBSTRING(".$session_personnela.".personcd,1,4)"),'=', $subdivision)
                            ->groupBy('govtcategory.govt')
                            ->pluck('govtcategory.govt_description', 'govtcategory.govt')->all();
                $response = array(
                   'options' => $filtered,
                   'status' => 1
                );
            }catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
              $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
        
    }
    public function getInstituteDetails(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        }
        else
        {
           $this->validate($request, [            
            'forDist' => 'required|alpha_num|min:2|max:2',
            'subdivision' => 'required|alpha_num|min:4|max:4',
            'zone' => 'required|alpha_num|min:4|max:4'
            ], [
            
            'forDist.required' => 'District is required',
            'forDist.alpha_num' => 'District code must be an alpha numeric',
            'subdivision.required' => 'Subdivision is required',
            'subdivision.alpha_num' => 'Subdivision must be an alpha numeric',
            'zone.required' => 'Zone is required',
            'zone.alpha_num' => 'Zone must be an alpha numeric characters'
            ]);
            $forDist=$request->forDist;
            $subdivision=$request->subdivision;
            $zone=$request->zone;
            try
            {
                $filtered=""; 
                $tbl_personnela = new tbl_personnela();             
                $session_personnela=session()->get('personnela_ppds');
                $filtered = $tbl_personnela
                           ->join('office',''.$session_personnela.'.officecd','=','office.officecd')
                            ->join('institute','office.institutecd','=','institute.institutecd')
                            ->where(''.$session_personnela.'.fordistrict','=', $forDist)
                            ->where(''.$session_personnela.'.forzone','=', $zone)
                            ->where(DB::raw("SUBSTRING(".$session_personnela.".personcd,1,4)"),'=', $subdivision)
                            ->groupBy('institute.institutecd')
                            ->pluck('institute.institute', 'institute.institutecd')->all();
                $response = array(
                   'options' => $filtered,
                   'status' => 1
                );
            }catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
              $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
        
    }
     //::::::::::::::: FirstSubBlockQueryReport Excel Format::::::::::::::::::::://
    public function getFirstSubBlockQueryReport(Request $request) {
       $this->validate($request, [ 
            'districtcd' => 'required|alpha_num|min:2|max:2',
            'subdivision' => 'required|alpha_num|min:4|max:4',
            'zone' => 'required|alpha_num|min:4|max:4',
            'blockMunicipality' => 'nullable|alpha_num|min:6|max:6',
            'type' => 'required|alpha|min:1|max:1',
            'phase' => 'required|integer'
            ], [
            'districtcd.required' => 'District is required',
            'districtcd.alpha_num' => 'District code must be an alpha numeric',
            'subdivision.required' => 'Subdivision is required',
            'subdivision.alpha_num' => 'Subdivision must be an alpha numeric',
            'blockMunicipality.alpha_num' => 'Block/Municipality must be an alpha numeric',
            'zone.required' => 'Zone is required',
            'zone.alpha_num' => 'Zone must be an alpha numeric characters',
            'type.required' => 'Type is required',
            'type.alpha' => 'Type must be an alpha characters',
            'phase.required' => 'Phase is required',  
            'phase.integer' => 'Phase is must be integer'    
           ]);
        $districtcd=$request->districtcd;
        $subdivision=$request->subdivision;
        $blockcd=$request->blockMunicipality;
        $zone=$request->zone;
        $type=$request->type;
        $phase = $request->phase;
        $data = array();
        $session_personnela=session()->get('personnela_ppds');
        $tbl_personnela = new tbl_personnela();
        if($blockcd!="")
        {
           $tbl_personnela = $tbl_personnela->where('block_muni.blockminicd','=', $blockcd);
        }
        if($zone!="")
        {
           $tbl_personnela = $tbl_personnela->where('forzone','=', $zone);
        }
        if($phase!="")
        {
           $tbl_personnela = $tbl_personnela->where('phase','=', $phase);
        }  
        if($type=="C"){
             $tbl_personnela = $tbl_personnela->where('selected','=',0)->where('booked','=','C');
        }else if($type=="B"){
             $tbl_personnela = $tbl_personnela->where('selected','=',1)->whereNull('1stTrainingSch')
                      ->whereNull('1stTrainingSch_2');
        }else if($type=="D"){
             $tbl_personnela = $tbl_personnela->where('selected','=',1)->where(function($q) {
                                $q->orwhereNotNull('1stTrainingSch')
                                ->orwhereNotNull('1stTrainingSch_2');
                            });
//             $tbl_personnela = $tbl_personnela->where('selected','=',1)->whereNotNull('1stTrainingSch')
//                      ->whereNotNull('1stTrainingSch_2');
        }else if($type=="E"){
             $tbl_personnela = $tbl_personnela->where('selected','=',0)->where('booked','<>','C');
        }else if($type=="F"){
             $tbl_personnela = $tbl_personnela->where('selected','=',1)->whereNull('2ndTrainingSch');
        }else if($type=="G"){
             $tbl_personnela = $tbl_personnela->where('selected','=',1)->whereNotNull('2ndTrainingSch');
        }
        $records = $tbl_personnela->join('office', '' . $session_personnela . '.officecd', '=', 'office.officecd')
                        ->join('subdivision', 'office.subdivisioncd', '=', 'subdivision.subdivisioncd')
                        ->join('block_muni', 'office.blockormuni_cd', '=', 'block_muni.blockminicd')
                        ->join('zone', '' . $session_personnela . '.forzone', '=', 'zone.zone')
                        ->where('office.subdivisioncd','=',$subdivision)
                        ->groupBy('block_muni.blockminicd')
                        ->select('zone.zonename','subdivision.subdivision','subdivision.subdivisioncd','block_muni.blockmuni','block_muni.blockminicd')->get();
        
        foreach($records as $recordspp){
            $nestedData['Zone']=$recordspp->zonename;
            $nestedData['Subdivision']=$recordspp->subdivision;
            $nestedData['Block/Municipality']=$recordspp->blockmuni;
            
             $rsPostStat = tbl_poststat::orderBy('post_stat')
                        ->select('post_stat')->get();
            foreach($rsPostStat as $rowPstat){
                $tbl_personnela1 = new tbl_personnela();
                if($type=="C"){
                    $tbl_personnela1 = $tbl_personnela1->where('selected','=',0)->where('booked','=','C');
                }else if($type=="B"){
                     $tbl_personnela1 = $tbl_personnela1->where('selected','=',1)->whereNull('1stTrainingSch')
                              ->whereNull('1stTrainingSch_2');
                }else if($type=="D"){
                     $tbl_personnela1 = $tbl_personnela1->where('selected','=',1)->where(function($q) {
                                $q->orwhereNotNull('1stTrainingSch')
                                ->orwhereNotNull('1stTrainingSch_2');
                            });
//                     $tbl_personnela1 = $tbl_personnela1->where('selected','=',1)->whereNotNull('1stTrainingSch')
//                              ->whereNotNull('1stTrainingSch_2');
                }else if($type=="E"){
                     $tbl_personnela1 = $tbl_personnela1->where('selected','=',0)->where('booked','<>','C');
                }else if($type=="F"){
                    $tbl_personnela1 = $tbl_personnela1->where('selected','=',1)->whereNull('2ndTrainingSch');
               }else if($type=="G"){
                    $tbl_personnela1 = $tbl_personnela1->where('selected','=',1)->whereNotNull('2ndTrainingSch');
               }
                 $recordsPost = $tbl_personnela1->join('office', '' . $session_personnela . '.officecd', '=', 'office.officecd')
                            ->where('office.subdivisioncd','=',$recordspp->subdivisioncd)
                            ->where('office.blockormuni_cd','=',$recordspp->blockminicd)
                            ->where('forzone','=', $zone)
                            ->where('phase','=', $phase)
                            ->where('poststat','=', $rowPstat->post_stat)
                            ->value(DB::raw('Count(personcd)'));
                 $nestedData[$rowPstat->post_stat]=$recordsPost;                  
            }
            $data[]=$nestedData;
           
        }
        //$data1=array();
      
        $rsPostStat1 = tbl_poststat::orderBy('post_stat')
                        ->select('post_stat')->get();
        
        
        $nestedData1['Zone']='';
        $nestedData1['Subdivision']='';
        $nestedData1['Block/Municipality']='Total Records';
        foreach($rsPostStat1 as $rowPstatT){
            $tbl_personnelaR = new tbl_personnela();
             if($blockcd!="")
            {
               $tbl_personnelaR = $tbl_personnelaR->where('office.blockormuni_cd','=', $blockcd);
            }
            if($type=="C"){
                $tbl_personnelaR = $tbl_personnelaR->where('selected','=',0)->where('booked','=','C');
            }else if($type=="B"){
                 $tbl_personnelaR = $tbl_personnelaR->where('selected','=',1)->whereNull('1stTrainingSch')
                          ->whereNull('1stTrainingSch_2');
            }else if($type=="D"){
                 $tbl_personnelaR = $tbl_personnelaR->where('selected','=',1)->where(function($q) {
                                $q->orwhereNotNull('1stTrainingSch')
                                ->orwhereNotNull('1stTrainingSch_2');
                            });
//                 $tbl_personnelaR = $tbl_personnelaR->where('selected','=',1)->whereNotNull('1stTrainingSch')
//                          ->whereNotNull('1stTrainingSch_2');
            }else if($type=="E"){
                 $tbl_personnelaR = $tbl_personnelaR->where('selected','=',0)->where('booked','<>','C');
            }else if($type=="F"){
                $tbl_personnelaR = $tbl_personnelaR->where('selected','=',1)->whereNull('2ndTrainingSch');
           }else if($type=="G"){
                $tbl_personnelaR = $tbl_personnelaR->where('selected','=',1)->whereNotNull('2ndTrainingSch');
           }
            $recordsTotal = $tbl_personnelaR->join('office', '' . $session_personnela . '.officecd', '=', 'office.officecd')
                            ->where('office.subdivisioncd','=',$subdivision)
                            ->where('poststat','=', $rowPstatT->post_stat)
                            ->where('forzone','=', $zone)
                            ->where('phase','=', $phase)
                            ->value(DB::raw('Count(personcd)'));
           
            $nestedData1[$rowPstatT->post_stat]=$recordsTotal; 
        }
        $data1=$nestedData1;       
        array_push($data,$data1);
        date_default_timezone_set('Asia/Calcutta'); 
        $dt = date_create('now')->format('YmdHi');
        $reportName='SubBlockReport'.$dt;
        return \Excel::create($reportName, function($excel) use ($data) {
                    $excel->sheet(' ', function($sheet) use ($data) {
                        $sheet->fromArray($data);
                    });
                })->download('xlsx');
    }
    
    public function getFirstSubInstiQueryReport(Request $request) {
       $this->validate($request, [ 
            'districtcd' => 'required|alpha_num|min:2|max:2',
            'subdivision' => 'required|alpha_num|min:4|max:4',
            'zone' => 'required|alpha_num|min:4|max:4',
            'institute' => 'nullable|alpha_num|min:2|max:2',
            'type' => 'required|alpha|min:1|max:1',
            'phase' => 'required|integer'
            ], [
            'districtcd.required' => 'District is required',
            'districtcd.alpha_num' => 'District code must be an alpha numeric',
            'subdivision.required' => 'Subdivision is required',
            'subdivision.alpha_num' => 'Subdivision must be an alpha numeric',
            'institute.alpha_num' => 'Institute must be an alpha numeric',
            'zone.required' => 'Zone is required',
            'zone.alpha_num' => 'Zone must be an alpha numeric characters',
             'type.required' => 'Type is required',
            'type.alpha' => 'Type must be an alpha characters' ,
            'phase.required' => 'Phase is required',  
            'phase.integer' => 'Phase is must be integer' 
           ]);
        $districtcd=$request->districtcd;
        $subdivision=$request->subdivision;
        $institute=$request->institute;
        $zone=$request->zone;
        $type=$request->type;
        $phase=$request->phase;
        $data = array();
        $session_personnela=session()->get('personnela_ppds');
        $tbl_personnela = new tbl_personnela();
        if($institute!="")
        {
           $tbl_personnela = $tbl_personnela->where('institute.institutecd','=', $institute);
        }
        if($zone!="")
        {
           $tbl_personnela = $tbl_personnela->where('forzone','=', $zone);
        }
        if($phase!="")
        {
           $tbl_personnela = $tbl_personnela->where('phase','=', $phase);
        } 
        if($type=="C"){
             $tbl_personnela = $tbl_personnela->where('selected','=',0)->where('booked','=','C');
        }else if($type=="B"){
             $tbl_personnela = $tbl_personnela->where('selected','=',1)->whereNull('1stTrainingSch')
                      ->whereNull('1stTrainingSch_2');
        }else if($type=="D"){
             $tbl_personnela = $tbl_personnela->where('selected','=',1)->where(function($q) {
                                $q->orwhereNotNull('1stTrainingSch')
                                ->orwhereNotNull('1stTrainingSch_2');
                            });
//             $tbl_personnela = $tbl_personnela->where('selected','=',1)->whereNotNull('1stTrainingSch')
//                      ->whereNotNull('1stTrainingSch_2');
        }else if($type=="E"){
             $tbl_personnela = $tbl_personnela->where('selected','=',0)->where('booked','<>','C');
        }else if($type=="F"){
             $tbl_personnela = $tbl_personnela->where('selected','=',1)->whereNull('2ndTrainingSch');
        }else if($type=="G"){
             $tbl_personnela = $tbl_personnela->where('selected','=',1)->whereNotNull('2ndTrainingSch');
        }
        $records = $tbl_personnela->join('office', '' . $session_personnela . '.officecd', '=', 'office.officecd')
                        ->join('subdivision', 'office.subdivisioncd', '=', 'subdivision.subdivisioncd')
                        ->join('institute','office.institutecd','=','institute.institutecd')
                        ->join('zone', '' . $session_personnela . '.forzone', '=', 'zone.zone')                     
                        ->where('office.subdivisioncd','=',$subdivision)
                        ->groupBy('institute.institutecd')
                        ->select('zone.zonename','subdivision.subdivision','subdivision.subdivisioncd','institute.institute','institute.institutecd')->get();
        
        foreach($records as $recordspp){
            $nestedData['Zone']=$recordspp->zonename;
            $nestedData['Subdivision']=$recordspp->subdivision;
            $nestedData['Institute']=$recordspp->institute;
                      
            $rsPostStat = tbl_poststat::orderBy('post_stat')
                        ->select('post_stat')->get();
            foreach($rsPostStat as $rowPstat){
                $tbl_personnela1 = new tbl_personnela();
                if($type=="C"){
                    $tbl_personnela1 = $tbl_personnela1->where('selected','=',0)->where('booked','=','C');
                }else if($type=="B"){
                     $tbl_personnela1 = $tbl_personnela1->where('selected','=',1)->whereNull('1stTrainingSch')
                              ->whereNull('1stTrainingSch_2');
                }else if($type=="D"){
                        $tbl_personnela1 = $tbl_personnela1->where('selected','=',1)->where(function($q) {
                                $q->orwhereNotNull('1stTrainingSch')
                                ->orwhereNotNull('1stTrainingSch_2');
                            });
//                     $tbl_personnela1 = $tbl_personnela1->where('selected','=',1)->whereNotNull('1stTrainingSch')
//                              ->whereNotNull('1stTrainingSch_2');
                }else if($type=="E"){
                     $tbl_personnela1 = $tbl_personnela1->where('selected','=',0)->where('booked','<>','C');
                }else if($type=="F"){
                    $tbl_personnela1 = $tbl_personnela1->where('selected','=',1)->whereNull('2ndTrainingSch');
                }else if($type=="G"){
                    $tbl_personnela1 = $tbl_personnela1->where('selected','=',1)->whereNotNull('2ndTrainingSch');
                }
                 $recordsPost = $tbl_personnela1->join('office', '' . $session_personnela . '.officecd', '=', 'office.officecd')
                            ->where('office.subdivisioncd','=',$recordspp->subdivisioncd)
                            ->where('office.institutecd','=',$recordspp->institutecd)
                            ->where('forzone','=', $zone)
                            ->where('phase','=', $phase)
                            ->where('poststat','=', $rowPstat->post_stat)
                            ->value(DB::raw('Count(personcd)'));
                 $nestedData[$rowPstat->post_stat]=$recordsPost;                  
            }
            $data[]=$nestedData;
        }
        
        
        $rsPostStat1 = tbl_poststat::orderBy('post_stat')
                        ->select('post_stat')->get();
        $nestedData1['Zone']='';
        $nestedData1['Subdivision']='';
         $nestedData1['Institute']='Total Records';
        foreach($rsPostStat1 as $rowPstatT){
            $tbl_personnelaR = new tbl_personnela();
            if($institute!="")
            {
               $tbl_personnelaR = $tbl_personnelaR->where('institute.institutecd','=', $institute);
            }
            if($type=="C"){
                $tbl_personnelaR = $tbl_personnelaR->where('selected','=',0)->where('booked','=','C');
            }else if($type=="B"){
                 $tbl_personnelaR = $tbl_personnelaR->where('selected','=',1)->whereNull('1stTrainingSch')
                          ->whereNull('1stTrainingSch_2');
            }else if($type=="D"){
                 $tbl_personnelaR = $tbl_personnelaR->where('selected','=',1)->where(function($q) {
                                $q->orwhereNotNull('1stTrainingSch')
                                ->orwhereNotNull('1stTrainingSch_2');
                            });
//                 $tbl_personnelaR = $tbl_personnelaR->where('selected','=',1)->whereNotNull('1stTrainingSch')
//                          ->whereNotNull('1stTrainingSch_2');
            }else if($type=="E"){
                 $tbl_personnelaR = $tbl_personnelaR->where('selected','=',0)->where('booked','<>','C');
            }else if($type=="F"){
                $tbl_personnelaR = $tbl_personnelaR->where('selected','=',1)->whereNull('2ndTrainingSch');
           }else if($type=="G"){
                $tbl_personnelaR = $tbl_personnelaR->where('selected','=',1)->whereNotNull('2ndTrainingSch');
           }
            $recordsTotal = $tbl_personnelaR->join('office', '' . $session_personnela . '.officecd', '=', 'office.officecd')
                            ->where('office.subdivisioncd','=',$subdivision)
                            ->where('poststat','=', $rowPstatT->post_stat)
                            ->where('forzone','=', $zone)
                            ->where('phase','=', $phase)
                            ->value(DB::raw('Count(personcd)'));
           
            $nestedData1[$rowPstatT->post_stat]=$recordsTotal; 
        }
          
        $data1=$nestedData1;       
        array_push($data,$data1);
        date_default_timezone_set('Asia/Calcutta'); 
        $dt = date_create('now')->format('YmdHi');
        $reportName='SubInstituteReport'.$dt;
        return \Excel::create($reportName, function($excel) use ($data) {
                    $excel->sheet(' ', function($sheet) use ($data) {
                        $sheet->fromArray($data);
                    });
                })->download('xlsx');
    }
    
    public function getFirstSubCatQueryReport(Request $request) {
       $this->validate($request, [ 
            'districtcd' => 'required|alpha_num|min:2|max:2',
            'subdivision' => 'required|alpha_num|min:4|max:4',
            'zone' => 'required|alpha_num|min:4|max:4',
            'category' => 'nullable|alpha_num|min:2|max:2',
            'type' => 'required|alpha|min:1|max:1',
            'phase' => 'required|integer'
            ], [
            'districtcd.required' => 'District is required',
            'districtcd.alpha_num' => 'District code must be an alpha numeric',
            'subdivision.required' => 'Subdivision is required',
            'subdivision.alpha_num' => 'Subdivision must be an alpha numeric',
            'category.alpha_num' => 'Category must be an alpha numeric',
            'zone.required' => 'Zone is required',
            'zone.alpha_num' => 'Zone must be an alpha numeric characters',
            'type.required' => 'Type is required',
            'type.alpha' => 'Type must be an alpha characters',  
            'phase.required' => 'Phase is required',  
            'phase.integer' => 'Phase is must be integer'  
           ]);
        $districtcd=$request->districtcd;
        $subdivision=$request->subdivision;
        $category=$request->category;
        $zone=$request->zone;
        $type=$request->type;
         $phase = $request->phase;
        $data = array();
        $session_personnela=session()->get('personnela_ppds');
        $tbl_personnela = new tbl_personnela();
        if($category!="")
        {
           $tbl_personnela = $tbl_personnela->where('govtcategory.govt','=', $category);
        }
        if($zone!="")
        {
           $tbl_personnela = $tbl_personnela->where('forzone','=', $zone);
        } 
        if($phase!="")
        {
           $tbl_personnela = $tbl_personnela->where('phase','=', $phase);
        } 
        if($type=="C"){
             $tbl_personnela = $tbl_personnela->where('selected','=',0)->where('booked','=','C');
        }else if($type=="B"){
             $tbl_personnela = $tbl_personnela->where('selected','=',1)->whereNull('1stTrainingSch')
                      ->whereNull('1stTrainingSch_2');
        }else if($type=="D"){
             $tbl_personnela = $tbl_personnela->where('selected','=',1)->where(function($q) {
                                $q->orwhereNotNull('1stTrainingSch')
                                ->orwhereNotNull('1stTrainingSch_2');
                            });
//             $tbl_personnela = $tbl_personnela->where('selected','=',1)->whereNotNull('1stTrainingSch')
//                      ->whereNotNull('1stTrainingSch_2');
        }else if($type=="E"){
             $tbl_personnela = $tbl_personnela->where('selected','=',0)->where('booked','<>','C');
        }else if($type=="F"){
             $tbl_personnela = $tbl_personnela->where('selected','=',1)->whereNull('2ndTrainingSch');
        }else if($type=="G"){
             $tbl_personnela = $tbl_personnela->where('selected','=',1)->whereNotNull('2ndTrainingSch');
        }
        $records = $tbl_personnela->join('office', '' . $session_personnela . '.officecd', '=', 'office.officecd')
                        ->join('subdivision', 'office.subdivisioncd', '=', 'subdivision.subdivisioncd')
                        ->join('govtcategory','office.govt','=','govtcategory.govt')
                        ->join('zone', '' . $session_personnela . '.forzone', '=', 'zone.zone')
                        ->where('office.subdivisioncd','=',$subdivision)
                        ->groupBy('govtcategory.govt')
                        ->select('zone.zonename','subdivision.subdivision','subdivision.subdivisioncd','govtcategory.govt_description','govtcategory.govt')->get();
     
        foreach($records as $recordspp){
            $nestedData['Zone']=$recordspp->zonename;
            $nestedData['Subdivision']=$recordspp->subdivision;
            $nestedData['Category']=$recordspp->govt_description;
            
            $rsPostStat = tbl_poststat::orderBy('post_stat')
                        ->select('post_stat')->get();
            foreach($rsPostStat as $rowPstat){
                $tbl_personnela1 = new tbl_personnela();
                if($type=="C"){
                    $tbl_personnela1 = $tbl_personnela1->where('selected','=',0)->where('booked','=','C');
                }else if($type=="B"){
                     $tbl_personnela1 = $tbl_personnela1->where('selected','=',1)->whereNull('1stTrainingSch')
                              ->whereNull('1stTrainingSch_2');
                }else if($type=="D"){
                     $tbl_personnela1 = $tbl_personnela1->where('selected','=',1)->where(function($q) {
                                $q->orwhereNotNull('1stTrainingSch')
                                ->orwhereNotNull('1stTrainingSch_2');
                            });
//                     $tbl_personnela1 = $tbl_personnela1->where('selected','=',1)->whereNotNull('1stTrainingSch')
//                              ->whereNotNull('1stTrainingSch_2');
                }else if($type=="E"){
                     $tbl_personnela1 = $tbl_personnela1->where('selected','=',0)->where('booked','<>','C');
                }else if($type=="F"){
                    $tbl_personnela1 = $tbl_personnela1->where('selected','=',1)->whereNull('2ndTrainingSch');
                }else if($type=="G"){
                    $tbl_personnela1 = $tbl_personnela1->where('selected','=',1)->whereNotNull('2ndTrainingSch');
                }
                $recordsPost = $tbl_personnela1->join('office', '' . $session_personnela . '.officecd', '=', 'office.officecd')

                            ->where('office.subdivisioncd','=',$recordspp->subdivisioncd)
                            ->where('office.govt','=',$recordspp->govt)
                            ->where('forzone','=', $zone)
                            ->where('phase','=', $phase)
                            ->where('poststat','=', $rowPstat->post_stat)
                            ->value(DB::raw('Count(personcd)'));
                 $nestedData[$rowPstat->post_stat]=$recordsPost;                  
            }
            $data[]=$nestedData;
            
        }
       
        $rsPostStat1 = tbl_poststat::orderBy('post_stat')
                        ->select('post_stat')->get();
        $nestedData1['Zone']='';
        $nestedData1['Subdivision']='';
        $nestedData1['Category']='Total Records';
        foreach($rsPostStat1 as $rowPstatT){
            $tbl_personnelaR = new tbl_personnela();
            if($category!="")
            {
               $tbl_personnelaR = $tbl_personnelaR->where('office.govt','=', $category);
            }
            if($type=="C"){
                $tbl_personnelaR = $tbl_personnelaR->where('selected','=',0)->where('booked','=','C');
            }else if($type=="B"){
                 $tbl_personnelaR = $tbl_personnelaR->where('selected','=',1)->whereNull('1stTrainingSch')
                          ->whereNull('1stTrainingSch_2');
            }else if($type=="D"){
                 $tbl_personnelaR = $tbl_personnelaR->where('selected','=',1)->where(function($q) {
                                $q->orwhereNotNull('1stTrainingSch')
                                ->orwhereNotNull('1stTrainingSch_2');
                            });
//                 $tbl_personnelaR = $tbl_personnelaR->where('selected','=',1)->whereNotNull('1stTrainingSch')
//                          ->whereNotNull('1stTrainingSch_2');
            }else if($type=="E"){
                 $tbl_personnelaR = $tbl_personnelaR->where('selected','=',0)->where('booked','<>','C');
            }else if($type=="F"){
                $tbl_personnelaR = $tbl_personnelaR->where('selected','=',1)->whereNull('2ndTrainingSch');
            }else if($type=="G"){
                $tbl_personnelaR = $tbl_personnelaR->where('selected','=',1)->whereNotNull('2ndTrainingSch');
            }
            $recordsTotal = $tbl_personnelaR->join('office', '' . $session_personnela . '.officecd', '=', 'office.officecd')
                            ->where('office.subdivisioncd','=',$subdivision)
                            ->where('poststat','=', $rowPstatT->post_stat)
                            ->where('forzone','=', $zone)
                            ->where('phase','=', $phase)
                            ->value(DB::raw('Count(personcd)'));
           
            $nestedData1[$rowPstatT->post_stat]=$recordsTotal; 
        }        
        $data1=$nestedData1;       
        array_push($data,$data1);
        date_default_timezone_set('Asia/Calcutta'); 
        $dt = date_create('now')->format('YmdHi');
        $reportName='SubCatgoryReport'.$dt;
        return \Excel::create($reportName, function($excel) use ($data) {
                    $excel->sheet(' ', function($sheet) use ($data) {
                        $sheet->fromArray($data);
                    });
                })->download('xlsx');
    }
    
    public function getFirstSubPoststatusQueryReport(Request $request) {
       $this->validate($request, [ 
            'districtcd' => 'required|alpha_num|min:2|max:2',
            'subdivision' => 'required|alpha_num|min:4|max:4',
            'zone' => 'required|alpha_num|min:4|max:4',
            'poststatus' => 'nullable|alpha_num|min:2|max:2',
            'type' => 'required|alpha|min:1|max:1',
            'phase' => 'required|integer'
            ], [
            'districtcd.required' => 'District is required',
            'districtcd.alpha_num' => 'District code must be an alpha numeric',
            'subdivision.required' => 'Subdivision is required',
            'subdivision.alpha_num' => 'Subdivision must be an alpha numeric',
            'poststatus.alpha_num' => 'Poststatus must be an alpha numeric',
            'zone.required' => 'Zone is required',
            'zone.alpha_num' => 'Zone must be an alpha numeric characters',
             'type.required' => 'Type is required',
            'type.alpha' => 'Type must be an alpha characters',
            'phase.required' => 'Phase is required',  
            'phase.integer' => 'Phase is must be integer'    
           ]);
        $districtcd=$request->districtcd;
        $subdivision=$request->subdivision;
        $poststatus=$request->poststatus;
        $zone=$request->zone;
        $type=$request->type;
        $phase = $request->phase;
        $data = array();
        $session_personnela=session()->get('personnela_ppds');
        $tbl_personnela = new tbl_personnela();
        if($poststatus!="")
        {
           $tbl_personnela = $tbl_personnela->where('poststat','=', $poststatus);
        }
        if($zone!="")
        {
           $tbl_personnela = $tbl_personnela->where('forzone','=', $zone);
        }
        if($phase!="")
        {
           $tbl_personnela = $tbl_personnela->where('phase','=', $phase);
        } 
        if($type=="C"){
             $tbl_personnela = $tbl_personnela->where('selected','=',0)->where('booked','=','C');
        }else if($type=="B"){
             $tbl_personnela = $tbl_personnela->where('selected','=',1)->whereNull('1stTrainingSch')
                      ->whereNull('1stTrainingSch_2');
        }else if($type=="D"){
             $tbl_personnela = $tbl_personnela->where('selected','=',1)->where(function($q) {
                                $q->orwhereNotNull('1stTrainingSch')
                                ->orwhereNotNull('1stTrainingSch_2');
                            });
//             $tbl_personnela = $tbl_personnela->where('selected','=',1)->whereNotNull('1stTrainingSch')
//                      ->whereNotNull('1stTrainingSch_2');
        }else if($type=="E"){
             $tbl_personnela = $tbl_personnela->where('selected','=',0)->where('booked','<>','C');
        }else if($type=="F"){
             $tbl_personnela = $tbl_personnela->where('selected','=',1)->whereNull('2ndTrainingSch');
        }else if($type=="G"){
             $tbl_personnela = $tbl_personnela->where('selected','=',1)->whereNotNull('2ndTrainingSch');
        }
        $recordsTotal = $tbl_personnela->where(DB::raw('substring(personcd,1,4)'),'=', $subdivision) 
                       ->select('personcd','phase','officecd','poststat','no_of_member','gender',
                                'assembly_temp','assembly_off','assembly_perm','fordistrict','forzone',
                                'forassembly','groupid','booked','dcrccd','selected','home_pc','priority','training_gr_cd','1stTrainingSch','1stTrainingSch_2','2ndTrainingSch')->get();
        
        foreach($recordsTotal as $recordsTo){
             $fst='1stTrainingSch';
             $fst_1='1stTrainingSch_2';
             $sst='2ndTrainingSch';
             $nestedData1['personcd']=$recordsTo->personcd; 
             $nestedData1['phase']=$recordsTo->phase; 
             $nestedData1['officecd']=$recordsTo->officecd; 
             $nestedData1['poststat']=$recordsTo->poststat; 
             $nestedData1['no_of_member']=$recordsTo->no_of_member; 
             $nestedData1['gender']=$recordsTo->gender; 
             $nestedData1['assembly_temp']=$recordsTo->assembly_temp; 
             $nestedData1['assembly_off']=$recordsTo->assembly_off;
             $nestedData1['assembly_perm']=$recordsTo->assembly_perm;
             $nestedData1['Fordistrict']=$recordsTo->fordistrict; 
             $nestedData1['Forzone']=$recordsTo->forzone;

             $nestedData1['Forassembly']=$recordsTo->forassembly;
             $nestedData1['Groupid']=$recordsTo->groupid; 
             $nestedData1['Booked']=$recordsTo->booked;
             $nestedData1['Dcrccd']=$recordsTo->dcrccd;
             $nestedData1['Selected']=$recordsTo->selected;
             $nestedData1['Home PC']=$recordsTo->home_pc;
             $nestedData1['Priority']=$recordsTo->priority;
             $nestedData1['1stTraining Group Code']=$recordsTo->training_gr_cd;
             $nestedData1['1stTrainingSch']=$recordsTo->$fst; 
             $nestedData1['1stTrainingSch_2']=$recordsTo->$fst_1; 
             $nestedData1['2ndTrainingSch']=$recordsTo->$sst; 
             $data[]=$nestedData1; 
        }
                 
        //$data1=$nestedData1;       
       // array_push($data,$data1);
        date_default_timezone_set('Asia/Calcutta'); 
        $dt = date_create('now')->format('YmdHi');
        $reportName='SubPostStatPPReport'.$dt;
        return \Excel::create($reportName, function($excel) use ($data) {
                    $excel->sheet(' ', function($sheet) use ($data) {
                        $sheet->fromArray($data);
                    });
                })->download('xlsx');
    }
}
