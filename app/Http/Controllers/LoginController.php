<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\tbl_user;
use App\tbl_user_permission;
use App\tbl_submenu;
use Session;
use Validator;

class LoginController extends Controller {

    public function login(Request $request) {
        $name = $request->username;
        $pass = $request->password;
        $v = Validator::make($request->all(), [
                    'username' => 'required|alpha_num',
                    'password' => "required|regex:/^[a-zA-Z0-9!$#%_@]+$/",
                        ], [
                    'username.required' => 'Username is required',
                    'password.required' => 'Password is Required',
        ]);

        if ($v->fails()) {
            $msg = $v->errors()->all();
            session(['login_error' => $msg]);
            return redirect('/login');
        }

        $user_check1 = DB::select('call Login(?,?)', [$name, '']);
        //return $user_check1;
        if ($user_check1[0]->Count != 0) {
            $user_check2 = DB::select('call Login(?,?)', [$name, md5($pass)]);
            if (count($user_check2) > 0) {
                $login = DB::select('call Login(?,?)', [$name, md5($pass)]);
                foreach ($login as $login_detail) {
                    session(['user_id_ppds' => $login_detail->user_id]);
                    session(['code_ppds' => $login_detail->code]);
                    session(['category_ppds' => $login_detail->category]);
                    session(['districtcd_ppds' => $login_detail->districtcd]);
                    session(['subdivisioncd_ppds' => $login_detail->subdivisioncd]);
                    session(['parliamentcd_ppds' => $login_detail->parliamentcd]);
                    session(['assemblycd_ppds' => $login_detail->assemblycd]);
                    session(['blockmunicd_ppds' => $login_detail->blockmunicd]);
                    session(['zonecd_ppds' => $login_detail->zonecd]);
                    $permission = $this->get_permission($login_detail->code, 'login', '');
                    session(['permission_ppds' => $permission]);
                    $permission_all = $this->get_permission($login_detail->code, 'permission', '');
                    session(['permission_ppds_all' => $permission_all]);
                    session(['personnela_ppds' => 'personnela'.$login_detail->districtcd]);
                }
                return redirect('/dashboard');
            } else {
                $msg = ["Worng password"];
                session(['login_error' => $msg]);
                return redirect('/login');
            }
        } else {
            $msg = ["Username not found"];
            session(['login_error' => $msg]);
            return redirect('/login');
        }
    }

    public function get_permission($uid,$type,$menu_cd) {
//echo $uid;die;
        $ucode= \Session::get('code_ppds');
        if($uid!=''){
            $ucode=$uid;
        }
        $submenu_get = tbl_user_permission::where('user_cd', '=', $ucode);
        if ($type == 'login') {
            $submenu_get = $submenu_get->where('show_status', '=', 1);
        }
        $submenu_get = $submenu_get->pluck('submenu_cd')->all();
        $permission = tbl_user::where('user.code','=',$ucode)->with(array('user_permission_code' => function($query) use($submenu_get,$menu_cd,$uid) {
            if($menu_cd!=''){
               $query->where('menu_cd', '=', $menu_cd);  
            }
            $query->with(array('menu.menu_wise_submenu' => function($query) use($submenu_get,$uid) {
                       $query->whereIn('submenu_cd', $submenu_get)->with(array('user_permission_submenu_cd' => function($query) use($uid) {
                                if($uid!= '') {
                                    $query->where('user_cd', '=', $uid);
                                }
                        }));
                    }));
        }))->get();
        return $permission;
    }

    public static function get_submenu($menu_cd, $user_cd) {
        $result = tbl_user_permission::select('submenu_cd')->where('show_status', '=', 1)->where('menu_cd', '=', $menu_cd)->where('user_cd', '=', $user_cd)->distinct()->with('sub_menu')->get();
        return $result;
    }

    public function logout() {
        Session::flush();
        return redirect('/login');
    }

    public static function get_page_permission($curr_url, $permission_ppds_all) {
        $arr = json_decode($permission_ppds_all, true);
        //print_r($arr);die;
        $length = sizeof($arr[0]['user_permission_code']);
        //echo($length);die;
        for ($i = 0; $i < $length; $i++) {
            $new_arr = $arr[0]['user_permission_code'][$i]['menu'];
            //print_r($new_arr);die;
            if (in_array($curr_url,$new_arr)) { // search value in the array
                $ret = 2;
                break;
            } else {
                $new_arr1 = $arr[0]['user_permission_code'][$i]['menu']['menu_wise_submenu'];
                //print_r($new_arr1);die;
                if (in_array($curr_url, array_column($new_arr1, 'link'))) { // search value in the array
                    $ret = 1;
                     break;
                } else {
                    $ret = 0;
                }
            }
        }
        return $ret;
    }

    public static function get_page_status($url) {
        $data = tbl_submenu::where('link', '=', $url)->with('menu_menu_cd')->get();
        return ($data);
    }

}
