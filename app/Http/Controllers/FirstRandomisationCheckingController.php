<?php

namespace App\Http\Controllers;

use App\tbl_assembly_party;
use App\tbl_assembly_zone;
use App\tbl_first_rand_table;
use App\tbl_personnel;
use App\tbl_personnela;
use App\tbl_reserve;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FirstRandomisationCheckingController extends Controller
{
    public function getsubdivisionwisedata(Request $request)
    {
        // echo "hi";
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            $this->validate($request, [
                'zone' => 'required|alpha_num|min:4|max:4',
                'phase' => 'nullable|digits_between:1,1'
            ], [
                'zone.required' => 'Zone is required',
                'zone.alpha_num' => 'Zone must be an alpha numeric characters',
                'phase.digits_between' => 'Phase must be integer'
            ]);
            try {

                $phase = $request->phase;
                $forZone = $request->zone;
                $requirement = "";

                $totalp1 = 0;
                $totalp2 = 0;
                $totalp3 = 0;
                $totalpr = 0;

                $totalp1assign = 0;
                $totalp2assign = 0;
                $totalp3assign = 0;
                $totalprassign = 0;

                $p1_stat_count = 0;
                $p2_stat_count = 0;
                $p3_stat_count = 0;
                $pr_stat_count = 0;
                $session_personnela = session()->get('personnela_ppds');

                $phasewise_data = tbl_assembly_zone::join('assembly_party', 'assembly_party.assemblycd', '=', 'assembly_zone.assemblycd')
               
                ->join('phase', 'phase.code', '=', 'assembly_zone.phase_id')
                ->select('assembly_zone.*', 'phase.name', 'assembly_party.no_party')->where('zone', $forZone)->where('phase_id', $phase);
            

                $phasewise_data = $phasewise_data->get();
                if(count( $phasewise_data)>0){
                    foreach ($phasewise_data as $key => $phasewise_data) {

                        $reserve_data = tbl_reserve::join('assembly_party', 'assembly_party.assemblycd', '=', 'reserve.forassembly')
                            ->where('forassembly', $phasewise_data->assemblycd)->select('no_or_pc', 'poststat', 'numb')->get();
                        foreach ($reserve_data as $key1 => $reserve_data) {
                            if ($reserve_data->no_or_pc == 'P' && $reserve_data->poststat == 'P1') {
                                $p1_stat_count = $phasewise_data->no_party + round($phasewise_data->no_party *$reserve_data->numb/100,0); 
                              
                            } else if ($reserve_data->no_or_pc == 'N' && $reserve_data->poststat == 'P1') {
                                $p1_stat_count = $phasewise_data->no_party + $reserve_data->numb;
                            }
    
                            if ($reserve_data->no_or_pc == 'P' && $reserve_data->poststat == 'P2') {
                                $p2_stat_count = $phasewise_data->no_party + round($phasewise_data->no_party *$reserve_data->numb/100,0);
                            } else if ($reserve_data->no_or_pc == 'N' && $reserve_data->poststat == 'P2') {
                                $p2_stat_count = $phasewise_data->no_party + $reserve_data->numb;
                            }
    
                            if ($reserve_data->no_or_pc == 'P' && $reserve_data->poststat == 'P3') {
                                $p3_stat_count = $phasewise_data->no_party + round($phasewise_data->no_party *$reserve_data->numb/100,0);
                            } else if ($reserve_data->no_or_pc == 'N' && $reserve_data->poststat == 'P3') {
                                $p3_stat_count = $phasewise_data->no_party + $reserve_data->numb;
                            }
    
                            if ($reserve_data->no_or_pc == 'P' && $reserve_data->poststat == 'PR') {
                                $pr_stat_count = $phasewise_data->no_party + round($phasewise_data->no_party *$reserve_data->numb/100,0);
                            } else if ($reserve_data->no_or_pc == 'N' && $reserve_data->poststat == 'PR') {
                                $pr_stat_count = $phasewise_data->no_party + $reserve_data->numb;
                            }
                        }
                        $totalp1 = $totalp1 + $p1_stat_count;
                        $totalp2 = $totalp2 + $p2_stat_count;
                        $totalp3 = $totalp3 + $p3_stat_count;
                        $totalpr = $totalpr + $pr_stat_count;
                    }
    
                    $personallaassign = tbl_personnela::where('phase', $phase)->where('forzone', $forZone)->select('poststat', DB::raw('Count(' . $session_personnela . '.personcd) As total'))
                        ->groupBy('poststat')->get();
                    
    
                    foreach ($personallaassign as  $personallaassign) {
                        if ($personallaassign->poststat == 'P1') {
                            $totalp1assign = $personallaassign->total;
                        } else if ($personallaassign->poststat == 'P2') {
                            $totalp2assign = $personallaassign->total;
                        } else if ($personallaassign->poststat == 'P3') {
                            $totalp3assign = $personallaassign->total;
                        } else if ($personallaassign->poststat == 'PR') {
                            $totalprassign = $personallaassign->total;
                        }
                    }
                    $requirement .= '<tr>';
                    $requirement .= '<td align="left" style="width: 15%;">&nbsp;'.$phasewise_data->name.'</td>';
    
                    $requirement .= "<td>" . $totalp1 . '</td>';
                    $requirement .= "<td>" . $totalp2 . '</td>';
                    $requirement .= "<td>" . $totalp3 . '</td>';
                    $requirement .= "<td>" . $totalpr . '</td>';
    
    
    
                    $requirement .= "<td>" . $totalp1assign . '</td>';
                    $requirement .= "<td>" . $totalp2assign . '</td>';
                    $requirement .= "<td>" . $totalp3assign . '</td>';
                    $requirement .= "<td>" . $totalprassign . '</td>';
                    $requirement .= '</tr>';
                    $response = array(
                        'requirement' => $requirement,
    
                        'status' => 1
                    );
                }
                else{
                    $requirement='<tr><td colspan="9">No record Found</td></tr>';
                    $response = array(
                        'requirement' => $requirement,
    
                        'status' => 2
                    );
                }
               
               
           
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }

    public function getAssemblyWiserecord(Request $request)
    {
        // echo "hi";
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            $this->validate($request, [
                'zone' => 'required|alpha_num|min:4|max:4',
                'phase' => 'nullable|digits_between:1,1',
                'assembly' => 'nullable|alpha_num|min:3|max:3',
            ], [
                'zone.required' => 'Zone is required',
                'zone.alpha_num' => 'Zone must be an alpha numeric characters',
                'phase.digits_between' => 'Phase must be integer',
                'assembly.alpha_num' => 'Assembly code must be an alpha numeric'
            ]);
            try {
                //dd($request->all());
                $phase = $request->phase;
                $forZone = $request->zone;
                $assembly = $request->assembly;
                $session_personnela = session()->get('personnela_ppds');
                $data = [];

                $assembly_details = tbl_assembly_zone::join('assembly', 'assembly.assemblycd', '=', 'assembly_zone.assemblycd')
                    ->join('assembly_party', 'assembly_party.assemblycd', '=', 'assembly.assemblycd')
                   
                    ->select('assembly.assemblyname', 'assembly_party.assemblycd', 'assembly_party.no_party');

                if ($phase != '') {
                    $assembly_details = $assembly_details->where('assembly_zone.phase_id', $phase);
                }
                if ($assembly != '') {
                    $assembly_details = $assembly_details->where('assembly_zone.assemblycd', $assembly);
                }
                if ($forZone != '') {
                    $assembly_details = $assembly_details->where('assembly_zone.zone', $forZone);
                }
                $assembly_details = $assembly_details->get();
                
                foreach ($assembly_details as $value) {
                    $assembly_code = $value->assemblycd;
                    $no_of_party = $value->no_party;
                    $nestedData['assemblycd'] = $assembly_code;
                    $nestedData['assemblyname'] = $value->assemblyname;
                    $nestedData['subdivision'] = $value->subdivision;
                    $nestedData['phase'] = $value->name;
                    $assembly_wise_reserve = tbl_reserve::join('assembly_zone', 'assembly_zone.assemblycd', '=', 'reserve.forassembly')->select('assembly_zone.phase_id', 'reserve.poststat', 'reserve.numb', 'reserve.no_or_pc')->where('reserve.forassembly', $assembly_code)->groupBy('forassembly', 'poststat')->get();

                    $reserve_arr = [];
                    foreach ($assembly_wise_reserve as $value1) {
                        if ($value1->no_or_pc == 'P') {
                            $numb = round($no_of_party *$value1->numb/100,0); 
                        } else {
                            $numb = $value1->numb;
                        }
                        $sum = $no_of_party + $numb;

                        $numb_arr['sum'] = $sum;
                        $numb_arr['poststat'] = $value1->poststat;
                        $reserve_arr[] = $numb_arr;
                    }


                    $nestedData['reserve_arr'] = $reserve_arr;

                    $ZoneAvailable = tbl_personnela::select('poststat', 'forassembly', DB::raw('Count(' . $session_personnela . '.personcd) As total'))->where('selected', '=', 1)

                        ->where('forassembly', $assembly_code)->groupBy('forassembly', 'poststat');


                    $ZoneAvailable = $ZoneAvailable->get();
                    $zone = [];
                    foreach ($ZoneAvailable as $value2) {
                        $current_arr['total'] = $value2->total;
                        $current_arr['poststat'] = $value2->poststat;
                        $current_arr['forassembly'] = $value2->forassembly;
                        $zone[] = $current_arr;
                    }
                    $nestedData['current_arr'] = $zone;

                    $data[] = $nestedData;
                }
                //   dd($data);

                $response = array(
                    'options' => $data,
                    'status' => 1
                );
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }
    public function getAfterPopulateRecord(Request $request)
    {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        }
        else
        {
            try{
                $session_personnela=session()->get('personnela_ppds');
                $assign_record = tbl_personnela::join('first_training_schedule','first_training_schedule.schedule_code','=',''.$session_personnela.'.1stTrainingSch')
                                                ->leftjoin('first_training_venue','first_training_venue.venue_cd','=','first_training_schedule.tr_venue_cd')
                                                ->select('1stTrainingSch','first_training_venue.venuename')
                                                ->where('selected','=',1)
                                                ->whereNotnull('1stTrainingSch')->groupBy('1stTrainingSch')->orderby('first_training_venue.venuename')->get();
                $first_tr_ass = [];
                foreach ($assign_record as $value) {
                    $record['venuename'] = $value->venuename;
                    $query = tbl_personnela::where('1stTrainingSch',$value['1stTrainingSch'])->select('poststat',DB::raw('Count('.$session_personnela.'.personcd) As first_traning'))
                                            ->groupBy('poststat')->get();
                    $arr1 = [];
                    foreach ($query as $value2) {
                        $record_tr['poststat'] = $value2->poststat;

                        $record_tr['first_traning'] = $value2->first_traning;
                        $arr1[] = $record_tr;
                        
                    }
                    $record['record_tr'] = $arr1;
                    $populate_data = tbl_first_rand_table::where('schedule_code',$value['1stTrainingSch'])->groupBy('poststat')
                                            ->select('poststat',DB::raw('Count(first_rand_table.personcd) As first_traning_populate'))->get();
                    
                        $first_tr_populate = [];
                            foreach ($populate_data as $value1) {
                                $record1['poststat'] = $value1->poststat;
                                $record1['first_traning_populate'] = $value1->first_traning_populate;
                                $first_tr_populate[] = $record1;
                            }
                        $record['first_tr_populate'] = $first_tr_populate;
                        $first_tr_ass[] = $record;
                }
                       
                        $assign_scnd_rcd = tbl_personnela::join('first_training_schedule','first_training_schedule.schedule_code','=',''.$session_personnela.'.1stTrainingSch_2')
                        ->leftjoin('first_training_venue','first_training_venue.venue_cd','=','first_training_schedule.tr_venue_cd')
                        ->select('1stTrainingSch_2','first_training_venue.venuename')
                        ->where('selected','=',1)
                        ->whereNotnull('1stTrainingSch_2')->groupBy('1stTrainingSch_2')->orderby('first_training_venue.venuename')->get();
                        $second_tr_ass = [];
                        foreach ($assign_scnd_rcd as $assign_scnd) {
                            $record2['venuename'] = $assign_scnd->venuename;  
                            $query1 = tbl_personnela::where('1stTrainingSch_2',$assign_scnd['1stTrainingSch_2'])->select('poststat',DB::raw('Count('.$session_personnela.'.personcd) As second_traning'))
                                                    ->groupBy('poststat')->get();
                            $arr2 = [];
                            foreach ($query1 as $assign_scnd1) {
                                $record_tr1['poststat'] = $assign_scnd1->poststat;
                                $record_tr1['second_traning'] = $assign_scnd1->second_traning;
                                $arr2[] = $record_tr1;
                            }
                            $record2['record_tr1'] = $arr2;
                            $populate_data1 = tbl_first_rand_table::where('schedule_code_2',$assign_scnd['1stTrainingSch_2'])->groupBy('poststat')
                                                    ->select('poststat',DB::raw('Count(first_rand_table.personcd) As second_traning_populate'))->get();
                                $second_tr_populate = [];
                                    foreach ($populate_data1 as $value2) {
                                        $record3['poststat'] = $value2->poststat;
                                        $record3['second_traning_populate'] = $value2->second_traning_populate;
                                        $second_tr_populate[] = $record3;
                                    }
                                $record2['second_tr_populate'] = $second_tr_populate;
                                $second_tr_ass[] = $record2;
                        }
                $response = array(
                  'first_tr_ass' => $first_tr_ass,
                  'second_tr_ass' => $second_tr_ass,
                 'status' => 1 
              );
            }catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }

    public function getBeforeRandomisationPPExcel(Request $request)
    {
            try {
              //  dd($request->all());
              $phase = $request->phase;
              $forZone = $request->zone;
            $requirement = "";
            $data = [];

              $totalp1 = 0;
              $totalp2 = 0;
              $totalp3 = 0;
              $totalpr = 0;

              $totalp1assign = 0;
              $totalp2assign = 0;
              $totalp3assign = 0;
              $totalprassign = 0;

              $p1_stat_count = 0;
              $p2_stat_count = 0;
              $p3_stat_count = 0;
              $pr_stat_count = 0;
              $session_personnela = session()->get('personnela_ppds');

              $phasewise_data = tbl_assembly_zone::join('assembly_party', 'assembly_party.assemblycd', '=', 'assembly_zone.assemblycd')
             
              ->join('phase', 'phase.code', '=', 'assembly_zone.phase_id')
              ->select('assembly_zone.*', 'phase.name', 'assembly_party.no_party')->where('zone', $forZone)->where('phase_id', $phase);
          

              $phasewise_data = $phasewise_data->get();
              if(count( $phasewise_data)>0){
                  foreach ($phasewise_data as $key => $phasewise_data) {

                      $reserve_data = tbl_reserve::join('assembly_party', 'assembly_party.assemblycd', '=', 'reserve.forassembly')
                          ->where('forassembly', $phasewise_data->assemblycd)->select('no_or_pc', 'poststat', 'numb')->get();
                      foreach ($reserve_data as $key1 => $reserve_data) {
                          if ($reserve_data->no_or_pc == 'P' && $reserve_data->poststat == 'P1') {
                              $p1_stat_count = $phasewise_data->no_party + round($phasewise_data->no_party *$reserve_data->numb/100,0); 
                            
                          } else if ($reserve_data->no_or_pc == 'N' && $reserve_data->poststat == 'P1') {
                              $p1_stat_count = $phasewise_data->no_party + $reserve_data->numb;
                          }
  
                          if ($reserve_data->no_or_pc == 'P' && $reserve_data->poststat == 'P2') {
                              $p2_stat_count = $phasewise_data->no_party + round($phasewise_data->no_party *$reserve_data->numb/100,0);
                          } else if ($reserve_data->no_or_pc == 'N' && $reserve_data->poststat == 'P2') {
                              $p2_stat_count = $phasewise_data->no_party + $reserve_data->numb;
                          }
  
                          if ($reserve_data->no_or_pc == 'P' && $reserve_data->poststat == 'P3') {
                              $p3_stat_count = $phasewise_data->no_party + round($phasewise_data->no_party *$reserve_data->numb/100,0);
                          } else if ($reserve_data->no_or_pc == 'N' && $reserve_data->poststat == 'P3') {
                              $p3_stat_count = $phasewise_data->no_party + $reserve_data->numb;
                          }
  
                          if ($reserve_data->no_or_pc == 'P' && $reserve_data->poststat == 'PR') {
                              $pr_stat_count = $phasewise_data->no_party + round($phasewise_data->no_party *$reserve_data->numb/100,0);
                          } else if ($reserve_data->no_or_pc == 'N' && $reserve_data->poststat == 'PR') {
                              $pr_stat_count = $phasewise_data->no_party + $reserve_data->numb;
                          }
                      }
                      $totalp1 = $totalp1 + $p1_stat_count;
                      $totalp2 = $totalp2 + $p2_stat_count;
                      $totalp3 = $totalp3 + $p3_stat_count;
                      $totalpr = $totalpr + $pr_stat_count;
                  }
  
                  $personallaassign = tbl_personnela::where('phase', $phase)->where('forzone', $forZone)->select('poststat', DB::raw('Count(' . $session_personnela . '.personcd) As total'))
                      ->groupBy('poststat')->get();
                  
  
                  foreach ($personallaassign as  $personallaassign) {
                      if ($personallaassign->poststat == 'P1') {
                          $totalp1assign = $personallaassign->total;
                      } else if ($personallaassign->poststat == 'P2') {
                          $totalp2assign = $personallaassign->total;
                      } else if ($personallaassign->poststat == 'P3') {
                          $totalp3assign = $personallaassign->total;
                      } else if ($personallaassign->poststat == 'PR') {
                          $totalprassign = $personallaassign->total;
                      }
                  }
                //   $requirement .= '<tr>';

                $nestedData['Phase'] = $phasewise_data->name;
                
                $nestedData['Requirement(P1)'] = $totalp1;
                $nestedData['Requirement(P2)'] = $totalp2;
                $nestedData['Requirement(P3)'] = $totalp3;
                $nestedData['Requirement(PR)'] = $totalpr;

                $nestedData['Swapped(P1)'] = $totalp1assign;
                $nestedData['Swapped(P2)'] = $totalp2assign;
                $nestedData['Swapped(P3)'] = $totalp3assign;
                $nestedData['Swapped(PR)'] = $totalprassign;
                
             

                $data[] = $nestedData;
                // dd($data);
              }
            
              return \Excel::create('BeforeRandomisationPPExcel', function($excel) use ($data) {
                $excel->sheet(' ', function($sheet) use ($data) {
                    $sheet->fromArray($data);
                });
            })->download('xlsx');
                
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }


        public function checkingAssemblyWiseRPTEXcel(Request $request)
        {
            $this->validate($request, [
                'zone' => 'required|alpha_num|min:4|max:4',
                'phase' => 'nullable|digits_between:1,1',
                'assembly' => 'nullable|alpha_num|min:3|max:3',
            ], [
                'zone.required' => 'Zone is required',
                'zone.alpha_num' => 'Zone must be an alpha numeric characters',
                'phase.digits_between' => 'Phase must be integer',
                'assembly.alpha_num' => 'Assembly code must be an alpha numeric'
            ]);
            try {
                //dd($request->all());
                $phase = $request->phase;
                $forZone = $request->zone;
                $assembly = $request->assembly;
                $session_personnela = session()->get('personnela_ppds');
                $data = [];

                $assembly_details = tbl_assembly_zone::join('assembly', 'assembly.assemblycd', '=', 'assembly_zone.assemblycd')
                    ->join('assembly_party', 'assembly_party.assemblycd', '=', 'assembly.assemblycd')
                   
                    ->select('assembly.assemblyname', 'assembly_party.assemblycd', 'assembly_party.no_party');

                if ($phase != '') {
                    $assembly_details = $assembly_details->where('assembly_zone.phase_id', $phase);
                }
                if ($assembly != '') {
                    $assembly_details = $assembly_details->where('assembly_zone.assemblycd', $assembly);
                }
                if ($forZone != '') {
                    $assembly_details = $assembly_details->where('assembly_zone.zone', $forZone);
                }
                $assembly_details = $assembly_details->get();

                foreach ($assembly_details as $value) {
                    $assembly_code = $value->assemblycd;
                    $no_of_party = $value->no_party;
                    $nestedData['Assembly Code'] = $assembly_code;
                    $nestedData['Assembly Name'] = $value->assemblyname;
                    // $nestedData['subdivision'] = $value->subdivision;
                    // $nestedData['phase'] = $value->name;
                    $assembly_wise_reserve = tbl_reserve::join('assembly_zone', 'assembly_zone.assemblycd', '=', 'reserve.forassembly')->select('assembly_zone.phase_id', 'reserve.poststat', 'reserve.numb', 'reserve.no_or_pc')->where('reserve.forassembly', $assembly_code)->groupBy('forassembly', 'poststat')->get();

                    $reserve_arr = [];
                    foreach ($assembly_wise_reserve as $value1) {
                        if ($value1->no_or_pc == 'P') {
                            $numb = round($no_of_party *$value1->numb/100,0); 
                        } else {
                            $numb = $value1->numb;
                        }
                        $sum = $no_of_party + $numb;

                        $numb_arr['sum'] = $sum;
                        $numb_arr['poststat'] = $value1->poststat;
                        $reserve_arr[] = $numb_arr;
                       
                    }
                    $nestedData['Before 1st Randomisation'] = $reserve_arr;

                    //  dd($reserve_arr['poststat']);
                    // if($reserve_arr->poststat == 'P1'){
                    //     $nestedData['Before 1st Randomisation(P1)'] = $reserve_arr['sum'];

                    // }

                    $ZoneAvailable = tbl_personnela::select('poststat', 'forassembly', DB::raw('Count(' . $session_personnela . '.personcd) As total'))->where('selected', '=', 1)

                        ->where('forassembly', $assembly_code)->groupBy('forassembly', 'poststat');


                    $ZoneAvailable = $ZoneAvailable->get();
                    $zone = [];
                    foreach ($ZoneAvailable as $value2) {
                        $current_arr['total'] = $value2->total;
                        $current_arr['poststat'] = $value2->poststat;
                        $current_arr['forassembly'] = $value2->forassembly;
                        $zone[] = $current_arr;
                    }
                    $nestedData['current_arr'] = $zone;

                    $data[] = $nestedData;
                }
                //   dd($data);

                return \Excel::create('assemblyWiseReport', function($excel) use ($data) {
                    $excel->sheet(' ', function($sheet) use ($data) {
                        $sheet->fromArray($data);
                    });
                })->download('xlsx');
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
  
}
