<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\tbl_assembly_zone;
use App\tbl_dcrc_party;
use App\tbl_dcrcmaster;
use App\tbl_assembly_party;
use DB;
use App\tbl_pollingstation;
use App\tbl_personnela;

class DCRCPartyController extends Controller {

    public function getDCVenueDetails(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            $this->validate($request, [
                'forZone' => 'required|alpha_num|min:4|max:4'
                    ], [
                'forZone.required' => 'For Zone is required',
                'forZone.alpha_num' => 'For Zone must be an alpha numeric'
            ]);
            try {
                $forZone = $request->forZone;
                $dcVenue = tbl_dcrcmaster::select(DB::raw("CONCAT('DC: ',dc_venue,', RC: ',rc_venue,'-',DATE_FORMAT(dcrcmaster.dc_date,'%d/%m/%Y'),'-',dc_time) AS dc_venue"), 'dcrcgrp')
                                ->where('zone', '=', $forZone)
                                ->pluck('dc_venue', 'dcrcgrp')->all();
                //print_r($dcVenue);die;
                $response = array(
                    'options' => $dcVenue,
                    'status' => 1);
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }
     
    public function getAssemblyPartyDetailsForDCRC(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            $this->validate($request, [
                'forZone' => 'required|alpha_num|min:4|max:4'
                    ], [
                'forZone.required' => 'For Zone is required',
                'forZone.alpha_num' => 'For Zone must be an alpha numeric'
            ]);
            try {
                $forZone = $request->forZone;
                $filtered = '';
                $filtered = tbl_assembly_zone::where('zone', '=', $forZone)
                        ->join('assembly', 'assembly.assemblycd', '=', 'assembly_zone.assemblycd')
                        ->Rightjoin('assembly_party', 'assembly_party.assemblycd', '=', 'assembly_zone.assemblycd')
                        ->select('assembly_party.assemblycd', 'assembly.assemblyname', 'assembly_party.no_of_member', 'assembly_party.gender', 'assembly_party.no_party')
                        ->get();
                $data = array();
                if (!empty($filtered)) {
                    foreach ($filtered as $post) {
                        $nestedData['assemblycd'] = $post->assemblycd;
                        $nestedData['assemblyname'] = $post->assemblyname;
                        $nestedData['gender'] = $post->gender;
                        $nestedData['no_of_member'] = $post->no_of_member;
                        $nestedData['no_party'] = $post->no_party;

                        $partyReserve = tbl_dcrc_party::join('dcrcmaster','dcrcmaster.dcrcgrp','=','dcrc_party.dcrcgrp')->where('assemblycd', '=', $post->assemblycd)
                                ->where('dcrc_party.number_of_member', '=', $post->no_of_member)
                                ->where('dcrc_party.gender', '=', $post->gender)
                                ->where('dcrc_party.zone', '=', $forZone)
                                ->select(DB::raw("CONCAT('DC: ',dc_venue,', RC: ',rc_venue,'-',DATE_FORMAT(dcrcmaster.dc_date,'%d/%m/%Y'),'-',dc_time) AS dc_venue"))
                                //->select(DB::raw('count(*) as cnt'))
                                ->get();
                        if($partyReserve->count()>0){
                        $nestedData['reserveStatus'] = $partyReserve[0]->dc_venue;
                        }else{
                            $nestedData['reserveStatus'] = '';
                        }
                        $data[] = $nestedData;
                    }
                }
                $response = array(
                    'options' => $data,
                    'status' => 1);
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }

    public function DCRCParty(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
        // echo $request->dcvenue;die;
            $validate_array = ['forZone' => 'required|alpha_num|min:4|max:4',
                'dcvenue' => 'required|alpha_num|min:6|max:6'];
//                'dcdate' => 'required|string',
//                'dctime' => 'required|string'
            for ($x = 0; $x <= $request->row_count; $x++) {
                $validate_array['myCheck' . $x] = 'nullable|alpha_num|min:5|max:5';
            }
            $validate_array1 = ['forZone.required' => 'Zone is required',
                'forZone.alpha_num' => 'Zone must be an alpha numeric characters',
                'dcvenue.required' => 'DC Venue is required',
                'dcvenue.alpha_num' => 'DC Venue must be an alpha numeric characters'
            ];
//                              'dcdate.required' => 'DC Date is required',
//                              'dcdate.string' => 'DC Date must be string',
//                              'dctime.required' => 'DC Time is required',
//                              'dctime.string' => 'DC Time must be string'];
            for ($y = 0; $y <= $request->row_count; $y++) {
                $validate_array1['myCheck' . $y . '.alpha_num'] = 'Assembly code must be an alpha numeric characters';
            }
            $this->validate($request, $validate_array, $validate_array1);
            try {
                $user_code = session()->get('code_ppds');
                $row_count = $request->row_count;
                $forZone = $request->forZone;
                $dcvenue = $request->dcvenue;
//              $dcdate=$request->dcdate;
//              $dctime=$request->dctime;
                $count = 0;
                $status = 0;
                for ($i = 1; $i <= $row_count; $i++) {
                    $asmID = 'myCheck' . $i;
                    if ($request->$asmID != "") {
                        $asmstat_id = $request->$asmID;
                        $assembly = substr($asmstat_id, 0, 3);
                        $party_member = substr($asmstat_id, 3, 1);
                        $gender = substr($asmstat_id, 4, 1);
                        $tbl_dcrc_party = new tbl_dcrc_party();
                        $tbl_dcrc_party->where('assemblycd', $assembly)
                                ->where('number_of_member', $party_member)
                                ->where('gender', $gender)
                                ->delete();
                        $tbl_assembly_party = new tbl_assembly_party();
                        $partyindcrcArray = $tbl_assembly_party->where('assemblycd', '=', $assembly)
                                        ->where('no_of_member', '=', $party_member)
                                        ->where('gender', '=', $gender)
                                        ->select('no_party')->get();
                        $partyindcrc = json_decode($partyindcrcArray);

                        $tbl_dcrc_party->assemblycd = $assembly;
                        $tbl_dcrc_party->number_of_member = $party_member;
                        $tbl_dcrc_party->gender = $gender;
                        $tbl_dcrc_party->partyindcrc = $partyindcrc[0]->no_party;
                        $tbl_dcrc_party->dcrcgrp = $dcvenue;
                        $tbl_dcrc_party->zone = $forZone;
                        $tbl_dcrc_party->usercode = $user_code;
                        //$tbl_dcrc_party->dc_date=$fdcdate;
                        // $tbl_dcrc_party->dc_time=$dctime;

                        $tbl_dcrc_party->save();
                        $count++;
                    }
                }

                $response = array(
                    'options' => $count,
                    'status' => 1);
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }
    public function getPartyNumber(Request $request){
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        }
        else
        {
            $validate_array=[//'forZone' => 'required|alpha_num',
                             'assembly' => 'required|alpha_num|min:3|max:3',
                             'gender' => 'required|alpha|min:1|max:1',
                             'no_of_member'=> 'required|digits_between:1,1'
                            ];

            $validate_array1=[
//                              'forZone.required' => 'For Zone is required',
//                              'forZone.alpha_num' => 'For Zone must be an alpha numeric characters',
                              'gender.required' => 'Gender is required',
                              'gender.alpha' => 'Gender must be an alpha characters',
                              'assembly.required' => 'Assembly is required',
                              'assembly.alpha_num' => 'Assembly must be an alpha numeric characters',
                              'no_of_member.required' => 'No of Member is required',
                              'no_of_member.digits_between' => 'No of Member must be an integer'];

            $this->validate($request,$validate_array,$validate_array1);
            
            try
            {
                //$forzone=$request->forZone;
                $gender=$request->gender;
                $assembly=$request->assembly;
                $no_of_member=$request->no_of_member;
                $count="";
                $max_code=tbl_assembly_party::where('gender','=', $gender)->where('no_of_member','=', $no_of_member);
                                               
                  if($assembly!=''){
                  $max_code=$max_code->where('assemblycd','=', $assembly);
                  }
                  $count=$max_code->value('no_party');
//                 echo $count;
//                 $max_count=json_decode($max_code);
//                 $count=$max_count[0]->cnt;
                  $response = array(
                   'options' => $count,                     
                   'status' => 1 
                );
                
            }
            catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
              $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }
    public function DCRCPartySeparate(Request $request){
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        }
        else
        {
            $this->validate($request, [            
            'districtcd' => 'required|alpha_num|min:2|max:2',
            'zone' => 'required|alpha_num|min:4|max:4',
            'assembly' => 'required|alpha_num|min:3|max:3',
            'gender' => 'required|alpha|min:1|max:1',
            'no_of_member'=> 'required|digits_between:1,1',
            'Party'=> 'required|digits_between:1,4',
            'dcvenue' => 'required|alpha_num|min:6|max:6'
            ], [
            
            'districtcd.required' => 'District is required',
            'districtcd.alpha_num' => 'District must be an alpha numeric',
            'zone.required' => 'For Zone is required',
            'zone.alpha_num' => 'For Zone must be an alpha numeric characters',
            'gender.required' => 'Gender is required',
            'gender.alpha' => 'Gender must be an alpha characters',
            'assembly.required' => 'Assembly is required',
            'assembly.alpha_num' => 'Assembly must be an alpha numeric characters',
            'no_of_member.required' => 'No of Member is required',
            'no_of_member.digits_between' => 'No of Member must be an integer',
            'Party.required' => 'Party is required',
            'Party.digits_between' => 'Party must be an integer',  
            'dcvenue.required' => 'DC Venue is required',
            'dcvenue.alpha_num' => 'DC Venue must be an alpha numeric characters'
            ]);
            try
            {
                $forDist=$request->districtcd;
                $zone=$request->zone;
                $number_of_member = $request->no_of_member;
                $assemblycd = $request->assembly;
                $Party = $request->Party;
                $gender = $request->gender;
                $dcvenue = $request->dcvenue;
                $user_code=session()->get("code_ppds");
                	
                $save_zone = new tbl_dcrc_party();
                $save_zone->number_of_member = $number_of_member;
                $save_zone->zone = $zone;
                $save_zone->assemblycd = $assemblycd;
                $save_zone->partyindcrc = $Party;
                $save_zone->gender = $gender;
                $save_zone->dcrcgrp = $dcvenue;
                $save_zone->usercode = $user_code;
                $save_zone->save();
                $response = array(
                   'options' => $save_zone,
                   'status' => 1);          
            }catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
              $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
      
        }
    }
    ////:::::::Datatable:::::::////
    public function dcrc_party_list_datatable(Request $request) {
        $response = [];
        $perm = null;
        $statusCode = 200;
        $users = array(); //Should be changed #4
        $search_val = array();
         $this->validate($request, [
            'search.*' => 'nullable|regex:/^[A-Za-z0-9\s]+$/i',
            'draw' => 'required|integer',
            'start' => 'required|integer',
            'length' => 'required|integer',
            'zone' => 'nullable|alpha_num|min:4|max:4',
            'dcvenue' => 'nullable|alpha_num|min:6|max:6'
                ], [
            'search.*.regex' => 'Special Charecters not allowed',
            'draw.required' => 'Draw is required',
            'start.integer' => 'Start must be an integer',
            'start.required' => 'Start is required',
            'length.integer' => 'Length must be an integer',
            'length.required' => 'Length is required',
            'zone.alpha_num' => 'Zone code must be an alpha numeric',
            'dcvenue.alpha_num' => 'DC Venue code must be an alpha numeric'     
        ]);
        try {
            $draw = $request->draw;
            $offset = $request->start;
            $length = $request->length;
            $search = $request->search ["value"];
            $order = $request->order;
            //print_r($order);die;
            $forZone = $request->zone;
            $dcvenue = $request->dcvenue;
            $dcrcs_par = tbl_dcrc_party::all();

            //  $categ= \Session::get('category_ppds');
            $dist = \Session::get('districtcd_ppds');
            $filtered = tbl_dcrc_party::join('zone', 'zone.zone', '=', 'dcrc_party.zone')
                    ->join('assembly', 'assembly.assemblycd', '=', 'dcrc_party.assemblycd')
                    ->join('dcrcmaster', 'dcrcmaster.dcrcgrp', '=', 'dcrc_party.dcrcgrp')
                    ->select('dcrc_party.number_of_member', 'dcrc_party.partyindcrc', 'dcrc_party.gender', 'dcrc_party.dcrcgrp', 'zone.zonename', 'assembly.assemblyname', 'dcrc_party.assemblycd', 'dcrc_party.zone', 'dcrcmaster.dc_venue', 'dcrcmaster.dc_date', 'dcrcmaster.dc_time')
                    ->where(\DB::raw('SUBSTRING(dcrc_party.zone,1,2)'), '=', $dist)
                    ->orderBY('dcrc_party.assemblycd')
                    ->where(function($q) use ($search) {
                $q->orwhere('number_of_member', 'like', '%' . $search . '%')
                ->orwhere('partyindcrc', 'like', '%' . $search . '%')
                ->orwhere('zonename', 'like', '%' . $search . '%')
                ->orwhere('assembly.assemblycd', 'like', '%' . $search . '%')
                ->orwhere('assemblyname', 'like', '%' . $search . '%');
            });
            if ($forZone != '') {
                $filtered = $filtered->where('dcrc_party.zone', '=', $forZone);
            }
            if ($dcvenue != '') {
                $filtered = $filtered->where('dcrc_party.dcrcgrp', '=', $dcvenue);
            }
//                 
//                            ->where('category','>=',$categ);
//                    if($categ!=0){
//                           $filtered=$filtered->where('districtcd','=',$dist);
//                    }
            $ordered = $filtered;
            //echo $zone_sub=$ordered[0]->zone;die;
            // $last_two_digit_ofyear = substr($academic_year_text, 2);
            $filtered_count = $filtered->count();
            //echo count ( $order );die;
            for ($i = 0; $i < count($order); $i ++) {
                $ordered = $ordered->orderBy($request->columns [$order [$i] ['column']] ['data'], strtoupper($order [$i] ['dir']));
            }
            $page_displayed = $ordered->offset($offset)->limit($length)->get();
            $data = array();
            if (!empty($page_displayed)) {
                foreach ($page_displayed as $dcrc_pty) {
                    //$cat_arr = config('constants.USER_CATEGORY');

                    $nestedData['dc_venue'] = $dcrc_pty->dc_venue . '-' . date('d/m/Y', strtotime(trim(str_replace('/', '-', $dcrc_pty->dc_date)))) . '-' . $dcrc_pty->dc_time;


                    $nestedData['assemblyname'] = $dcrc_pty->assemblycd . '-' . $dcrc_pty->assemblyname;
                    $nestedData['number_of_member'] = $dcrc_pty->number_of_member;
                    $nestedData['partyindcrc'] = $dcrc_pty->partyindcrc;


//                    if ($dcrc_pty->gender == 'M') {
//                        $nestedData['gender'] = 'Male';
//                    } else {
//                        $nestedData['gender'] = 'Female';
//                    }
                    $nestedData['gender'] = $dcrc_pty->gender;
                    // $nestedData['zonename'] = $dcrc_pty->zonename;
                    $nestedData['assemblycd'] = $dcrc_pty->assemblycd;
                    $nestedData['zone'] = $dcrc_pty->zone;
                    $edit_button = $delete_button = $nestedData['assemblycd'] . '/' . $nestedData['number_of_member'] . '/' . $nestedData['gender'] . '/' . $nestedData['zone'] . '/' . $nestedData['partyindcrc'];

                    $nestedData['action'] = array('e' => $edit_button, 'd' => $delete_button);
                    $data[] = $nestedData;
                }
            }
            $response = array(
                "draw" => $draw,
                "recordsTotal" => $dcrcs_par->count(), //Should be changed #7
                "recordsFiltered" => $filtered_count,
                'dcrc_masters' => $data //Should be changed #8
            );
        } catch (\Exception $e) {
            $response = array(
                'exception' => true,
                'exception_message' => $e->getMessage()
            );
            $statusCode = 400;
        } finally {
            return response()->json($response, $statusCode);
        }
    }

    public function check_for_edit_delete_dcrc_party(Request $request) {
        $statusCode = 200;
        $all_value = $request->data;
        $data_all = explode("/", $all_value);
        $assemblycd = $data_all[0];
        $no_of_member = $data_all[1];
        $gender = $data_all[2];
        $zone = $data_all[3];

//        $check_exist = tbl_pollingstation::where('forzone', '=', $zone)
//                ->where('member', '=', $no_of_member)
//                ->where('forassembly', '=', $assemblycd)
//                ->where('gender', '=', $gender)
//                ->get();
//        $check_exist = tbl_personnela::where('forzone', '=', $zone)
//                ->where('no_of_member', '=', $no_of_member)
//                ->where('forassembly', '=', $assemblycd)
//                ->where('gender', '=', $gender)
//                ->get();
//          $count = $check_exist->count();
        $count = tbl_personnela::where('forzone', '=', $zone)
                ->where('no_of_member', '=', $no_of_member)
                ->where('forassembly', '=', $assemblycd)
                ->where('gender', '=', $gender)
                ->value(DB::raw('count(*)'));
      
        $response = array(
            'count' => $count,
        );
        return response()->json($response, $statusCode);
    }

    public function delete_dcrc_party(Request $request) {
        $statusCode = 200;
        $this->validate($request, [
            'data' => 'required|regex:/^[A-Za-z0-9\s\/\(\)]+$/i|min:14|max:17',
                ], [
            'data.required' => 'Data is required to delete',
            'data.regex' => 'Special Charecters are not allow in Data',
        ]);
        try {
            $all_value = $request->data;
            $data_all = explode("/", $all_value);
            $assemblycd = $data_all[0];
            $no_of_member = $data_all[1];
            $gender = $data_all[2];
            $zone = $data_all[3];
            $no_of_party = $data_all[4];
            $record = tbl_dcrc_party::where('assemblycd', '=', $assemblycd)
                    ->where('number_of_member', '=', $no_of_member)
                    ->where('gender', '=', $gender)
                    ->where('zone', '=', $zone)
                    ->where('partyindcrc', '=', $no_of_party); //Should be changed #27

            if (!empty($record)) {//Should be changed #30
                $record = $record->delete();
            }

            $response = array(
                'record' => $record,
                'status' => 2//Should be changed #32
            );
        } catch (\Exception $e) {
            $response = array(
                'exception' => true,
                'exception_message' => $e->getMessage(),
            );
            $statusCode = 400;
        } finally {
            return response()->json($response, $statusCode);
        }
    }

}
