<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\tbl_user;

class ChangePasswordController extends Controller
{
     public function change_password_create(Request $request) {
        $statusCode = 200;
        $usermaster = null;
        $filename = "";
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in form submit.');
            return response()->json($response, $statusCode);
        }
        $response = [
            'usermaster' => [] //Should be changed #9
        ];
        /*         * ****Validation****** */
        $this->validate($request, [
            'pass' => "required|min:6|max:10|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&_])[A-Za-z\d$@$!%*?&_]{6,}[a-zA-Z0-9!$@#%_]+$/",
            'con_pass' => "required|same:pass|min:6|max:10|regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&_])[A-Za-z\d$@$!%*?&_]{6,}[a-zA-Z0-9!$@#%_]+$/"
                ], [
            'pass.required' => 'Password field is required.',
            'pass.min' => 'Password must be at least 6 characters.',
            'pass.max' => 'Password may not be greater than 10 characters.',
            'pass.regex' => 'Password Atleast Contain Six characters, at least one uppercase letter, one lowercase letter, one number and one special character[!@#$%^&*_-].',
            'con_pass.required' => 'Confirm Password field is required.',
            'con_pass.min' => 'Confirm Password must be at least 6 characters.',
            'con_pass.max' => 'Confirm Password may not be greater than 10 characters.',
            'con_pass.regex' => 'Confirm Password Atleast Contain Six characters, at least one uppercase letter, one lowercase letter, one number and one special character[!@#$%^&*_-].',
            'con_pass.same' => 'Confirm Password does not match with Confirm Password.'
        ]);
        try {
            $password = md5($request->con_pass);
            $user_code = session()->get('code_ppds');
            $usermaster = tbl_user::where('code', '=', $user_code)->update(['password' => $password]);
            if ($usermaster == true) {
                $response = array(
                    'status' => 1 //Should be changed #13
                );
            }
        } catch (\Exception $e) {
            $response = array(
                'exception' => true,
                'exception_message' => $e->getMessage(),
            );
            $statusCode = 400;
        } finally {
            return response()->json($response, $statusCode);
        }
    }
}
