<?php

namespace App\Http\Controllers;

use App\tbl_first_training_schedule;
use App\tbl_first_training_venue;
use App\tbl_personnela;
use App\tbl_training_type;
use Illuminate\Http\Request;

class FirstTrainingAIController extends Controller
{
    public function getData()
    {
        //$training_datetime= $this->getTrainingDateTime();
        $training_type = $this->getTrainingTypeData();
        return view('first_training_venueactiveinactive', compact('training_type'));
    }
    public function getTrainingTypeData()
    {
        $desc = tbl_training_type::pluck('training_desc', 'training_code')->all();
        $data = array('desc' => $desc);
        return $data;
    }
    public function getfirstTrainingActiveInactive(Request $request)
    {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            $this->validate($request, [
                'subdivision' => 'nullable|alpha_num|min:4|max:4',
                'zone' => 'nullable|alpha_num|min:4|max:4'
            ], [

                'subdivision.alpha_num' => 'Subdivision must be an alpha numeric characters',
                'zone.required' => 'Zone is required',
                'zone.alpha_num' => 'Zone must be an alpha numeric characters'
            ]);
            $subdivision = $request->subdivision;

            try {
                $filtered = "";
                $filtered = tbl_first_training_venue::join('subdivision', 'subdivision.subdivisioncd', '=', 'first_training_venue.subdivision')
                    ->join('block_muni', 'block_muni.blockminicd', '=', 'first_training_venue.blockmunicd')
                    ->join('assembly', 'assembly.assemblycd', '=', 'first_training_venue.assemblycd')

                    ->select(
                        'first_training_venue.venue_cd',
                        'first_training_venue.status',
                        'subdivision.subdivision',
                        'block_muni.blockmuni',
                        'assembly.assemblyname',
                        'assembly.assemblycd',
                        'first_training_venue.venuename',
                        'first_training_venue.maximumcapacity'
                    )
                    ->orderBy('first_training_venue.venue_cd');
                if (!empty($subdivision)) {
                    $filtered = $filtered->where('first_training_venue.subdivision', $subdivision);
                }
                $html="";
                $filtered = $filtered->get();

                 $html .='<input type="hidden" id="row_count" name="row_count" value="'.count( $filtered).'"> ';
                $html .= "<table class='table table-bordered table-striped' width='100%' style='border-top: 2px solid #4cae4c;'>";
                $html .= ' <tr style="background-color: #f5f8fa"><th align="left">&nbsp;<input type="checkbox" id="myCheckall" name="myCheckall"  value="1"  class="myCheckall">&nbsp;</th><th align="left">&nbsp;Subdivision</th><th>&nbsp;Block </th><th>&nbsp;Assembly </th><th>&nbsp;Venue Name </th><th>&nbsp;Status </th></tr>';
     $firstTrainingAllocContro= new FirstTrainingAllocationController();
     $no_of_PR=0;
     $no_of_P1=0;
     $no_of_P2=0;
     $no_of_P3=0;
                 foreach ($filtered as $key=> $val)
                 {
                    $scheduleArr=array();
                       $c = $key + 1;
                        $html .= '<tr><td align="left" width="5%">&nbsp;';
//                         $scheduleCode=tbl_first_training_schedule::where('tr_venue_cd',$val->venue_cd)->select('schedule_code',
//                         'no_of_PR','no_of_P1','no_of_P2','no_of_P3')->get();
//                         foreach($scheduleCode as $scheduleCodeVal){
//                             array_push($scheduleArr, $scheduleCodeVal->schedule_code);
//                             $no_of_PR += $scheduleCodeVal->no_of_PR;
//                             $no_of_P1 += $scheduleCodeVal->no_of_P1;
//                             $no_of_P2 += $scheduleCodeVal->no_of_P2;
//                             $no_of_P3 += $scheduleCodeVal->no_of_P3;
                           
//                         }
//                         $total=$no_of_PR+$no_of_P1+$no_of_P2 + $no_of_P3;
//                         //echo $total;die;
//                         $getTotalTrainingAllocated=tbl_personnela::wherein('1stTrainingSch',  $scheduleArr)->count();
// echo $getTotalTrainingAllocated;die;
                      
//                         if( $getTotalTrainingAllocated>=$total){
//                             $disabled='disabled';
//                             $flag=1;
//                         }
//                         else{
//                             $disabled='';
//                             $flag=0;
//                         }
//                      //   $firstTrainingAllocContro->get_used_postat_first_training();
//                      if( $flag==1){
//                         $html .='';                             

//                      }
//                      else{
//                         $html .='<input type="checkbox" '. $disabled.' id="myCheck' .   $c . '" name="myCheck' .   $c . '"  value="'. $val->venue_cd.'"  class="submenu" >'; 
//                      }
$disabled='';
                     $html .='<input type="checkbox" '. $disabled.' id="myCheck' .   $c . '" name="myCheck' .   $c . '"  value="'. $val->venue_cd.'"  class="submenu" >'; 
                        $html .='</td><td align="left" width="20%">&nbsp;' .  $val->subdivision . '</td>';
                        $html .='</td><td align="left" width="20%">&nbsp;' . $val->blockmuni . '</td>';
                        $html .='</td><td align="left" width="20%">&nbsp;' . $val->assemblycd .' - '. $val->assemblyname . '</td>';
                        $html .='<td align="left" width="20%">';
                        $html .='&nbsp;' . $val->venuename . '</td>';
                        
                        $html .='<td align="left" width="15%">';
                      
                            if($val->status==0)
                            {
                              $html .='<button type="button" class="btn btn-danger btn-sm status-button"  id="'.$val->venue_cd.'"><i class="fa fa-toggle-off" aria-hidden="true"></i></button></td>';
                            }
                            else{
                                $html .='<button type="button" class="btn btn-primary btn-sm status-button" id="'.$val->venue_cd.'"><i class="fa fa-toggle-on" aria-hidden="true"></i></button></td>';
                             
                            }
                      
                        
                        
                      
                        $html .='</tr>'; // return empty
                }
                $html .= "</table>";
                $response = array(
                    'options' => $html,
                    'status' => 1
                );
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }

    public function changeFirstTrainingStatus(Request $request)
    {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            //     $validate_array=['zone' => 'required|alpha_num|min:4|max:4',
            //     'phase' => 'required|integer'
            // ];

            for ($x = 0; $x <= $request->row_count; $x++) {
                $validate_array['myCheck' . $x] = 'nullable|alpha_num|min:6|max:6';
            }
            $validate_array1 = [ //'zone.required' => 'Zone is required',
                //  'zone.alpha_num' => 'Zone must be an alpha numeric characters',
                // 'phase.required'=>'Phase is required',
                //'phase.integer'=>'Invalid phase select'
            ];
            for ($y = 0; $y <= $request->row_count; $y++) {
                $validate_array1['myCheck' . $y . '.alpha_num'] = 'Venue must be an integer';
            }
            $this->validate($request, $validate_array, $validate_array1);

            try {
                $row_count = $request->row_count;

                $user_code = session()->get('code_ppds');
                $changeCount = 0;
                $remainCount = 0;
                $count = 0;
                $remainMsg = "";
                for ($i = 1; $i <= $row_count; $i++) {
                    $count++;
                    $venuecd = 'myCheck' . $i;
                    if ($request->$venuecd != "") {
                        $venuecd_code = $request->$venuecd;

                        $status = $request->status;

                        $updateStatus = tbl_first_training_venue::where('venue_cd', $venuecd_code)->update([
                            'status' => $status
                        ]);
                        if ($updateStatus == 1) {
                            $changeCount++;
                        } else {
                            $remainCount++;
                        }
                    }
                }

                if ($remainCount > 0) {
                    $remainMsg = $remainCount . ' Venue(s) unchanged.';
                }
                $response = array(
                    'options' => $count,
                    'status' => 1,
                    'msg' => $changeCount . ' Venue(s) status changed succesfully. <br>' . $remainMsg,
                );
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }


    /*********************************Training Active Inactive ************************/

    public function getTrainingTypeDetails()
    {
       
     
        return view('training_active_inactive');
    }
  
    public function getTrainingTypeActiveInactive(Request $request)
    {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            // $this->validate($request, [
            //     'subdivision' => 'nullable|alpha_num|min:4|max:4',
            //     'zone' => 'nullable|alpha_num|min:4|max:4'
            // ], [

            //     'subdivision.alpha_num' => 'Subdivision must be an alpha numeric characters',
            //     'zone.required' => 'Zone is required',
            //     'zone.alpha_num' => 'Zone must be an alpha numeric characters'
            // ]);
            // $subdivision = $request->subdivision;

            try {
                $filtered = "";
                $filtered = tbl_training_type::select('training_type.*')
                    ->orderBy('training_type.training_code');
            
                $html="";
                $filtered = $filtered->get();

                 $html .='<input type="hidden" id="row_count" name="row_count" value="'.count( $filtered).'"> ';
                $html .= "<table class='table table-bordered table-striped' width='100%' style='border-top: 2px solid #4cae4c;'>";
                $html .= ' <tr style="background-color: #f5f8fa"><th align="left">&nbsp;<input type="checkbox" id="myCheckall" name="myCheckall"  value="1"  class="myCheckall">&nbsp;</th><th align="left">&nbsp;Training Code</th><th>&nbsp;Traning Name </th><th>&nbsp;Status </th></tr>';
  
                 foreach ($filtered as $key=> $val)
                 {
                    $c = $key + 1;
                     //   $firstTrainingAllocContro->get_used_postat_first_training();
                        $html .='<td><input type="checkbox"  id="myCheck' .   $c . '" name="myCheck' .   $c . '"  value="'. $val->training_code.'"  class="submenu" ></td>';                             
                        $html .='<td align="left" width="20%">&nbsp;' .  $val->training_code . '</td>';
                        $html .='<td align="left" width="20%">&nbsp;' . $val->training_desc . '</td>';
                      
                        
                        
                        $html .='<td align="left" width="15%">';
                        if($val->status==0)
                        {
                          $html .='<button type="button" class="btn btn-danger btn-sm status-button" id="'.$val->training_code.'"><i class="fa fa-toggle-off" aria-hidden="true"></i></button></td>';
                        }
                        else{
                            $html .='<button type="button" class="btn btn-primary btn-sm status-button" id="'.$val->training_code.'"><i class="fa fa-toggle-on" aria-hidden="true"></i></button></td>';
                         
                        }
                        
                      
                        $html .='</tr>'; // return empty
                }
                $html .= "</table>";
                $response = array(
                    'options' => $html,
                    'status' => 1
                );
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }

    public function changeTrainingTypeStatus(Request $request)
    {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            //     $validate_array=['zone' => 'required|alpha_num|min:4|max:4',
            //     'phase' => 'required|integer'
            // ];

            for ($x = 0; $x <= $request->row_count; $x++) {
                $validate_array['myCheck' . $x] = 'nullable|alpha_num|min:2|max:2';
            }
            $validate_array1 = [ //'zone.required' => 'Zone is required',
                //  'zone.alpha_num' => 'Zone must be an alpha numeric characters',
                // 'phase.required'=>'Phase is required',
                //'phase.integer'=>'Invalid phase select'
            ];
            for ($y = 0; $y <= $request->row_count; $y++) {
                $validate_array1['myCheck' . $y . '.alpha_num'] = 'Venue must be an integer';
            }
            $this->validate($request, $validate_array, $validate_array1);

            try {
                $row_count = $request->row_count;

                $user_code = session()->get('code_ppds');
                $changeCount = 0;
                $remainCount = 0;
                $count = 0;
                $remainMsg = "";
                for ($i = 1; $i <= $row_count; $i++) {
                    $count++;
                    $trainingtypecd = 'myCheck' . $i;
                    if ($request->$trainingtypecd != "") {
                        $training_code = $request->$trainingtypecd;

                        $status = $request->status;

                        $updateStatus = tbl_training_type::where('training_code', $training_code)->update([
                            'status' => $status
                        ]);
                        if ($updateStatus == 1) {
                            $changeCount++;
                        } else {
                            $remainCount++;
                        }
                    }
                }

                if ($remainCount > 0) {
                    $remainMsg = $remainCount . ' Training Type(s) unchanged.';
                }
                $response = array(
                    'options' => $count,
                    'status' => 1,
                    'msg' => $changeCount . ' Training Type(s) status changed succesfully. <br>' . $remainMsg,
                );
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }
}
