<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\FirstTrainingAllocationController;
use App\tbl_first_training_schedule;
use App\tbl_training_type;
use App\tbl_personnela;
use DB;
use App\tbl_first_training_venue;

class FirstTrainingAssignController extends Controller
{
   private $firstTrainingAllocation;
   public function getTrData(FirstTrainingAllocationController $firstTrainingAllocation)
   {
      $this->firstTrainingAllocation = $firstTrainingAllocation;
      $training_type = $this->firstTrainingAllocation->getTrainingGroupData();
      return view('first_training_assign', compact('training_type'));
   }
   public function savefirstTrAssign(Request $request)
   {
      $statusCode = 200;
      if (!$request->ajax()) {
         $statusCode = 400;
         $response = array('error' => 'Error occured in Ajax Call.');
         return response()->json($response, $statusCode);
      } else {
         $validate_array = [
            'zone' => 'required|alpha_num|min:4|max:4',
            'subdivision' => 'required|alpha_num|min:4|max:4',
            'trainingtype' => 'required|digits_between:1,1'
         ];

         $validate_array1 = [
            'zone.required' => 'Zone is required',
            'zone.alpha_num' => 'Zone must be an alpha numeric characters',
            'subdivision.required' => 'Subdivision is required',
            'subdivision.alpha_num' => 'Subdivision must be an alpha numeric characters',
            'trainingtype.required' => 'Training Head is required',
            'trainingtype.digits_between' => 'Training Head must be numeric'
         ];

         $this->validate($request, $validate_array, $validate_array1);

         try {
            $subdivision = $request->subdivision;
            $trainingHead = $request->trainingtype;
            $phase = $request->phase;
            $zone = $request->zone;
            $count = 0;
            $totalAssign = 0;
            $totalAssignPP = 0;
            $totalAssignPP_2 = 0;
            $totalTRPP = 0;
            $totalPostAssignPP = 0;
            $totalPostAssignPP_2 = 0;
            $filter = "";

            $trPGrp = tbl_personnela::where('selected', '=', 1)
               ->where('forzone', '=', $zone)
               ->where('phase', '=', $phase)
               ->where('training_gr_cd', '=', $trainingHead)
               ->groupBy('training_gr_cd')
               ->value('training_gr_cd');
            if ($trPGrp == null) {
               tbl_personnela::where('selected', '=', 1)
                  ->where('forzone', '=', $zone)
                  ->where('phase', '=', $phase)
                  ->whereNull('training_gr_cd')
                  ->update(['training_gr_cd' => $trainingHead]);
            }

            $totalTRPP = tbl_personnela::where('selected', '=', 1)
               ->where('forzone', '=', $zone)
               ->where('phase', '=', $phase)
               ->where('training_gr_cd', '=', $trainingHead)
               ->value(DB::raw('count(personcd)'));

            $trTypeArray = tbl_training_type::where('training_group_cd', '=', $trainingHead)
               ->orderBy('training_code')->pluck('training_code')->toArray();
            $totalAssignPP = tbl_personnela::where('selected', '=', 1)
               ->where('forzone', '=', $zone)
               ->where('phase', '=', $phase)
               ->where('training_gr_cd', '=', $trainingHead)
               ->whereNotNull('1stTrainingSch')
               ->value(DB::raw('count(personcd)'));
            $totalAssignPP_2 = tbl_personnela::where('selected', '=', 1)
               ->where('forzone', '=', $zone)
               ->where('phase', '=', $phase)
               ->whereNotNull('1stTrainingSch_2')
               ->where('training_gr_cd', '=', $trainingHead)
               ->value(DB::raw('count(personcd)'));
            $dataAr = array();
            foreach ($trTypeArray as $trTypeArray1) {
               $filter = tbl_first_training_schedule::where('forzone', '=', $zone)
                  //                        ->where('training_type','=', $trainingtype)
                  ->where('subdivisioncd', '=', $subdivision)
                  ->where('training_type', $trTypeArray1)
                  ->where('phase_id', $phase)
                  ->orderBy('training_type')
                  ->orderBy('schedule_code')
                  ->select('schedule_code', 'training_type', 'no_of_PR', 'no_of_P1', 'no_of_P2', 'no_of_P3', 'no_of_PA', 'no_of_PB', 'choice_type', 'choice_block', 'choice_subdiv', 'tr_venue_cd')->get();
               $dataAr[] = $filter;
            }
            //   print_r(json_encode( $dataAr));die;
            $session_personnela = session()->get('personnela_ppds');
            if (count($dataAr[0]) > 0) {
               foreach ($dataAr[0] as $subTe) {
                  if ($subTe->choice_type === 'O') {
                     $assembly_column = '.assembly_off';
                  } else if ($subTe->choice_type === 'H') {
                     $assembly_column = '.assembly_temp';
                  }
                  //    echo  $assembly_column; echo $subTe->tr_venue_cd;die;
                  $getAssembly = tbl_first_training_venue::where('venue_cd', $subTe->tr_venue_cd)->value('assemblycd');
                  //                    $assignPP = 0;
                  //                   $assignPR = $assignP1 = $assignP2 = $assignP3 = $assignPA = $assignPB = 0;
                  $tbl_personnelaDE = new tbl_personnela;
                  $tbl_personnelaDE->where('1stTrainingSch', '=', $subTe->schedule_code)
                     ->where('phase', '=', $phase)
                     ->update(['1stTrainingSch' => null]);
                  if ($subTe->no_of_PR != null) {
                     $tbl_personnelaPR = new tbl_personnela;
                     if ($subTe->choice_type === 'B') {
                        $tbl_personnelaPR1 = $tbl_personnelaPR->join('office', 'office.officecd', '=', $session_personnela . ".officecd")->where('office.blockormuni_cd', '=', $subTe->choice_block)->pluck('personcd')->ToArray();


                        $tbl_personnelaPR->where('selected', '=', 1)
                           ->where('forzone', '=', $zone)
                           ->where('phase', '=', $phase)
                           ->whereIn('personcd', $tbl_personnelaPR1)
                           ->where('poststat', '=', 'PR')
                           ->where('training_gr_cd', '=', $trainingHead)
                           ->whereNull('1stTrainingSch')
                           ->limit($subTe->no_of_PR)
                           ->update(['1stTrainingSch' => $subTe->schedule_code]);
                        //die;
                     } else if ($subTe->choice_type === 'O' || $subTe->choice_type === 'H') {
                        $tbl_personnelaPR1 = $tbl_personnelaPR->where($session_personnela . $assembly_column, '=', $getAssembly)
                           //   ->join('office', 'office.officecd', '=', $session_personnela . ".officecd")
                           ->pluck('personcd')->ToArray();
                        // echo json_encode($tbl_personnelaPR1);die;

                        $tbl_personnelaPR->where('selected', '=', 1)
                           ->where('forzone', '=', $zone)
                           ->where('phase', '=', $phase)

                           ->whereIn('personcd', $tbl_personnelaPR1)
                           ->where('poststat', '=', 'PR')
                           ->where('training_gr_cd', '=', $trainingHead)
                           ->whereNull('1stTrainingSch')
                           ->limit($subTe->no_of_PR)
                           ->update(['1stTrainingSch' => $subTe->schedule_code]);
                     } else {
                        $tbl_personnelaPR->where('selected', '=', 1)
                           ->where('forzone', '=', $zone)
                           ->where('phase', '=', $phase)
                           ->where(DB::raw('SUBSTRING(personcd,1,4)'), '=', $subTe->choice_subdiv)
                           ->where('poststat', '=', 'PR')
                           ->where('training_gr_cd', '=', $trainingHead)
                           ->whereNull('1stTrainingSch')
                           ->limit($subTe->no_of_PR)
                           ->update(['1stTrainingSch' => $subTe->schedule_code]);
                     }
                     //$assignPR = $subTe->no_of_PR;

                  }
                  if ($subTe->no_of_P1 != null) {
                     $tbl_personnelaP1 = new tbl_personnela;
                     if ($subTe->choice_type === 'B') {
                        $tbl_personnelaP11 = $tbl_personnelaP1->join('office', 'office.officecd', '=', $session_personnela . ".officecd")->where('office.blockormuni_cd', '=', $subTe->choice_block)->pluck('personcd')->ToArray();

                        $tbl_personnelaP1->where('selected', '=', 1)
                           ->where('forzone', '=', $zone)
                           ->where('phase', '=', $phase)
                           ->whereIn('personcd', $tbl_personnelaP11)
                           ->where('poststat', '=', 'P1')
                           ->where('training_gr_cd', '=', $trainingHead)
                           ->whereNull('1stTrainingSch')
                           ->limit($subTe->no_of_P1)
                           ->update(['1stTrainingSch' => $subTe->schedule_code]);
                     } else if ($subTe->choice_type === 'O' || $subTe->choice_type === 'H') {
                        $tbl_personnelaP11 = $tbl_personnelaP1
                           ->where($session_personnela . $assembly_column, '=', $getAssembly)
                           //->join('office', 'office.officecd', '=', $session_personnela . ".officecd")
                           ->pluck('personcd')->ToArray();


                        $tbl_personnelaP1->where('selected', '=', 1)
                           ->where('forzone', '=', $zone)
                           ->where('phase', '=', $phase)

                           ->whereIn('personcd', $tbl_personnelaP11)
                           ->where('poststat', '=', 'P1')
                           ->where('training_gr_cd', '=', $trainingHead)
                           ->whereNull('1stTrainingSch')
                           ->limit($subTe->no_of_P1)
                           ->update(['1stTrainingSch' => $subTe->schedule_code]);
                     } else {
                        $tbl_personnelaP1->where('selected', '=', 1)
                           ->where('forzone', '=', $zone)
                           ->where('phase', '=', $phase)
                           ->where(DB::raw('SUBSTRING(personcd,1,4)'), '=', $subTe->choice_subdiv)
                           ->where('poststat', '=', 'P1')
                           ->where('training_gr_cd', '=', $trainingHead)
                           ->whereNull('1stTrainingSch')
                           ->limit($subTe->no_of_P1)
                           ->update(['1stTrainingSch' => $subTe->schedule_code]);
                     }
                     // $assignP1 = $subTe->no_of_P1;
                  }
                  if ($subTe->no_of_P2 != null) {
                     $tbl_personnelaP2 = new tbl_personnela;
                     if ($subTe->choice_type === 'B') {
                        $tbl_personnelaP21 = $tbl_personnelaP2->join('office', 'office.officecd', '=', $session_personnela . ".officecd")->where('office.blockormuni_cd', '=', $subTe->choice_block)->pluck('personcd')->ToArray();
                        $tbl_personnelaP2->where('selected', '=', 1)
                           ->where('forzone', '=', $zone)
                           ->where('phase', '=', $phase)
                           ->whereIn('personcd', $tbl_personnelaP21)
                           ->where('poststat', '=', 'P2')
                           ->where('training_gr_cd', '=', $trainingHead)
                           ->whereNull('1stTrainingSch')
                           ->limit($subTe->no_of_P2)
                           ->update(['1stTrainingSch' => $subTe->schedule_code]);
                     } else if ($subTe->choice_type === 'O' || $subTe->choice_type === 'H') {
                        $tbl_personnelaP21 = $tbl_personnelaP2
                           ->where($session_personnela . $assembly_column, '=', $getAssembly)
                           //->join('office', 'office.officecd', '=', $session_personnela . ".officecd")
                           ->pluck('personcd')->ToArray();


                        $tbl_personnelaP2->where('selected', '=', 1)
                           ->where('forzone', '=', $zone)
                           ->where('phase', '=', $phase)
                           // ->where($session_personnela .$assembly_column, '=', $getAssembly)
                           ->whereIn('personcd', $tbl_personnelaP21)
                           ->where('poststat', '=', 'P2')
                           ->where('training_gr_cd', '=', $trainingHead)
                           ->whereNull('1stTrainingSch')
                           ->limit($subTe->no_of_P2)
                           ->update(['1stTrainingSch' => $subTe->schedule_code]);
                     } else {
                        $tbl_personnelaP2->where('selected', '=', 1)
                           ->where('forzone', '=', $zone)
                           ->where('phase', '=', $phase)
                           ->where(DB::raw('SUBSTRING(personcd,1,4)'), '=', $subTe->choice_subdiv)
                           ->where('poststat', '=', 'P2')
                           ->where('training_gr_cd', '=', $trainingHead)
                           ->whereNull('1stTrainingSch')
                           ->limit($subTe->no_of_P2)
                           ->update(['1stTrainingSch' => $subTe->schedule_code]);
                     }
                     //$assignP2 = $subTe->no_of_P2;
                  }
                  if ($subTe->no_of_P3 != null) {
                     $tbl_personnelaP3 = new tbl_personnela;
                     if ($subTe->choice_type === 'B') {
                        $tbl_personnelaP31 = $tbl_personnelaP3->join('office', 'office.officecd', '=', $session_personnela . ".officecd")->where('office.blockormuni_cd', '=', $subTe->choice_block)->pluck('personcd')->ToArray();
                        $tbl_personnelaP3->where('selected', '=', 1)
                           ->where('forzone', '=', $zone)
                           ->where('phase', '=', $phase)
                           ->whereIn('personcd', $tbl_personnelaP31)
                           ->where('poststat', '=', 'P3')
                           ->where('training_gr_cd', '=', $trainingHead)
                           ->whereNull('1stTrainingSch')
                           ->limit($subTe->no_of_P3)
                           ->update(['1stTrainingSch' => $subTe->schedule_code]);
                     } else if ($subTe->choice_type === 'O' || $subTe->choice_type === 'H') {
                        $tbl_personnelaP31 = $tbl_personnelaP3
                           //->join('office', 'office.officecd', '=', $session_personnela . ".officecd")
                           ->where($session_personnela . $assembly_column, '=', $getAssembly)
                           ->pluck('personcd')->ToArray();


                        $tbl_personnelaP3->where('selected', '=', 1)
                           ->where('forzone', '=', $zone)
                           ->where('phase', '=', $phase)
                           // ->where($session_personnela .$assembly_column, '=', $getAssembly)
                           ->whereIn('personcd', $tbl_personnelaP31)
                           ->where('poststat', '=', 'P3')
                           ->where('training_gr_cd', '=', $trainingHead)
                           ->whereNull('1stTrainingSch')
                           ->limit($subTe->no_of_P3)
                           ->update(['1stTrainingSch' => $subTe->schedule_code]);
                     } else {
                        $tbl_personnelaP3->where('selected', '=', 1)
                           ->where('forzone', '=', $zone)
                           ->where('phase', '=', $phase)
                           ->where(DB::raw('SUBSTRING(personcd,1,4)'), '=', $subTe->choice_subdiv)
                           ->where('poststat', '=', 'P3')
                           ->where('training_gr_cd', '=', $trainingHead)
                           ->whereNull('1stTrainingSch')
                           ->limit($subTe->no_of_P3)
                           ->update(['1stTrainingSch' => $subTe->schedule_code]);
                     }
                     //$assignP3 = $subTe->no_of_P3;
                  }
                  if ($subTe->no_of_PA != null) {
                     $tbl_personnelaPA = new tbl_personnela;
                     if ($subTe->choice_type === 'B') {
                        $tbl_personnelaPA1 = $tbl_personnelaPA->join('office', 'office.officecd', '=', $session_personnela . ".officecd")->where('office.blockormuni_cd', '=', $subTe->choice_block)->pluck('personcd')->ToArray();
                        $tbl_personnelaPA->where('selected', '=', 1)
                           ->where('forzone', '=', $zone)
                           ->where('phase', '=', $phase)
                           ->whereIn('personcd', $tbl_personnelaPA1)
                           ->where('poststat', '=', 'PA')
                           ->where('training_gr_cd', '=', $trainingHead)
                           ->whereNull('1stTrainingSch')
                           ->limit($subTe->no_of_PA)
                           ->update(['1stTrainingSch' => $subTe->schedule_code]);
                     } else if ($subTe->choice_type === 'O' || $subTe->choice_type === 'H') {
                        $tbl_personnelaPA1 = $tbl_personnelaPA
                           //->join('office', 'office.officecd', '=', $session_personnela . ".officecd")
                           ->where($session_personnela . $assembly_column, '=', $getAssembly)
                           ->pluck('personcd')->ToArray();


                        $tbl_personnelaPA->where('selected', '=', 1)
                           ->where('forzone', '=', $zone)
                           ->where('phase', '=', $phase)
                           // ->where($session_personnela .$assembly_column, '=', $getAssembly)
                           ->whereIn('personcd', $tbl_personnelaPA1)
                           ->where('poststat', '=', 'PA')
                           ->where('training_gr_cd', '=', $trainingHead)
                           ->whereNull('1stTrainingSch')
                           ->limit($subTe->no_of_PA)
                           ->update(['1stTrainingSch' => $subTe->schedule_code]);
                     } else {
                        $tbl_personnelaPA->where('selected', '=', 1)
                           ->where('forzone', '=', $zone)
                           ->where('phase', '=', $phase)
                           ->where(DB::raw('SUBSTRING(personcd,1,4)'), '=', $subTe->choice_subdiv)
                           ->where('poststat', '=', 'PA')
                           ->where('training_gr_cd', '=', $trainingHead)
                           ->whereNull('1stTrainingSch')
                           ->limit($subTe->no_of_PA)
                           ->update(['1stTrainingSch' => $subTe->schedule_code]);
                     }
                     //$assignPA = $subTe->no_of_PA;
                  }
                  if ($subTe->no_of_PB != null) {
                     $tbl_personnelaPB = new tbl_personnela;
                     if ($subTe->choice_type === 'B') {
                        $tbl_personnelaPB1 = $tbl_personnelaPB->join('office', 'office.officecd', '=', $session_personnela . ".officecd")->where('office.blockormuni_cd', '=', $subTe->choice_block)->pluck('personcd')->ToArray();
                        $tbl_personnelaPB->where('selected', '=', 1)
                           ->where('forzone', '=', $zone)
                           ->where('phase', '=', $phase)
                           ->whereIn('personcd', $tbl_personnelaPB1)
                           ->where('poststat', '=', 'PB')
                           ->where('training_gr_cd', '=', $trainingHead)
                           ->whereNull('1stTrainingSch')
                           ->limit($subTe->no_of_PB)
                           ->update(['1stTrainingSch' => $subTe->schedule_code]);
                     } else if ($subTe->choice_type === 'O' || $subTe->choice_type === 'H') {
                        $tbl_personnelaPB1 = $tbl_personnelaPB
                           ->where($session_personnela . $assembly_column, '=', $getAssembly)
                           //->join('office', 'office.officecd', '=', $session_personnela . ".officecd")
                           ->pluck('personcd')->ToArray();


                        $tbl_personnelaPB->where('selected', '=', 1)
                           ->where('forzone', '=', $zone)
                           ->where('phase', '=', $phase)
                           ->where($session_personnela . $assembly_column, '=', $getAssembly)
                           ->whereIn('personcd', $tbl_personnelaPB1)
                           ->where('poststat', '=', 'PB')
                           ->where('training_gr_cd', '=', $trainingHead)
                           ->whereNull('1stTrainingSch')
                           ->limit($subTe->no_of_PB)
                           ->update(['1stTrainingSch' => $subTe->schedule_code]);
                     } else {
                        $tbl_personnelaPB->where('selected', '=', 1)
                           ->where('forzone', '=', $zone)
                           ->where('phase', '=', $phase)
                           ->where(DB::raw('SUBSTRING(personcd,1,4)'), '=', $subTe->choice_subdiv)
                           ->where('poststat', '=', 'PB')
                           ->where('training_gr_cd', '=', $trainingHead)
                           ->whereNull('1stTrainingSch')
                           ->limit($subTe->no_of_PB)
                           ->update(['1stTrainingSch' => $subTe->schedule_code]);
                     }
                     //$assignPB = $subTe->no_of_PB;
                  }
               }
            }
            if (count($dataAr[1]) > 0) {
               foreach ($dataAr[1] as $subTe) {
                  if ($subTe->choice_type === 'O') {
                     $assembly_column = '.assembly_off';
                  } else if ($subTe->choice_type === 'H') {
                     $assembly_column = '.assembly_temp';
                  }

                  //                 $assignPP = 0;
                  //                 $assignPR = $assignP1 = $assignP2 = $assignP3 = $assignPA = $assignPB = 0;
                  $getAssembly = tbl_first_training_venue::where('venue_cd', $subTe->tr_venue_cd)->value('assemblycd');
                  $tbl_personnelaDE = new tbl_personnela;
                  $tbl_personnelaDE->where('1stTrainingSch_2', '=', $subTe->schedule_code)
                     ->where('phase', '=', $phase)
                     ->update(['1stTrainingSch_2' => null]);
                  if ($subTe->no_of_PR != null) {
                     $tbl_personnelaPR = new tbl_personnela;
                     if ($subTe->choice_type === 'B') {
                        $tbl_personnelaPR1 = $tbl_personnelaPR->join('office', 'office.officecd', '=', $session_personnela . ".officecd")->where('office.blockormuni_cd', '=', $subTe->choice_block)->pluck('personcd')->ToArray();
                        $tbl_personnelaPR->where('selected', '=', 1)
                           ->where('forzone', '=', $zone)
                           ->where('phase', '=', $phase)
                           ->whereIn('personcd', $tbl_personnelaPR1)
                           ->where('poststat', '=', 'PR')
                           ->where('training_gr_cd', '=', $trainingHead)
                           ->whereNull('1stTrainingSch_2')
                           ->whereNotNull('1stTrainingSch')
                           ->limit($subTe->no_of_PR)
                           ->update(['1stTrainingSch_2' => $subTe->schedule_code]);
                     } else if ($subTe->choice_type === 'O' || $subTe->choice_type === 'H') {
                        $tbl_personnelaPR1 = $tbl_personnelaPR
                           //->join('office', 'office.officecd', '=', $session_personnela . ".officecd")
                           ->where($session_personnela . $assembly_column, '=', $getAssembly)
                           ->pluck('personcd')->ToArray();


                        $tbl_personnelaPR->where('selected', '=', 1)
                           ->where('forzone', '=', $zone)
                           ->where('phase', '=', $phase)
                           //->where($session_personnela .$assembly_column, '=', $getAssembly)
                           ->whereIn('personcd', $tbl_personnelaPR1)
                           ->where('poststat', '=', 'PR')
                           ->where('training_gr_cd', '=', $trainingHead)
                           ->whereNotNull('1stTrainingSch')
                           ->whereNull('1stTrainingSch_2')
                           ->limit($subTe->no_of_PR)
                           ->update(['1stTrainingSch_2' => $subTe->schedule_code]);
                     } else {
                        $tbl_personnelaPR->where('selected', '=', 1)
                           ->where('forzone', '=', $zone)
                           ->where('phase', '=', $phase)
                           ->where(DB::raw('SUBSTRING(personcd,1,4)'), '=', $subTe->choice_subdiv)
                           ->where('poststat', '=', 'PR')
                           ->where('training_gr_cd', '=', $trainingHead)
                           ->whereNull('1stTrainingSch_2')
                           ->whereNotNull('1stTrainingSch')
                           ->limit($subTe->no_of_PR)
                           ->update(['1stTrainingSch_2' => $subTe->schedule_code]);
                     }
                     // $assignPR = $subTe->no_of_PR;
                  }
                  if ($subTe->no_of_P1 != null) {
                     $tbl_personnelaP1 = new tbl_personnela;
                     if ($subTe->choice_type === 'B') {
                        $tbl_personnelaP11 = $tbl_personnelaP1->join('office', 'office.officecd', '=', $session_personnela . ".officecd")->where('office.blockormuni_cd', '=', $subTe->choice_block)->pluck('personcd')->ToArray();
                        $tbl_personnelaP1->where('selected', '=', 1)
                           ->where('forzone', '=', $zone)
                           ->where('phase', '=', $phase)
                           ->whereIn('personcd', $tbl_personnelaP11)
                           ->where('poststat', '=', 'P1')
                           ->where('training_gr_cd', '=', $trainingHead)
                           ->whereNotNull('1stTrainingSch')
                           ->whereNull('1stTrainingSch_2')
                           ->limit($subTe->no_of_P1)
                           ->update(['1stTrainingSch_2' => $subTe->schedule_code]);
                     } else if ($subTe->choice_type === 'O' || $subTe->choice_type === 'H') {
                        $tbl_personnelaP11 = $tbl_personnelaP1
                           ->where($session_personnela . $assembly_column, '=', $getAssembly)
                           //->join('office', 'office.officecd', '=', $session_personnela . ".officecd")
                           ->pluck('personcd')->ToArray();




                        $tbl_personnelaP1->where('selected', '=', 1)
                           ->where('forzone', '=', $zone)
                           ->where('phase', '=', $phase)

                           ->whereIn('personcd', $tbl_personnelaP11)
                           ->where('poststat', '=', 'P1')
                           ->where('training_gr_cd', '=', $trainingHead)
                           ->whereNotNull('1stTrainingSch')
                           ->whereNull('1stTrainingSch_2')
                           ->limit($subTe->no_of_P1)
                           ->update(['1stTrainingSch_2' => $subTe->schedule_code]);
                     } else {
                        $tbl_personnelaP1->where('selected', '=', 1)
                           ->where('forzone', '=', $zone)
                           ->where('phase', '=', $phase)
                           ->where(DB::raw('SUBSTRING(personcd,1,4)'), '=', $subTe->choice_subdiv)
                           ->where('poststat', '=', 'P1')
                           ->where('training_gr_cd', '=', $trainingHead)
                           ->whereNotNull('1stTrainingSch')
                           ->whereNull('1stTrainingSch_2')
                           ->limit($subTe->no_of_P1)
                           ->update(['1stTrainingSch_2' => $subTe->schedule_code]);
                     }
                     // $assignP1 = $subTe->no_of_P1;
                  }
                  if ($subTe->no_of_P2 != null) {
                     $tbl_personnelaP2 = new tbl_personnela;
                     if ($subTe->choice_type === 'B') {
                        $tbl_personnelaP21 = $tbl_personnelaP2->join('office', 'office.officecd', '=', $session_personnela . ".officecd")->where('office.blockormuni_cd', '=', $subTe->choice_block)->pluck('personcd')->ToArray();
                        $tbl_personnelaP2->where('selected', '=', 1)
                           ->where('forzone', '=', $zone)
                           ->where('phase', '=', $phase)
                           ->whereIn('personcd', $tbl_personnelaP21)
                           ->where('poststat', '=', 'P2')
                           ->where('training_gr_cd', '=', $trainingHead)
                           ->whereNotNull('1stTrainingSch')
                           ->whereNull('1stTrainingSch_2')
                           ->limit($subTe->no_of_P2)
                           ->update(['1stTrainingSch_2' => $subTe->schedule_code]);
                     } else if ($subTe->choice_type === 'O' || $subTe->choice_type === 'H') {
                        $tbl_personnelaP21 = $tbl_personnelaP2
                           //->join('office', 'office.officecd', '=', $session_personnela . ".officecd")
                           ->where($session_personnela . $assembly_column, '=', $getAssembly)
                           ->pluck('personcd')->ToArray();


                        $tbl_personnelaP2->where('selected', '=', 1)
                           ->where('forzone', '=', $zone)
                           ->where('phase', '=', $phase)
                           // ->where($session_personnela .$assembly_column, '=', $getAssembly)
                           ->whereIn('personcd', $tbl_personnelaP21)
                           ->where('poststat', '=', 'P2')
                           ->where('training_gr_cd', '=', $trainingHead)
                           ->whereNotNull('1stTrainingSch')
                           ->whereNull('1stTrainingSch_2')
                           ->limit($subTe->no_of_P2)
                           ->update(['1stTrainingSch_2' => $subTe->schedule_code]);
                     } else {
                        $tbl_personnelaP2->where('selected', '=', 1)
                           ->where('forzone', '=', $zone)
                           ->where(DB::raw('SUBSTRING(personcd,1,4)'), '=', $subTe->choice_subdiv)
                           ->where('poststat', '=', 'P2')
                           ->where('training_gr_cd', '=', $trainingHead)
                           ->whereNotNull('1stTrainingSch')
                           ->whereNull('1stTrainingSch_2')
                           ->limit($subTe->no_of_P2)
                           ->update(['1stTrainingSch_2' => $subTe->schedule_code]);
                     }
                     //$assignP2 = $subTe->no_of_P2;
                  }
                  if ($subTe->no_of_P3 != null) {
                     $tbl_personnelaP3 = new tbl_personnela;
                     if ($subTe->choice_type === 'B') {
                        $tbl_personnelaP31 = $tbl_personnelaP3->join('office', 'office.officecd', '=', $session_personnela . ".officecd")->where('office.blockormuni_cd', '=', $subTe->choice_block)->pluck('personcd')->ToArray();
                        $tbl_personnelaP3->where('selected', '=', 1)
                           ->where('forzone', '=', $zone)
                           ->where('phase', '=', $phase)
                           ->whereIn('personcd', $tbl_personnelaP31)
                           ->where('poststat', '=', 'P3')
                           ->where('training_gr_cd', '=', $trainingHead)
                           ->whereNotNull('1stTrainingSch')
                           ->whereNull('1stTrainingSch_2')
                           ->limit($subTe->no_of_P3)
                           ->update(['1stTrainingSch_2' => $subTe->schedule_code]);
                     } else if ($subTe->choice_type === 'O' || $subTe->choice_type === 'H') {
                        $tbl_personnelaP31 = $tbl_personnelaP3
                           //->join('office', 'office.officecd', '=', $session_personnela . ".officecd")
                           ->where($session_personnela . $assembly_column, '=', $getAssembly)
                           ->pluck('personcd')->ToArray();


                        $tbl_personnelaP3->where('selected', '=', 1)
                           ->where('forzone', '=', $zone)
                           ->where('phase', '=', $phase)
                           //  ->where($session_personnela .$assembly_column, '=', $getAssembly)
                           ->whereIn('personcd', $tbl_personnelaP31)
                           ->where('poststat', '=', 'P3')
                           ->where('training_gr_cd', '=', $trainingHead)
                           ->whereNotNull('1stTrainingSch')
                           ->whereNull('1stTrainingSch_2')
                           ->limit($subTe->no_of_P3)
                           ->update(['1stTrainingSch_2' => $subTe->schedule_code]);
                     } else {
                        $tbl_personnelaP3->where('selected', '=', 1)
                           ->where('forzone', '=', $zone)
                           ->where('phase', '=', $phase)
                           ->where(DB::raw('SUBSTRING(personcd,1,4)'), '=', $subTe->choice_subdiv)
                           ->where('poststat', '=', 'P3')
                           ->where('training_gr_cd', '=', $trainingHead)
                           ->whereNotNull('1stTrainingSch')
                           ->whereNull('1stTrainingSch_2')
                           ->limit($subTe->no_of_P3)
                           ->update(['1stTrainingSch_2' => $subTe->schedule_code]);
                     }
                     //$assignP3 = $subTe->no_of_P3;
                  }
                  if ($subTe->no_of_PA != null) {
                     $tbl_personnelaPA = new tbl_personnela;
                     if ($subTe->choice_type === 'B') {
                        $tbl_personnelaPA1 = $tbl_personnelaPA->join('office', 'office.officecd', '=', $session_personnela . ".officecd")->where('office.blockormuni_cd', '=', $subTe->choice_block)->pluck('personcd')->ToArray();
                        $tbl_personnelaPA->where('selected', '=', 1)
                           ->where('forzone', '=', $zone)
                           ->where('phase', '=', $phase)
                           ->whereIn('personcd', $tbl_personnelaPA1)
                           ->where('poststat', '=', 'PA')
                           ->where('training_gr_cd', '=', $trainingHead)
                           ->whereNotNull('1stTrainingSch')
                           ->whereNull('1stTrainingSch_2')
                           ->limit($subTe->no_of_PA)
                           ->update(['1stTrainingSch_2' => $subTe->schedule_code]);
                     } else if ($subTe->choice_type === 'O' || $subTe->choice_type === 'H') {
                        $tbl_personnelaPA1 = $tbl_personnelaPA
                           //->join('office', 'office.officecd', '=', $session_personnela . ".officecd")
                           ->where($session_personnela . $assembly_column, '=', $getAssembly)
                           ->pluck('personcd')->ToArray();


                        $tbl_personnelaPA->where('selected', '=', 1)
                           ->where('forzone', '=', $zone)
                           ->where('phase', '=', $phase)
                           // ->where($session_personnela .$assembly_column, '=', $getAssembly)
                           ->whereIn('personcd', $tbl_personnelaPA1)
                           ->where('poststat', '=', 'PA')
                           ->where('training_gr_cd', '=', $trainingHead)
                           ->whereNotNull('1stTrainingSch')
                           ->whereNull('1stTrainingSch_2')
                           ->limit($subTe->no_of_PA)
                           ->update(['1stTrainingSch_2' => $subTe->schedule_code]);
                     } else {
                        $tbl_personnelaPA->where('selected', '=', 1)
                           ->where('forzone', '=', $zone)
                           ->where('phase', '=', $phase)
                           ->where(DB::raw('SUBSTRING(personcd,1,4)'), '=', $subTe->choice_subdiv)
                           ->where('poststat', '=', 'PA')
                           ->where('training_gr_cd', '=', $trainingHead)
                           ->whereNotNull('1stTrainingSch')
                           ->whereNull('1stTrainingSch_2')
                           ->limit($subTe->no_of_PA)
                           ->update(['1stTrainingSch_2' => $subTe->schedule_code]);
                     }
                     //$assignPA = $subTe->no_of_PA;
                  }
                  if ($subTe->no_of_PB != null) {
                     $tbl_personnelaPB = new tbl_personnela;
                     if ($subTe->choice_type === 'B') {
                        $tbl_personnelaPB1 = $tbl_personnelaPB->join('office', 'office.officecd', '=', $session_personnela . ".officecd")->where('office.blockormuni_cd', '=', $subTe->choice_block)->pluck('personcd')->ToArray();
                        $tbl_personnelaPB->where('selected', '=', 1)
                           ->where('forzone', '=', $zone)
                           ->where('phase', '=', $phase)
                           ->whereIn('personcd', $tbl_personnelaPB1)
                           ->where('poststat', '=', 'PB')
                           ->where('training_gr_cd', '=', $trainingHead)
                           ->whereNotNull('1stTrainingSch')
                           ->whereNull('1stTrainingSch_2')
                           ->limit($subTe->no_of_PB)
                           ->update(['1stTrainingSch_2' => $subTe->schedule_code]);
                     } else if ($subTe->choice_type === 'O' || $subTe->choice_type === 'H') {
                        $tbl_personnelaPB1 = $tbl_personnelaPB
                           //->join('office', 'office.officecd', '=', $session_personnela . ".officecd")
                           ->where($session_personnela . $assembly_column, '=', $getAssembly)
                           ->pluck('personcd')->ToArray();


                        $tbl_personnelaPB->where('selected', '=', 1)
                           ->where('forzone', '=', $zone)
                           ->where('phase', '=', $phase)
                           //  ->where($session_personnela .$assembly_column, '=', $getAssembly)
                           ->whereIn('personcd', $tbl_personnelaPB1)
                           ->where('poststat', '=', 'PB')
                           ->where('training_gr_cd', '=', $trainingHead)
                           ->whereNotNull('1stTrainingSch')
                           ->whereNull('1stTrainingSch_2')
                           ->limit($subTe->no_of_PB)
                           ->update(['1stTrainingSch_2' => $subTe->schedule_code]);
                     } else {
                        $tbl_personnelaPB->where('selected', '=', 1)
                           ->where('forzone', '=', $zone)
                           ->where('phase', '=', $phase)
                           ->where(DB::raw('SUBSTRING(personcd,1,4)'), '=', $subTe->choice_subdiv)
                           ->where('poststat', '=', 'PB')
                           ->where('training_gr_cd', '=', $trainingHead)
                           ->whereNotNull('1stTrainingSch')
                           ->whereNull('1stTrainingSch_2')
                           ->limit($subTe->no_of_PB)
                           ->update(['1stTrainingSch_2' => $subTe->schedule_code]);
                     }
                     //$assignPB = $subTe->no_of_PB;
                  }
               }
            }


            $totalPostAssignPP = tbl_personnela::where('selected', '=', 1)
               ->where('forzone', '=', $zone)
               ->where('phase', '=', $phase)
               ->where('training_gr_cd', '=', $trainingHead)
               ->whereNotNull('1stTrainingSch')
               ->value(DB::raw('count(personcd)'));
            $totalPostAssignPP_2 = tbl_personnela::where('selected', '=', 1)
               ->where('forzone', '=', $zone)
               ->where('phase', '=', $phase)
               ->whereNotNull('1stTrainingSch_2')
               ->where('training_gr_cd', '=', $trainingHead)
               ->value(DB::raw('count(personcd)'));

            /************************************Status Update********************* */
            // $trainingTypeData = tbl_training_type::where('training_group_cd', $trainingHead)->get();
            // $trainingtypeArr = array();
            // $venueCodesArr = array();
            // foreach ($trainingTypeData as $trainingTypeDataVal) {
            //    array_push($trainingtypeArr, $trainingTypeDataVal->training_code);
            // }
            // $filtered = tbl_first_training_venue::select('first_training_venue.maximumcapacity', 'venue_cd')->get();

            // foreach ($filtered as $key => $filteredeval) {
            //    $scheduleArr = array();

            //    $scheduleCode = tbl_first_training_schedule::where('tr_venue_cd', $filteredeval->venue_cd)
            //       ->where('forzone', $zone)->select('schedule_code')->get();
            //    foreach ($scheduleCode as $scheduleCodeVal) {
            //       array_push($scheduleArr, $scheduleCodeVal->schedule_code);
            //    }
            //    $firstTrainingCodes = array("01", "03", "05");
            //    $secondTrainingCodes = array("02", "04", "06");


            //    $getTotalFirstTrainingAllocated = 0;
            //    $getTotalSecondTrainingAllocated = 0;
            //    if (array_intersect($firstTrainingCodes, $trainingtypeArr)) {
            //       $getTotalFirstTrainingAllocated = tbl_personnela::where('forzone', $zone)->where('phase', '=', $phase)->where('training_gr_cd', $trainingHead)
            //          ->wherein('1stTrainingSch',  $scheduleArr)->count();
            //    } else if (array_intersect($firstTrainingCodes, $secondTrainingCodes)) {
            //       $getTotalSecondTrainingAllocated = tbl_personnela::where('training_gr_cd', $trainingHead)
            //          ->where('forzone', $zone)->where('phase', '=', $phase)->wherein('1stTrainingSch_2',  $scheduleArr)->count();
            //    }
            //    if ($getTotalFirstTrainingAllocated >= $filteredeval->maximumcapacity ||  $getTotalSecondTrainingAllocated >= $filteredeval->maximumcapacity) {
            //       array_push($venueCodesArr, $filteredeval->venue_cd);
            //    }
            // }
            // $updateFirstTrainingSchedule = tbl_first_training_venue::whereIn('venue_cd', $venueCodesArr)->update([
            //    'status' => 0
            // ]);

            $response = array(
               'options' => $totalAssign,
               'totalPP' => $totalTRPP,
               'totalAsPP' => $totalPostAssignPP - $totalAssignPP,
               'totalAs2PP' => $totalPostAssignPP_2 - $totalAssignPP_2,
               'totalPostAsPP' => $totalPostAssignPP,
               'totalPostAs2PP' => $totalPostAssignPP_2,
               'subd' => $subdivision,
               'status' => 1
            );
         } catch (\Exception $e) {
            $response = array(
               'exception' => true,
               'exception_message' => $e->getMessage(),
            );
            $statusCode = 400;
         } finally {
            return response()->json($response, $statusCode);
         }
      }
   }
}
