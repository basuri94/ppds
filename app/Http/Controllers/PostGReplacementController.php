<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\SwappingController;
use App\tbl_personnela;
use App\tbl_replacement_log;
use App\tbl_replacement_log_reserve;
use App\tbl_second_rand_table;
use App\tbl_cancellation_log_reserve;
use DB;

class PostGReplacementController extends Controller
{
    private $swapping;
    public function getPostGData(SwappingController $swapping){
        $this->swapping=$swapping;
        $poststat= $this->swapping->getPostStatus();        
        return view('post_group_replacement_newpp', compact('poststat'));
    }
    public function getPostRData(SwappingController $swapping){
        $this->swapping=$swapping;
        $poststat= $this->swapping->getPostStatus();        
        return view('post_group_replacement_reserve', compact('poststat'));
    }
    public function getSelectedPersonnelID(Request $request){
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        }
        else
        {
            $this->validate($request, [            
            'forZone' => 'required|alpha_num|min:4|max:4',
            'assembly' => 'required|alpha_num|min:3|max:3',
            'bookedP' => 'required|alpha|min:1|max:1',
            'partyno' => 'required|digits_between:1,4',
            'poststatus' => 'required|alpha_num|min:2|max:2',
            'phase' => 'required|digits_between:1,1|min:1|max:1'
            ], [
            
            'forZone.required' => 'For Zone is required',
            'forZone.alpha_num' => 'For Zone must be an alpha numeric',
            'assembly.required' => 'Assembly is required',
            'assembly.alpha_num' => 'Assembly must be an alpha numeric',
            'bookedP.required' => 'Booked is required',
            'bookedP.alpha' => 'Booked must be an alpha',
            'partyno.required' => 'Polling Party No is required',
            'partyno.digits_between' => 'Polling Party No is integer',
            'poststatus.required' => 'Post Status is required',
            'poststatus.alpha_num' => 'Post Status must be an alpha numeric',
            'phase.required' => 'Phase is required',
            'phase.digits_between' => 'Phase must be integer',
            ]);
            try
            {
                $forZone=$request->forZone;
                $phase=$request->phase;
              $assembly=$request->assembly;
              $partyno=$request->partyno;
              $poststatus=$request->poststatus;
              $bookedP=$request->bookedP;
              $list = '';
              $asmData = tbl_personnela::where('forzone','=',$forZone)
                                        ->where('phase','=',$phase)
                                        ->where('forassembly','=',$assembly)
                                         ->where('poststat','=',$poststatus)
                                         ->where('booked','=',$bookedP)
                                         ->where('groupid','=',$partyno)
                                         ->value('personcd');
              //echo $asmData;
              // $trArray=json_decode($asmData);
              $list .= "<option value>[Select]</option>";
            //   foreach($trArray as $trDate){
                  $list .= "<option value='" . $asmData . "'>" . $asmData . "</option>";
            //    }
              $response = array(
                   'options' => $list,
                   'status' => 1);          
            }catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
              $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
      
        }
    }  
    //:::::::::::::::::::::::: For New PP :::::::::::::::::::::::::::://
    public function searchNewPPPostData(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        }
        else
        {
           $this->validate($request, [            
            'zone' => 'required|alpha_num|min:4|max:4',
            'forassembly' => 'required|alpha_num|min:3|max:3',
            'ofc_cd' => 'required|alpha_num|min:10|max:10',
            'booked' => 'required|alpha|min:1|max:1',
            'post_stat' => 'required|alpha_num|min:2|max:2',
            'gender' => 'required|alpha|min:1|max:1',
            'partyno' => 'required|digits_between:1,4',
            'phase' => 'required|digits_between:1,1|min:1|max:1'
            ], [
            
            'zone.required' => 'Zone is required',
            'zone.alpha_num' => 'Zone must be an alpha numeric',
            'forassembly.required' => 'Forassembly is required',
            'forassembly.alpha_num' => 'Forassembly must be an alpha numeric',
            'ofc_cd.required' => 'Office ID is required',
            'ofc_cd.alpha_num' => 'Office ID must be an alpha numeric',
            'booked.required' => 'Booked is required',
            'booked.alpha' => 'Booked must be an alpha',
            'post_stat.required' => 'Post Status is required',
            'post_stat.alpha_num' => 'Post Status must be an alpha numeric',
            'gender.required' => 'Gender is required',
            'gender.alpha' => 'Gender must be an alpha',
            'partyno.required' => 'Polling Party No is required',
            'partyno.digits_between' => 'Polling Party No is integer',
            'phase.required' => 'Phase is required',
            'phase.digits_between' => 'Phase must be integer',
            ]);
            
            try
            {
                $zone=$request->zone;
                $forassembly=$request->forassembly;
                $ofc_id=$request->ofc_cd;
                $booked=$request->booked;
                $post_stat=$request->post_stat;
                $gender=$request->gender;
                $partyno=$request->partyno;

                $phase=$request->phase;
                // dd($phase);
                
                $filtered=""; 
                $resMessage="";
                $Ofccount=0;
                $tbl_personnela1 = new tbl_personnela();             
                $session_personnela=session()->get('personnela_ppds');
                $officeArray= tbl_personnela::where('forzone','=',$zone)
                                        ->where('phase','=',$phase)
                                        ->where('forassembly','=',$forassembly)
                                         ->where('booked','=','P')
                                         ->where('groupid','=',$partyno)
                                         ->where('officecd','!=',$ofc_id)
                                         ->pluck('officecd')->toArray();
               
//                foreach($officeArray as $subTe){ 
//                    $office[$Ofccount]=$subTe->officecd;
//                    $Ofccount++;
//                }
//                $ppcount = $tbl_personnela1
//                           ->join('office',''.$session_personnela.'.officecd','=','office.officecd')
//                           ->join('personnel',''.$session_personnela.'.personcd','=','personnel.personcd')
//                           ->join('poststat',''.$session_personnela.'.poststat','=','poststat.post_stat')
//                           ->leftjoin('policestation','office.policestn_cd','=','policestation.policestationcd')
//                           ->join('district','personnel.districtcd','=','district.districtcd')
//                           ->join('subdivision','office.subdivisioncd','=','subdivision.subdivisioncd')
//                           ->leftjoin('assembly as ass_pre','ass_pre.assemblycd','=','personnel.assembly_temp') 
//                            ->leftjoin('assembly as ass_ofc','ass_ofc.assemblycd','=','personnel.assembly_off')  
//                            ->leftjoin('assembly as ass_per','ass_per.assemblycd','=','personnel.assembly_perm')
//                           ->where(''.$session_personnela.'.assembly_temp','!=', $forassembly)
//                           ->where(''.$session_personnela.'.assembly_perm','!=', $forassembly)
//                           ->where(''.$session_personnela.'.assembly_off','!=', $forassembly)
//                            ->where(''.$session_personnela.'.gender','=', $gender) 
//                           ->where(''.$session_personnela.'.poststat','=', $post_stat)
//                            //->where(''.$session_personnela.'.forassembly','=', $forassembly)
//                           ->where(''.$session_personnela.'.forzone','=', $zone)
//                            ->where(function($q) {
//                                $q->orwhereNull('booked')
//                                ->orwhere('booked', '=', '');
//                            })
//                            ->whereNotIn(''.$session_personnela.'.officecd',$officeArray)
//                            ->value(DB::raw("count(personnel.personcd)"));

//                $mode="own-sub";
//                if($ppcount==0){
//                  $mode="other-sub";  
//                }
                $tbl_personnela = new tbl_personnela();
//                if($mode=="own-sub")
//                {
//                   $tbl_personnela = $tbl_personnela->where('personnel.subdivisioncd','=', substr($ofc_id,0,4));
//                }
                $filtered = $tbl_personnela
                           ->join('office',''.$session_personnela.'.officecd','=','office.officecd')
                           ->join('personnel',''.$session_personnela.'.personcd','=','personnel.personcd')
                           ->join('poststat',''.$session_personnela.'.poststat','=','poststat.post_stat')
                           ->leftjoin('policestation','office.policestn_cd','=','policestation.policestationcd')
                           ->join('district','personnel.districtcd','=','district.districtcd')
                           ->join('subdivision','office.subdivisioncd','=','subdivision.subdivisioncd')
                           ->leftjoin('assembly as ass_pre','ass_pre.assemblycd','=','personnel.assembly_temp') 
                            ->leftjoin('assembly as ass_ofc','ass_ofc.assemblycd','=','personnel.assembly_off')  
                            ->leftjoin('assembly as ass_per','ass_per.assemblycd','=','personnel.assembly_perm')
                           
                            ->where(''.$session_personnela.'.gender','=', $gender) 
                           ->where(''.$session_personnela.'.poststat','=', $post_stat)
                           // ->where(''.$session_personnela.'.forassembly','=', $forassembly)
                            ->where(''.$session_personnela.'.forzone','=', $zone)
                            ->where(''.$session_personnela.'.phase','=', $phase)
                            ->where(function($q) {
                                $q->orwhereNull('booked')
                                ->orwhere('booked', '=', '');
                            })
                            ->whereNotIn(''.$session_personnela.'.officecd',$officeArray)
                            ->where(''.$session_personnela.'.assembly_temp','!=', $forassembly)
                           ->where(''.$session_personnela.'.assembly_perm','!=', $forassembly)
                           ->where(''.$session_personnela.'.assembly_off','!=', $forassembly)
                            ->select(DB::raw("DATE_FORMAT(personnel.dateofbirth,'%d/%m/%Y') as dateofbirth"),''.$session_personnela.'.personcd','district.district','address1','address2','postoffice','pin'
                                    ,'policestation.policestation','subdivision.subdivision','personnel.officecd','personnel.officer_name','personnel.off_desg','personnel.present_addr1','personnel.present_addr2'
                                    ,'personnel.assembly_temp as pre_ass_cd','ass_pre.assemblyname AS pre_ass', 'personnel.assembly_perm as per_ass_cd','ass_per.assemblyname AS per_ass', 'personnel.assembly_off as post_ass_cd','ass_ofc.assemblyname AS post_ass'
                                    ,'poststat.poststatus',''.$session_personnela.'.poststat',''.$session_personnela.'.booked','personnel.gender','personnel.epic','forassembly','forzone','groupid','personnel.subdivisioncd','2ndTrainingSch')
                            ->inRandomOrder()
                            ->take(1)->get();
                if(count($filtered)>0){
                   foreach($filtered as $rwfilter){
//                       if($rwfilter->booked=="C" || $rwfilter->booked==""){
//                          $resMessage="Not Available for Selected Operation";
//                          $status = 0;
//                       }else{
                           $fst='2ndTrainingSch';
                            $resMessage.="<table width='100%' style='font-family: verdana,arial,sans-serif;font-size: 11px;color: #000;'>\n";
                            $resMessage.="<tr><td align='left'>Name: </td><td align='left' colspan='3'>".$rwfilter->officer_name."</td></tr>\n";
                            $resMessage.="<tr><td align='left'>Office ID: </td><td align='left' colspan='3'>".$rwfilter->officecd."</td></tr>\n";
                            $resMessage.="<tr><td align='left'>Designation: </td><td align='left' colspan='3'>".$rwfilter->off_desg."</td></tr>\n";
                            $resMessage.="<tr><td align='left'>Office Address: </td><td align='left' colspan='3'>".$rwfilter->address1.",".$rwfilter->address2.", PO-".$rwfilter->postoffice.", PS-".$rwfilter->policestation.", Subdiv-".$rwfilter->subdivision.", Dist.-".$rwfilter->district.",".$rwfilter->pin."</td></tr>\n";
                            $resMessage.="<tr><td align='left'>Date of Birth: </td><td align='left'>".$rwfilter->dateofbirth."</td><td align='left'>Gender: </td><td align='left'>".$rwfilter->gender."<hidden id='hid_gender' name='hid_gender' style='display:none;'>".$rwfilter->gender."</hidden></td></tr>\n";
                            $resMessage.="<tr><td align='left'>EPIC No: </td><td align='left'>".$rwfilter->epic."</td><td align='left'>Posting Status: </td><td align='left'>".$rwfilter->poststatus."<hidden id='hid_post_stat' name='hid_post_stat' style='display:none;'>".$rwfilter->poststat."</hidden></td></tr>\n";
                            $resMessage.="<tr><td align='left'>Present Address: </td><td align='left' colspan='3'>".$rwfilter->present_addr1.", ".$rwfilter->present_addr2."</td></tr>\n";
                            $resMessage.="<tr><td align='left' colspan='4'><b>Assembly of</b></td></tr>\n";
                            $resMessage.="<tr><td align='left' colspan='2'>Present Address: </td><td align='left' colspan='2'>".$rwfilter->pre_ass."<hidden id='hid_pre_ass' name='hid_pre_ass' style='display:none;'>".$rwfilter->pre_ass_cd."</hidden></td></tr>\n";
                            $resMessage.="<tr><td align='left' colspan='2'>Permanent Address: </td><td align='left' colspan='2'>".$rwfilter->per_ass."<hidden id='hid_per_ass' name='hid_per_ass' style='display:none;'>".$rwfilter->per_ass_cd."</hidden></td></tr>\n";
                            $resMessage.="<tr><td align='left' colspan='2'>Place of Posting: </td><td align='left' colspan='2'>".$rwfilter->post_ass."<hidden id='hid_post_ass' name='hid_post_ass' style='display:none;'>".$rwfilter->post_ass_cd."</hidden></td></tr>\n";
                            $resMessage.="<tr><td align='left' colspan='4'><hidden id='hid_forassembly' name='hid_forassembly' style='display:none;'>".$rwfilter->forassembly."</hidden>\n<input type='hidden' id='hid_regroupid' name='hid_regroupid' value='".$rwfilter->groupid."'/>\n<input type='hidden' id='hid_rebooked' name='hid_rebooked' value='".$rwfilter->booked."'/>\n<hidden id='hid_per_cd' name='hid_per_cd' style='display:none;'>".$rwfilter->personcd."</hidden>\n <hidden id='hid_for_zone' name='hid_for_zone' style='display:none;'>".$rwfilter->forzone."</hidden>\n <hidden id='hid_dcrccd' name='hid_dcrccd' style='display:none;'>".$rwfilter->dcrccd."</hidden>\n <hidden id='hid_sub_div' name='hid_sub_div' style='display:none;'>".$rwfilter->subdivisioncd."</hidden>\n <hidden id='hid_ofc_cd' name='hid_ofc_cd' style='display:none;'>".$rwfilter->officecd."</hidden>\n <hidden id='hid_training2_sch' name='hid_training2_sch' style='display:none;'>".$rwfilter->$fst."</hidden></td></tr>\n";

                            $resMessage.="<tr><td align='right' colspan='2'>Booked : </td><td colspan='2' align='left' id='n_booked'>No</td></tr>\n";
                            $resMessage.="</table>";
                            $pid = $rwfilter->personcd;
                            $poststat=$rwfilter->poststat;
                            $status = 1;
                       //}
                           
                         
                   }                    
                }else{
                   $pid = "";
                   $resMessage="No records found"; 
                   $status = 0;
                }

                $response = array(
                   'options' => $resMessage,
                   'perid' =>$pid,
                    'poststat'=>$poststat,
                   'status' => $status
                );
            }catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
              $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
        
    }
    //:::::::::::::::::::::::: For Reserve PP ::::::::::::::::::::::://
    public function searchNewPPReverseData(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        }
        else
        {
           $this->validate($request, [            
            'zone' => 'required|alpha_num|min:4|max:4',
            'forassembly' => 'required|alpha_num|min:3|max:3',
            'ofc_cd' => 'required|alpha_num|min:10|max:10',
            'booked' => 'required|alpha|min:1|max:1',
            'post_stat' => 'required|alpha_num|min:2|max:2',
            'gender' => 'required|alpha|min:1|max:1',
            'partyno' => 'required|digits_between:1,4',
            'phase' => 'required|digits_between:1,1|min:1|max:1'
            ], [
            
            'zone.required' => 'Zone is required',
            'zone.alpha_num' => 'Zone must be an alpha numeric',
            'forassembly.required' => 'Forassembly is required',
            'forassembly.alpha_num' => 'Forassembly must be an alpha numeric',
            'ofc_cd.required' => 'Office ID is required',
            'ofc_cd.alpha_num' => 'Office ID must be an alpha numeric',
            'booked.required' => 'Booked is required',
            'booked.alpha' => 'Booked must be an alpha',
            'post_stat.required' => 'Post Status is required',
            'post_stat.alpha_num' => 'Post Status must be an alpha numeric',
            'gender.required' => 'Gender is required',
            'gender.alpha' => 'Gender must be an alpha',
            'partyno.required' => 'Polling Party No is required',
            'partyno.digits_between' => 'Polling Party No is integer',
            'phase.required' => 'Phase is required',
            'phase.digits_between' => 'Phase must be integer',
            ]);
            
            try
            {
                $zone=$request->zone;
                $phase=$request->phase;
                // dd($phase);
                $forassembly=$request->forassembly;
                $ofc_id=$request->ofc_cd;
                $booked=$request->booked;
                $post_stat=$request->post_stat;
                $gender=$request->gender;
                $partyno=$request->partyno;
                
                $filtered=""; 
                $resMessage="";
                $Ofccount=0;
                $tbl_personnela1 = new tbl_personnela();             
                $session_personnela=session()->get('personnela_ppds');
                $officeArray= tbl_personnela::where('forzone','=',$zone)
                                         ->where('phase','=',$phase)
                                         ->where('forassembly','=',$forassembly)
                                         ->where('booked','=','P')
                                         ->where('groupid','=',$partyno)
                                         ->where('officecd','!=',$ofc_id)
                                         ->pluck('officecd')->toArray();
               
                $tbl_personnela = new tbl_personnela();
//                if($mode=="own-sub")
//                {
//                   $tbl_personnela = $tbl_personnela->where('personnel.subdivisioncd','=', substr($ofc_id,0,4));
//                }
                $filtered = $tbl_personnela
                           ->join('office',''.$session_personnela.'.officecd','=','office.officecd')
                           ->join('personnel',''.$session_personnela.'.personcd','=','personnel.personcd')
                           ->join('poststat',''.$session_personnela.'.poststat','=','poststat.post_stat')
                           ->leftjoin('policestation','office.policestn_cd','=','policestation.policestationcd')
                           ->join('district','personnel.districtcd','=','district.districtcd')
                           ->join('subdivision','office.subdivisioncd','=','subdivision.subdivisioncd')
                           ->leftjoin('assembly as ass_pre','ass_pre.assemblycd','=','personnel.assembly_temp') 
                            ->leftjoin('assembly as ass_ofc','ass_ofc.assemblycd','=','personnel.assembly_off')  
                            ->leftjoin('assembly as ass_per','ass_per.assemblycd','=','personnel.assembly_perm')
                           
                            ->where(''.$session_personnela.'.gender','=', $gender) 
                           ->where(''.$session_personnela.'.poststat','=', $post_stat)
                            ->where(''.$session_personnela.'.forassembly','=', $forassembly)
                            ->where(''.$session_personnela.'.forzone','=', $zone)
                            ->where(''.$session_personnela.'.phase','=', $phase)
                            ->where('booked', '=', 'R')                          
                            ->whereNotIn(''.$session_personnela.'.officecd',$officeArray)
                            ->where(''.$session_personnela.'.assembly_temp','!=', $forassembly)
                           ->where(''.$session_personnela.'.assembly_perm','!=', $forassembly)
                           ->where(''.$session_personnela.'.assembly_off','!=', $forassembly)
                            ->select(DB::raw("DATE_FORMAT(personnel.dateofbirth,'%d/%m/%Y') as dateofbirth"),''.$session_personnela.'.personcd','district.district','address1','address2','postoffice','pin'
                                    ,'policestation.policestation','subdivision.subdivision','personnel.officecd','personnel.officer_name','personnel.off_desg','personnel.present_addr1','personnel.present_addr2'
                                    ,'personnel.assembly_temp as pre_ass_cd','ass_pre.assemblyname AS pre_ass', 'personnel.assembly_perm as per_ass_cd','ass_per.assemblyname AS per_ass', 'personnel.assembly_off as post_ass_cd','ass_ofc.assemblyname AS post_ass'
                                    ,'poststat.poststatus',''.$session_personnela.'.poststat',''.$session_personnela.'.booked','personnel.gender','personnel.epic','forassembly','forzone','groupid','personnel.subdivisioncd','2ndTrainingSch')
                            ->inRandomOrder()
                            ->take(1)->get();
                if(count($filtered)>0){
                   foreach($filtered as $rwfilter){
//                       if($rwfilter->booked=="C" || $rwfilter->booked==""){
//                          $resMessage="Not Available for Selected Operation";
//                          $status = 0;
//                       }else{
                           $fst='2ndTrainingSch';
                            $resMessage.="<table width='100%' style='font-family: verdana,arial,sans-serif;font-size: 11px;color: #000;'>\n";
                            $resMessage.="<tr><td align='left'>Name: </td><td align='left' colspan='3'>".$rwfilter->officer_name."</td></tr>\n";
                            $resMessage.="<tr><td align='left'>Office ID: </td><td align='left' colspan='3'>".$rwfilter->officecd."</td></tr>\n";
                            $resMessage.="<tr><td align='left'>Designation: </td><td align='left' colspan='3'>".$rwfilter->off_desg."</td></tr>\n";
                            $resMessage.="<tr><td align='left'>Office Address: </td><td align='left' colspan='3'>".$rwfilter->address1.",".$rwfilter->address2.", PO-".$rwfilter->postoffice.", PS-".$rwfilter->policestation.", Subdiv-".$rwfilter->subdivision.", Dist.-".$rwfilter->district.",".$rwfilter->pin."</td></tr>\n";
                            $resMessage.="<tr><td align='left'>Date of Birth: </td><td align='left'>".$rwfilter->dateofbirth."</td><td align='left'>Gender: </td><td align='left'>".$rwfilter->gender."<hidden id='hid_gender' name='hid_gender' style='display:none;'>".$rwfilter->gender."</hidden></td></tr>\n";
                            $resMessage.="<tr><td align='left'>EPIC No: </td><td align='left'>".$rwfilter->epic."</td><td align='left'>Posting Status: </td><td align='left'>".$rwfilter->poststatus."<hidden id='hid_post_stat' name='hid_post_stat' style='display:none;'>".$rwfilter->poststat."</hidden></td></tr>\n";
                            $resMessage.="<tr><td align='left'>Present Address: </td><td align='left' colspan='3'>".$rwfilter->present_addr1.", ".$rwfilter->present_addr2."</td></tr>\n";
                            $resMessage.="<tr><td align='left' colspan='4'><b>Assembly of</b></td></tr>\n";
                            $resMessage.="<tr><td align='left' colspan='2'>Present Address: </td><td align='left' colspan='2'>".$rwfilter->pre_ass."<hidden id='hid_pre_ass' name='hid_pre_ass' style='display:none;'>".$rwfilter->pre_ass_cd."</hidden></td></tr>\n";
                            $resMessage.="<tr><td align='left' colspan='2'>Permanent Address: </td><td align='left' colspan='2'>".$rwfilter->per_ass."<hidden id='hid_per_ass' name='hid_per_ass' style='display:none;'>".$rwfilter->per_ass_cd."</hidden></td></tr>\n";
                            $resMessage.="<tr><td align='left' colspan='2'>Place of Posting: </td><td align='left' colspan='2'>".$rwfilter->post_ass."<hidden id='hid_post_ass' name='hid_post_ass' style='display:none;'>".$rwfilter->post_ass_cd."</hidden></td></tr>\n";
                            $resMessage.="<tr><td align='left' colspan='4'><hidden id='hid_forassembly' name='hid_forassembly' style='display:none;'>".$rwfilter->forassembly."</hidden>\n<input type='hidden' id='hid_regroupid' name='hid_regroupid' value='".$rwfilter->groupid."'/>\n<input type='hidden' id='hid_rebooked' name='hid_rebooked' value='".$rwfilter->booked."'/>\n<hidden id='hid_per_cd' name='hid_per_cd' style='display:none;'>".$rwfilter->personcd."</hidden>\n <hidden id='hid_for_zone' name='hid_for_zone' style='display:none;'>".$rwfilter->forzone."</hidden>\n <hidden id='hid_dcrccd' name='hid_dcrccd' style='display:none;'>".$rwfilter->dcrccd."</hidden>\n <hidden id='hid_sub_div' name='hid_sub_div' style='display:none;'>".$rwfilter->subdivisioncd."</hidden>\n <hidden id='hid_ofc_cd' name='hid_ofc_cd' style='display:none;'>".$rwfilter->officecd."</hidden>\n <hidden id='hid_training2_sch' name='hid_training2_sch' style='display:none;'>".$rwfilter->$fst."</hidden></td></tr>\n";

                            $resMessage.="<tr><td align='right' colspan='2'>Booked : </td><td colspan='2' align='left' id='n_booked'>Reserve</td></tr>\n";
                            $resMessage.="</table>";
                            $pid = $rwfilter->personcd;
                            $status = 1;
                       //}
                           
                         
                   }                    
                }else{
                   $pid = "";
                   $resMessage="No records found"; 
                   $status = 0;
                }

                $response = array(
                   'options' => $resMessage,
                   'perid' =>$pid,
                   'status' => $status
                );
            }catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
              $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
        
    }
    public function postReplaceNewPPData(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        }
        else
        {
           $this->validate($request, [            
                'zone' => 'required|alpha_num|min:4|max:4',
                'forassembly' => 'required|alpha_num|min:3|max:3',
                'ofc_cd' => 'required|alpha_num|min:10|max:10',
                'booked' => 'required|alpha|min:1|max:1',
               'reason' => 'required|regex:/^[A-Za-z0-9\s,-.]+$/i|max:30',
                'hid_rebooked' => 'nullable|alpha|min:1|max:1',
                'hid_regroupid' => 'nullable|digits_between:1,4',
                'post_stat' => 'required|alpha_num|min:2|max:2',
                'gender' => 'required|alpha|min:1|max:1',
                'partyno' => 'required|digits_between:1,4',
                'personnel_id' => 'required|alpha_num|min:11|max:11',
                'newpersonnel_id' => 'required|alpha_num|min:11|max:11',
                'dcrccd' => 'required|alpha_num|min:6|max:6',
                'training2_sch' => 'nullable|alpha_num|min:12|max:12',
                'no_of_member' => 'required|digits_between:1,1',
                'phase' => 'required|digits_between:1,1|min:1|max:1'
            ], [
            
                'zone.required' => 'Zone is required',
                'zone.alpha_num' => 'Zone must be an alpha numeric',
                'forassembly.required' => 'Forassembly is required',
                'forassembly.alpha_num' => 'Forassembly must be an alpha numeric',
                'ofc_cd.required' => 'Office ID is required',
                'ofc_cd.alpha_num' => 'Office ID must be an alpha numeric',
                'booked.required' => 'Booked is required',
                'booked.alpha' => 'Booked must be an alpha',
                'reason.required' => 'Reason is required',
                'reason.regex' => 'Reason must not be special characters',
                'reason.max' => 'Reason must be 30 characters',
                'post_stat.required' => 'Post Status is required',
                'post_stat.alpha_num' => 'Post Status must be an alpha numeric',
                'gender.required' => 'Gender is required',
                'gender.alpha' => 'Gender must be an alpha',
                'partyno.required' => 'Polling Party No is required',
                'partyno.digits_between' => 'Polling Party No is integer',
                'personnel_id.required' => 'OLD Personnel ID is required',
                'personnel_id.alpha_num' => 'OLD Personnel ID must be an alpha numeric',
                'newpersonnel_id.required' => 'NEW Personnel ID is required',
                'newpersonnel_id.alpha_num' => 'NEW Personnel ID must be an alpha numeric',
                'dcrccd.required' => 'DCRC is required',
                'dcrccd.alpha_num' => 'DCRC must be an alpha numeric',
                //'training2_sch.required' => 'OlD Training Schedule is required',
                'training2_sch.alpha_num' => 'OlD Training Schedule must be an alpha numeric',
                'no_of_member.required' => 'Number of member is required',
                'phase.required' => 'Phase is required',
            'phase.digits_between'=>'Phase must be an integer',
            ]);
            
            try
            {
                $zone=$request->zone;

                $phase=$request->phase;
                // dd($phase);
                $forassembly=$request->forassembly;
                $ofc_id=$request->ofc_cd;
                $no_of_member=$request->no_of_member;
                $booked=$request->booked;
                 $reason=$request->reason;
                $post_stat=$request->post_stat;
                $gender=$request->gender;
                $oldpersonnel_id=$request->personnel_id;
                $newpersonnel_id=$request->newpersonnel_id;
                $partyno=$request->partyno;
                $dcrccd=$request->dcrccd;
                $oldtraining_sch=$request->training2_sch;
                $hid_rebooked=$request->hid_rebooked;
                $hid_regroupid=$request->hid_regroupid;
                
                $filtered=""; 
                $resMessage="";
               // $tbl_personnela1 = new tbl_personnela();             
                $session_personnela=session()->get('personnela_ppds');
                
                $duplicate_id = tbl_replacement_log::where('new_personnel','=', $newpersonnel_id)
                                                      ->value(DB::raw("count(*)"));
                                               
                if($duplicate_id == 0){
                     $tbl_personnela=new tbl_personnela; 
//                   if($chkSameVenueTraining=='true'){                      
//                      $tbl_personnela
//                         ->where('forzone','=',$zone)
//                         ->where('personcd','=',$newpersonnel_id)
//                         ->update(['1stTrainingSch'=>$oldtraining_sch]); 
//                    }else{
//                      $tbl_personnela
//                         ->where('forzone','=',$zone)
//                         ->where('personcd','=',$newpersonnel_id)
//                         ->update(['1stTrainingSch'=>$training_sch]);   
//                    }
                    $user_code=session()->get("code_ppds");
                    $tbl_replacement_log = new tbl_replacement_log();
                    $tbl_replacement_log->old_personnel = $oldpersonnel_id;
                    $tbl_replacement_log->new_personnel = $newpersonnel_id;
                    $tbl_replacement_log->assemblycd = $forassembly;
                    $tbl_replacement_log->groupid = $partyno;
                    $tbl_replacement_log->phase_id = $phase;
                     $tbl_replacement_log->reason = $reason;
                    $tbl_replacement_log->usercode = $user_code;
                    $tbl_replacement_log->save();
                    
                    tbl_personnela::where('forzone','=',$zone)
                         ->where('personcd','=',$oldpersonnel_id)
                         ->where('phase','=',$phase)
                         ->update(['booked'=>'C','selected'=>'0','2ndTrainingSch'=>null,'dcrccd'=>'','forassembly'=>'','groupid'=>'0']); 
                    tbl_personnela::where('forzone','=',$zone)
                         ->where('personcd','=',$newpersonnel_id)
                         ->where('phase','=',$phase)
                         ->update(['booked'=>$booked,'no_of_member'=>$no_of_member,'forassembly'=>$forassembly,'selected'=>'1','2ndTrainingSch'=>$oldtraining_sch,'dcrccd'=>$dcrccd,'groupid'=>$partyno]);
                    
                    if($hid_rebooked=='R'){
                    tbl_second_rand_table::where('per_poststat', '=', $post_stat)
                             ->where('groupid', '=', $hid_regroupid)
                             ->where('assembly', '=', $forassembly)
                             ->where('phase','=',$phase)
                             ->where('party_reserve', '=', 'R')
                             ->delete();
                    }
                    DB::select('call second_replace_party(?,?,?,?,?,?)',[$post_stat,$booked,$forassembly,$partyno,$session_personnela,$phase]);
                    // DB::select('call second_replace_party(?,?,?,?,?)',[$post_stat,$booked,$forassembly,$partyno,$session_personnela]);
//                    switch($post_stat)
//                    {
//                         case ($post_stat=='PR'):
//                             tbl_second_rand_table::join(''.$session_personnela.'', function($join) {
//                                $join->on(''.$session_personnela.'.forassembly', '=', 'second_rand_table.assembly');
//                                $join->on(''.$session_personnela.'.groupid', '=', 'second_rand_table.groupid');
//                            })
//                            ->join('personnel',''.$session_personnela.'.personcd','=','personnel.personcd')
//                            ->where('second_rand_table.assembly','=',$forassembly)
//                            ->where('second_rand_table.groupid','=',$partyno)
//                            ->where('second_rand_table.party_reserve','=',$booked)
//                            ->where(''.$session_personnela.'.booked','=',$booked)
//                            ->where(''.$session_personnela.'.poststat','=',$post_stat)
//                            ->update(['pr_personcd'=>$booked,'forassembly'=>$forassembly,'selected'=>'1','2ndTrainingSch'=>$oldtraining_sch,'dcrccd'=>$dcrccd,'groupid'=>$partyno]);
//                          break;
//                         case ($post_stat=='P1'):
//                          break;
//                         case ($post_stat=='P2'):
//                          break;
//                         case ($post_stat=='P3'):
//                          break;
//                         case ($post_stat=='PA'):
//                          break;
//                         case ($post_stat=='PB'):
//                          break;
//                         default:
//		          break;
//                    }
//                    $ccount = tbl_second_rand_table::where('personcd', '=', $oldpersonnel_id)
//                             ->where('forzone','=',$zone)
//                             ->value(DB::raw("count(personcd)"));
//                    if($ccount>0){
//                       $slno = tbl_second_rand_table::where('personcd', '=', $oldpersonnel_id)
//                               ->where('forzone','=',$zone)
//                               ->value('sl_no');
//                    }else{
//                        $slno = 0;
//                    }

                    //$sub_divcd = substr($newpersonnel_id,0,4);
//                    DB::select('call first_replace_pid(?,?,?,?,?)',[$zone,$newpersonnel_id,$session_personnela,"",$slno]);
                    $resMessage = "NEW Personnel ID [".$newpersonnel_id."] has been replaced successfuly";
                    $status = 1;
                }else{
                    $resMessage = "NEW Personnel ID already replaced.Please try again";
                    $status = 0;
                }

                $response = array(
                   'options' => $resMessage,
                   'status' => $status
                );
            }catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
              $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
        
    }
    
 
    
    //:::::::::::::::::::::::: for Reserve Replacement :::::::::::::://
    public function getOldPersonnelReserveDetails(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        }
        else
        {
          $this->validate($request, [            
            'personnel_id' => 'required|alpha_num|max:11|min:11',
            'zone' => 'required|alpha_num|max:4|min:4',
            'phase' => 'required|digits_between:1,1|min:1|max:1'
            ], [
            
            'personnel_id.required' => 'Personnel ID is required',
            'personnel_id.alpha_num' => 'Personnel ID must be an alpha numeric',
              'zone.required' => 'Zone is required',
            'zone.alpha_num' => 'Zone must be an alpha numeric characters',
            'phase.required' => 'Phase is required',
            'phase.digits_between'=>'Phase must be an integer',
            ]);
            $personnel_id=$request->personnel_id;
            $zone=$request->zone;
            $phase=$request->phase;
            try
            {
                $filtered=""; 
                $resMessage="";
                $tbl_personnela = new tbl_personnela();             
                $session_personnela=session()->get('personnela_ppds');
                $filtered = $tbl_personnela
                           ->join('office',''.$session_personnela.'.officecd','=','office.officecd')
                           ->join('personnel',''.$session_personnela.'.personcd','=','personnel.personcd')
                           ->join('poststat',''.$session_personnela.'.poststat','=','poststat.post_stat')
                           ->leftjoin('policestation','office.policestn_cd','=','policestation.policestationcd')
                           ->join('district','personnel.districtcd','=','district.districtcd')
                           ->join('subdivision','office.subdivisioncd','=','subdivision.subdivisioncd')
                           ->leftjoin('assembly as ass_pre','ass_pre.assemblycd','=','personnel.assembly_temp') 
                            ->leftjoin('assembly as ass_ofc','ass_ofc.assemblycd','=','personnel.assembly_off')  
                            ->leftjoin('assembly as ass_per','ass_per.assemblycd','=','personnel.assembly_perm')  
                            ->where(''.$session_personnela.'.personcd','=', $personnel_id)
                            ->where(''.$session_personnela.'.forzone', '=', $zone)
                            ->where(''.$session_personnela.'.phase', '=', $phase)
                            ->groupBy(''.$session_personnela.'.personcd')
                            ->select(DB::raw("DATE_FORMAT(personnel.dateofbirth,'%d/%m/%Y') as dateofbirth"),''.$session_personnela.'.personcd','district.district','address1','address2','postoffice','pin'
                                    ,'policestation.policestation','subdivision.subdivision','personnel.officecd','personnel.officer_name','personnel.off_desg','personnel.present_addr1','personnel.present_addr2'
                                    ,'personnel.assembly_temp as pre_ass_cd','ass_pre.assemblyname AS pre_ass', 'personnel.assembly_perm as per_ass_cd','ass_per.assemblyname AS per_ass', 'personnel.assembly_off as post_ass_cd','ass_ofc.assemblyname AS post_ass'
                                    ,'poststat.poststatus',''.$session_personnela.'.poststat',''.$session_personnela.'.booked','personnel.gender','personnel.epic','forassembly','forzone','groupid','personnel.subdivisioncd','1stTrainingSch',''.$session_personnela.'.dcrccd','2ndTrainingSch',''.$session_personnela.'.no_of_member')->get();
           
                if(count($filtered)>0){
                   foreach($filtered as $rwfilter){
                       if($rwfilter->booked=="C" || $rwfilter->booked=="" || $rwfilter->booked=="P"){
                          $resMessage="Not Available for Selected Operation";
                          $status = 0;
                       }else{
                           $fst='1stTrainingSch';
                           $sst='2ndTrainingSch';
                            $resMessage.="<table width='100%' style='font-family: verdana,arial,sans-serif;font-size: 11px;color: #000;'>\n";
                            $resMessage.="<tr><td align='left'>Name: </td><td align='left' colspan='3'>".$rwfilter->officer_name."</td></tr>\n";
                            $resMessage.="<tr><td align='left'>Office ID: </td><td align='left' colspan='3'>".$rwfilter->officecd."</td></tr>\n";
                            $resMessage.="<tr><td align='left'>Designation: </td><td align='left' colspan='3'>".$rwfilter->off_desg."</td></tr>\n";
                            $resMessage.="<tr><td align='left'>Office Address: </td><td align='left' colspan='3'>".$rwfilter->address1.",".$rwfilter->address2.", PO-".$rwfilter->postoffice.", PS-".$rwfilter->policestation.", Subdiv-".$rwfilter->subdivision.", Dist.-".$rwfilter->district.",".$rwfilter->pin."</td></tr>\n";
                            $resMessage.="<tr><td align='left'>Date of Birth: </td><td align='left'>".$rwfilter->dateofbirth."</td><td align='left'>Gender: </td><td align='left'>".$rwfilter->gender."<input type='hidden' id='gender' name='gender' value='".$rwfilter->gender."'></td></tr>\n";
                            $resMessage.="<tr><td align='left'>EPIC No: </td><td align='left'>".$rwfilter->epic."</td><td align='left'>Posting Status: </td><td align='left'>".$rwfilter->poststatus."<input type='hidden' id='post_stat' name='post_stat' value='".$rwfilter->poststat."'></td></tr>\n";
                            $resMessage.="<tr><td align='left'>Present Address: </td><td align='left' colspan='3'>".$rwfilter->present_addr1.", ".$rwfilter->present_addr2."</td></tr>\n";
                            $resMessage.="<tr><td align='left' colspan='4'><b>Assembly of</b></td></tr>\n";
                            $resMessage.="<tr><td align='left' colspan='2'>Present Address: </td><td align='left' colspan='2'>".$rwfilter->pre_ass."<hidden id='hid_pre_ass' name='hid_pre_ass' style='display:none;'>".$rwfilter->pre_ass_cd."</hidden></td></tr>\n";
                            $resMessage.="<tr><td align='left' colspan='2'>Permanent Address: </td><td align='left' colspan='2'>".$rwfilter->per_ass."<hidden id='hid_per_ass' name='hid_per_ass' style='display:none;'>".$rwfilter->per_ass_cd."</hidden></td></tr>\n";
                            $resMessage.="<tr><td align='left' colspan='2'>Place of Posting: </td><td align='left' colspan='2'>".$rwfilter->post_ass."<hidden id='hid_post_ass' name='hid_post_ass' style='display:none;'>".$rwfilter->post_ass_cd."</hidden></td></tr>\n";
                            $resMessage.="<tr><td align='left' colspan='4'><input type='hidden' id='forassembly' name='forassembly'  value='".$rwfilter->forassembly."'>\n<input type='hidden' id='groupid' name='groupid' value='".$rwfilter->groupid."'>\n<input type='hidden' id='booked' name='booked' value='".$rwfilter->booked."'>\n <input type='hidden' id='no_of_member' name='no_of_member' value='".$rwfilter->no_of_member."'>\n <input type='hidden' id='per_cd' name='per_cd' value='".$rwfilter->personcd."'>\n <input type='hidden' id='zone' name='zone' value='".$rwfilter->forzone."'>\n <input type='hidden' id='dcrccd' name='dcrccd' value='".$rwfilter->dcrccd."'>\n <input type='hidden' id='sub_div' name='sub_div' value='".$rwfilter->subdivisioncd."'>\n <input type='hidden' id='ofc_cd' name='ofc_cd' value='".$rwfilter->officecd."'>\n <input type='hidden' id='training1_sch' name='training1_sch' value='".$rwfilter->$fst."'> <input type='hidden' id='training2_sch' name='training2_sch' value='".$rwfilter->$sst."'></td></tr>\n";

                            $resMessage.="<tr><td align='right' colspan='2'>Booked : </td><td colspan='2' align='left' id='o_booked'>Yes</td></tr>\n";
                            $resMessage.="</table>";
                            $status = 1;
                       }
                   }                    
                }else{
                   $resMessage="No records found"; 
                   $status = 0;
                }

                $response = array(
                   'options' => $resMessage,
                   'status' => $status
                );
            }catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
              $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
        
    }
    
    public function searchNewReserveDataFR(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        }
        else
        {
           $this->validate($request, [            
            'zone' => 'required|alpha_num|min:4|max:4',
            'forassembly' => 'required|alpha_num|min:3|max:3',
            'ofc_cd' => 'required|alpha_num|min:10|max:10',
            'booked' => 'required|alpha|min:1|max:1',
            'post_stat' => 'required|alpha_num|min:2|max:2',
            'gender' => 'required|alpha|min:1|max:1',
            'phase' => 'required|digits_between:1,1|min:1|max:1'
            ], [
            
            'zone.required' => 'Zone is required',
            'zone.alpha_num' => 'Zone must be an alpha numeric',
            'forassembly.required' => 'Forassembly is required',
            'forassembly.alpha_num' => 'Forassembly must be an alpha numeric',
            'ofc_cd.required' => 'Office ID is required',
            'ofc_cd.alpha_num' => 'Office ID must be an alpha numeric',
            'booked.required' => 'Booked is required',
            'booked.alpha' => 'Booked must be an alpha',
            'post_stat.required' => 'Post Status is required',
            'post_stat.alpha_num' => 'Post Status must be an alpha numeric',
            'gender.required' => 'Gender is required',
            'gender.alpha' => 'Gender must be an alpha',
            'phase.required' => 'Phase is required',
            'phase.digits_between'=>'Phase must be an integer',
            ]);
            
            try
            {
                $zone=$request->zone;
                $phase = $request->phase;
                // dd($phase);
                $forassembly=$request->forassembly;
                $ofc_id=$request->ofc_cd;
                $booked=$request->booked;
                $post_stat=$request->post_stat;
                $gender=$request->gender;
                
                $filtered=""; 
                $resMessage="";
                $tbl_personnela1 = new tbl_personnela();             
                $session_personnela=session()->get('personnela_ppds');

                $tbl_personnela = new tbl_personnela();
//                if($mode=="own-sub")
//                {
//                   $tbl_personnela = $tbl_personnela->where('personnel.subdivisioncd','=', substr($ofc_id,0,4));
//                }
                $filtered = $tbl_personnela
                           ->join('office',''.$session_personnela.'.officecd','=','office.officecd')
                           ->join('personnel',''.$session_personnela.'.personcd','=','personnel.personcd')
                           ->join('poststat',''.$session_personnela.'.poststat','=','poststat.post_stat')
                           ->leftjoin('policestation','office.policestn_cd','=','policestation.policestationcd')
                           ->join('district','personnel.districtcd','=','district.districtcd')
                           ->join('subdivision','office.subdivisioncd','=','subdivision.subdivisioncd')
                           ->leftjoin('assembly as ass_pre','ass_pre.assemblycd','=','personnel.assembly_temp') 
                            ->leftjoin('assembly as ass_ofc','ass_ofc.assemblycd','=','personnel.assembly_off')  
                            ->leftjoin('assembly as ass_per','ass_per.assemblycd','=','personnel.assembly_perm')
                           
                            ->where(''.$session_personnela.'.gender','=', $gender) 
                           ->where(''.$session_personnela.'.poststat','=', $post_stat)
                           // ->where(''.$session_personnela.'.forassembly','=', $forassembly)
                            ->where(''.$session_personnela.'.forzone','=', $zone)
                            ->where(''.$session_personnela.'.phase','=', $phase)
                            ->where(function($q) {
                                $q->orwhereNull('booked')
                                ->orwhere('booked', '=', '');
                            })
                            ->where(''.$session_personnela.'.assembly_temp','!=', $forassembly)
                            ->where(''.$session_personnela.'.assembly_perm','!=', $forassembly)
                            ->where(''.$session_personnela.'.assembly_off','!=', $forassembly)
                            ->select(DB::raw("DATE_FORMAT(personnel.dateofbirth,'%d/%m/%Y') as dateofbirth"),''.$session_personnela.'.personcd','district.district','address1','address2','postoffice','pin'
                                    ,'policestation.policestation','subdivision.subdivision','personnel.officecd','personnel.officer_name','personnel.off_desg','personnel.present_addr1','personnel.present_addr2'
                                    ,'personnel.assembly_temp as pre_ass_cd','ass_pre.assemblyname AS pre_ass', 'personnel.assembly_perm as per_ass_cd','ass_per.assemblyname AS per_ass', 'personnel.assembly_off as post_ass_cd','ass_ofc.assemblyname AS post_ass'
                                    ,'poststat.poststatus',''.$session_personnela.'.poststat',''.$session_personnela.'.booked','personnel.gender','personnel.epic','forassembly','forzone','groupid','personnel.subdivisioncd','1stTrainingSch')
                            ->inRandomOrder()
                            ->take(1)->get();
                if(count($filtered)>0){
                   foreach($filtered as $rwfilter){
//                       if($rwfilter->booked=="C" || $rwfilter->booked==""){
//                          $resMessage="Not Available for Selected Operation";
//                          $status = 0;
//                       }else{
                           $fst='1stTrainingSch';
                            $resMessage.="<table width='100%' style='font-family: verdana,arial,sans-serif;font-size: 11px;color: #000;'>\n";
                            $resMessage.="<tr><td align='left'>Name: </td><td align='left' colspan='3'>".$rwfilter->officer_name."</td></tr>\n";
                            $resMessage.="<tr><td align='left'>Office ID: </td><td align='left' colspan='3'>".$rwfilter->officecd."</td></tr>\n";
                            $resMessage.="<tr><td align='left'>Designation: </td><td align='left' colspan='3'>".$rwfilter->off_desg."</td></tr>\n";
                            $resMessage.="<tr><td align='left'>Office Address: </td><td align='left' colspan='3'>".$rwfilter->address1.",".$rwfilter->address2.", PO-".$rwfilter->postoffice.", PS-".$rwfilter->policestation.", Subdiv-".$rwfilter->subdivision.", Dist.-".$rwfilter->district.",".$rwfilter->pin."</td></tr>\n";
                            $resMessage.="<tr><td align='left'>Date of Birth: </td><td align='left'>".$rwfilter->dateofbirth."</td><td align='left'>Gender: </td><td align='left'>".$rwfilter->gender."<hidden id='hid_gender' name='hid_gender' style='display:none;'>".$rwfilter->gender."</hidden></td></tr>\n";
                            $resMessage.="<tr><td align='left'>EPIC No: </td><td align='left'>".$rwfilter->epic."</td><td align='left'>Posting Status: </td><td align='left'>".$rwfilter->poststatus."<hidden id='hid_post_stat' name='hid_post_stat' style='display:none;'>".$rwfilter->poststat."</hidden></td></tr>\n";
                            $resMessage.="<tr><td align='left'>Present Address: </td><td align='left' colspan='3'>".$rwfilter->present_addr1.", ".$rwfilter->present_addr2."</td></tr>\n";
                            $resMessage.="<tr><td align='left' colspan='4'><b>Assembly of</b></td></tr>\n";
                            $resMessage.="<tr><td align='left' colspan='2'>Present Address: </td><td align='left' colspan='2'>".$rwfilter->pre_ass."<hidden id='hid_pre_ass' name='hid_pre_ass' style='display:none;'>".$rwfilter->pre_ass_cd."</hidden></td></tr>\n";
                            $resMessage.="<tr><td align='left' colspan='2'>Permanent Address: </td><td align='left' colspan='2'>".$rwfilter->per_ass."<hidden id='hid_per_ass' name='hid_per_ass' style='display:none;'>".$rwfilter->per_ass_cd."</hidden></td></tr>\n";
                            $resMessage.="<tr><td align='left' colspan='2'>Place of Posting: </td><td align='left' colspan='2'>".$rwfilter->post_ass."<hidden id='hid_post_ass' name='hid_post_ass' style='display:none;'>".$rwfilter->post_ass_cd."</hidden></td></tr>\n";
                            $resMessage.="<tr><td align='left' colspan='4'><hidden id='hid_forassembly' name='hid_forassembly' style='display:none;'>".$rwfilter->forassembly."</hidden>\n<hidden id='hid_groupid' name='hid_groupid' style='display:none;'>".$rwfilter->groupid."</hidden>\n<hidden id='hid_booked' name='hid_booked' style='display:none;'>".$rwfilter->booked."</hidden>\n<hidden id='hid_per_cd' name='hid_per_cd' style='display:none;'>".$rwfilter->personcd."</hidden>\n <hidden id='hid_for_zone' name='hid_for_zone' style='display:none;'>".$rwfilter->forzone."</hidden>\n <hidden id='hid_dcrccd' name='hid_dcrccd' style='display:none;'>".$rwfilter->dcrccd."</hidden>\n <hidden id='hid_sub_div' name='hid_sub_div' style='display:none;'>".$rwfilter->subdivisioncd."</hidden>\n <hidden id='hid_ofc_cd' name='hid_ofc_cd' style='display:none;'>".$rwfilter->officecd."</hidden>\n <hidden id='hid_training1_sch' name='hid_training1_sch' style='display:none;'>".$rwfilter->$fst."</hidden></td></tr>\n";

                            $resMessage.="<tr><td align='right' colspan='2'>Booked : </td><td colspan='2' align='left' id='n_booked'>No</td></tr>\n";
                            $resMessage.="</table>";
                            $pid = $rwfilter->personcd;
                             $poststat = $rwfilter->poststat;
                            $status = 1;
                       //}
                            
                           
                   }                    
                }else{
                   $pid = "";
                   $resMessage="No records found"; 
                   $status = 0;
                }

                $response = array(
                   'options' => $resMessage,
                   'perid' =>$pid,
                    'poststat'=>$poststat,
                   'status' => $status
                );
            }catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
              $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
        
    }
    
    public function reserveReplaceNewPPData(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        }
        else
        {
           $this->validate($request, [            
                'zone' => 'required|alpha_num|min:4|max:4',
                'forassembly' => 'required|alpha_num|min:3|max:3',
                'ofc_cd' => 'required|alpha_num|min:10|max:10',
                'booked' => 'required|alpha|min:1|max:1',
                'post_stat' => 'required|alpha_num|min:2|max:2',
                'gender' => 'required|alpha|min:1|max:1',
                'groupid' => 'required|digits_between:1,4',
                'personnel_id' => 'required|alpha_num|min:11|max:11',
                'newpersonnel_id' => 'required|alpha_num|min:11|max:11',
                'dcrccd' => 'required|alpha_num|min:6|max:6',
                'training2_sch' => 'required|alpha_num|min:12|max:12',
                'no_of_member' => 'required|digits_between:1,1',
                'phase' => 'required|digits_between:1,1|min:1|max:1'
            ], [
            
                'zone.required' => 'Zone is required',
                'zone.alpha_num' => 'Zone must be an alpha numeric',
                'forassembly.required' => 'Forassembly is required',
                'forassembly.alpha_num' => 'Forassembly must be an alpha numeric',
                'ofc_cd.required' => 'Office ID is required',
                'ofc_cd.alpha_num' => 'Office ID must be an alpha numeric',
                'booked.required' => 'Booked is required',
                'booked.alpha' => 'Booked must be an alpha',
                'post_stat.required' => 'Post Status is required',
                'post_stat.alpha_num' => 'Post Status must be an alpha numeric',
                'gender.required' => 'Gender is required',
                'gender.alpha' => 'Gender must be an alpha',
                'groupid.required' => 'Polling Party No is required',
                'groupid.digits_between' => 'Polling Party No is integer',
                'personnel_id.required' => 'OLD Personnel ID is required',
                'personnel_id.alpha_num' => 'OLD Personnel ID must be an alpha numeric',
                'newpersonnel_id.required' => 'NEW Personnel ID is required',
                'newpersonnel_id.alpha_num' => 'NEW Personnel ID must be an alpha numeric',
                'dcrccd.required' => 'DCRC is required',
                'dcrccd.alpha_num' => 'DCRC must be an alpha numeric',
                'training2_sch.required' => 'OlD Training Schedule is required',
                'training2_sch.alpha_num' => 'OlD Training Schedule must be an alpha numeric',
                'no_of_member.required' => 'Number of member is required',
                'phase.required' => 'Phase is required',
                'phase.digits_between'=>'Phase must be an integer'
            ]);
            
            try
            {
                $zone=$request->zone;
                $phase=$request->phase;
                // dd($phase);
                $forassembly=$request->forassembly;
                $ofc_id=$request->ofc_cd;
                $no_of_member=$request->no_of_member;
                $booked=$request->booked;
                $post_stat=$request->post_stat;
                $gender=$request->gender;
                $oldpersonnel_id=$request->personnel_id;
                $newpersonnel_id=$request->newpersonnel_id;
                $partyno=$request->groupid;
                $dcrccd=$request->dcrccd;
                $oldtraining_sch=$request->training2_sch;
                
                $filtered=""; 
                $resMessage="";
               // $tbl_personnela1 = new tbl_personnela();             
                $session_personnela=session()->get('personnela_ppds');
                
                $duplicate_id = tbl_replacement_log_reserve::where('new_personnel','=', $newpersonnel_id)
                                                      ->value(DB::raw("count(*)"));
                                               
                if($duplicate_id == 0){
                     $tbl_personnela=new tbl_personnela; 

                    $user_code=session()->get("code_ppds");
                    $tbl_replacement_log = new tbl_replacement_log_reserve();
                    $tbl_replacement_log->old_personnel = $oldpersonnel_id;
                    $tbl_replacement_log->new_personnel = $newpersonnel_id;
                    $tbl_replacement_log->phase_id = $phase;
                    $tbl_replacement_log->assemblycd = $forassembly;
                    $tbl_replacement_log->groupid = $partyno;
                    $tbl_replacement_log->usercode = $user_code;
                    $tbl_replacement_log->save();
                    
                    tbl_personnela::where('forzone','=',$zone)
                        ->where('phase','=',$phase)
                         ->where('personcd','=',$oldpersonnel_id)
                         ->update(['booked'=>'C','selected'=>'0','2ndTrainingSch'=>null,'dcrccd'=>'','forassembly'=>'','groupid'=>'0']); 
                    tbl_personnela::where('forzone','=',$zone)
                         ->where('phase','=',$phase)
                         ->where('personcd','=',$newpersonnel_id)
                         ->update(['booked'=>$booked,'no_of_member'=>$no_of_member,'forassembly'=>$forassembly,'selected'=>'1','2ndTrainingSch'=>$oldtraining_sch,'dcrccd'=>$dcrccd,'groupid'=>$partyno]);
//                    tbl_second_rand_table::where('per_poststat', '=', $post_stat)
//                             ->where('groupid', '=', $partyno)
//                             ->where('assembly', '=', $forassembly)
//                             ->where('party_reserve', '=', 'R')
//                             ->delete();
                    DB::select('call second_replace_reserve(?,?,?,?,?)',[$post_stat,$booked,$forassembly,$partyno,$session_personnela]);
                    
                    $resMessage = "NEW Personnel ID [".$newpersonnel_id."] has been replaced successfuly";
                    $status = 1;
                }else{
                    $resMessage = "NEW Personnel ID already replaced.Please try again";
                    $status = 0;
                }

                $response = array(
                   'options' => $resMessage,
                   'status' => $status
                );
            }catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
              $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
        
    }
      
    //:::::::::::::::::::::::: for Reserve Cancellation :::::::::::::://
    public function reserveCancelPPData(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        }
        else
        {
           $this->validate($request, [            
                'personnel_id' => 'required|alpha_num|max:11|min:11',
                'zone' => 'required|alpha_num|min:4|max:4',
                'forassembly' => 'required|alpha_num|min:3|max:3',
                'post_stat' => 'required|alpha_num|min:2|max:2',
                'groupid' => 'required|digits_between:1,4',
                'reason' => 'required|regex:/^[A-Za-z0-9\s,-.]+$/i|max:30',
                'phase' => 'required|digits_between:1,1|min:1|max:1'
            ], [           
                'personnel_id.required' => 'OLD Personnel ID is required',
                'personnel_id.alpha_num' => 'OLD Personnel ID must be an alpha numeric',
                'zone.required' => 'Zone is required',
                'zone.alpha_num' => 'Zone must be an alpha numeric',
                'forassembly.required' => 'Forassembly is required',
                'forassembly.alpha_num' => 'Forassembly must be an alpha numeric',
                'post_stat.required' => 'Post Status is required',
                'post_stat.alpha_num' => 'Post Status must be an alpha numeric',
                'groupid.required' => 'Polling Party No is required',
                'groupid.digits_between' => 'Polling Party No is integer',
                 'reason.required' => 'Reason is required',
                'reason.regex' => 'Reason must not be special characters',
                'reason.max' => 'Reason must be 30 characters',
                'phase.required' => 'Phase is required',
                'phase.digits_between'=>'Phase must be an integer',
            ]);
            
            try
            {
                $oldpersonnel_id=$request->personnel_id;
                $zone=$request->zone;
                $phase=$request->phase;
                $forassembly=$request->forassembly;
                $post_stat=$request->post_stat;
                $groupid=$request->groupid;
                $reason=$request->reason;
                $filtered=""; 
                $resMessage="";
               // $tbl_personnela1 = new tbl_personnela();             
                $session_personnela=session()->get('personnela_ppds');
                
                 $user_code=session()->get("code_ppds");  
                tbl_personnela::where('personcd','=',$oldpersonnel_id)
                          ->where('forzone','=',$zone)
                          ->where('phase','=',$phase)
                         ->update(['booked'=>'C','selected'=>'0','2ndTrainingSch'=>null,'usercode'=>$user_code,'dcrccd'=>'','forassembly'=>'','groupid'=>'0']);
                tbl_second_rand_table::where('per_poststat', '=', $post_stat)
                             ->where('groupid', '=', $groupid)
                             ->where('assembly', '=', $forassembly)
                             ->where('phase','=',$phase)
                             ->where('party_reserve', '=', 'R')
                             ->delete();
                $tbl_cancellation_log_reserve = new tbl_cancellation_log_reserve();
                $tbl_cancellation_log_reserve->personnel_id = $oldpersonnel_id;
                $tbl_cancellation_log_reserve->forzone = $zone;
                $tbl_cancellation_log_reserve->phase_id = $phase;
                $tbl_cancellation_log_reserve->reason = $reason;
                $tbl_cancellation_log_reserve->usercode = $user_code;
                $tbl_cancellation_log_reserve->save();
                
                $resMessage = "Personnel ID has been cancelled successfuly";
                $status = 1;
                $response = array(
                   'options' => $resMessage,
                   'status' => $status
                );
            }catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
              $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
        
    }
}
