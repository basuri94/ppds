<?php

namespace App\Http\Controllers;

use App\tbl_assembly;
use App\tbl_assembly_zone;
use App\tbl_personnela;
use App\tbl_poststat;
use App\tbl_second_rand_table;
use App\tbl_second_training_schedule;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class GroupTrCheckingController extends Controller
{
    public function get_subdivison_wise_grp_tr_allocation(Request $request)
    {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            // $this->validate($request, [
            //     'zone' => 'required|alpha_num|min:4|max:4',
            //     'phase' => 'nullable|digits_between:1,1'
            // ], [
            //     'zone.required' => 'Zone is required',
            //     'zone.alpha_num' => 'Zone must be an alpha numeric characters',
            //     'phase.digits_between' => 'Phase must be integer'
            // ]);
            try {

                $phase = $request->phase;
                $subdivision = $request->subdivision;
                $assembly = $request->assembly;
               
                $requirement = "";
                $finaltotalfirstp1assign =0;
                $finaltotalfirstp2assign =0;
                $finaltotalfirstp3assign =0;
                $finaltotalfirstprassign =0;
              

                $session_personnela = session()->get('personnela_ppds');
                $tbl_personnela = new tbl_personnela();
                $records = $tbl_personnela->join('office', '' . $session_personnela . '.officecd', '=', 'office.officecd')
                    ->join('subdivision', 'office.subdivisioncd', '=', 'subdivision.subdivisioncd')
                    
                    ->join('zone', '' . $session_personnela . '.forzone', '=', 'zone.zone')
                   
                    ->groupBy('subdivision.subdivisioncd')
                    ->select('zone.zonename', 'subdivision.subdivision', 'subdivision.subdivisioncd');
                // dd($records);
              if($subdivision!=''){
                $records = $records->where('subdivision.subdivisioncd',$subdivision);
              }
              $records =  $records->get();
                foreach ($records as $recordspp) {
               
                    $totalp1firstassign = 0;
                    $totalp2firstassign = 0;
                    $totalp3firstassign = 0;
                    $totalprfirstassign = 0;
    
                    $totalp1secondassign = 0;
                    $totalp2secondassign = 0;
                    $totalp3secondassign = 0;
                    $totalprsecondassign = 0;
    
                //     $nestedData['Zone'] = $recordspp->zonename;
                    $nestedData['Subdivision'] = $recordspp->subdivision;

                       


                    $rsPostStat = tbl_poststat::orderBy('post_stat')
                        ->select('post_stat')->get();
                    foreach ($rsPostStat as $rowPstat) {
                        
                        $totalPostAssignPP = tbl_second_training_schedule::where('phase_id', '=', $phase)
                                                ->where('subdivision', '=', $recordspp->subdivisioncd)
                                                ->where('assembly', '=', $assembly)
                                                ->where('party_reserve', '=', 'P');
                                                
                            if($rowPstat->post_stat=='P1'){
                                $totalPostAssignPP = $totalPostAssignPP->sum('no_of_rsvP1');
                            } 
                            
                            if($rowPstat->post_stat=='P2'){
                                $totalPostAssignPP = $totalPostAssignPP->sum('no_of_rsvP2');
                            } 
                            if($rowPstat->post_stat=='P3'){
                                $totalPostAssignPP = $totalPostAssignPP->sum('no_of_rsvP3');
                            } 
                            if($rowPstat->post_stat=='PR'){
                                $totalPostAssignPP = $totalPostAssignPP->sum('no_of_rsvPR');
                            }  

                        
                            $totalPostAssignPP_R = tbl_second_training_schedule::where('phase_id', '=', $phase)
                            ->where('subdivision', '=', $recordspp->subdivisioncd)
                            ->where('assembly', '=', $assembly)
                            ->where('party_reserve', '=', 'R');
                            
                                                  
                            if($rowPstat->post_stat=='P1'){
                                $totalPostAssignPP_R = $totalPostAssignPP_R->sum('no_of_rsvP1');
                            } 
                            
                            if($rowPstat->post_stat=='P2'){
                                $totalPostAssignPP_R = $totalPostAssignPP_R->sum('no_of_rsvP2');
                            } 
                            if($rowPstat->post_stat=='P3'){
                                $totalPostAssignPP_R = $totalPostAssignPP_R->sum('no_of_rsvP3');
                            } 
                            if($rowPstat->post_stat=='PR'){
                                $totalPostAssignPP_R = $totalPostAssignPP_R->sum('no_of_rsvPR');
                            }  






                        if ($rowPstat->post_stat == 'P1') {
                            
                            $totalp1firstassign = $totalp1firstassign + $totalPostAssignPP;
                            $totalp1secondassign = $totalp1secondassign + $totalPostAssignPP_R;

                            $total_p1 = $totalp1firstassign+$totalp1secondassign;

                        } else if ($rowPstat->post_stat == 'P2') {
                           
                            $totalp2firstassign = $totalp2firstassign + $totalPostAssignPP;
                            $totalp2secondassign = $totalp2secondassign + $totalPostAssignPP_R;
                            $total_p2 = $totalp2firstassign+$totalp2secondassign;
                           
                        } else if ($rowPstat->post_stat == 'P3') {
                           
                            $totalp3firstassign = $totalp3firstassign + $totalPostAssignPP;
                            $totalp3secondassign = $totalp3secondassign + $totalPostAssignPP_R;
                            $total_p3 = $totalp3firstassign+$totalp3secondassign;
                           
                        } else if ($rowPstat->post_stat == 'PR') {
                           
                            $totalprfirstassign = $totalprfirstassign + $totalPostAssignPP;
                            $totalprsecondassign = $totalprsecondassign + $totalPostAssignPP_R;
                            $total_pR = $totalprfirstassign+$totalprsecondassign;
                            
                        }
               
                 }


                    $requirement .= '<tr>';
                    $requirement .= '<td align="left" style="width: 15%;">&nbsp;' . $recordspp->subdivision . '</td>';

               


                    $requirement .= "<td>" . $totalp1firstassign . '+' . $totalp1secondassign .'='.$total_p1.'</td>';
                    $requirement .= "<td>" . $totalp2firstassign . '+' . $totalp2secondassign .'='.$total_p2.'</td>';
                    $requirement .= "<td>" . $totalp3firstassign . '+' . $totalp3secondassign .'='.$total_p3.'</td>';
                    $requirement .= "<td>" . $totalprfirstassign . '+' . $totalprsecondassign .'='.$total_pR.'</td>';

                //     $requirement .= "<td>" . $totalp1secondassign . '</td>';
                //     $requirement .= "<td>" . $totalp2secondassign . '</td>';
                //     $requirement .= "<td>" . $totalp3secondassign . '</td>';
                //     $requirement .= "<td>" . $totalprsecondassign . '</td>';

                    $finaltotalfirstp1assign +=$total_p1;
                    $finaltotalfirstp2assign +=$total_p2;
                    $finaltotalfirstp3assign +=$total_p3;
                    $finaltotalfirstprassign +=$total_pR;

                //     $finaltotalsecondp1assign +=$totalp1secondassign;
                //     $finaltotalsecondp2assign +=$totalp2secondassign;
                //     $finaltotalsecondp3assign +=$totalp3secondassign;
                //     $finaltotalsecondprassign +=$totalprsecondassign;
                    
                //     $finaltotalp1 +=$totalp1;
                //     $finaltotalp2 +=$totalp2;
                //     $finaltotalp3 +=$totalp3;
                //     $finaltotalpr +=$totalpr;
                   
                    $requirement .= '</tr>';
                }
                $requirement .= '<td colspan="1">Total</td><td>'.$finaltotalfirstp1assign.'</td><td>'.$finaltotalfirstp2assign.'</td><td>'.$finaltotalfirstp3assign.'</td><td>'.$finaltotalfirstprassign.'</td>';
                // $requirement .='<td>'. $finaltotalfirstp1assign.'</td><td>'. $finaltotalfirstp2assign.'</td><td>'. $finaltotalfirstp3assign.'</td><td>'. $finaltotalfirstprassign.'</td>';
                // $requirement .= '<td>'.$finaltotalsecondp1assign.'</td><td>'.$finaltotalsecondp2assign.'</td><td>'.$finaltotalsecondp3assign.'</td><td>'.$finaltotalsecondprassign.'</td>';


                $response = array(
                    'requirement' => $requirement,
                    'status' => 1
                );
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }



    public function group_tr_after_populate(Request $request)
    {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            // $this->validate($request, [
            //     'zone' => 'required|alpha_num|min:4|max:4',
            //     'phase' => 'nullable|digits_between:1,1'
            // ], [
            //     'zone.required' => 'Zone is required',
            //     'zone.alpha_num' => 'Zone must be an alpha numeric characters',
            //     'phase.digits_between' => 'Phase must be integer'
            // ]);
            try {
                $zone = $request->zone;
                $phase = $request->phase;
                // $subdivision = $request->subdivision;
                $assembly = $request->assembly;
               
                $requirement = "";
                $finaltotalfirstp1assign =0;
                $finaltotalfirstp2assign =0;
                $finaltotalfirstp3assign =0;
                $finaltotalfirstprassign =0;



                // $assemblydata=tbl_assembly::select('assemblyname','assemblycd')->where('districtcd',session()->get('districtcd_ppds'))->get();
                // dd($assemblydata);


                $filtered = tbl_assembly_zone::join('assembly', 'assembly.assemblycd', '=', 'assembly_zone.assemblycd')
                ->join('assembly_party', 'assembly_party.assemblycd', '=', 'assembly.assemblycd')
                ->join('reserve', function($join) {
                    $join->on('reserve.forassembly', '=', 'assembly_party.assemblycd');
                    $join->on('reserve.number_of_member', '=', 'assembly_party.no_of_member');
                    $join->on('reserve.gender', '=', 'assembly_party.gender');
                })->select('assembly_zone.assemblycd', 'assembly_zone.rand_status2', 'assembly.assemblyname', DB::raw("SUM(no_party) AS P"), DB::raw("SUM(case when no_or_pc ='P' then ROUND(no_party * numb/100) ELSE numb end) AS R"));
// if ($zone != "") {
//     $filtered = $filtered
//             ->where('assembly_zone.districtcd', '=', $forDist)
//             ->where('assembly_zone.zone', '=', $zone)
//             ->where('assembly_zone.rand_status2', '!=', 'Y')
//             ->where('assembly_zone.rand_status1', '=', 'Y');
// }
if ($phase != "") {
    $filtered = $filtered->where('assembly_zone.phase_id', '=', $phase);
}
$filtered = $filtered->groupBy('assembly_zone.assemblycd')->get();

                foreach ($filtered as $assembly) {

               
                    $totalp1firstassign = 0;
                    $totalp2firstassign = 0;
                    $totalp3firstassign = 0;
                    $totalprfirstassign = 0;
    
                    $totalp1secondassign = 0;
                    $totalp2secondassign = 0;
                    $totalp3secondassign = 0;
                    $totalprsecondassign = 0;
// 
                        $totalP = 0;
                        $totalR = 0;


                    $nestedData['assemblycd'] = $assembly->assemblycd;
                    $nestedData['assemblyname'] = $assembly->assemblyname;

                    $rsPostStat = tbl_poststat::orderBy('post_stat')
                    ->select('post_stat')->get();
                    foreach ($rsPostStat as $rowPstat) {
                        $records = tbl_second_rand_table::where('forzone',$zone)
                        ->where('phase',$phase)
                        ->where('assembly',$assembly->assemblycd)
                        ->where('party_reserve','P')
                        ->whereNotNull('training_date')
                        ->whereNotNull('training_time');
                        // ->select('per_poststat',DB::raw('Count(*) as cnt'))
                        // ->groupBy('per_poststat');

                        if($rowPstat->post_stat=='P1'){
                            $records = $records->where('per_poststat',$rowPstat->post_stat)->count('*');
                        }
                        if($rowPstat->post_stat=='P2'){
                            $records = $records->where('per_poststat',$rowPstat->post_stat)->count('*');
                        }
                        if($rowPstat->post_stat=='P3'){
                            $records = $records->where('per_poststat',$rowPstat->post_stat)->count('*');
                        }
                        if($rowPstat->post_stat=='PR'){
                            $records = $records->where('per_poststat',$rowPstat->post_stat)->count('*');
                        }




                        $records1 = tbl_second_rand_table::where('forzone',$zone)
                        ->where('phase',$phase)
                        ->where('assembly',$assembly->assemblycd)
                        ->where('party_reserve','R')
                        ->whereNotNull('training_date')
                        ->whereNotNull('training_time');




                        if($rowPstat->post_stat=='P1'){
                            $records1 = $records1->where('per_poststat',$rowPstat->post_stat)->count('*');
                        }
                        if($rowPstat->post_stat=='P2'){
                            $records1 = $records1->where('per_poststat',$rowPstat->post_stat)->count('*');
                        }
                        if($rowPstat->post_stat=='P3'){
                            $records1 = $records1->where('per_poststat',$rowPstat->post_stat)->count('*');
                        }
                        if($rowPstat->post_stat=='PR'){
                            $records1 = $records1->where('per_poststat',$rowPstat->post_stat)->count('*');
                        }





                        if ($rowPstat->post_stat == 'P1') {
                            
                            $totalp1firstassign = $totalp1firstassign + $records;
                            $totalp1secondassign = $totalp1secondassign + $records1;

                            // $total_p1 = $totalp1firstassign+$totalp1secondassign;

                        } else if ($rowPstat->post_stat == 'P2') {
                           
                            $totalp2firstassign = $totalp2firstassign + $records;
                            $totalp2secondassign = $totalp2secondassign + $records1;
                            // $total_p2 = $totalp2firstassign+$totalp2secondassign;
                           
                        } else if ($rowPstat->post_stat == 'P3') {
                           
                            $totalp3firstassign = $totalp3firstassign + $records;
                            $totalp3secondassign = $totalp3secondassign + $records1;
                            // $total_p3 = $totalp3firstassign+$totalp3secondassign;
                           
                        } else if ($rowPstat->post_stat == 'PR') {
                           
                            $totalprfirstassign = $totalprfirstassign + $records;
                            $totalprsecondassign = $totalprsecondassign + $records1;
                            // $total_pR = $totalprfirstassign+$totalprsecondassign;
                            
                        }


                        $totalP = $totalp1firstassign + $totalp2firstassign + $totalp3firstassign + $totalprfirstassign;
                        $totalR = $totalp1secondassign + $totalp2secondassign + $totalp3secondassign + $totalprsecondassign;
                        $total = $totalP + $totalR;
                    }

                    $requirement .= '<tr>';
                    $requirement .= '<td align="left" style="width: 15%;">&nbsp;' .$assembly->assemblycd . '-' .$assembly->assemblyname . '</td>';

               


                    $requirement .= "<td>" . $totalp1firstassign . '+'.$totalp1secondassign.'</td>';
                    $requirement .= "<td>" . $totalp2firstassign . '+'.$totalp2secondassign. '</td>';
                    $requirement .= "<td>" . $totalp3firstassign . '+'.$totalp3secondassign. '</td>';
                    $requirement .= "<td>" . $totalprfirstassign . '+'.$totalprsecondassign. '</td>';

                    $requirement .= "<td>" . $totalP . '+' .$totalR. '='.$total .'</td>';
                   
                    $requirement .= '</tr>';

                    
                }





               
                $response = array(
                    'requirement' => $requirement,
                    'status' => 1
                );
             
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }


    public function getAfterPopulateRecordExcel(Request $request) {

        // $this->validate($request, [
        //     'districtcd' => 'required|alpha_num|min:2|max:2',
        //     'subdivision' => 'nullable|alpha_num|min:4|max:4',
        //     'blockMunicipality' => 'nullable|alpha_num|min:6|max:6',
        //     'officeName' => 'nullable|alpha_num|min:10|max:10',
        //     'PersonnelId' => 'nullable|alpha_num|min:11|max:11',
        //     'trainingtype' => 'nullable|digits_between:1,1',
        //     'phase' => 'nullable|digits_between:1,1',
        //     'from' => 'required|digits_between:1,4',
        //     'to' => 'required|digits_between:1,4'
        //         ], [
        //     'districtcd.required' => 'District is required',
        //     'districtcd.alpha_num' => 'District code must be an alpha numeric',
        //     'subdivision.alpha_num' => 'Subdivision must be an alpha numeric',
        //     'blockMunicipality.alpha_num' => 'Block/Municipality must be an alpha numeric',
        //     'officeName.alpha_num' => 'Office Code must be an alpha numeric',
        //     'PersonnelId.alpha_num' => 'Personnel ID must be an alpha numeric',
        //     'trainingtype.digits_between' => 'Training Head must be an integer',
        //     'from.required' => 'Records From is required',
        //     'from.digits_between' => 'Records From must be an integer',
        //     'to.required' => 'Records To is required',
        //     'to.digits_between' => 'Records To must be an integer',
        //     'phase.digits_between' => 'Phase must be integer'
        // ]);


        try {
            $zone = $request->zone;
            $phase = $request->phase;
            $data = [];
            $ApptRecord = tbl_second_rand_table::where('forzone',$zone)
                                            ->where('phase',$phase)
                                            // ->where('party_reserve','P')
                                            ->whereNotNull('training_date')
                                            ->whereNotNull('training_time')
                                            ->select('assembly','assembly_name','per_poststat',DB::raw('COUNT(*) as cnt'))
                                            ->groupBy('per_poststat','assembly')->get();
            foreach ($ApptRecord as $ApptDetail) {
                $nestedData['Assembly Code'] = $ApptDetail->assembly;
                $nestedData['Assembly Name'] = $ApptDetail->assembly_name;
                $nestedData['Poststat'] = $ApptDetail->per_poststat;
                $nestedData['Count'] = $ApptDetail->cnt;
                $data[] = $nestedData;
            }

            return \Excel::create('1stApptExcel', function($excel) use ($data) {
                        $excel->sheet(' ', function($sheet) use ($data) {
                            $sheet->fromArray($data);
                        });
                    })->download('xlsx');
        } catch (\Exception $e) {
            $response = array(
                'exception' => true,
                'exception_message' => $e->getMessage(),
            );
            // $statusCode = 400;
            return $response;
        }
    }


}
