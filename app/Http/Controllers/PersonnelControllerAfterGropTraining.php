<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Personnel;
use App\tbl_personnela;
use DB;

class PersonnelControllerAfterGropTraining extends Controller {

    public function getPersonnelDetailsAfterGroupTraining(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            $this->validate($request, [
                'personnel_id' => 'required|alpha_num|max:11|min:11',
                    ], [
                'personnel_id.required' => 'Personnel ID is required',
                'personnel_id.alpha_num' => 'Personnel ID must be an alpha numeric',
            ]);
            $personnel_id = $request->personnel_id;

            try {
                $filtered = "";
                $resMessage = "";
                $tbl_personnel = new Personnel();
                $session_personnela = session()->get('personnela_ppds');
                $filtered = $tbl_personnel::where('personcd', '=', $personnel_id)
                                ->groupBy('personnel.personcd')
                                ->select('personnel.officer_name', 'personnel.branch_ifsc', 'personnel.bank_acc_no', 'personnel.mob_no','personnel.poststat')->get();
                $data = array();
                if (count($filtered) > 0) {
                    $status = 1;
                    foreach ($filtered as $rwfilter) {
                        $nestedData['officer_name'] = $rwfilter->officer_name;
                        $nestedData['branch_ifsc'] = $rwfilter->branch_ifsc;
                        $nestedData['bank_acc_no'] = $rwfilter->bank_acc_no;
                        $nestedData['mob_no'] = $rwfilter->mob_no;
                         $nestedData['poststat'] = $rwfilter->poststat;
                        $data[] = $nestedData;
                    }
                } else {
                    $data = "No records found";
                    $status = 0;
                }

                $response = array(
                    'options' => $data,
                    'status' => $status
                );
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }

    public function update_personnel_new(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            $personnelId = $request->personnel_id;
            $this->validate($request, [
                'officer_name' => 'required|string|max:50',
                'mob_no' => 'required|digits:10',
                'branch_ifsc' => 'required|max:11',
                'bank_acc_no' => 'required|unique:personnel,bank_acc_no,' . $personnelId . ',personcd|max:16',
                'personnel_id' => 'required|alpha_num|max:11|min:11',
                'poststat_personnel'=>'required|alpha_num|max:2|min:2'
                    ], [
                'personnel_id.required' => 'Personnel ID is required',
                'personnel_id.alpha_num' => 'Personnel ID must be an alpha numeric',
                'officer_name.required' => 'Officer Name is required',
                'officer_name.string' => 'Officer Name should be alphabatical',
                'mob_no.required' => 'Mobile No is required',
                'mob_no.digits' => 'Mobile No should be 10 digits only',
                'branch_ifsc.required' => 'Branch IFSC is required',
                'branch_ifsc.max' => 'Branch IFSC should be 11 characters only',
                'bank_acc_no.required' => 'Bank Account No is required',
                'bank_acc_no.unique' => 'Bank Account No has been taken already',
                'bank_acc_no.max' => 'Bank Account No should not be more than 16 characters',
                        'poststat_personnel.required'=>'Poststat is required',
                        'poststat_personnel.alpha_num'=>'Poststat should be alphabatic',
                        
            ]);

            try {




                $personnel = array();
                $personnel['officer_name'] = strip_tags($request->officer_name, '');
                $personnel['mob_no'] = strip_tags($request->mob_no, '');
                $personnel['branch_ifsc'] = strip_tags($request->branch_ifsc, '');
                $personnel['bank_acc_no'] = strip_tags($request->bank_acc_no, '');
               // $personnel['updated'] = 'Y';



                $scroll = array();

                $scroll['officer_name'] = strip_tags($request->officer_name, '');
                $scroll['mob_no'] = strip_tags($request->mob_no, '');
                $scroll['branch_ifsc'] = strip_tags($request->branch_ifsc, '');
                $scroll['bank_acc_no'] = strip_tags($request->bank_acc_no, '');
                
                $secondRand=array();
                $personCd="";
                $poststat_personnel=strtolower($request->poststat_personnel);
                if($request->poststat_personnel=="P1"){
                     $secondRand['p1_name'] = strip_tags($request->officer_name, '');
                $secondRand['p1_mobno'] = strip_tags($request->mob_no, '');
               
                }
                else if($request->poststat_personnel=="P2"){
                     $secondRand['p2_name'] = strip_tags($request->officer_name, '');
                $secondRand['p2_mobno'] = strip_tags($request->mob_no, '');
                }
                else if($request->poststat_personnel=="P3"){
                     $secondRand['p3_name'] = strip_tags($request->officer_name, '');
                $secondRand['p3_mobno'] = strip_tags($request->mob_no, '');
                }
                else if($request->poststat_personnel=="PR"){
                     $secondRand['pr_name'] = strip_tags($request->officer_name, '');
                $secondRand['pr_mobno'] = strip_tags($request->mob_no, '');
                }
                else if($request->poststat_personnel=="PA"){
                     $secondRand['pa_name'] = strip_tags($request->officer_name, '');
                $secondRand['pa_mobno'] = strip_tags($request->mob_no, '');
                }
                else if($request->poststat_personnel=="PB"){
                     $secondRand['pb_name'] = strip_tags($request->officer_name, '');
                $secondRand['pb_mobno'] = strip_tags($request->mob_no, '');
                }
              
                
                $updateSecondRandUpdate=DB::table('second_rand_table')->where($poststat_personnel.'_personcd', $personnelId)->update($secondRand);

                $personeelMainTable = DB::table('personnel')->where('personcd', $personnelId)->update($personnel);
                $updateScroll = DB::table('scroll')->where('personcd', $personnelId)->update($scroll);


                $response = array('personeelMainTable' => $personeelMainTable, 'updateScroll' => $updateScroll,
                    'status' => 1);
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage()
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }

    public function get_personnel_details(Request $request){
        $personnel_id=$request->personnel_id;
         $tbl_personnela = new tbl_personnela();
         $session_personnela = session()->get('personnela_ppds');
         $data=$tbl_personnela::where('personcd','=',$personnel_id)
                 ->select('groupid','forassembly','poststat','booked')->get(); 
  
     return $data;  
    }
}
