<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\tbl_assembly_zone;
use App\tbl_personnela;
use App\tbl_assembly_party;
use App\tbl_second_rand_table;
use App\tbl_second_training_schedule;
use App\tbl_assembly;
use App\tbl_poststatorder;
use App\tbl_block_muni;
use App\tbl_order_no_date;
use DB;

class SecondRandController extends Controller {

    //::::::::Second Randomisation(Lock/Unlock):::::::://
    public function getZonePCdata(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            $this->validate($request, [
                'zone' => 'required|alpha_num|min:4|max:4'
                    ], [
                'zone.required' => 'Zone is required',
                'zone.alpha_num' => 'Zone must be an alpha numeric characters'
            ]);
            $zone = $request->zone;
            try {
                $filtered = "";
                $filtered = tbl_assembly_zone::join('assembly', 'assembly.assemblycd', '=', 'assembly_zone.assemblycd')
                        ->join('pc', 'assembly.pccd', '=', 'pc.pccd')
                        ->where('assembly_zone.zone', '=', $zone)
                        ->groupBy('assembly.pccd')
                        ->pluck('pc.pcname', 'pc.pccd')
                        ->all();

                $response = array(
                    'options' => $filtered,
                    'status' => 1
                );
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }

    public function getZonePhasedata(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            $this->validate($request, [
                'zone' => 'required|alpha_num|min:4|max:4'
                    ], [
                'zone.required' => 'Zone is required',
                'zone.alpha_num' => 'Zone must be an alpha numeric characters'
            ]);
            $zone = $request->zone;
            try {
                $filtered = "";
                $filtered = tbl_assembly_zone::join('assembly', 'assembly.assemblycd', '=', 'assembly_zone.assemblycd')
                        ->join('phase', 'phase.code', '=', 'assembly_zone.phase_id')
                        ->where('assembly_zone.zone', '=', $zone)
                        ->groupBy('phase.code')
                        ->pluck('phase.name', 'phase.code')
                        ->all();

                $response = array(
                    'options' => $filtered,
                    'status' => 1
                );
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }
    public function getSubdivisionPhasedata(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            $this->validate($request, [
                'subdivision' => 'required|alpha_num|min:4|max:4'
                    ], [
                'subdivision.required' => 'Subdivision is required',
                'subdivision.alpha_num' => 'Subdivision must be an alpha numeric characters'
            ]);
            $subdivision = $request->subdivision;
            try {
                $filtered = "";
                $filtered = tbl_assembly_zone::join('assembly', 'assembly.assemblycd', '=', 'assembly_zone.assemblycd')
                        ->join('phase', 'phase.code', '=', 'assembly_zone.phase_id')
                        ->where('assembly_zone.subdivisioncd', '=', $subdivision)
                        ->groupBy('phase.code')
                        ->pluck('phase.name', 'phase.code')
                        ->all();

                $response = array(
                    'options' => $filtered,
                    'status' => 1
                );
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }
    public function secondzone_phase_wise_assembly(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            $this->validate($request, [
                'zone' => 'required|alpha_num|min:4|max:4',
                'forDist' => 'required|alpha_num|min:2|max:2',
                'phase' => 'nullable|integer'
                    ], [
                'zone.required' => 'Zone is required',
                'zone.alpha_num' => 'Zone must be an alpha numeric characters',
                'forDist.required' => 'District is required',
                'forDist.alpha_num' => 'District must be an alpha numeric characters',
                'phase.integer' => 'Phase must be an integer'
            ]);
            $zone = $request->zone;
            $forDist = $request->forDist;
            $phase = $request->phase;
            try {
                $filtered = "";
                $filtered = tbl_assembly_zone::join('assembly', 'assembly.assemblycd', '=', 'assembly_zone.assemblycd')
                        ->where('assembly_zone.districtcd', '=', $forDist)
                        ->where('assembly_zone.rand_status1', '=', 'Y')
                        ->select('assembly_zone.assemblycd', 'assembly_zone.rand_status1', 'assembly_zone.rand_status2', 'assembly_zone.rand_status3', 'assembly.assemblyname');

                if ($zone != "") {
                    $filtered = $filtered->where('assembly_zone.zone', '=', $zone);
                }
                if ($phase != "") {
                    $filtered = $filtered->where('assembly_zone.phase_id', '=', $phase);
                }
                $filtered = $filtered->groupBy('assembly_zone.assemblycd')->get();
              
                $response = array(
                    'options' => $filtered,
                    'status' => 1
                );
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }

    public function secondzone_wise_assembly(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            $this->validate($request, [
                'zone' => 'required|alpha_num|min:4|max:4',
                'forDist' => 'required|alpha_num|min:2|max:2',
                'pc' => 'nullable|alpha_num|min:2|max:2'
                    ], [
                'zone.required' => 'Zone is required',
                'zone.alpha_num' => 'Zone must be an alpha numeric characters',
                'forDist.required' => 'District is required',
                'forDist.alpha_num' => 'District must be an alpha numeric characters',
                'pc.alpha_num' => 'Pc must be an alpha numeric character'
            ]);
            $zone = $request->zone;
            $forDist = $request->forDist;
            $pccd = $request->pc;
            try {
                $filtered = "";
                $filtered = tbl_assembly_zone::join('assembly', 'assembly.assemblycd', '=', 'assembly_zone.assemblycd')
                        ->where('assembly_zone.districtcd', '=', $forDist)
                        ->where('assembly_zone.rand_status1', '=', 'Y')
                        ->select('assembly_zone.assemblycd', 'assembly_zone.rand_status1', 'assembly_zone.rand_status2', 'assembly_zone.rand_status3', 'assembly.assemblyname');

                if ($zone != "") {
                    $filtered = $filtered->where('assembly_zone.zone', '=', $zone);
                }
                if ($pccd != "") {
                    $filtered = $filtered->where('assembly.pccd', '=', $pccd);
                }
                $filtered = $filtered->groupBy('assembly_zone.assemblycd')->get();
                /* $data = array();
                  if (!empty($filtered)) {
                  foreach ($filtered as $post) {
                  $nestedData['assemblycd'] = $post->assemblycd;
                  $nestedData['assemblyname'] = $post->assemblyname;
                  $nestedData['rand_status1'] = $post->rand_status1;
                  $nestedData['status'] =  tbl_personnela::where('personnela.forassembly','=',$post->assemblycd)
                  ->where('personnela.forzone','=',$zone)
                  ->select(DB::raw('Count(personnela.personcd) as total'))
                  ->get();
                  $data[]=$nestedData;
                  }

                  } */
                // print_r($data);
                $response = array(
                    'options' => $filtered,
                    'status' => 1
                );
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }

    public function secondrand_lock_unlock(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            $validate_array = ['zone' => 'required|alpha_num|min:4|max:4', 'districtcd' => 'required|alpha_num|min:2|max:2'];
            for ($x = 0; $x <= $request->row_count; $x++) {
                $validate_array['myCheck' . $x] = 'nullable|alpha_dash';
            }
            $validate_array1 = ['zone.required' => 'Zone is required',
                'zone.alpha_num' => 'Zone must be an alpha numeric characters',
                'districtcd.required' => 'District is required',
                'districtcd.alpha_num' => 'District must be an alpha numeric characters'];
            for ($y = 0; $y <= $request->row_count; $y++) {
                $validate_array1['myCheck' . $y . '.alpha_dash'] = 'Assembly must be an alpha numeric characters';
                //  $validate_array1['myCheck'. $y.'.unique'] = 'Already created some assembly(s) for this zone';
            }
            $this->validate($request, $validate_array, $validate_array1);

            try {
                $row_count = $request->row_count;
                $zone = $request->zone;
//                $filtered0=tbl_assembly_zone::where('assembly_zone.zone','=',$zone)
//                         ->where('assembly_zone.rand_status2','!=','Y')
//                         ->update(['rand_status2'=>'N']);
                $count = 0;
                $totalCount = 0;
                for ($i = 1; $i <= $row_count; $i++) {
                    $asmcd = 'myCheck' . $i;
                    $count1 = 0;
                    if ($request->$asmcd != "") {
                        $asmstat = explode('-', $request->$asmcd);
                        $asmcode = $asmstat[0];
                        $asmstatus = $asmstat[1];
                        if ($asmstatus == "A") {
                            $randstat = 'Y';
                        } else if ($asmstatus == "Y") {
                            $randstat = 'N';
                        } else if ($asmstatus == "N") {
                            $count1++;
                            $totalCount++;
                        }
                        if ($count1 == 0) {
                            tbl_assembly_zone::where('assembly_zone.assemblycd', '=', $asmcode)
                                    ->where('assembly_zone.zone', '=', $zone)
                                    ->update(['rand_status2' => $randstat]);
                            $count++;
                        }
                    }
                }
                $response = array(
                    'options' => $count,
                    'totalC' => $totalCount,
                    'status' => 1
                );
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }

    //:::::::::::::::::Second Rand:::::::::::::::://
    public function second_rand_zone_wise_assembly(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            $this->validate($request, [
                'zone' => 'required|alpha_num|min:4|max:4',
                'forDist' => 'required|alpha_num|min:2|max:2',
                'pc' => 'nullable|alpha_num|min:2|max:2'
                    ], [
                'zone.required' => 'Zone is required',
                'zone.alpha_num' => 'Zone must be an alpha numeric characters',
                'forDist.required' => 'District is required',
                'forDist.alpha_num' => 'District must be an alpha numeric characters',
                'pc.required' => 'PC is required',
                'pc.alpha_num' => 'PC must be an alpha numeric characters'
            ]);
            $zone = $request->zone;
            $forDist = $request->forDist;
            $pccd = $request->pc;

            try {
                $filtered = tbl_assembly_zone::join('assembly', 'assembly.assemblycd', '=', 'assembly_zone.assemblycd')
                                ->join('assembly_party', 'assembly_party.assemblycd', '=', 'assembly.assemblycd')
                                ->join('reserve', function($join) {
                                    $join->on('reserve.forassembly', '=', 'assembly_party.assemblycd');
                                    $join->on('reserve.number_of_member', '=', 'assembly_party.no_of_member');
                                    $join->on('reserve.gender', '=', 'assembly_party.gender');
                                })->select('assembly_zone.assemblycd', 'assembly_zone.rand_status2', 'assembly.assemblyname', DB::raw("SUM(no_party) AS P"), DB::raw("SUM(case when no_or_pc ='P' then ROUND(no_party * numb/100) ELSE numb end) AS R"));
                if ($zone != "") {
                    $filtered = $filtered
                            ->where('assembly_zone.districtcd', '=', $forDist)
                            ->where('assembly_zone.zone', '=', $zone)
                            ->where('assembly_zone.rand_status2', '!=', 'Y')
                            ->where('assembly_zone.rand_status1', '=', 'Y');
                }
                if ($pccd != "") {
                    $filtered = $filtered->where('assembly.pccd', '=', $pccd);
                }
                $filtered = $filtered->groupBy('assembly_zone.assemblycd')->get();
                $data = array();
                if (!empty($filtered)) {
                    foreach ($filtered as $post) {
                        $nestedData['assemblycd'] = $post->assemblycd;
                        $nestedData['assemblyname'] = $post->assemblyname;
                        $nestedData['rand_status2'] = $post->rand_status2;
                        $toPARTY = $post->P;
                        $toRESERVE = $post->R;
                        $nestedData['totRES'] = $toRESERVE;
                        $nestedData['totPAR'] = $toPARTY;
                        $shortfallREQ = $toPARTY + $toRESERVE;
                        $partyReserve = tbl_personnela::where('forassembly', '=', $post->assemblycd)
                                ->where('forzone', '=', $zone)
                                ->where(function($q) {
                                    $q->orwhere('booked', '=', 'P')
                                    ->orwhere('booked', '=', 'R');
                                })
                                ->select('booked', 'dcrccd')
                                ->get();

                        $toparty = 0;
                        $toreserve = 0;
                        if (!empty($partyReserve)) {
                            foreach ($partyReserve as $paRes) {
                                if ($paRes->dcrccd != '') {
                                    if ($paRes->booked == 'P') {
                                        $toparty = $toparty + 1;
                                    } else {
                                        $toreserve = $toreserve + 1;
                                    }
                                }
                            }
                        }
                        $nestedData['toparty'] = $toparty;
                        $nestedData['toreserve'] = $toreserve;
                        $shortfallASS = $toparty + $toreserve;
                        $nestedData['shortfall'] = $shortfallREQ - $shortfallASS;
                        $data[] = $nestedData;
                    }
                }
                // print_r($data);
                $response = array(
                    'options' => $data,
                    'status' => 1
                );
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }

    public function second_rand_zone_phase_wise_assembly(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            $this->validate($request, [
                'zone' => 'required|alpha_num|min:4|max:4',
                'forDist' => 'required|alpha_num|min:2|max:2',
                'phase' => 'nullable|integer'
                    ], [
                'zone.required' => 'Zone is required',
                'zone.alpha_num' => 'Zone must be an alpha numeric characters',
                'forDist.required' => 'District is required',
                'forDist.alpha_num' => 'District must be an alpha numeric characters',
                'phase.required' => 'PC is required',
                'phase.integer' => 'Phase must be an integer'
            ]);
            $zone = $request->zone;
            $forDist = $request->forDist;
            $phase = $request->phase;

            try {
                $filtered = tbl_assembly_zone::join('assembly', 'assembly.assemblycd', '=', 'assembly_zone.assemblycd')
                                ->join('assembly_party', 'assembly_party.assemblycd', '=', 'assembly.assemblycd')
                                ->join('reserve', function($join) {
                                    $join->on('reserve.forassembly', '=', 'assembly_party.assemblycd');
                                    $join->on('reserve.number_of_member', '=', 'assembly_party.no_of_member');
                                    $join->on('reserve.gender', '=', 'assembly_party.gender');
                                })
                                // ->where('assembly_zone.districtcd', '=', $forDist)
                                // ->where('assembly_zone.rand_status2', '!=', 'Y')
                                // ->where('assembly_zone.rand_status1', '=', 'Y')
                                ->select('assembly_zone.assemblycd', 'assembly_zone.rand_status2', 'assembly.assemblyname', DB::raw("SUM(no_party) AS P"), DB::raw("SUM(case when no_or_pc ='P' then ROUND(no_party * numb/100) ELSE numb end) AS R"));
                if ($zone != "") {
                    $filtered = $filtered
                            ->where('assembly_zone.districtcd', '=', $forDist)
                            ->where('assembly_zone.zone', '=', $zone)
                            ->where('assembly_zone.rand_status2', '!=', 'Y')
                            ->where('assembly_zone.rand_status1', '=', 'Y');
                }
                
                // if ($zone != "") {
                //     $filtered = $filtered->where('assembly_zone.zone', '=', $zone);
                // }


                if ($phase != "") {
                    $filtered = $filtered->where('assembly_zone.phase_id', '=', $phase);
                }
                $filtered = $filtered->groupBy('assembly_zone.assemblycd')->get();
                $data = array();
                if (!empty($filtered)) {
                    foreach ($filtered as $post) {
                        $nestedData['assemblycd'] = $post->assemblycd;
                        $nestedData['assemblyname'] = $post->assemblyname;
                        $nestedData['rand_status2'] = $post->rand_status2;
                        $toPARTY = $post->P;
                        $toRESERVE = $post->R;
                        $nestedData['totRES'] = $toRESERVE;
                        $nestedData['totPAR'] = $toPARTY;
                        $shortfallREQ = $toPARTY + $toRESERVE;
                        $partyReserve = tbl_personnela::where('forassembly', '=', $post->assemblycd)
                                    ->where('forzone', '=', $zone)
                                // ->where('phase', '=', $phase)
                                   
                                ->where(function($q) {
                                    $q->orwhere('booked', '=', 'P')
                                    ->orwhere('booked', '=', 'R');
                                })
                                ->select('booked', 'dcrccd')
                                ->get();

                        $toparty = 0;
                        $toreserve = 0;
                        if (!empty($partyReserve)) {
                            foreach ($partyReserve as $paRes) {
                                if ($paRes->dcrccd != '') {
                                    if ($paRes->booked == 'P') {
                                        $toparty = $toparty + 1;
                                    } else {
                                        $toreserve = $toreserve + 1;
                                    }
                                }
                            }
                        }
                        $nestedData['toparty'] = $toparty;
                        $nestedData['toreserve'] = $toreserve;
                        $shortfallASS = $toparty + $toreserve;
                        $nestedData['shortfall'] = $shortfallREQ - $shortfallASS;
                        $data[] = $nestedData;
                    }
                }
                // print_r($data);
                $response = array(
                    'options' => $data,
                    'status' => 1
                );
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }


    public function secondrand(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            $validate_array = ['zone' => 'required|alpha_num|min:4|max:4',
                'districtcd' => 'required|alpha_num|min:2|max:2',
              //  'pc' => 'required|alpha_num|min:2|max:2',
                'phase' => 'required|integer'];
            for ($x = 0; $x <= $request->row_count; $x++) {
                $validate_array['myCheck' . $x] = 'nullable|alpha_dash';
            }
            $validate_array1 = ['zone.required' => 'Zone is required',
                'zone.alpha_num' => 'Zone must be an alpha numeric characters',
                'districtcd.required' => 'District is required',
                'districtcd.alpha_num' => 'District must be an alpha numeric characters',
                'phase.required' => 'Phase is required',
                'phase.integer' => 'Phase must be an integer'];
            for ($y = 0; $y <= $request->row_count; $y++) {
                $validate_array1['myCheck' . $y . '.alpha_dash'] = 'Assembly must be an alpha numeric characters';
                //  $validate_array1['myCheck'. $y.'.unique'] = 'Already created some assembly(s) for this zone';
            }
            $this->validate($request, $validate_array, $validate_array1);

            try {
                $row_count = $request->row_count;
                $zone = $request->zone;
                $districtcd = $request->districtcd;
                $phase = $request->phase;
                $filtered0 = tbl_assembly_zone::where('assembly_zone.zone', '=', $zone)
                        ->where('assembly_zone.rand_status2', '!=', 'Y')
                        ->where('assembly_zone.rand_status1', '=', 'Y')
                        ->update(['rand_status2' => 'N']);
                $count = 0;
                for ($i = 1; $i <= $row_count; $i++) {
                    $asmcd = 'myCheck' . $i;
                    if ($request->$asmcd != "") {
                        $asmstat = explode('-', $request->$asmcd);
                        $asmcode = $asmstat[0];
                        $asmstatus = $asmstat[1];
                        $randstat = 'A';

                        $filtered = tbl_assembly_zone::where('assembly_zone.assemblycd', '=', $asmcode)
                                ->where('assembly_zone.zone', '=', $zone)
                                ->update(['rand_status2' => $randstat]);
                        $count++;
                    }
                }

                DB::select('call randomisation2(?,?,?)', [$zone, $districtcd, $phase]);
                //  $pcount=$personnelCheck[0]->t_Count;
//                  $filteredUp=tbl_assembly_zone::where('assembly_zone.rand_status2','=','A')
//                         ->where('assembly_zone.zone','=',$zone)
//                         ->update(['rand_status2'=>'N']);

                $response = array(
                    'options' => $count,
                    'status' => 1
                );
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }

    //:::::::::::::::::::::Second Appointment Letter Populate:::::::::://
    public function second_appt_letter_populate(Request $request) { //dd($request->all());
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            $this->validate($request, [
                'zone' => 'required|alpha_num|min:4|max:4',
                'districtcd' => 'required|alpha_num|min:2|max:2',
                'party_reserve' => 'required|alpha|min:1|max:1',
                'orderno' => 'required|alpha_num|min:4|max:4'
                    ], [
                'zone.required' => 'Zone is required',
                'zone.alpha_num' => 'Zone must be an alpha numeric characters',
                'districtcd.required' => 'District is required',
                'districtcd.alpha_num' => 'District must be an alpha numeric characters',
                'party_reserve.required' => 'Party/Reserve is required',
                'party_reserve.alpha' => 'Party/Reserve must be an alpha characters',
                'orderno.required' => 'Order Date & No is required',
                'orderno.alpha_num' => 'Order Date & No be an alpha numeric characters'
            ]);
            try {
                $zone = $request->zone;
                $districtcd = $request->districtcd;
                $party_reserve = $request->party_reserve;
                $orderno = $request->orderno;
                $phase = $request->phase;
                $count = 0;
                $session_personnela = session()->get('personnela_ppds');
                if ($party_reserve == "P") {
                   $secondRand = DB::select('call second_rand_p(?,?,?,?)', [$zone, $session_personnela, $orderno,$phase]);
               //   $secondRand = DB::select('call second_rand_p(?,?,?)', [$zone, $session_personnela, $orderno]);
                    $pcount = $secondRand[0]->t_Count;
                } else {
                    $tbl_second_rand_table = new tbl_second_rand_table();
                    $tbl_second_rand_table->where('party_reserve', '=', 'R')->where('forzone', '=', $zone)->where('phase', '=', $phase)->delete();
                    $select = tbl_personnela::
                            join('order_no_date', '' . $session_personnela . '.fordistrict', '=', 'order_no_date.districtcd')
                            ->where('booked', '=', 'R')->where('poststat', '!=', 'MO')
                            ->where('selected', '=', '1')
                            ->where('order_no_date.order_cd', '=', $orderno)
                            ->where('forzone', '=', $zone)
                            ->where('phase', '=', $phase)
                            ->select('forassembly', 'forzone', 'groupid', 'no_of_member', 'dcrccd', 'booked', 'officecd', 'poststat', 'order_no_date.order_no', 'order_no_date.order_dt','phase');
                    $bindings = $select->getBindings();
                    $insertQuery = 'INSERT INTO second_rand_table( assembly, forzone, groupid ,mem_no, dcrcgrp,party_reserve,pers_off,per_poststat,order_no,order_date,phase)' . $select->toSql();
                    DB::insert($insertQuery, $bindings);

                    $select = tbl_second_rand_table::where('party_reserve', '=', 'R')
                                    ->where('forzone', '=', $zone)
                                    ->where('phase', '=', $phase)
                                    ->select('pers_off', 'per_poststat', 'groupid', 'assembly')->get();
                   // dd($select);
                    $secondRand = DB::select('call second_rand_r(?,?,?)', [$zone, $session_personnela,$phase]);
                    $pcount = $secondRand[0]->t_Count;
                }
                $response = array(
                    'options' => $pcount,
                    'status' => 1
                );
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }

    public function getSecondRecordAvailable(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            $validate_array = ['forZone' => 'required|alpha_num|min:4|max:4',
                'party_reserve' => 'nullable|alpha|min:1|max:1',
                'assembly' => 'nullable|alpha_num|min:3|max:3',
                'group_id' => 'nullable|digits_between:1,4',
                'no_of_member' => 'nullable|digits_between:1,1',
                'blockMunicipality' => 'nullable|alpha_num',
                'phase' => 'nullable|digits_between:1,1'
            ];

            $validate_array1 = ['forZone.required' => 'For Zone is required',
                'forZone.alpha_num' => 'For Zone must be an alpha numeric characters',
                'party_reserve.required' => 'Party/Reserve is required',
                'party_reserve.alpha' => 'Party/Reserve must be an alpha characters',
                'assembly.alpha_num' => 'Assembly must be an alpha numeric characters',
                'group_id.digits_between' => 'Group ID must be an integer',
                'no_of_member.digits_between' => 'No of Member must be an integer',
                'blockMunicipality.alpha_num' => 'Block must be an alpha numeric characters',
                'phase.digits_between' => 'Phase must be integer'];

            $this->validate($request, $validate_array, $validate_array1);

            try {
                $forzone = $request->forZone;
                $party_reserve = $request->party_reserve;
                $assembly = $request->assembly;
                $group_id = $request->group_id;
                $no_of_member = $request->no_of_member;
                $blockMunicipality = $request->blockMunicipality;
                $orderno = $request->orderno;
                $phase = $request->phase;
                $count = 0;
                $max_count = "";
                $tbl_second_rand_table = new tbl_second_rand_table();
                $max_code = new tbl_second_rand_table();
                if ($party_reserve != "") {
                    $max_code = $max_code->where('party_reserve', '=', $party_reserve);
                }
                if ($forzone != '') {
                    $max_code = $max_code->where('forzone', '=', $forzone);
                }
                if ($assembly != '') {
                    $max_code = $max_code->where('assembly', '=', $assembly);
                }
                if ($group_id != '') {
                    $max_code = $max_code->where('groupid', '=', $group_id);
                }
                if ($blockMunicipality != '') {
                    $max_code = $max_code->where('block_muni_cd', '=', $blockMunicipality);
                }
                if ($orderno != '') {
                    $max_code = $max_code->where('order_no', '=', $orderno);
                }
                if ($phase != '') {
                    $max_code = $max_code->where('phase', '=', $phase);
                }
                if ($party_reserve == "P") {
                    if ($no_of_member == '4') {
                        $max_code = $max_code->where(function($q) {
                            $q->orwhere('mem_no', '=', '4')
                                    ->orwhere('mem_no', '=', '5');
                        });
                    }
                    if ($no_of_member == '6') {
                        $max_code = $max_code->where('mem_no', '=', $no_of_member);
                    }
                }
                $max_code = $max_code->select(DB::raw('count(*) as cnt'))->get();
                $max_count = json_decode($max_code);
                $count = $max_count[0]->cnt;
                $response = array(
                    'options' => $count,
                    'status' => 1
                );
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }

    //::::::::::::::::::::::: Second Appt Letter EXCEL :::::::::::::::::::::://
    public function getSecondAppointmentLetterEXCEL(Request $request) {
        $validate_array = ['forZone' => 'required|alpha_num|min:4|max:4',
            'party_reserve' => 'nullable|alpha|min:1|max:1',
            'assembly' => 'nullable|alpha_num|min:3|max:3',
            'group_id' => 'nullable|digits_between:1,4',
            'no_of_member' => 'nullable|digits_between:1,1',
            'phase' => 'nullable|digits_between:1,1',
            'from' => 'required|digits_between:1,4',
            'to' => 'required|digits_between:1,4',
            'blockMunicipality' => 'nullable|alpha_num',
            'poststat' => 'nullable|alpha_num',
        ];

        $validate_array1 = ['forZone.required' => 'For Zone is required',
            'forZone.alpha_num' => 'For Zone must be an alpha numeric characters',
            'party_reserve.required' => 'Party/Reserve is required',
            'party_reserve.alpha' => 'Party/Reserve must be an alpha characters',
            'assembly.alpha_num' => 'Assembly must be an alpha numeric characters',
            'group_id.digits_between' => 'Group ID must be an integer',
            'no_of_member.digits_between' => 'No of Member must be an integer',
            'from.digits_between' => 'Records From must be an integer',
            'to.digits_between' => 'Records To must be an integer',
            'blockMunicipality.alpha_num' => 'Block must be an alpha numeric characters',
            'poststat.alpha_num' => 'posstat must be an alpha numeric characters',
            'phase.digits_between' => 'Phase must be integer'

        ];

        $this->validate($request, $validate_array, $validate_array1);
        try {
            $forzone = $request->forZone;
            $party_reserve = $request->party_reserve;
            $blockMunicipality = $request->blockMunicipality;
            $poststat = $request->poststat;
            $phase = $request->phase;
            $assembly = $request->assembly;
            $group_id = $request->group_id;
            $no_of_member = $request->no_of_member;
            $orderno = $request->orderno;
            $pfrom = $request->from;
            $pto = $request->to;
            $count = 0;
            if ($assembly == '') {
                $ApptRecord = DB::select('call getSecondAppointmentLetterPDF45(?,?,?,?,?,?,?,?,?,?,?)', [$forzone, $party_reserve, $assembly, $group_id, $no_of_member, $pfrom, $pto, $blockMunicipality, $poststat, $orderno,$phase]);
                //dd($ApptRecord);
            } else {
                $ApptRecord = DB::select('call getSecondAppointmentLetterPDF45Assembly(?,?,?,?,?,?,?,?,?,?)', [$forzone, $party_reserve, $assembly, $group_id, $no_of_member, $pfrom, $pto, $blockMunicipality, $poststat, $orderno]);
            }

            $data = array();
            foreach ($ApptRecord as $ApptDetail) {
                $nestedData['Groupid'] = $ApptDetail->groupid;
                $nestedData['Per Office ID'] = $ApptDetail->pers_off;
                $nestedData['Block_muni ID'] = $ApptDetail->block_muni_cd;
                $nestedData['Block_muni Name'] = $ApptDetail->block_muni_name;
                $nestedData['Poststat'] = $ApptDetail->per_poststat;
                $nestedData['Assembly'] = $ApptDetail->assembly;
                $nestedData['Assembly Name'] = $ApptDetail->assembly_name;
                $nestedData['PC'] = $ApptDetail->pccd;
                $nestedData['PC Name'] = $ApptDetail->pcname;
                $nestedData['District'] = $ApptDetail->district;

                $nestedData['pr_personcd'] = $ApptDetail->pr_personcd;
                $nestedData['p1_personcd'] = $ApptDetail->p1_personcd;
                $nestedData['p2_personcd'] = $ApptDetail->p2_personcd;
                $nestedData['p3_personcd'] = $ApptDetail->p3_personcd;
                $nestedData['pa_personcd'] = $ApptDetail->pa_personcd;
                $nestedData['pb_personcd'] = $ApptDetail->pb_personcd;

                $nestedData['pr_name'] = $ApptDetail->pr_name;
                $nestedData['p1_name'] = $ApptDetail->p1_name;
                $nestedData['p2_name'] = $ApptDetail->p2_name;
                $nestedData['p3_name'] = $ApptDetail->p3_name;
                $nestedData['pa_name'] = $ApptDetail->pa_name;
                $nestedData['pb_name'] = $ApptDetail->pb_name;

                $nestedData['pr_designation'] = $ApptDetail->pr_designation;
                $nestedData['p1_designation'] = $ApptDetail->p1_designation;
                $nestedData['p2_designation'] = $ApptDetail->p2_designation;
                $nestedData['p3_designation'] = $ApptDetail->p3_designation;
                $nestedData['pa_designation'] = $ApptDetail->pa_designation;
                $nestedData['pb_designation'] = $ApptDetail->pb_designation;

                $nestedData['pr_status'] = $ApptDetail->pr_status;
                $nestedData['p1_status'] = $ApptDetail->p1_status;
                $nestedData['p2_status'] = $ApptDetail->p2_status;
                $nestedData['p3_status'] = $ApptDetail->p3_status;
                $nestedData['pa_status'] = $ApptDetail->pa_status;
                $nestedData['pb_status'] = $ApptDetail->pb_status;

                $nestedData['pr_post_stat'] = $ApptDetail->pr_post_stat;
                $nestedData['p1_post_stat'] = $ApptDetail->p1_post_stat;
                $nestedData['p2_post_stat'] = $ApptDetail->p2_post_stat;
                $nestedData['p3_post_stat'] = $ApptDetail->p3_post_stat;
                $nestedData['pa_post_stat'] = $ApptDetail->pa_post_stat;
                $nestedData['pb_post_stat'] = $ApptDetail->pb_post_stat;

                $nestedData['pr_officecd'] = $ApptDetail->pr_officecd;
                $nestedData['p1_officecd'] = $ApptDetail->p1_officecd;
                $nestedData['p2_officecd'] = $ApptDetail->p2_officecd;
                $nestedData['p3_officecd'] = $ApptDetail->p3_officecd;
                $nestedData['pa_officecd'] = $ApptDetail->pa_officecd;
                $nestedData['pb_officecd'] = $ApptDetail->pb_officecd;

                $nestedData['pr_officename'] = $ApptDetail->pr_officename;
                $nestedData['p1_officename'] = $ApptDetail->p1_officename;
                $nestedData['p2_officename'] = $ApptDetail->p2_officename;
                $nestedData['p3_officename'] = $ApptDetail->p3_officename;
                $nestedData['pa_officename'] = $ApptDetail->pa_officename;
                $nestedData['pb_officename'] = $ApptDetail->pb_officename;

                $nestedData['pr_officeaddress'] = $ApptDetail->pr_officeaddress;
                $nestedData['p1_officeaddress'] = $ApptDetail->p1_officeaddress;
                $nestedData['p2_officeaddress'] = $ApptDetail->p2_officeaddress;
                $nestedData['p3_officeaddress'] = $ApptDetail->p3_officeaddress;
                $nestedData['pa_officeaddress'] = $ApptDetail->pa_officeaddress;
                $nestedData['pb_officeaddress'] = $ApptDetail->pb_officeaddress;

                $nestedData['pr_postoffice'] = $ApptDetail->pr_postoffice;
                $nestedData['p1_postoffice'] = $ApptDetail->p1_postoffice;
                $nestedData['p2_postoffice'] = $ApptDetail->p2_postoffice;
                $nestedData['p3_postoffice'] = $ApptDetail->p3_postoffice;
                $nestedData['pa_postoffice'] = $ApptDetail->pa_postoffice;
                $nestedData['pb_postoffice'] = $ApptDetail->pb_postoffice;

                $nestedData['pr_subdivision'] = $ApptDetail->pr_subdivision;
                $nestedData['p1_subdivision'] = $ApptDetail->p1_subdivision;
                $nestedData['p2_subdivision'] = $ApptDetail->p2_subdivision;
                $nestedData['p3_subdivision'] = $ApptDetail->p3_subdivision;
                $nestedData['pa_subdivision'] = $ApptDetail->pa_subdivision;
                $nestedData['pb_subdivision'] = $ApptDetail->pb_subdivision;

                $nestedData['pr_district'] = $ApptDetail->pr_district;
                $nestedData['p1_district'] = $ApptDetail->p1_district;
                $nestedData['p2_district'] = $ApptDetail->p2_district;
                $nestedData['p3_district'] = $ApptDetail->p3_district;
                $nestedData['pa_district'] = $ApptDetail->pa_district;
                $nestedData['pb_district'] = $ApptDetail->pb_district;

                $nestedData['pr_pincode'] = $ApptDetail->pr_pincode;
                $nestedData['p1_pincode'] = $ApptDetail->p1_pincode;
                $nestedData['p2_pincode'] = $ApptDetail->p2_pincode;
                $nestedData['p3_pincode'] = $ApptDetail->p3_pincode;
                $nestedData['pa_pincode'] = $ApptDetail->pa_pincode;
                $nestedData['pb_pincode'] = $ApptDetail->pb_pincode;

                $nestedData['pr_mobno'] = $ApptDetail->pr_mobno;
                $nestedData['p1_mobno'] = $ApptDetail->p1_mobno;
                $nestedData['p2_mobno'] = $ApptDetail->p2_mobno;
                $nestedData['p3_mobno'] = $ApptDetail->p3_mobno;
                $nestedData['pa_mobno'] = $ApptDetail->pa_mobno;
                $nestedData['pb_mobno'] = $ApptDetail->pb_mobno;

                $nestedData['DC Venue'] = $ApptDetail->dc_venue;
                $nestedData['DC Address'] = $ApptDetail->dc_address;
                $nestedData['DC Date'] = $ApptDetail->dc_date;
                $nestedData['DC Time'] = $ApptDetail->dc_time;
                $nestedData['RC Venue'] = $ApptDetail->rc_venue;

                $nestedData['Training Venue'] = $ApptDetail->training_venue . " , " . $ApptDetail->sub_venuename;
                $nestedData['Venue Address'] = $ApptDetail->venue_addr;
                $nestedData['Training Date'] = $ApptDetail->training_date;
                $nestedData['Training Time'] = $ApptDetail->training_time;

                $nestedData['Poll Date'] = $ApptDetail->polldate;
                $nestedData['Poll Time'] = $ApptDetail->polltime;
                $nestedData['Order No'] = $ApptDetail->order_no;
                $nestedData['order Date'] = $ApptDetail->order_date;
                $data[] = $nestedData;
            }
            return \Excel::create('2ndApptExcelPP', function($excel) use ($data) {
                        $excel->sheet(' ', function($sheet) use ($data) {
                            $sheet->fromArray($data);
                        });
                    })->download('xlsx');
        } catch (\Exception $e) {
            $response = array(
                'exception' => true,
                'exception_message' => $e->getMessage(),
            );
            // $statusCode = 400;
            return $response;
        }
    }

    //::::::::::::::::::::::: Second Appt letter Reserve PDF::::::::::::::::::://
    public function getSecondAppointmentLetterReserveEXCEL(Request $request) {
        $validate_array = ['forZone' => 'required|alpha_num|min:4|max:4',
            'party_reserve' => 'required|alpha|min:1|max:1',
            'assembly' => 'nullable|alpha_num|min:3|max:3',
            'group_id' => 'nullable|digits_between:1,4',
            'no_of_member' => 'nullable|digits_between:1,1',
            'from' => 'required|digits_between:1,4',
            'to' => 'required|digits_between:1,4',
            'poststat' => 'nullable|alpha_num'
        ];

        $validate_array1 = ['forZone.required' => 'For Zone is required',
            'forZone.alpha_num' => 'For Zone must be an alpha numeric characters',
            'party_reserve.required' => 'Party/Reserve is required',
            'party_reserve.alpha' => 'Party/Reserve must be an alpha characters',
            'assembly.alpha_num' => 'Assembly must be an alpha numeric characters',
            'group_id.digits_between' => 'Group ID must be an integer',
            'no_of_member.digits_between' => 'No of Member must be an integer',
            'from.required' => 'Records From is required',
            'from.digits_between' => 'Records From must be an integer',
            'to.required' => 'Records To is required',
            'to.digits_between' => 'Records To must be an integer',
            'poststat.alpha_num' => 'Poststat must be an alpha numeric characters'
        ];

        $this->validate($request, $validate_array, $validate_array1);

        $forzone = $request->forZone;
        $party_reserve = $request->party_reserve;
        $assembly = $request->assembly;
        $group_id = $request->group_id;
        $no_of_member = $request->no_of_member;
        $blockMunicipality = $request->blockMunicipality;
        $poststat = $request->poststat;
        $orderno = $request->orderno;
        $pfrom = $request->from;
        $pto = $request->to;
        $count = 0;

        if ($assembly == '') {
            $ApptRecord = DB::select('call getSecondAppointmentLetterPDF45(?,?,?,?,?,?,?,?,?,?)', [$forzone, $party_reserve, $assembly, $group_id, $no_of_member, $pfrom, $pto, $blockMunicipality, $poststat, $orderno]);
        } else {
            $ApptRecord = DB::select('call getSecondAppointmentLetterPDF45Assembly(?,?,?,?,?,?,?,?,?,?)', [$forzone, $party_reserve, $assembly, $group_id, $no_of_member, $pfrom, $pto, $blockMunicipality, $poststat, $orderno]);
        }


        $pdf = app('FPDF');
        $pdf->AddPage();
        $count = 0;
        $per_page = 1;
        $signaturePath = config('constants.DEOSIGNATURE');
        foreach ($ApptRecord as $ApptDetail) {
            
        }
    }

    //::::::::::::::::::::::: Second Appt Letter PDF 4-5 (PC Election):::::::::::::::::::://
    public function getSecondAppointmentLetterPDF452019(Request $request) {
        // echo 'hi';die;
        $validate_array = ['forZone' => 'required|alpha_num|min:4|max:4',
            'party_reserve' => 'nullable|alpha|min:1|max:1',
            'assembly' => 'nullable|alpha_num|min:3|max:3',
            'group_id' => 'nullable|digits_between:1,4',
            'no_of_member' => 'nullable|digits_between:1,1',
            'from' => 'required|digits_between:1,5',
            'to' => 'required|digits_between:1,5',
            'blockMunicipality' => 'nullable|alpha_num',
            'poststat.alpha_num' => 'nullable|alpha_num',
            'phase' => 'nullable|digits_between:1,1'
        ];

        $validate_array1 = ['forZone.required' => 'For Zone is required',
            'forZone.alpha_num' => 'For Zone must be an alpha numeric characters',
            'party_reserve.required' => 'Party/Reserve is required',
            'party_reserve.alpha' => 'Party/Reserve must be an alpha characters',
            'assembly.alpha_num' => 'Assembly must be an alpha numeric characters',
            'group_id.digits_between' => 'Group ID must be an integer',
            'no_of_member.digits_between' => 'No of Member must be an integer',
            'from.required' => 'Records From is required',
            'from.digits_between' => 'Records From must be an integer',
            'to.required' => 'Records To is required',
            'to.digits_between' => 'Records To must be an integer',
            'blockMunicipality.alpha_num' => 'Block must be an alpha numeric characters',
            'poststat.alpha_num' => 'Poststat must be an alpha numeric characters',
            'phase.digits_between' => 'Phase must be integer'

        ];

        $this->validate($request, $validate_array, $validate_array1);

        $forzone = $request->forZone;
        $party_reserve = $request->party_reserve;
        $blockMunicipality = $request->blockMunicipality;
        $assembly = $request->assembly;
        $group_id = $request->group_id;
        $no_of_member = $request->no_of_member;
        $orderno = $request->orderno;
        $phase = $request->phase;
        $poststat = $request->poststat;
        $pfrom = $request->from;
        $pto = $request->to;
        $count = 0;

        $tick = 'images/tick.png';

        if ($assembly == '') {
            $ApptRecord = DB::select('call getSecondAppointmentLetterPDF45(?,?,?,?,?,?,?,?,?,?,?)', [$forzone, $party_reserve, $assembly, $group_id, $no_of_member, $pfrom, $pto, $blockMunicipality, $poststat, $orderno,$phase]);
            // dd($ApptRecord);
        } else {
            $ApptRecord = DB::select('call getSecondAppointmentLetterPDF45Assembly(?,?,?,?,?,?,?,?,?,?,?)', [$forzone, $party_reserve, $assembly, $group_id, $no_of_member, $pfrom, $pto, $blockMunicipality, $poststat, $orderno,$phase]);
        }

        //echo json_encode($ApptRecord);die;
        //    echo count($ApptRecord);die;
        $pdf = app('FPDF');
        $pdf->AddPage();
        $count = 0;
        $per_page = 1;
        // $signaturePath = config('constants.DEOSIGNATURE');
        $signaturePath = config('constants.ROSIGNATURE');
        foreach ($ApptRecord as $ApptDetail) {
            $tick_p1 = 'images/blank.png';
            $tick_p2 = 'images/blank.png';
            $tick_p3 = 'images/blank.png';
            $tick_pa = 'images/blank.png';
            $tick_pb = 'images/blank.png';
            $tick_pr = 'images/blank.png';
            if ($count < $per_page) {
                if ($ApptDetail->party_reserve == "P") {
                    $fill = false;
                    $poll_date = $ApptDetail->polldate;
                    $poll_time = $ApptDetail->polltime;
                    $training_venue = $ApptDetail->training_venue;
                    $training_subvenue = $ApptDetail->sub_venuename;
                    $venue_addr = $ApptDetail->venue_addr;
                    $training_date = $ApptDetail->training_date;
                    $training_time = $ApptDetail->training_time;

                    $dc = ($ApptDetail->dc_venue != '' ? $ApptDetail->dc_venue . ", " . $ApptDetail->dc_address : "___________________________________");
                    $dc_date = ($ApptDetail->dc_date != '' ? $ApptDetail->dc_date : "___________");
                    $dc_time = ($ApptDetail->dc_time != '' ? $ApptDetail->dc_time : "___________");
                    $rcvenue = ($ApptDetail->rc_venue != '' ? $ApptDetail->rc_venue : "_______________________________");

                    $euname = "ELECTION URGENT";
                    $euname1 = "ORDER OF APPOINTMENT FOR POLLING DUTIES";
                    // $euname2 = "GENERAL PARLIAMENTARY ELECTION, 2019";
                    $euname2 = "General Election to West Bengal Legislative Assembly, 2021";
                    // $euname21="ASSEMBLY ELECTION, 2016";
                    $euname3 = ($assembly == '') ? $ApptDetail->pers_off . "/" . $ApptDetail->per_poststat : "";
                    $euname22 = "Polling Party No. " . $ApptDetail->groupid;
                    $euname4 = "Order No: " . $ApptDetail->order_no;
                    $euname5 = "Date: " . date('d/m/Y', strtotime(trim(str_replace('/', '-', $ApptDetail->order_date))));

                    $euname6 = "     In pursuance of sub-section(1) and sub-section(3) of section 26 of the Representation of the People Act 1951, I hereby ";
                    $euname7 = "appoint the officers specified in column(2) and (3) of the table below as Presiding Officer and Polling Officers respectively for the Polling ";
                    $euname8 = "Party specified in corresponding entry in column(1) of the table provided by me for " . $ApptDetail->assembly . " - " . $ApptDetail->assembly_name . " L.A. Constituency ";
                    $euname78 = "forming part of " . $ApptDetail->pccd . " - " . $ApptDetail->pcname . " Parliamentary Constituency.";
                    $euname81 = "I also authorise the Polling Officer specified in column(4) of the table against that entry to perform the functions of the Presiding Officer";
                    $euname82 = "during the unavoidable absence, if any, of the Presiding Officer.";

                    $euname9 = "The Poll will be taken on " . $poll_date . " during the hours " . $poll_time . ". The Presiding Officer should arrange to collect the Polling ";
                    $euname10 = "materials from " . $dc . " on " . $dc_date . " at " . $dc_time . " ";
                    $euname11 = "and after the Poll, these should be returned to collecting centre at " . $rcvenue;

                    $euname12 = "Place: " . $ApptDetail->district;
                    $euname13 = "Date: " . date("d/m/Y");
                    // $euname14 = "District: " . $ApptDetail->district;
                    $euname14 = "Assembly: " . $ApptDetail->assembly.'-'.$ApptDetail->assembly_name;
                    $nb1 = "1. You are requested to attend the training at " . $training_venue . " " . $training_subvenue . " , " . $venue_addr;
                    $nb2 = " on " . $training_date . " from " . $training_time;
                    //$nb3 = "(__________________________)";
//	        $signature=$signaturePath.$ApptDetail->assembly.".jpg";
//		$roname="RO/".$ApptDetail->assembly." - ".$ApptDetail->assembly_name;
                    // dd($signaturePath . $ApptDetail->pccd);
                    $signature = $signaturePath . $ApptDetail->assembly . ".jpg";
                    // $roname = "RO/" . $ApptDetail->pccd . " - " . $ApptDetail->pcname;
                    // $roname = "District Election Officer";
                    $roname = "Returning Officer";
                    $pdf->SetFont('Arial', 'B', 10);
                    $pdf->Cell(37, 6, $euname, 1, 0, 'L');
                    $pdf->SetFont('Arial', 'B', 10);
                    $pdf->Cell(20);
                    $pdf->Cell(84, 4, $euname1, 'B', 0, 'C');
                    $pdf->SetFont('Arial', 'B', 8);
                    $pdf->Cell(28);
                    $pdf->Cell(10, 3, $euname3, 0, 0, 'R');

                    // Line break
                    $pdf->Ln(6);

                    $pdf->SetFont('Arial', 'B', 8);
                    $pdf->Cell(15, 10, '', 0, 0, 'L');
                    $pdf->SetFont('Arial', 'U', 10);
                    $pdf->Cell(46);
                    $pdf->Cell(77, 4, $euname2, '', 0, 'C');
                    $pdf->SetFont('Arial', 'B', 8);
                    $pdf->Cell(12);
                    $pdf->SetFont('Arial', 'B', 10);
                    $pdf->Cell(37, 6, $euname22, 1, 0, 'R');

                    $pdf->Ln(5);

//                $pdf->SetFont('Arial','B',8);
//                $pdf->Cell(15,10,'',0,0,'L');
//                $pdf->SetFont('Arial','',10);
//                $pdf->Cell(60);
//                $pdf->Cell(47,4,$euname21,'B',0,'C');
//                $pdf->SetFont('Arial','B',8);
//                $pdf->Cell(13);
//                $pdf->SetFont('Arial','B',10);
//                $pdf->Cell(33,6,'',0,0,'R');
//                // Line break
//
//                $pdf->Ln(4);
                    $pdf->Ln(4);
                    $pdf->SetFont('Arial', '', 9);
                    $pdf->Cell(15, 10, $euname4, 0, 0, 'L');
                    $pdf->SetFont('Arial', '', 10);
                    $pdf->Cell(60);
                    $pdf->Cell(47, 4, '', '', 0, 'C');
                    $pdf->SetFont('Arial', '', 8);
                    $pdf->Cell(33);
                    $pdf->SetFont('Arial', '', 9);
                    $pdf->Cell(33, 6, $euname5, 0, 0, 'R');

                    $pdf->Ln(6);

                    $pdf->Ln();
                    $pdf->SetFont('Arial', '', 8.7);
                    $pdf->MultiCell(190, 4, $euname6 . $euname7 . $euname8 . $euname78, 0, 'J');

                    $pdf->SetFont('Arial', '', 8.7);
                    $pdf->Cell(87);
                    $pdf->Cell(20, 10, $euname81, 0, 0, 'C');

                    // Line break
                    $pdf->Ln(4);
                    $pdf->SetFont('Arial', '', 8.7);
                    $pdf->Cell(50, 10, $euname82, 0, 0, 'L');

                    // Line break
                    $pdf->Ln(10);

                    $pdf->SetFillColor(255, 255, 255);
                    $pdf->SetDrawColor(0, 0, 0);
                    $pdf->SetLineWidth(.3);
                    $pdf->SetFont('Arial', 'B', '8');

                    $head = array('Polling', 'Name of the Presiding Officer', 'Name of the Polling Officers', 'Polling Officer authorised to perform');
                    $w = array(11, 59, 60, 60);
                    $head1 = array('Party', '', '', 'the functions of the Presiding Officer');
                    $head2 = array('No.', '', '', "in the latter's absence");
                    $head3 = array('(1)', '(2)', '(3)', "(4)");

                    //	$this->SetFont('Arial','',9);
                    for ($j = 0; $j < count($head); $j++)
                        $pdf->Cell($w[$j], 5, $head[$j], 'LTR', 0, 'C', true);
                    $pdf->Ln();
                    for ($j1 = 0; $j1 < count($head1); $j1++)
                        $pdf->Cell($w[$j1], 4, $head1[$j1], 'LR', 0, 'C', true);
                    $pdf->Ln();
                    for ($j2 = 0; $j2 < count($head2); $j2++)
                        $pdf->Cell($w[$j2], 5, $head2[$j2], 'LR', 0, 'C', true);
                    $pdf->Ln();

                    for ($j3 = 0; $j3 < count($head3); $j3++)
                        $pdf->Cell($w[$j3], 7, $head3[$j3], 1, 0, 'C', true);
                    $pdf->Ln();

                    $pr_name = $ApptDetail->pr_name;
                    $pr_desig = $ApptDetail->pr_designation;
                    $pr_code = $ApptDetail->pr_personcd;
                    $pr_office = $ApptDetail->pr_officename;
                    $pr_ofc_address = $ApptDetail->pr_officeaddress;
                    $pr_ofc_address1 = "P.O. - " . $ApptDetail->pr_postoffice . ", Subdiv. - " . $ApptDetail->pr_subdivision;
                    $pr_ofc_address2 = "Dist - " . $ApptDetail->pr_district;
                    $pr_ofc_cd = "Mobile No. - " . $ApptDetail->pr_mobno . ", OFFICE - (" . $ApptDetail->pr_officecd . ")";
                    $pr_post_stat = $ApptDetail->pr_post_stat;
                    $pr_join = $pr_post_stat . " PIN - (" . $pr_code . ")";

                    $p1_name = "1. " . $ApptDetail->p1_name;
                    $p1_desig = $ApptDetail->p1_designation;
                    $p1_code = $ApptDetail->p1_personcd;
                    $p1_office = $ApptDetail->p1_officename;
                    $p1_ofc_address = $ApptDetail->p1_officeaddress;
                    $p1_ofc_address1 = "P.O. - " . $ApptDetail->p1_postoffice . ", Subdiv. - " . $ApptDetail->p1_subdivision;
                    $p1_ofc_address2 = "Dist - " . $ApptDetail->p1_district;
                    $p1_ofc_cd = "Mobile No. - " . $ApptDetail->p1_mobno . ", OFFICE - (" . $ApptDetail->p1_officecd . ")";
                    $p1_post_stat = $ApptDetail->p1_post_stat;
                    $p1_join = $p1_post_stat . " PIN - (" . $p1_code . ")";

                    $p2_name = "2. " . $ApptDetail->p2_name;
                    $p2_desig = $ApptDetail->p2_designation;
                    $p2_code = $ApptDetail->p2_personcd;
                    $p2_office = $ApptDetail->p2_officename;
                    $p2_ofc_address = $ApptDetail->p2_officeaddress;
                    $p2_ofc_address1 = "P.O. - " . $ApptDetail->p2_postoffice . ", Subdiv. - " . $ApptDetail->p2_subdivision;
                    $p2_ofc_address2 = "Dist - " . $ApptDetail->p2_district;
                    $p2_ofc_cd = "Mobile No. - " . $ApptDetail->p2_mobno . ", OFFICE - (" . $ApptDetail->p2_officecd . ")";
                    $p2_post_stat = $ApptDetail->p2_post_stat;
                    $p2_join = $p2_post_stat . " PIN - (" . $p2_code . ")";

                    $sl = (($ApptDetail->pa_name == '') ? "3. " : (($ApptDetail->pb_name == '') ? "4. " : "5. "));

                    $p3_name = $sl . $ApptDetail->p3_name;
                    $p3_desig = $ApptDetail->p3_designation;
                    $p3_code = $ApptDetail->p3_personcd;
                    $p3_office = $ApptDetail->p3_officename;
                    $p3_ofc_address = $ApptDetail->p3_officeaddress;
                    $p3_ofc_address1 = "P.O. - " . $ApptDetail->p3_postoffice . ", Subdiv. - " . $ApptDetail->p3_subdivision;
                    $p3_ofc_address2 = "Dist - " . $ApptDetail->p3_district;
                    $p3_ofc_cd = "Mobile No. - " . $ApptDetail->p3_mobno . ", OFFICE - (" . $ApptDetail->p3_officecd . ")";
                    $p3_post_stat = $ApptDetail->p3_post_stat;
                    $p3_join = $p3_post_stat . " PIN - (" . $p3_code . ")";

                    // dd($ApptDetail->pa_name);
                    if ($ApptDetail->pa_name != '') {
                        $pa_name = "3. " . $ApptDetail->pa_name;
                        $pa_desig = $ApptDetail->pa_designation;
                        $pa_code = $ApptDetail->pa_personcd;
                        $pa_office = $ApptDetail->pa_officename;
                        $pa_ofc_address = $ApptDetail->pa_officeaddress;
                        $pa_ofc_address1 = "P.O. - " . $ApptDetail->pa_postoffice . ", Subdiv. - " . $ApptDetail->pa_subdivision;
                        $pa_ofc_address2 = "Dist - " . $ApptDetail->pa_district;
                        $pa_ofc_cd = "Mobile No. - " . $ApptDetail->pa_mobno . ", OFFICE - (" . $ApptDetail->pa_officecd . ")";
                        $pa_post_stat = $ApptDetail->pa_post_stat;
                        $pa_join = $pa_post_stat . " PIN - (" . $pa_code . ")";
                    }

                    if ($ApptDetail->pb_name != '') {
                        $pb_name = "4. " . $ApptDetail->pb_name;
                        $pb_desig = $ApptDetail->pb_designation;
                        $pb_code = $ApptDetail->pb_personcd;
                        $pb_office = $ApptDetail->pb_officename;
                        $pb_ofc_address = $ApptDetail->pb_officeaddress;
                        $pb_ofc_address1 = "P.O. - " . $ApptDetail->pb_postoffice . ", Subdiv. - " . $ApptDetail->pb_subdivision;
                        $pb_ofc_address2 = "Dist - " . $ApptDetail->pb_district;
                        $pb_ofc_cd = "Mobile No. - " . $ApptDetail->pb_mobno . ", OFFICE - (" . $ApptDetail->pb_officecd . ")";
                        $pb_post_stat = $ApptDetail->pb_post_stat;
                        $pb_join = $pb_post_stat . " PIN - (" . $pb_code . ")";
                    }

                    if ($ApptDetail->per_poststat == 'P1' && $assembly == '') {
                        $tick_p1 = $tick;
                    } else if ($ApptDetail->per_poststat == 'P2' && $assembly == '') {
                        $tick_p2 = $tick;
                    } else if ($ApptDetail->per_poststat == 'P3' && $assembly == '') {
                        $tick_p3 = $tick;
                    } else if ($ApptDetail->per_poststat == 'PA' && $assembly == '') {
                        $tick_pa = $tick;
                    } else if ($ApptDetail->per_poststat == 'PB' && $assembly == '') {
                        $tick_pb = $tick;
                    } else if ($ApptDetail->per_poststat == 'PR' && $assembly == '') {
                        $tick_pr = $tick;
                    }



                    // if ($ApptDetail->per_poststat == 'P1' && $assembly != '') {
                    //     $tick_p1 = $tick;
                    // } else if ($ApptDetail->per_poststat == 'P2' && $assembly != '') {
                    //     $tick_p2 = $tick;
                    // } else if ($ApptDetail->per_poststat == 'P3' && $assembly != '') {
                    //     $tick_p3 = $tick;
                    // } else if ($ApptDetail->per_poststat == 'PA' && $assembly != '') {
                    //     $tick_pa = $tick;
                    // } else if ($ApptDetail->per_poststat == 'PB' && $assembly != '') {
                    //     $tick_pb = $tick;
                    // } else if ($ApptDetail->per_poststat == 'PR' && $assembly != '') {
                    //     $tick_pr = $tick;
                    // }




                    //$aa=$pdf->Image($tick,$pdf->GetX()+$w[1], $pdf->GetY()+1,3,3);
                    $pdf->SetFont('Arial', 'B', '7');
                    $pdf->Cell($w[0], 6, $ApptDetail->groupid, 'LTR', 0, 'C', $fill);
                    $pdf->Cell($w[1], 6, $pr_name . $pdf->Image($tick_pr, $pdf->GetX() + $w[1] - 5, $pdf->GetY() + 1, 3, 3), 'LTR', 0, 'L', $fill);
                    $pdf->Cell($w[2], 6, $p1_name . $pdf->Image($tick_p1, $pdf->GetX() + $w[2] - 5, $pdf->GetY() + 1, 3, 3), 'LTR', 0, 'L', $fill);
                    $pdf->Cell($w[3], 6, $p1_name . $pdf->Image($tick_p1, $pdf->GetX() + $w[3] - 5, $pdf->GetY() + 1, 3, 3), 'LTR', 0, 'L', $fill);
                    $pdf->Ln(4);

                    $pdf->SetFont('Arial', 'B', '4.7');

                    $pdf->Cell($w[0], 4, '', 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[1], 4, $pr_desig, 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[2], 4, $p1_desig, 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[3], 4, $p1_desig, 'LR', 0, 'L', $fill);
                    $pdf->Ln(3);

                    $pdf->SetFont('Arial', 'B', '4.7');
                    $pdf->Cell($w[0], 4, '', 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[1], 4, $pr_join, 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[2], 4, $p1_join, 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[3], 4, $p1_join, 'LR', 0, 'L', $fill);
                    $pdf->Ln(3);

                    $pdf->SetFont('Arial', 'B', '4.7');
                    $pdf->Cell($w[0], 4, '', 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[1], 4, $pr_office, 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[2], 4, $p1_office, 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[3], 4, $p1_office, 'LR', 0, 'L', $fill);
                    $pdf->Ln(3);

                    $pdf->Cell($w[0], 4, '', 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[1], 4, $pr_ofc_address, 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[2], 4, $p1_ofc_address, 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[3], 4, $p1_ofc_address, 'LR', 0, 'L', $fill);
                    $pdf->Ln(3);

                    $pdf->Cell($w[0], 4, '', 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[1], 4, $pr_ofc_address1, 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[2], 4, $p1_ofc_address1, 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[3], 4, $p1_ofc_address1, 'LR', 0, 'L', $fill);
                    $pdf->Ln(3);

                    $pdf->Cell($w[0], 4, '', 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[1], 4, $pr_ofc_address2, 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[2], 4, $p1_ofc_address2, 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[3], 4, $p1_ofc_address2, 'LR', 0, 'L', $fill);
                    $pdf->Ln(3);

                    $pdf->SetFont('Arial', 'B', '4.7');
                    $pdf->Cell($w[0], 4, '', 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[1], 4, $pr_ofc_cd, 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[2], 4, $p1_ofc_cd, 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[3], 4, $p1_ofc_cd, 'LR', 0, 'L', $fill);
                    $pdf->Ln();


                    $pdf->SetFont('Arial', 'B', '7');
                    $pdf->Cell($w[0], 6, '', 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[1], 6, '', 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[2], 6, $p2_name . $pdf->Image($tick_p2, $pdf->GetX() + $w[2] - 5, $pdf->GetY() + 1, 3, 3), 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[3], 6, '', 'LR', 0, 'L', $fill);
                    $pdf->Ln(4);

                    $pdf->SetFont('Arial', 'B', '4.6');
                    $pdf->Cell($w[0], 4, '', 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[1], 4, '', 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[2], 4, $p2_desig, 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[3], 4, '', 'LR', 0, 'L', $fill);
                    $pdf->Ln(3);
                    $pdf->SetFont('Arial', 'B', '4.6');

                    $pdf->Cell($w[0], 4, '', 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[1], 4, '', 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[2], 4, $p2_join, 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[3], 4, '', 'LR', 0, 'L', $fill);
                    $pdf->Ln(3);

                    $pdf->SetFont('Arial', 'B', '4.6');
                    $pdf->Cell($w[0], 4, '', 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[1], 4, '', 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[2], 4, $p2_office, 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[3], 4, '', 'LR', 0, 'L', $fill);
                    $pdf->Ln(3);

                    $pdf->Cell($w[0], 4, '', 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[1], 4, '', 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[2], 4, $p2_ofc_address, 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[3], 4, '', 'LR', 0, 'L', $fill);
                    $pdf->Ln(3);

                    $pdf->Cell($w[0], 4, '', 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[1], 4, '', 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[2], 4, $p2_ofc_address1, 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[3], 4, '', 'LR', 0, 'L', $fill);
                    $pdf->Ln(3);

                    $pdf->Cell($w[0], 4, '', 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[1], 4, '', 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[2], 4, $p2_ofc_address2, 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[3], 4, '', 'LR', 0, 'L', $fill);
                    $pdf->Ln(3);

                    $pdf->Cell($w[0], 5, '', 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[1], 5, '', 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[2], 5, $p2_ofc_cd, 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[3], 5, '', 'LR', 0, 'L', $fill);
                    $pdf->Ln();
                    //memb 5//
                    // dd($ApptDetail->mem_no);
                    if ($ApptDetail->mem_no == 5 || $ApptDetail->mem_no == 6) {
                        $pdf->SetFont('Arial', 'B', '7');
                        $pdf->Cell($w[0], 6, '', 'LR', 0, 'L', $fill);
                        $pdf->Cell($w[1], 6, '', 'LR', 0, 'L', $fill);
                        $pdf->Cell($w[2], 6, $pa_name . $pdf->Image($tick_pa, $pdf->GetX() + $w[2] - 5, $pdf->GetY() + 1, 3, 3), 'LR', 0, 'L', $fill);
                        $pdf->Cell($w[3], 6, '', 'LR', 0, 'L', $fill);
                        $pdf->Ln(4);

                        $pdf->SetFont('Arial', 'B', '4.6');
                        $pdf->Cell($w[0], 4, '', 'LR', 0, 'L', $fill);
                        $pdf->Cell($w[1], 4, '', 'LR', 0, 'L', $fill);
                        $pdf->Cell($w[2], 4, $pa_desig, 'LR', 0, 'L', $fill);
                        $pdf->Cell($w[3], 4, '', 'LR', 0, 'L', $fill);
                        $pdf->Ln(3);

                        $pdf->SetFont('Arial', 'B', '4.6');

                        $pdf->Cell($w[0], 4, '', 'LR', 0, 'L', $fill);
                        $pdf->Cell($w[1], 4, '', 'LR', 0, 'L', $fill);
                        $pdf->Cell($w[2], 4, $pa_join, 'LR', 0, 'L', $fill);
                        $pdf->Cell($w[3], 4, '', 'LR', 0, 'L', $fill);
                        $pdf->Ln(3);

                        $pdf->SetFont('Arial', 'B', '4.6');
                        $pdf->Cell($w[0], 4, '', 'LR', 0, 'L', $fill);
                        $pdf->Cell($w[1], 4, '', 'LR', 0, 'L', $fill);
                        $pdf->Cell($w[2], 4, $pa_office, 'LR', 0, 'L', $fill);
                        $pdf->Cell($w[3], 4, '', 'LR', 0, 'L', $fill);
                        $pdf->Ln(3);

                        $pdf->Cell($w[0], 4, '', 'LR', 0, 'L', $fill);
                        $pdf->Cell($w[1], 4, '', 'LR', 0, 'L', $fill);
                        $pdf->Cell($w[2], 4, $pa_ofc_address, 'LR', 0, 'L', $fill);
                        $pdf->Cell($w[3], 4, '', 'LR', 0, 'L', $fill);
                        $pdf->Ln(3);

                        $pdf->Cell($w[0], 4, '', 'LR', 0, 'L', $fill);
                        $pdf->Cell($w[1], 4, '', 'LR', 0, 'L', $fill);
                        $pdf->Cell($w[2], 4, $pa_ofc_address1 . " " . $pa_ofc_address2, 'LR', 0, 'L', $fill);
                        $pdf->Cell($w[3], 4, '', 'LR', 0, 'L', $fill);
                        $pdf->Ln(3);

                        $pdf->Cell($w[0], 5, '', 'LR', 0, 'L', $fill);
                        $pdf->Cell($w[1], 5, '', 'LR', 0, 'L', $fill);
                        $pdf->Cell($w[2], 5, $pa_ofc_cd, 'LR', 0, 'L', $fill);
                        $pdf->Cell($w[3], 5, '', 'LR', 0, 'L', $fill);
                        $pdf->Ln();
                    }
                    //memb 6//
                    if ($ApptDetail->mem_no == 6) {
                        $pdf->SetFont('Arial', 'B', '7');
                        $pdf->Cell($w[0], 6, '', 'LR', 0, 'L', $fill);
                        $pdf->Cell($w[1], 6, '', 'LR', 0, 'L', $fill);
                        $pdf->Cell($w[2], 6, $pb_name . $pdf->Image($tick_pb, $pdf->GetX() + $w[2] - 5, $pdf->GetY() + 1, 3, 3), 'LR', 0, 'L', $fill);
                        $pdf->Cell($w[3], 6, '', 'LR', 0, 'L', $fill);
                        $pdf->Ln(4);

                        $pdf->SetFont('Arial', 'B', '4.6');
                        $pdf->Cell($w[0], 4, '', 'LR', 0, 'L', $fill);
                        $pdf->Cell($w[1], 4, '', 'LR', 0, 'L', $fill);
                        $pdf->Cell($w[2], 4, $pb_desig, 'LR', 0, 'L', $fill);
                        $pdf->Cell($w[3], 4, '', 'LR', 0, 'L', $fill);
                        $pdf->Ln(3);
                        $pdf->SetFont('Arial', 'B', '4.6');
                        $pdf->Cell($w[0], 4, '', 'LR', 0, 'L', $fill);
                        $pdf->Cell($w[1], 4, '', 'LR', 0, 'L', $fill);
                        $pdf->Cell($w[2], 4, $pb_join, 'LR', 0, 'L', $fill);
                        $pdf->Cell($w[3], 4, '', 'LR', 0, 'L', $fill);
                        $pdf->Ln(3);
                        $pdf->SetFont('Arial', 'B', '4.6');
                        $pdf->Cell($w[0], 4, '', 'LR', 0, 'L', $fill);
                        $pdf->Cell($w[1], 4, '', 'LR', 0, 'L', $fill);
                        $pdf->Cell($w[2], 4, $pb_office, 'LR', 0, 'L', $fill);
                        $pdf->Cell($w[3], 4, '', 'LR', 0, 'L', $fill);
                        $pdf->Ln(3);

                        $pdf->Cell($w[0], 4, '', 'LR', 0, 'L', $fill);
                        $pdf->Cell($w[1], 4, '', 'LR', 0, 'L', $fill);
                        $pdf->Cell($w[2], 4, $pb_ofc_address, 'LR', 0, 'L', $fill);
                        $pdf->Cell($w[3], 4, '', 'LR', 0, 'L', $fill);
                        $pdf->Ln(3);

                        $pdf->Cell($w[0], 4, '', 'LR', 0, 'L', $fill);
                        $pdf->Cell($w[1], 4, '', 'LR', 0, 'L', $fill);
                        $pdf->Cell($w[2], 4, $pb_ofc_address1 . " " . $pb_ofc_address2, 'LR', 0, 'L', $fill);
                        $pdf->Cell($w[3], 4, '', 'LR', 0, 'L', $fill);
                        $pdf->Ln(3);

                        $pdf->Cell($w[0], 5, '', 'LR', 0, 'L', $fill);
                        $pdf->Cell($w[1], 5, '', 'LR', 0, 'L', $fill);
                        $pdf->Cell($w[2], 5, $pb_ofc_cd, 'LR', 0, 'L', $fill);
                        $pdf->Cell($w[3], 5, '', 'LR', 0, 'L', $fill);
                        $pdf->Ln();
                    }

                    //memb 6//
                    $pdf->SetFont('Arial', 'B', '7');
                    $pdf->Cell($w[0], 6, '', 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[1], 6, '', 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[2], 6, $p3_name . $pdf->Image($tick_p3, $pdf->GetX() + $w[2] - 5, $pdf->GetY() + 1, 3, 3), 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[3], 6, '', 'LR', 0, 'L', $fill);
                    $pdf->Ln(4);

                    $pdf->SetFont('Arial', 'B', '4.6');
                    $pdf->Cell($w[0], 4, '', 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[1], 4, '', 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[2], 4, $p3_desig, 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[3], 4, '', 'LR', 0, 'L', $fill);
                    $pdf->Ln(3);
                    $pdf->SetFont('Arial', 'B', '4.6');
                    $pdf->Cell($w[0], 4, '', 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[1], 4, '', 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[2], 4, $p3_join, 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[3], 4, '', 'LR', 0, 'L', $fill);
                    $pdf->Ln(3);
                    $pdf->SetFont('Arial', 'B', '4.6');
                    $pdf->Cell($w[0], 4, '', 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[1], 4, '', 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[2], 4, $p3_office, 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[3], 4, '', 'LR', 0, 'L', $fill);
                    $pdf->Ln(3);

                    $pdf->Cell($w[0], 4, '', 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[1], 4, '', 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[2], 4, $p3_ofc_address, 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[3], 4, '', 'LR', 0, 'L', $fill);
                    $pdf->Ln(3);

                    $pdf->Cell($w[0], 4, '', 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[1], 4, '', 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[2], 4, $p3_ofc_address1, 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[3], 4, '', 'LR', 0, 'L', $fill);
                    $pdf->Ln(3);

                    $pdf->Cell($w[0], 4, '', 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[1], 4, '', 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[2], 4, $p3_ofc_address2, 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[3], 4, '', 'LR', 0, 'L', $fill);
                    $pdf->Ln(3);

                    $pdf->Cell($w[0], 5, '', 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[1], 5, '', 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[2], 5, $p3_ofc_cd, 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[3], 5, '', 'LR', 0, 'L', $fill);


                    $pdf->Ln();
                    $pdf->Cell(array_sum($w), 0, '', 1, 0, 'L', $fill);
                    //	$pdf->Ln(5);

                    $pdf->Ln(4);

                    $pdf->SetFont('Arial', '', 8.7);
                    $pdf->MultiCell(190, 4, "     " . $euname9 . $euname10 . $euname11 . ".", 0, 'J', $fill);

                    $pdf->Ln();


                    $pdf->SetFont('Arial', '', 9);
                    //$pdf->Cell(80);
                    $pdf->Cell(30, 10, $euname12, 0, 0, 'L');
                    $pdf->SetFont('Arial', '', 10);
                    $pdf->Cell(120);
                    $pdf->Cell(10, 10, "Signature", 0, 0, 'R');
                    // Line break
                    $pdf->Ln(8.3);

                    $pdf->SetFont('Arial', '', 9);
                    $pdf->Cell(30, 1, $euname13, 0, 0, 'L');
                    $pdf->SetFont('Arial', '', 10);
                    $pdf->Cell(110);
                    //	$pdf->Cell(10,10,"yuyu",0,0,'R');
                    $pdf->Cell(10, 10, $pdf->Image($signature, $pdf->GetX(), $pdf->GetY(), 30.78), 0, 0, 'R', false);
                    // Line break
                    $pdf->Ln(4);

                    $pdf->SetFont('Arial', '', 8);
                    $pdf->Cell(164);
                    //$pdf->Cell(10, 10, $nb3, 0, 0, 'R');
                    // Line break
                    $pdf->Ln(4);
                    $pdf->SetFont('Arial', '', 9);
                    $pdf->Cell(160);
                    $pdf->Cell(10, 12, $roname, 0, 0, 'R');

                    // Line break
                    $pdf->Ln(4);
                    $pdf->SetFont('Arial', '', 9);
                    $pdf->Cell(129);
                    $pdf->Cell(49, 12, $euname14, 0, 0, 'R');

                    // Line break
                    $pdf->Ln();
                    $pdf->Cell(190, 0, '', 1, 0, 'L', $fill);
                    $pdf->Ln();


                    $pdf->SetFont('Arial', 'B', 8.7);
                    $pdf->Ln(1);
                    $pdf->MultiCell(190, 4, $nb1 . $nb2, 0, 'J', $fill);
                    $pdf->Ln(1);
                    //$pdf->Ln();
                    $pdf->SetFont('Arial', '', 8.7);
                    $pdf->MultiCell(190, 4, "2. This is also to inform you that, a Facilitation Centre for submission of Postal Ballot will be made at your respective training centre.You may utilize the facility.", 0, 'J');
                    $pdf->Ln(1);



                    // $pdf->SetFont('Arial', '', 8.7);
                    // $pdf->MultiCell(190, 4, "3. The payment has been released.", 0, 'J');

                    //$bmname=$row['block_muni_cd'];
                    $pdf->Ln(13);
                    $bmname = ($assembly == '') ? "Block/Municipality: " . $ApptDetail->block_muni_name : "";
                    $pdf->SetFont('Arial', '', 9);
                    $pdf->Cell(190, 5, $bmname, 0, 0, 'C');

                    $count++;
                } else {

                    $fill = false;
                    $poll_date = $ApptDetail->polldate;
                    $poll_time = $ApptDetail->polltime;
                    $training_venue = $ApptDetail->training_venue;
                    $training_subvenue = $ApptDetail->sub_venuename;
                    $venue_addr = $ApptDetail->venue_addr;
                    $training_date = $ApptDetail->training_date;
                    $training_time = $ApptDetail->training_time;

                    $dc = ($ApptDetail->dc_venue != '' ? $ApptDetail->dc_venue . ", " . $ApptDetail->dc_address : "___________________________________");
                    $dc_date = ($ApptDetail->dc_date != '' ? $ApptDetail->dc_date : "___________");
                    $dc_time = ($ApptDetail->dc_time != '' ? $ApptDetail->dc_time : "___________");
                    $rcvenue = ($ApptDetail->rc_venue != '' ? $ApptDetail->rc_venue : "_______________________________");

                    $euname = "ELECTION URGENT";
                    $euname1 = "ORDER OF APPOINTMENT FOR POLLING DUTIES";
                    // $euname2 = "GENERAL PARLIAMENTARY ELECTION, 2019";
                    //$euname21="ASSEMBLY ELECTION, 2016";
                    $euname2 ="General Election to West Bengal Legislative Assembly, 2021";
                    $euname3 = "";
                    $euname22 = "Reserve Serial No. " . $ApptDetail->groupid;
                    $euname4 = "Order No: " . $ApptDetail->order_no;
                    $euname5 = "Date: " . date('d/m/Y', strtotime(trim(str_replace('/', '-', $ApptDetail->order_date))));

                    // $euname6 = "     In pursuance of sub-section(1) and sub-section(3) of section 26 of the Representation of the People Act,1963(43 of 1951), I hereby ";
                    $euname6 = "     In pursuance of sub-section(1) and sub-section(3) of section 26 of the Representation of the People Act,1951(43 of 1951), I hereby ";
                    $euname7 = "appoint the officers specified in column(2) and (3) of the table below as Presiding Officer and Polling Officers respectively for the Polling ";
                    $euname8 = "Party specified in corresponding entry in column(1) of the table provided by me for " . $ApptDetail->assembly . " - " . $ApptDetail->assembly_name . " L.A. Constituency ";
                    $euname78 = "forming part of " . $ApptDetail->pccd . " - " . $ApptDetail->pcname . " Parliamentary Constituency.";
                    $euname81 = "I also authorise the Polling Officer specified in column(4) of the table against that entry to perform the functions of the Presiding Officer";
                    $euname82 = "during the unavoidable absence, if any, of the Presiding Officer.";

                    $euname9 = "The Poll will be taken on " . $poll_date . " during the hours " . $poll_time . ". The Presiding Officer should arrange to collect the Polling ";
                    $euname10 = "materials from " . $dc . " on " . $dc_date . " at " . $dc_time . " ";
                    $euname11 = "and after the Poll, these should be returned to collecting centre at " . $rcvenue;

                    $euname12 = "Place: " . $ApptDetail->district;
                    $euname13 = "Date: " . date("d/m/Y");
                    // $euname14 = "District: " . $ApptDetail->district;
                    $euname14 = "Assembly: " . $ApptDetail->assembly.'-'.$ApptDetail->assembly_name;
                    $nb1 = "1. You are requested to attend the training at " . $training_venue . " " . $training_subvenue . " , " . $venue_addr;
                    $nb2 = " on " . $training_date . " from " . $training_time;
                    //$nb3 = "(__________________________)";
//	        $signature=$signaturePath.$ApptDetail->assembly.".jpg";
//		$roname="RO/".$ApptDetail->assembly." - ".$ApptDetail->assembly_name;
                    $signature = $signaturePath . $ApptDetail->assembly . ".jpg";
                    // $roname = "RO/" . $ApptDetail->pccd . " - " . $ApptDetail->pcname;
                    // $roname = "District Election Officer";
                    $roname = "Returning Officer";
                    $pdf->SetFont('Arial', 'B', 10);
                    $pdf->Cell(37, 6, $euname, 1, 0, 'L');
                    $pdf->SetFont('Arial', 'B', 10);
                    $pdf->Cell(20);
                    $pdf->Cell(84, 4, $euname1, 'B', 0, 'C');
                    $pdf->SetFont('Arial', 'B', 8);
                    $pdf->Cell(28);
                    $pdf->Cell(10, 3, $euname3, 0, 0, 'R');

                    // Line break
                    $pdf->Ln(6);

                    $pdf->SetFont('Arial', 'B', 8);
                    $pdf->Cell(15, 10, '', 0, 0, 'L');
                    $pdf->SetFont('Arial', '', 10);
                    $pdf->Cell(46);
                    $pdf->Cell(77, 4, $euname2, 'B', 0, 'C');
                    $pdf->SetFont('Arial', 'B', 8);
                    $pdf->Cell(12);
                    $pdf->SetFont('Arial', 'B', 10);
                    $pdf->Cell(37, 6, $euname22, 1, 0, 'R');

                    $pdf->Ln(5);

//                $pdf->SetFont('Arial','B',8);
//                $pdf->Cell(15,10,'',0,0,'L');
//                $pdf->SetFont('Arial','',10);
//                $pdf->Cell(60);
//                $pdf->Cell(47,4,$euname21,'B',0,'C');
//                $pdf->SetFont('Arial','B',8);
//                $pdf->Cell(13);
//                $pdf->SetFont('Arial','B',10);
//                $pdf->Cell(33,6,'',0,0,'R');
//                // Line break
//
                    $pdf->Ln(4);

                    $pdf->SetFont('Arial', '', 9);
                    $pdf->Cell(15, 10, $euname4, 0, 0, 'L');
                    $pdf->SetFont('Arial', '', 10);
                    $pdf->Cell(60);
                    $pdf->Cell(47, 4, '', '', 0, 'C');
                    $pdf->SetFont('Arial', '', 8);
                    $pdf->Cell(33);
                    $pdf->SetFont('Arial', '', 9);
                    $pdf->Cell(33, 6, $euname5, 0, 0, 'R');

                    $pdf->Ln(12);

                    $pdf->Ln();
                    $pdf->SetFont('Arial', '', 8.7);
                    $pdf->MultiCell(190, 4, $euname6 . $euname7 . $euname8 . $euname78, 0, 'J');

                    // Line break
                    $pdf->Ln(7);
                    $pdf->SetFont('Arial', '', 8.7);
                    $pdf->Cell(87);
                    $pdf->Cell(20, 10, $euname81, 0, 0, 'C');

                    // Line break
                    $pdf->Ln(4);
                    $pdf->SetFont('Arial', '', 8.7);
                    $pdf->Cell(50, 10, $euname82, 0, 0, 'L');

                    // Line break
                    $pdf->Ln(18);

                    $pdf->SetFillColor(255, 255, 255);
                    $pdf->SetDrawColor(0, 0, 0);
                    $pdf->SetLineWidth(.3);
                    $pdf->SetFont('Arial', 'B', '8');

                    $head = array('Polling', 'Name of the Presiding Officer', 'Name of the Polling Officers', 'Polling Officer authorised to perform');
                    $w = array(11, 59, 60, 60);
                    $head1 = array('Party', '', '', 'the functions of the Presiding Officer');
                    $head2 = array('No.', '', '', "in the latter's absence");
                    $head3 = array('(1)', '(2)', '(3)', "(4)");

                    //	$this->SetFont('Arial','',9);
                    for ($j = 0; $j < count($head); $j++)
                        $pdf->Cell($w[$j], 5, $head[$j], 'LTR', 0, 'C', true);
                    $pdf->Ln();
                    for ($j1 = 0; $j1 < count($head1); $j1++)
                        $pdf->Cell($w[$j1], 4, $head1[$j1], 'LR', 0, 'C', true);
                    $pdf->Ln();
                    for ($j2 = 0; $j2 < count($head2); $j2++)
                        $pdf->Cell($w[$j2], 5, $head2[$j2], 'LR', 0, 'C', true);
                    $pdf->Ln();

                    for ($j3 = 0; $j3 < count($head3); $j3++)
                        $pdf->Cell($w[$j3], 7, $head3[$j3], 1, 0, 'C', true);
                    $pdf->Ln();

                    if ($ApptDetail->per_poststat == "PR") {
                        $pr_name = $ApptDetail->pr_name;
                        $pr_desig = $ApptDetail->pr_designation;
                        $pr_code = $ApptDetail->pr_personcd;
                        $pr_office = $ApptDetail->pr_officename;
                        $pr_ofc_address = $ApptDetail->pr_officeaddress;
                        $pr_ofc_address1 = "P.O. - " . $ApptDetail->pr_postoffice . ", Subdiv. - " . $ApptDetail->pr_subdivision;
                        $pr_ofc_address2 = "Dist - " . $ApptDetail->pr_district;
                        $pr_ofc_cd = "Mobile No. - " . $ApptDetail->pr_mobno . ", OFFICE - (" . $ApptDetail->pr_officecd . ")";
                        $pr_post_stat = $ApptDetail->pr_post_stat;
                        $pr_join = $pr_post_stat . " PIN - (" . $pr_code . ")";

                        $p1_name = "";
                        $p1_desig = "";
                        $p1_code = "";
                        $p1_office = "";
                        $p1_ofc_address = "";
                        $p1_ofc_address1 = "";
                        $p1_ofc_address2 = "";
                        $p1_ofc_cd = "";
                        $p1_post_stat = "";
                        $p1_join = "";

                        $pp_name = "";
                        $pp_desig = "";
                        $pp_code = "";
                        $pp_office = "";
                        $pp_ofc_address = "";
                        $pp_ofc_address1 = "";
                        $pp_ofc_address2 = "";
                        $pp_ofc_cd = "";
                        $pp_post_stat = "";
                        $pp_join = "";
                    }
                    if ($ApptDetail->per_poststat == "P1" || $ApptDetail->per_poststat == "P2" || $ApptDetail->per_poststat == "P3" || $ApptDetail->per_poststat == "PA" || $ApptDetail->per_poststat == "PB") {
                        if ($ApptDetail->per_poststat == "P1") {
                            $p1_name = $ApptDetail->p1_name;
                            $p1_desig = $ApptDetail->p1_designation;
                            $p1_code = $ApptDetail->p1_personcd;
                            $p1_office = $ApptDetail->p1_officename;
                            $p1_ofc_address = $ApptDetail->p1_officeaddress;
                            $p1_ofc_address1 = "P.O. - " . $ApptDetail->p1_postoffice . ", Subdiv. - " . $ApptDetail->p1_subdivision;
                            $p1_ofc_address2 = "Dist - " . $ApptDetail->p1_district;
                            $p1_ofc_cd = "Mobile No. - " . $ApptDetail->p1_mobno . ", OFFICE - (" . $ApptDetail->p1_officecd . ")";
                            $p1_post_stat = $ApptDetail->p1_post_stat;
                            $p1_join = $p1_post_stat . " PIN - (" . $p1_code . ")";
                        }
                        if ($ApptDetail->per_poststat == "P2") {
                            $p1_name = $ApptDetail->p2_name;
                            $p1_desig = $ApptDetail->p2_designation;
                            $p1_code = $ApptDetail->p2_personcd;
                            $p1_office = $ApptDetail->p2_officename;
                            $p1_ofc_address = $ApptDetail->p2_officeaddress;
                            $p1_ofc_address1 = "P.O. - " . $ApptDetail->p2_postoffice . ", Subdiv. - " . $ApptDetail->p2_subdivision;
                            $p1_ofc_address2 = "Dist - " . $ApptDetail->p2_district;
                            $p1_ofc_cd = "Mobile No. - " . $ApptDetail->p2_mobno . ", OFFICE - (" . $ApptDetail->p2_officecd . ")";
                            $p1_post_stat = $ApptDetail->p2_post_stat;
                            $p1_join = $p1_post_stat . " PIN - (" . $p1_code . ")";
                        }
                        if ($ApptDetail->per_poststat == "P3") {
                            $p1_name = $ApptDetail->p3_name;
                            $p1_desig = $ApptDetail->p3_designation;
                            $p1_code = $ApptDetail->p3_personcd;
                            $p1_office = $ApptDetail->p3_officename;
                            $p1_ofc_address = $ApptDetail->p3_officeaddress;
                            $p1_ofc_address1 = "P.O. - " . $ApptDetail->p3_postoffice . ", Subdiv. - " . $ApptDetail->p3_subdivision;
                            $p1_ofc_address2 = "Dist - " . $ApptDetail->p3_district;
                            $p1_ofc_cd = "Mobile No. - " . $ApptDetail->p3_mobno . ", OFFICE - (" . $ApptDetail->p3_officecd . ")";
                            $p1_post_stat = $ApptDetail->p3_post_stat;
                            $p1_join = $p1_post_stat . " PIN - (" . $p1_code . ")";
                        }

                        if ($ApptDetail->pa_name != '') {
                            $p1_name = $ApptDetail->pa_name;
                            $p1_desig = $ApptDetail->pa_designation;
                            $p1_code = $ApptDetail->pa_personcd;
                            $p1_office = $ApptDetail->pa_officename;
                            $p1_ofc_address = $ApptDetail->pa_officeaddress;
                            $p1_ofc_address1 = "P.O. - " . $ApptDetail->pa_postoffice . ", Subdiv. - " . $ApptDetail->pa_subdivision;
                            $p1_ofc_address1 = "Dist - " . $ApptDetail->pa_district;
                            $p1_ofc_cd = "Mobile No. - " . $ApptDetail->pa_mobno . ", OFFICE - (" . $ApptDetail->pa_officecd . ")";
                            $p1_post_stat = $ApptDetail->pa_post_stat;
                            $p1_join = $ApptDetail->pa_post_stat . " PIN - (" . $p1_code . ")";
                        }

                        if ($ApptDetail->pb_name != '') {
                            $p1_name = $ApptDetail->pb_name;
                            $p1_desig = $ApptDetail->pb_designation;
                            $p1_code = $ApptDetail->pb_personcd;
                            $p1_office = $ApptDetail->pb_officename;
                            $p1_ofc_address = $ApptDetail->pb_officeaddress;
                            $p1_ofc_address1 = "P.O. - " . $ApptDetail->pb_postoffice . ", Subdiv. - " . $ApptDetail->pb_subdivision;
                            $p1_ofc_address2 = "Dist - " . $ApptDetail->pb_district;
                            $p1_ofc_cd = "Mobile No. - " . $ApptDetail->pb_mobno . ", OFFICE - (" . $ApptDetail->pb_officecd . ")";
                            $p1_post_stat = $ApptDetail->pb_post_stat;
                            $p1_join = $p1_post_stat . " PIN - (" . $p1_code . ")";
                        }


                        $pr_name = '';
                        $pr_desig = '';
                        $pr_code = '';
                        $pr_office = '';
                        $pr_ofc_address = '';
                        $pr_ofc_address1 = '';
                        $pr_ofc_address2 = '';
                        $pr_ofc_cd = '';
                        $pr_post_stat = '';
                        $pr_join = '';
                        if ($ApptDetail->per_poststat != 'P1') {
                            $pp_name = '';
                            $pp_desig = '';
                            $pp_code = '';
                            $pp_office = '';
                            $pp_ofc_address = '';
                            $pp_ofc_address1 = '';
                            $pp_ofc_address2 = '';
                            $pp_ofc_cd = '';
                            $pp_post_stat = '';
                            $pp_join = '';
                        }
                    }
                    if ($ApptDetail->per_poststat == "P1") {
                        $pp_name = $ApptDetail->p1_name;
                        $pp_desig = $ApptDetail->p1_designation;
                        $pp_code = $ApptDetail->p1_personcd;
                        $pp_office = $ApptDetail->p1_officename;
                        $pp_ofc_address = $ApptDetail->p1_officeaddress;
                        $pp_ofc_address1 = "P.O. - " . $ApptDetail->p1_postoffice . ", Subdiv. - " . $ApptDetail->p1_subdivision;
                        $pp_ofc_address2 = "Dist - " . $ApptDetail->p1_district;
                        $pp_ofc_cd = "Mobile No. - " . $ApptDetail->p1_mobno . ", OFFICE - (" . $ApptDetail->p1_officecd . ")";
                        $pp_post_stat = $ApptDetail->p1_post_stat;
                        $pp_join = $p1_post_stat . " PIN - (" . $p1_code . ")";

                        $pr_name = '';
                        $pr_desig = '';
                        $pr_code = '';
                        $pr_office = '';
                        $pr_ofc_address = '';
                        $pr_ofc_address1 = '';
                        $pr_ofc_address2 = '';
                        $pr_ofc_cd = '';
                        $pr_post_stat = '';
                        $pr_join = '';
                    }



                    $pdf->SetFont('Arial', 'B', '7');
                    $pdf->Cell($w[0], 6, $ApptDetail->groupid, 'LTR', 0, 'C', $fill);
                    $pdf->Cell($w[1], 6, $pr_name, 'LTR', 0, 'L', $fill);
                    $pdf->Cell($w[2], 6, $p1_name, 'LTR', 0, 'L', $fill);
                    $pdf->Cell($w[3], 6, $pp_name, 'LTR', 0, 'L', $fill);
                    $pdf->Ln(4);

                    $pdf->SetFont('Arial', 'B', '4.7');

                    $pdf->Cell($w[0], 4, '', 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[1], 4, $pr_desig, 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[2], 4, $p1_desig, 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[3], 4, $pp_desig, 'LR', 0, 'L', $fill);
                    $pdf->Ln(3);

                    $pdf->SetFont('Arial', 'B', '4.7');
                    $pdf->Cell($w[0], 4, '', 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[1], 4, $pr_join, 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[2], 4, $p1_join, 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[3], 4, $pp_join, 'LR', 0, 'L', $fill);
                    $pdf->Ln(3);

                    $pdf->SetFont('Arial', 'B', '4.7');
                    $pdf->Cell($w[0], 4, '', 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[1], 4, $pr_office, 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[2], 4, $p1_office, 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[3], 4, $pp_office, 'LR', 0, 'L', $fill);
                    $pdf->Ln(3);

                    $pdf->Cell($w[0], 4, '', 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[1], 4, $pr_ofc_address, 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[2], 4, $p1_ofc_address, 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[3], 4, $pp_ofc_address, 'LR', 0, 'L', $fill);
                    $pdf->Ln(3);

                    $pdf->Cell($w[0], 4, '', 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[1], 4, $pr_ofc_address1, 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[2], 4, $p1_ofc_address1, 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[3], 4, $pp_ofc_address1, 'LR', 0, 'L', $fill);
                    $pdf->Ln(3);

                    $pdf->Cell($w[0], 4, '', 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[1], 4, $pr_ofc_address2, 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[2], 4, $p1_ofc_address2, 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[3], 4, $pp_ofc_address2, 'LR', 0, 'L', $fill);
                    $pdf->Ln(3);

                    $pdf->SetFont('Arial', 'B', '4.7');
                    $pdf->Cell($w[0], 4, '', 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[1], 4, $pr_ofc_cd, 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[2], 4, $p1_ofc_cd, 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[3], 4, $pp_ofc_cd, 'LR', 0, 'L', $fill);
                    $pdf->Ln();



                    // $pdf->Ln();
                    $pdf->Cell(array_sum($w), 0, '', 1, 0, 'L', $fill);
                    //	$pdf->Ln(5);

                    $pdf->Ln(4);

                    $pdf->SetFont('Arial', '', 8.7);
                    $pdf->MultiCell(190, 4, "     " . $euname9 . $euname10 . $euname11 . ".", 0, 'J', $fill);

                    $pdf->Ln(18);


                    $pdf->SetFont('Arial', '', 9);
                    //$pdf->Cell(80);
                    $pdf->Cell(30, 10, $euname12, 0, 0, 'L');
                    $pdf->SetFont('Arial', '', 10);
                    $pdf->Cell(120);
                    $pdf->Cell(10, 10, "Signature", 0, 0, 'R');
                    // Line break
                    $pdf->Ln(9);

                    $pdf->SetFont('Arial', '', 9);
                    $pdf->Cell(30, 1, $euname13, 0, 0, 'L');
                    $pdf->SetFont('Arial', '', 10);
                    $pdf->Cell(110);
                    //	$pdf->Cell(10,10,"yuyu",0,0,'R');
                    $pdf->Cell(10, 10, $pdf->Image($signature, $pdf->GetX(), $pdf->GetY(), 30.78), 0, 0, 'R', false);
                    // Line break
                    $pdf->Ln(4);

                    $pdf->SetFont('Arial', '', 8);
                    $pdf->Cell(164);
                    // $pdf->Cell(10, 10, $nb3, 0, 0, 'R');
                    // Line break
                    $pdf->Ln(4);
                    $pdf->SetFont('Arial', '', 9);
                    $pdf->Cell(160);
                    $pdf->Cell(10, 12, $roname, 0, 0, 'R');

                    // Line break
                    $pdf->Ln(4);
                    $pdf->SetFont('Arial', '', 9);
                    $pdf->Cell(129);
                    $pdf->Cell(49, 12, $euname14, 0, 0, 'R');

                    // Line break
                    $pdf->Ln();
                    $pdf->Cell(190, 0, '', 1, 0, 'L', $fill);
                    $pdf->Ln();


                    $pdf->SetFont('Arial', 'B', 8.7);
                    $pdf->Ln(4);
                    $pdf->MultiCell(190, 4, $nb1 . $nb2, 0, 'J', $fill);
                    $pdf->Ln(4);
                    //$pdf->Ln();
                    $pdf->SetFont('Arial', '', 8.7);
                    $pdf->MultiCell(190, 4, "2. This is also to inform you that, a Facilitation Centre for submission of Postal Ballot will be made at your respective training centre.You may utilize the facility.", 0, 'J');
                    $pdf->Ln(1);

                    // $pdf->SetFont('Arial', '', 8.7);
                    // $pdf->MultiCell(190, 4, "3. The payment has been released.", 0, 'J');
                    $pdf->Ln(13);
                    // dd($ApptDetail->block_muni_name);

                    // $bmname = "ddddd";
                    $bmname = ($assembly == '') ? "Block/Municipality: " . $ApptDetail->block_muni_name : "";
                    // $bmname = ($assembly != '') ? "Block/Municipality: " . $ApptDetail->block_muni_name : "";
                    $pdf->SetFont('Arial', '', 9);
                    $pdf->Cell(190, 5, $bmname, 0, 0, 'C');

                    $count++;
                }
            }
            if ($count == $per_page) {
                $per_page = $per_page + 1;
                if ($count != Count($ApptRecord)) {
                    $pdf->AddPage();
                }
            }
        }

        $pdf->Output();
        exit;
    }

    //::::::::::::::::::::::: Second Appt Letter PDF 6 (PC Election):::::::::::::::::::://
    public function getSecondAppointmentLetterPDF62019(Request $request) {
        $validate_array = ['forZone' => 'required|alpha_num|min:4|max:4',
            'party_reserve' => 'nullable|alpha|min:1|max:1',
            'assembly' => 'nullable|alpha_num|min:3|max:3',
            'group_id' => 'nullable|digits_between:1,4',
            'no_of_member' => 'nullable|digits_between:1,1',
            'from' => 'required|digits_between:1,4',
            'to' => 'required|digits_between:1,4',
            'blockMunicipality' => 'nullable|alpha_num',
            'poststat' => 'nullable|alpha_num'
        ];

        $validate_array1 = ['forZone.required' => 'For Zone is required',
            'forZone.alpha_num' => 'For Zone must be an alpha numeric characters',
            'party_reserve.required' => 'Party/Reserve is required',
            'party_reserve.alpha' => 'Party/Reserve must be an alpha characters',
            'assembly.alpha_num' => 'Assembly must be an alpha numeric characters',
            'group_id.digits_between' => 'Group ID must be an integer',
            'no_of_member.digits_between' => 'No of Member must be an integer',
            'from.required' => 'Records From is required',
            'from.digits_between' => 'Records From must be an integer',
            'to.required' => 'Records To is required',
            'to.digits_between' => 'Records To must be an integer',
            'blockMunicipality.alpha_num' => 'Block must be an alpha numeric characters',
            'poststat.alpha_num' => 'Poststat must be an alpha numeric characters'
        ];

        $this->validate($request, $validate_array, $validate_array1);

        $forzone = $request->forZone;
        $party_reserve = $request->party_reserve;
        $blockMunicipality = $request->blockMunicipality;
        $assembly = $request->assembly;
        $group_id = $request->group_id;
        $no_of_member = $request->no_of_member;
        $poststat = $request->poststat;
        $orderno = $request->orderno;
        $pfrom = $request->from;
        $pto = $request->to;
        $count = 0;
        $tick = 'images/tick.png';
        if ($assembly == '') {
            $ApptRecord = DB::select('call getSecondAppointmentLetterPDF45(?,?,?,?,?,?,?,?,?,?)', [$forzone, $party_reserve, $assembly, $group_id, $no_of_member, $pfrom, $pto, $blockMunicipality, $poststat, $orderno]);
        } else {
            $ApptRecord = DB::select('call getSecondAppointmentLetterPDF45Assembly(?,?,?,?,?,?,?,?,?,?)', [$forzone, $party_reserve, $assembly, $group_id, $no_of_member, $pfrom, $pto, $blockMunicipality, $poststat, $orderno]);
        }

        // $ApptRecord = DB::select('call getSecondAppointmentLetterPDF45(?,?,?,?,?,?,?,?,?,?)', [$forzone, $party_reserve, $assembly, $group_id, $no_of_member, $pfrom, $pto,$blockMunicipality,$poststat,$orderno]);
        $pdf = app('FPDF');
        $pdf->AddPage();
        $count = 0;
        $per_page = 1;
        $signaturePath = config('constants.DEOSIGNATURE');
        foreach ($ApptRecord as $ApptDetail) {
            $tick_p1 = 'images/blank.png';
            $tick_p2 = 'images/blank.png';
            $tick_p3 = 'images/blank.png';
            $tick_pa = 'images/blank.png';
            $tick_pb = 'images/blank.png';
            $tick_pr = 'images/blank.png';
            if ($count < $per_page) {
                $fill = false;
                $poll_date = $ApptDetail->polldate;
                $poll_time = $ApptDetail->polltime;
                $training_venue = $ApptDetail->training_venue;
                $training_subvenue = $ApptDetail->sub_venuename;
                $venue_addr = $ApptDetail->venue_addr;
                $training_date = $ApptDetail->training_date;
                $training_time = $ApptDetail->training_time;

                $dc = ($ApptDetail->dc_venue != '' ? $ApptDetail->dc_venue . ", " . $ApptDetail->dc_address : "___________________________________");
                $dc_date = ($ApptDetail->dc_date != '' ? $ApptDetail->dc_date : "___________");
                $dc_time = ($ApptDetail->dc_time != '' ? $ApptDetail->dc_time : "___________");
                $rcvenue = ($ApptDetail->rc_venue != '' ? $ApptDetail->rc_venue : "_______________________________");

                $euname = "ELECTION URGENT";
                $euname1 = "ORDER OF APPOINTMENT FOR POLLING DUTIES";
                $euname2 = "GENERAL ELECTION TO WEST BENGAL LEGISLATIVE";
                $euname21 = "ASSEMBLY ELECTION, 2016";
                $euname3 = ($assembly == '') ? $ApptDetail->pers_off . "/" . $ApptDetail->per_poststat : "";
                $euname22 = "Polling Party No. " . $ApptDetail->groupid;
                $euname4 = "Order No: " . $ApptDetail->order_no;
                $euname5 = "Date: " . date('d/m/Y', strtotime(trim(str_replace('/', '-', $ApptDetail->order_date))));

                $euname6 = "     In persuance of sub-section(1) and sub-section(3) of section 26 of the Representation of the People Act,1963(43 of 1951), I hereby ";
                $euname7 = "appoint the officers specified in column(2) and (3) of the table below as Presiding Officer and Polling Officers respectively for the Polling ";
                $euname8 = "Party specified in corresponding entry in column(1) of the table provided by me for " . $ApptDetail->assembly . " - " . $ApptDetail->assembly_name . " L.A. Constituency ";
                $euname78 = "forming part of " . $ApptDetail->pccd . " - " . $ApptDetail->pcname . " Parliamentary Constituency.";
                $euname81 = "I also authorise the Polling Officer specified in column(4) of the table against that entry to perform the functions of the Presiding Officer";
                $euname82 = "during the unavoidable absence, if any, of the Presiding Officer.";

                $euname9 = "The Poll will be taken on " . $poll_date . " during the hours " . $poll_time . ". The Presiding Officer should arrange to collect the Polling ";
                $euname10 = "materials from " . $dc . " on " . $dc_date . " at " . $dc_time . " ";
                $euname11 = "and after the Poll, these should be returned to collecting centre at " . $rcvenue;

                $euname12 = "Place: " . $ApptDetail->district;
                $euname13 = "Date: " . date("d/m/Y");
                $euname14 = "District: " . $ApptDetail->district;
                // $euname14 = "Assembly: " . $ApptDetail->assembly .'-'.$ApptDetail->assembly_name;
                $nb1 = "1. You are requested to attend the training at " . $training_venue . " " . $training_subvenue . " , " . $venue_addr;
                $nb2 = " on " . $training_date . " from " . $training_time;
                //$nb3 = "(__________________________)";
//	        $signature=$signaturePath.$ApptDetail->assembly.".jpg";
//		$roname="RO/".$ApptDetail->assembly." - ".$ApptDetail->assembly_name;
                $signature = $signaturePath . $ApptDetail->pccd . ".jpg";
                //$roname = "RO/" . $ApptDetail->pccd . " - " . $ApptDetail->pcname;
                $roname = "District Election Officer";
                $pdf->SetFont('Arial', 'B', 10);
                $pdf->Cell(37, 6, $euname, 1, 0, 'L');
                $pdf->SetFont('Arial', 'B', 10);
                $pdf->Cell(20);
                $pdf->Cell(84, 4, $euname1, 'B', 0, 'C');
                $pdf->SetFont('Arial', 'B', 8);
                $pdf->Cell(28);
                $pdf->Cell(10, 3, $euname3, 0, 0, 'R');

                // Line break
                $pdf->Ln(6);

                $pdf->SetFont('Arial', 'B', 8);
                $pdf->Cell(15, 10, '', 0, 0, 'L');
                $pdf->SetFont('Arial', '', 10);
                $pdf->Cell(38);
                $pdf->Cell(92, 4, $euname2, 'B', 0, 'C');
                $pdf->SetFont('Arial', 'B', 8);
                $pdf->Cell(8);
                $pdf->SetFont('Arial', 'B', 10);
                $pdf->Cell(37, 6, $euname22, 1, 0, 'R');

                $pdf->Ln(5);

                $pdf->SetFont('Arial', 'B', 8);
                $pdf->Cell(15, 10, '', 0, 0, 'L');
                $pdf->SetFont('Arial', '', 10);
                $pdf->Cell(60);
                $pdf->Cell(47, 4, $euname21, 'B', 0, 'C');
                $pdf->SetFont('Arial', 'B', 8);
                $pdf->Cell(13);
                $pdf->SetFont('Arial', 'B', 10);
                $pdf->Cell(33, 6, '', 0, 0, 'R');
                // Line break

                $pdf->Ln(4);

                $pdf->SetFont('Arial', '', 9);
                $pdf->Cell(15, 10, $euname4, 0, 0, 'L');
                $pdf->SetFont('Arial', '', 10);
                $pdf->Cell(60);
                $pdf->Cell(47, 4, '', '', 0, 'C');
                $pdf->SetFont('Arial', '', 8);
                $pdf->Cell(33);
                $pdf->SetFont('Arial', '', 9);
                $pdf->Cell(33, 6, $euname5, 0, 0, 'R');

                $pdf->Ln(6);

                $pdf->Ln();
                $pdf->SetFont('Arial', '', 8.7);
                $pdf->MultiCell(190, 4, $euname6 . $euname7 . $euname8 . $euname78, 0, 'J');

                $pdf->SetFont('Arial', '', 8.7);
                $pdf->Cell(87);
                $pdf->Cell(20, 10, $euname81, 0, 0, 'C');

                // Line break
                $pdf->Ln(4);
                $pdf->SetFont('Arial', '', 8.7);
                $pdf->Cell(50, 10, $euname82, 0, 0, 'L');

                // Line break
                $pdf->Ln(10);

                $pdf->SetFillColor(255, 255, 255);
                $pdf->SetDrawColor(0, 0, 0);
                $pdf->SetLineWidth(.3);
                $pdf->SetFont('Arial', 'B', '8');

                $head = array('Polling', 'Name of the Presiding Officer', 'Name of the Polling Officers', 'Polling Officer authorised to perform');
                $w = array(11, 59, 60, 60);
                $head1 = array('Party', '', '', 'the functions of the Presiding Officer');
                $head2 = array('No.', '', '', "in the latter's absence");
                $head3 = array('(1)', '(2)', '(3)', "(4)");

                //	$this->SetFont('Arial','',9);
                for ($j = 0; $j < count($head); $j++)
                    $pdf->Cell($w[$j], 5, $head[$j], 'LTR', 0, 'C', true);
                $pdf->Ln();
                for ($j1 = 0; $j1 < count($head1); $j1++)
                    $pdf->Cell($w[$j1], 4, $head1[$j1], 'LR', 0, 'C', true);
                $pdf->Ln();
                for ($j2 = 0; $j2 < count($head2); $j2++)
                    $pdf->Cell($w[$j2], 5, $head2[$j2], 'LR', 0, 'C', true);
                $pdf->Ln();

                for ($j3 = 0; $j3 < count($head3); $j3++)
                    $pdf->Cell($w[$j3], 7, $head3[$j3], 1, 0, 'C', true);
                $pdf->Ln();

                $pr_name = $ApptDetail->pr_name;
                $pr_desig = $ApptDetail->pr_designation;
                $pr_code = $ApptDetail->pr_personcd;
                $pr_office = $ApptDetail->pr_officename;
                $pr_ofc_address = $ApptDetail->pr_officeaddress;
                $pr_ofc_address1 = "P.O. - " . $ApptDetail->pr_postoffice . ", Subdiv. - " . $ApptDetail->pr_subdivision . ", " . $ApptDetail->district;
                $pr_ofc_cd = "Mobile No. - " . $ApptDetail->pr_mobno . ", OFFICE - (" . $ApptDetail->pr_officecd . ")";
                $pr_post_stat = $ApptDetail->pr_post_stat;
                $pr_join = $pr_post_stat . " PIN - (" . $pr_code . ")";

                $p1_name = "1. " . $ApptDetail->p1_name;
                $p1_desig = $ApptDetail->p1_designation;
                $p1_code = $ApptDetail->p1_personcd;
                $p1_office = $ApptDetail->p1_officename;
                $p1_ofc_address = $ApptDetail->p1_officeaddress;
                $p1_ofc_address1 = "P.O. - " . $ApptDetail->p1_postoffice . ", Subdiv. - " . $ApptDetail->p1_subdivision . ", " . $ApptDetail->district;
                $p1_ofc_cd = "Mobile No. - " . $ApptDetail->p1_mobno . ", OFFICE - (" . $ApptDetail->p1_officecd . ")";
                $p1_post_stat = $ApptDetail->p1_post_stat;
                $p1_join = $p1_post_stat . " PIN - (" . $p1_code . ")";

                $p2_name = "2. " . $ApptDetail->p2_name;
                $p2_desig = $ApptDetail->p2_designation;
                $p2_code = $ApptDetail->p2_personcd;
                $p2_office = $ApptDetail->p2_officename;
                $p2_ofc_address = $ApptDetail->p2_officeaddress;
                $p2_ofc_address1 = "P.O. - " . $ApptDetail->p2_postoffice . ", Subdiv. - " . $ApptDetail->p2_subdivision . ", " . $ApptDetail->district;
                $p2_ofc_cd = "Mobile No. - " . $ApptDetail->p2_mobno . ", OFFICE - (" . $ApptDetail->p2_officecd . ")";
                $p2_post_stat = $ApptDetail->p2_post_stat;
                $p2_join = $p2_post_stat . " PIN - (" . $p2_code . ")";

                $sl = (($ApptDetail->pa_name == '') ? "3. " : (($ApptDetail->pb_name == '') ? "4. " : "5. "));

                $p3_name = $sl . $ApptDetail->p3_name;
                $p3_desig = $ApptDetail->p3_designation;
                $p3_code = $ApptDetail->p3_personcd;
                $p3_office = $ApptDetail->p3_officename;
                $p3_ofc_address = $ApptDetail->p3_officeaddress;
                $p3_ofc_address1 = "P.O. - " . $ApptDetail->p3_postoffice . ", Subdiv. - " . $ApptDetail->p3_subdivision . ", " . $ApptDetail->district;
                $p3_ofc_cd = "Mobile No. - " . $ApptDetail->p3_mobno . ", OFFICE - (" . $ApptDetail->p3_officecd . ")";
                $p3_post_stat = $ApptDetail->p3_post_stat;
                $p3_join = $p3_post_stat . " PIN - (" . $p3_code . ")";


                if ($ApptDetail->pa_name != '') {
                    $pa_name = "3. " . $ApptDetail->pa_name;
                    $pa_desig = $ApptDetail->pa_designation;
                    $pa_code = $ApptDetail->pa_personcd;
                    $pa_office = $ApptDetail->pa_officename;
                    $pa_ofc_address = $ApptDetail->pa_officeaddress;
                    $pa_ofc_address1 = "P.O. - " . $ApptDetail->pa_postoffice . ", Subdiv. - " . $ApptDetail->pa_subdivision . ", " . $ApptDetail->district;
                    $pa_ofc_cd = "Mobile No. - " . $ApptDetail->pa_mobno . ", OFFICE - (" . $ApptDetail->pa_officecd . ")";
                    $pa_post_stat = $ApptDetail->pa_post_stat;
                    $pa_join = $pa_post_stat . " PIN - (" . $pa_code . ")";
                }

                if ($ApptDetail->pb_name != '') {
                    $pb_name = "4. " . $ApptDetail->pb_name;
                    $pb_desig = $ApptDetail->pb_designation;
                    $pb_code = $ApptDetail->pb_personcd;
                    $pb_office = $ApptDetail->pb_officename;
                    $pb_ofc_address = $ApptDetail->pb_officeaddress;
                    $pb_ofc_address1 = "P.O. - " . $ApptDetail->pb_postoffice . ", Subdiv. - " . $ApptDetail->pb_subdivision . ", " . $ApptDetail->district;
                    $pb_ofc_cd = "Mobile No. - " . $ApptDetail->pb_mobno . ", OFFICE - (" . $ApptDetail->pb_officecd . ")";
                    $pb_post_stat = $ApptDetail->pb_post_stat;
                    $pb_join = $pb_post_stat . " PIN - (" . $pb_code . ")";
                }
                if ($ApptDetail->per_poststat == 'P1' && $assembly == '') {
                    $tick_p1 = $tick;
                } else if ($ApptDetail->per_poststat == 'P2' && $assembly == '') {
                    $tick_p2 = $tick;
                } else if ($ApptDetail->per_poststat == 'P3' && $assembly == '') {
                    $tick_p3 = $tick;
                } else if ($ApptDetail->per_poststat == 'PA' && $assembly == '') {
                    $tick_pa = $tick;
                } else if ($ApptDetail->per_poststat == 'PB' && $assembly == '') {
                    $tick_pb = $tick;
                } else if ($ApptDetail->per_poststat == 'PR' && $assembly == '') {
                    $tick_pr = $tick;
                }
                $pdf->SetFont('Arial', 'B', '7');
                $pdf->Cell($w[0], 6, $ApptDetail->groupid, 'LTR', 0, 'C', $fill);
//                $pdf->Cell($w[1], 6, $pr_name, 'LTR', 0, 'L', $fill);
//                $pdf->Cell($w[2], 6, $p1_name, 'LTR', 0, 'L', $fill);
//                $pdf->Cell($w[3], 6, $p1_name, 'LTR', 0, 'L', $fill);
                $pdf->Cell($w[1], 6, $pr_name . $pdf->Image($tick_pr, $pdf->GetX() + $w[1] - 5, $pdf->GetY() + 1, 3, 3), 'LTR', 0, 'L', $fill);
                $pdf->Cell($w[2], 6, $p1_name . $pdf->Image($tick_p1, $pdf->GetX() + $w[2] - 5, $pdf->GetY() + 1, 3, 3), 'LTR', 0, 'L', $fill);
                $pdf->Cell($w[3], 6, $p1_name . $pdf->Image($tick_p1, $pdf->GetX() + $w[3] - 5, $pdf->GetY() + 1, 3, 3), 'LTR', 0, 'L', $fill);
                $pdf->Ln(4);

                $pdf->SetFont('Arial', 'B', '4.7');

                $pdf->Cell($w[0], 4, '', 'LR', 0, 'L', $fill);
                $pdf->Cell($w[1], 4, $pr_desig, 'LR', 0, 'L', $fill);
                $pdf->Cell($w[2], 4, $p1_desig, 'LR', 0, 'L', $fill);
                $pdf->Cell($w[3], 4, $p1_desig, 'LR', 0, 'L', $fill);
                $pdf->Ln(3);

                $pdf->SetFont('Arial', 'B', '4.7');
                $pdf->Cell($w[0], 4, '', 'LR', 0, 'L', $fill);
                $pdf->Cell($w[1], 4, $pr_join, 'LR', 0, 'L', $fill);
                $pdf->Cell($w[2], 4, $p1_join, 'LR', 0, 'L', $fill);
                $pdf->Cell($w[3], 4, $p1_join, 'LR', 0, 'L', $fill);
                $pdf->Ln(3);

                $pdf->SetFont('Arial', 'B', '4.7');
                $pdf->Cell($w[0], 4, '', 'LR', 0, 'L', $fill);
                $pdf->Cell($w[1], 4, $pr_office, 'LR', 0, 'L', $fill);
                $pdf->Cell($w[2], 4, $p1_office, 'LR', 0, 'L', $fill);
                $pdf->Cell($w[3], 4, $p1_office, 'LR', 0, 'L', $fill);
                $pdf->Ln(3);

                $pdf->Cell($w[0], 4, '', 'LR', 0, 'L', $fill);
                $pdf->Cell($w[1], 4, $pr_ofc_address, 'LR', 0, 'L', $fill);
                $pdf->Cell($w[2], 4, $p1_ofc_address, 'LR', 0, 'L', $fill);
                $pdf->Cell($w[3], 4, $p1_ofc_address, 'LR', 0, 'L', $fill);
                $pdf->Ln(3);

                $pdf->Cell($w[0], 4, '', 'LR', 0, 'L', $fill);
                $pdf->Cell($w[1], 4, $pr_ofc_address1, 'LR', 0, 'L', $fill);
                $pdf->Cell($w[2], 4, $p1_ofc_address1, 'LR', 0, 'L', $fill);
                $pdf->Cell($w[3], 4, $p1_ofc_address1, 'LR', 0, 'L', $fill);
                $pdf->Ln(3);

                $pdf->SetFont('Arial', 'B', '4.7');
                $pdf->Cell($w[0], 4, '', 'LR', 0, 'L', $fill);
                $pdf->Cell($w[1], 4, $pr_ofc_cd, 'LR', 0, 'L', $fill);
                $pdf->Cell($w[2], 4, $p1_ofc_cd, 'LR', 0, 'L', $fill);
                $pdf->Cell($w[3], 4, $p1_ofc_cd, 'LR', 0, 'L', $fill);
                $pdf->Ln();


                $pdf->SetFont('Arial', 'B', '7');
                $pdf->Cell($w[0], 6, '', 'LR', 0, 'L', $fill);
                $pdf->Cell($w[1], 6, '', 'LR', 0, 'L', $fill);
//                $pdf->Cell($w[2], 6, $p2_name, 'LR', 0, 'L', $fill);
                $pdf->Cell($w[2], 6, $p2_name . $pdf->Image($tick_p2, $pdf->GetX() + $w[2] - 5, $pdf->GetY() + 1, 3, 3), 'LR', 0, 'L', $fill);
                $pdf->Cell($w[3], 6, '', 'LR', 0, 'L', $fill);
                $pdf->Ln(4);

                $pdf->SetFont('Arial', 'B', '4.6');
                $pdf->Cell($w[0], 4, '', 'LR', 0, 'L', $fill);
                $pdf->Cell($w[1], 4, '', 'LR', 0, 'L', $fill);
                $pdf->Cell($w[2], 4, $p2_desig, 'LR', 0, 'L', $fill);
                $pdf->Cell($w[3], 4, '', 'LR', 0, 'L', $fill);
                $pdf->Ln(3);
                $pdf->SetFont('Arial', 'B', '4.6');

                $pdf->Cell($w[0], 4, '', 'LR', 0, 'L', $fill);
                $pdf->Cell($w[1], 4, '', 'LR', 0, 'L', $fill);
                $pdf->Cell($w[2], 4, $p2_join, 'LR', 0, 'L', $fill);
                $pdf->Cell($w[3], 4, '', 'LR', 0, 'L', $fill);
                $pdf->Ln(3);

                $pdf->SetFont('Arial', 'B', '4.6');
                $pdf->Cell($w[0], 4, '', 'LR', 0, 'L', $fill);
                $pdf->Cell($w[1], 4, '', 'LR', 0, 'L', $fill);
                $pdf->Cell($w[2], 4, $p2_office, 'LR', 0, 'L', $fill);
                $pdf->Cell($w[3], 4, '', 'LR', 0, 'L', $fill);
                $pdf->Ln(3);

                $pdf->Cell($w[0], 4, '', 'LR', 0, 'L', $fill);
                $pdf->Cell($w[1], 4, '', 'LR', 0, 'L', $fill);
                $pdf->Cell($w[2], 4, $p2_ofc_address, 'LR', 0, 'L', $fill);
                $pdf->Cell($w[3], 4, '', 'LR', 0, 'L', $fill);
                $pdf->Ln(3);

                $pdf->Cell($w[0], 4, '', 'LR', 0, 'L', $fill);
                $pdf->Cell($w[1], 4, '', 'LR', 0, 'L', $fill);
                $pdf->Cell($w[2], 4, $p2_ofc_address1, 'LR', 0, 'L', $fill);
                $pdf->Cell($w[3], 4, '', 'LR', 0, 'L', $fill);
                $pdf->Ln(3);
                $pdf->SetFont('Arial', 'B', '4.6');

                $pdf->Cell($w[0], 5, '', 'LR', 0, 'L', $fill);
                $pdf->Cell($w[1], 5, '', 'LR', 0, 'L', $fill);
                $pdf->Cell($w[2], 5, $p2_ofc_cd, 'LR', 0, 'L', $fill);
                $pdf->Cell($w[3], 5, '', 'LR', 0, 'L', $fill);
                $pdf->Ln();
                //memb 5//
                // dd($ApptDetail->mem_no);
                if ($ApptDetail->mem_no == 5 || $ApptDetail->mem_no == 6) {
                    $pdf->SetFont('Arial', 'B', '7');
                    $pdf->Cell($w[0], 6, '', 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[1], 6, '', 'LR', 0, 'L', $fill);
//                    $pdf->Cell($w[2], 6, $pa_name, 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[2], 6, $pa_name . $pdf->Image($tick_pa, $pdf->GetX() + $w[2] - 5, $pdf->GetY() + 1, 3, 3), 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[3], 6, '', 'LR', 0, 'L', $fill);
                    $pdf->Ln(4);

                    $pdf->SetFont('Arial', 'B', '4.6');
                    $pdf->Cell($w[0], 4, '', 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[1], 4, '', 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[2], 4, $pa_desig, 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[3], 4, '', 'LR', 0, 'L', $fill);
                    $pdf->Ln(3);

                    $pdf->SetFont('Arial', 'B', '4.6');

                    $pdf->Cell($w[0], 4, '', 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[1], 4, '', 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[2], 4, $pa_join, 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[3], 4, '', 'LR', 0, 'L', $fill);
                    $pdf->Ln(3);

                    $pdf->SetFont('Arial', 'B', '4.6');
                    $pdf->Cell($w[0], 4, '', 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[1], 4, '', 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[2], 4, $pa_office, 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[3], 4, '', 'LR', 0, 'L', $fill);
                    $pdf->Ln(3);

                    $pdf->Cell($w[0], 4, '', 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[1], 4, '', 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[2], 4, $pa_ofc_address, 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[3], 4, '', 'LR', 0, 'L', $fill);
                    $pdf->Ln(3);

                    $pdf->Cell($w[0], 4, '', 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[1], 4, '', 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[2], 4, $pa_ofc_address1, 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[3], 4, '', 'LR', 0, 'L', $fill);
                    $pdf->Ln(3);
                    $pdf->SetFont('Arial', 'B', '4.6');

                    $pdf->Cell($w[0], 5, '', 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[1], 5, '', 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[2], 5, $pa_ofc_cd, 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[3], 5, '', 'LR', 0, 'L', $fill);
                    $pdf->Ln();
                }
                //memb 6//
                if ($ApptDetail->mem_no == 6) {
                    $pdf->SetFont('Arial', 'B', '7');
                    $pdf->Cell($w[0], 6, '', 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[1], 6, '', 'LR', 0, 'L', $fill);
//                    $pdf->Cell($w[2], 6, $pb_name, 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[2], 6, $pb_name . $pdf->Image($tick_pb, $pdf->GetX() + $w[2] - 5, $pdf->GetY() + 1, 3, 3), 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[3], 6, '', 'LR', 0, 'L', $fill);
                    $pdf->Ln(4);

                    $pdf->SetFont('Arial', 'B', '4.6');
                    $pdf->Cell($w[0], 4, '', 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[1], 4, '', 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[2], 4, $pb_desig, 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[3], 4, '', 'LR', 0, 'L', $fill);
                    $pdf->Ln(3);
                    $pdf->SetFont('Arial', 'B', '4.6');
                    $pdf->Cell($w[0], 4, '', 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[1], 4, '', 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[2], 4, $pb_join, 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[3], 4, '', 'LR', 0, 'L', $fill);
                    $pdf->Ln(3);
                    $pdf->SetFont('Arial', 'B', '4.6');
                    $pdf->Cell($w[0], 4, '', 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[1], 4, '', 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[2], 4, $pb_office, 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[3], 4, '', 'LR', 0, 'L', $fill);
                    $pdf->Ln(3);

                    $pdf->Cell($w[0], 4, '', 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[1], 4, '', 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[2], 4, $pb_ofc_address, 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[3], 4, '', 'LR', 0, 'L', $fill);
                    $pdf->Ln(3);

                    $pdf->Cell($w[0], 4, '', 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[1], 4, '', 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[2], 4, $pb_ofc_address1, 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[3], 4, '', 'LR', 0, 'L', $fill);
                    $pdf->Ln(3);
                    $pdf->SetFont('Arial', 'B', '4.6');
                    $pdf->Cell($w[0], 5, '', 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[1], 5, '', 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[2], 5, $pb_ofc_cd, 'LR', 0, 'L', $fill);
                    $pdf->Cell($w[3], 5, '', 'LR', 0, 'L', $fill);
                    $pdf->Ln();
                }

                //memb 6//
                $pdf->SetFont('Arial', 'B', '7');
                $pdf->Cell($w[0], 6, '', 'LR', 0, 'L', $fill);
                $pdf->Cell($w[1], 6, '', 'LR', 0, 'L', $fill);
//                $pdf->Cell($w[2], 6, $p3_name, 'LR', 0, 'L', $fill);
                $pdf->Cell($w[2], 6, $p3_name . $pdf->Image($tick_p3, $pdf->GetX() + $w[2] - 5, $pdf->GetY() + 1, 3, 3), 'LR', 0, 'L', $fill);
                $pdf->Cell($w[3], 6, '', 'LR', 0, 'L', $fill);
                $pdf->Ln(4);

                $pdf->SetFont('Arial', 'B', '4.6');
                $pdf->Cell($w[0], 4, '', 'LR', 0, 'L', $fill);
                $pdf->Cell($w[1], 4, '', 'LR', 0, 'L', $fill);
                $pdf->Cell($w[2], 4, $p3_desig, 'LR', 0, 'L', $fill);
                $pdf->Cell($w[3], 4, '', 'LR', 0, 'L', $fill);
                $pdf->Ln(3);
                $pdf->SetFont('Arial', 'B', '4.6');
                $pdf->Cell($w[0], 4, '', 'LR', 0, 'L', $fill);
                $pdf->Cell($w[1], 4, '', 'LR', 0, 'L', $fill);
                $pdf->Cell($w[2], 4, $p3_join, 'LR', 0, 'L', $fill);
                $pdf->Cell($w[3], 4, '', 'LR', 0, 'L', $fill);
                $pdf->Ln(3);
                $pdf->SetFont('Arial', 'B', '4.6');
                $pdf->Cell($w[0], 4, '', 'LR', 0, 'L', $fill);
                $pdf->Cell($w[1], 4, '', 'LR', 0, 'L', $fill);
                $pdf->Cell($w[2], 4, $p3_office, 'LR', 0, 'L', $fill);
                $pdf->Cell($w[3], 4, '', 'LR', 0, 'L', $fill);
                $pdf->Ln(3);

                $pdf->Cell($w[0], 4, '', 'LR', 0, 'L', $fill);
                $pdf->Cell($w[1], 4, '', 'LR', 0, 'L', $fill);
                $pdf->Cell($w[2], 4, $p3_ofc_address, 'LR', 0, 'L', $fill);
                $pdf->Cell($w[3], 4, '', 'LR', 0, 'L', $fill);
                $pdf->Ln(3);

                $pdf->Cell($w[0], 4, '', 'LR', 0, 'L', $fill);
                $pdf->Cell($w[1], 4, '', 'LR', 0, 'L', $fill);
                $pdf->Cell($w[2], 4, $p3_ofc_address1, 'LR', 0, 'L', $fill);
                $pdf->Cell($w[3], 4, '', 'LR', 0, 'L', $fill);
                $pdf->Ln(3);
                $pdf->SetFont('Arial', 'B', '4.6');
                $pdf->Cell($w[0], 5, '', 'LR', 0, 'L', $fill);
                $pdf->Cell($w[1], 5, '', 'LR', 0, 'L', $fill);
                $pdf->Cell($w[2], 5, $p3_ofc_cd, 'LR', 0, 'L', $fill);
                $pdf->Cell($w[3], 5, '', 'LR', 0, 'L', $fill);


                $pdf->Ln();
                $pdf->Cell(array_sum($w), 0, '', 1, 0, 'L', $fill);
                //	$pdf->Ln(5);

                $pdf->Ln(4);

                $pdf->SetFont('Arial', '', 8.7);
                $pdf->MultiCell(190, 4, "     " . $euname9 . $euname10 . $euname11 . ".", 0, 'J', $fill);

                $pdf->Ln();


                $pdf->SetFont('Arial', '', 9);
                //$pdf->Cell(80);
                $pdf->Cell(30, 10, $euname12, 0, 0, 'L');
                $pdf->SetFont('Arial', '', 10);
                $pdf->Cell(120);
                $pdf->Cell(10, 10, "Signature", 0, 0, 'R');
                // Line break
                $pdf->Ln(8.3);

                $pdf->SetFont('Arial', '', 9);
                $pdf->Cell(30, 1, $euname13, 0, 0, 'L');
                $pdf->SetFont('Arial', '', 10);
                $pdf->Cell(110);
                //	$pdf->Cell(10,10,"yuyu",0,0,'R');
                $pdf->Cell(10, 10, $pdf->Image($signature, $pdf->GetX(), $pdf->GetY(), 30.78), 0, 0, 'R', false);
                // Line break
                $pdf->Ln(4);

                $pdf->SetFont('Arial', '', 8);
                $pdf->Cell(164);
                //$pdf->Cell(10, 10, $nb3, 0, 0, 'R');
                // Line break
                $pdf->Ln(4);
                $pdf->SetFont('Arial', '', 9);
                $pdf->Cell(160);
                $pdf->Cell(10, 12, $roname, 0, 0, 'R');

                // Line break
                $pdf->Ln(4);
                $pdf->SetFont('Arial', '', 9);
                $pdf->Cell(129);
                $pdf->Cell(49, 12, $euname14, 0, 0, 'R');

                // Line break
                $pdf->Ln();
                $pdf->Cell(190, 0, '', 1, 0, 'L', $fill);
                $pdf->Ln();


                $pdf->SetFont('Arial', 'B', 8.7);
                $pdf->Ln(1);
                $pdf->MultiCell(190, 4, $nb1 . $nb2, 0, 'J', $fill);
                $pdf->Ln(1);
                //$pdf->Ln();
                $pdf->SetFont('Arial', '', 8.7);
                $pdf->MultiCell(190, 4, "2. This is also to inform you that, a Facilitation Cntre for submission of Postal Ballot will be made at your respective training centre.You may utilize the facility.", 0, 'J');
                //$bmname=$row['block_muni_cd'];
                $pdf->Ln(1);
                $pdf->SetFont('Arial', '', 8.7);
                $pdf->MultiCell(190, 4, "3. The payment has been released.", 0, 'J');
                $pdf->Ln(1);
                $bmname = ($assembly == '') ? "Block/Municipality: " . $ApptDetail->block_muni_name : "";
                $pdf->SetFont('Arial', '', 9);
                $pdf->Cell(190, 5, $bmname, 0, 0, 'C');

                $count++;
            }
            if ($count == $per_page) {
                $per_page = $per_page + 1;
                if ($count != Count($ApptRecord)) {
                    $pdf->AddPage();
                }
            }
        }

        $pdf->Output();
        exit;
    }

    //::::::::::::::::::::::: Second Appt letter Reserve PDF (PC Election)::::::::::::::::::://
    public function getSecondAppointmentLetterReservePDF2019(Request $request) {
        $validate_array = ['forZone' => 'required|alpha_num|min:4|max:4',
            'party_reserve' => 'nullable|alpha|min:1|max:1',
            'assembly' => 'nullable|alpha_num|min:3|max:3',
            'group_id' => 'nullable|digits_between:1,4',
            'no_of_member' => 'nullable|digits_between:1,1',
            'from' => 'required|digits_between:1,4',
            'to' => 'required|digits_between:1,4',
            'blockMunicipality' => 'nullable|alpha_num',
            'newpersonnel_id' => 'nullable|alpha_num|min:11|max:11',
            'poststat' => 'nullable|alpha_num',
            'phase' => 'nullable|digits_between:1,1'
        ];

        $validate_array1 = ['forZone.required' => 'For Zone is required',
            'forZone.alpha_num' => 'For Zone must be an alpha numeric characters',
            'party_reserve.required' => 'Party/Reserve is required',
            'party_reserve.alpha' => 'Party/Reserve must be an alpha characters',
            'assembly.alpha_num' => 'Assembly must be an alpha numeric characters',
            'group_id.digits_between' => 'Group ID must be an integer',
            'no_of_member.digits_between' => 'No of Member must be an integer',
            'from.required' => 'Records From is required',
            'from.digits_between' => 'Records From must be an integer',
            'to.required' => 'Records To is required',
            'to.digits_between' => 'Records To must be an integer',
            'blockMunicipality.alpha_num' => 'Block must be an alpha numeric characters',
             'phase.digits_between' => 'Phase must be integer'
        ];

        $this->validate($request, $validate_array, $validate_array1);

        $forzone = $request->forZone;
        $phase = $request->phase;
        $party_reserve = $request->party_reserve;
        $assembly = $request->assembly;
        $blockMunicipality = $request->blockMunicipality;
        $group_id = $request->group_id;
        $no_of_member = $request->no_of_member;
        $newpersonnel_id = $request->newpersonnel_id;
        $poststat = $request->poststat;
        $orderno = $request->orderno;
        $pfrom = $request->from;
        $pto = $request->to;
        $count = 0;
        $tick = 'images/tick.png';

        // $ApptRecord = DB::select('call getSecondAppointmentLetterPDF45(?,?,?,?,?,?,?,?,?,?)', [$forzone, $party_reserve, $assembly, $group_id, $no_of_member, $pfrom, $pto,$blockMunicipality,$poststat,$orderno]);
        if ($assembly == '') {
            $ApptRecord = DB::select('call getSecondAppointmentLetterPDF45(?,?,?,?,?,?,?,?,?,?,?)', [$forzone, $party_reserve, $assembly, $group_id, $no_of_member, $pfrom, $pto, $blockMunicipality, $poststat, $orderno,$phase]);
        } else {
            $ApptRecord = DB::select('call getSecondAppointmentLetterPDF45Assembly(?,?,?,?,?,?,?,?,?,?,?)', [$forzone, $party_reserve, $assembly, $group_id, $no_of_member, $pfrom, $pto, $blockMunicipality, $poststat, $orderno,$phase]);
        }
        $pdf = app('FPDF');
        $pdf->AddPage();
        $count = 0;
        $per_page = 1;
        // $signaturePath = config('constants.DEOSIGNATURE');
        $signaturePath = config('constants.ROSIGNATURE');
        foreach ($ApptRecord as $ApptDetail) {
            if ($count < $per_page) {
                $fill = false;
                $poll_date = $ApptDetail->polldate;
                $poll_time = $ApptDetail->polltime;
                $training_venue = $ApptDetail->training_venue;
                $training_subvenue = $ApptDetail->sub_venuename;
                $venue_addr = $ApptDetail->venue_addr;
                $training_date = $ApptDetail->training_date;
                $training_time = $ApptDetail->training_time;

                $dc = ($ApptDetail->dc_venue != '' ? $ApptDetail->dc_venue . ", " . $ApptDetail->dc_address : "___________________________________");
                $dc_date = ($ApptDetail->dc_date != '' ? $ApptDetail->dc_date : "___________");
                $dc_time = ($ApptDetail->dc_time != '' ? $ApptDetail->dc_time : "___________");
                $rcvenue = ($ApptDetail->rc_venue != '' ? $ApptDetail->rc_venue : "_______________________________");

                $euname = "ELECTION URGENT";
                $euname1 = "ORDER OF APPOINTMENT FOR POLLING DUTIES";
                // $euname2 = "GENERAL PARLIAMENTARY ELECTION, 2019";
                $euname2 = "General Election to West Bengal Legislative Assembly, 2021";

                //$euname21="ASSEMBLY ELECTION, 2016";
                $euname3 = "";
                $euname22 = "Reserve Serial No. " . $ApptDetail->groupid;
                $euname4 = "Order No: " . $ApptDetail->order_no;
                $euname5 = "Date: " . date('d/m/Y', strtotime(trim(str_replace('/', '-', $ApptDetail->order_date))));

                $euname6 = "     In pursuance of sub-section(1) and sub-section(3) of section 26 of the Representation of the People Act,1951(43 of 1951), I hereby ";
                $euname7 = "appoint the officers specified in column(2) and (3) of the table below as Presiding Officer and Polling Officers respectively for the Polling ";
                $euname8 = "Party specified in corresponding entry in column(1) of the table provided by me for " . $ApptDetail->assembly . " - " . $ApptDetail->assembly_name . " L.A. Constituency ";
                $euname78 = "forming part of " . $ApptDetail->pccd . " - " . $ApptDetail->pcname . " Parliamentary Constituency.";
                $euname81 = "I also authorise the Polling Officer specified in column(4) of the table against that entry to perform the functions of the Presiding Officer";
                $euname82 = "during the unavoidable absence, if any, of the Presiding Officer.";

                $euname9 = "The Poll will be taken on " . $poll_date . " during the hours " . $poll_time . ". The Presiding Officer should arrange to collect the Polling ";
                $euname10 = "materials from " . $dc . " on " . $dc_date . " at " . $dc_time . " ";
                $euname11 = "and after the Poll, these should be returned to collecting centre at " . $rcvenue;

                $euname12 = "Place: " . $ApptDetail->district;
                $euname13 = "Date: " . date("d/m/Y");
                // $euname14 = "District: " . $ApptDetail->district;
                $euname14 = "Assembly: " . $ApptDetail->assembly .'-'.$ApptDetail->assembly_name;
                $nb1 = "1. You are requested to attend the training at " . $training_venue . " " . $training_subvenue . " , " . $venue_addr;
                $nb2 = " on " . $training_date . " from " . $training_time;
                //$nb3 = "(__________________________)";
//	        $signature=$signaturePath.$ApptDetail->assembly.".jpg";
//		$roname="RO/".$ApptDetail->assembly." - ".$ApptDetail->assembly_name;
                $signature = $signaturePath . $ApptDetail->assembly . ".jpg";
                // $roname = "RO/" . $ApptDetail->pccd . " - " . $ApptDetail->pcname;
                // $roname = "District Election Officer";
                $roname = "Returning Officer";
                $pdf->SetFont('Arial', 'B', 10);
                $pdf->Cell(37, 6, $euname, 1, 0, 'L');
                $pdf->SetFont('Arial', 'B', 10);
                $pdf->Cell(20);
                $pdf->Cell(84, 4, $euname1, 'B', 0, 'C');
                $pdf->SetFont('Arial', 'B', 8);
                $pdf->Cell(28);
                $pdf->Cell(10, 3, $euname3, 0, 0, 'R');

                // Line break
                $pdf->Ln(6);

                $pdf->SetFont('Arial', 'B', 8);
                $pdf->Cell(15, 10, '', 0, 0, 'L');
                $pdf->SetFont('Arial', 'U', 10);
                $pdf->Cell(46);
                $pdf->Cell(77, 4, $euname2, '', 0, 'C');
                $pdf->SetFont('Arial', 'B', 8);
                $pdf->Cell(12);
                $pdf->SetFont('Arial', 'B', 10);
                $pdf->Cell(37, 6, $euname22, 1, 0, 'R');

                $pdf->Ln(5);

//                $pdf->SetFont('Arial','B',8);
//                $pdf->Cell(15,10,'',0,0,'L');
//                $pdf->SetFont('Arial','',10);
//                $pdf->Cell(60);
//                $pdf->Cell(47,4,$euname21,'B',0,'C');
//                $pdf->SetFont('Arial','B',8);
//                $pdf->Cell(13);
//                $pdf->SetFont('Arial','B',10);
//                $pdf->Cell(33,6,'',0,0,'R');
//                // Line break
//
                $pdf->Ln(4);

                $pdf->SetFont('Arial', '', 9);
                $pdf->Cell(15, 10, $euname4, 0, 0, 'L');
                $pdf->SetFont('Arial', '', 10);
                $pdf->Cell(60);
                $pdf->Cell(47, 4, '', '', 0, 'C');
                $pdf->SetFont('Arial', '', 8);
                $pdf->Cell(33);
                $pdf->SetFont('Arial', '', 9);
                $pdf->Cell(33, 6, $euname5, 0, 0, 'R');

                $pdf->Ln(12);

                $pdf->Ln();
                $pdf->SetFont('Arial', '', 8.7);
                $pdf->MultiCell(190, 4, $euname6 . $euname7 . $euname8 . $euname78, 0, 'J');

                // Line break
                $pdf->Ln(7);
                $pdf->SetFont('Arial', '', 8.7);
                $pdf->Cell(87);
                $pdf->Cell(20, 10, $euname81, 0, 0, 'C');

                // Line break
                $pdf->Ln(4);
                $pdf->SetFont('Arial', '', 8.7);
                $pdf->Cell(50, 10, $euname82, 0, 0, 'L');

                // Line break
                $pdf->Ln(18);

                $pdf->SetFillColor(255, 255, 255);
                $pdf->SetDrawColor(0, 0, 0);
                $pdf->SetLineWidth(.3);
                $pdf->SetFont('Arial', 'B', '8');

                $head = array('Polling', 'Name of the Presiding Officer', 'Name of the Polling Officers', 'Polling Officer authorised to perform');
                $w = array(11, 59, 60, 60);
                $head1 = array('Party', '', '', 'the functions of the Presiding Officer');
                $head2 = array('No.', '', '', "in the latter's absence");
                $head3 = array('(1)', '(2)', '(3)', "(4)");

                //	$this->SetFont('Arial','',9);
                for ($j = 0; $j < count($head); $j++)
                    $pdf->Cell($w[$j], 5, $head[$j], 'LTR', 0, 'C', true);
                $pdf->Ln();
                for ($j1 = 0; $j1 < count($head1); $j1++)
                    $pdf->Cell($w[$j1], 4, $head1[$j1], 'LR', 0, 'C', true);
                $pdf->Ln();
                for ($j2 = 0; $j2 < count($head2); $j2++)
                    $pdf->Cell($w[$j2], 5, $head2[$j2], 'LR', 0, 'C', true);
                $pdf->Ln();

                for ($j3 = 0; $j3 < count($head3); $j3++)
                    $pdf->Cell($w[$j3], 7, $head3[$j3], 1, 0, 'C', true);
                $pdf->Ln();

                if ($ApptDetail->per_poststat == "PR") {
                    $pr_name = $ApptDetail->pr_name;
                    $pr_desig = $ApptDetail->pr_designation;
                    $pr_code = $ApptDetail->pr_personcd;
                    $pr_office = $ApptDetail->pr_officename;
                    $pr_ofc_address = $ApptDetail->pr_officeaddress;
                    $pr_ofc_address1 = "P.O. - " . $ApptDetail->pr_postoffice . ", Subdiv. - " . $ApptDetail->pr_subdivision;
                    $pr_ofc_address2 = "Dist - " . $ApptDetail->pr_district;
                    $pr_ofc_cd = "Mobile No. - " . $ApptDetail->pr_mobno . ", OFFICE - (" . $ApptDetail->pr_officecd . ")";
                    $pr_post_stat = $ApptDetail->pr_post_stat;
                    $pr_join = $pr_post_stat . " PIN - (" . $pr_code . ")";

                    $p1_name = "";
                    $p1_desig = "";
                    $p1_code = "";
                    $p1_office = "";
                    $p1_ofc_address = "";
                    $p1_ofc_address1 = "";
                    $p1_ofc_address2 = "";
                    $p1_ofc_cd = "";
                    $p1_post_stat = "";
                    $p1_join = "";

                    $pp_name = "";
                    $pp_desig = "";
                    $pp_code = "";
                    $pp_office = "";
                    $pp_ofc_address = "";
                    $pp_ofc_address1 = "";
                    $pp_ofc_address2 = "";
                    $pp_ofc_cd = "";
                    $pp_post_stat = "";
                    $pp_join = "";
                }
                if ($ApptDetail->per_poststat == "P1" || $ApptDetail->per_poststat == "P2" || $ApptDetail->per_poststat == "P3" || $ApptDetail->per_poststat == "PA" || $ApptDetail->per_poststat == "PB") {
                    if ($ApptDetail->per_poststat == "P1") {
                        $p1_name = $ApptDetail->p1_name;
                        $p1_desig = $ApptDetail->p1_designation;
                        $p1_code = $ApptDetail->p1_personcd;
                        $p1_office = $ApptDetail->p1_officename;
                        $p1_ofc_address = $ApptDetail->p1_officeaddress;
                        $p1_ofc_address1 = "P.O. - " . $ApptDetail->p1_postoffice . ", Subdiv. - " . $ApptDetail->p1_subdivision;
                        $p1_ofc_address2 = "Dist - " . $ApptDetail->p1_district;
                        $p1_ofc_cd = "Mobile No. - " . $ApptDetail->p1_mobno . ", OFFICE - (" . $ApptDetail->p1_officecd . ")";
                        $p1_post_stat = $ApptDetail->p1_post_stat;
                        $p1_join = $p1_post_stat . " PIN - (" . $p1_code . ")";
                    }
                    if ($ApptDetail->per_poststat == "P2") {
                        $p1_name = $ApptDetail->p2_name;
                        $p1_desig = $ApptDetail->p2_designation;
                        $p1_code = $ApptDetail->p2_personcd;
                        $p1_office = $ApptDetail->p2_officename;
                        $p1_ofc_address = $ApptDetail->p2_officeaddress;
                        $p1_ofc_address1 = "P.O. - " . $ApptDetail->p2_postoffice . ", Subdiv. - " . $ApptDetail->p2_subdivision;
                        $p1_ofc_address2 = "Dist - " . $ApptDetail->p2_district;
                        $p1_ofc_cd = "Mobile No. - " . $ApptDetail->p2_mobno . ", OFFICE - (" . $ApptDetail->p2_officecd . ")";
                        $p1_post_stat = $ApptDetail->p2_post_stat;
                        $p1_join = $p1_post_stat . " PIN - (" . $p1_code . ")";
                    }
                    if ($ApptDetail->per_poststat == "P3") {
                        $p1_name = $ApptDetail->p3_name;
                        $p1_desig = $ApptDetail->p3_designation;
                        $p1_code = $ApptDetail->p3_personcd;
                        $p1_office = $ApptDetail->p3_officename;
                        $p1_ofc_address = $ApptDetail->p3_officeaddress;
                        $p1_ofc_address1 = "P.O. - " . $ApptDetail->p3_postoffice . ", Subdiv. - " . $ApptDetail->p3_subdivision;
                        $p1_ofc_address2 = "Dist - " . $ApptDetail->p3_district;
                        $p1_ofc_cd = "Mobile No. - " . $ApptDetail->p3_mobno . ", OFFICE - (" . $ApptDetail->p3_officecd . ")";
                        $p1_post_stat = $ApptDetail->p3_post_stat;
                        $p1_join = $p1_post_stat . " PIN - (" . $p1_code . ")";
                    }

                    if ($ApptDetail->pa_name != '') {
                        $p1_name = $ApptDetail->pa_name;
                        $p1_desig = $ApptDetail->pa_designation;
                        $p1_code = $ApptDetail->pa_personcd;
                        $p1_office = $ApptDetail->pa_officename;
                        $p1_ofc_address = $ApptDetail->pa_officeaddress;
                        $p1_ofc_address1 = "P.O. - " . $ApptDetail->pa_postoffice . ", Subdiv. - " . $ApptDetail->pa_subdivision;
                        $p1_ofc_address2 = "Dist - " . $ApptDetail->pa_district;
                        $p1_ofc_cd = "Mobile No. - " . $ApptDetail->pa_mobno . ", OFFICE - (" . $ApptDetail->pa_officecd . ")";
                        $p1_post_stat = $ApptDetail->pa_post_stat;
                        $p1_join = $ApptDetail->pa_post_stat . " PIN - (" . $p1_code . ")";
                    }

                    if ($ApptDetail->pb_name != '') {
                        $p1_name = $ApptDetail->pb_name;
                        $p1_desig = $ApptDetail->pb_designation;
                        $p1_code = $ApptDetail->pb_personcd;
                        $p1_office = $ApptDetail->pb_officename;
                        $p1_ofc_address = $ApptDetail->pb_officeaddress;
                        $p1_ofc_address1 = "P.O. - " . $ApptDetail->pb_postoffice . ", Subdiv. - " . $ApptDetail->pb_subdivision;
                        $p1_ofc_address2 = "Dist - " . $ApptDetail->pb_district;
                        $p1_ofc_cd = "Mobile No. - " . $ApptDetail->pb_mobno . ", OFFICE - (" . $ApptDetail->pb_officecd . ")";
                        $p1_post_stat = $ApptDetail->pb_post_stat;
                        $p1_join = $p1_post_stat . " PIN - (" . $p1_code . ")";
                    }


                    $pr_name = '';
                    $pr_desig = '';
                    $pr_code = '';
                    $pr_office = '';
                    $pr_ofc_address = '';
                    $pr_ofc_address1 = '';
                    $pr_ofc_address2 = '';
                    $pr_ofc_cd = '';
                    $pr_post_stat = '';
                    $pr_join = '';
                    if ($ApptDetail->per_poststat != 'P1') {
                        $pp_name = '';
                        $pp_desig = '';
                        $pp_code = '';
                        $pp_office = '';
                        $pp_ofc_address = '';
                        $pp_ofc_address1 = '';
                        $pp_ofc_address2 = '';
                        $pp_ofc_cd = '';
                        $pp_post_stat = '';
                        $pp_join = '';
                    }
                }
                if ($ApptDetail->per_poststat == "P1") {
                    $pp_name = $ApptDetail->p1_name;
                    $pp_desig = $ApptDetail->p1_designation;
                    $pp_code = $ApptDetail->p1_personcd;
                    $pp_office = $ApptDetail->p1_officename;
                    $pp_ofc_address = $ApptDetail->p1_officeaddress;
                    $pp_ofc_address1 = "P.O. - " . $ApptDetail->p1_postoffice . ", Subdiv. - " . $ApptDetail->p1_subdivision;
                    $pp_ofc_address2 = "Dist - " . $ApptDetail->p1_district;
                    $pp_ofc_cd = "Mobile No. - " . $ApptDetail->p1_mobno . ", OFFICE - (" . $ApptDetail->p1_officecd . ")";
                    $pp_post_stat = $ApptDetail->p1_post_stat;
                    $pp_join = $p1_post_stat . " PIN - (" . $p1_code . ")";

                    $pr_name = '';
                    $pr_desig = '';
                    $pr_code = '';
                    $pr_office = '';
                    $pr_ofc_address = '';
                    $pr_ofc_address1 = '';
                    $pr_ofc_address2 = '';
                    $pr_ofc_cd = '';
                    $pr_post_stat = '';
                    $pr_join = '';
                }



                $pdf->SetFont('Arial', 'B', '7');
                $pdf->Cell($w[0], 6, $ApptDetail->groupid, 'LTR', 0, 'C', $fill);
                $pdf->Cell($w[1], 6, $pr_name, 'LTR', 0, 'L', $fill);
                $pdf->Cell($w[2], 6, $p1_name, 'LTR', 0, 'L', $fill);
                $pdf->Cell($w[3], 6, $pp_name, 'LTR', 0, 'L', $fill);
                $pdf->Ln(4);

                $pdf->SetFont('Arial', 'B', '4.7');

                $pdf->Cell($w[0], 4, '', 'LR', 0, 'L', $fill);
                $pdf->Cell($w[1], 4, $pr_desig, 'LR', 0, 'L', $fill);
                $pdf->Cell($w[2], 4, $p1_desig, 'LR', 0, 'L', $fill);
                $pdf->Cell($w[3], 4, $pp_desig, 'LR', 0, 'L', $fill);
                $pdf->Ln(3);

                $pdf->SetFont('Arial', 'B', '4.7');
                $pdf->Cell($w[0], 4, '', 'LR', 0, 'L', $fill);
                $pdf->Cell($w[1], 4, $pr_join, 'LR', 0, 'L', $fill);
                $pdf->Cell($w[2], 4, $p1_join, 'LR', 0, 'L', $fill);
                $pdf->Cell($w[3], 4, $pp_join, 'LR', 0, 'L', $fill);
                $pdf->Ln(3);

                $pdf->SetFont('Arial', 'B', '4.7');
                $pdf->Cell($w[0], 4, '', 'LR', 0, 'L', $fill);
                $pdf->Cell($w[1], 4, $pr_office, 'LR', 0, 'L', $fill);
                $pdf->Cell($w[2], 4, $p1_office, 'LR', 0, 'L', $fill);
                $pdf->Cell($w[3], 4, $pp_office, 'LR', 0, 'L', $fill);
                $pdf->Ln(3);

                $pdf->Cell($w[0], 4, '', 'LR', 0, 'L', $fill);
                $pdf->Cell($w[1], 4, $pr_ofc_address, 'LR', 0, 'L', $fill);
                $pdf->Cell($w[2], 4, $p1_ofc_address, 'LR', 0, 'L', $fill);
                $pdf->Cell($w[3], 4, $pp_ofc_address, 'LR', 0, 'L', $fill);
                $pdf->Ln(3);

                $pdf->Cell($w[0], 4, '', 'LR', 0, 'L', $fill);
                $pdf->Cell($w[1], 4, $pr_ofc_address1, 'LR', 0, 'L', $fill);
                $pdf->Cell($w[2], 4, $p1_ofc_address1, 'LR', 0, 'L', $fill);
                $pdf->Cell($w[3], 4, $pp_ofc_address1, 'LR', 0, 'L', $fill);
                $pdf->Ln(3);

                $pdf->Cell($w[0], 4, '', 'LR', 0, 'L', $fill);
                $pdf->Cell($w[1], 4, $pr_ofc_address2, 'LR', 0, 'L', $fill);
                $pdf->Cell($w[2], 4, $p1_ofc_address2, 'LR', 0, 'L', $fill);
                $pdf->Cell($w[3], 4, $pp_ofc_address2, 'LR', 0, 'L', $fill);
                $pdf->Ln(3);

                $pdf->SetFont('Arial', 'B', '4.7');
                $pdf->Cell($w[0], 4, '', 'LR', 0, 'L', $fill);
                $pdf->Cell($w[1], 4, $pr_ofc_cd, 'LR', 0, 'L', $fill);
                $pdf->Cell($w[2], 4, $p1_ofc_cd, 'LR', 0, 'L', $fill);
                $pdf->Cell($w[3], 4, $pp_ofc_cd, 'LR', 0, 'L', $fill);
                $pdf->Ln();



                // $pdf->Ln();
                $pdf->Cell(array_sum($w), 0, '', 1, 0, 'L', $fill);
                //	$pdf->Ln(5);

                $pdf->Ln(4);

                $pdf->SetFont('Arial', '', 8.7);
                $pdf->MultiCell(190, 4, "     " . $euname9 . $euname10 . $euname11 . ".", 0, 'J', $fill);

                $pdf->Ln(18);


                $pdf->SetFont('Arial', '', 9);
                //$pdf->Cell(80);
                $pdf->Cell(30, 10, $euname12, 0, 0, 'L');
                $pdf->SetFont('Arial', '', 10);
                $pdf->Cell(120);
                $pdf->Cell(10, 10, "Signature", 0, 0, 'R');
                // Line break
                $pdf->Ln(9);

                $pdf->SetFont('Arial', '', 9);
                $pdf->Cell(30, 1, $euname13, 0, 0, 'L');
                $pdf->SetFont('Arial', '', 10);
                $pdf->Cell(110);
                //	$pdf->Cell(10,10,"yuyu",0,0,'R');
                $pdf->Cell(10, 10, $pdf->Image($signature, $pdf->GetX(), $pdf->GetY(), 30.78), 0, 0, 'R', false);
                // Line break
                $pdf->Ln(4);

                $pdf->SetFont('Arial', '', 8);
                $pdf->Cell(164);
                //$pdf->Cell(10, 10, $nb3, 0, 0, 'R');
                // Line break
                $pdf->Ln(4);
                $pdf->SetFont('Arial', '', 9);
                $pdf->Cell(160);
                $pdf->Cell(10, 12, $roname, 0, 0, 'R');

                // Line break
                $pdf->Ln(4);
                $pdf->SetFont('Arial', '', 9);
                $pdf->Cell(129);
                $pdf->Cell(49, 12, $euname14, 0, 0, 'R');

                // Line break
                $pdf->Ln();
                $pdf->Cell(190, 0, '', 1, 0, 'L', $fill);
                $pdf->Ln();


                $pdf->SetFont('Arial', 'B', 8.7);
                $pdf->Ln(4);
                $pdf->MultiCell(190, 4, $nb1 . $nb2, 0, 'J', $fill);
                $pdf->Ln(1);
                //$pdf->Ln();
                $pdf->SetFont('Arial', '', 8.7);
                $pdf->MultiCell(190, 4, "2. This is also to inform you that, a Facilitation Centre for submission of Postal Ballot will be made at your respective training centre.You may utilize the facility.", 0, 'J');
                //$bmname=$row['block_muni_cd'];
                $pdf->Ln(1);
                // $pdf->SetFont('Arial', '', 8.7);
                // $pdf->MultiCell(190, 4, "3. The payment has been released.", 0, 'J');
                $pdf->Ln(13);
                $bmname = ($assembly == '') ? "Block/Municipality: " . $ApptDetail->block_muni_name : "";
                $pdf->SetFont('Arial', '', 9);
                $pdf->Cell(190, 5, $bmname, 0, 0, 'C');

//                $pdf->Ln(4);
//                $bmname = "Block/Municipality: " . $ApptDetail->block_muni_name;
//                $pdf->SetFont('Arial', '', 9);
//                $pdf->Cell(190, 5, $bmname, 0, 0, 'C');

                $count++;
            }
            if ($count == $per_page) {
                $per_page = $per_page + 1;
                if ($count != Count($ApptRecord)) {
                    $pdf->AddPage();
                }
            }
        }

        $pdf->Output();
        exit;
    }

//    //::::::::::::::::::::::: Second Appt Letter PDF 4-5 (Assembly Election):::::::::::::::::::://
//    public function getSecondAppointmentLetterPDF45(Request $request) {
//        $validate_array=['forZone' => 'required|alpha_num|min:4|max:4',
//                            'party_reserve'=> 'required|alpha|min:1|max:1',
//                            'assembly' => 'nullable|alpha_num|min:3|max:3',
//                            'group_id'=> 'nullable|digits_between:1,4',
//                            'no_of_member'=> 'nullable|digits_between:1,1',
//                            'from'=>'required|digits_between:1,4',
//                            'to'=>'required|digits_between:1,4'
//                           ];
//
//           $validate_array1=['forZone.required' => 'For Zone is required',
//                             'forZone.alpha_num' => 'For Zone must be an alpha numeric characters',
//                             'party_reserve.required' => 'Party/Reserve is required',
//                             'party_reserve.alpha' => 'Party/Reserve must be an alpha characters',
//                             'assembly.alpha_num' => 'Assembly must be an alpha numeric characters',
//                             'group_id.digits_between' => 'Group ID must be an integer',
//                             'no_of_member.digits_between' => 'No of Member must be an integer',
//                             'from.required' => 'Records From is required',
//                             'from.digits_between' => 'Records From must be an integer',
//                             'to.required' => 'Records To is required',
//                             'to.digits_between' => 'Records To must be an integer'
//                            ];
//
//           $this->validate($request,$validate_array,$validate_array1);
//           
//        $forzone=$request->forZone;
//        $party_reserve=$request->party_reserve;
//        $assembly=$request->assembly;
//        $group_id=$request->group_id;
//        $no_of_member=$request->no_of_member;
//        $pfrom=$request->from;
//        $pto=$request->to;
//        $count=0;
//        
//       
//        $ApptRecord=DB::select('call getSecondAppointmentLetterPDF45(?,?,?,?,?,?,?)',[$forzone,$party_reserve,$assembly,$group_id,$no_of_member,$pfrom,$pto]);
//        $pdf = app('FPDF');
//        $pdf->AddPage();
//        $count=0;
//        $per_page=1;
//        $signaturePath=config('constants.DEOSIGNATURE');
//        foreach ($ApptRecord as $ApptDetail)
//        {
//            if($count<$per_page)
//            {
//                $fill = false;
//                $poll_date=$ApptDetail->polldate;
//                $poll_time=$ApptDetail->polltime;
//                $training_venue=$ApptDetail->training_venue;
//                $training_subvenue=$ApptDetail->sub_venuename;
//                $venue_addr=$ApptDetail->venue_addr;
//                $training_date=$ApptDetail->training_date;
//                $training_time=$ApptDetail->training_time;
//
//                $dc=($ApptDetail->dc_venue!=''?$ApptDetail->dc_venue.", ".$ApptDetail->dc_address:"___________________________________");
//                $dc_date=($ApptDetail->dc_date!=''?$ApptDetail->dc_date:"___________");
//                $dc_time=($ApptDetail->dc_time!=''?$ApptDetail->dc_time:"___________");
//                $rcvenue=($ApptDetail->rc_venue!=''?$ApptDetail->rc_venue:"_______________________________");
//                
//                $euname="ELECTION URGENT";
//                $euname1="ORDER OF APPOINTMENT FOR POLLING DUTIES";
//                $euname2="GENERAL ELECTION TO WEST BENGAL LEGISLATIVE";
//                $euname21="ASSEMBLY ELECTION, 2016";
//                $euname3=($assembly=='')?$ApptDetail->pers_off."/".$ApptDetail->per_poststat:"";
//                $euname22="Polling Party No. ".$ApptDetail->groupid;
//                $euname4="Order No: ";
//                $euname5="Date: ";
//
//                $euname6="     In persuance of sub-section(1) and sub-section(3) of section 26 of the Representation of the People Act,1963(43 of 1951), I hereby ";
//                $euname7="appoint the officers specified in columb(2) and (3) of the table below as Presiding Officer and Polling Officers respectively for the Polling ";
//                $euname8="Station specified in corresponding entry in column(1) of the table provided by me for ".$ApptDetail->assembly." - ".$ApptDetail->assembly_name." L.A. Constituency ";
//                $euname78="forming part of ".$ApptDetail->pcname." Parliamentary Constituency.";
//                $euname81="I also authorise the Polling Officer specified in column(4) of the table against that entry to perform the functions of the Presiding Officer";
//                $euname82="during the unavoidable absence, if any, of the Presiding Officer.";
//
//                $euname9="The Poll will be taken on ".$poll_date." during the hours ".$poll_time.". The Presiding Officer should arrange to collect the Polling ";
//                $euname10="materials from ".$dc." on ".$dc_date." at ".$dc_time." ";
//                $euname11="and after the Poll, these should be returned to collecting centre at ".$rcvenue;
//
//                $euname12="Place: ".$ApptDetail->district;
//                $euname13="Date: ".date("d/m/Y");
//                $euname14="District ".$ApptDetail->district;
//                $nb1="1. You are requested to attend the training at ".$training_venue." ".$training_subvenue." , ".$venue_addr;
//                $nb2="on ".$training_date." from ".$training_time;
//                $nb3="(__________________________)";
//			
//	        $signature=$signaturePath.$ApptDetail->assembly.".jpg";
//		$roname="RO/".$ApptDetail->assembly." - ".$ApptDetail->assembly_name;
//
//                $pdf->SetFont('Arial','B',10);
//                $pdf->Cell(37,6,$euname,1,0,'L');
//                $pdf->SetFont('Arial','B',10);
//                $pdf->Cell(20);
//                $pdf->Cell(84,4,$euname1,'B',0,'C');
//                $pdf->SetFont('Arial','B',8);
//                $pdf->Cell(28);
//                $pdf->Cell(10,3,$euname3,0,0,'R');
//
//                // Line break
//                $pdf->Ln(6);
//
//                $pdf->SetFont('Arial','B',8);
//                $pdf->Cell(15,10,'',0,0,'L');
//                $pdf->SetFont('Arial','',10);
//                $pdf->Cell(38);
//                $pdf->Cell(92,4,$euname2,'B',0,'C');
//                $pdf->SetFont('Arial','B',8);
//                $pdf->Cell(8);
//                $pdf->SetFont('Arial','B',10);
//                $pdf->Cell(37,6,$euname22,1,0,'R');
//
//                $pdf->Ln(5);
//
//                $pdf->SetFont('Arial','B',8);
//                $pdf->Cell(15,10,'',0,0,'L');
//                $pdf->SetFont('Arial','',10);
//                $pdf->Cell(60);
//                $pdf->Cell(47,4,$euname21,'B',0,'C');
//                $pdf->SetFont('Arial','B',8);
//                $pdf->Cell(13);
//                $pdf->SetFont('Arial','B',10);
//                $pdf->Cell(33,6,'',0,0,'R');
//                // Line break
//
//                $pdf->Ln(4);
//
//                $pdf->SetFont('Arial','',9);
//                $pdf->Cell(15,10,$euname4,0,0,'L');
//                $pdf->SetFont('Arial','',10);
//                $pdf->Cell(60);
//                $pdf->Cell(47,4,'','',0,'C');
//                $pdf->SetFont('Arial','',8);
//                $pdf->Cell(33);
//                $pdf->SetFont('Arial','',9);
//                $pdf->Cell(33,6,$euname5,0,0,'R');
//
//                $pdf->Ln(6);
//
//                $pdf->Ln();
//                $pdf->SetFont('Arial','',8.7);
//                $pdf->MultiCell(190,4,$euname6.$euname7.$euname8.$euname78,0,'J');
//
//                $pdf->SetFont('Arial','',8.7);
//                $pdf->Cell(87);
//                $pdf->Cell(20,10,$euname81,0,0,'C');
//
//                // Line break
//                $pdf->Ln(4);
//                $pdf->SetFont('Arial','',8.7);
//                $pdf->Cell(50,10,$euname82,0,0,'L');
//
//                // Line break
//                $pdf->Ln(10);
//
//                $pdf->SetFillColor(255,255,255);
//                $pdf->SetDrawColor(0,0,0);
//                $pdf->SetLineWidth(.3);
//                $pdf->SetFont('Arial','B','8');
//
//                $head = array('Polling','Name of the Presiding Officer','Name of the Polling Officers','Polling Officer authorised to perform');
//                $w = array(11,59,60,60);
//                $head1=array('Party','','','the functions of the Presiding Officer');
//                $head2=array('No.','','',"in the latter's absence");
//                $head3=array('(1)','(2)','(3)',"(4)");
//
//                //	$this->SetFont('Arial','',9);
//                for($j=0;$j<count($head);$j++)
//                        $pdf->Cell($w[$j],5,$head[$j],'LTR',0,'C',true);				 
//                $pdf->Ln();
//                for($j1=0;$j1<count($head1);$j1++)
//                        $pdf->Cell($w[$j1],4,$head1[$j1],'LR',0,'C',true);
//                $pdf->Ln();	
//                for($j2=0;$j2<count($head2);$j2++)
//                        $pdf->Cell($w[$j2],5,$head2[$j2],'LR',0,'C',true);
//                $pdf->Ln();
//
//                for($j3=0;$j3<count($head3);$j3++)
//                        $pdf->Cell($w[$j3],7,$head3[$j3],1,0,'C',true);
//                $pdf->Ln();
//                
//                $pr_name=$ApptDetail->pr_name;
//                $pr_desig=$ApptDetail->pr_designation;
//                $pr_code=$ApptDetail->pr_personcd;
//                $pr_office=$ApptDetail->pr_officename;
//                $pr_ofc_address=$ApptDetail->pr_officeaddress;
//                $pr_ofc_address1="P.O. - ".$ApptDetail->pr_postoffice.", Subdiv. - ".$ApptDetail->pr_subdivision.", ".$ApptDetail->district;
//                $pr_ofc_cd="Mobile No. - ".$ApptDetail->pr_mobno.", OFFICE - (".$ApptDetail->pr_officecd.")";
//                $pr_post_stat=$ApptDetail->pr_post_stat;
//                $pr_join=$pr_post_stat." PIN - (".$pr_code.")";
//
//                $p1_name="1. ".$ApptDetail->p1_name;
//                $p1_desig=$ApptDetail->p1_designation;
//                $p1_code=$ApptDetail->p1_personcd;
//                $p1_office=$ApptDetail->p1_officename;
//                $p1_ofc_address=$ApptDetail->p1_officeaddress;
//                $p1_ofc_address1="P.O. - ".$ApptDetail->p1_postoffice.", Subdiv. - ".$ApptDetail->p1_subdivision.", ".$ApptDetail->district;
//                $p1_ofc_cd="Mobile No. - ".$ApptDetail->p1_mobno.", OFFICE - (".$ApptDetail->p1_officecd.")";
//                $p1_post_stat=$ApptDetail->p1_post_stat;
//                $p1_join=$p1_post_stat." PIN - (".$p1_code.")";
//
//                $p2_name="2. ".$ApptDetail->p2_name;
//                $p2_desig=$ApptDetail->p2_designation;
//                $p2_code=$ApptDetail->p2_personcd;
//                $p2_office=$ApptDetail->p2_officename;
//                $p2_ofc_address=$ApptDetail->p2_officeaddress;
//                $p2_ofc_address1="P.O. - ".$ApptDetail->p2_postoffice.", Subdiv. - ".$ApptDetail->p2_subdivision.", ".$ApptDetail->district;
//                $p2_ofc_cd="Mobile No. - ".$ApptDetail->p2_mobno.", OFFICE - (".$ApptDetail->p2_officecd.")";
//                $p2_post_stat=$ApptDetail->p2_post_stat;
//                $p2_join=$p2_post_stat." PIN - (".$p2_code.")";
//
//                $sl=(($ApptDetail->pa_name=='')?"3. ":(($ApptDetail->pb_name=='')?"4. ":"5. "));
//
//                $p3_name=$sl.$ApptDetail->p3_name;
//                $p3_desig=$ApptDetail->p3_designation;
//                $p3_code=$ApptDetail->p3_personcd;
//                $p3_office=$ApptDetail->p3_officename;
//                $p3_ofc_address=$ApptDetail->p3_officeaddress;
//                $p3_ofc_address1="P.O. - ".$ApptDetail->p3_postoffice.", Subdiv. - ".$ApptDetail->p3_subdivision.", ".$ApptDetail->district;
//                $p3_ofc_cd="Mobile No. - ".$ApptDetail->p3_mobno.", OFFICE - (".$ApptDetail->p3_officecd.")";
//                $p3_post_stat=$ApptDetail->p3_post_stat;
//                $p3_join=$p3_post_stat." PIN - (".$p3_code.")";
//
//
//                if($ApptDetail->pa_name!=''){
//                $pa_name="3. ".$ApptDetail->pa_name;
//                $pa_desig=$ApptDetail->pa_designation;
//                $pa_code=$ApptDetail->pa_personcd;
//                $pa_office=$ApptDetail->pa_officename;
//                $pa_ofc_address=$ApptDetail->pa_officeaddress;
//                $pa_ofc_address1="P.O. - ".$ApptDetail->pa_postoffice.", Subdiv. - ".$ApptDetail->pa_subdivision.", ".$ApptDetail->district;
//                $pa_ofc_cd="Mobile No. - ".$ApptDetail->pa_mobno.", OFFICE - (".$ApptDetail->pa_officecd.")";
//                $pa_post_stat=$ApptDetail->pa_post_stat;
//                $pa_join=$pa_post_stat." PIN - (".$pa_code.")";
//                }
//
//                if($ApptDetail->pb_name!=''){
//                $pb_name="4. ".$ApptDetail->pb_name;
//                $pb_desig=$ApptDetail->pb_designation;
//                $pb_code=$ApptDetail->pb_personcd;
//                $pb_office=$ApptDetail->pb_officename;
//                $pb_ofc_address=$ApptDetail->pb_officeaddress;
//                $pb_ofc_address1="P.O. - ".$ApptDetail->pb_postoffice.", Subdiv. - ".$ApptDetail->pb_subdivision.", ".$ApptDetail->district;
//                $pb_ofc_cd="Mobile No. - ".$ApptDetail->pb_mobno.", OFFICE - (".$ApptDetail->pb_officecd.")";
//                $pb_post_stat=$ApptDetail->pb_post_stat;
//                $pb_join=$pb_post_stat." PIN - (".$pb_code.")";
//                }
//                
//                $pdf->SetFont('Arial','B','7');
//                $pdf->Cell($w[0],6,$ApptDetail->groupid,'LTR',0,'C',$fill);						
//                $pdf->Cell($w[1],6,$pr_name,'LTR',0,'L',$fill);
//                $pdf->Cell($w[2],6,$p1_name,'LTR',0,'L',$fill);
//                $pdf->Cell($w[3],6,$p1_name,'LTR',0,'L',$fill);
//                $pdf->Ln(4);
//
//                $pdf->SetFont('Arial','B','4.7');
//
//                $pdf->Cell($w[0],4,'','LR',0,'L',$fill);						
//                $pdf->Cell($w[1],4,$pr_desig,'LR',0,'L',$fill);
//                $pdf->Cell($w[2],4,$p1_desig,'LR',0,'L',$fill);
//                $pdf->Cell($w[3],4,$p1_desig,'LR',0,'L',$fill);
//                $pdf->Ln(3);
//
//                $pdf->SetFont('Arial','B','4.7');
//                $pdf->Cell($w[0],4,'','LR',0,'L',$fill);						
//                $pdf->Cell($w[1],4,$pr_join,'LR',0,'L',$fill);
//                $pdf->Cell($w[2],4,$p1_join,'LR',0,'L',$fill);
//                $pdf->Cell($w[3],4,$p1_join,'LR',0,'L',$fill);
//                $pdf->Ln(3);
//
//                $pdf->SetFont('Arial','B','4.7');
//                $pdf->Cell($w[0],4,'','LR',0,'L',$fill);						
//                $pdf->Cell($w[1],4,$pr_office,'LR',0,'L',$fill);
//                $pdf->Cell($w[2],4,$p1_office,'LR',0,'L',$fill);
//                $pdf->Cell($w[3],4,$p1_office,'LR',0,'L',$fill);
//                $pdf->Ln(3);
//
//                $pdf->Cell($w[0],4,'','LR',0,'L',$fill);						
//                $pdf->Cell($w[1],4,$pr_ofc_address,'LR',0,'L',$fill);
//                $pdf->Cell($w[2],4,$p1_ofc_address,'LR',0,'L',$fill);
//                $pdf->Cell($w[3],4,$p1_ofc_address,'LR',0,'L',$fill);
//                $pdf->Ln(3);
//
//                $pdf->Cell($w[0],4,'','LR',0,'L',$fill);						
//                $pdf->Cell($w[1],4,$pr_ofc_address1,'LR',0,'L',$fill);
//                $pdf->Cell($w[2],4,$p1_ofc_address1,'LR',0,'L',$fill);
//                $pdf->Cell($w[3],4,$p1_ofc_address1,'LR',0,'L',$fill);
//                $pdf->Ln(3);
//
//                $pdf->SetFont('Arial','B','4.7');
//                $pdf->Cell($w[0],4,'','LR',0,'L',$fill);						
//                $pdf->Cell($w[1],4,$pr_ofc_cd,'LR',0,'L',$fill);
//                $pdf->Cell($w[2],4,$p1_ofc_cd,'LR',0,'L',$fill);
//                $pdf->Cell($w[3],4,$p1_ofc_cd,'LR',0,'L',$fill);
//                $pdf->Ln();
//
//
//                $pdf->SetFont('Arial','B','7');
//                $pdf->Cell($w[0],6,'','LR',0,'L',$fill);						
//                $pdf->Cell($w[1],6,'','LR',0,'L',$fill);
//                $pdf->Cell($w[2],6,$p2_name,'LR',0,'L',$fill);
//                $pdf->Cell($w[3],6,'','LR',0,'L',$fill);
//                $pdf->Ln(4);
//
//                $pdf->SetFont('Arial','B','4.6');
//                $pdf->Cell($w[0],4,'','LR',0,'L',$fill);						
//                $pdf->Cell($w[1],4,'','LR',0,'L',$fill);
//                $pdf->Cell($w[2],4,$p2_desig,'LR',0,'L',$fill);
//                $pdf->Cell($w[3],4,'','LR',0,'L',$fill);
//                $pdf->Ln(3);
//                $pdf->SetFont('Arial','B','4.6');
//
//                $pdf->Cell($w[0],4,'','LR',0,'L',$fill);						
//                $pdf->Cell($w[1],4,'','LR',0,'L',$fill);
//                $pdf->Cell($w[2],4,$p2_join,'LR',0,'L',$fill);
//                $pdf->Cell($w[3],4,'','LR',0,'L',$fill);
//                $pdf->Ln(3);
//
//                $pdf->SetFont('Arial','B','4.6');
//                $pdf->Cell($w[0],4,'','LR',0,'L',$fill);						
//                $pdf->Cell($w[1],4,'','LR',0,'L',$fill);
//                $pdf->Cell($w[2],4,$p2_office,'LR',0,'L',$fill);
//                $pdf->Cell($w[3],4,'','LR',0,'L',$fill);
//                $pdf->Ln(3);
//
//                $pdf->Cell($w[0],4,'','LR',0,'L',$fill);						
//                $pdf->Cell($w[1],4,'','LR',0,'L',$fill);
//                $pdf->Cell($w[2],4,$p2_ofc_address,'LR',0,'L',$fill);
//                $pdf->Cell($w[3],4,'','LR',0,'L',$fill);
//                $pdf->Ln(3);
//
//                $pdf->Cell($w[0],4,'','LR',0,'L',$fill);						
//                $pdf->Cell($w[1],4,'','LR',0,'L',$fill);
//                $pdf->Cell($w[2],4,$p2_ofc_address1,'LR',0,'L',$fill);
//                $pdf->Cell($w[3],4,'','LR',0,'L',$fill);
//                $pdf->Ln(3);
//                $pdf->SetFont('Arial','B','4.6');
//
//                $pdf->Cell($w[0],5,'','LR',0,'L',$fill);						
//                $pdf->Cell($w[1],5,'','LR',0,'L',$fill);
//                $pdf->Cell($w[2],5,$p2_ofc_cd,'LR',0,'L',$fill);
//                $pdf->Cell($w[3],5,'','LR',0,'L',$fill);
//                $pdf->Ln();
//                //memb 5//
//                if($ApptDetail->mem_no==5 || $ApptDetail->mem_no==6)
//                {
//                $pdf->SetFont('Arial','B','7');
//                $pdf->Cell($w[0],6,'','LR',0,'L',$fill);						
//                $pdf->Cell($w[1],6,'','LR',0,'L',$fill);
//                $pdf->Cell($w[2],6,$pa_name,'LR',0,'L',$fill);
//                $pdf->Cell($w[3],6,'','LR',0,'L',$fill);
//                $pdf->Ln(4);
//
//                $pdf->SetFont('Arial','B','4.6');
//                $pdf->Cell($w[0],4,'','LR',0,'L',$fill);						
//                $pdf->Cell($w[1],4,'','LR',0,'L',$fill);
//                $pdf->Cell($w[2],4,$pa_desig,'LR',0,'L',$fill);
//                $pdf->Cell($w[3],4,'','LR',0,'L',$fill);
//                $pdf->Ln(3);
//
//                $pdf->SetFont('Arial','B','4.6');
//
//                $pdf->Cell($w[0],4,'','LR',0,'L',$fill);						
//                $pdf->Cell($w[1],4,'','LR',0,'L',$fill);
//                $pdf->Cell($w[2],4,$pa_join,'LR',0,'L',$fill);
//                $pdf->Cell($w[3],4,'','LR',0,'L',$fill);
//                $pdf->Ln(3);
//
//                $pdf->SetFont('Arial','B','4.6');
//                $pdf->Cell($w[0],4,'','LR',0,'L',$fill);						
//                $pdf->Cell($w[1],4,'','LR',0,'L',$fill);
//                $pdf->Cell($w[2],4,$pa_office,'LR',0,'L',$fill);
//                $pdf->Cell($w[3],4,'','LR',0,'L',$fill);
//                $pdf->Ln(3);
//
//                $pdf->Cell($w[0],4,'','LR',0,'L',$fill);						
//                $pdf->Cell($w[1],4,'','LR',0,'L',$fill);
//                $pdf->Cell($w[2],4,$pa_ofc_address,'LR',0,'L',$fill);
//                $pdf->Cell($w[3],4,'','LR',0,'L',$fill);
//                $pdf->Ln(3);
//
//                $pdf->Cell($w[0],4,'','LR',0,'L',$fill);						
//                $pdf->Cell($w[1],4,'','LR',0,'L',$fill);
//                $pdf->Cell($w[2],4,$pa_ofc_address1,'LR',0,'L',$fill);
//                $pdf->Cell($w[3],4,'','LR',0,'L',$fill);
//                $pdf->Ln(3);
//                $pdf->SetFont('Arial','B','4.6');
//
//                $pdf->Cell($w[0],5,'','LR',0,'L',$fill);						
//                $pdf->Cell($w[1],5,'','LR',0,'L',$fill);
//                $pdf->Cell($w[2],5,$pa_ofc_cd,'LR',0,'L',$fill);
//                $pdf->Cell($w[3],5,'','LR',0,'L',$fill);
//                $pdf->Ln();
//                }
//                //memb 6//
//                if($ApptDetail->mem_no==6)
//                {
//                $pdf->SetFont('Arial','B','7');
//                $pdf->Cell($w[0],6,'','LR',0,'L',$fill);						
//                $pdf->Cell($w[1],6,'','LR',0,'L',$fill);
//                $pdf->Cell($w[2],6,$pb_name,'LR',0,'L',$fill);
//                $pdf->Cell($w[3],6,'','LR',0,'L',$fill);
//                $pdf->Ln(4);
//
//                $pdf->SetFont('Arial','B','4.6');
//                $pdf->Cell($w[0],4,'','LR',0,'L',$fill);						
//                $pdf->Cell($w[1],4,'','LR',0,'L',$fill);
//                $pdf->Cell($w[2],4,$pb_desig,'LR',0,'L',$fill);
//                $pdf->Cell($w[3],4,'','LR',0,'L',$fill);
//                $pdf->Ln(3);
//                $pdf->SetFont('Arial','B','4.6');
//                $pdf->Cell($w[0],4,'','LR',0,'L',$fill);						
//                $pdf->Cell($w[1],4,'','LR',0,'L',$fill);
//                $pdf->Cell($w[2],4,$pb_join,'LR',0,'L',$fill);
//                $pdf->Cell($w[3],4,'','LR',0,'L',$fill);
//                $pdf->Ln(3);
//                $pdf->SetFont('Arial','B','4.6');
//                $pdf->Cell($w[0],4,'','LR',0,'L',$fill);						
//                $pdf->Cell($w[1],4,'','LR',0,'L',$fill);
//                $pdf->Cell($w[2],4,$pb_office,'LR',0,'L',$fill);
//                $pdf->Cell($w[3],4,'','LR',0,'L',$fill);
//                $pdf->Ln(3);
//
//                $pdf->Cell($w[0],4,'','LR',0,'L',$fill);						
//                $pdf->Cell($w[1],4,'','LR',0,'L',$fill);
//                $pdf->Cell($w[2],4,$pb_ofc_address,'LR',0,'L',$fill);
//                $pdf->Cell($w[3],4,'','LR',0,'L',$fill);
//                $pdf->Ln(3);
//
//                $pdf->Cell($w[0],4,'','LR',0,'L',$fill);						
//                $pdf->Cell($w[1],4,'','LR',0,'L',$fill);
//                $pdf->Cell($w[2],4,$pb_ofc_address1,'LR',0,'L',$fill);
//                $pdf->Cell($w[3],4,'','LR',0,'L',$fill);
//                $pdf->Ln(3);
//                $pdf->SetFont('Arial','B','4.6');
//                $pdf->Cell($w[0],5,'','LR',0,'L',$fill);						
//                $pdf->Cell($w[1],5,'','LR',0,'L',$fill);
//                $pdf->Cell($w[2],5,$pb_ofc_cd,'LR',0,'L',$fill);
//                $pdf->Cell($w[3],5,'','LR',0,'L',$fill);
//                $pdf->Ln();
//                }
//
//                //memb 6//
//                $pdf->SetFont('Arial','B','7');
//                $pdf->Cell($w[0],6,'','LR',0,'L',$fill);						
//                $pdf->Cell($w[1],6,'','LR',0,'L',$fill);
//                $pdf->Cell($w[2],6,$p3_name,'LR',0,'L',$fill);
//                $pdf->Cell($w[3],6,'','LR',0,'L',$fill);
//                $pdf->Ln(4);
//
//                $pdf->SetFont('Arial','B','4.6');
//                $pdf->Cell($w[0],4,'','LR',0,'L',$fill);						
//                $pdf->Cell($w[1],4,'','LR',0,'L',$fill);
//                $pdf->Cell($w[2],4,$p3_desig,'LR',0,'L',$fill);
//                $pdf->Cell($w[3],4,'','LR',0,'L',$fill);
//                $pdf->Ln(3);
//                $pdf->SetFont('Arial','B','4.6');
//                $pdf->Cell($w[0],4,'','LR',0,'L',$fill);						
//                $pdf->Cell($w[1],4,'','LR',0,'L',$fill);
//                $pdf->Cell($w[2],4,$p3_join,'LR',0,'L',$fill);
//                $pdf->Cell($w[3],4,'','LR',0,'L',$fill);
//                $pdf->Ln(3);
//                $pdf->SetFont('Arial','B','4.6');
//                $pdf->Cell($w[0],4,'','LR',0,'L',$fill);						
//                $pdf->Cell($w[1],4,'','LR',0,'L',$fill);
//                $pdf->Cell($w[2],4,$p3_office,'LR',0,'L',$fill);
//                $pdf->Cell($w[3],4,'','LR',0,'L',$fill);
//                $pdf->Ln(3);
//
//                $pdf->Cell($w[0],4,'','LR',0,'L',$fill);						
//                $pdf->Cell($w[1],4,'','LR',0,'L',$fill);
//                $pdf->Cell($w[2],4,$p3_ofc_address,'LR',0,'L',$fill);
//                $pdf->Cell($w[3],4,'','LR',0,'L',$fill);
//                $pdf->Ln(3);
//
//                $pdf->Cell($w[0],4,'','LR',0,'L',$fill);						
//                $pdf->Cell($w[1],4,'','LR',0,'L',$fill);
//                $pdf->Cell($w[2],4,$p3_ofc_address1,'LR',0,'L',$fill);
//                $pdf->Cell($w[3],4,'','LR',0,'L',$fill);
//                $pdf->Ln(3);
//                $pdf->SetFont('Arial','B','4.6');
//                $pdf->Cell($w[0],5,'','LR',0,'L',$fill);						
//                $pdf->Cell($w[1],5,'','LR',0,'L',$fill);
//                $pdf->Cell($w[2],5,$p3_ofc_cd,'LR',0,'L',$fill);
//                $pdf->Cell($w[3],5,'','LR',0,'L',$fill);
//
//
//                $pdf->Ln();
//                $pdf->Cell(array_sum($w),0,'',1,0,'L',$fill);
//        //	$pdf->Ln(5);
//
//                $pdf->Ln(4);
//
//                $pdf->SetFont('Arial','',8.7);
//                $pdf->MultiCell(190,4,"     ".$euname9.$euname10.$euname11.".",0,'J',$fill);
//                
//                $pdf->Ln();
//			
//			
//                $pdf->SetFont('Arial','',9);
//                //$pdf->Cell(80);
//                $pdf->Cell(30,10,$euname12,0,0,'L');
//                $pdf->SetFont('Arial','',10);			
//                $pdf->Cell(120);
//                $pdf->Cell(10,10,"Signature",0,0,'R');
//                // Line break
//                $pdf->Ln(8.3);
//
//                $pdf->SetFont('Arial','',9);
//                $pdf->Cell(30,1,$euname13,0,0,'L');
//                $pdf->SetFont('Arial','',10);			
//                $pdf->Cell(110);
//        //	$pdf->Cell(10,10,"yuyu",0,0,'R');
//                $pdf->Cell(10, 10, $pdf->Image($signature, $pdf->GetX(), $pdf->GetY(), 30.78), 0, 0, 'R', false );
//                // Line break
//                $pdf->Ln(7);
//
//                $pdf->SetFont('Arial','',8);
//                $pdf->Cell(164);
//                $pdf->Cell(10,10,$nb3,0,0,'R');
//
//                // Line break
//                $pdf->Ln(4);
//                $pdf->SetFont('Arial','',9);
//                $pdf->Cell(160);
//                $pdf->Cell(10,10,$roname,0,0,'R');
//
//                // Line break
//                $pdf->Ln(4);
//                $pdf->SetFont('Arial','',9);
//                $pdf->Cell(160);
//                $pdf->Cell(10,10,$euname14,0,0,'R');
//
//                // Line break
//                $pdf->Ln();			
//                $pdf->Cell(190,0,'',1,0,'L',$fill);
//                $pdf->Ln();
//
//
//                $pdf->SetFont('Arial','',8.7);
//                $pdf->Ln(1);
//                $pdf->MultiCell(190,4,$nb1.$nb2,0,'J',$fill);
//                $pdf->Ln(1);
//                //$pdf->Ln();
//                $pdf->SetFont('Arial','',8.7);
//                $pdf->MultiCell(190,4,"2. This is also to inform you that, a Facilitation Cntre for submission of Postal Ballot will be made at your respective training centre.You may utilize the facility.",0,'J');
//                //$bmname=$row['block_muni_cd'];
//                $pdf->Ln(1);
//                $bmname=($assembly=='')?"Block/Municipality: ".$ApptDetail->block_muni_name:"";
//                $pdf->SetFont('Arial','',9);
//                $pdf->Cell(190,5,$bmname,0,0,'C');
//                
//                $count++;
//            }
//            if($count==$per_page)
//            {
//                $per_page=$per_page+1;
//                if($count!=Count($ApptRecord))
//                {
//                  $pdf->AddPage();
//                }
//            } 
//               
//        }
//    
//        $pdf->Output();
//        exit;
//    }
//    //::::::::::::::::::::::: Second Appt Letter PDF 6 (Assembly Election):::::::::::::::::::://
//    public function getSecondAppointmentLetterPDF6(Request $request) {
//        $validate_array=['forZone' => 'required|alpha_num|min:4|max:4',
//                            'party_reserve'=> 'required|alpha|min:1|max:1',
//                            'assembly' => 'nullable|alpha_num|min:3|max:3',
//                            'group_id'=> 'nullable|digits_between:1,4',
//                            'no_of_member'=> 'nullable|digits_between:1,1',
//                            'from'=>'required|digits_between:1,4',
//                            'to'=>'required|digits_between:1,4'
//                           ];
//
//           $validate_array1=['forZone.required' => 'For Zone is required',
//                             'forZone.alpha_num' => 'For Zone must be an alpha numeric characters',
//                             'party_reserve.required' => 'Party/Reserve is required',
//                             'party_reserve.alpha' => 'Party/Reserve must be an alpha characters',
//                             'assembly.alpha_num' => 'Assembly must be an alpha numeric characters',
//                             'group_id.digits_between' => 'Group ID must be an integer',
//                             'no_of_member.digits_between' => 'No of Member must be an integer',
//                             'from.required' => 'Records From is required',
//                             'from.digits_between' => 'Records From must be an integer',
//                             'to.required' => 'Records To is required',
//                             'to.digits_between' => 'Records To must be an integer'
//                            ];
//
//           $this->validate($request,$validate_array,$validate_array1);
//           
//        $forzone=$request->forZone;
//        $party_reserve=$request->party_reserve;
//        $assembly=$request->assembly;
//        $group_id=$request->group_id;
//        $no_of_member=$request->no_of_member;
//        $pfrom=$request->from;
//        $pto=$request->to;
//        $count=0;
//        
//       
//
//        $ApptRecord=DB::select('call getSecondAppointmentLetterPDF45(?,?,?,?,?,?,?)',[$forzone,$party_reserve,$assembly,$group_id,$no_of_member,$pfrom,$pto]);
//        $pdf = app('FPDF');
//        $pdf->AddPage();
//        $count=0;
//        $per_page=1;
//        $signaturePath=config('constants.DEOSIGNATURE');
//        foreach ($ApptRecord as $ApptDetail)
//        {
//            if($count<$per_page)
//            {
//                $fill = false;
//                $poll_date=$ApptDetail->polldate;
//                $poll_time=$ApptDetail->polltime;
//                $training_venue=$ApptDetail->training_venue;
//                $training_subvenue=$ApptDetail->sub_venuename;
//                $venue_addr=$ApptDetail->venue_addr;
//                $training_date=$ApptDetail->training_date;
//                $training_time=$ApptDetail->training_time;
//
//                $dc=($ApptDetail->dc_venue!=''?$ApptDetail->dc_venue.", ".$ApptDetail->dc_address:"___________________________________");
//                $dc_date=($ApptDetail->dc_date!=''?$ApptDetail->dc_date:"___________");
//                $dc_time=($ApptDetail->dc_time!=''?$ApptDetail->dc_time:"___________");
//                $rcvenue=($ApptDetail->rc_venue!=''?$ApptDetail->rc_venue:"_______________________________");
//                
//                $euname="ELECTION URGENT";
//                $euname1="ORDER OF APPOINTMENT FOR POLLING DUTIES";
//                $euname2="GENERAL ELECTION TO WEST BENGAL LEGISLATIVE";
//                $euname21="ASSEMBLY ELECTION, 2016";
//                $euname3=($assembly=='')?$ApptDetail->pers_off."/".$ApptDetail->per_poststat:"";
//                $euname22="Polling Party No. ".$ApptDetail->groupid;
//                $euname4="Order No: ";
//                $euname5="Date: ";
//
//                $euname6="     In persuance of sub-section(1) and sub-section(3) of section 26 of the Representation of the People Act,1963(43 of 1951), I hereby ";
//                $euname7="appoint the officers specified in columb(2) and (3) of the table below as Presiding Officer and Polling Officers respectively for the Polling ";
//                $euname8="Station specified in corresponding entry in column(1) of the table provided by me for ".$ApptDetail->assembly." - ".$ApptDetail->assembly_name." L.A. Constituency ";
//                $euname78="forming part of ".$ApptDetail->pcname." Parliamentary Constituency.";
//                $euname81="I also authorise the Polling Officer specified in column(4) of the table against that entry to perform the functions of the Presiding Officer";
//                $euname82="during the unavoidable absence, if any, of the Presiding Officer.";
//
//                $euname9="The Poll will be taken on ".$poll_date." during the hours ".$poll_time.". The Presiding Officer should arrange to collect the Polling ";
//                $euname10="materials from ".$dc." on ".$dc_date." at ".$dc_time." ";
//                $euname11="and after the Poll, these should be returned to collecting centre at ".$rcvenue;
//
//                $euname12="Place: ".$ApptDetail->district;
//                $euname13="Date: ".date("d/m/Y");
//                $euname14="District ".$ApptDetail->district;
//                $nb1="1. You are requested to attend the training at ".$training_venue." ".$training_subvenue." , ".$venue_addr;
//                $nb2="on ".$training_date." from ".$training_time;
//                $nb3="(__________________________)";
//			
//	        $signature=$signaturePath.$ApptDetail->assembly.".jpg";
//		$roname="RO/".$ApptDetail->assembly." - ".$ApptDetail->assembly_name;
//
//                $pdf->SetFont('Arial','B',10);
//                $pdf->Cell(37,6,$euname,1,0,'L');
//                $pdf->SetFont('Arial','B',10);
//                $pdf->Cell(20);
//                $pdf->Cell(84,4,$euname1,'B',0,'C');
//                $pdf->SetFont('Arial','B',8);
//                $pdf->Cell(28);
//                $pdf->Cell(10,3,$euname3,0,0,'R');
//
//                // Line break
//                $pdf->Ln(6);
//
//                $pdf->SetFont('Arial','B',8);
//                $pdf->Cell(15,10,'',0,0,'L');
//                $pdf->SetFont('Arial','',10);
//                $pdf->Cell(38);
//                $pdf->Cell(92,4,$euname2,'B',0,'C');
//                $pdf->SetFont('Arial','B',8);
//                $pdf->Cell(8);
//                $pdf->SetFont('Arial','B',10);
//                $pdf->Cell(37,6,$euname22,1,0,'R');
//
//                $pdf->Ln(5);
//
//                $pdf->SetFont('Arial','B',8);
//                $pdf->Cell(15,10,'',0,0,'L');
//                $pdf->SetFont('Arial','',10);
//                $pdf->Cell(60);
//                $pdf->Cell(47,4,$euname21,'B',0,'C');
//                $pdf->SetFont('Arial','B',8);
//                $pdf->Cell(13);
//                $pdf->SetFont('Arial','B',10);
//                $pdf->Cell(33,6,'',0,0,'R');
//                // Line break
//
//                $pdf->Ln(4);
//
//                $pdf->SetFont('Arial','',9);
//                $pdf->Cell(15,10,$euname4,0,0,'L');
//                $pdf->SetFont('Arial','',10);
//                $pdf->Cell(60);
//                $pdf->Cell(47,4,'','',0,'C');
//                $pdf->SetFont('Arial','',8);
//                $pdf->Cell(33);
//                $pdf->SetFont('Arial','',9);
//                $pdf->Cell(33,6,$euname5,0,0,'R');
//
//                $pdf->Ln(6);
//
//                $pdf->Ln();
//                $pdf->SetFont('Arial','',8.7);
//                $pdf->MultiCell(190,4,$euname6.$euname7.$euname8.$euname78,0,'J');
//
//                $pdf->SetFont('Arial','',8.7);
//                $pdf->Cell(87);
//                $pdf->Cell(20,10,$euname81,0,0,'C');
//
//                // Line break
//                $pdf->Ln(4);
//                $pdf->SetFont('Arial','',8.7);
//                $pdf->Cell(50,10,$euname82,0,0,'L');
//
//                // Line break
//                $pdf->Ln(10);
//
//                $pdf->SetFillColor(255,255,255);
//                $pdf->SetDrawColor(0,0,0);
//                $pdf->SetLineWidth(.3);
//                $pdf->SetFont('Arial','B','8');
//
//                $head = array('Polling','Name of the Presiding Officer','Name of the Polling Officers','Polling Officer authorised to perform');
//                $w = array(11,59,60,60);
//                $head1=array('Party','','','the functions of the Presiding Officer');
//                $head2=array('No.','','',"in the latter's absence");
//                $head3=array('(1)','(2)','(3)',"(4)");
//
//                //	$this->SetFont('Arial','',9);
//                for($j=0;$j<count($head);$j++)
//                        $pdf->Cell($w[$j],5,$head[$j],'LTR',0,'C',true);				 
//                $pdf->Ln();
//                for($j1=0;$j1<count($head1);$j1++)
//                        $pdf->Cell($w[$j1],4,$head1[$j1],'LR',0,'C',true);
//                $pdf->Ln();	
//                for($j2=0;$j2<count($head2);$j2++)
//                        $pdf->Cell($w[$j2],5,$head2[$j2],'LR',0,'C',true);
//                $pdf->Ln();
//
//                for($j3=0;$j3<count($head3);$j3++)
//                        $pdf->Cell($w[$j3],7,$head3[$j3],1,0,'C',true);
//                $pdf->Ln();
//                
//                $pr_name=$ApptDetail->pr_name;
//                $pr_desig=$ApptDetail->pr_designation;
//                $pr_code=$ApptDetail->pr_personcd;
//                $pr_office=$ApptDetail->pr_officename;
//                $pr_ofc_address=$ApptDetail->pr_officeaddress;
//                $pr_ofc_address1="P.O. - ".$ApptDetail->pr_postoffice.", Subdiv. - ".$ApptDetail->pr_subdivision.", ".$ApptDetail->district;
//                $pr_ofc_cd="Mobile No. - ".$ApptDetail->pr_mobno.", OFFICE - (".$ApptDetail->pr_officecd.")";
//                $pr_post_stat=$ApptDetail->pr_post_stat;
//                $pr_join=$pr_post_stat." PIN - (".$pr_code.")";
//
//                $p1_name="1. ".$ApptDetail->p1_name;
//                $p1_desig=$ApptDetail->p1_designation;
//                $p1_code=$ApptDetail->p1_personcd;
//                $p1_office=$ApptDetail->p1_officename;
//                $p1_ofc_address=$ApptDetail->p1_officeaddress;
//                $p1_ofc_address1="P.O. - ".$ApptDetail->p1_postoffice.", Subdiv. - ".$ApptDetail->p1_subdivision.", ".$ApptDetail->district;
//                $p1_ofc_cd="Mobile No. - ".$ApptDetail->p1_mobno.", OFFICE - (".$ApptDetail->p1_officecd.")";
//                $p1_post_stat=$ApptDetail->p1_post_stat;
//                $p1_join=$p1_post_stat." PIN - (".$p1_code.")";
//
//                $p2_name="2. ".$ApptDetail->p2_name;
//                $p2_desig=$ApptDetail->p2_designation;
//                $p2_code=$ApptDetail->p2_personcd;
//                $p2_office=$ApptDetail->p2_officename;
//                $p2_ofc_address=$ApptDetail->p2_officeaddress;
//                $p2_ofc_address1="P.O. - ".$ApptDetail->p2_postoffice.", Subdiv. - ".$ApptDetail->p2_subdivision.", ".$ApptDetail->district;
//                $p2_ofc_cd="Mobile No. - ".$ApptDetail->p2_mobno.", OFFICE - (".$ApptDetail->p2_officecd.")";
//                $p2_post_stat=$ApptDetail->p2_post_stat;
//                $p2_join=$p2_post_stat." PIN - (".$p2_code.")";
//
//                $sl=(($ApptDetail->pa_name=='')?"3. ":(($ApptDetail->pb_name=='')?"4. ":"5. "));
//
//                $p3_name=$sl.$ApptDetail->p3_name;
//                $p3_desig=$ApptDetail->p3_designation;
//                $p3_code=$ApptDetail->p3_personcd;
//                $p3_office=$ApptDetail->p3_officename;
//                $p3_ofc_address=$ApptDetail->p3_officeaddress;
//                $p3_ofc_address1="P.O. - ".$ApptDetail->p3_postoffice.", Subdiv. - ".$ApptDetail->p3_subdivision.", ".$ApptDetail->district;
//                $p3_ofc_cd="Mobile No. - ".$ApptDetail->p3_mobno.", OFFICE - (".$ApptDetail->p3_officecd.")";
//                $p3_post_stat=$ApptDetail->p3_post_stat;
//                $p3_join=$p3_post_stat." PIN - (".$p3_code.")";
//
//
//                if($ApptDetail->pa_name!=''){
//                $pa_name="3. ".$ApptDetail->pa_name;
//                $pa_desig=$ApptDetail->pa_designation;
//                $pa_code=$ApptDetail->pa_personcd;
//                $pa_office=$ApptDetail->pa_officename;
//                $pa_ofc_address=$ApptDetail->pa_officeaddress;
//                $pa_ofc_address1="P.O. - ".$ApptDetail->pa_postoffice.", Subdiv. - ".$ApptDetail->pa_subdivision.", ".$ApptDetail->district;
//                $pa_ofc_cd="Mobile No. - ".$ApptDetail->pa_mobno.", OFFICE - (".$ApptDetail->pa_officecd.")";
//                $pa_post_stat=$ApptDetail->pa_post_stat;
//                $pa_join=$pa_post_stat." PIN - (".$pa_code.")";
//                }
//
//                if($ApptDetail->pb_name!=''){
//                $pb_name="4. ".$ApptDetail->pb_name;
//                $pb_desig=$ApptDetail->pb_designation;
//                $pb_code=$ApptDetail->pb_personcd;
//                $pb_office=$ApptDetail->pb_officename;
//                $pb_ofc_address=$ApptDetail->pb_officeaddress;
//                $pb_ofc_address1="P.O. - ".$ApptDetail->pb_postoffice.", Subdiv. - ".$ApptDetail->pb_subdivision.", ".$ApptDetail->district;
//                $pb_ofc_cd="Mobile No. - ".$ApptDetail->pb_mobno.", OFFICE - (".$ApptDetail->pb_officecd.")";
//                $pb_post_stat=$ApptDetail->pb_post_stat;
//                $pb_join=$pb_post_stat." PIN - (".$pb_code.")";
//                }
//                
//                $pdf->SetFont('Arial','B','7');
//                $pdf->Cell($w[0],6,$ApptDetail->groupid,'LTR',0,'C',$fill);						
//                $pdf->Cell($w[1],6,$pr_name,'LTR',0,'L',$fill);
//                $pdf->Cell($w[2],6,$p1_name,'LTR',0,'L',$fill);
//                $pdf->Cell($w[3],6,$p1_name,'LTR',0,'L',$fill);
//                $pdf->Ln(4);
//
//                $pdf->SetFont('Arial','B','4.7');
//
//                $pdf->Cell($w[0],4,'','LR',0,'L',$fill);						
//                $pdf->Cell($w[1],4,$pr_desig,'LR',0,'L',$fill);
//                $pdf->Cell($w[2],4,$p1_desig,'LR',0,'L',$fill);
//                $pdf->Cell($w[3],4,$p1_desig,'LR',0,'L',$fill);
//                $pdf->Ln(3);
//
//                $pdf->SetFont('Arial','B','4.7');
//                $pdf->Cell($w[0],4,'','LR',0,'L',$fill);						
//                $pdf->Cell($w[1],4,$pr_join,'LR',0,'L',$fill);
//                $pdf->Cell($w[2],4,$p1_join,'LR',0,'L',$fill);
//                $pdf->Cell($w[3],4,$p1_join,'LR',0,'L',$fill);
//                $pdf->Ln(3);
//
//                $pdf->SetFont('Arial','B','4.7');
//                $pdf->Cell($w[0],4,'','LR',0,'L',$fill);						
//                $pdf->Cell($w[1],4,$pr_office,'LR',0,'L',$fill);
//                $pdf->Cell($w[2],4,$p1_office,'LR',0,'L',$fill);
//                $pdf->Cell($w[3],4,$p1_office,'LR',0,'L',$fill);
//                $pdf->Ln(3);
//
//                $pdf->Cell($w[0],4,'','LR',0,'L',$fill);						
//                $pdf->Cell($w[1],4,$pr_ofc_address,'LR',0,'L',$fill);
//                $pdf->Cell($w[2],4,$p1_ofc_address,'LR',0,'L',$fill);
//                $pdf->Cell($w[3],4,$p1_ofc_address,'LR',0,'L',$fill);
//                $pdf->Ln(3);
//
//                $pdf->Cell($w[0],4,'','LR',0,'L',$fill);						
//                $pdf->Cell($w[1],4,$pr_ofc_address1,'LR',0,'L',$fill);
//                $pdf->Cell($w[2],4,$p1_ofc_address1,'LR',0,'L',$fill);
//                $pdf->Cell($w[3],4,$p1_ofc_address1,'LR',0,'L',$fill);
//                $pdf->Ln(3);
//
//                $pdf->SetFont('Arial','B','4.7');
//                $pdf->Cell($w[0],4,'','LR',0,'L',$fill);						
//                $pdf->Cell($w[1],4,$pr_ofc_cd,'LR',0,'L',$fill);
//                $pdf->Cell($w[2],4,$p1_ofc_cd,'LR',0,'L',$fill);
//                $pdf->Cell($w[3],4,$p1_ofc_cd,'LR',0,'L',$fill);
//                $pdf->Ln();
//
//
//                $pdf->SetFont('Arial','B','7');
//                $pdf->Cell($w[0],6,'','LR',0,'L',$fill);						
//                $pdf->Cell($w[1],6,'','LR',0,'L',$fill);
//                $pdf->Cell($w[2],6,$p2_name,'LR',0,'L',$fill);
//                $pdf->Cell($w[3],6,'','LR',0,'L',$fill);
//                $pdf->Ln(4);
//
//                $pdf->SetFont('Arial','B','4.6');
//                $pdf->Cell($w[0],4,'','LR',0,'L',$fill);						
//                $pdf->Cell($w[1],4,'','LR',0,'L',$fill);
//                $pdf->Cell($w[2],4,$p2_desig,'LR',0,'L',$fill);
//                $pdf->Cell($w[3],4,'','LR',0,'L',$fill);
//                $pdf->Ln(3);
//                $pdf->SetFont('Arial','B','4.6');
//
//                $pdf->Cell($w[0],4,'','LR',0,'L',$fill);						
//                $pdf->Cell($w[1],4,'','LR',0,'L',$fill);
//                $pdf->Cell($w[2],4,$p2_join,'LR',0,'L',$fill);
//                $pdf->Cell($w[3],4,'','LR',0,'L',$fill);
//                $pdf->Ln(3);
//
//                $pdf->SetFont('Arial','B','4.6');
//                $pdf->Cell($w[0],4,'','LR',0,'L',$fill);						
//                $pdf->Cell($w[1],4,'','LR',0,'L',$fill);
//                $pdf->Cell($w[2],4,$p2_office,'LR',0,'L',$fill);
//                $pdf->Cell($w[3],4,'','LR',0,'L',$fill);
//                $pdf->Ln(3);
//
//                $pdf->Cell($w[0],4,'','LR',0,'L',$fill);						
//                $pdf->Cell($w[1],4,'','LR',0,'L',$fill);
//                $pdf->Cell($w[2],4,$p2_ofc_address,'LR',0,'L',$fill);
//                $pdf->Cell($w[3],4,'','LR',0,'L',$fill);
//                $pdf->Ln(3);
//
//                $pdf->Cell($w[0],4,'','LR',0,'L',$fill);						
//                $pdf->Cell($w[1],4,'','LR',0,'L',$fill);
//                $pdf->Cell($w[2],4,$p2_ofc_address1,'LR',0,'L',$fill);
//                $pdf->Cell($w[3],4,'','LR',0,'L',$fill);
//                $pdf->Ln(3);
//                $pdf->SetFont('Arial','B','4.6');
//
//                $pdf->Cell($w[0],5,'','LR',0,'L',$fill);						
//                $pdf->Cell($w[1],5,'','LR',0,'L',$fill);
//                $pdf->Cell($w[2],5,$p2_ofc_cd,'LR',0,'L',$fill);
//                $pdf->Cell($w[3],5,'','LR',0,'L',$fill);
//                $pdf->Ln();
//                //memb 5//
//                if($ApptDetail->mem_no==5 || $ApptDetail->mem_no==6)
//                {
//                $pdf->SetFont('Arial','B','7');
//                $pdf->Cell($w[0],6,'','LR',0,'L',$fill);						
//                $pdf->Cell($w[1],6,'','LR',0,'L',$fill);
//                $pdf->Cell($w[2],6,$pa_name,'LR',0,'L',$fill);
//                $pdf->Cell($w[3],6,'','LR',0,'L',$fill);
//                $pdf->Ln(4);
//
//                $pdf->SetFont('Arial','B','4.6');
//                $pdf->Cell($w[0],4,'','LR',0,'L',$fill);						
//                $pdf->Cell($w[1],4,'','LR',0,'L',$fill);
//                $pdf->Cell($w[2],4,$pa_desig,'LR',0,'L',$fill);
//                $pdf->Cell($w[3],4,'','LR',0,'L',$fill);
//                $pdf->Ln(3);
//
//                $pdf->SetFont('Arial','B','4.6');
//
//                $pdf->Cell($w[0],4,'','LR',0,'L',$fill);						
//                $pdf->Cell($w[1],4,'','LR',0,'L',$fill);
//                $pdf->Cell($w[2],4,$pa_join,'LR',0,'L',$fill);
//                $pdf->Cell($w[3],4,'','LR',0,'L',$fill);
//                $pdf->Ln(3);
//
//                $pdf->SetFont('Arial','B','4.6');
//                $pdf->Cell($w[0],4,'','LR',0,'L',$fill);						
//                $pdf->Cell($w[1],4,'','LR',0,'L',$fill);
//                $pdf->Cell($w[2],4,$pa_office,'LR',0,'L',$fill);
//                $pdf->Cell($w[3],4,'','LR',0,'L',$fill);
//                $pdf->Ln(3);
//
//                $pdf->Cell($w[0],4,'','LR',0,'L',$fill);						
//                $pdf->Cell($w[1],4,'','LR',0,'L',$fill);
//                $pdf->Cell($w[2],4,$pa_ofc_address,'LR',0,'L',$fill);
//                $pdf->Cell($w[3],4,'','LR',0,'L',$fill);
//                $pdf->Ln(3);
//
//                $pdf->Cell($w[0],4,'','LR',0,'L',$fill);						
//                $pdf->Cell($w[1],4,'','LR',0,'L',$fill);
//                $pdf->Cell($w[2],4,$pa_ofc_address1,'LR',0,'L',$fill);
//                $pdf->Cell($w[3],4,'','LR',0,'L',$fill);
//                $pdf->Ln(3);
//                $pdf->SetFont('Arial','B','4.6');
//
//                $pdf->Cell($w[0],5,'','LR',0,'L',$fill);						
//                $pdf->Cell($w[1],5,'','LR',0,'L',$fill);
//                $pdf->Cell($w[2],5,$pa_ofc_cd,'LR',0,'L',$fill);
//                $pdf->Cell($w[3],5,'','LR',0,'L',$fill);
//                $pdf->Ln();
//                }
//                //memb 6//
//                if($ApptDetail->mem_no==6)
//                {
//                $pdf->SetFont('Arial','B','7');
//                $pdf->Cell($w[0],6,'','LR',0,'L',$fill);						
//                $pdf->Cell($w[1],6,'','LR',0,'L',$fill);
//                $pdf->Cell($w[2],6,$pb_name,'LR',0,'L',$fill);
//                $pdf->Cell($w[3],6,'','LR',0,'L',$fill);
//                $pdf->Ln(4);
//
//                $pdf->SetFont('Arial','B','4.6');
//                $pdf->Cell($w[0],4,'','LR',0,'L',$fill);						
//                $pdf->Cell($w[1],4,'','LR',0,'L',$fill);
//                $pdf->Cell($w[2],4,$pb_desig,'LR',0,'L',$fill);
//                $pdf->Cell($w[3],4,'','LR',0,'L',$fill);
//                $pdf->Ln(3);
//                $pdf->SetFont('Arial','B','4.6');
//                $pdf->Cell($w[0],4,'','LR',0,'L',$fill);						
//                $pdf->Cell($w[1],4,'','LR',0,'L',$fill);
//                $pdf->Cell($w[2],4,$pb_join,'LR',0,'L',$fill);
//                $pdf->Cell($w[3],4,'','LR',0,'L',$fill);
//                $pdf->Ln(3);
//                $pdf->SetFont('Arial','B','4.6');
//                $pdf->Cell($w[0],4,'','LR',0,'L',$fill);						
//                $pdf->Cell($w[1],4,'','LR',0,'L',$fill);
//                $pdf->Cell($w[2],4,$pb_office,'LR',0,'L',$fill);
//                $pdf->Cell($w[3],4,'','LR',0,'L',$fill);
//                $pdf->Ln(3);
//
//                $pdf->Cell($w[0],4,'','LR',0,'L',$fill);						
//                $pdf->Cell($w[1],4,'','LR',0,'L',$fill);
//                $pdf->Cell($w[2],4,$pb_ofc_address,'LR',0,'L',$fill);
//                $pdf->Cell($w[3],4,'','LR',0,'L',$fill);
//                $pdf->Ln(3);
//
//                $pdf->Cell($w[0],4,'','LR',0,'L',$fill);						
//                $pdf->Cell($w[1],4,'','LR',0,'L',$fill);
//                $pdf->Cell($w[2],4,$pb_ofc_address1,'LR',0,'L',$fill);
//                $pdf->Cell($w[3],4,'','LR',0,'L',$fill);
//                $pdf->Ln(3);
//                $pdf->SetFont('Arial','B','4.6');
//                $pdf->Cell($w[0],5,'','LR',0,'L',$fill);						
//                $pdf->Cell($w[1],5,'','LR',0,'L',$fill);
//                $pdf->Cell($w[2],5,$pb_ofc_cd,'LR',0,'L',$fill);
//                $pdf->Cell($w[3],5,'','LR',0,'L',$fill);
//                $pdf->Ln();
//                }
//
//                //memb 6//
//                $pdf->SetFont('Arial','B','7');
//                $pdf->Cell($w[0],6,'','LR',0,'L',$fill);						
//                $pdf->Cell($w[1],6,'','LR',0,'L',$fill);
//                $pdf->Cell($w[2],6,$p3_name,'LR',0,'L',$fill);
//                $pdf->Cell($w[3],6,'','LR',0,'L',$fill);
//                $pdf->Ln(4);
//
//                $pdf->SetFont('Arial','B','4.6');
//                $pdf->Cell($w[0],4,'','LR',0,'L',$fill);						
//                $pdf->Cell($w[1],4,'','LR',0,'L',$fill);
//                $pdf->Cell($w[2],4,$p3_desig,'LR',0,'L',$fill);
//                $pdf->Cell($w[3],4,'','LR',0,'L',$fill);
//                $pdf->Ln(3);
//                $pdf->SetFont('Arial','B','4.6');
//                $pdf->Cell($w[0],4,'','LR',0,'L',$fill);						
//                $pdf->Cell($w[1],4,'','LR',0,'L',$fill);
//                $pdf->Cell($w[2],4,$p3_join,'LR',0,'L',$fill);
//                $pdf->Cell($w[3],4,'','LR',0,'L',$fill);
//                $pdf->Ln(3);
//                $pdf->SetFont('Arial','B','4.6');
//                $pdf->Cell($w[0],4,'','LR',0,'L',$fill);						
//                $pdf->Cell($w[1],4,'','LR',0,'L',$fill);
//                $pdf->Cell($w[2],4,$p3_office,'LR',0,'L',$fill);
//                $pdf->Cell($w[3],4,'','LR',0,'L',$fill);
//                $pdf->Ln(3);
//
//                $pdf->Cell($w[0],4,'','LR',0,'L',$fill);						
//                $pdf->Cell($w[1],4,'','LR',0,'L',$fill);
//                $pdf->Cell($w[2],4,$p3_ofc_address,'LR',0,'L',$fill);
//                $pdf->Cell($w[3],4,'','LR',0,'L',$fill);
//                $pdf->Ln(3);
//
//                $pdf->Cell($w[0],4,'','LR',0,'L',$fill);						
//                $pdf->Cell($w[1],4,'','LR',0,'L',$fill);
//                $pdf->Cell($w[2],4,$p3_ofc_address1,'LR',0,'L',$fill);
//                $pdf->Cell($w[3],4,'','LR',0,'L',$fill);
//                $pdf->Ln(3);
//                $pdf->SetFont('Arial','B','4.6');
//                $pdf->Cell($w[0],5,'','LR',0,'L',$fill);						
//                $pdf->Cell($w[1],5,'','LR',0,'L',$fill);
//                $pdf->Cell($w[2],5,$p3_ofc_cd,'LR',0,'L',$fill);
//                $pdf->Cell($w[3],5,'','LR',0,'L',$fill);
//
//
//                $pdf->Ln();
//                $pdf->Cell(array_sum($w),0,'',1,0,'L',$fill);
//        //	$pdf->Ln(5);
//
//                $pdf->Ln(4);
//
//                $pdf->SetFont('Arial','',8.7);
//                $pdf->MultiCell(190,4,"     ".$euname9.$euname10.$euname11.".",0,'J',$fill);
//                
//                $pdf->Ln();
//			
//			
//                $pdf->SetFont('Arial','',9);
//                //$pdf->Cell(80);
//                $pdf->Cell(30,10,$euname12,0,0,'L');
//                $pdf->SetFont('Arial','',10);			
//                $pdf->Cell(120);
//                $pdf->Cell(10,10,"Signature",0,0,'R');
//                // Line break
//                $pdf->Ln(8.3);
//
//                $pdf->SetFont('Arial','',9);
//                $pdf->Cell(30,1,$euname13,0,0,'L');
//                $pdf->SetFont('Arial','',10);			
//                $pdf->Cell(110);
//        //	$pdf->Cell(10,10,"yuyu",0,0,'R');
//                $pdf->Cell(10, 10, $pdf->Image($signature, $pdf->GetX(), $pdf->GetY(), 30.78), 0, 0, 'R', false );
//                // Line break
//                $pdf->Ln(7);
//
//                $pdf->SetFont('Arial','',8);
//                $pdf->Cell(164);
//                $pdf->Cell(10,10,$nb3,0,0,'R');
//
//                // Line break
//                $pdf->Ln(4);
//                $pdf->SetFont('Arial','',9);
//                $pdf->Cell(160);
//                $pdf->Cell(10,10,$roname,0,0,'R');
//
//                // Line break
//                $pdf->Ln(4);
//                $pdf->SetFont('Arial','',9);
//                $pdf->Cell(160);
//                $pdf->Cell(10,10,$euname14,0,0,'R');
//
//                // Line break
//                $pdf->Ln();			
//                $pdf->Cell(190,0,'',1,0,'L',$fill);
//                $pdf->Ln();
//
//
//                $pdf->SetFont('Arial','',8.7);
//                $pdf->Ln(1);
//                $pdf->MultiCell(190,4,$nb1.$nb2,0,'J',$fill);
//                $pdf->Ln(1);
//                //$pdf->Ln();
//                $pdf->SetFont('Arial','',8.7);
//                $pdf->MultiCell(190,4,"2. This is also to inform you that, a Facilitation Cntre for submission of Postal Ballot will be made at your respective training centre.You may utilize the facility.",0,'J');
//                //$bmname=$row['block_muni_cd'];
//                $pdf->Ln(1);
//                $bmname=($assembly=='')?"Block/Municipality: ".$ApptDetail->block_muni_name:"";
//                $pdf->SetFont('Arial','',9);
//                $pdf->Cell(190,5,$bmname,0,0,'C');
//                
//                $count++;
//            }
//            if($count==$per_page)
//            {
//                $per_page=$per_page+1;
//                if($count!=Count($ApptRecord))
//                {
//                  $pdf->AddPage();
//                }
//            } 
//               
//        }
//    
//        $pdf->Output();
//        exit;
//    }  
    //::::::::::::::::::::::: Second Master Roll(Party) PDF :::::::::::::::::::://
    public function getSecondMasterRollPDF(Request $request) {
        $this->validate($request, [
            'forZone' => 'required|alpha_num|min:4|max:4',
            'party_reserve' => 'required|alpha|min:1|max:1',
            'assembly' => 'nullable|alpha_num|min:3|max:3',
            'phase' => 'nullable|digits_between:1,1',
                ], [
            'forZone.required' => 'For Zone is required',
            'forZone.alpha_num' => 'For Zone must be an alpha numeric characters',
            'party_reserve.required' => 'Party/Reserve is required',
            'party_reserve.alpha' => 'Party/Reserve must be an alpha characters',
            'assembly.alpha_num' => 'Assembly must be an alpha numeric characters',
            'phase.digits_between' => 'Phase must be integer'
        ]);
       // dd($request->all());
        $forzone = $request->forZone;
        $party_reserve = $request->party_reserve;
        $assembly = $request->assembly;
       // dd($request->phase);
        $phase = $request->phase;
       // echo $phase;die;
        $assemblyname = tbl_assembly::where('assemblycd', '=', $assembly)
                ->value(DB::raw('assemblyname'));
      //  dd($assemblyname);
//        $ApptRecordAssembly= tbl_assembly::where('assemblycd','=', $assembly)
//                    ->select('assemblycd','assembly.assemblyname')
//                    ->get();
//       foreach($ApptRecordAssembly as $ApptRecordAssemblyName){
//           $assemblyname=$ApptRecordAssemblyName->assemblyname;
//       }
        $session_personnela = session()->get('personnela_ppds');
        $ApptRecord = tbl_personnela::join('assembly', 'assembly.assemblycd', '=', '' . $session_personnela . '.forassembly')
                ->where('forzone', '=', $forzone)
                ->where('phase', '=', $phase)
                ->where('booked', '=', $party_reserve)
                ->where('forassembly', '=', $assembly)
                ->distinct()
                ->select('groupid', 'forassembly', 'assembly.assemblyname')
                ->orderBy('groupid')
                ->get();
               // dd($ApptRecord);
        $subV = json_decode($ApptRecord);

        $pdf = app('FPDF');
        $pdf->AddPage('L', '', 'A4');
        $pdf->SetTitle('Master Roll Report');
        $count = 0;
        $per_page = 3;
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Cell(275, 5, 'MASTER ROLL (POLLING PERSONNEL)', 0, 0, 'C');
        $pdf->Ln(6);
        $assem = "ASSEMBLY : " . $assembly . " - " . $assemblyname;
        $pdf->SetFont('Arial', 'B', 8.5);
        $pdf->Cell(275, 4, $assem, 0, 0, 'C');
        $pdf->Ln(6);
        foreach ($subV as $subTe) {
            if ($count < $per_page) {
                $fill = false;

                $pdf->SetFont('Arial', 'B', 8);
                $pdf->Cell(275, 5, "Polling Party : " . $subTe->groupid, "LTR", 0, 'L');
                $pdf->Ln();

                $pdf->SetFillColor(255, 255, 255);
                //	$this->SetTextColor(0,0,0);
                $pdf->SetDrawColor(0, 0, 0);
                $pdf->SetLineWidth(.3);
                $pdf->SetFont('Arial', '', 6);

                $w = array(7, 206, 62);
                $session_personnela = session()->get('personnela_ppds');
                $ApptRecordPP = tbl_personnela::Leftjoin('assembly', 'assembly.assemblycd', '=', '' . $session_personnela . '.forassembly')
                        ->join('office', 'office.officecd', '=', '' . $session_personnela . '.officecd')
                        ->join('subdivision', 'subdivision.subdivisioncd', '=', 'office.subdivisioncd')
                        ->leftjoin('policestation', 'policestation.policestationcd', '=', 'office.policestn_cd')
                        ->join('district', 'district.districtcd', '=', 'office.districtcd')
                        ->join('personnel', 'personnel.personcd', '=', '' . $session_personnela . '.personcd')
                        // ->leftjoin('bank','bank.bank_cd','=','personnel.bank_cd')
//                      ->leftjoin('branch',function($join){
//                            $join->on('branch.branchcd', '=', ''.$session_personnela.'.branchcd');
//                            $join->on('branch.bank_cd', '=', ''.$session_personnela.'.bank_cd');
//                          })                       
                        ->join('poststat', 'poststat.post_stat', '=', '' . $session_personnela . '.poststat')
                        ->where('forzone', '=', $forzone)
                        ->where('phase', '=', $phase)
                        ->where('booked', '=', $party_reserve)
                        ->where('forassembly', '=', $assembly)
                        ->where('groupid', '=', $subTe->groupid)
                        ->distinct()
                        ->select('' . $session_personnela . '.personcd', 'personnel.officer_name', 'personnel.off_desg', 'office.office', 'office.address1', 'office.address2', 'office.postoffice', 'subdivision.subdivision', 'policestation.policestation', 'district.district', 'office.pin', 'office.officecd', '' . $session_personnela . '.poststat', 'personnel.mob_no')
//                       'bank.bank_name' ,'branch.branch_name','personnel.bank_acc_no','branch.ifsc_code'
                        ->orderBy('groupid')
                        ->get();
                        //dd($ApptRecordPP);
                $subVPP = json_decode($ApptRecordPP);
                foreach ($subVPP as $subVPA) {
                    $p_dtl = "NAME - " . $subVPA->officer_name . ", DESG. - " . $subVPA->off_desg . ", PIN - (" . $subVPA->personcd . "), OFFICE NAME - " . $subVPA->office;
                    $p_dtl1 = "ADDRESS - " . $subVPA->address1 . ", " . $subVPA->address2;
                    $p_dtl2 = ", P.O. - " . $subVPA->postoffice . ", Subdiv.-" . $subVPA->subdivision . ", Dist.-" . $subVPA->district . ", PIN - " . $subVPA->pin . ", OFFICE - " . $subVPA->officecd . ", Mob No - " . $subVPA->mob_no;
                    $amount = tbl_poststatorder::where('poststat', '=', $subVPA->poststat)
                            ->where('party_reserve', '=', $party_reserve)
                            ->value(DB::raw('amount'));
                    $amount1 = "Received Rs. " . $amount;
                    $pdf->Cell($w[0], 4.5, $subVPA->poststat, "LTR", 0, 'C', $fill);
                    $pdf->Cell($w[1], 4.5, $p_dtl, "LTR", 0, 'L', $fill);
                    $pdf->Cell($w[2], 4.5, $amount1, "LTR", 0, 'L', $fill);
                    $pdf->Ln(4);
                    $pdf->Cell($w[0], 4.3, "", "LR", 0, 'L', $fill);
                    $pdf->Cell($w[1], 4.3, $p_dtl1 . $p_dtl2, "LR", 0, 'L', $fill);
                    $pdf->Cell($w[2], 4.3, "", "LR", 0, 'L', $fill);
                    $pdf->Ln();
                    $pdf->Cell(array_sum($w), 0, '', 1, 0, 'L', $fill);
                    $pdf->Ln();
                }
                $fill = !$fill;
                $count++;
            }
            if ($count == $per_page) {
                $per_page = $per_page + 3;
                if ($count != Count($subV)) {
                    $pdf->AddPage('L', '', 'A4');
                    $pdf->SetFont('Arial', 'B', 10);
                    $pdf->Cell(275, 5, 'MASTER ROLL (POLLING PERSONNEL)', 0, 0, 'C');
                    $pdf->Ln(6);
                    $assem = "ASSEMBLY : " . $assembly . " - " . $assemblyname;
                    $pdf->SetFont('Arial', 'B', 8.5);
                    $pdf->Cell(275, 4, $assem, 0, 0, 'C');
                    $pdf->Ln(6);
                }
            }
        }
        $pdf->Output();
        exit;
    }

    //::::::::::::::::::::::: Second Master Roll Excel :::::::::::::::::::::://
    public function getSecondMasterRollEXCEL(Request $request) {
        $this->validate($request, [
            'forZone' => 'required|alpha_num|min:4|max:4',
            'party_reserve' => 'required|alpha|min:1|max:1',
            'assembly' => 'nullable|alpha_num|min:3|max:3',
            'phase' => 'nullable|digits_between:1,1',
                ], [
            'forZone.required' => 'For Zone is required',
            'forZone.alpha_num' => 'For Zone must be an alpha numeric characters',
            'party_reserve.required' => 'Party/Reserve is required',
            'party_reserve.alpha' => 'Party/Reserve must be an alpha characters',
            'assembly.alpha_num' => 'Assembly must be an alpha numeric characters',
            'phase.digits_between' => 'Phase must be integer'
           
        ]);
        $forzone = $request->forZone;
        $party_reserve = $request->party_reserve;
        $assembly = $request->assembly;
        $phase = $request->phase;

        $session_personnela = session()->get('personnela_ppds');
        $recordsTotal = tbl_personnela::join('personnel', 'personnel.personcd', '=', '' . $session_personnela . '.personcd')
                ->join('office', 'office.officecd', '=', '' . $session_personnela . '.officecd')
//                      ->join('poststat','poststat.post_stat','=',''.$session_personnela.'.poststat')
                ->select('' . $session_personnela . '.personcd', '' . $session_personnela . '.poststat', 'forassembly', 'personnel.officer_name', 'personnel.off_desg', 'office.office', 'groupid', 'bank_acc_no', 'branch_ifsc', 'personnel.acno', 'personnel.slno', 'personnel.partno')
                ->where('forzone', '=', $forzone)
                ->where('phase', '=', $phase)
                ->where('booked', '=', $party_reserve)
                ->where('forassembly', '=', $assembly)
                ->orderBy('groupid')
                ->get();
        //dd($recordsTotal);
        foreach ($recordsTotal as $recordsTo) {
            $amount = tbl_poststatorder::where('poststat', '=', $recordsTo->poststat)
                    ->where('party_reserve', '=', $party_reserve)
                    ->value(DB::raw('amount'));
            $nestedData1['Personcd'] = $recordsTo->personcd;
            $nestedData1['Poststat'] = $recordsTo->poststat;
            $nestedData1['Officer Name'] = $recordsTo->officer_name;
            $nestedData1['Office Name'] = $recordsTo->office;
            $nestedData1['Assembly'] = $recordsTo->forassembly;
            $nestedData1['Groupid'] = $recordsTo->groupid;
            $nestedData1['Bank A/C No'] = $recordsTo->bank_acc_no;
            $nestedData1['IFSC Code'] = $recordsTo->branch_ifsc;
            $nestedData1['Ac No'] = $recordsTo->acno;
            $nestedData1['Sl No'] = $recordsTo->slno;
            $nestedData1['Part No'] = $recordsTo->partno;
            $nestedData1['Amount'] = $amount;
            $data[] = $nestedData1;
        }

        //$data1=$nestedData1;       
        // array_push($data,$data1);
        date_default_timezone_set('Asia/Calcutta');
        $dt = date_create('now')->format('YmdHi');
        $reportName = 'MasterRollExcel' . $dt;
        return \Excel::create($reportName, function($excel) use ($data) {
                    $excel->sheet(' ', function($sheet) use ($data) {
                        $sheet->fromArray($data);
                    });
                })->download('xlsx');
    }

    //::::::::::::::::::::::: Second Scroll PDF :::::::::::::::::::://
    public function getSecondPartyScrollPDF(Request $request) {
        $this->validate($request, [
            'forZone' => 'required|alpha_num|min:4|max:4',
            'party_reserve' => 'required|alpha|min:1|max:1',
            'assembly' => 'nullable|alpha_num|min:3|max:3',
            'phase' => 'nullable|digits_between:1,1',
                ], [
            'forZone.required' => 'For Zone is required',
            'forZone.alpha_num' => 'For Zone must be an alpha numeric characters',
            'party_reserve.required' => 'Party/Reserve is required',
            'party_reserve.alpha' => 'Party/Reserve must be an alpha characters',
            'assembly.alpha_num' => 'Assembly must be an alpha numeric characters',
            'phase.digits_between' => 'Phase must be integer'
        ]);
        $forzone = $request->forZone;
        $party_reserve = $request->party_reserve;
        $assembly = $request->assembly;
        $phase = $request->phase;
//        $ApptRecordAssembly= tbl_assembly::where('assemblycd','=', $assembly)
//                    ->select('assemblycd','assembly.assemblyname')
//                    ->get();
//        foreach($ApptRecordAssembly as $ApptRecordAssemblyName){
//           $assemblyname=$ApptRecordAssemblyName->assemblyname;
//        }
        $assemblyname = tbl_assembly::where('assemblycd', '=', $assembly)
                ->value(DB::raw('assemblyname'));

        $session_personnela = session()->get('personnela_ppds');
        $ApptRecord = tbl_personnela::join('assembly', 'assembly.assemblycd', '=', '' . $session_personnela . '.forassembly')
                ->where('forzone', '=', $forzone)
                ->where('phase', '=', $phase)
                ->where('booked', '=', $party_reserve)
                ->where('forassembly', '=', $assembly)
                ->distinct()
                ->select('groupid', 'forassembly', 'assembly.assemblyname')
                ->orderBy('groupid')
                ->get();
                //dd($ApptRecord);
        $subV = json_decode($ApptRecord);

        $pdf = app('FPDF');
        $pdf->AddPage('L', '', 'A4');
        $pdf->SetTitle('Scroll Report');
        $count = 0;
        $per_page = 3;
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Cell(275, 5, 'SCROLL (POLLING PERSONNEL)', 0, 0, 'C');
        $pdf->Ln(6);
        $assem = "ASSEMBLY : " . $assembly . " - " . $assemblyname;
        $pdf->SetFont('Arial', 'B', 8.5);
        $pdf->Cell(275, 4, $assem, 0, 0, 'C');
        $pdf->Ln(6);
        foreach ($subV as $subTe) {
            if ($count < $per_page) {
                $fill = false;

                $pdf->SetFont('Arial', 'B', 8);
                $pdf->Cell(275, 5, "Polling Party : " . $subTe->groupid, "LTR", 0, 'L');
                $pdf->Ln();

                $pdf->SetFillColor(255, 255, 255);
                //	$this->SetTextColor(0,0,0);
                $pdf->SetDrawColor(0, 0, 0);
                $pdf->SetLineWidth(.3);
                $pdf->SetFont('Arial', '', 6);

                $w = array(12, 263);
                $session_personnela = session()->get('personnela_ppds');
                $ApptRecordPP = tbl_personnela::Leftjoin('assembly', 'assembly.assemblycd', '=', '' . $session_personnela . '.forassembly')
                        ->join('office', 'office.officecd', '=', '' . $session_personnela . '.officecd')
                        ->join('subdivision', 'subdivision.subdivisioncd', '=', 'office.subdivisioncd')
                        ->leftjoin('policestation', 'policestation.policestationcd', '=', 'office.policestn_cd')
                        ->join('district', 'district.districtcd', '=', 'office.districtcd')
                        ->join('personnel', 'personnel.personcd', '=', '' . $session_personnela . '.personcd')
                        // ->leftjoin('bank','bank.bank_cd','=','personnel.bank_cd')
//                      ->leftjoin('branch',function($join){
//                            $join->on('branch.branchcd', '=', ''.$session_personnela.'.branchcd');
//                            $join->on('branch.bank_cd', '=', ''.$session_personnela.'.bank_cd');
//                          })                       
                        ->join('poststat', 'poststat.post_stat', '=', '' . $session_personnela . '.poststat')
                        ->where('forzone', '=', $forzone)
                        ->where('phase', '=', $phase)
                        ->where('booked', '=', $party_reserve)
                        ->where('forassembly', '=', $assembly)
                        ->where('groupid', '=', $subTe->groupid)
                        ->distinct()
                        ->select('' . $session_personnela . '.personcd', 'personnel.officer_name', 'personnel.off_desg', 'office.office', 'office.address1', 'office.address2', 'office.postoffice', 'subdivision.subdivision', 'policestation.policestation', 'district.district', 'office.pin', 'office.officecd', '' . $session_personnela . '.poststat', 'personnel.mob_no')
//                       'bank.bank_name' ,'branch.branch_name','personnel.bank_acc_no','branch.ifsc_code'
                        ->orderBy('groupid')
                        ->get();
                       // dd($ApptRecordPP);
                $subVPP = json_decode($ApptRecordPP);
                foreach ($subVPP as $subVPA) {
                    $p_dtl = "NAME - " . $subVPA->officer_name . ", DESG. - " . $subVPA->off_desg . ", PIN - (" . $subVPA->personcd . "), OFFICE NAME - " . $subVPA->office;
                    $p_dtl1 = "ADDRESS - " . $subVPA->address1 . ", " . $subVPA->address2;
                    $p_dtl2 = ", P.O. - " . $subVPA->postoffice . ", Subdiv.-" . $subVPA->subdivision . ", Dist.-" . $subVPA->district . ", PIN - " . $subVPA->pin . ", OFFICE - " . $subVPA->officecd . ", Mob No - " . $subVPA->mob_no;

                    $amount1 = "Received Rs. ";
                    $pdf->Cell($w[0], 4.5, $subVPA->poststat, "LTR", 0, 'C', $fill);
                    $pdf->Cell($w[1], 4.5, $p_dtl, "LTR", 0, 'L', $fill);
                    //$pdf->Cell($w[2],4.5,$amount1,"LTR",0,'L',$fill);
                    $pdf->Ln(4);
                    $pdf->Cell($w[0], 4.3, "", "LR", 0, 'L', $fill);
                    $pdf->Cell($w[1], 4.3, $p_dtl1 . $p_dtl2, "LR", 0, 'L', $fill);
                    //$pdf->Cell($w[2],4.3,"","LR",0,'L',$fill);
                    $pdf->Ln();
                    $pdf->Cell(array_sum($w), 0, '', 1, 0, 'L', $fill);
                    $pdf->Ln();
                }
                $fill = !$fill;
                $count++;
            }
            if ($count == $per_page) {
                $per_page = $per_page + 3;
                if ($count != Count($subV)) {
                    $pdf->AddPage('L', '', 'A4');
                    $pdf->SetFont('Arial', 'B', 10);
                    $pdf->Cell(275, 5, 'SCROLL (POLLING PERSONNEL)', 0, 0, 'C');
                    $pdf->Ln(6);
                    $assem = "ASSEMBLY : " . $assembly . " - " . $assemblyname;
                    $pdf->SetFont('Arial', 'B', 8.5);
                    $pdf->Cell(275, 4, $assem, 0, 0, 'C');
                    $pdf->Ln(6);
                }
            }
        }
        $pdf->Output();
        exit;
    }

    //::::::::::::::::::::::: Second Party Venue PDF :::::::::::::::::::://
    public function getSecondPartyVenuePDF(Request $request) {
        $this->validate($request, [
            'forZone' => 'required|alpha_num|min:4|max:4',
            'party_reserve' => 'required|alpha|min:1|max:1',
            'assembly' => 'nullable|alpha_num|min:3|max:3',
            'phase' => 'nullable|digits_between:1,1',
                ], [
            'forZone.required' => 'For Zone is required',
            'forZone.alpha_num' => 'For Zone must be an alpha numeric characters',
            'party_reserve.required' => 'Party/Reserve is required',
            'party_reserve.alpha' => 'Party/Reserve must be an alpha characters',
            'assembly.alpha_num' => 'Assembly must be an alpha numeric characters',
            'phase.digits_between' => 'Phase must be integer' 
        ]);
        $forzone = $request->forZone;
        $phase = $request->phase;

        $party_reserve = $request->party_reserve;
        $assembly = $request->assembly;
        $subdivision = $request->subdivision;
        $venuename = $request->venuename;
        $trainingdatetime = $request->trainingdatetime;

        $assemblyname = tbl_assembly::where('assemblycd', '=', $assembly)
                ->value(DB::raw('assemblyname'));

//        $ApptRecordAssembly= tbl_assembly::where('assemblycd','=', $assembly)
//                    ->select('assemblycd','assembly.assemblyname')
//                    ->get();
//        foreach($ApptRecordAssembly as $ApptRecordAssemblyName){
//           $assemblyname=$ApptRecordAssemblyName->assemblyname;
//        }
        $session_personnela = session()->get('personnela_ppds');
        $ApptRecord = tbl_second_training_schedule::join('second_training_venue', 'second_training_venue.venue_cd', '=', 'second_training_schedule.tr_venue_cd')
                ->join('second_training_date_time', 'second_training_date_time.datetime_cd', '=', 'second_training_schedule.datetimecd')
                ->join('second_training_subvenue', 'second_training_subvenue.subvenue_cd', '=', 'second_training_schedule.tr_subvenue_cd')
                ->where('second_training_schedule.assembly', '=', $assembly)
                ->where('second_training_schedule.party_reserve', '=', $party_reserve)
                //->where(''.$session_personnela.'.officecd','=',$officeName)
                ->with('ppDetailGroupBrief')
                ->select('second_training_schedule.schedule_code', 'second_training_venue.venuename', 'second_training_venue.venueaddress', 'second_training_date_time.training_dt', 'second_training_date_time.training_time', 'second_training_subvenue.subvenue');
        if ($subdivision != '') {
            $ApptRecord = $ApptRecord->where('second_training_schedule.subdivision', '=', $subdivision);
        }
        if ($venuename != '') {
            $ApptRecord = $ApptRecord->where('second_training_schedule.tr_venue_cd', '=', $venuename);
        }
        if ($trainingdatetime != '') {
            $ApptRecord = $ApptRecord->where('second_training_schedule.datetimecd', '=', $trainingdatetime);
        }
        $ApptRecord = $ApptRecord->get();
        $subV = json_decode($ApptRecord);


        $pdf = app('FPDF');
        $pdf->AddPage('L', '', 'A4');
        $pdf->SetTitle('Venue wise Report');
        $count = 0;
        $per_page = 1;
//        $pdf->SetFont('Arial','B',10);
//        $pdf->Cell(275,5,'SCROLL (POLLING PERSONNEL)',0,0,'C');
//        $pdf->Ln(6);
        $assem = "ASSEMBLY : " . $assembly . " - " . $assemblyname;
        $pdf->SetFont('Arial', 'B', 8.5);
        $pdf->Cell(275, 4, $assem, 0, 0, 'C');
        $pdf->Ln(6);
        foreach ($subV as $subTe) {
            if ($count < $per_page) {
                $fill = false;
                $received_date = date('d/m/Y', strtotime(trim(str_replace('-', '/', $subTe->training_dt))));
                $venue = "VENUE : " . $subTe->venuename . ", " . $subTe->subvenue . ", " . $subTe->venueaddress;
                $venue1 = "on " . $received_date . " from " . $subTe->training_time;

//                $pdf->Ln();
                $pdf->SetFont('Arial', 'B', 8);
                $pdf->Cell(275, 5, $venue . $venue1, "LTR", 0, 'L');
                $pdf->Ln(5);


                $subTe_array = $subTe->pp_detail_group_brief;
                $subTe_array_count = count($subTe_array);
                if ($subTe_array_count > 0) {
                    $count1 = 0;
                    $per_page1 = 3;
                    foreach ($subTe_array as $stud_sub) {
                        if ($count1 < $per_page1) {
                            $pdf->SetFont('Arial', 'B', 8);
                            $pdf->Cell(275, 5, "Polling Party : " . $stud_sub->groupid, "LTR", 0, 'L');
                            $pdf->Ln();

                            $pdf->SetFillColor(255, 255, 255);
                            //	$this->SetTextColor(0,0,0);
                            $pdf->SetDrawColor(0, 0, 0);
                            $pdf->SetLineWidth(.3);
                            $pdf->SetFont('Arial', '', 6);

                            $w = array(12, 263);
                            $session_personnela = session()->get('personnela_ppds');
                            $ApptRecordPP = tbl_personnela::Leftjoin('assembly', 'assembly.assemblycd', '=', '' . $session_personnela . '.forassembly')
                                    ->join('office', 'office.officecd', '=', '' . $session_personnela . '.officecd')
                                    ->join('subdivision', 'subdivision.subdivisioncd', '=', 'office.subdivisioncd')
                                    ->leftjoin('policestation', 'policestation.policestationcd', '=', 'office.policestn_cd')
                                    ->join('district', 'district.districtcd', '=', 'office.districtcd')
                                    ->join('personnel', 'personnel.personcd', '=', '' . $session_personnela . '.personcd')
                                    // ->leftjoin('bank','bank.bank_cd','=','personnel.bank_cd')
                                    //                      ->leftjoin('branch',function($join){
                                    //                            $join->on('branch.branchcd', '=', ''.$session_personnela.'.branchcd');
                                    //                            $join->on('branch.bank_cd', '=', ''.$session_personnela.'.bank_cd');
                                    //                          })                       
                                    ->join('poststat', 'poststat.post_stat', '=', '' . $session_personnela . '.poststat')
                                    ->where('forzone', '=', $forzone)
                                    ->where('phase', '=', $phase)
                                    ->where('booked', '=', $party_reserve)
                                    ->where('forassembly', '=', $assembly)
                                    ->where('groupid', '=', $stud_sub->groupid)
                                    ->distinct()
                                    ->select('' . $session_personnela . '.personcd', 'personnel.officer_name', 'personnel.off_desg', 'office.office', 'office.address1', 'office.address2', 'office.postoffice', 'subdivision.subdivision', 'policestation.policestation', 'district.district', 'office.pin', 'office.officecd', '' . $session_personnela . '.poststat', 'personnel.mob_no')
                                    //                       'bank.bank_name' ,'branch.branch_name','personnel.bank_acc_no','branch.ifsc_code'
                                    ->orderBy('groupid')
                                    ->get();
                                  //  dd($ApptRecordPP);
                            $subVPP = json_decode($ApptRecordPP);
                            foreach ($subVPP as $subVPA) {
                                $p_dtl = "NAME - " . $subVPA->officer_name . ", DESG. - " . $subVPA->off_desg . ", PIN - (" . $subVPA->personcd . "), OFFICE NAME - " . $subVPA->office;
                                $p_dtl1 = "ADDRESS - " . $subVPA->address1 . ", " . $subVPA->address2;
                                $p_dtl2 = ", P.O. - " . $subVPA->postoffice . ", Subdiv.-" . $subVPA->subdivision . ", Dist.-" . $subVPA->district . ", PIN - " . $subVPA->pin . ", OFFICE - " . $subVPA->officecd . ", Mob No - " . $subVPA->mob_no;

                                $amount1 = "Received Rs. ";
                                $pdf->Cell($w[0], 4.5, $subVPA->poststat, "LTR", 0, 'C', $fill);
                                $pdf->Cell($w[1], 4.5, $p_dtl, "LTR", 0, 'L', $fill);
                                //$pdf->Cell($w[2],4.5,$amount1,"LTR",0,'L',$fill);
                                $pdf->Ln(4);
                                $pdf->Cell($w[0], 4.3, "", "LR", 0, 'L', $fill);
                                $pdf->Cell($w[1], 4.3, $p_dtl1 . $p_dtl2, "LR", 0, 'L', $fill);
                                //$pdf->Cell($w[2],4.3,"","LR",0,'L',$fill);
                                $pdf->Ln();
                                $pdf->Cell(array_sum($w), 0, '', 1, 0, 'L', $fill);
                                $pdf->Ln();
                            }
                            $count1++;
                        }
                        if ($count1 == $per_page1) {
                            $per_page1 = $per_page1 + 3;
                            if ($count1 != $subTe_array_count) {
                                $pdf->AddPage('L', '', 'A4');
                                $pdf->Ln();
                                $pdf->SetFont('Arial', 'B', 8);
                                $pdf->Cell(275, 5, $venue . $venue1, "LTR", 0, 'L');
                                $pdf->Ln(5);
                            }
                        }
                    }
                }

                $fill = !$fill;
                $count++;
            }
            if ($count == $per_page) {
                $per_page = $per_page + 1;
                if ($count != Count($subV)) {
                    $pdf->AddPage('L', '', 'A4');
//                    $pdf->SetFont('Arial','B',10);
//                    $pdf->Cell(275,5,'SCROLL (POLLING PERSONNEL)',0,0,'C');
//                    $pdf->Ln(6);
                    $assem = "ASSEMBLY : " . $assembly . " - " . $assemblyname;
                    $pdf->SetFont('Arial', 'B', 8.5);
                    $pdf->Cell(275, 4, $assem, 0, 0, 'C');
                    $pdf->Ln(6);
                }
            }
        }
        $pdf->Output();
        exit;
    }

    //::::::::::::::::::::::: Second Reserve Venue PDF :::::::::::::::::::://
    public function getSecondReserveVenuePDF(Request $request) {
        $this->validate($request, [
            'forZone' => 'required|alpha_num|min:4|max:4',
            'party_reserve' => 'required|alpha|min:1|max:1',
            'assembly' => 'nullable|alpha_num|min:3|max:3',
            'phase' => 'nullable|digits_between:1,1',
                ], [
            'forZone.required' => 'For Zone is required',
            'forZone.alpha_num' => 'For Zone must be an alpha numeric characters',
            'party_reserve.required' => 'Party/Reserve is required',
            'party_reserve.alpha' => 'Party/Reserve must be an alpha characters',
            'assembly.alpha_num' => 'Assembly must be an alpha numeric characters',
            'phase.digits_between' => 'Phase must be integer' 

        ]);
        $forzone = $request->forZone;
        $phase = $request->phase;
       // echo $phase;die;
        $party_reserve = $request->party_reserve;
        $assembly = $request->assembly;
        $subdivision = $request->subdivision;
        $venuename = $request->venuename;
        $trainingdatetime = $request->trainingdatetime;
        $assemblyname = tbl_assembly::where('assemblycd', '=', $assembly)
                ->value(DB::raw('assemblyname'));

//        $ApptRecordAssembly= tbl_assembly::where('assemblycd','=', $assembly)
//                    ->select('assemblycd','assembly.assemblyname')
//                    ->get();
//        foreach($ApptRecordAssembly as $ApptRecordAssemblyName){
//           $assemblyname=$ApptRecordAssemblyName->assemblyname;
//        }
        $session_personnela = session()->get('personnela_ppds');
        $ApptRecord = tbl_second_training_schedule::join('second_training_venue', 'second_training_venue.venue_cd', '=', 'second_training_schedule.tr_venue_cd')
                ->join('second_training_date_time', 'second_training_date_time.datetime_cd', '=', 'second_training_schedule.datetimecd')
                ->join('second_training_subvenue', 'second_training_subvenue.subvenue_cd', '=', 'second_training_schedule.tr_subvenue_cd')
                ->where('second_training_schedule.assembly', '=', $assembly)
                ->where('second_training_schedule.party_reserve', '=', $party_reserve)
                //->where(''.$session_personnela.'.officecd','=',$officeName)
                ->with('ppDetailGroupReserveBrief')
                ->select('second_training_schedule.schedule_code', 'second_training_venue.venuename', 'second_training_venue.venueaddress', 'second_training_date_time.training_dt', 'second_training_date_time.training_time', 'second_training_subvenue.subvenue');
        if ($subdivision != '') {
            $ApptRecord = $ApptRecord->where('second_training_schedule.subdivision', '=', $subdivision);
        }
        if ($venuename != '') {
            $ApptRecord = $ApptRecord->where('second_training_schedule.tr_venue_cd', '=', $venuename);
        }
        if ($trainingdatetime != '') {
            $ApptRecord = $ApptRecord->where('second_training_schedule.datetimecd', '=', $trainingdatetime);
        }
        $ApptRecord = $ApptRecord->get();
        $subV = json_decode($ApptRecord);
        //   print_r($subV);

        $pdf = app('FPDF');
        $pdf->AddPage('L', '', 'A4');
        $pdf->SetTitle('Venue wise Report(Reserve)');
        $count = 0;
        $per_page = 1;
//        $pdf->SetFont('Arial','B',10);
//        $pdf->Cell(275,5,'SCROLL (POLLING PERSONNEL)',0,0,'C');
//        $pdf->Ln(6);
        $assem = "ASSEMBLY : " . $assembly . " - " . $assemblyname;
        $pdf->SetFont('Arial', 'B', 8.5);
        $pdf->Cell(275, 4, $assem, 0, 0, 'C');
        $pdf->Ln(6);
        foreach ($subV as $subTe) {
            if ($count < $per_page) {
                $fill = false;
                $received_date = date('d/m/Y', strtotime(trim(str_replace('-', '/', $subTe->training_dt))));
                $venue = "VENUE : " . $subTe->venuename . ", " . $subTe->subvenue . ", " . $subTe->venueaddress;
                $venue1 = " on " . $received_date . " from " . $subTe->training_time;

//                $pdf->Ln();
                $pdf->SetFont('Arial', 'B', 8);
                $pdf->Cell(275, 5, $venue . $venue1, "LTR", 0, 'L');
                $pdf->Ln(5);


                $subTe_array = $subTe->pp_detail_group_reserve_brief;
                $subTe_array_count = count($subTe_array);
                if ($subTe_array_count > 0) {
                    $count1 = 0;
                    $per_page1 = 20;
                    foreach ($subTe_array as $stud_sub) {
                        if ($count1 < $per_page1) {
                            //  $pdf->SetFont('Arial','B',8);
                            //  $pdf->Cell(275,5,"Polling Party : ".$stud_sub->groupid,"LTR",0,'L');
                            //   $pdf->Ln();

                            $pdf->SetFillColor(255, 255, 255);
                            //	$this->SetTextColor(0,0,0);
                            $pdf->SetDrawColor(0, 0, 0);
                            $pdf->SetLineWidth(.3);
                            $pdf->SetFont('Arial', '', 6);

                            $w = array(9, 12, 254);
                            $session_personnela = session()->get('personnela_ppds');
                            $ApptRecordPP = tbl_personnela::Leftjoin('assembly', 'assembly.assemblycd', '=', '' . $session_personnela . '.forassembly')
                                    ->join('office', 'office.officecd', '=', '' . $session_personnela . '.officecd')
                                    ->join('subdivision', 'subdivision.subdivisioncd', '=', 'office.subdivisioncd')
                                    ->leftjoin('policestation', 'policestation.policestationcd', '=', 'office.policestn_cd')
                                    ->join('district', 'district.districtcd', '=', 'office.districtcd')
                                    ->join('personnel', 'personnel.personcd', '=', '' . $session_personnela . '.personcd')
                                    // ->leftjoin('bank','bank.bank_cd','=','personnel.bank_cd')
                                    //                      ->leftjoin('branch',function($join){
                                    //                            $join->on('branch.branchcd', '=', ''.$session_personnela.'.branchcd');
                                    //                            $join->on('branch.bank_cd', '=', ''.$session_personnela.'.bank_cd');
                                    //                          })                       
                                    ->join('poststat', 'poststat.post_stat', '=', '' . $session_personnela . '.poststat')
                                    ->where('forzone', '=', $forzone)
                                    ->where('phase', '=', $phase)
                                    ->where('booked', '=', $party_reserve)
                                    ->where('forassembly', '=', $assembly)
                                    ->where('groupid', '=', $stud_sub->groupid)
                                    ->where('' . $session_personnela . '.personcd', '=', $stud_sub->personcd)
                                    //->distinct()
                                    ->select('' . $session_personnela . '.personcd', 'personnel.officer_name', 'personnel.off_desg', 'office.office', 'office.address1', 'office.address2', 'office.postoffice', 'subdivision.subdivision', 'policestation.policestation', 'district.district', 'office.pin', 'office.officecd', '' . $session_personnela . '.poststat', 'personnel.mob_no')
                                    //                       'bank.bank_name' ,'branch.branch_name','personnel.bank_acc_no','branch.ifsc_code'
                                    ->orderBy('groupid')
                                    ->get();
                                   // dd($ApptRecordPP);
                            $subVPP = json_decode($ApptRecordPP);
                            $count1++;
                            foreach ($subVPP as $subVPA) {
                                $p_dtl = "NAME - " . $subVPA->officer_name . ", DESG. - " . $subVPA->off_desg . ", PIN - (" . $subVPA->personcd . "), OFFICE NAME - " . $subVPA->office;
                                $p_dtl1 = "ADDRESS - " . $subVPA->address1 . ", " . $subVPA->address2;
                                $p_dtl2 = ", P.O. - " . $subVPA->postoffice . ", Subdiv.-" . $subVPA->subdivision . ", Dist.-" . $subVPA->district . ", PIN - " . $subVPA->pin . ", OFFICE - " . $subVPA->officecd . ", Mob No - " . $subVPA->mob_no;

                                // $amount1="Received Rs. ";
                                $pdf->Cell($w[0], 4.5, $count1, "LTR", 0, 'C', $fill);
                                $pdf->Cell($w[1], 4.5, $subVPA->poststat, "LTR", 0, 'L', $fill);
                                $pdf->Cell($w[2], 4.5, $p_dtl, "LTR", 0, 'L', $fill);
                                $pdf->Ln(4);
                                $pdf->Cell($w[0], 4.3, "", "LR", 0, 'L', $fill);
                                $pdf->Cell($w[1], 4.3, "", "LR", 0, 'L', $fill);
                                $pdf->Cell($w[2], 4.3, $p_dtl1 . $p_dtl2, "LR", 0, 'L', $fill);
                                $pdf->Ln();
                                $pdf->Cell(array_sum($w), 0, '', 1, 0, 'L', $fill);
                                $pdf->Ln();
                            }
                        }
                        if ($count1 == $per_page1) {
                            $per_page1 = $per_page1 + 20;
                            if ($count1 != $subTe_array_count) {
                                $pdf->AddPage('L', '', 'A4');
                                $pdf->Ln();
                                $assem = "ASSEMBLY : " . $assembly . " - " . $assemblyname;
                                $pdf->SetFont('Arial', 'B', 8.5);
                                $pdf->Cell(275, 4, $assem, 0, 0, 'C');
                                $pdf->Ln(3);
                                $pdf->SetFont('Arial', 'B', 8);
                                $pdf->Cell(275, 5, $venue . $venue1, "LTR", 0, 'L');
                                $pdf->Ln(5);
                            }
                        }
                    }
                }

                $fill = !$fill;
                $count++;
            }
            if ($count == $per_page) {
                $per_page = $per_page + 1;
                if ($count != Count($subV)) {
                    $pdf->AddPage('L', '', 'A4');
//                    $pdf->SetFont('Arial','B',10);
//                    $pdf->Cell(275,5,'SCROLL (POLLING PERSONNEL)',0,0,'C');
//                    $pdf->Ln(6);
                    $assem = "ASSEMBLY : " . $assembly . " - " . $assemblyname;
                    $pdf->SetFont('Arial', 'B', 8.5);
                    $pdf->Cell(275, 4, $assem, 0, 0, 'C');
                    $pdf->Ln(6);
                }
            }
        }
        $pdf->Output();
        exit;
    }

    //::::::::::::::::::::::: Second Master Roll(Reserve) PDF :::::::::::::::::::://
    public function getSecondReserveMasterPDF(Request $request) {
        $this->validate($request, [
            'forZone' => 'required|alpha_num|min:4|max:4',
            'party_reserve' => 'required|alpha|min:1|max:1',
            'assembly' => 'nullable|alpha_num|min:3|max:3',
            'phase' => 'nullable|digits_between:1,1',
                ], [
            'forZone.required' => 'For Zone is required',
            'forZone.alpha_num' => 'For Zone must be an alpha numeric characters',
            'party_reserve.required' => 'Party/Reserve is required',
            'party_reserve.alpha' => 'Party/Reserve must be an alpha characters',
            'assembly.alpha_num' => 'Assembly must be an alpha numeric characters',
            'phase.digits_between' => 'Phase must be integer'
        ]);
        $forzone = $request->forZone;
        $party_reserve = $request->party_reserve;
        $assembly = $request->assembly;
        $phase = $request->phase;
        $assemblyname = tbl_assembly::where('assemblycd', '=', $assembly)
                ->value(DB::raw('assemblyname'));
//       foreach($ApptRecordAssembly as $ApptRecordAssemblyName){
//           $assemblyname=$ApptRecordAssemblyName->assemblyname;
//       }
        $session_personnela = session()->get('personnela_ppds');
        $ApptRecord = tbl_personnela::join('assembly', 'assembly.assemblycd', '=', '' . $session_personnela . '.forassembly')
                ->where('forzone', '=', $forzone)
                ->where('phase', '=', $phase)
                ->where('booked', '=', $party_reserve)
                ->where('forassembly', '=', $assembly)
                //->distinct()
                ->select('groupid', 'forassembly', 'assembly.assemblyname', 'personcd', 'poststat')
                ->orderBy('poststat')
                ->orderBy('groupid')
                ->get();
             //   dd($ApptRecord);
        $subV = json_decode($ApptRecord);

        $pdf = app('FPDF');
        $pdf->AddPage('L', '', 'A4');
        $pdf->SetTitle('Master Roll Report(Reserve)');
        $count = 0;
        $per_page = 20;
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Cell(275, 5, 'MASTER ROLL RESERVE (POLLING PERSONNEL)', 0, 0, 'C');
        $pdf->Ln(6);
        $assem = "ASSEMBLY : " . $assembly . " - " . $assemblyname;
        $pdf->SetFont('Arial', 'B', 8.5);
        $pdf->Cell(275, 4, $assem, 0, 0, 'C');
        $pdf->Ln(6);
        foreach ($subV as $subTe) {
            if ($count < $per_page) {
                $fill = false;

//                $pdf->SetFont('Arial','B',8);
//                $pdf->Cell(275,5,"Polling Party : ".$subTe->groupid,"LTR",0,'L');
//                $pdf->Ln();

                $pdf->SetFillColor(255, 255, 255);
                //	$this->SetTextColor(0,0,0);
                $pdf->SetDrawColor(0, 0, 0);
                $pdf->SetLineWidth(.3);
                $pdf->SetFont('Arial', '', 6);

                $w = array(9, 7, 200, 59);
                $session_personnela = session()->get('personnela_ppds');
                $ApptRecordPP = tbl_personnela::Leftjoin('assembly', 'assembly.assemblycd', '=', '' . $session_personnela . '.forassembly')
                        ->join('office', 'office.officecd', '=', '' . $session_personnela . '.officecd')
                        ->join('subdivision', 'subdivision.subdivisioncd', '=', 'office.subdivisioncd')
                        ->leftjoin('policestation', 'policestation.policestationcd', '=', 'office.policestn_cd')
                        ->join('district', 'district.districtcd', '=', 'office.districtcd')
                        ->join('personnel', 'personnel.personcd', '=', '' . $session_personnela . '.personcd')
                        // ->leftjoin('bank','bank.bank_cd','=','personnel.bank_cd')
//                      ->leftjoin('branch',function($join){
//                            $join->on('branch.branchcd', '=', ''.$session_personnela.'.branchcd');
//                            $join->on('branch.bank_cd', '=', ''.$session_personnela.'.bank_cd');
//                          })                       
                        ->join('poststat', 'poststat.post_stat', '=', '' . $session_personnela . '.poststat')
                        ->where('forzone', '=', $forzone)
                        ->where('phase', '=', $phase)
                        ->where('booked', '=', $party_reserve)
                        ->where('forassembly', '=', $assembly)
                        ->where('groupid', '=', $subTe->groupid)
                        ->where('' . $session_personnela . '.personcd', '=', $subTe->personcd)
                        //->distinct()
                        ->select('' . $session_personnela . '.personcd', '' . $session_personnela . '.groupid', 'personnel.officer_name', 'personnel.off_desg', 'office.office', 'office.address1', 'office.address2', 'office.postoffice', 'subdivision.subdivision', 'policestation.policestation', 'district.district', 'office.pin', 'office.officecd', '' . $session_personnela . '.poststat', 'personnel.mob_no')
//                       'bank.bank_name' ,'branch.branch_name','personnel.bank_acc_no','branch.ifsc_code'
                        ->orderBy('groupid')
                        ->get();
                       // dd($ApptRecordPP);
                $subVPP = json_decode($ApptRecordPP);
                $count++;
                foreach ($subVPP as $subVPA) {
                    $p_dtl = "NAME - " . $subVPA->officer_name . ", DESG. - " . $subVPA->off_desg . ", PIN - (" . $subVPA->personcd . "), OFFICE NAME - " . $subVPA->office;
                    $p_dtl1 = "ADDRESS - " . $subVPA->address1 . ", " . $subVPA->address2;
                    $p_dtl2 = ", P.O. - " . $subVPA->postoffice . ", Subdiv.-" . $subVPA->subdivision . ", Dist.-" . $subVPA->district . ", PIN - " . $subVPA->pin . ", OFFICE - " . $subVPA->officecd . ", Mob No - " . $subVPA->mob_no;

                    $amount = tbl_poststatorder::where('poststat', '=', $subVPA->poststat)
                            ->where('party_reserve', '=', $party_reserve)
                            ->value(DB::raw('amount'));
                    $amount1 = "Received Rs. " . $amount;
                    $pdf->Cell($w[0], 4.5, $subVPA->groupid, "LTR", 0, 'C', $fill);
                    $pdf->Cell($w[1], 4.5, $subVPA->poststat, "LTR", 0, 'C', $fill);
                    $pdf->Cell($w[2], 4.5, $p_dtl, "LTR", 0, 'L', $fill);
                    $pdf->Cell($w[3], 4.5, $amount1, "LTR", 0, 'L', $fill);
                    $pdf->Ln(4);
                    $pdf->Cell($w[0], 4.3, "", "LR", 0, 'L', $fill);
                    $pdf->Cell($w[1], 4.3, "", "LR", 0, 'L', $fill);
                    $pdf->Cell($w[2], 4.3, $p_dtl1 . $p_dtl2, "LR", 0, 'L', $fill);
                    $pdf->Cell($w[3], 4.3, "", "LR", 0, 'L', $fill);
                    $pdf->Ln();
                    $pdf->Cell(array_sum($w), 0, '', 1, 0, 'L', $fill);
                    $pdf->Ln();
                }
                $fill = !$fill;
            }
            if ($count == $per_page) {
                $per_page = $per_page + 20;
                if ($count != Count($subV)) {
                    $pdf->AddPage('L', '', 'A4');
                    $pdf->SetFont('Arial', 'B', 10);
                    $pdf->Cell(275, 5, 'MASTER ROLL RESERVE (POLLING PERSONNEL)', 0, 0, 'C');
                    $pdf->Ln(6);
                    $assem = "ASSEMBLY : " . $assembly . " - " . $assemblyname;
                    $pdf->SetFont('Arial', 'B', 8.5);
                    $pdf->Cell(275, 4, $assem, 0, 0, 'C');
                    $pdf->Ln(6);
                }
            }
        }
        $pdf->Output();
        exit;
    }

    //::::::::::::::::::::::: Second Master Roll(Reserve) PDF :::::::::::::::::::://
    public function getSecondReserveScrollPDF(Request $request) {
        $this->validate($request, [
            'forZone' => 'required|alpha_num|min:4|max:4',
            'party_reserve' => 'required|alpha|min:1|max:1',
            'assembly' => 'nullable|alpha_num|min:3|max:3',
            'phase' => 'nullable|digits_between:1,1',
                ], [
            'forZone.required' => 'For Zone is required',
            'forZone.alpha_num' => 'For Zone must be an alpha numeric characters',
            'party_reserve.required' => 'Party/Reserve is required',
            'party_reserve.alpha' => 'Party/Reserve must be an alpha characters',
            'assembly.alpha_num' => 'Assembly must be an alpha numeric characters',
            'phase.digits_between' => 'Phase must be integer'
        ]);
        $forzone = $request->forZone;
        $party_reserve = $request->party_reserve;
        $assembly = $request->assembly;
        $phase = $request->phase;
        $assemblyname = tbl_assembly::where('assemblycd', '=', $assembly)
                ->value(DB::raw('assemblyname'));
//       foreach($ApptRecordAssembly as $ApptRecordAssemblyName){
//           $assemblyname=$ApptRecordAssemblyName->assemblyname;
//       }
        $session_personnela = session()->get('personnela_ppds');
        $ApptRecord = tbl_personnela::join('assembly', 'assembly.assemblycd', '=', '' . $session_personnela . '.forassembly')
                ->where('forzone', '=', $forzone)
                ->where('phase', '=', $phase)
                ->where('booked', '=', $party_reserve)
                ->where('forassembly', '=', $assembly)
                //->distinct()
                ->select('groupid', 'forassembly', 'assembly.assemblyname', 'personcd', 'poststat')
                ->orderBy('poststat')
                ->orderBy('groupid')
                ->get();
               // dd($ApptRecord);
        $subV = json_decode($ApptRecord);

        $pdf = app('FPDF');
        $pdf->AddPage('L', '', 'A4');
        $pdf->SetTitle('Scroll Report(Reserve)');
        $count = 0;
        $per_page = 20;
        $pdf->SetFont('Arial', 'B', 10);
        $pdf->Cell(275, 5, 'SCROLL ROLL RESERVE (POLLING PERSONNEL)', 0, 0, 'C');
        $pdf->Ln(6);
        $assem = "ASSEMBLY : " . $assembly . " - " . $assemblyname;
        $pdf->SetFont('Arial', 'B', 8.5);
        $pdf->Cell(275, 4, $assem, 0, 0, 'C');
        $pdf->Ln(6);
        foreach ($subV as $subTe) {
            if ($count < $per_page) {
                $fill = false;

//                $pdf->SetFont('Arial','B',8);
//                $pdf->Cell(275,5,"Polling Party : ".$subTe->groupid,"LTR",0,'L');
//                $pdf->Ln();

                $pdf->SetFillColor(255, 255, 255);
                //	$this->SetTextColor(0,0,0);
                $pdf->SetDrawColor(0, 0, 0);
                $pdf->SetLineWidth(.3);
                $pdf->SetFont('Arial', '', 6);

                $w = array(11, 14, 250);
                $session_personnela = session()->get('personnela_ppds');
                $ApptRecordPP = tbl_personnela::Leftjoin('assembly', 'assembly.assemblycd', '=', '' . $session_personnela . '.forassembly')
                        ->join('office', 'office.officecd', '=', '' . $session_personnela . '.officecd')
                        ->join('subdivision', 'subdivision.subdivisioncd', '=', 'office.subdivisioncd')
                        ->leftjoin('policestation', 'policestation.policestationcd', '=', 'office.policestn_cd')
                        ->join('district', 'district.districtcd', '=', 'office.districtcd')
                        ->join('personnel', 'personnel.personcd', '=', '' . $session_personnela . '.personcd')
                        // ->leftjoin('bank','bank.bank_cd','=','personnel.bank_cd')
//                      ->leftjoin('branch',function($join){
//                            $join->on('branch.branchcd', '=', ''.$session_personnela.'.branchcd');
//                            $join->on('branch.bank_cd', '=', ''.$session_personnela.'.bank_cd');
//                          })                       
                        ->join('poststat', 'poststat.post_stat', '=', '' . $session_personnela . '.poststat')
                        ->where('forzone', '=', $forzone)
                        ->where('phase', '=', $phase)
                        ->where('booked', '=', $party_reserve)
                        ->where('forassembly', '=', $assembly)
                        ->where('groupid', '=', $subTe->groupid)
                        ->where('' . $session_personnela . '.personcd', '=', $subTe->personcd)
                        //->distinct()
                        ->select('' . $session_personnela . '.personcd', '' . $session_personnela . '.groupid', 'personnel.officer_name', 'personnel.off_desg', 'office.office', 'office.address1', 'office.address2', 'office.postoffice', 'subdivision.subdivision', 'policestation.policestation', 'district.district', 'office.pin', 'office.officecd', '' . $session_personnela . '.poststat', 'personnel.mob_no')
//                       'bank.bank_name' ,'branch.branch_name','personnel.bank_acc_no','branch.ifsc_code'
                        ->orderBy('groupid')
                        ->get();
                       // dd($ApptRecordPP);
                $subVPP = json_decode($ApptRecordPP);
                $count++;
                foreach ($subVPP as $subVPA) {
                    $p_dtl = "NAME - " . $subVPA->officer_name . ", DESG. - " . $subVPA->off_desg . ", PIN - (" . $subVPA->personcd . "), OFFICE NAME - " . $subVPA->office;
                    $p_dtl1 = "ADDRESS - " . $subVPA->address1 . ", " . $subVPA->address2;
                    $p_dtl2 = ", P.O. - " . $subVPA->postoffice . ", Subdiv.-" . $subVPA->subdivision . ", Dist.-" . $subVPA->district . ", PIN - " . $subVPA->pin . ", OFFICE - " . $subVPA->officecd . ", Mob No - " . $subVPA->mob_no;

//                    $amount=tbl_poststatorder::where('poststat','=', $subVPA->poststat)
//                                            ->where('party_reserve','=', $party_reserve)
//                                            ->value(DB::raw('amount'));
//                    $amount1="Received Rs. ".$amount;
                    $pdf->Cell($w[0], 4.5, $subVPA->groupid, "LTR", 0, 'C', $fill);
                    $pdf->Cell($w[1], 4.5, $subVPA->poststat, "LTR", 0, 'C', $fill);
                    $pdf->Cell($w[2], 4.5, $p_dtl, "LTR", 0, 'L', $fill);
                    $pdf->Ln(4);
                    $pdf->Cell($w[0], 4.3, "", "LR", 0, 'L', $fill);
                    $pdf->Cell($w[1], 4.3, "", "LR", 0, 'L', $fill);
                    $pdf->Cell($w[2], 4.3, $p_dtl1 . $p_dtl2, "LR", 0, 'L', $fill);
                    $pdf->Ln();
                    $pdf->Cell(array_sum($w), 0, '', 1, 0, 'L', $fill);
                    $pdf->Ln();
                }
                $fill = !$fill;
            }
            if ($count == $per_page) {
                $per_page = $per_page + 20;
                if ($count != Count($subV)) {
                    $pdf->AddPage('L', '', 'A4');
                    $pdf->SetFont('Arial', 'B', 10);
                    $pdf->Cell(275, 5, 'MASTER ROLL RESERVE (POLLING PERSONNEL)', 0, 0, 'C');
                    $pdf->Ln(6);
                    $assem = "ASSEMBLY : " . $assembly . " - " . $assemblyname;
                    $pdf->SetFont('Arial', 'B', 8.5);
                    $pdf->Cell(275, 4, $assem, 0, 0, 'C');
                    $pdf->Ln(6);
                }
            }
        }
        $pdf->Output();
        exit;
    }

    public function getBlockMuniForSecondAppt(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            $this->validate($request, [
                    // 'subdivision' => 'required|alpha_num|min:4|max:4'
                    ], [
                    //subdivision.required' => 'Subdivision is required',
                    //'subdivision.alpha_num' => 'Subdivision must be an alpha numeric'
            ]);
            try {
                //$subdivision=$request->subdivision;
                $tbl_block_muni = new tbl_block_muni();
                $list = '';
                $districtcd_ppds = session()->get('districtcd_ppds');
                $blockmuni = $tbl_block_muni->where(DB::raw("SUBSTRING(subdivisioncd,1,2)"), '=', $districtcd_ppds)->pluck('blockmuni', 'blockminicd')->all();

                $list .= "<option value>[Select]</option>";
                foreach ($blockmuni as $key => $value) {
                    $list .= "<option value='" . $key . "'>" . $value . "</option>";
                }
                $response = array(
                    'options' => $list,
                    'status' => 1);
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }

    public function getOrderNoDateForSecondAppointment(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            $this->validate($request, [
                'forDist' => 'required|alpha_num|min:2|max:2'
                    ], [
                'forDist.required' => 'District is required',
                'forDist.alpha_num' => 'District must be an alpha numeric'
            ]);
            try {
                $forDist = $request->forDist;
                $list = '';
                $trainingData = tbl_order_no_date::where('districtcd', '=', $forDist)
                                ->select('order_no', DB::raw("Date_Format(order_dt, '%d/%m/%Y') As tr_date"), 'order_no')->get();
                $trArray = json_decode($trainingData);
                $list .= "<option value>[Select]</option>";
                foreach ($trArray as $trDate) {
                    $list .= "<option value='" . $trDate->order_no . "'>" . $trDate->tr_date . " - " . $trDate->order_no . "</option>";
                }
                $response = array(
                    'options' => $list,
                    'status' => 1);
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }

    public function getSecondVenueEXCEL(Request $request) {
        $this->validate($request, [
            'forZone' => 'required|alpha_num|min:4|max:4',
            'party_reserve' => 'required|alpha|min:1|max:1',
            'assembly' => 'nullable|alpha_num|min:3|max:3',
            'phase' => 'nullable|digits_between:1,1',
                ], [
            'forZone.required' => 'For Zone is required',
            'forZone.alpha_num' => 'For Zone must be an alpha numeric characters',
            'party_reserve.required' => 'Party/Reserve is required',
            'party_reserve.alpha' => 'Party/Reserve must be an alpha characters',
            'assembly.alpha_num' => 'Assembly must be an alpha numeric characters',
            'phase.digits_between' => 'Phase must be integer'
        ]);
        $forzone = $request->forZone;
        //echo  
        $phase = $request->phase;
        $party_reserve = $request->party_reserve;
        //die;
        $assembly = $request->assembly;
        $subdivision = $request->subdivision;
        $venuename = $request->venuename;
        $trainingdatetime = $request->trainingdatetime;

        $assemblyname = tbl_assembly::where('assemblycd', '=', $assembly)
                ->value(DB::raw('assemblyname'));

//        $ApptRecordAssembly= tbl_assembly::where('assemblycd','=', $assembly)
//                    ->select('assemblycd','assembly.assemblyname')
//                    ->get();
//        foreach($ApptRecordAssembly as $ApptRecordAssemblyName){
//           $assemblyname=$ApptRecordAssemblyName->assemblyname;
//        }
        $session_personnela = session()->get('personnela_ppds');
        $ApptRecord = tbl_second_training_schedule::join('second_training_venue', 'second_training_venue.venue_cd', '=', 'second_training_schedule.tr_venue_cd')
                ->join('second_training_date_time', 'second_training_date_time.datetime_cd', '=', 'second_training_schedule.datetimecd')
                ->join('second_training_subvenue', 'second_training_subvenue.subvenue_cd', '=', 'second_training_schedule.tr_subvenue_cd')
                ->where('second_training_schedule.assembly', '=', $assembly)
                ->where('second_training_schedule.party_reserve', '=', $party_reserve)
                //->where(''.$session_personnela.'.officecd','=',$officeName)
                ->with('ppDetailGroupBrief')
                ->select('second_training_schedule.schedule_code', 'second_training_venue.venuename', 'second_training_venue.venueaddress', 'second_training_date_time.training_dt', 'second_training_date_time.training_time', 'second_training_subvenue.subvenue');
        if ($subdivision != '') {
            $ApptRecord = $ApptRecord->where('second_training_schedule.subdivision', '=', $subdivision);
        }
        if ($venuename != '') {
            $ApptRecord = $ApptRecord->where('second_training_schedule.tr_venue_cd', '=', $venuename);
        }
        if ($trainingdatetime != '') {
            $ApptRecord = $ApptRecord->where('second_training_schedule.datetimecd', '=', $trainingdatetime);
        }
        $ApptRecord = $ApptRecord->get();
        $subV = json_decode($ApptRecord);
        $data = array();
        foreach ($subV as $subTe) {
            $subTe_array = $subTe->pp_detail_group_brief;

            $subTe_array_count = count($subTe_array);
            if ($subTe_array_count > 0) {
                foreach ($subTe_array as $stud_sub) {

                    $session_personnela = session()->get('personnela_ppds');
                    $ApptRecordPP = tbl_personnela::Leftjoin('assembly', 'assembly.assemblycd', '=', '' . $session_personnela . '.forassembly')
                            ->join('office', 'office.officecd', '=', '' . $session_personnela . '.officecd')
                            ->join('subdivision', 'subdivision.subdivisioncd', '=', 'office.subdivisioncd')
                            ->leftjoin('policestation', 'policestation.policestationcd', '=', 'office.policestn_cd')
                            ->join('district', 'district.districtcd', '=', 'office.districtcd')
                            ->join('personnel', 'personnel.personcd', '=', '' . $session_personnela . '.personcd')
                            // ->leftjoin('bank','bank.bank_cd','=','personnel.bank_cd')
                            //                      ->leftjoin('branch',function($join){
                            //                            $join->on('branch.branchcd', '=', ''.$session_personnela.'.branchcd');
                            //                            $join->on('branch.bank_cd', '=', ''.$session_personnela.'.bank_cd');
                            //                          })                       
                            ->join('poststat', 'poststat.post_stat', '=', '' . $session_personnela . '.poststat')
                            ->where('forzone', '=', $forzone)
                            ->where('phase', '=', $phase)
                            ->where('booked', '=', $party_reserve)
                            ->where('forassembly', '=', $assembly)
                            ->where('groupid', '=', $stud_sub->groupid)
                            ->distinct()
                            ->select('' . $session_personnela . '.personcd', 'personnel.officer_name', 'personnel.off_desg', 'office.office', 'office.address1', 'office.address2', 'office.postoffice', 'subdivision.subdivision', 'policestation.policestation', 'district.district', 'office.pin', 'office.officecd', '' . $session_personnela . '.poststat'
                                    , 'personnel.acno', 'personnel.slno', 'personnel.partno','personnel.mob_no','personnel.bank_acc_no','personnel.branch_ifsc','personnel.epic')
                            //                       'bank.bank_name' ,'branch.branch_name','personnel.bank_acc_no','branch.ifsc_code'
                            ->orderBy('groupid')
                            ->get();
                           // dd($ApptRecordPP);
                    $subVPP = json_decode($ApptRecordPP);

                    foreach ($subVPP as $subVPA) {
                        $nestedData1['PIN'] = $subVPA->personcd;
                        $nestedData1['Officer Name'] = $subVPA->officer_name;
                        $nestedData1['Mobile No'] = $subVPA->mob_no;

                        $nestedData1['Office Designation'] = $subVPA->off_desg;
                        $nestedData1['Polling Party'] = $stud_sub->groupid;
                        $nestedData1['Office Name'] = $subVPA->office;
                        $nestedData1['Poststat'] = $subVPA->poststat;

                        $nestedData1['Venue Name'] = $subTe->venuename;
                        $nestedData1['Sub Venue Name'] = $subTe->subvenue;
                        $nestedData1['Venue Address'] = $subTe->venueaddress;
                        $nestedData1['Training Time'] = $subTe->training_dt . " - " . $subTe->training_time;
                        $nestedData1['Assembly'] = $assembly . " - " . $assemblyname;
                        $nestedData1['AC No'] = $subVPA->acno;
                        $nestedData1['Sl No'] = $subVPA->slno;
                        $nestedData1['Part No'] = $subVPA->partno;
                        $nestedData1['Bank Ac. No.'] = $subVPA->bank_acc_no;
                        $nestedData1['Ifsc'] = $subVPA->branch_ifsc;
                        // $nestedData1['Part No'] = $subVPA->partno;

//            $nestedData1['Bank A/C No'] = $recordsTo->bank_acc_no;
//            $nestedData1['IFSC Code'] = $recordsTo->branch_ifsc;
//            $nestedData1['Amount'] = $amount;
                        $data[] = $nestedData1;
                    }
                }
            }
        }

        //$data1=$nestedData1;       
        // array_push($data,$data1);
        date_default_timezone_set('Asia/Calcutta');
        $dt = date_create('now')->format('YmdHi');
        $reportName = 'VenueRollExcel' . $dt;
        return \Excel::create($reportName, function($excel) use ($data) {
                    $excel->sheet(' ', function($sheet) use ($data) {
                        $sheet->fromArray($data);
                    });
                })->download('xlsx');
    }

    public function getSecondVenueReserveEXCEL(Request $request) {
        $this->validate($request, [
            'forZone' => 'required|alpha_num|min:4|max:4',
            'party_reserve' => 'required|alpha|min:1|max:1',
            'assembly' => 'nullable|alpha_num|min:3|max:3',
            'phase' => 'nullable|digits_between:1,1',
                ], [
            'forZone.required' => 'For Zone is required',
            'forZone.alpha_num' => 'For Zone must be an alpha numeric characters',
            'party_reserve.required' => 'Party/Reserve is required',
            'party_reserve.alpha' => 'Party/Reserve must be an alpha characters',
            'assembly.alpha_num' => 'Assembly must be an alpha numeric characters',
            'phase.digits_between' => 'Phase must be integer'

        ]);
        $forzone = $request->forZone;
        $phase = $request->phase;
        $party_reserve = $request->party_reserve;
        $assembly = $request->assembly;
        $subdivision = $request->subdivision;
        $venuename = $request->venuename;
        $trainingdatetime = $request->trainingdatetime;
        $assemblyname = tbl_assembly::where('assemblycd', '=', $assembly)
                ->value(DB::raw('assemblyname'));

//        $ApptRecordAssembly= tbl_assembly::where('assemblycd','=', $assembly)
//                    ->select('assemblycd','assembly.assemblyname')
//                    ->get();
//        foreach($ApptRecordAssembly as $ApptRecordAssemblyName){
//           $assemblyname=$ApptRecordAssemblyName->assemblyname;
//        }
        $session_personnela = session()->get('personnela_ppds');
        $ApptRecord = tbl_second_training_schedule::join('second_training_venue', 'second_training_venue.venue_cd', '=', 'second_training_schedule.tr_venue_cd')
                ->join('second_training_date_time', 'second_training_date_time.datetime_cd', '=', 'second_training_schedule.datetimecd')
                ->join('second_training_subvenue', 'second_training_subvenue.subvenue_cd', '=', 'second_training_schedule.tr_subvenue_cd')
                ->where('second_training_schedule.assembly', '=', $assembly)
                ->where('second_training_schedule.party_reserve', '=', $party_reserve)
                //->where(''.$session_personnela.'.officecd','=',$officeName)
                ->with('ppDetailGroupReserveBriefExcel')
                ->select('second_training_schedule.schedule_code', 'second_training_venue.venuename', 'second_training_venue.venueaddress', 'second_training_date_time.training_dt', 'second_training_date_time.training_time', 'second_training_subvenue.subvenue');
        if ($subdivision != '') {
            $ApptRecord = $ApptRecord->where('second_training_schedule.subdivision', '=', $subdivision);
        }
        if ($venuename != '') {
            $ApptRecord = $ApptRecord->where('second_training_schedule.tr_venue_cd', '=', $venuename);
        }
        if ($trainingdatetime != '') {
            $ApptRecord = $ApptRecord->where('second_training_schedule.datetimecd', '=', $trainingdatetime);
        }
        $ApptRecord = $ApptRecord->get();
        $subV = json_decode($ApptRecord);
        // echo json_encode($subV);die;
        $data = array();
        foreach ($subV as $subTe) {
            $subTe_array = $subTe->pp_detail_group_reserve_brief_excel;

            $subTe_array_count = count($subTe_array);
            if ($subTe_array_count > 0) {
                foreach ($subTe_array as $stud_sub) {

                    $session_personnela = session()->get('personnela_ppds');
                    $ApptRecordPP = tbl_personnela::Leftjoin('assembly', 'assembly.assemblycd', '=', '' . $session_personnela . '.forassembly')
                            ->join('office', 'office.officecd', '=', '' . $session_personnela . '.officecd')
                            ->join('subdivision', 'subdivision.subdivisioncd', '=', 'office.subdivisioncd')
                            ->leftjoin('policestation', 'policestation.policestationcd', '=', 'office.policestn_cd')
                            ->join('district', 'district.districtcd', '=', 'office.districtcd')
                            ->join('personnel', 'personnel.personcd', '=', '' . $session_personnela . '.personcd')
                            // ->leftjoin('bank','bank.bank_cd','=','personnel.bank_cd')
                            //                      ->leftjoin('branch',function($join){
                            //                            $join->on('branch.branchcd', '=', ''.$session_personnela.'.branchcd');
                            //                            $join->on('branch.bank_cd', '=', ''.$session_personnela.'.bank_cd');
                            //                          })                       
                            ->join('poststat', 'poststat.post_stat', '=', '' . $session_personnela . '.poststat')
                            ->where('forzone', '=', $forzone)
                            ->where('phase', '=', $phase)
                            ->where('booked', '=', $party_reserve)
                            ->where('forassembly', '=', $assembly)
                            ->where('groupid', '=', $stud_sub->groupid)
                            ->where('' . $session_personnela . '.personcd', '=', $stud_sub->personcd)
                            //->distinct()
                            ->select('' . $session_personnela . '.personcd', 'personnel.officer_name', 'personnel.off_desg', 'office.office', 'office.address1', 'office.address2', 'office.postoffice', 'subdivision.subdivision', 'policestation.policestation', 'district.district', 'office.pin', 'office.officecd', '' . $session_personnela . '.poststat', 'personnel.acno', 'personnel.slno', 'personnel.partno','personnel.mob_no','personnel.bank_acc_no','personnel.branch_ifsc','personnel.epic')
                            //                       'bank.bank_name' ,'branch.branch_name','personnel.bank_acc_no','branch.ifsc_code'
                            ->orderBy('groupid')
                            ->get();
                    $subVPP = json_decode($ApptRecordPP);

                    foreach ($subVPP as $subVPA) {
                        $nestedData1['PIN'] = $subVPA->personcd;
                        $nestedData1['Officer Name'] = $subVPA->officer_name;

                        $nestedData1['Mobile No'] = $subVPA->mob_no;
                        $nestedData1['Office Designation'] = $subVPA->off_desg;
                        $nestedData1['Polling Party'] = $stud_sub->groupid;
                        $nestedData1['Office Name'] = $subVPA->office;
                        $nestedData1['Poststat'] = $subVPA->poststat;

                        $nestedData1['Venue Name'] = $subTe->venuename;
                        $nestedData1['Sub Venue Name'] = $subTe->subvenue;
                        $nestedData1['Venue Address'] = $subTe->venueaddress;
                        $nestedData1['Training Time'] = $subTe->training_dt . " - " . $subTe->training_time;
                        $nestedData1['Assembly'] = $assembly . " - " . $assemblyname;
                        $nestedData1['AC No'] = $subVPA->acno;
                        $nestedData1['Epic'] = $subVPA->epic;

                        $nestedData1['Sl No'] = $subVPA->slno;
                        $nestedData1['Part No'] = $subVPA->partno;
                        $nestedData1['Bank Ac. No.'] = $subVPA->bank_acc_no;
                        $nestedData1['Ifsc'] = $subVPA->branch_ifsc;

//            $nestedData1['Bank A/C No'] = $recordsTo->bank_acc_no;
//            $nestedData1['IFSC Code'] = $recordsTo->branch_ifsc;
//            $nestedData1['Amount'] = $amount;
                        $data[] = $nestedData1;
                    }
                }
            }
        }

        //$data1=$nestedData1;       
        // array_push($data,$data1);
        date_default_timezone_set('Asia/Calcutta');
        $dt = date_create('now')->format('YmdHi');
        $reportName = 'VenueRollReserveExcel' . $dt;
        return \Excel::create($reportName, function($excel) use ($data) {
                    $excel->sheet(' ', function($sheet) use ($data) {
                        $sheet->fromArray($data);
                    });
                })->download('xlsx');
    }

    public function getSecondPartyScrollRollEXCEL(Request $request) {
        $this->validate($request, [
            'forZone' => 'required|alpha_num|min:4|max:4',
            'party_reserve' => 'required|alpha|min:1|max:1',
            'assembly' => 'nullable|alpha_num|min:3|max:3',
            'phase' => 'nullable|digits_between:1,1',
                ], [
            'forZone.required' => 'For Zone is required',
            'forZone.alpha_num' => 'For Zone must be an alpha numeric characters',
            'party_reserve.required' => 'Party/Reserve is required',
            'party_reserve.alpha' => 'Party/Reserve must be an alpha characters',
            'assembly.alpha_num' => 'Assembly must be an alpha numeric characters',
            'phase.digits_between' => 'Phase must be integer'
        ]);
        $forzone = $request->forZone;
        $party_reserve = $request->party_reserve;
        $assembly = $request->assembly;
        $phase = $request->phase;


        $assemblyname = tbl_assembly::where('assemblycd', '=', $assembly)
                ->value(DB::raw('assemblyname'));

        $session_personnela = session()->get('personnela_ppds');
        $ApptRecord = tbl_personnela::join('assembly', 'assembly.assemblycd', '=', '' . $session_personnela . '.forassembly')
                ->where('forzone', '=', $forzone)
                ->where('phase', '=', $phase)
                ->where('booked', '=', $party_reserve)
                ->where('forassembly', '=', $assembly)
                ->distinct()
                ->select('groupid', 'forassembly', 'assembly.assemblyname')
                ->orderBy('groupid')
                ->get();
               // dd($ApptRecord);
        $subV = json_decode($ApptRecord);
        $data = array();

        foreach ($subV as $subTe) {
            $session_personnela = session()->get('personnela_ppds');
            $ApptRecordPP = tbl_personnela::Leftjoin('assembly', 'assembly.assemblycd', '=', '' . $session_personnela . '.forassembly')
                    ->join('office', 'office.officecd', '=', '' . $session_personnela . '.officecd')
                    ->join('subdivision', 'subdivision.subdivisioncd', '=', 'office.subdivisioncd')
                    ->leftjoin('policestation', 'policestation.policestationcd', '=', 'office.policestn_cd')
                    ->join('district', 'district.districtcd', '=', 'office.districtcd')
                    ->join('personnel', 'personnel.personcd', '=', '' . $session_personnela . '.personcd')
                    // ->leftjoin('bank','bank.bank_cd','=','personnel.bank_cd')
//                      ->leftjoin('branch',function($join){
//                            $join->on('branch.branchcd', '=', ''.$session_personnela.'.branchcd');
//                            $join->on('branch.bank_cd', '=', ''.$session_personnela.'.bank_cd');
//                          })                       
                    ->join('poststat', 'poststat.post_stat', '=', '' . $session_personnela . '.poststat')
                    ->where('forzone', '=', $forzone)
                    ->where('phase', '=', $phase)
                    ->where('booked', '=', $party_reserve)
                    ->where('forassembly', '=', $assembly)
                    ->where('groupid', '=', $subTe->groupid)
                    ->distinct()
                    ->select('' . $session_personnela . '.personcd', 'personnel.officer_name', 'personnel.off_desg', 'office.office', 'office.address1', 'office.address2', 'office.postoffice', 'subdivision.subdivision', 'policestation.policestation', 'district.district', 'office.pin', 'office.officecd', '' . $session_personnela . '.poststat', 'personnel.mob_no')
//                       'bank.bank_name' ,'branch.branch_name','personnel.bank_acc_no','branch.ifsc_code'
                    ->orderBy('groupid')
                    ->get();
                   // dd($ApptRecordPP);
            $subVPP = json_decode($ApptRecordPP);
            foreach ($subVPP as $subVPA) {
                $nestedData1['PIN'] = $subVPA->personcd;
                $nestedData1['Officer Name'] = $subVPA->officer_name;
                $nestedData1['Office Designation'] = $subVPA->off_desg;
                $nestedData1['Polling Party'] = $subTe->groupid;
                $nestedData1['Office Name'] = $subVPA->office;
                $nestedData1['Mob No'] = $subVPA->mob_no;
                $nestedData1['Poststat'] = $subVPA->poststat;
                $nestedData1['Office Address1'] = $subVPA->address1;
                $nestedData1['Office Address2'] = $subVPA->address2;
                $nestedData1['Post Office'] = $subVPA->postoffice;
                $nestedData1['Subdivision'] = $subVPA->subdivision;
                $nestedData1['District'] = $subVPA->district;
                $nestedData1['Pincode'] = $subVPA->pin;
                $nestedData1['Office Code'] = $subVPA->officecd;

                $data[] = $nestedData1;
            }
        }
         date_default_timezone_set('Asia/Calcutta');
        $dt = date_create('now')->format('YmdHi');
        $reportName = 'ScrollRollPartyExcel' . $dt;
        return \Excel::create($reportName, function($excel) use ($data) {
                    $excel->sheet(' ', function($sheet) use ($data) {
                        $sheet->fromArray($data);
                    });
                })->download('xlsx');
    }
    
    public function getSecondReserveScrollRollEXCEL(Request $request){
         $this->validate($request, [
            'forZone' => 'required|alpha_num|min:4|max:4',
            'party_reserve' => 'required|alpha|min:1|max:1',
            'assembly' => 'nullable|alpha_num|min:3|max:3',
            'phase' => 'nullable|digits_between:1,1',
                ], [
            'forZone.required' => 'For Zone is required',
            'forZone.alpha_num' => 'For Zone must be an alpha numeric characters',
            'party_reserve.required' => 'Party/Reserve is required',
            'party_reserve.alpha' => 'Party/Reserve must be an alpha characters',
            'assembly.alpha_num' => 'Assembly must be an alpha numeric characters',
            'phase.digits_between' => 'Phase must be integer'
        ]);
        $forzone = $request->forZone;
        $party_reserve = $request->party_reserve;
        $assembly = $request->assembly;
        $phase = $request->phase;
        
        $assemblyname = tbl_assembly::where('assemblycd', '=', $assembly)
                ->value(DB::raw('assemblyname'));
//       foreach($ApptRecordAssembly as $ApptRecordAssemblyName){
//           $assemblyname=$ApptRecordAssemblyName->assemblyname;
//       }
        $session_personnela = session()->get('personnela_ppds');
        $ApptRecord = tbl_personnela::join('assembly', 'assembly.assemblycd', '=', '' . $session_personnela . '.forassembly')
                ->where('forzone', '=', $forzone)
                ->where('phase', '=', $phase)
                ->where('booked', '=', $party_reserve)
                ->where('forassembly', '=', $assembly)
                //->distinct()
                ->select('groupid', 'forassembly', 'assembly.assemblyname', 'personcd', 'poststat')
                ->orderBy('poststat')
                ->orderBy('groupid')
                ->get();
               // dd($ApptRecord);
        $subV = json_decode($ApptRecord);
          foreach ($subV as $subTe) {
              $session_personnela = session()->get('personnela_ppds');
                $ApptRecordPP = tbl_personnela::Leftjoin('assembly', 'assembly.assemblycd', '=', '' . $session_personnela . '.forassembly')
                        ->join('office', 'office.officecd', '=', '' . $session_personnela . '.officecd')
                        ->join('subdivision', 'subdivision.subdivisioncd', '=', 'office.subdivisioncd')
                        ->leftjoin('policestation', 'policestation.policestationcd', '=', 'office.policestn_cd')
                        ->join('district', 'district.districtcd', '=', 'office.districtcd')
                        ->join('personnel', 'personnel.personcd', '=', '' . $session_personnela . '.personcd')
                        // ->leftjoin('bank','bank.bank_cd','=','personnel.bank_cd')
//                      ->leftjoin('branch',function($join){
//                            $join->on('branch.branchcd', '=', ''.$session_personnela.'.branchcd');
//                            $join->on('branch.bank_cd', '=', ''.$session_personnela.'.bank_cd');
//                          })                       
                        ->join('poststat', 'poststat.post_stat', '=', '' . $session_personnela . '.poststat')
                        ->where('forzone', '=', $forzone)
                        ->where('phase', '=', $phase)
                        ->where('booked', '=', $party_reserve)
                        ->where('forassembly', '=', $assembly)
                        ->where('groupid', '=', $subTe->groupid)
                        ->where('' . $session_personnela . '.personcd', '=', $subTe->personcd)
                        //->distinct()
                        ->select('' . $session_personnela . '.personcd', '' . $session_personnela . '.groupid', 'personnel.officer_name', 'personnel.off_desg', 'office.office', 'office.address1', 'office.address2', 'office.postoffice', 'subdivision.subdivision', 'policestation.policestation', 'district.district', 'office.pin', 'office.officecd', '' . $session_personnela . '.poststat', 'personnel.mob_no')
//                       'bank.bank_name' ,'branch.branch_name','personnel.bank_acc_no','branch.ifsc_code'
                        ->orderBy('groupid')
                        ->get();
                        //dd($ApptRecordPP);
                $subVPP = json_decode($ApptRecordPP);
                
                foreach ($subVPP as $subVPA) {
                     $nestedData1['PIN'] = $subVPA->personcd;
                $nestedData1['Officer Name'] = $subVPA->officer_name;
                $nestedData1['Office Designation'] = $subVPA->off_desg;
                $nestedData1['Polling Party'] = $subTe->groupid;
                $nestedData1['Office Name'] = $subVPA->office;
                $nestedData1['Mob No'] = $subVPA->mob_no;
                $nestedData1['Poststat'] = $subVPA->poststat;
                $nestedData1['Office Address1'] = $subVPA->address1;
                $nestedData1['Office Address2'] = $subVPA->address2;
                $nestedData1['Post Office'] = $subVPA->postoffice;
                $nestedData1['Subdivision'] = $subVPA->subdivision;
                $nestedData1['District'] = $subVPA->district;
                $nestedData1['Pincode'] = $subVPA->pin;
                $nestedData1['Office Code'] = $subVPA->officecd;

                $data[] = $nestedData1;
                }
          }
        
           date_default_timezone_set('Asia/Calcutta');
        $dt = date_create('now')->format('YmdHi');
        $reportName = 'ScrollRollReserveExcel' . $dt;
        return \Excel::create($reportName, function($excel) use ($data) {
                    $excel->sheet(' ', function($sheet) use ($data) {
                        $sheet->fromArray($data);
                    });
                })->download('xlsx');
    }

}
