DELIMITER $$
CREATE DEFINER=`ppds`@`%` PROCEDURE `decl_statement_oth_2`(IN `dist` CHAR(2), IN `ph` CHAR(1))
    NO SQL
    DETERMINISTIC
BEGIN

 set @var_dist=dist;
 set @a=0;
 set @phv=ph;
 
 
 set @table=concat('personnela',@var_dist); 
set @stmprsv=concat("UPDATE ",@table, " as P1 \n INNER JOIN (SELECT personcd ,phase FROM " , @table, " \n  WHERE assembly_temp <> ?  AND assembly_off <> ?\n  AND assembly_perm <> ?  AND poststat = ?                   AND forzone = ? AND booked = ' ' and selected=1 and phase=@phv  and gender=? ORDER BY priority,rand() \n   LIMIT 0,?) as P2 on P1.personcd = P2.personcd and P1.phase=P2.phase \n   SET forassembly = ?,   booked = 'R',\n    groupid = @a:=@a+1 ");
    
                        
  prepare stmtrsv_2 from @stmprsv;
  

END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`ppds`@`%` PROCEDURE `decl_statement_p2_2`(IN `dist` CHAR(2), IN `ph` CHAR(1))
    NO SQL
    DETERMINISTIC
BEGIN

 set @var_dist=dist;
 set @phv=ph;
 
 set @table=concat('personnela',@var_dist); 
 
 set @stmt_p2_n=concat("UPDATE ",@table," as P1 \n                            INNER JOIN\n                            (SELECT personcd ,phase  FROM " ,@table, " WHERE poststat='P2' and  forzone = ?           AND booked = ' ' and gender=? and  phase=@phv       and selected=1 AND assembly_temp <> ?\n AND assembly_off <> ? AND assembly_perm <> ?  ORDER BY priority, rand()  LIMIT 0,?) as P2  on P1.personcd = P2.personcd and P1.phase=P2.phase \n                             SET forassembly = ?, \n                             booked = 'P', groupid =   @a:=@a+1, no_of_member=?");
 prepare stmtp2_n_2 from @stmt_p2_n;
 
  

  
  set @stmtp2_n:=concat("UPDATE ",@table, "  SET forassembly = ?,     groupid = ?,    booked = 'P', no_of_member=? where personcd = ? and  phase=@phv        ");
prepare stmt_p2_u_2 from @stmtp2_n;

 set @qry8_n:=concat("SELECT personcd into @pcd   FROM ", @table , "    WHERE  poststat='P2' and forzone = ?  and   poststat = ?  AND booked = ' ' and selected=1  and phase=@phv        AND    assembly_temp <> ?  AND assembly_off <> ? AND assembly_perm <> ? and gender=? and  officecd NOT IN \n (SELECT officecd from ",@table, "   WHERE forassembly = ? AND groupid =?)          \nORDER BY priority, rand()   LIMIT 0,1");
  prepare stmt_p2_r_2 from @qry8_n ;



END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`ppds`@`%` PROCEDURE `decl_statement_p1_2`(IN `dist` CHAR(2), IN `ph` CHAR(1))
    NO SQL
    DETERMINISTIC
BEGIN
  set @var_dist=dist;
 set @phv=ph;
 set @table=concat('personnela',@var_dist); 
 
 set @stmt_p1_n=concat("UPDATE ",@table," as P1 \n                            INNER JOIN\n                            (SELECT personcd ,phase  FROM " ,@table, " WHERE poststat='P1' and  forzone = ?           AND booked = ' ' and gender=? and selected=1 and phase=@phv       AND assembly_temp <> ?\n AND assembly_off <> ? AND assembly_perm <> ?  ORDER BY priority, rand()  LIMIT 0,?) as P2  on P1.personcd = P2.personcd and P1.phase=P2.phase \n                             SET forassembly = ?, \n                             booked = 'P', groupid =   @a:=@a+1, no_of_member=?");
 prepare stmtp1_n_2 from @stmt_p1_n;
  


                  
set @stmtp1_n:=concat("UPDATE ",@table, "  SET forassembly = ?,     groupid = ?,    booked = 'P', no_of_member=?  where personcd = ? and phase=@phv ");
prepare stmt_p1_u_2 from @stmtp1_n;

 set @qry8_n:=concat("SELECT personcd into @pcd   FROM ", @table , "    WHERE  poststat='P1' and forzone = ?  and   poststat = ?  AND booked = ' '  and phase=@phv  and     selected=1 AND    assembly_temp <> ?  AND assembly_off <> ? AND assembly_perm <> ? and gender=? and  officecd NOT IN \n (SELECT officecd from ",@table, "   WHERE forassembly = ? AND groupid =?)          \nORDER BY priority, rand()   LIMIT 0,1");
  prepare stmt_p1_r_2 from @qry8_n ;

                 



END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`ppds`@`%` PROCEDURE `decl_statement_p3_2`(IN `dist` CHAR(2), IN `ph` CHAR(1))
    NO SQL
    DETERMINISTIC
BEGIN

 set @var_dist=dist;
 set @phv=ph;
 
 set @table=concat('personnela',@var_dist); 
 
  set @stmt_p3_n=concat("UPDATE ",@table," as P1 \n                            INNER JOIN\n                            (SELECT personcd ,phase  FROM " ,@table, " WHERE poststat='P3' and  forzone = ?           AND booked = ' ' and gender=? and selected=1 and phase=@phv AND assembly_temp <> ?\n AND assembly_off <> ? AND assembly_perm <> ?  ORDER BY priority, rand()  LIMIT 0,?) as P2  on P1.personcd = P2.personcd  and P1.phase=P2.phase \n                             SET forassembly = ?, \n                             booked = 'P', groupid =   @a:=@a+1, no_of_member=?");
 prepare stmtp3_n_2 from @stmt_p3_n;
  

   
  set @stmtp3_n:=concat("UPDATE ",@table, "  SET forassembly = ?,     groupid = ?,    booked = 'P', no_of_member=? where personcd = ? and phase=@phv ");
prepare stmt_p3_u_2 from @stmtp3_n;

 set @qry8_n:=concat("SELECT personcd into @pcd   FROM ", @table , "    WHERE  poststat='P3' and forzone = ?  and   poststat = ?  AND booked = ' ' and selected=1 and phase=@phv AND    assembly_temp <> ?  AND assembly_off <> ? AND assembly_perm <> ? and gender=? and  officecd NOT IN \n (SELECT officecd from ",@table, "   WHERE forassembly = ? AND groupid =?)          \nORDER BY priority,rand()   LIMIT 0,1");
  prepare stmt_p3_r_2 from @qry8_n ;

                             



END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`ppds`@`%` PROCEDURE `decl_statement_pr_2`(IN `dist` CHAR(2), IN `ph` CHAR(1))
    NO SQL
    DETERMINISTIC
BEGIN

set @var_dist=dist;
 
 set @table=concat('personnela',@var_dist); 
 
  

set @stmt_pr_n=concat("UPDATE ",@table," as P1 \n                            INNER JOIN\n                            (SELECT personcd , phase   FROM " ,@table, " WHERE poststat='PR' and  forzone = ?           AND booked = ' ' and gender=? and selected=1 and phase=@phv AND assembly_temp <> ?\n AND assembly_off <> ? AND assembly_perm <> ?  ORDER BY priority,rand()  LIMIT 0,?) as P2  on P1.personcd = P2.personcd and P1.phase=P2.phase  \n                             SET forassembly = ?, \n                             booked = 'P', groupid =   @a:=@a+1, no_of_member=?");
 prepare stmtpr_n_2 from @stmt_pr_n;
                  

             

set @qry8_ba:=concat("SELECT personcd into @pcd   FROM ", @table , "    WHERE  poststat='PR' and forzone = ?  and   poststat = ?  AND booked = ' ' and selected=1 and phase=@phv AND    assembly_temp <> ?  AND assembly_off <> ? AND assembly_perm <> ? and gender=?  \nORDER BY priority, rand()   LIMIT 0,1");
  prepare stmt_pr_r_ba_2 from @qry8_ba ;
  
  set @stmtp3_ba:=concat("UPDATE ",@table, "  SET forassembly = ?,     groupid = ?,    booked = 'P', no_of_member=?  where personcd = ? and phase=@phv ");
prepare stmt_pr_u_ba_2 from @stmtp3_ba;


END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`ppds`@`%` PROCEDURE `decl_statement_pa_2`(IN `dist` CHAR(2), IN `ph` CHAR(1))
    NO SQL
    DETERMINISTIC
BEGIN
  set @var_dist=dist;
  set @phv=ph;
 
 set @table=concat('personnela',@var_dist); 
  
 set @stmt_pa_n=concat("UPDATE ",@table," as P1 \n                            INNER JOIN\n                            (SELECT personcd ,phase   FROM " ,@table, " WHERE poststat='PA' and  forzone = ?           AND booked = ' ' and gender=? and phase=@phv   and selected=1 AND assembly_temp <> ?\n AND assembly_off <> ? AND assembly_perm <> ?  ORDER BY priority,rand()  LIMIT 0,?) as P2  on P1.personcd = P2.personcd and P1.phase=P2.phase \n                             SET forassembly = ?, \n                             booked = 'P', groupid =   @a:=@a+1, no_of_member=?");
 prepare stmtpa_n_2 from @stmt_pa_n;
 

 
                 
                 
  set @stmtp3_n:=concat("UPDATE ",@table, "  SET forassembly = ?,     groupid = ?,    booked = 'P', no_of_member=?  where personcd = ? and  phase=@phv   ");
prepare stmt_pa_u_2 from @stmtp3_n;

 set @qry8_n:=concat("SELECT personcd into @pcd   FROM ", @table , "    WHERE  poststat='PA' and forzone = ?  and   poststat = ?  AND booked = ' ' and selected=1 and phase=@phv        AND    assembly_temp <> ?  AND assembly_off <> ? AND assembly_perm <> ? and gender=? and  officecd NOT IN \n (SELECT officecd from ",@table, "   WHERE forassembly = ? AND groupid =?)          \nORDER BY priority,rand()   LIMIT 0,1");
  prepare stmt_pa_r_2 from @qry8_n ;

                 



END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`ppds`@`%` PROCEDURE `init_2`(IN `dist` CHAR(2), IN `zone` CHAR(4), IN `pc` CHAR(2))
    NO SQL
BEGIN

set @table=concat('personnela',dist);
set @d=dist;
set @z=zone;
set @pcsel=pc;

set AUTOCOMMIT  = 0;
set @stmDefpriority0=concat("update ", @table , " as pp set pp.priority=99 where fordistrict='",@d,"'");
prepare stmDefpriority0 from @stmDefpriority0;
execute stmDefpriority0;

set @stmDefpriority=concat("update ", @table , " as pp set pp.priority=8 where fordistrict='",@d,"'  and home_pc='", @pcsel,"'");
prepare stmDefpriority from @stmDefpriority;
execute stmDefpriority;











 
set @stmpriorityp=concat("update ", @table , " as pp inner join  (select districtcd,pc_priority,priority_no  from district_priority_pc where districtcd='",@d,"'  and pc='", @pcsel,"' ) as pp1 on pp.home_pc=pp1.pc_priority set pp.priority=pp1.priority_no+20 ");
prepare stmpriorityp from @stmpriorityp;
execute stmpriorityp;
deallocate prepare stmpriorityp;

set @stmpriority=concat("update ", @table , " as pp inner join  (select districtcd,ac_priority,priority_no  from district_priority_ac where districtcd='",@d,"'  and pc='", @pcsel,"' ) as pp1 on pp.assembly_off=pp1.ac_priority set pp.priority=pp1.priority_no+10 ");
 prepare stmpriority from @stmpriority;
 execute stmpriority;
deallocate prepare stmpriority;







set @stmti=concat("update ",@table , " set booked=' ' , groupid=0 ,forassembly='  ',dcrccd='  ' ,no_of_member= 0  where selected=1 and forzone= '",@z, "' and forassembly not in ( select assemblycd from assembly_zone where rand_status2='Y' and districtcd= '",@d,"' and zone= '",@z, "'  )  " );
prepare stmi from @stmti;

execute stmi;
deallocate prepare stmi;

COMMIT;
end$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`ppds`@`%` PROCEDURE `rand2`(IN `zone` CHAR(4), IN `dist` CHAR(2), IN `ph` CHAR(1))
    NO SQL
BEGIN

DECLARE flag_ass1 INT DEFAULT 0;
DECLARE flag_ass2 INT DEFAULT 0;
    DECLARE var_no_party INT DEFAULT 0 ;
    DECLARE var_prior int;
    DECLARE var_start_sl INT DEFAULT 0 ;
    DECLARE var_no_of_member INT DEFAULT 0 ;
	DECLARE single_assembly CHAR(3) DEFAULT "" ;
    DECLARE var_gender char(1) ;
    
    
    
    
    DECLARE assembly_cursor CURSOR FOR 
    	SELECT assembly_party.assemblycd ,assembly_party.no_of_member,assembly_party.gender,assembly_party.start_sl,assembly_party.no_party,assembly_party.priority
        FROM assembly_party 
        INNER JOIN assembly_zone  ON assembly_party.assemblycd = assembly_zone.assemblycd 
        WHERE assembly_zone.zone = zone and assembly_zone.districtcd = dist and assembly_zone.rand_status2='A' and  assembly_zone.phase_id=@phv
    ORDER BY assembly_party.priority DESC;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET flag_ass1 = 1;
 
  set @d=dist;
  set @phv=ph;

  
set @inl=1;
set @p2=0;
 call decl_statement_pr_2(@d,@phv);    
  call decl_statement_p2_2(@d,@phv);
 call decl_statement_p3_2(@d,@phv);
 call decl_statement_pa_2(@d,@phv);
 call decl_statement_p1_2(@d,@phv);


     OPEN assembly_cursor;
    
   
    loop_assembly: LOOP
    	FETCH assembly_cursor INTO single_assembly,var_no_of_member,var_gender,var_start_sl,var_no_party,var_prior;
        
		IF flag_ass1 = 1 THEN 
    		LEAVE loop_assembly;
    	END IF;
        	      
                SET @p0=var_no_of_member ; 
                SET @p1=single_assembly; 
                SET @p2=var_start_sl; 
                SET @p3=zone; 
                SET @p4=var_no_party; 
                set @p5=dist;
                set @p6=var_gender;
                
              set   @table=concat('personnela',dist);
                
               

 
   
    
    
   
    
  
   
  
  CALL `rand2_main_all`(@p0,@p1,@p2,@p3,@p4,@p5,@p6);
        
    
 
 
    
 
    
 
    
 
   
 
 
   
 
   
                     
    END LOOP loop_assembly;
   commit;
    call stmt_deallocate_p2_2(@p5);
    call stmt_deallocate_p3_2(@p5);
   call stmt_deallocate_p1_2(@p5);
   call stmt_deallocate_pr_2(@p5);
   call stmt_deallocate_pa_2(@p5);
    CLOSE assembly_cursor;
    
   
   


END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`ppds`@`%` PROCEDURE `rand2_ab`(IN `zone` CHAR(4), IN `dist` CHAR(2), IN `ph` CHAR(1))
    NO SQL
BEGIN

DECLARE flag_ass1 INT DEFAULT 0;
    DECLARE var_no_party INT DEFAULT 0 ;
    DECLARE var_start_sl INT DEFAULT 0 ;
    DECLARE var_no_of_member INT DEFAULT 0 ;
	DECLARE single_assembly CHAR(3) DEFAULT "" ;
    DECLARE var_gender char(1) ;
    declare memberno int;
    declare grpid int;
   set @phv=ph;
   set @d=dist;
   SET @zone = zone;
   set @table=concat('personnela',dist);
  
  set @deltmp=concat("delete from tmp_rand where forzone = '", @zone , "' and phase='",@phv,"'");
  prepare tmpdel from @deltmp;
  EXECUTE tmpdel;
  DEALLOCATE PREPARE tmpdel;
 
set @delstmt=concat("update ",@table," as pupd inner join  (SELECT distinct pr.forassembly fasm,pr.groupid gid,pr.no_of_member memno ,pr.gender gnd, pr.phase ph1 FROM ",@table, "  pr,",@table, " p1,",@table, " p2,",@table, " p3 WHERE  pr.forzone= '", @zone ,"' and pr.booked='P' and pr.forassembly = p1.forassembly and pr.groupid = p1.groupid
and p1.booked = 'P' and pr.poststat ='PR' and p1.poststat = 'P1' and p1.forassembly = p2.forassembly and p1.groupid = p2.groupid and p2.booked = 'P' and p2.poststat = 'P2'
and p2.forassembly = p3.forassembly and p2.groupid = p3.groupid and p3.booked = 'P' and p3.poststat = 'P3' and pr.phase=@phv and p1.phase=@phv and p2.phase=@phv and p3.phase=@phv  and (((pr.officecd = p1.officecd) or (pr.officecd = p2.officecd) or (pr.officecd = p3.officecd))
      or ((p2.officecd = p3.officecd) or (p3.officecd = p1.officecd))
       or (p2.officecd = p1.officecd)) order by pr.forassembly ) as pp on pupd.forassembly=pp.fasm and pp.gid=pupd.groupid and pupd.no_of_member=pp.memno and pupd.gender=pp.gnd and pupd.phase=pp.ph1 set pupd.booked=' ' , pupd.groupid=0,pupd.forassembly='  ',pupd.no_of_member=0");
 

 
 set @selstmt=concat("Insert into tmp_rand SELECT distinct pr.forzone fzone, pr.forassembly fasm,pr.groupid gid,pr.no_of_member memno ,pr.gender gnd ,pr.phase phv FROM ", @table , " pr,", @table , "  p1,", @table , " p2,", @table , " p3

WHERE  pr.booked='P' and pr.forassembly = p1.forassembly and pr.groupid = p1.groupid
and p1.booked = 'P' and pr.poststat ='PR' and p1.poststat = 'P1'
and p1.forassembly = p2.forassembly and p1.groupid = p2.groupid and p2.booked = 'P' and p2.poststat = 'P2'
and p2.forassembly = p3.forassembly and p2.groupid = p3.groupid and p3.booked = 'P' and p3.poststat = 'P3' and pr.phase=@phv and p1.phase=@phv and p2.phase=@phv and p3.phase=@phv  and (((pr.officecd = p1.officecd) or (pr.officecd = p2.officecd) or (pr.officecd = p3.officecd))
      or ((p2.officecd = p3.officecd) or (p3.officecd = p1.officecd))
       or (p2.officecd = p1.officecd)) order by pr.forassembly ");
       
           
  prepare stmtsel from @selstmt;
  prepare stmtdel from @delstmt;
  execute stmtsel;
  execute stmtdel;
 
  deallocate prepare stmtsel;
  deallocate prepare stmtdel;
  
 
 
 
  
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`ppds`@`%` PROCEDURE `rand2_ab_loop`(IN `zone` CHAR(4), IN `dist` CHAR(2), IN `ph` CHAR(1))
    NO SQL
BEGIN

  DECLARE flag_ass1 INT DEFAULT 0;
    DECLARE var_no_party INT DEFAULT 0 ;
    DECLARE var_start_sl INT DEFAULT 0 ;
    DECLARE var_no_of_member INT DEFAULT 0 ;
	DECLARE single_assembly CHAR(3) DEFAULT "" ;
    DECLARE var_gender char(1) ;
    declare memberno int;
    declare grpid int;
   DECLARE tempcur CURSOR FOR SELECT forassembly,groupid,no_of_member,gender from tmp_rand where forzone = zone;
   DECLARE CONTINUE HANDLER FOR NOT FOUND SET flag_ass1 = 1;
   
   SET @zone = zone;
   set @table=concat('personnela',dist);
   set @phv=ph;

     
   set @d=dist;
   
  call decl_statement_pr_2(@d,@phv);
  call decl_statement_p1_2(@d,@phv);
  call decl_statement_p2_2(@d,@phv);
  call decl_statement_p3_2(@d,@phv);
  call decl_statement_pa_2(@d,@phv);
     OPEN tempcur;
    loop_tempcur: LOOP
   
        
        FETCH tempcur INTO single_assembly,grpid,memberno,var_gender;
        
          
        IF flag_ass1 = 1 THEN           
    		LEAVE loop_tempcur;
    	END IF;
           SET @p0=memberno ; 
                SET @p1=single_assembly; 
                SET @p2=grpid; 
                SET @p3=zone; 
                set @p4=0;
                 set @p5=dist;
                set @p6=var_gender;
                set @p7=ph;
                
                      
             CALL `rand2_main_ab`(@p0,@p1,@p2,@p3,@p4,@p5,@p6,@p7);    
       
        
 end loop loop_tempcur;
  
 close tempcur;
 
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`ppds`@`%` PROCEDURE `rand2_init_asm`(IN `zone` CHAR(4), IN `dist` CHAR(2), IN `ph` CHAR(1))
    NO SQL
BEGIN

DECLARE flag_ass1 INT DEFAULT 0;
DECLARE flag_ass2 INT DEFAULT 0;
    DECLARE var_no_party INT DEFAULT 0 ;
    DECLARE var_prior int;
    DECLARE var_start_sl INT DEFAULT 0 ;
    DECLARE var_no_of_member INT DEFAULT 0 ;
	DECLARE single_assembly CHAR(3) DEFAULT "" ;
    DECLARE var_gender char(1) ;
    DECLARE cntsel int;
    
    
    
    
    DECLARE assembly_cursor CURSOR FOR 
    	SELECT assembly_party.assemblycd ,assembly_party.no_of_member,assembly_party.gender,assembly_party.start_sl,assembly_party.no_party,assembly_party.priority
        FROM assembly_party 
        INNER JOIN assembly_zone  ON assembly_party.assemblycd = assembly_zone.assemblycd 
        WHERE assembly_zone.zone = zone and assembly_zone.districtcd = dist and assembly_zone.phase_id=ph and assembly_zone.rand_status2='A' 
    ORDER BY assembly_party.priority DESC;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET flag_ass1 = 1;
  set @table=concat('personnela',dist);
  
  set @d=dist;
  set @zn=zone;
  set @cntpp=0;
  set @phv=ph;
set @stmcntpp=concat("SELECT count(*) into  @cntpp  FROM ", @table , "  where selected=1 and (booked=' ') and phase='",@phv,"' and forzone='",@zn,"'");

prepare stmDefcntpp from @stmcntpp;
 execute stmDefcntpp;
 set @sumpp=0;
 set @stmcntreq=concat("SELECT round(4*(sum(no_party)+sum(no_party)*0.2)) into @sumpp FROM `assembly_party` WHERE `assemblycd` in  (SELECT pp1.assemblycd FROM `assembly_district`  as pp1 inner join (select assembly_zone.assemblycd from assembly_zone where zone='",@zn,"' and phase_id='",@phv,"' ) as pp2 on pp1.assemblycd=pp2.assemblycd )");
 
 prepare stmDefcntreq from @stmcntreq;
 execute stmDefcntreq;
 
 
 

 

  
  

  
set @inl=1;
set @p2=0;

 call decl_statement_pr_2(@d,@phv);    
 call decl_statement_p2_2(@d,@phv); 
 call decl_statement_p3_2(@d,@phv); 
 call decl_statement_pa_2(@d,@phv); 
  call decl_statement_p1_2(@d,@phv); 

    OPEN assembly_cursor;
    set @j=0;
     loop_assembly: LOOP
    	FETCH assembly_cursor INTO single_assembly,var_no_of_member,var_gender,var_start_sl,var_no_party,var_prior;
   
        set @i=7-@j;
		IF flag_ass1 = 1 THEN 
         
    		LEAVE loop_assembly;
    	END IF;
        
  
        SET @asm_v=single_assembly;
     
       set @pcsel=pc;
     
  

    if (round(abs(@sumpp-@cntpp))<300) then   
       
 set @stmDefpriority10=concat("update ", @table , " as pp inner join  (select personcd, priority,phase from ",  @table , "  where phase='",@phv,"' and (assembly_off='",@asm_v,"' or assembly_temp='",@asm_v,"' or assembly_perm='",@asm_v,"' )  ) as pp1 on pp.personcd=pp1.personcd and pp1.phase=pp.phase set pp.priority=",@i,"");
ELSE
set @stmDefpriority10=concat("update ", @table , " as pp inner join  (select personcd, priority  from ",  @table , "  where (assembly_off='",@asm_v,"' or assembly_temp='",@asm_v,"' or assembly_perm='",@asm_v,"' ) and home_pc='", @pcsel,"' ) as pp1 on pp.personcd=pp1.personcd set pp.priority=",@i,"");

end if;

 
 prepare stmDefpriority10 from @stmDefpriority10;
 execute stmDefpriority10;
        
        set @j=@j+1;
        
        End loop loop_assembly;
        commit;
    close assembly_cursor;
    
   
    call stmt_deallocate_p2_2(@p5);
    call stmt_deallocate_p3_2(@p5);
   call stmt_deallocate_p1_2(@p5);
   call stmt_deallocate_pr_2(@p5);
   call stmt_deallocate_pa_2(@p5);
   
   
   


END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`ppds`@`%` PROCEDURE `rand2_mainP1`(IN `var_no_of_member` INT, IN `single_assembly` CHAR(3), IN `var_start_sl` INT, IN `zone` CHAR(4), IN `var_no_party` INT, IN `var_dist` CHAR(2), IN `var_gender` CHAR(1))
    NO SQL
BEGIN

 
DECLARE flag_member INT DEFAULT 0;
Declare  a1  INT default 0;
Declare  cnt1  INT default 0;
Declare  cnt2  INT default 0;
Declare  cnt3  INT default 0;
Declare  cnt4  INT default 0;
declare pcd character(11) default ' ' ;
declare found int default 0;
 DECLARE single_poststat CHAR(2) DEFAULT '';

 set @table=concat('personnela',var_dist);
              
     set single_poststat='P1';
     SET @a = var_start_sl;
     set @asm=single_assembly;
     set @zn=zone;
     set @pst=single_poststat;
     set @gend=var_gender;
     set @noofmember=var_no_of_member;
     SET a1 = var_start_sl+1;                           
     
              

    
 execute stmtc11_2 using @gend;
execute stmtc12_2 using @gend;

execute stmtc13_2 using  @gend;
execute stmtc14_2 using @gend;




    
  if (@cnt1> 0 ) then
  
 
 
 
      while (a1<=(var_no_party+var_start_sl)) do 
      
 SET @a1 = a1;
 set @zone=zone;
 set @single_assembly=single_assembly;
 set @single_poststat=single_poststat;
 
  set @gend=var_gender; 

execute stmtr11_2 using @zone,@single_poststat,@single_assembly,@single_assembly,@single_assembly,@gend,@single_assembly,@a1;


   if length(trim(@pcd))=11 then

set @asm=single_assembly;
set @gid=a1;
set @pcode=@pcd;
set @noofmember=var_no_of_member;
execute stmtu11_2 using @asm,@gid,@noofmember,@pcode;


    set found=0;
    set @pcd=" ";
    ELSE
    set @pcd="  ";
   set  found=1;
   end if;
if (@cnt2>0 and found=1) THEN

execute stmtr12_2 using  @zone,@single_poststat,@single_assembly,@single_assembly,@single_assembly,@gend,@single_assembly,@a1;

   if length(trim(@pcd))=11 then

set @asm=single_assembly;
set @gid=a1;
set @pcode=@pcd;
set @noofmember=var_no_of_member;
 execute stmtu12_2 using @asm,@gid,@noofmember,@pcode;
    set found=0;
    set @pcd=" ";
    ELSE
    set @pcd="  ";
   set  found=1;
   end if;
end if ;
if (@cnt3>0 and found=1) THEN

execute stmtr13_2 using @zone,@single_poststat,@single_assembly,@single_assembly,@single_assembly,@gend,@single_assembly,@a1;

   if length(trim(@pcd))=11 then

set @asm=single_assembly;
set @gid=a1;

set @pcode=@pcd;
set @noofmember=var_no_of_member;
 execute stmtu13_2 using @asm,@gid,@noofmember,@pcode;
    set found=0;
    set @pcd=" ";
    ELSE
    set @pcd="  ";
   set  found=1;
   end if;
end if;
if (@cnt4>0 and found=1) THEN

execute stmtr14_2 using @zone,@single_poststat,@single_assembly,@single_assembly,@single_assembly,@gend,@single_assembly,@a1;

   if length(trim(@pcd))=11 then

set @asm=single_assembly;
set @gid=a1;
set @pcode=@pcd;
set @noofmember=var_no_of_member;
 execute stmtu14_2 using @asm,@gid,@noofmember,@pcode;
    set found=0;
    set @pcd=" ";
    ELSE
    set @pcd="  ";
   set  found=1;
   end if;
    end if;
   
  set a1=a1+1;
  END WHILE;

end if;


   
 if (@cnt1=0 and @cnt2>0) THEN
 
      while (a1<=(var_no_party+var_start_sl)) do 
 SET @a1 = a1;
 set @zone=zone;
 set @single_assembly=single_assembly;
 set @single_poststat=single_poststat;
 
  set @gend=var_gender; 

 execute stmtr12_2 using @zone,@single_poststat,@single_assembly,@single_assembly,@single_assembly,@gend,@single_assembly,@a1;
 
 if length(trim(@pcd))=11 then

set @asm=single_assembly;
set @gid=a1;
set @pcode=@pcd;
set @noofmember=var_no_of_member;
 execute stmtu12_2 using @asm,@gid,@noofmember,@pcode;
    set found=0;
    set @pcd=" ";
    ELSE
    set @pcd="  ";
   set  found=1;
   end if;
   
if (@cnt3>0 and found=1) THEN

execute stmtr13_2 using @zone,@single_poststat,@single_assembly,@single_assembly,@single_assembly,@gend,@single_assembly,@a1;


   if length(trim(@pcd))=11 then

set @asm=single_assembly;
set @gid=a1;

set @pcode=@pcd;
set @noofmember=var_no_of_member;
 execute stmtu13_2 using @asm,@gid,@noofmember,@pcode;
    set found=0;
    set @pcd=" ";
    ELSE
    set @pcd="  ";
   set  found=1;
   end if;
   end if;
if (@cnt4>0 and found=1) THEN

execute stmtr14_2 using @zone,@single_poststat,@single_assembly,@single_assembly,@single_assembly,@gend,@single_assembly,@a1;

   if length(trim(@pcd))=11 then

set @asm=single_assembly;
set @gid=a1;
set @pcode=@pcd;
set @noofmember=var_no_of_member;
 execute stmtu14_2 using @asm,@gid,@noofmember,@pcode;
    set found=0;
      set @pcd=" ";
    ELSE
    set @pcd="  ";
   set  found=1;
   end if;
    end if;
   
  set a1=a1+1;
  END WHILE;
end if;




   
 if (@cnt1=0 and @cnt2=0 and @cnt3>0) THEN
 
      while (a1<=(var_no_party+var_start_sl)) do 
 SET @a1 = a1;
 set @zone=zone;
 set @single_assembly=single_assembly;
 set @single_poststat=single_poststat;
 
  set @gend=var_gender; 


execute stmtr13_2 using @zone,@single_poststat,@single_assembly,@single_assembly,@single_assembly,@gend,@single_assembly,@a1;

   if length(trim(@pcd))=11 then

set @asm=single_assembly;
set @gid=a1;

set @pcode=@pcd;
set @noofmember=var_no_of_member;
 execute stmtu13_2 using @asm,@gid,@noofmember,@pcode;
    set found=0;
    set @pcd=" ";
    ELSE
    set @pcd="  ";
   set  found=1;
   end if;
if (@cnt4>0 and found=1) THEN

execute stmtr14_2 using @zone,@single_poststat,@single_assembly,@single_assembly,@single_assembly,@gend,@single_assembly,@a1;
   if length(trim(@pcd))=11 then

set @asm=single_assembly;
set @gid=a1;
set @pcode=@pcd;
set @noofmember=var_no_of_member;

 execute stmtu14_2 using @asm,@gid,@noofmember,@pcode;
    set found=0;
    set @pcd=" ";
    ELSE
    set @pcd="  ";
   set  found=1;
   end if;
    end if;
   
  set a1=a1+1;
  END WHILE;
  
  end if;
  
  



  
 if (@cnt1=0 and @cnt2=0 and @cnt3=0 and @cnt4>0) THEN
 
 myloop:     while (a1<=(var_no_party+var_start_sl)) do 
 
SET @a1 = a1;
 set @zone=zone;
 set @single_assembly=single_assembly;
 set @single_poststat=single_poststat;
 
  set @gend=var_gender; 

execute stmtr14_2 using @zone,@single_poststat,@single_assembly,@single_assembly,@single_assembly,@gend,@single_assembly,@a1;

   if length(trim(@pcd))=11 then

set @asm=single_assembly;
set @gid=a1;
set @pcode=@pcd;
set @noofmember=var_no_of_member;

 execute stmtu14_2 using @asm,@gid,@noofmember,@pcode;
    set found=0;
    set @pcd=" ";
    ELSE
    set @pcd="  ";
   set  found=1;
 
   leave myloop;
   end if;
    
   
  set a1=a1+1;
  END WHILE myloop;
end if;


 
    
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`ppds`@`%` PROCEDURE `rand2_mainP2`(IN `var_no_of_member` INT, IN `single_assembly` CHAR(3), IN `var_start_sl` INT, IN `zone` CHAR(4), IN `var_no_party` INT, IN `var_dist` CHAR(2), IN `var_gender` CHAR(1))
    NO SQL
BEGIN

 
DECLARE flag_member INT DEFAULT 0;
Declare  a1  INT default 0;
Declare  cnt1  INT default 0;
Declare  cnt2  INT default 0;
Declare  cnt3  INT default 0;
Declare  cnt4  INT default 0;
declare pcd character(11) default ' ' ;
declare found int default 0;
 DECLARE single_poststat CHAR(2) DEFAULT '';

 set @table=concat('personnela',var_dist);
              
     set single_poststat='P2';
     SET @a = var_start_sl;
     set @asm=single_assembly;
     set @zn=zone;
     set @pst=single_poststat;
     set @gend=var_gender;
     set @noofmember=var_no_of_member;
     SET a1 = var_start_sl+1;                           
     
              

    
 execute stmtc21_2 using @gend;
execute stmtc22_2 using @gend;

execute stmtc23_2 using  @gend;
execute stmtc24_2 using @gend;




    
  if (@cnt1> 0 ) then
  
 
 
 
      while (a1<=(var_no_party+var_start_sl)) do 
      
 SET @a1 = a1;
 set @zone=zone;
 set @single_assembly=single_assembly;
 set @single_poststat=single_poststat;
 
  set @gend=var_gender; 

execute stmtr21_2 using @zone,@single_poststat,@single_assembly,@single_assembly,@single_assembly,@gend,@single_assembly,@a1;


   if length(trim(@pcd))=11 then

set @asm=single_assembly;
set @gid=a1;
set @pcode=@pcd;
set @noofmember=var_no_of_member;
execute stmtu21_2 using @asm,@gid,@noofmember,@pcode;


    set found=0;
    set @pcd=" ";
    ELSE
    set @pcd="  ";
   set  found=1;
   end if;
if (@cnt2>0 and found=1) THEN

execute stmtr22_2 using  @zone,@single_poststat,@single_assembly,@single_assembly,@single_assembly,@gend,@single_assembly,@a1;

   if length(trim(@pcd))=11 then

set @asm=single_assembly;
set @gid=a1;
set @pcode=@pcd;
set @noofmember=var_no_of_member;
 execute stmtu22_2 using @asm,@gid,@noofmember,@pcode;
    set found=0;
    set @pcd=" ";
    ELSE
    set @pcd="  ";
   set  found=1;
   end if;
end if ;
if (@cnt3>0 and found=1) THEN

execute stmtr23_2 using @zone,@single_poststat,@single_assembly,@single_assembly,@single_assembly,@gend,@single_assembly,@a1;

   if length(trim(@pcd))=11 then

set @asm=single_assembly;
set @gid=a1;

set @pcode=@pcd;
set @noofmember=var_no_of_member;
 execute stmtu23_2 using @asm,@gid,@noofmember,@pcode;
    set found=0;
    set @pcd=" ";
    ELSE
    set @pcd="  ";
   set  found=1;
   end if;
end if;
if (@cnt4>0 and found=1) THEN

execute stmtr24_2 using @zone,@single_poststat,@single_assembly,@single_assembly,@single_assembly,@gend,@single_assembly,@a1;

   if length(trim(@pcd))=11 then

set @asm=single_assembly;
set @gid=a1;
set @pcode=@pcd;
set @noofmember=var_no_of_member;
 execute stmtu24_2 using @asm,@gid,@noofmember,@pcode;
    set found=0;
    set @pcd=" ";
    ELSE
    set @pcd="  ";
   set  found=1;
   end if;
    end if;
   
  set a1=a1+1;
  END WHILE;

end if;


   
 if (@cnt1=0 and @cnt2>0) THEN
 
      while (a1<=(var_no_party+var_start_sl)) do 
 SET @a1 = a1;
 set @zone=zone;
 set @single_assembly=single_assembly;
 set @single_poststat=single_poststat;
 
  set @gend=var_gender; 

 execute stmtr22_2 using @zone,@single_poststat,@single_assembly,@single_assembly,@single_assembly,@gend,@single_assembly,@a1;
 
 if length(trim(@pcd))=11 then

set @asm=single_assembly;
set @gid=a1;
set @pcode=@pcd;
set @noofmember=var_no_of_member;
 execute stmtu22_2 using @asm,@gid,@noofmember,@pcode;
    set found=0;
    set @pcd=" ";
    ELSE
    set @pcd="  ";
   set  found=1;
   end if;
   
if (@cnt3>0 and found=1) THEN

execute stmtr23_2 using @zone,@single_poststat,@single_assembly,@single_assembly,@single_assembly,@gend,@single_assembly,@a1;


   if length(trim(@pcd))=11 then

set @asm=single_assembly;
set @gid=a1;

set @pcode=@pcd;
set @noofmember=var_no_of_member;
 execute stmtu23_2 using @asm,@gid,@noofmember,@pcode;
    set found=0;
    set @pcd=" ";
    ELSE
    set @pcd="  ";
   set  found=1;
   end if;
   end if;
if (@cnt4>0 and found=1) THEN

execute stmtr24_2 using @zone,@single_poststat,@single_assembly,@single_assembly,@single_assembly,@gend,@single_assembly,@a1;

   if length(trim(@pcd))=11 then

set @asm=single_assembly;
set @gid=a1;
set @pcode=@pcd;
set @noofmember=var_no_of_member;
 execute stmtu24_2 using @asm,@gid,@noofmember,@pcode;
    set found=0;
      set @pcd=" ";
    ELSE
    set @pcd="  ";
   set  found=1;
   end if;
    end if;
   
  set a1=a1+1;
  END WHILE;
end if;




   
 if (@cnt1=0 and @cnt2=0 and @cnt3>0) THEN
 
      while (a1<=(var_no_party+var_start_sl)) do 
 SET @a1 = a1;
 set @zone=zone;
 set @single_assembly=single_assembly;
 set @single_poststat=single_poststat;
 
  set @gend=var_gender; 


execute stmtr23_2 using @zone,@single_poststat,@single_assembly,@single_assembly,@single_assembly,@gend,@single_assembly,@a1;

   if length(trim(@pcd))=11 then

set @asm=single_assembly;
set @gid=a1;

set @pcode=@pcd;
set @noofmember=var_no_of_member;
 execute stmtu23_2 using @asm,@gid,@noofmember,@pcode;
    set found=0;
    set @pcd=" ";
    ELSE
    set @pcd="  ";
   set  found=1;
   end if;
if (@cnt4>0 and found=1) THEN

execute stmtr24_2 using @zone,@single_poststat,@single_assembly,@single_assembly,@single_assembly,@gend,@single_assembly,@a1;
   if length(trim(@pcd))=11 then

set @asm=single_assembly;
set @gid=a1;
set @pcode=@pcd;
set @noofmember=var_no_of_member;

 execute stmtu24_2 using @asm,@gid,@noofmember,@pcode;
    set found=0;
    set @pcd=" ";
    ELSE
    set @pcd="  ";
   set  found=1;
   end if;
    end if;
   
  set a1=a1+1;
  END WHILE;
  
  end if;
  
  



  
 if (@cnt1=0 and @cnt2=0 and @cnt3=0 and @cnt4>0) THEN
 
 myloop:     while (a1<=(var_no_party+var_start_sl)) do 
 
SET @a1 = a1;
 set @zone=zone;
 set @single_assembly=single_assembly;
 set @single_poststat=single_poststat;
 
  set @gend=var_gender; 

execute stmtr24_2 using @zone,@single_poststat,@single_assembly,@single_assembly,@single_assembly,@gend,@single_assembly,@a1;

   if length(trim(@pcd))=11 then

set @asm=single_assembly;
set @gid=a1;
set @pcode=@pcd;
set @noofmember=var_no_of_member;

 execute stmtu24_2 using @asm,@gid,@noofmember,@pcode;
    set found=0;
    set @pcd=" ";
    ELSE
    set @pcd="  ";
   set  found=1;
 
   leave myloop;
   end if;
    
   
  set a1=a1+1;
  END WHILE myloop;
end if;


 
    
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`ppds`@`%` PROCEDURE `rand2_mainP3`(IN `var_no_of_member` INT, IN `single_assembly` CHAR(3), IN `var_start_sl` INT, IN `zone` CHAR(4), IN `var_no_party` INT, IN `var_dist` CHAR(2), IN `var_gender` CHAR(1))
    NO SQL
BEGIN

 
DECLARE flag_member INT DEFAULT 0;
Declare  a1  INT default 0;
Declare  cnt1  INT default 0;
Declare  cnt2  INT default 0;
Declare  cnt3  INT default 0;
Declare  cnt4  INT default 0;
declare pcd character(11) default ' ' ;
declare found int default 0;
 DECLARE single_poststat CHAR(2) DEFAULT '';

 set @table=concat('personnela',var_dist);
              
     set single_poststat='P2';
     SET @a = var_start_sl;
     set @asm=single_assembly;
     set @zn=zone;
     set @pst=single_poststat;
     set @gend=var_gender;
     set @noofmember=var_no_of_member;
     SET a1 = var_start_sl+1;                           
     set single_poststat='P3';
              

    
 execute stmtc31_2 using @gend;
execute stmtc32_2 using @gend;

execute stmtc33_2 using  @gend;
execute stmtc34_2 using @gend;




    
  if (@cnt1> 0 ) then
  
 
 
 
      while (a1<=(var_no_party+var_start_sl)) do 
      
 SET @a1 = a1;
 set @zone=zone;
 set @single_assembly=single_assembly;
 set @single_poststat=single_poststat;
 
  set @gend=var_gender; 

execute stmtr31_2 using @zone,@single_poststat,@single_assembly,@single_assembly,@single_assembly,@gend,@single_assembly,@a1;


   if length(trim(@pcd))=11 then

set @asm=single_assembly;
set @gid=a1;
set @pcode=@pcd;
set @noofmember=var_no_of_member;
execute stmtu31_2 using @asm,@gid,@noofmember,@pcode;


    set found=0;
    set @pcd=" ";
    ELSE
    set @pcd="  ";
   set  found=1;
   end if;
if (@cnt2>0 and found=1) THEN

execute stmtr32_2 using  @zone,@single_poststat,@single_assembly,@single_assembly,@single_assembly,@gend,@single_assembly,@a1;

   if length(trim(@pcd))=11 then

set @asm=single_assembly;
set @gid=a1;
set @pcode=@pcd;
set @noofmember=var_no_of_member;
 execute stmtu32_2 using @asm,@gid,@noofmember,@pcode;
    set found=0;
    set @pcd=" ";
    ELSE
    set @pcd="  ";
   set  found=1;
   end if;
end if ;
if (@cnt3>0 and found=1) THEN

execute stmtr33_2 using @zone,@single_poststat,@single_assembly,@single_assembly,@single_assembly,@gend,@single_assembly,@a1;

   if length(trim(@pcd))=11 then

set @asm=single_assembly;
set @gid=a1;

set @pcode=@pcd;
set @noofmember=var_no_of_member;
 execute stmtu33_2 using @asm,@gid,@noofmember,@pcode;
    set found=0;
    set @pcd=" ";
    ELSE
    set @pcd="  ";
   set  found=1;
   end if;
end if;
if (@cnt4>0 and found=1) THEN

execute stmtr34_2 using @zone,@single_poststat,@single_assembly,@single_assembly,@single_assembly,@gend,@single_assembly,@a1;

   if length(trim(@pcd))=11 then

set @asm=single_assembly;
set @gid=a1;
set @pcode=@pcd;
set @noofmember=var_no_of_member;
 execute stmtu34_2 using @asm,@gid,@noofmember,@pcode;
    set found=0;
    set @pcd=" ";
    ELSE
    set @pcd="  ";
   set  found=1;
   end if;
    end if;
   
  set a1=a1+1;
  END WHILE;

end if;


   
 if (@cnt1=0 and @cnt2>0) THEN
 
      while (a1<=(var_no_party+var_start_sl)) do 
 SET @a1 = a1;
 set @zone=zone;
 set @single_assembly=single_assembly;
 set @single_poststat=single_poststat;
 
  set @gend=var_gender; 

 execute stmtr32_2 using @zone,@single_poststat,@single_assembly,@single_assembly,@single_assembly,@gend,@single_assembly,@a1;
 
 if length(trim(@pcd))=11 then

set @asm=single_assembly;
set @gid=a1;
set @pcode=@pcd;
set @noofmember=var_no_of_member;
 execute stmtu32_2 using @asm,@gid,@noofmember,@pcode;
    set found=0;
    set @pcd=" ";
    ELSE
    set @pcd="  ";
   set  found=1;
   end if;
   
if (@cnt3>0 and found=1) THEN

execute stmtr33_2 using @zone,@single_poststat,@single_assembly,@single_assembly,@single_assembly,@gend,@single_assembly,@a1;


   if length(trim(@pcd))=11 then

set @asm=single_assembly;
set @gid=a1;

set @pcode=@pcd;
set @noofmember=var_no_of_member;
 execute stmtu33_2 using @asm,@gid,@noofmember,@pcode;
    set found=0;
    set @pcd=" ";
    ELSE
    set @pcd="  ";
   set  found=1;
   end if;
   end if;
if (@cnt4>0 and found=1) THEN

execute stmtr34_2 using @zone,@single_poststat,@single_assembly,@single_assembly,@single_assembly,@gend,@single_assembly,@a1;

   if length(trim(@pcd))=11 then

set @asm=single_assembly;
set @gid=a1;
set @pcode=@pcd;
set @noofmember=var_no_of_member;
 execute stmtu34_2 using @asm,@gid,@noofmember,@pcode;
    set found=0;
      set @pcd=" ";
    ELSE
    set @pcd="  ";
   set  found=1;
   end if;
    end if;
   
  set a1=a1+1;

  END WHILE;
end if;




   
 if (@cnt1=0 and @cnt2=0 and @cnt3>0) THEN
 
      while (a1<=(var_no_party+var_start_sl)) do 
 SET @a1 = a1;
 set @zone=zone;
 set @single_assembly=single_assembly;
 set @single_poststat=single_poststat;
 
  set @gend=var_gender; 


execute stmtr33_2 using @zone,@single_poststat,@single_assembly,@single_assembly,@single_assembly,@gend,@single_assembly,@a1;

   if length(trim(@pcd))=11 then

set @asm=single_assembly;
set @gid=a1;

set @pcode=@pcd;
set @noofmember=var_no_of_member;
 execute stmtu33_2 using @asm,@gid,@noofmember,@pcode;
    set found=0;
    set @pcd=" ";
    ELSE
    set @pcd="  ";
   set  found=1;
   end if;
if (@cnt4>0 and found=1) THEN

execute stmtr34_2 using @zone,@single_poststat,@single_assembly,@single_assembly,@single_assembly,@gend,@single_assembly,@a1;
   if length(trim(@pcd))=11 then

set @asm=single_assembly;
set @gid=a1;
set @pcode=@pcd;
set @noofmember=var_no_of_member;

 execute stmtu34_2 using @asm,@gid,@noofmember,@pcode;
    set found=0;
    set @pcd=" ";
    ELSE
    set @pcd="  ";
   set  found=1;
   end if;
    end if;
   
  set a1=a1+1;
  END WHILE;
  
  end if;
  
  



  
 if (@cnt1=0 and @cnt2=0 and @cnt3=0 and @cnt4>0) THEN
 
 myloop:     while (a1<=(var_no_party+var_start_sl)) do 
 
SET @a1 = a1;
 set @zone=zone;
 set @single_assembly=single_assembly;
 set @single_poststat=single_poststat;
 
  set @gend=var_gender; 

execute stmtr34_2 using @zone,@single_poststat,@single_assembly,@single_assembly,@single_assembly,@gend,@single_assembly,@a1;

   if length(trim(@pcd))=11 then

set @asm=single_assembly;
set @gid=a1;
set @pcode=@pcd;
set @noofmember=var_no_of_member;

 execute stmtu34_2 using @asm,@gid,@noofmember,@pcode;
    set found=0;
    set @pcd=" ";
    ELSE
    set @pcd="  ";
   set  found=1;
 
   leave myloop;
   end if;
    
   
  set a1=a1+1;
  END WHILE myloop;
end if;


 
    
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`ppds`@`%` PROCEDURE `rand2_mainPR`(IN `var_no_of_member` INT, IN `single_assembly` CHAR(3), IN `var_start_sl` INT, IN `zone` CHAR(4), IN `var_no_party` INT, IN `var_dist` CHAR(2), IN `var_gender` CHAR(1))
    NO SQL
BEGIN

 
DECLARE flag_member INT DEFAULT 0;
Declare  a1  INT default 0;
Declare  cnt1  INT default 0;
Declare  cnt2  INT default 0;
Declare  cnt3  INT default 0;
Declare  cnt4  INT default 0;
declare pcd character(11) default ' ' ;
declare found int default 0;
 DECLARE single_poststat CHAR(2) DEFAULT '';

 set @table=concat('personnela',var_dist);
 
              
     set single_poststat='PR';
     SET @a = var_start_sl;
     set @asm=single_assembly;
     set @zn=zone;
     set @pst=single_poststat;
     set @gend=var_gender;
     set @noofmember=var_no_of_member;
     SET a1 = var_start_sl+1;                           
     
              

    
 execute stmtcr1_2 using @gend;
execute stmtcr2_2 using @gend;

execute stmtcr3_2 using  @gend;
execute stmtcr4_2 using @gend;




    
  if (@cnt1> 0 ) then
  
 
 
 
      while (a1<=(var_no_party+var_start_sl)) do 
      
 SET @a1 = a1;
 set @zone=zone;
 set @single_assembly=single_assembly;
 set @single_poststat=single_poststat;
 
  set @gend=var_gender; 

execute stmtrr1_2 using @zone,@single_poststat,@single_assembly,@single_assembly,@single_assembly,@gend,@single_assembly,@a1;


   if length(trim(@pcd))=11 then

set @asm=single_assembly;
set @gid=a1;
set @pcode=@pcd;
set @noofmember=var_no_of_member;
execute stmtur1_2 using @asm,@gid,@noofmember,@pcode;


    set found=0;
    set @pcd=" ";
    ELSE
    set @pcd="  ";
   set  found=1;
   end if;
if (@cnt2>0 and found=1) THEN

execute stmtrr2_2 using  @zone,@single_poststat,@single_assembly,@single_assembly,@single_assembly,@gend,@single_assembly,@a1;

   if length(trim(@pcd))=11 then

set @asm=single_assembly;
set @gid=a1;
set @pcode=@pcd;
set @noofmember=var_no_of_member;
 execute stmtur2_2 using @asm,@gid,@noofmember,@pcode;
    set found=0;
    set @pcd=" ";
    ELSE
    set @pcd="  ";
   set  found=1;
   end if;
end if ;
if (@cnt3>0 and found=1) THEN

execute stmtrr3_2 using @zone,@single_poststat,@single_assembly,@single_assembly,@single_assembly,@gend,@single_assembly,@a1;

   if length(trim(@pcd))=11 then

set @asm=single_assembly;
set @gid=a1;

set @pcode=@pcd;
set @noofmember=var_no_of_member;
 execute stmtur3_2 using @asm,@gid,@noofmember,@pcode;
    set found=0;
    set @pcd=" ";
    ELSE
    set @pcd="  ";
   set  found=1;
   end if;
end if;
if (@cnt4>0 and found=1) THEN

execute stmtrr4_2 using @zone,@single_poststat,@single_assembly,@single_assembly,@single_assembly,@gend,@single_assembly,@a1;

   if length(trim(@pcd))=11 then

set @asm=single_assembly;
set @gid=a1;
set @pcode=@pcd;
set @noofmember=var_no_of_member;
 execute stmtur4_2 using @asm,@gid,@noofmember,@pcode;
    set found=0;
    set @pcd=" ";
    ELSE
    set @pcd="  ";
   set  found=1;
   end if;
    end if;
   
  set a1=a1+1;
  END WHILE;

end if;


   
 if (@cnt1=0 and @cnt2>0) THEN
 
      while (a1<=(var_no_party+var_start_sl)) do 
 SET @a1 = a1;
 set @zone=zone;
 set @single_assembly=single_assembly;
 set @single_poststat=single_poststat;
 
  set @gend=var_gender; 

 execute stmtrr2_2 using @zone,@single_poststat,@single_assembly,@single_assembly,@single_assembly,@gend,@single_assembly,@a1;
 
 if length(trim(@pcd))=11 then

set @asm=single_assembly;
set @gid=a1;
set @pcode=@pcd;
set @noofmember=var_no_of_member;
 execute stmtur2_2 using @asm,@gid,@noofmember,@pcode;
    set found=0;
    set @pcd=" ";
    ELSE
    set @pcd="  ";
   set  found=1;
   end if;
   
if (@cnt3>0 and found=1) THEN

execute stmtrr3_2 using @zone,@single_poststat,@single_assembly,@single_assembly,@single_assembly,@gend,@single_assembly,@a1;


   if length(trim(@pcd))=11 then

set @asm=single_assembly;
set @gid=a1;

set @pcode=@pcd;
set @noofmember=var_no_of_member;
 execute stmtur3_2 using @asm,@gid,@noofmember,@pcode;
    set found=0;
    set @pcd=" ";
    ELSE
    set @pcd="  ";
   set  found=1;
   end if;
   end if;
if (@cnt4>0 and found=1) THEN

execute stmtrr4_2 using @zone,@single_poststat,@single_assembly,@single_assembly,@single_assembly,@gend,@single_assembly,@a1;

   if length(trim(@pcd))=11 then

set @asm=single_assembly;
set @gid=a1;
set @pcode=@pcd;
set @noofmember=var_no_of_member;
 execute stmtur4_2 using @asm,@gid,@noofmember,@pcode;
    set found=0;
      set @pcd=" ";
    ELSE
    set @pcd="  ";
   set  found=1;
   end if;
    end if;
   
  set a1=a1+1;
  END WHILE;
end if;




   
 if (@cnt1=0 and @cnt2=0 and @cnt3>0) THEN
 
      while (a1<=(var_no_party+var_start_sl)) do 
 SET @a1 = a1;
 set @zone=zone;
 set @single_assembly=single_assembly;
 set @single_poststat=single_poststat;
 
  set @gend=var_gender; 


execute stmtrr3_2 using @zone,@single_poststat,@single_assembly,@single_assembly,@single_assembly,@gend,@single_assembly,@a1;

   if length(trim(@pcd))=11 then

set @asm=single_assembly;
set @gid=a1;

set @pcode=@pcd;
set @noofmember=var_no_of_member;
 execute stmtur3_2 using @asm,@gid,@noofmember,@pcode;
    set found=0;
    set @pcd=" ";
    ELSE
    set @pcd="  ";
   set  found=1;
   end if;
if (@cnt4>0 and found=1) THEN

execute stmtrr4_2 using @zone,@single_poststat,@single_assembly,@single_assembly,@single_assembly,@gend,@single_assembly,@a1;
   if length(trim(@pcd))=11 then

set @asm=single_assembly;
set @gid=a1;
set @pcode=@pcd;
set @noofmember=var_no_of_member;

 execute stmtur4_2 using @asm,@gid,@noofmember,@pcode;
    set found=0;
    set @pcd=" ";
    ELSE
    set @pcd="  ";
   set  found=1;
   end if;
    end if;
   
  set a1=a1+1;
  END WHILE;
  
  end if;
  
  



  
 if (@cnt1=0 and @cnt2=0 and @cnt3=0 and @cnt4>0) THEN
 
 myloop:     while (a1<=(var_no_party+var_start_sl)) do 
 
SET @a1 = a1;
 set @zone=zone;
 set @single_assembly=single_assembly;
 set @single_poststat=single_poststat;
 
  set @gend=var_gender; 

execute stmtrr4_2 using @zone,@single_poststat,@single_assembly,@single_assembly,@single_assembly,@gend,@single_assembly,@a1;

   if length(trim(@pcd))=11 then

set @asm=single_assembly;
set @gid=a1;
set @pcode=@pcd;
set @noofmember=var_no_of_member;

 execute stmtur4_2 using @asm,@gid,@noofmember,@pcode;
    set found=0;
    set @pcd=" ";
    ELSE
    set @pcd="  ";
   set  found=1;
 
   leave myloop;
   end if;
    
   
  set a1=a1+1;
  END WHILE myloop;
end if;


 
    
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`ppds`@`%` PROCEDURE `rand2_mainPa`(IN `var_no_of_member` INT, IN `single_assembly` CHAR(3), IN `var_start_sl` INT, IN `zone` CHAR(4), IN `var_no_party` INT, IN `var_dist` CHAR(2), IN `var_gender` CHAR(1))
    NO SQL
BEGIN

 
DECLARE flag_member INT DEFAULT 0;
Declare  a1  INT default 0;
Declare  cnt1  INT default 0;
Declare  cnt2  INT default 0;
Declare  cnt3  INT default 0;
Declare  cnt4  INT default 0;
declare pcd character(11) default ' ' ;
declare found int default 0;
 DECLARE single_poststat CHAR(2) DEFAULT '';

 set @table=concat('personnela',var_dist);
              
     set single_poststat='PA';
     SET @a = var_start_sl;
     set @asm=single_assembly;
     set @zn=zone;
     set @pst=single_poststat;
     set @gend=var_gender;
     set @noofmember=var_no_of_member;
     SET a1 = var_start_sl+1;                           
     
              

    
 execute stmtca1_2 using @gend;
execute stmtca2_2 using @gend;

execute stmtca3_2 using  @gend;
execute stmtca4_2 using @gend;




    
  if (@cnt1> 0 ) then
  
 
 
 
      while (a1<=(var_no_party+var_start_sl)) do 
      
 SET @a1 = a1;
 set @zone=zone;
 set @single_assembly=single_assembly;
 set @single_poststat=single_poststat;
 
  set @gend=var_gender; 

execute stmtra1_2 using @zone,@single_poststat,@single_assembly,@single_assembly,@single_assembly,@gend,@single_assembly,@a1;


   if length(trim(@pcd))=11 then

set @asm=single_assembly;
set @gid=a1;
set @pcode=@pcd;
set @noofmember=var_no_of_member;
execute stmtua1_2 using @asm,@gid,@noofmember,@pcode;


    set found=0;
    set @pcd=" ";
    ELSE
    set @pcd="  ";
   set  found=1;
   end if;
if (@cnt2>0 and found=1) THEN

execute stmtra2_2 using  @zone,@single_poststat,@single_assembly,@single_assembly,@single_assembly,@gend,@single_assembly,@a1;

   if length(trim(@pcd))=11 then

set @asm=single_assembly;
set @gid=a1;
set @pcode=@pcd;
set @noofmember=var_no_of_member;
 execute stmtua2_2 using @asm,@gid,@noofmember,@pcode;
    set found=0;
    set @pcd=" ";
    ELSE
    set @pcd="  ";
   set  found=1;
   end if;
end if ;
if (@cnt3>0 and found=1) THEN

execute stmtra3_2 using @zone,@single_poststat,@single_assembly,@single_assembly,@single_assembly,@gend,@single_assembly,@a1;

   if length(trim(@pcd))=11 then

set @asm=single_assembly;
set @gid=a1;

set @pcode=@pcd;
set @noofmember=var_no_of_member;
 execute stmtua3_2 using @asm,@gid,@noofmember,@pcode;
    set found=0;
    set @pcd=" ";
    ELSE
    set @pcd="  ";
   set  found=1;
   end if;
end if;
if (@cnt4>0 and found=1) THEN

execute stmtra4_2 using @zone,@single_poststat,@single_assembly,@single_assembly,@single_assembly,@gend,@single_assembly,@a1;

   if length(trim(@pcd))=11 then

set @asm=single_assembly;
set @gid=a1;
set @pcode=@pcd;
set @noofmember=var_no_of_member;
 execute stmtua4_2 using @asm,@gid,@noofmember,@pcode;
    set found=0;
    set @pcd=" ";
    ELSE
    set @pcd="  ";
   set  found=1;
   end if;
    end if;
   
  set a1=a1+1;
  END WHILE;

end if;


   
 if (@cnt1=0 and @cnt2>0) THEN
 
      while (a1<=(var_no_party+var_start_sl)) do 
 SET @a1 = a1;
 set @zone=zone;
 set @single_assembly=single_assembly;
 set @single_poststat=single_poststat;
 
  set @gend=var_gender; 

 execute stmtra2_2 using @zone,@single_poststat,@single_assembly,@single_assembly,@single_assembly,@gend,@single_assembly,@a1;
 
 if length(trim(@pcd))=11 then

set @asm=single_assembly;
set @gid=a1;
set @pcode=@pcd;
set @noofmember=var_no_of_member;
 execute stmtua2_2 using @asm,@gid,@noofmember,@pcode;
    set found=0;
    set @pcd=" ";
    ELSE
    set @pcd="  ";
   set  found=1;
   end if;
   
if (@cnt3>0 and found=1) THEN

execute stmtra3_2 using @zone,@single_poststat,@single_assembly,@single_assembly,@single_assembly,@gend,@single_assembly,@a1;


   if length(trim(@pcd))=11 then

set @asm=single_assembly;
set @gid=a1;

set @pcode=@pcd;
set @noofmember=var_no_of_member;
 execute stmtua3_2 using @asm,@gid,@noofmember,@pcode;
    set found=0;
    set @pcd=" ";
    ELSE
    set @pcd="  ";
   set  found=1;
   end if;
   end if;
if (@cnt4>0 and found=1) THEN

execute stmtra4_2 using @zone,@single_poststat,@single_assembly,@single_assembly,@single_assembly,@gend,@single_assembly,@a1;

   if length(trim(@pcd))=11 then

set @asm=single_assembly;
set @gid=a1;
set @pcode=@pcd;
set @noofmember=var_no_of_member;
 execute stmtua4_2 using @asm,@gid,@noofmember,@pcode;
    set found=0;
      set @pcd=" ";
    ELSE
    set @pcd="  ";
   set  found=1;
   end if;
    end if;
   
  set a1=a1+1;
  END WHILE;
end if;




   
 if (@cnt1=0 and @cnt2=0 and @cnt3>0) THEN
 
      while (a1<=(var_no_party+var_start_sl)) do 
 SET @a1 = a1;
 set @zone=zone;
 set @single_assembly=single_assembly;
 set @single_poststat=single_poststat;
 
  set @gend=var_gender; 


execute stmtra3_2 using @zone,@single_poststat,@single_assembly,@single_assembly,@single_assembly,@gend,@single_assembly,@a1;

   if length(trim(@pcd))=11 then

set @asm=single_assembly;
set @gid=a1;

set @pcode=@pcd;
set @noofmember=var_no_of_member;
 execute stmtua3_2 using @asm,@gid,@noofmember,@pcode;
    set found=0;
    set @pcd=" ";
    ELSE
    set @pcd="  ";
   set  found=1;
   end if;
if (@cnt4>0 and found=1) THEN

execute stmtra4_2 using @zone,@single_poststat,@single_assembly,@single_assembly,@single_assembly,@gend,@single_assembly,@a1;
   if length(trim(@pcd))=11 then

set @asm=single_assembly;
set @gid=a1;
set @pcode=@pcd;
set @noofmember=var_no_of_member;

 execute stmtua4_2 using @asm,@gid,@noofmember,@pcode;
    set found=0;
    set @pcd=" ";
    ELSE
    set @pcd="  ";
   set  found=1;
   end if;
    end if;
   
  set a1=a1+1;
  END WHILE;
  
  end if;
  
  



  
 if (@cnt1=0 and @cnt2=0 and @cnt3=0 and @cnt4>0) THEN
 
 myloop:     while (a1<=(var_no_party+var_start_sl)) do 
 
SET @a1 = a1;
 set @zone=zone;
 set @single_assembly=single_assembly;
 set @single_poststat=single_poststat;
 
  set @gend=var_gender; 

execute stmtra4_2 using @zone,@single_poststat,@single_assembly,@single_assembly,@single_assembly,@gend,@single_assembly,@a1;

   if length(trim(@pcd))=11 then

set @asm=single_assembly;
set @gid=a1;
set @pcode=@pcd;
set @noofmember=var_no_of_member;

 execute stmtua4_2 using @asm,@gid,@noofmember,@pcode;
    set found=0;
    set @pcd=" ";
    ELSE
    set @pcd="  ";
   set  found=1;
 
   leave myloop;
   end if;
    
   
  set a1=a1+1;
  END WHILE myloop;
end if;


 
    
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`ppds`@`%` PROCEDURE `rand2_main_ab`(IN `var_no_of_member` INT, IN `single_assembly` CHAR(3), IN `var_groupid` INT, IN `zone` CHAR(4), IN `var_no_party` INT, IN `var_dist` CHAR(2), IN `var_gender` CHAR(1), IN `ph` CHAR(1))
    NO SQL
BEGIN
DECLARE flag_member INT DEFAULT 0;
Declare  a1  INT default 0;
Declare  cnt1  INT default 0;
Declare  cnt2  INT default 0;
Declare  cnt3  INT default 0;
Declare  cnt4  INT default 0;
declare pcd character(11) default ' ' ;
declare found int default 0;
 DECLARE single_poststat CHAR(2) DEFAULT '';

set @table=concat('personnela',var_dist);
           set @asm=single_assembly;
     set @zn=zone;
     set @pst=single_poststat;
     set @gend=var_gender;
     set @noofmember=var_no_of_member;
       
             
 
      
 SET @a1 = var_groupid;
 set @zone=zone;
  set @single_assembly=single_assembly;
  
  
  set @single_poststat='PR';
set single_poststat='PR';
 set @gend=var_gender; 
 execute  stmt_pr_r_ba_2 using @zone,@single_poststat,@single_assembly,@single_assembly,@single_assembly,@gend;


set @asm=single_assembly;
set @gid=var_groupid;
set @pcode=@pcd;
set @noofmember=var_no_of_member;
execute stmt_pr_u_ba_2 using @asm,@gid,@noofmember,@pcode;

  
  
  
 set @single_poststat='P1';
set single_poststat='P1';
 set @gend=var_gender; 
 execute stmt_p1_r_2 using @zone,@single_poststat,@single_assembly,@single_assembly,@single_assembly,@gend,@single_assembly,@a1;


set @asm=single_assembly;
set @gid=var_groupid;
set @pcode=@pcd;
set @noofmember=var_no_of_member;
execute stmt_p1_u_2 using @asm,@gid,@noofmember,@pcode;

set @single_poststat='P2';
execute stmt_p2_r_2 using @zone,@single_poststat,@single_assembly,@single_assembly,@single_assembly,@gend,@single_assembly,@a1;


set @asm=single_assembly;
set @gid=var_groupid;
set @pcode=@pcd;
set @noofmember=var_no_of_member;
execute stmt_p2_u_2 using @asm,@gid,@noofmember,@pcode;

set @single_poststat='P3';
execute stmt_p3_r_2 using @zone,@single_poststat,@single_assembly,@single_assembly,@single_assembly,@gend,@single_assembly,@a1;


set @asm=single_assembly;
set @gid=var_groupid;
set @pcode=@pcd;
set @noofmember=var_no_of_member;
execute stmt_p2_u_2 using @asm,@gid,@noofmember,@pcode;

set @noofmember=var_no_of_member;

if (@noofmember=5) then
select @noofmember;
set @single_poststat='PA';
execute stmt_pA_r_2 using @zone,@single_poststat,@single_assembly,@single_assembly,@single_assembly,@gend,@single_assembly,@a1;
 select @pcd;


set @asm=single_assembly;
set @gid=var_groupid;
set @pcode=@pcd;

execute stmt_pA_u_2 using @asm,@gid,@noofmember,@pcode;

end if;






   
  
end$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`ppds`@`%` PROCEDURE `rand2_main_all`(IN `var_no_of_member` INT, IN `single_assembly` CHAR(3), IN `var_start_sl` INT, IN `zone` CHAR(4), IN `var_no_party` INT, IN `var_dist` CHAR(2), IN `var_gender` CHAR(1))
    NO SQL
BEGIN
set @asm=single_assembly;
set @zn=zone;
set @nop=var_no_party;
set @nom=var_no_of_member;
set @a=var_start_sl;
set @gnd=var_gender;
set @dist=var_dist;



 execute stmtpr_n_2 using @zn,@gnd,@asm,@asm,@asm,@nop,@asm,@nom;
 set @a=var_start_sl;
 execute stmtp1_n_2 using @zn,@gnd,@asm,@asm,@asm,@nop,@asm,@nom;
 set @a=var_start_sl;
 execute stmtp2_n_2 using @zn,@gnd,@asm,@asm,@asm,@nop,@asm,@nom;
 set @a=var_start_sl;
 execute stmtp3_n_2 using @zn,@gnd,@asm,@asm,@asm,@nop,@asm,@nom;
 
 if  (var_no_of_member=5) THEN
 
 set @a=var_start_sl;
 execute stmtpa_n_2 using @zn,@gnd,@asm,@asm,@asm,@nop,@asm,@nom;
 
 end if;




END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`ppds`@`%` PROCEDURE `rand2_reserve`(IN `zone` CHAR(4), IN `dist` CHAR(2), IN `ph` CHAR(1))
BEGIN
	DECLARE flag_ass2 INT DEFAULT 0;
    DECLARE var_no_party INT ;
    Declare var_reserve INT DEFAULT 0;
    DECLARE var_start_sl INT ;
    DECLARE var_no_of_member INT ;
	DECLARE single_assembly CHAR(3) DEFAULT "" ;
    DECLARE var_poststat CHAR(2) DEFAULT "" ;
    DECLARE var_no_pc CHAR(1) DEFAULT "" ;
    DECLARE var_gender CHAR(1) DEFAULT "" ;
    DECLARE var_numb INT ;
       
    DECLARE assembly_cursor1 CURSOR FOR 
    	SELECT reserve.forassembly,reserve.number_of_member,reserve.poststat,reserve.no_or_pc ,reserve.numb,assembly_party.no_party,assembly_party.gender
        FROM reserve 
        INNER JOIN assembly_zone ON reserve.forassembly = assembly_zone.assemblycd 
        INNER JOIN assembly_party  on  assembly_party.assemblycd = reserve.forassembly and assembly_party.no_of_member = reserve.number_of_member
        WHERE assembly_zone.zone = zone AND assembly_zone.phase_id=ph AND assembly_zone.districtcd=dist  and assembly_zone.rand_status2='A';
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET flag_ass2 = 1;
    
     
     
    OPEN assembly_cursor1;
    loop_assembly1: LOOP
    	FETCH assembly_cursor1 INTO single_assembly,var_no_of_member,var_poststat,var_no_pc,var_numb,var_no_party,var_gender ;
		IF flag_ass2 = 1 THEN 
    		LEAVE loop_assembly1;
    	END IF;
        	if var_no_pc='P' THEN
              SET  var_reserve=var_no_party*var_numb/100;
            end if; 
            if var_no_pc='N' THEN
              SET  var_reserve=var_numb;
            end if; 
               SET var_start_sl=0;
              SET @p0=var_no_of_member; 
              SET @p1=single_assembly; 
              SET @p2=var_start_sl; 
               SET @p3=zone; 
               SET @p4=var_reserve;
               set @p5=var_poststat;
               set @p6=dist;
               set @p7=var_gender;
               set @p8=ph;
               
                 
               CALL `reserve2_main`(@p0,@p1,@p2,@p3,@p4,@p5,@p6,@p7,@p8); 
               
            
    END LOOP loop_assembly1;
   commit;
    CLOSE assembly_cursor1;
    DEALLOCATE PREPARE stmtrsv_2;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`ppds`@`%` PROCEDURE `randomisation2`(IN `zone` CHAR(4), IN `dist` CHAR(2), IN `ph` CHAR(1))
    NO SQL
BEGIN
set @p9=zone;
set @p10=dist;
set @p11=ph;
set @d=dist;

 call init_2(@p10,@p9,@p11);

   call rand2(@p9,@p10,@p11);
   call rand2_ab(@p9,@p10,@p11);
   call rand2_ab_loop(@p9,@p10,@p11);
  
   call decl_statement_oth_2(@d,@p11);
   call rand2_reserve(@p9,@p10,@p11);
  
  call dcrc_pp(zone,dist,@p11);
   call dcrc_reserve(zone,dist,@p11);
  deallocate prepare stmtdc;
  deallocate prepare stmtdc1;


END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`ppds`@`%` PROCEDURE `reserve2_main`(IN `var_no_of_member` INT, IN `single_assembly` CHAR(3), IN `var_start_sl` INT, IN `zone` CHAR(4), IN `var_reserve` INT, IN `var_poststat` CHAR(2), IN `dist` CHAR(2), IN `var_gender` CHAR(1), IN `ph` CHAR(1))
    NO SQL
BEGIN

set @zone=zone;
   set @asm=single_assembly;
   set @rsv1=var_reserve;
   set @pst=var_poststat;
   set @table=concat('personnela',dist);
   set @gnd=var_gender;
   set @a=0;
 
 execute stmtrsv_2 using @asm,@asm,@asm,@pst,@zone,@gnd,@rsv1,@asm;
 
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`ppds`@`%` PROCEDURE `second_rand_p`(IN `forzone` CHAR(4), IN `tbl` VARCHAR(12), IN `order_no` CHAR(4), IN `phase_id` CHAR(1))
    NO SQL
BEGIN

SET @query = CONCAT("delete from second_rand_table where party_reserve='P' and forzone=", "'", forzone, "' and second_rand_table.phase=","'", phase_id, "'"); 
   
PREPARE stmt FROM @query; 
EXECUTE stmt; 
DEALLOCATE PREPARE stmt; 

SET 
  @query1 = CONCAT("INSERT INTO second_rand_table( assembly, forzone, groupid ,mem_no, dcrcgrp,party_reserve,order_no,order_date,phase) \n                   \nSELECT  forassembly, forzone, groupid,count(*), dcrccd ,booked,order_no_date.order_no,\n      order_no_date.order_dt,phase FROM ", tbl," a\n                   \nInner Join order_no_date On order_no_date.districtcd = a.fordistrict\nLEFT JOIN phase On phase.code = a.phase\nWHERE booked = 'P'  and poststat != 'MO' and selected = 1 and forzone = ", "'", forzone, "'"," and order_no_date.order_cd = ", "'", order_no,"' "," and a.phase = ", "'", phase_id,"'"," GROUP BY a.forassembly, a.groupid, a.forzone,a.dcrccd ,a.booked"); 
   
PREPARE stmt FROM @query1; 
EXECUTE stmt; 
DEALLOCATE PREPARE stmt; 

SET 
  @query2 = CONCAT("UPDATE second_rand_table \nJOIN ", tbl," a  ON second_rand_table.assembly=a.forassembly and second_rand_table.groupid=a.groupid\nJOIN personnel ON personnel.personcd=a.personcd\njoin district b on personnel.districtcd=b.districtcd\n  SET second_rand_table.pr_personcd = personnel.personcd,second_rand_table.pr_name = personnel.officer_name,\n  second_rand_table.`pr_designation` =personnel.off_desg,second_rand_table.`pr_officecd`= a.officecd,\n  second_rand_table.`pr_status`='PR',second_rand_table.pr_post_stat='Presiding Officer',second_rand_table.pr_mobno = personnel.mob_no,second_rand_table.pr_district = b.district\nWHERE a.booked = 'P'  and a.poststat = 'PR' and second_rand_table.party_reserve='P' and a.forzone=", "'", forzone, "' and a.phase = ", "'", phase_id,"'"); 
   
PREPARE stmt FROM @query2; 
EXECUTE stmt; 
DEALLOCATE PREPARE stmt; 

SET 
  @query3 = CONCAT("\nUPDATE second_rand_table \nJOIN ", tbl," a  ON second_rand_table.assembly=a.forassembly and second_rand_table.groupid=a.groupid\nJOIN personnel ON personnel.personcd=a.personcd\njoin district b on personnel.districtcd=b.districtcd\n  SET second_rand_table.p1_personcd = personnel.personcd,second_rand_table.p1_name = personnel.officer_name,\n  second_rand_table.`p1_designation` =personnel.off_desg,second_rand_table.`p1_officecd`= a.officecd,\n  second_rand_table.`p1_status`='P1',second_rand_table.p1_post_stat='1st Polling Officer',second_rand_table.p1_mobno =personnel.mob_no,second_rand_table.p1_district = b.district\nWHERE  a.booked = 'P'  and a.poststat = 'P1' and second_rand_table.party_reserve='P' and  a.forzone=", "'", forzone, "' and a.phase = ", "'", phase_id,"'"); 
   
PREPARE stmt FROM @query3; 
EXECUTE stmt; 
DEALLOCATE PREPARE stmt; 

SET 
  @query4 = CONCAT("UPDATE second_rand_table \nJOIN ", tbl," a  ON second_rand_table.assembly=a.forassembly and second_rand_table.groupid=a.groupid\nJOIN personnel ON personnel.personcd=a.personcd\njoin district b on personnel.districtcd=b.districtcd\n  SET second_rand_table.p2_personcd = personnel.personcd,second_rand_table.p2_name = personnel.officer_name,\n  second_rand_table.`p2_designation` =personnel.off_desg,second_rand_table.`p2_officecd`= a.officecd,\n  second_rand_table.`p2_status`='P2',second_rand_table.p2_post_stat='2nd Polling Officer',second_rand_table.p2_mobno =personnel.mob_no,second_rand_table.p2_district = b.district\nWHERE  a.booked = 'P'  and a.poststat = 'P2' and second_rand_table.party_reserve='P' and  a.forzone=", "'", forzone, "' and a.phase = ", "'", phase_id,"'"); 
   
PREPARE stmt FROM @query4; 
EXECUTE stmt; 
DEALLOCATE PREPARE stmt; 

SET 
  @query5 = CONCAT("UPDATE second_rand_table \nJOIN ", tbl," a  ON second_rand_table.assembly=a.forassembly and second_rand_table.groupid=a.groupid\nJOIN personnel ON personnel.personcd=a.personcd\njoin district b on personnel.districtcd=b.districtcd\n  SET second_rand_table.p3_personcd = personnel.personcd,second_rand_table.p3_name = personnel.officer_name,\n  second_rand_table.`p3_designation` =personnel.off_desg,second_rand_table.`p3_officecd`= a.officecd,\n  second_rand_table.`p3_status`='P3',second_rand_table.p3_post_stat='3rd Polling Officer',second_rand_table.p3_mobno =personnel.mob_no,second_rand_table.p3_district = b.district\nWHERE  a.booked = 'P'  and a.poststat = 'P3' and second_rand_table.party_reserve='P' and  a.forzone=", "'", forzone, "' and a.phase = ", "'", phase_id,"'"); 
   
PREPARE stmt FROM @query5; 
EXECUTE stmt; 
DEALLOCATE PREPARE stmt; 

SET 
  @query6 = CONCAT("UPDATE second_rand_table \nJOIN ", tbl," a  ON second_rand_table.assembly=a.forassembly and second_rand_table.groupid=a.groupid\nJOIN personnel ON personnel.personcd=a.personcd\n                   join district b on personnel.districtcd=b.districtcd\n  SET second_rand_table.pa_personcd = personnel.personcd,second_rand_table.pa_name = personnel.officer_name,\n  second_rand_table.`pa_designation` =personnel.off_desg,second_rand_table.`pa_officecd`= a.officecd,\n  second_rand_table.`pa_status`='PA',second_rand_table.pa_post_stat='Addl. 2nd Polling Officer-1',second_rand_table.pa_mobno =personnel.mob_no,second_rand_table.pa_district = b.district\nWHERE  a.booked = 'P'  and a.poststat = 'PA' and second_rand_table.party_reserve='P' and  a.forzone=", "'", forzone, "' and a.phase = ", "'", phase_id,"'"); 
   
PREPARE stmt FROM @query6; 
EXECUTE stmt; 
DEALLOCATE PREPARE stmt; 


SET 
  @query7 = CONCAT("UPDATE second_rand_table \nJOIN ", tbl," a  ON second_rand_table.assembly=a.forassembly and second_rand_table.groupid=a.groupid\nJOIN personnel ON personnel.personcd=a.personcd\n                   join district b on personnel.districtcd=b.districtcd\n  SET second_rand_table.pb_personcd = personnel.personcd,second_rand_table.pb_name = personnel.officer_name,\n  second_rand_table.`pb_designation` =personnel.off_desg,second_rand_table.`pb_officecd`= a.officecd,\n  second_rand_table.`pb_status`='PB',second_rand_table.pb_post_stat='Addl. 2nd Polling Officer-2',second_rand_table.pb_mobno =personnel.mob_no,second_rand_table.pb_district = b.district\nWHERE  a.booked = 'P'  and a.poststat = 'PB' and second_rand_table.party_reserve='P' and  a.forzone=", "'", forzone, "' and a.phase = ", "'", phase_id,"'"); 
   
PREPARE stmt FROM @query7; 
EXECUTE stmt; 
DEALLOCATE PREPARE stmt; 

SET 
  @query8 = CONCAT("UPDATE second_rand_table JOIN office ON second_rand_table.pr_officecd = office.officecd  \njoin subdivision b on office.subdivisioncd=b.subdivisioncd \n SET  second_rand_table.pr_officename =  office.office,second_rand_table.`pr_officeaddress`= office.address1,\n  second_rand_table.pr_postoffice=office.postoffice,second_rand_table.pr_pincode=office.pin, second_rand_table.pr_subdivision=office.subdivisioncd,\n  second_rand_table.pr_block=office.blockormuni_cd,second_rand_table.pr_subdivision=b.subdivision\n WHERE second_rand_table.pr_status = 'PR' and party_reserve='P' and second_rand_table.forzone=", "'", forzone, "' and second_rand_table.phase = ", "'", phase_id,"'"); 
   
PREPARE stmt FROM @query8; 
EXECUTE stmt; 
DEALLOCATE PREPARE stmt; 

SET 
  @query9 = CONCAT("UPDATE second_rand_table JOIN office ON second_rand_table.p1_officecd = office.officecd \njoin subdivision b on office.subdivisioncd=b.subdivisioncd  \n SET  second_rand_table.p1_officename =  office.office,second_rand_table.`p1_officeaddress`= office.address1,\n  second_rand_table.p1_postoffice=office.postoffice,second_rand_table.p1_pincode=office.pin, second_rand_table.p1_subdivision=office.subdivisioncd,\n  second_rand_table.p1_block=office.blockormuni_cd,second_rand_table.p1_subdivision=b.subdivision\n WHERE second_rand_table.p1_status = 'P1' and party_reserve='P' and second_rand_table.forzone=", "'", forzone, "' and second_rand_table.phase = ", "'", phase_id,"'"); 
   
PREPARE stmt FROM @query9; 
EXECUTE stmt; 
DEALLOCATE PREPARE stmt; 


SET 
  @query10 = CONCAT("UPDATE second_rand_table JOIN office ON second_rand_table.p2_officecd = office.officecd  \njoin subdivision b on office.subdivisioncd=b.subdivisioncd \n SET  second_rand_table.p2_officename =  office.office,second_rand_table.`p2_officeaddress`= office.address1,\n  second_rand_table.p2_postoffice=office.postoffice,second_rand_table.p2_pincode=office.pin, second_rand_table.p2_subdivision=office.subdivisioncd,\n  second_rand_table.p2_block=office.blockormuni_cd,second_rand_table.p2_subdivision=b.subdivision\n WHERE  second_rand_table.p2_status = 'P2' and party_reserve='P' and second_rand_table.forzone=", "'", forzone, "' and second_rand_table.phase = ", "'", phase_id,"'"); 
   
PREPARE stmt FROM @query10; 
EXECUTE stmt; 
DEALLOCATE PREPARE stmt; 

SET 
  @query11 = CONCAT("UPDATE second_rand_table JOIN office ON second_rand_table.p3_officecd = office.officecd \njoin subdivision b on office.subdivisioncd=b.subdivisioncd   \n SET  second_rand_table.p3_officename =  office.office,second_rand_table.`p3_officeaddress`= office.address1,\n  second_rand_table.p3_postoffice=office.postoffice,second_rand_table.p3_pincode=office.pin, second_rand_table.p3_subdivision=office.subdivisioncd,\n  second_rand_table.p3_block=office.blockormuni_cd,second_rand_table.p3_subdivision=b.subdivision\n WHERE second_rand_table.p3_status = 'P3' and party_reserve='P' and second_rand_table.forzone=", "'", forzone, "' and second_rand_table.phase = ", "'", phase_id,"'"); 
   
PREPARE stmt FROM @query11; 
EXECUTE stmt; 
DEALLOCATE PREPARE stmt; 

SET 
  @query12 = CONCAT("UPDATE second_rand_table JOIN office ON second_rand_table.pa_officecd = office.officecd \njoin subdivision b on office.subdivisioncd=b.subdivisioncd   \n SET  second_rand_table.pa_officename =  office.office,second_rand_table.`pa_officeaddress`= office.address1,\n  second_rand_table.pa_postoffice=office.postoffice,second_rand_table.pa_pincode=office.pin, second_rand_table.pa_subdivision=office.subdivisioncd,\n  second_rand_table.pa_block=office.blockormuni_cd,second_rand_table.pa_subdivision=b.subdivision\n WHERE second_rand_table.pa_status = 'PA' and party_reserve='P' and second_rand_table.forzone=", "'", forzone, "' and second_rand_table.phase = ", "'", phase_id,"'"); 
   
PREPARE stmt FROM @query12; 
EXECUTE stmt; 
DEALLOCATE PREPARE stmt; 

SET 
  @query13 = CONCAT("UPDATE second_rand_table JOIN office ON second_rand_table.pb_officecd = office.officecd  \njoin subdivision b on office.subdivisioncd=b.subdivisioncd \n SET  second_rand_table.pb_officename =  office.office,second_rand_table.`pb_officeaddress`= office.address1,\n  second_rand_table.pb_postoffice=office.postoffice,second_rand_table.pb_pincode=office.pin, second_rand_table.pb_subdivision=office.subdivisioncd,\n  second_rand_table.pb_block=office.blockormuni_cd,second_rand_table.pb_subdivision=b.subdivision\n WHERE second_rand_table.pb_status = 'PB' and party_reserve='P' and second_rand_table.forzone=", "'", forzone, "' and second_rand_table.phase = ", "'", phase_id,"'"); 
   
PREPARE stmt FROM @query13; 
EXECUTE stmt; 
DEALLOCATE PREPARE stmt; 

SET 
  @query14 = CONCAT("update second_rand_table a join assembly b on a.assembly=b.assemblycd \n set a.assembly_name=b.assemblyname where a.party_reserve='P' and a.forzone=", "'", forzone, "' and a.phase = ", "'", phase_id,"'"); 
   
PREPARE stmt FROM @query14; 
EXECUTE stmt; 
DEALLOCATE PREPARE stmt;

SET 
  @query15 = CONCAT("UPDATE second_rand_table JOIN dcrcmaster  ON second_rand_table.dcrcgrp=dcrcmaster.dcrcgrp \nSET second_rand_table.dc_venue = dcrcmaster.dc_venue, second_rand_table.dc_address = dcrcmaster.dc_address,second_rand_table.rc_venue = dcrcmaster.rc_venue,second_rand_table.dc_time=dcrcmaster.dc_time, second_rand_table.dc_date=DATE(dcrcmaster.dc_date) \nwhere second_rand_table.party_reserve='P' and second_rand_table.forzone=", "'", forzone, "' and second_rand_table.phase = ", "'", phase_id,"'"); 
   
PREPARE stmt FROM @query15; 
EXECUTE stmt; 
DEALLOCATE PREPARE stmt;


SET 
  @query19 = CONCAT("update second_rand_table a \nInner join assembly on assembly.assemblycd=a.assembly Inner join pc b on assembly.pccd=b.pccd\nset a.pcname=b.pcname,a.pccd=b.pccd where a.party_reserve='P' and a.forzone=", "'", forzone, "' and a.phase = ", "'", phase_id,"'"); 
   
PREPARE stmt FROM @query19; 
EXECUTE stmt; 
DEALLOCATE PREPARE stmt;


SET 
  @query191 = CONCAT("update ", tbl," a\nset a.2ndTrainingSch=null where a.booked='P' and a.forzone=", "'", forzone, "' and a.phase = ", "'", phase_id,"'"); 
   
PREPARE stmt FROM @query191; 
EXECUTE stmt; 
DEALLOCATE PREPARE stmt;

SET 
  @query192 = CONCAT("update second_rand_table a \nInner join second_training_schedule on second_training_schedule.assembly=a.assembly \nInner join second_training_venue on second_training_schedule.tr_venue_cd=second_training_venue.venue_cd\nInner join second_training_subvenue on  second_training_schedule.tr_subvenue_cd=second_training_subvenue.subvenue_cd                     \nInner join second_training_date_time on  second_training_schedule.datetimecd=second_training_date_time.datetime_cd  \n                     \nset a.traingcode=second_training_schedule.schedule_code,a.training_venue=second_training_venue.venuename,\n    a.venuecode=second_training_schedule.tr_venue_cd,a.sub_venuename=second_training_subvenue.subvenue,\n   a.venue_addr=second_training_venue.venueaddress,a.training_date=second_training_date_time.training_dt,\n   a.training_time=second_training_date_time.training_time\nwhere second_training_schedule.party_reserve='P' and second_training_schedule.phase_id= ", "'", phase_id,"' \nand a.groupid>=second_training_schedule.group_from and a.groupid<=second_training_schedule.group_to and a.party_reserve='P' and a.forzone=", "'", forzone, "' and a.phase = ", "'", phase_id,"'"); 
   
PREPARE stmt FROM @query192; 
EXECUTE stmt; 
DEALLOCATE PREPARE stmt;

SET 
  @query193 = CONCAT("update ", tbl," a\n Inner join second_training_schedule on second_training_schedule.assembly=a.forassembly                    \nset a.2ndTrainingSch=second_training_schedule.schedule_code \n  where second_training_schedule.party_reserve='P' and second_training_schedule.phase_id= ", "'", phase_id,"' \nand a.groupid>=second_training_schedule.group_from and a.groupid<=second_training_schedule.group_to and  a.booked='P' and a.forzone=", "'", forzone, "' and a.phase = ", "'", phase_id,"'"); 
   
PREPARE stmt FROM @query193; 
EXECUTE stmt; 
DEALLOCATE PREPARE stmt;

SET 
  @query20 = CONCAT("update second_rand_table a join poll_table b on a.assembly=b.assembly_cd set a.polldate=b.poll_date, a.polltime=b.poll_time  where a.party_reserve='P' and a.assembly=b.assembly_cd and a.forzone=", "'", forzone, "' and a.phase = ", "'", phase_id,"'"); 
   
PREPARE stmt FROM @query20; 
EXECUTE stmt; 
DEALLOCATE PREPARE stmt;

SET 
  @query21 = CONCAT("update second_rand_table a join district b on substr(a.`forzone`,1,2)=b.districtcd set a.district=b.district where a.party_reserve='P' and a.forzone=", "'", forzone, "' and a.phase = ", "'", phase_id,"'"); 
   
PREPARE stmt FROM @query21; 
EXECUTE stmt; 
DEALLOCATE PREPARE stmt;

SET @p0=forzone;
SET @p1=phase_id;
CALL second_rand_multi(@p0,@p1);

END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`ppds`@`%` PROCEDURE `second_rand_p_slno`(IN `forzone` CHAR(4), IN `phase_id` CHAR(1))
    NO SQL
BEGIN
SET 
  @query = CONCAT("Update second_rand_table set slno=0 where party_reserve='P' and forzone=", "'", forzone, "' and phase=", "'", phase_id, "'"); 
   
PREPARE stmt FROM @query; 
EXECUTE stmt; 
DEALLOCATE PREPARE stmt; 


SET @id = 0;
UPDATE second_rand_table 
  SET  slno = @id:= @id + 1 
where second_rand_table.forzone=forzone and party_reserve='P'
order by second_rand_table.block_muni_cd,
second_rand_table.pers_off;

SET @idCount="";
SET @idCount = (Select count(*) as cnt from second_rand_table WHERE second_rand_table.forzone=forzone and party_reserve='P' and phase=phase_id);
select @idCount as t_Count;

END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`ppds`@`%` PROCEDURE `second_rand_r`(IN `forzone` CHAR(4), IN `tbl` VARCHAR(12))
    NO SQL
BEGIN

SET 
  @query2 = CONCAT("UPDATE second_rand_table b
JOIN ", tbl," a  ON b.assembly=a.forassembly 
    and b.groupid=a.groupid and b.per_poststat=a.poststat
    and b.pers_off=a.officecd
JOIN personnel ON personnel.personcd=a.personcd
JOIN office ON b.pers_off = office.officecd 
join subdivision c on office.subdivisioncd=c.subdivisioncd 
join block_muni on office.blockormuni_cd=block_muni.blockminicd
join district d on personnel.districtcd=d.districtcd
                   
  SET b.pr_personcd = personnel.personcd,b.pr_name = personnel.officer_name,
  b.`pr_designation` =personnel.off_desg,b.`pr_officecd`= a.officecd,
  b.`pr_status`='PR',b.pr_post_stat='Presiding Officer',b.pr_mobno = personnel.mob_no,
  b.pr_officename =  office.office,b.`pr_officeaddress`= office.address1,
  b.pr_postoffice=office.postoffice,b.pr_pincode=office.pin, b.pr_subdivision=office.subdivisioncd,
  b.pr_block=office.blockormuni_cd,b.pr_subdivision=c.subdivision,
  b.block_muni_cd=office.blockormuni_cd,b.block_muni_name=block_muni.blockmuni,b.pr_district = d.district                
WHERE a.booked = 'R'  and a.poststat = 'PR' and b.party_reserve='R' and a.forzone=", "'", forzone, "'"); 
   
PREPARE stmt FROM @query2; 
EXECUTE stmt; 
DEALLOCATE PREPARE stmt; 

SET 
  @query3 = CONCAT("
UPDATE second_rand_table b
JOIN ", tbl," a  ON b.assembly=a.forassembly and b.groupid=a.groupid
                   and b.per_poststat=a.poststat
    and b.pers_off=a.officecd
JOIN personnel ON personnel.personcd=a.personcd 
JOIN office ON b.pers_off = office.officecd 
join subdivision c on office.subdivisioncd=c.subdivisioncd 
join block_muni on office.blockormuni_cd=block_muni.blockminicd
join district d on personnel.districtcd=d.districtcd
                   
  SET b.p1_personcd = personnel.personcd,b.p1_name = personnel.officer_name,
  b.`p1_designation` =personnel.off_desg,b.`p1_officecd`= a.officecd,
  b.`p1_status`='P1',b.p1_post_stat='1st Polling Officer',b.p1_mobno =personnel.mob_no,b.p1_officename =  office.office,b.`p1_officeaddress`= office.address1,
  b.p1_postoffice=office.postoffice,b.p1_pincode=office.pin, b.p1_subdivision=office.subdivisioncd,
  b.p1_block=office.blockormuni_cd,b.p1_subdivision=c.subdivision,
  b.block_muni_cd=office.blockormuni_cd,b.block_muni_name=block_muni.blockmuni,b.p1_district = d.district 
WHERE  a.booked = 'R'  and a.poststat = 'P1' and b.party_reserve='R' and  a.forzone=", "'", forzone, "'"); 
   
PREPARE stmt FROM @query3; 
EXECUTE stmt; 
DEALLOCATE PREPARE stmt; 

SET 
  @query4 = CONCAT("
UPDATE second_rand_table b
JOIN ", tbl," a  ON b.assembly=a.forassembly and b.groupid=a.groupid
                   and b.per_poststat=a.poststat
    and b.pers_off=a.officecd
JOIN personnel ON personnel.personcd=a.personcd 
JOIN office ON b.pers_off = office.officecd 
join subdivision c on office.subdivisioncd=c.subdivisioncd 
join block_muni on office.blockormuni_cd=block_muni.blockminicd
join district d on personnel.districtcd=d.districtcd
                   

SET b.p2_personcd = personnel.personcd,b.p2_name = personnel.officer_name,
  b.`p2_designation` =personnel.off_desg,b.`p2_officecd`= a.officecd,
  b.`p2_status`='P2',b.p2_post_stat='2nd Polling Officer',b.p2_mobno =personnel.mob_no,b.p2_officename =  office.office,b.`p2_officeaddress`= office.address1,
  b.p2_postoffice=office.postoffice,b.p2_pincode=office.pin, b.p2_subdivision=office.subdivisioncd,
  b.p2_block=office.blockormuni_cd,b.p2_subdivision=c.subdivision,
  b.block_muni_cd=office.blockormuni_cd,b.block_muni_name=block_muni.blockmuni,b.p2_district = d.district  
WHERE  a.booked = 'R'  and a.poststat = 'P2' and b.party_reserve='R' and  a.forzone=", "'", forzone, "'"); 
   
PREPARE stmt FROM @query4; 
EXECUTE stmt; 
DEALLOCATE PREPARE stmt; 


SET 
  @query5 = CONCAT("
UPDATE second_rand_table b
JOIN ", tbl," a  ON b.assembly=a.forassembly and b.groupid=a.groupid
                   and b.per_poststat=a.poststat
    and b.pers_off=a.officecd
JOIN personnel ON personnel.personcd=a.personcd 
JOIN office ON b.pers_off = office.officecd 
join subdivision c on office.subdivisioncd=c.subdivisioncd 
join block_muni on office.blockormuni_cd=block_muni.blockminicd
join district d on personnel.districtcd=d.districtcd
                   
SET b.p3_personcd = personnel.personcd,b.p3_name = personnel.officer_name,
  b.`p3_designation` =personnel.off_desg,b.`p3_officecd`= a.officecd,
  b.`p3_status`='P3',b.p3_post_stat='3rd Polling Officer',b.p3_mobno =personnel.mob_no,b.p3_officename =  office.office,b.`p3_officeaddress`= office.address1,
  b.p3_postoffice=office.postoffice,b.p3_pincode=office.pin, b.p3_subdivision=office.subdivisioncd,
  b.p3_block=office.blockormuni_cd,b.p3_subdivision=c.subdivision,
  b.block_muni_cd=office.blockormuni_cd,b.block_muni_name=block_muni.blockmuni,b.p3_district = d.district  
WHERE  a.booked = 'R'  and a.poststat = 'P3' and b.party_reserve='R' and  a.forzone=", "'", forzone, "'"); 
   
PREPARE stmt FROM @query5; 
EXECUTE stmt; 
DEALLOCATE PREPARE stmt; 

SET 
  @query6 = CONCAT("
UPDATE second_rand_table b
JOIN ", tbl," a  ON b.assembly=a.forassembly and b.groupid=a.groupid
                   and b.per_poststat=a.poststat
    and b.pers_off=a.officecd
JOIN personnel ON personnel.personcd=a.personcd 
JOIN office ON b.pers_off = office.officecd 
join subdivision c on office.subdivisioncd=c.subdivisioncd 
join block_muni on office.blockormuni_cd=block_muni.blockminicd
join district d on personnel.districtcd=d.districtcd
                   
SET b.pa_personcd = personnel.personcd,b.pa_name = personnel.officer_name,
  b.`pa_designation` =personnel.off_desg,b.`pa_officecd`= a.officecd,
  b.`pa_status`='PA',b.pa_post_stat='Addl. 2nd Polling Officer-1',b.pa_mobno =personnel.mob_no,b.pa_officename =  office.office,b.`pa_officeaddress`= office.address1,
  b.pa_postoffice=office.postoffice,b.pa_pincode=office.pin, b.pa_subdivision=office.subdivisioncd,
  b.pa_block=office.blockormuni_cd,b.pa_subdivision=c.subdivision,
  b.block_muni_cd=office.blockormuni_cd,b.block_muni_name=block_muni.blockmuni,b.pa_district = d.district 
WHERE  a.booked = 'R'  and a.poststat = 'PA' and b.party_reserve='R' and  a.forzone=", "'", forzone, "'"); 
   
PREPARE stmt FROM @query6; 
EXECUTE stmt; 
DEALLOCATE PREPARE stmt; 

SET 
  @query7 = CONCAT("
UPDATE second_rand_table b
JOIN ", tbl," a  ON b.assembly=a.forassembly and b.groupid=a.groupid
                   and b.per_poststat=a.poststat
    and b.pers_off=a.officecd
JOIN personnel ON personnel.personcd=a.personcd 
JOIN office ON b.pers_off = office.officecd 
join subdivision c on office.subdivisioncd=c.subdivisioncd 
join block_muni on office.blockormuni_cd=block_muni.blockminicd
join district d on personnel.districtcd=d.districtcd
                   
SET b.pb_personcd = personnel.personcd,b.pb_name = personnel.officer_name,
  b.`pb_designation` =personnel.off_desg,b.`pb_officecd`= a.officecd,
  b.`pb_status`='PB',b.pb_post_stat='Addl. 2nd Polling Officer-2',b.pb_mobno =personnel.mob_no,b.pb_officename =  office.office,b.`pb_officeaddress`= office.address1,
  b.pb_postoffice=office.postoffice,b.pb_pincode=office.pin, b.pb_subdivision=office.subdivisioncd,
  b.pb_block=office.blockormuni_cd,b.pb_subdivision=c.subdivision,
  b.block_muni_cd=office.blockormuni_cd,b.block_muni_name=block_muni.blockmuni,b.pb_district = d.district 
WHERE  a.booked = 'R'  and a.poststat = 'PB' and b.party_reserve='R' and  a.forzone=", "'", forzone, "'"); 
   
PREPARE stmt FROM @query7; 
EXECUTE stmt; 
DEALLOCATE PREPARE stmt;

SET 
  @query14 = CONCAT("update second_rand_table a join assembly b on a.assembly=b.assemblycd 
 set a.assembly_name=b.assemblyname where a.party_reserve='R' and a.forzone=", "'", forzone, "'"); 
   
PREPARE stmt FROM @query14; 
EXECUTE stmt; 
DEALLOCATE PREPARE stmt;

SET 
  @query15 = CONCAT("UPDATE second_rand_table JOIN dcrcmaster  ON second_rand_table.dcrcgrp=dcrcmaster.dcrcgrp 
SET second_rand_table.dc_venue = dcrcmaster.dc_venue, second_rand_table.dc_address = dcrcmaster.dc_address,second_rand_table.rc_venue = dcrcmaster.rc_venue ,second_rand_table.dc_time=dcrcmaster.dc_time, second_rand_table.dc_date=DATE(dcrcmaster.dc_date) 
where second_rand_table.party_reserve='R' and second_rand_table.forzone=", "'", forzone, "'"); 
   
PREPARE stmt FROM @query15; 
EXECUTE stmt; 
DEALLOCATE PREPARE stmt;


SET 
  @query19 = CONCAT("update second_rand_table a 
Inner join assembly on assembly.assemblycd=a.assembly Inner join pc b on assembly.pccd=b.pccd
set a.pcname=b.pcname,a.pccd=b.pccd where a.party_reserve='R' and a.forzone=", "'", forzone, "'"); 
   
PREPARE stmt FROM @query19; 
EXECUTE stmt; 
DEALLOCATE PREPARE stmt;

SET 
  @query191 = CONCAT("update ", tbl," a
set a.2ndTrainingSch=null where a.booked='R' and a.forzone=", "'", forzone, "'"); 
   
PREPARE stmt FROM @query191; 
EXECUTE stmt; 
DEALLOCATE PREPARE stmt;

SET 
  @query192 = CONCAT("update second_rand_table a 
Inner join second_training_schedule on second_training_schedule.assembly=a.assembly 
Inner join second_training_venue on second_training_schedule.tr_venue_cd=second_training_venue.venue_cd
Inner join second_training_subvenue on  second_training_schedule.tr_subvenue_cd=second_training_subvenue.subvenue_cd                     
Inner join second_training_date_time on  second_training_schedule.datetimecd=second_training_date_time.datetime_cd  
                     
set a.traingcode=second_training_schedule.schedule_code,a.training_venue=second_training_venue.venuename,
    a.venuecode=second_training_schedule.tr_venue_cd,a.sub_venuename=second_training_subvenue.subvenue,
   a.venue_addr=second_training_venue.venueaddress,a.training_date=second_training_date_time.training_dt,
   a.training_time=second_training_date_time.training_time
where second_training_schedule.party_reserve='R' 
and a.groupid>=second_training_schedule.group_from and a.groupid<=second_training_schedule.group_to and a.party_reserve='R' and a.forzone=", "'", forzone, "'"); 
   
PREPARE stmt FROM @query192; 
EXECUTE stmt; 
DEALLOCATE PREPARE stmt;

SET 
  @query193 = CONCAT("update ", tbl," a
 Inner join second_training_schedule on second_training_schedule.assembly=a.forassembly                    
set a.2ndTrainingSch=second_training_schedule.schedule_code 
  where second_training_schedule.party_reserve='R' 
and a.groupid>=second_training_schedule.group_from and a.groupid<=second_training_schedule.group_to and  a.booked='R' and a.forzone=", "'", forzone, "'"); 
   
PREPARE stmt FROM @query193; 
EXECUTE stmt; 
DEALLOCATE PREPARE stmt;

SET 
  @query20 = CONCAT("update second_rand_table a join poll_table b on a.pccd=b.pc set a.polldate=b.poll_date, a.polltime=b.poll_time  where a.party_reserve='R' and a.forzone=", "'", forzone, "'"); 
   
PREPARE stmt FROM @query20; 
EXECUTE stmt; 
DEALLOCATE PREPARE stmt;

SET 
  @query21 = CONCAT("update second_rand_table a join district b on substr(a.`forzone`,1,2)=b.districtcd set a.district=b.district where a.party_reserve='R' and a.forzone=", "'", forzone, "'"); 
   
PREPARE stmt FROM @query21; 
EXECUTE stmt; 
DEALLOCATE PREPARE stmt;

SET @p0=forzone;
CALL second_rand_r_slno(@p0);

END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`ppds`@`%` PROCEDURE `second_rand_r_slno`(IN `forzone` CHAR(4))
    NO SQL
BEGIN
SET 
  @query = CONCAT("Update second_rand_table set slno=0 where party_reserve='R' and forzone=", "'", forzone, "'"); 
   
PREPARE stmt FROM @query; 
EXECUTE stmt; 
DEALLOCATE PREPARE stmt; 


SET @id = 0;
UPDATE second_rand_table 
  SET  slno = @id:= @id + 1 
where second_rand_table.forzone=forzone and party_reserve='R'
order by second_rand_table.block_muni_cd,
second_rand_table.pers_off;

SET @idCount="";
SET @idCount = (Select count(*) as cnt from second_rand_table WHERE second_rand_table.forzone=forzone and party_reserve='R');
select @idCount as t_Count;

END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`ppds`@`%` PROCEDURE `second_replace_party`(IN `poststat` CHAR(2), IN `booked` CHAR(1), IN `assembly` CHAR(3), IN `groupid` CHAR(4), IN `tbl` VARCHAR(12))
    NO SQL
BEGIN 
  SET @query2 = "";
  SET @query3 = "";
  
  SET @query = Concat("UPDATE second_rand_table b JOIN ", tbl," a  ON b.assembly=a.forassembly 
        and b.groupid=a.groupid and b.per_poststat=a.poststat
       SET b.pers_off=a.officecd 
       WHERE a.booked =", "'", booked, "' and a.poststat =", "'", poststat, "' and b.per_poststat=", "'", poststat, "' and b.party_reserve=", "'", booked, "' and a.forassembly=", "'", assembly, "' and a.groupid=", "'", groupid, "'");
 
       PREPARE stmt FROM @query; 
       EXECUTE stmt; 
       DEALLOCATE PREPARE stmt; 
       
       
  IF poststat = 'PR' THEN 
      
     SET @query2 = CONCAT("UPDATE second_rand_table b
    JOIN ", tbl," a  ON b.assembly=a.forassembly 
        and b.groupid=a.groupid 
    JOIN personnel ON personnel.personcd=a.personcd
    join district d on personnel.districtcd=d.districtcd

      SET b.pr_personcd = personnel.personcd,b.pr_name = personnel.officer_name,
      b.`pr_designation` =personnel.off_desg,b.`pr_officecd`= a.officecd,
      b.pr_mobno = personnel.mob_no,
      b.pr_district = d.district                
    WHERE a.booked =", "'", booked, "' and a.poststat = ", "'", poststat, "' and b.party_reserve=", "'", booked, "' and a.forassembly=", "'", assembly, "' and a.groupid=", "'", groupid, "'"); 
      
       SET @query3 = CONCAT("UPDATE second_rand_table b
JOIN office ON b.pr_officecd = office.officecd 
join subdivision c on office.subdivisioncd=c.subdivisioncd 
join block_muni on office.blockormuni_cd=block_muni.blockminicd                  
  SET 
  b.pr_officename =  office.office,b.`pr_officeaddress`= office.address1,
  b.pr_postoffice=office.postoffice,b.pr_pincode=office.pin,
  b.pr_block=office.blockormuni_cd,b.pr_subdivision=c.subdivision,
  b.block_muni_cd=office.blockormuni_cd,b.block_muni_name=block_muni.blockmuni                
WHERE  b.`pr_status`=", "'", poststat, "' and b.party_reserve=", "'", booked, "' and b.assembly=", "'", assembly, "' and b.groupid=", "'", groupid, "'");

  END IF;
   
  IF poststat = 'P1' THEN 
      SET @query2 = CONCAT("UPDATE second_rand_table b
    JOIN ", tbl," a  ON b.assembly=a.forassembly 
        and b.groupid=a.groupid 
    JOIN personnel ON personnel.personcd=a.personcd
    join district d on personnel.districtcd=d.districtcd

      SET b.p1_personcd = personnel.personcd,b.p1_name = personnel.officer_name,
      b.`p1_designation` =personnel.off_desg,b.`p1_officecd`= a.officecd,
      b.p1_mobno = personnel.mob_no,
      b.p1_district = d.district                
    WHERE a.booked =", "'", booked, "' and a.poststat = ", "'", poststat, "' and b.party_reserve=", "'", booked, "' and a.forassembly=", "'", assembly, "' and a.groupid=", "'", groupid, "'"); 
      
     SET @query3 = CONCAT("UPDATE second_rand_table b
JOIN office ON b.p1_officecd = office.officecd 
join subdivision c on office.subdivisioncd=c.subdivisioncd 
join block_muni on office.blockormuni_cd=block_muni.blockminicd                  
  SET 
  b.p1_officename =  office.office,b.`p1_officeaddress`= office.address1,
  b.p1_postoffice=office.postoffice,b.p1_pincode=office.pin, 
  b.p1_block=office.blockormuni_cd,b.p1_subdivision=c.subdivision,
  b.block_muni_cd=office.blockormuni_cd,b.block_muni_name=block_muni.blockmuni                
WHERE  b.`p1_status`=", "'", poststat, "' and b.party_reserve=", "'", booked, "' and b.assembly=", "'", assembly, "' and b.groupid=", "'", groupid, "'");
    
 END IF;
 
  IF poststat = 'P2' THEN 
      SET @query2 = CONCAT("UPDATE second_rand_table b
    JOIN ", tbl," a  ON b.assembly=a.forassembly 
        and b.groupid=a.groupid 
    JOIN personnel ON personnel.personcd=a.personcd
    join district d on personnel.districtcd=d.districtcd

      SET b.p2_personcd = personnel.personcd,b.p2_name = personnel.officer_name,
      b.`p2_designation` =personnel.off_desg,b.`p2_officecd`= a.officecd,
      b.p2_mobno = personnel.mob_no,
      b.p2_district = d.district                
    WHERE a.booked =", "'", booked, "' and a.poststat = ", "'", poststat, "' and b.party_reserve=", "'", booked, "' and a.forassembly=", "'", assembly, "' and a.groupid=", "'", groupid, "'"); 
      
     SET @query3 = CONCAT("UPDATE second_rand_table b
JOIN office ON b.p2_officecd = office.officecd 
join subdivision c on office.subdivisioncd=c.subdivisioncd 
join block_muni on office.blockormuni_cd=block_muni.blockminicd                  
  SET 
  b.p2_officename =  office.office,b.`p2_officeaddress`= office.address1,
  b.p2_postoffice=office.postoffice,b.p2_pincode=office.pin, 
  b.p2_block=office.blockormuni_cd,b.p2_subdivision=c.subdivision,
  b.block_muni_cd=office.blockormuni_cd,b.block_muni_name=block_muni.blockmuni                
WHERE  b.`p2_status`=", "'", poststat, "' and b.party_reserve=", "'", booked, "' and b.assembly=", "'", assembly, "' and b.groupid=", "'", groupid, "'");
    
 END IF;
 
  IF poststat = 'P3' THEN 
      SET @query2 = CONCAT("UPDATE second_rand_table b
    JOIN ", tbl," a  ON b.assembly=a.forassembly 
        and b.groupid=a.groupid 
    JOIN personnel ON personnel.personcd=a.personcd
    join district d on personnel.districtcd=d.districtcd

      SET b.p3_personcd = personnel.personcd,b.p3_name = personnel.officer_name,
      b.`p3_designation` =personnel.off_desg,b.`p3_officecd`= a.officecd,
      b.p3_mobno = personnel.mob_no,
      b.p3_district = d.district                
    WHERE a.booked =", "'", booked, "' and a.poststat = ", "'", poststat, "' and b.party_reserve=", "'", booked, "' and a.forassembly=", "'", assembly, "' and a.groupid=", "'", groupid, "'"); 
      
     SET @query3 = CONCAT("UPDATE second_rand_table b
JOIN office ON b.p3_officecd = office.officecd 
join subdivision c on office.subdivisioncd=c.subdivisioncd 
join block_muni on office.blockormuni_cd=block_muni.blockminicd                  
  SET 
  b.p3_officename =  office.office,b.`p3_officeaddress`= office.address1,
  b.p3_postoffice=office.postoffice,b.p3_pincode=office.pin, 
  b.p3_block=office.blockormuni_cd,b.p3_subdivision=c.subdivision,
  b.block_muni_cd=office.blockormuni_cd,b.block_muni_name=block_muni.blockmuni                
WHERE  b.`p3_status`=", "'", poststat, "' and b.party_reserve=", "'", booked, "' and b.assembly=", "'", assembly, "' and b.groupid=", "'", groupid, "'");
    
 END IF;
 
 IF poststat = 'PA' THEN 
      SET @query2 = CONCAT("UPDATE second_rand_table b
    JOIN ", tbl," a  ON b.assembly=a.forassembly 
        and b.groupid=a.groupid 
    JOIN personnel ON personnel.personcd=a.personcd
    join district d on personnel.districtcd=d.districtcd

      SET b.pa_personcd = personnel.personcd,b.pa_name = personnel.officer_name,
      b.`pa_designation` =personnel.off_desg,b.`pa_officecd`= a.officecd,
      b.pa_mobno = personnel.mob_no,
      b.pa_district = d.district                
    WHERE a.booked =", "'", booked, "' and a.poststat = ", "'", poststat, "' and b.party_reserve=", "'", booked, "' and a.forassembly=", "'", assembly, "' and a.groupid=", "'", groupid, "'"); 
      
     SET @query3 = CONCAT("UPDATE second_rand_table b
JOIN office ON b.pa_officecd = office.officecd 
join subdivision c on office.subdivisioncd=c.subdivisioncd 
join block_muni on office.blockormuni_cd=block_muni.blockminicd                  
  SET 
  b.pa_officename =  office.office,b.`pa_officeaddress`= office.address1,
  b.pa_postoffice=office.postoffice,b.pa_pincode=office.pin, 
  b.pa_block=office.blockormuni_cd,b.pa_subdivision=c.subdivision,
  b.block_muni_cd=office.blockormuni_cd,b.block_muni_name=block_muni.blockmuni                
WHERE  b.`pa_status`=", "'", poststat, "' and b.party_reserve=", "'", booked, "' and b.assembly=", "'", assembly, "' and b.groupid=", "'", groupid, "'");
    
 END IF;
 
  IF poststat = 'PB' THEN 
      SET @query2 = CONCAT("UPDATE second_rand_table b
    JOIN ", tbl," a  ON b.assembly=a.forassembly 
        and b.groupid=a.groupid 
    JOIN personnel ON personnel.personcd=a.personcd
    join district d on personnel.districtcd=d.districtcd

      SET b.pb_personcd = personnel.personcd,b.pb_name = personnel.officer_name,
      b.`pb_designation`= personnel.off_desg,b.`pb_officecd`= a.officecd,
      b.pb_mobno = personnel.mob_no,
      b.pb_district = d.district                
    WHERE a.booked =", "'", booked, "' and a.poststat = ", "'", poststat, "' and b.party_reserve=", "'", booked, "' and a.forassembly=", "'", assembly, "' and a.groupid=", "'", groupid, "'"); 
      
     SET @query3 = CONCAT("UPDATE second_rand_table b
JOIN office ON b.pb_officecd = office.officecd 
join subdivision c on office.subdivisioncd=c.subdivisioncd 
join block_muni on office.blockormuni_cd=block_muni.blockminicd                  
  SET 
  b.pb_officename =  office.office,b.`pb_officeaddress`= office.address1,
  b.pb_postoffice=office.postoffice,b.pb_pincode=office.pin, 
  b.pb_block=office.blockormuni_cd,b.pb_subdivision=c.subdivision,
  b.block_muni_cd=office.blockormuni_cd,b.block_muni_name=block_muni.blockmuni                
WHERE  b.`pb_status`=", "'", poststat, "' and b.party_reserve=", "'", booked, "' and b.assembly=", "'", assembly, "' and b.groupid=", "'", groupid, "'");
    
 END IF;
 
    PREPARE stmt FROM @query2; 
    EXECUTE stmt; 
    DEALLOCATE PREPARE stmt; 
    
    PREPARE stmt FROM @query3; 
    EXECUTE stmt; 
    DEALLOCATE PREPARE stmt; 
    
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`ppds`@`%` PROCEDURE `second_replace_reserve`(IN `poststat` CHAR(2), IN `booked` CHAR(1), IN `assembly` CHAR(3), IN `groupid` CHAR(4), IN `tbl` VARCHAR(12))
    NO SQL
BEGIN 
  SET @query2 = "";
  
  SET @query = Concat("UPDATE second_rand_table b JOIN ", tbl," a  ON b.assembly=a.forassembly 
        and b.groupid=a.groupid and b.per_poststat=a.poststat
       SET b.pers_off=a.officecd 
       WHERE a.booked =", "'", booked, "' and a.poststat =", "'", poststat, "' and b.per_poststat=", "'", poststat, "' and b.party_reserve=", "'", booked, "' and a.forassembly=", "'", assembly, "' and a.groupid=", "'", groupid, "'");
 
       PREPARE stmt FROM @query; 
       EXECUTE stmt; 
       DEALLOCATE PREPARE stmt; 
       
IF poststat = 'PR' THEN 
  SET @query2 = CONCAT("UPDATE second_rand_table b
            JOIN ", tbl," a  ON b.assembly=a.forassembly 
                and b.groupid=a.groupid and b.per_poststat=a.poststat
                and b.pers_off=a.officecd
            JOIN personnel ON personnel.personcd=a.personcd
            JOIN office ON b.pers_off = office.officecd 
            join subdivision c on office.subdivisioncd=c.subdivisioncd 
            join block_muni on office.blockormuni_cd=block_muni.blockminicd
            join district d on personnel.districtcd=d.districtcd

              SET b.pr_personcd = personnel.personcd,b.pr_name = personnel.officer_name,
              b.`pr_designation` =personnel.off_desg,b.`pr_officecd`= a.officecd,
              b.`pr_status`='PR',b.pr_post_stat='Presiding Officer',b.pr_mobno = personnel.mob_no,
              b.pr_officename =  office.office,b.`pr_officeaddress`= office.address1,
              b.pr_postoffice=office.postoffice,b.pr_pincode=office.pin, b.pr_subdivision=office.subdivisioncd,
              b.pr_block=office.blockormuni_cd,b.pr_subdivision=c.subdivision,
              b.block_muni_cd=office.blockormuni_cd,b.block_muni_name=block_muni.blockmuni,b.pr_district = d.district                
            WHERE a.booked = 'R'  and a.poststat = 'PR' and b.party_reserve='R' and b.assembly=", "'", assembly, "' and b.groupid=", "'", groupid, "'");    
       
END IF;

IF poststat = 'P1' THEN 
      SET @query2 = CONCAT("UPDATE second_rand_table b
            JOIN ", tbl," a  ON b.assembly=a.forassembly and b.groupid=a.groupid
                               and b.per_poststat=a.poststat
                and b.pers_off=a.officecd
            JOIN personnel ON personnel.personcd=a.personcd 
            JOIN office ON b.pers_off = office.officecd 
            join subdivision c on office.subdivisioncd=c.subdivisioncd 
            join block_muni on office.blockormuni_cd=block_muni.blockminicd
            join district d on personnel.districtcd=d.districtcd

              SET b.p1_personcd = personnel.personcd,b.p1_name = personnel.officer_name,
              b.`p1_designation` =personnel.off_desg,b.`p1_officecd`= a.officecd,
              b.`p1_status`='P1',b.p1_post_stat='1st Polling Officer',b.p1_mobno =personnel.mob_no,b.p1_officename =  office.office,b.`p1_officeaddress`= concat(office.address1,',',office.address2),
              b.p1_postoffice=office.postoffice,b.p1_pincode=office.pin, b.p1_subdivision=office.subdivisioncd,
              b.p1_block=office.blockormuni_cd,b.p1_subdivision=c.subdivision,
              b.block_muni_cd=office.blockormuni_cd,b.block_muni_name=block_muni.blockmuni,b.p1_district = d.district 
            WHERE  a.booked = 'R'  and a.poststat = 'P1' and b.party_reserve='R' and b.assembly=", "'", assembly, "' and b.groupid=", "'", groupid, "'"); 
      
    
 END IF;
 
  IF poststat = 'P2' THEN 
      SET @query2 = CONCAT("UPDATE second_rand_table b
            JOIN ", tbl," a  ON b.assembly=a.forassembly and b.groupid=a.groupid
                               and b.per_poststat=a.poststat
                and b.pers_off=a.officecd
            JOIN personnel ON personnel.personcd=a.personcd 
            JOIN office ON b.pers_off = office.officecd 
            join subdivision c on office.subdivisioncd=c.subdivisioncd 
            join block_muni on office.blockormuni_cd=block_muni.blockminicd
            join district d on personnel.districtcd=d.districtcd

            SET b.p2_personcd = personnel.personcd,b.p2_name = personnel.officer_name,
              b.`p2_designation` =personnel.off_desg,b.`p2_officecd`= a.officecd,
              b.`p2_status`='P2',b.p2_post_stat='2nd Polling Officer',b.p2_mobno =personnel.mob_no,b.p2_officename =  office.office,b.`p2_officeaddress`= office.address1,
              b.p2_postoffice=office.postoffice,b.p2_pincode=office.pin, b.p2_subdivision=office.subdivisioncd,
              b.p2_block=office.blockormuni_cd,b.p2_subdivision=c.subdivision,
              b.block_muni_cd=office.blockormuni_cd,b.block_muni_name=block_muni.blockmuni,b.p2_district = d.district  
            WHERE  a.booked = 'R'  and a.poststat = 'P2' and b.party_reserve='R'  and b.assembly=", "'", assembly, "' and b.groupid=", "'", groupid, "'");
    
 END IF;
 
  IF poststat = 'P3' THEN 
      SET @query2 = CONCAT("UPDATE second_rand_table b
        JOIN ", tbl," a  ON b.assembly=a.forassembly and b.groupid=a.groupid
                           and b.per_poststat=a.poststat
            and b.pers_off=a.officecd
        JOIN personnel ON personnel.personcd=a.personcd 
        JOIN office ON b.pers_off = office.officecd 
        join subdivision c on office.subdivisioncd=c.subdivisioncd 
        join block_muni on office.blockormuni_cd=block_muni.blockminicd
        join district d on personnel.districtcd=d.districtcd

        SET b.p3_personcd = personnel.personcd,b.p3_name = personnel.officer_name,
          b.`p3_designation` =personnel.off_desg,b.`p3_officecd`= a.officecd,
          b.`p3_status`='P3',b.p3_post_stat='3rd Polling Officer',b.p3_mobno =personnel.mob_no,b.p3_officename =  office.office,b.`p3_officeaddress`= office.address1,
          b.p3_postoffice=office.postoffice,b.p3_pincode=office.pin, b.p3_subdivision=office.subdivisioncd,
          b.p3_block=office.blockormuni_cd,b.p3_subdivision=c.subdivision,
          b.block_muni_cd=office.blockormuni_cd,b.block_muni_name=block_muni.blockmuni,b.p3_district = d.district  
        WHERE  a.booked = 'R'  and a.poststat = 'P3' and b.party_reserve='R'  and b.assembly=", "'", assembly, "' and b.groupid=", "'", groupid, "'"); 
    
 END IF;
 
 IF poststat = 'PA' THEN 
      SET @query2 = CONCAT("UPDATE second_rand_table b
        JOIN ", tbl," a  ON b.assembly=a.forassembly and b.groupid=a.groupid
                           and b.per_poststat=a.poststat
            and b.pers_off=a.officecd
        JOIN personnel ON personnel.personcd=a.personcd 
        JOIN office ON b.pers_off = office.officecd 
        join subdivision c on office.subdivisioncd=c.subdivisioncd 
        join block_muni on office.blockormuni_cd=block_muni.blockminicd
        join district d on personnel.districtcd=d.districtcd

        SET b.pa_personcd = personnel.personcd,b.pa_name = personnel.officer_name,
          b.`pa_designation` =personnel.off_desg,b.`pa_officecd`= a.officecd,
          b.`pa_status`='PA',b.pa_post_stat='Addl. 2nd Polling Officer-1',b.pa_mobno =personnel.mob_no,b.pa_officename =  office.office,b.`pa_officeaddress`= office.address1,
          b.pa_postoffice=office.postoffice,b.pa_pincode=office.pin, b.pa_subdivision=office.subdivisioncd,
          b.pa_block=office.blockormuni_cd,b.pa_subdivision=c.subdivision,
          b.block_muni_cd=office.blockormuni_cd,b.block_muni_name=block_muni.blockmuni,b.pa_district = d.district 
        WHERE  a.booked = 'R'  and a.poststat = 'PA' and b.party_reserve='R' and b.assembly=", "'", assembly, "' and b.groupid=", "'", groupid, "'"); 
    
 END IF;
 
  IF poststat = 'PB' THEN 
      SET @query2 = CONCAT("UPDATE second_rand_table b
        JOIN ", tbl," a  ON b.assembly=a.forassembly and b.groupid=a.groupid
                           and b.per_poststat=a.poststat
            and b.pers_off=a.officecd
        JOIN personnel ON personnel.personcd=a.personcd 
        JOIN office ON b.pers_off = office.officecd 
        join subdivision c on office.subdivisioncd=c.subdivisioncd 
        join block_muni on office.blockormuni_cd=block_muni.blockminicd
        join district d on personnel.districtcd=d.districtcd

        SET b.pb_personcd = personnel.personcd,b.pb_name = personnel.officer_name,
          b.`pb_designation` =personnel.off_desg,b.`pb_officecd`= a.officecd,
          b.`pb_status`='PB',b.pb_post_stat='Addl. 2nd Polling Officer-2',b.pb_mobno =personnel.mob_no,b.pb_officename =  office.office,b.`pb_officeaddress`= office.address1,
          b.pb_postoffice=office.postoffice,b.pb_pincode=office.pin, b.pb_subdivision=office.subdivisioncd,
          b.pb_block=office.blockormuni_cd,b.pb_subdivision=c.subdivision,
          b.block_muni_cd=office.blockormuni_cd,b.block_muni_name=block_muni.blockmuni,b.pb_district = d.district 
        WHERE  a.booked = 'R'  and a.poststat = 'PB' and b.party_reserve='R' and b.assembly=", "'", assembly, "' and b.groupid=", "'", groupid, "'"); 
    
 END IF;
 
    PREPARE stmt FROM @query2; 
    EXECUTE stmt; 
    DEALLOCATE PREPARE stmt;    

    
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`ppds`@`%` PROCEDURE `stmt_deallocate_p1_2`(IN `dist` CHAR(2))
    NO SQL
    DETERMINISTIC
BEGIN
deallocate prepare stmtp1_n_2;
deallocate prepare  stmt_p1_u_2;
deallocate prepare  stmt_p1_r_2;


END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`ppds`@`%` PROCEDURE `stmt_deallocate_p2_2`(IN `dist` CHAR(2))
    NO SQL
    DETERMINISTIC
BEGIN

deallocate prepare stmtp2_n_2;
deallocate prepare  stmt_p2_u_2;
deallocate prepare  stmt_p2_r_2;


END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`ppds`@`%` PROCEDURE `stmt_deallocate_p3_2`(IN `dist` CHAR(2))
    NO SQL
    DETERMINISTIC
BEGIN

deallocate prepare stmtp3_n_2;
deallocate prepare  stmt_p3_u_2;
deallocate prepare  stmt_p3_r_2;
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`ppds`@`%` PROCEDURE `stmt_deallocate_pa_2`(IN `dist` CHAR(2))
    NO SQL
    DETERMINISTIC
BEGIN

deallocate prepare stmtpa_n_2;
deallocate prepare  stmt_pa_u_2;
deallocate prepare  stmt_pa_r_2;

END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`ppds`@`%` PROCEDURE `stmt_deallocate_pr_2`(IN `dist` CHAR(2))
    NO SQL
    DETERMINISTIC
BEGIN
deallocate prepare  stmtpr_n_2;
deallocate prepare stmt_pr_r_ba_2;
deallocate prepare stmt_pr_u_ba_2;



END$$
DELIMITER ;
