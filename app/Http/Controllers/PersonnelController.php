<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Personnel;
use App\Office;
use App\tbl_assembly;
use App\Language;
use App\Qualification;
use App\Remark;
use App\Poststatus;
use Session;
use Validator;
use App\tbl_office;

use DB;
use Illuminate\Support\Facades\Input;

class PersonnelController extends Controller
{
    //
     
    public function getAllOffice(){
        $dist = \Session::get('districtcd_ppds');
        $officeData= Office::where('districtcd','=',$dist)->groupBy('officecd')->orderBy('office')->pluck('office','officecd')->all();
        $dataOf = array('officeData' => $officeData);
       // print_r($data);die;
        return view('Personnel_list',compact('dataOf'));

    }
    public function getPersonnelByid(Request $request){
        
        // $data=Personnel::where('personcd',$request->edit_code)->get();
        $data=Personnel::leftjoin('personnela'.session()->get('districtcd_ppds'),'personnel.personcd','=','personnela'.session()->get('districtcd_ppds').'.personcd')->where('personnel.personcd',$request->edit_code)->select('personnel.*','personnela'.session()->get('districtcd_ppds').'.booked')->get();
        //dd($data);
        //$dataOf=$this->getAllOffice();
        // $poststatus = Poststatus::get();
        // dd($poststatus);
        return view('Personnel_edit',['person' => $data,'ac'=> tbl_assembly::where('districtcd',session()->get('districtcd_ppds'))->get(),'language'=> Language::get(),'qualification'=> Qualification::get(),'remark'=> Remark::get(),'poststatus'=> Poststatus::get()]);

// return view('Personnel_edit',compact('data'))->with('personnelDataEdit',$personnelDataEdit);
        }  
    public function getPersonnelByOffice(Request $request){
   
         $response = [];
        $perm = null;
        $statusCode = 200;
        $users = array(); //Should be changed #4
        $search_val = array();
        // $this->validate($request, [
        //     'search.*' => 'nullable|regex:/^[A-Za-z0-9\s]+$/i',
        //     'draw' => 'required|integer',
        //     'start' => 'required|integer',
        //     'length' => 'required|integer',
           
        //     'personnel_id'=>'nullable'
        //         ], [
        //     'search.*.regex' => 'Special Charecters not allowed',
        //     'draw.required' => 'Draw is required',
        //     'start.integer' => 'Start must be an integer',
        //     'start.required' => 'Start is required',
        //     'length.integer' => 'Length must be an integer',
        //     'length.required' => 'Length is required',
        //     // 'personnel_id.integer'=>'Personnel Id must be an integer',
        //              //'personnel_id.max'=>'Personnel Id may not be greater than 11 characters'
        // ]);
        try {
           // dd($request->officer_id);
            $draw = $request->draw;
            $offset = $request->start;
            $length = $request->length;
            $search = $request->search ["value"];
            $order = $request->order;
          
            $office_id = $request->office_id;
            
            $personnel_id = $request->personnel_id;
             $officer_id = $request->officer_id;
            //  dd($officer_id);
          // echo $officer_id;die;
            $Personnel = Personnel::all();
            
          
             $filtered=Personnel::select('personcd', 'officer_name', 'off_desg', 'mob_no', 'officecd')
                    
                    ->orderBy('personcd')
                    ->where(function($q) use ($search) {
                $q->orwhere('personcd', 'like', '%' . $search . '%')
                ->orwhere('officer_name', 'like', '%' . $search . '%')
                ->orwhere('off_desg', 'like', '%' . $search . '%')
                ->orwhere('mob_no', 'like', '%' . $search . '%')
                ->orwhere('officecd', 'like', '%' . $search . '%');
            });
            
            if ($personnel_id != '') {
                    $filtered = $filtered->where('personcd', '=', $personnel_id);
                }
               
           if ($office_id != '') {
                    $filtered = $filtered->where('officecd','=',$office_id);
                }
                if ($officer_id != '') {
                    $filtered = $filtered->where('personcd','=',$officer_id);
                }
            $ordered = $filtered;
            $filtered_count = $filtered->count();
            //echo count ( $order );die;
            for ($i = 0; $i < count($order); $i ++) {
                $ordered = $ordered->orderBy($request->columns [$order [$i] ['column']] ['data'], strtoupper($order [$i] ['dir']));
            }
            $page_displayed = $ordered->offset($offset)->limit($length)->get();
            // dd($page_displayed[0]);

            $dist = \Session::get('districtcd_ppds');

            $data = array();
            if (!empty($page_displayed)) {
                foreach ($page_displayed as $dcrc_mas) {
                    //$cat_arr = config('constants.USER_CATEGORY');
                    // dd($dcrc_mas->personcd);
                    if ($dist < 10) {  
                        $value = str_pad($dcrc_mas->personcd, 11, "0", STR_PAD_LEFT);
                    }
                    else{
                        $value =$dcrc_mas->personcd;
                    }
                    //$personnelResult['personcd'] = $searchPersonnel["personcd"];
    
                    $nestedData['personcd'] = $value;






                    // $nestedData['personcd'] = $dcrc_mas->personcd;
                    $nestedData['officer_name'] = $dcrc_mas->officer_name;
                    $nestedData['off_desg'] = $dcrc_mas->off_desg;
                    $nestedData['mob_no'] = $dcrc_mas->mob_no;
                    $nestedData['officecd'] = $dcrc_mas->officecd;
                 
                   
                  
                    $edit_button = $delete_button = $nestedData['personcd'] ;

                    $nestedData['action'] = array('e' => $edit_button, 'd' => $delete_button);
                    $data[] = $nestedData;
                }
            }
            $response = array(
                "draw" => $draw,
                "recordsTotal" => $Personnel->count(), //Should be changed #7
                "recordsFiltered" => $filtered_count,
                'personnel_masters' => $data //Should be changed #8
            );
        } catch (\Exception $e) {
            $response = array(
                'exception' => true,
                'exception_message' => $e->getMessage()
            );
            $statusCode = 400;
        } finally {
            return response()->json($response, $statusCode);
        }
        
    }   
        
        
        
        
        
        
        
        
        
        
   
    
    
    public function update(Request $request)
    {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        }
        else
        { 
            $validator =Validator::make($request->all(),[
             'officer_name' => 'required|string|max:50',
             'off_desg' => 'required|string|max:50',
             'resi_no'=> 'max:15',
             'present_addr1' => 'required|string|max:100',
             'perm_addr1' => 'required|string|max:100',
             'dateofbirth' => 'required|date',
             'gender' => 'required',
             'scale' => 'required|max:15',
             'basic_pay' => 'required|numeric|max:9999999',
             'grade_pay' => 'required|numeric',
             'pgroup' => 'required',
             'workingstatus' => 'required',
             'mob_no' => 'required|digits:10',
             'email' => 'required|email',
             'qualificationcd' => 'required',
             'languagecd' => 'required',
             'epic' => 'nullable|max:20',
             'partno' => 'numeric|max:9999',
             'slno' => 'numeric|max:9999',
             'assembly_temp' => 'required|numeric',
             'assembly_off' => 'required|numeric',
             'assembly_perm' => 'required|numeric',
             'branch_ifsc' => 'required|max:11',
             //'bank_acc_no' => 'required|unique:personnel,bank_acc_no|max:16'



         ]);
        try{
            // dd($request->booked);
         //   echo 'hi';die;
      
//if($validator->fails()) {
//			return redirect()->back()->withErrors($validator)->withInput();
//          }
    $personnelId=$request->personnel_id;
    // dd($personnelId);

    $dist = \Session::get('districtcd_ppds');

    if ($dist < 10) {  
        $value = str_pad($personnelId, 11, "0", STR_PAD_LEFT);
    }
    else{
        $value =$personnelId;
    }
    //$personnelResult['personcd'] = $searchPersonnel["personcd"];

    $personCd1 = $value;
    // dd($personCd1);

        $personnel =array();

       
        $personnel['officer_name']= strip_tags($request->officer_name,'');
        $personnel['off_desg']= strip_tags($request->off_desg,'');
        $personnel['qualificationcd']= strip_tags($request->qualificationcd,'');
        $personnel['languagecd']= strip_tags($request->languagecd,'');
        $personnel['dateofbirth']= strip_tags($request->dateofbirth,'');
        $personnel['gender']= strip_tags($request->gender,'');

        $personnel['scale']= strip_tags($request->scale,'');
        $personnel['basic_pay']= strip_tags($request->basic_pay,'');
        $personnel['grade_pay']= strip_tags($request->grade_pay,'');
        $personnel['workingstatus']= strip_tags($request->workingstatus,'');
        $personnel['pgroup']= strip_tags($request->pgroup,'');

        $personnel['email']= strip_tags($request->email,'');
        $personnel['resi_no']= strip_tags($request->resi_no,'');
        $personnel['mob_no']= strip_tags($request->mob_no,'');
        $personnel['present_addr1']=  strip_tags($request->present_addr1,'');
        $personnel['perm_addr1']=strip_tags($request->perm_addr1,'');
        $personnel['present_addr2']=  strip_tags($request->present_addr2,'');
        $personnel['perm_addr2']=strip_tags($request->perm_addr2,'');

        $personnel['epic']= strip_tags($request->epic,'');
        $personnel['partno']= strip_tags($request->partno,'');
        $personnel['slno']= strip_tags($request->slno,'');
        $personnel['poststat']= strip_tags($request->poststat,'');
        $personnel['assembly_off']= strip_tags($request->assembly_off,'');
        $personnel['assembly_perm']= strip_tags($request->assembly_perm,'');
        $personnel['assembly_temp']= strip_tags($request->assembly_temp,'');

        $personnel['branch_ifsc']= strip_tags($request->branch_ifsc,'');
        $personnel['bank_acc_no']= strip_tags($request->bank_acc_no,'');
        $personnel['remarks']=strip_tags($request->remarks,'');
       
        // $status_Id = $request->poststat;
        // dd($status_Id);
        if($request->poststat == 'PR'){
            $poststatus = 'Presiding Officer';
        }
        if($request->poststat == 'P1'){
            $poststatus = '1st Polling Officer';
        }

        if($request->poststat == 'P2'){
            $poststatus = '2nd Polling Officer';
        }
        if($request->poststat == 'P3'){
            $poststatus = '3rd Polling Officer';
        }
        // if($request->poststat == 'P1'){
        //     $poststatus = '';
        // }
        
       
		
	//	$personnela =array();
      //  $personnela['gender']= strip_tags($request->gender,'');
      //  $personnela['poststat']= strip_tags($request->poststat,'');
       // $personnela['assembly_off']= strip_tags($request->assembly_off,'');
       // $personnela['assembly_perm']= strip_tags($request->assembly_perm,'');
       // $personnela['assembly_temp']= strip_tags($request->assembly_temp,'');
        // echo '<hr>';
        // print_r($personnel);
        // die();
        //Update personnel
       // dd(strip_tags($request->booked,''));
       $updatePersoneel= DB::table('personnel')->where('personcd', $personCd1)->update($personnel);
		
       $updatePersoneel2 = DB::table('personnela'.session()->get('districtcd_ppds'))->where('personcd', $personCd1)
                            ->update([
                                'poststat'=>strip_tags($request->poststat,''),
                                'assembly_temp'=>strip_tags($request->assembly_temp,''),
                                'assembly_off'=>strip_tags($request->assembly_off,''),
                                'assembly_perm'=>strip_tags($request->assembly_perm,'')
                                ]);

    // if ($request->booked==true) {
    //     //echo 1;die;
    //     $updatePersoneel2 = DB::table('personnela'.session()->get('districtcd_ppds'))->where('personcd', $personCd1)
    //     ->update([
    //         'poststat'=>strip_tags($request->poststat,''),
    //         'assembly_temp'=>strip_tags($request->assembly_temp,''),
    //         'assembly_off'=>strip_tags($request->assembly_off,''),
    //         'assembly_perm'=>strip_tags($request->assembly_perm,''),
    //         'booked' => ''
    //         ]);
    // }

    // else {
    //     $updatePersoneel2 = DB::table('personnela'.session()->get('districtcd_ppds'))->where('personcd', $personCd1)
    //     ->update([
    //         'poststat'=>strip_tags($request->poststat,''),
    //         'assembly_temp'=>strip_tags($request->assembly_temp,''),
    //         'assembly_off'=>strip_tags($request->assembly_off,''),
    //         'assembly_perm'=>strip_tags($request->assembly_perm,'')
    //         ]);
    // }


       $updatePersoneel1 = DB::table('first_rand_table')->where('personcd',$personCd1)->update([
           'person_desig' => strip_tags($request->off_desg,''),
           'poststat' => strip_tags($request->poststat,''),
           'poststatus'=> $poststatus,
           'officer_name'=> strip_tags($request->officer_name,''),
           'mob_no' => strip_tags($request->mob_no,''),
           'ifsc' => strip_tags($request->branch_ifsc,''),
           'bank_accno' => strip_tags($request->bank_acc_no,''),
          

           ]);
       $person_first_rand_tbl = DB::table('first_rand_table')->where('personcd',$personCd1)
                                ->select('training_desc','venuename','sub_venuename','venueaddress','training_dt','training_time')->get();
        // dd($person_first_rand_tbl);
       //Update personnela
	//$updatePersoneel=DB::table('personnela'.session()->get('districtcd_ppds'))->where('personcd', $personnelId)->update($personnela);
        //return redirect('edit_personnel/'.$personnelId.'?save=1');
    
        $response=array('updatePersoneel'=>$updatePersoneel,
                        // 'updatePersoneel2'=>$updatePersoneel2,
                        'updatePersoneel1'=>$updatePersoneel1,
                        'person_first_rand_tbl'=>$person_first_rand_tbl,
            'status'=>1);      
                
}  catch (\Exception $e) {
            $response = array(
                'exception' => true,
                'exception_message' => $e->getMessage()
            );
            $statusCode = 400;
        } finally {
            return response()->json($response, $statusCode);
        }
        }
        
} 
    public function add(){
        return view('Personnel_add',['offices'=>Office::where('districtcd',session()->get('districtcd_ppds'))->get(),'ac'=> tbl_assembly::where('districtcd',session()->get('districtcd_ppds'))->get(),'language'=> Language::get(),'qualification'=> Qualification::get(),'remark'=> Remark::get(),'poststatus'=> Poststatus::get()]);
    }
    public function store(Request $request)
    {
		//  $statusCode = 200;
		//echo '<pre>';
	    //print_r($request->all());
	  //  dd($request->all());

        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        }








	
        $id = DB::select('SELECT MAX(CAST(SUBSTR(personcd,-5) AS UNSIGNED)) AS MaxID FROM personnel WHERE subdivisioncd = ?',[substr($request->office_id,0,4)]);
        //dd($id);
        $id = $id[0]->MaxID;
        // echo $id;die;
        if(is_null($id)){
            $id = substr($request->office_id,0,6).'00001';
        }
        else{
            $id = substr($request->office_id,0,6).str_pad($id+1,5,"0",STR_PAD_LEFT);
        }


        $office_id=$request->office_id;
        // dd($office_id);
       if($id!='' ){
        //    echo 1;die;

        
        $this->validate($request, [
            'officer_name' => 'required|string|max:50',
             'off_desg' => 'required|string|max:50',
             'resi_no'=> 'max:15',
             'present_addr1' => 'string|max:100',
             'perm_addr1' => 'string|max:100',
             'dateofbirth' => 'required|date_format:Y-m-d',
             'gender' => 'required',
             'scale' => 'required|max:15',
             'basic_pay' => 'required|max:9999999',
             'grade_pay' => 'required',
             'pgroup' => 'required',
             'workingstatus' => 'required',
             'mob_no' => 'required',
             'email' => 'nullable|email',
             'qualificationcd' => 'required',
             'languagecd' => 'required',
             'epic' => 'nullable|max:20',
             'partno' => 'nullable|max:9999',
             'slno' => 'nullable|max:9999',
             'assembly_temp' => 'required',
             'assembly_off' => 'required',
             'assembly_perm' => 'required',
           
             'branch_ifsc' => 'required|max:15',
             'bank_acc_no' => 'required|unique:personnel,bank_acc_no|max:16'
                ], [
             
        ]);
     
try{
    $dist = \Session::get('districtcd_ppds');
        $personnel =array();
        $user_code = session()->get('code_ppds');
        $personnel['personcd'] = $id;
        $personnel['officecd'] = strip_tags($request->office_id,'');
        $personnel['officer_name']= strip_tags($request->officer_name,'');
        $personnel['off_desg']= strip_tags($request->off_desg,'');
        //$personnel['aadhaar']= strip_tags($request->aadhaar,'');
        $personnel['qualificationcd']= strip_tags($request->qualificationcd,'');
        $personnel['languagecd']= strip_tags($request->languagecd,'');
        $personnel['dateofbirth']= strip_tags($request->dateofbirth,'');
        $personnel['gender']= strip_tags($request->gender,'');

        $personnel['scale']= strip_tags($request->scale,'');
        $personnel['basic_pay']= strip_tags($request->basic_pay,'');
        $personnel['grade_pay']= strip_tags($request->grade_pay,'');
        $personnel['workingstatus']= strip_tags($request->workingstatus,'');
        $personnel['pgroup']= strip_tags($request->pgroup,'');

        $personnel['email']= strip_tags($request->email,'');
        $personnel['resi_no']= strip_tags($request->resi_no,'');
        $personnel['mob_no']= strip_tags($request->mob_no,'');
        $personnel['present_addr1']=  strip_tags($request->present_addr1,'');
        $personnel['perm_addr1']=strip_tags($request->perm_addr1,'');
        $personnel['present_addr2']=  strip_tags($request->present_addr2,'');
        $personnel['perm_addr2']=strip_tags($request->perm_addr2,'');
        // $personnel['block_muni_temp_id']= strip_tags($request->block_muni_temp_id,'');
        // $personnel['block_muni_perm_id']=  strip_tags($request->block_muni_perm_id,'');
        // $personnel['block_muni_off_id']=  strip_tags($request->block_muni_off_id,'');

        $personnel['epic']= strip_tags($request->epic,'');
        $personnel['partno']= strip_tags($request->partno,'');
        $personnel['slno']= strip_tags($request->slno,'');
        $personnel['poststat']= strip_tags($request->poststat,'');
        $personnel['assembly_off']= strip_tags($request->assembly_off,'');
        $personnel['assembly_perm']= strip_tags($request->assembly_perm,'');
        $personnel['assembly_temp']= strip_tags($request->assembly_temp,'');

        //$personnel['post_office_account']= strip_tags($request->post_office_account,'');
        $personnel['branch_ifsc']= strip_tags($request->branch_ifsc,'');
        $personnel['bank_acc_no']= strip_tags($request->bank_acc_no,'');
        $personnel['remarks']=strip_tags($request->remarks,'');
    
        $personnel['districtcd']= substr($request->office_id,0,2);
        $personnel['subdivisioncd']= substr($request->office_id,0,4);
         $personnel['fordistrict']=  $dist;
        $personnel['usercode']=$user_code;
      //  $personnel['remark_reason']= strip_tags($request->remark_reason,'');
       // $personnel['pay_level']= strip_tags($request->pay_level,'');
       // $personnel['updated_at']=date('Y-m-d H:i:s');
    //echo '=============================================';
	//print_r($personnel);
	//exit();

        $insertData=DB::table('personnel')->insert($personnel);
        $response=array('options'=>$insertData,'office_id_code'=>$office_id,'status'=>1);
        }catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        
        
      //  return redirect('personnel_add?id='.$id.'office'.$office_id.'&save=1');
       }

    }
    
    
    public function officeSearch(Request $request) {
        // echo "hi";
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            $term = trim($request->q);
            // echo $term;die;
            $dist = \Session::get('districtcd_ppds');
            $validator1 = \Validator::make(compact('term'), [
                        'term' => 'required|regex:/(^[-0-9A-Za-z().,&\/ ]+$)/',
                            ], [
                        'term.required' => 'Office name must be an alpha numeric characters',
                        'term.regex' => 'Office name contains alphanumeric characters',
            ]);
            $this->validateWith($validator1);
            try {
                $officeResult = [];

                $term = trim($request->q);
            //    echo $term;die;
                if (empty($term)) {
                    return \Response::json([]);
                }
                // $searchOffice = tbl_office::where('office.officecd', 'like', $term . '%')->where('districtcd', '=', $dist)->groupBy('office')->select('office','officecd')->get();
                $searchOffice = tbl_office::where('office.officecd', 'like', $term . '%')->groupBy('office')->select('office','officecd')->get();
                //  dd($searchOffice);
                $nestedData=array();
                foreach ($searchOffice as $searchOffice1) {
                    // dd($searchOffice1["officecd"]);
                    $officeResult['office'] = $searchOffice1["office"];
                    $officeResult['officecd'] = $searchOffice1["officecd"];
                    $nestedData[]=$officeResult;
                }
                $response=array('options'=>$nestedData);
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }
    
    public function personnelIdSearch(Request $request) {
    //    echo "hi";
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            $term = trim($request->q);
            // dd($term);
            $dist = \Session::get('districtcd_ppds');
            $validator1 = \Validator::make(compact('term'), [
                        'term' => 'required|digits_between:1,10',
                            ], [
                        'term.required' => 'Personnel Id must be an alpha numeric characters',
                        'term.digits_between' => 'Personnel Id may not be greater than 10 characters',
            ]);
            $this->validateWith($validator1);
            try {
                $personnelResult = [];

                $term = trim($request->q);
             //   echo $term;die;


             if (empty($term)) {
                return \Response::json([]);
            }
            
            $searchPersonnel = Personnel::where('personnel.personcd', 'like', $term . '%')->where('districtcd', '=', $dist)->groupBy('personcd')->select('personcd')->get();
         
            $dist = \Session::get('districtcd_ppds');
         $nestedData=array();
            foreach ($searchPersonnel as $searchPersonnel) {
                if ($dist < 10) {  
                    $value = str_pad($searchPersonnel["personcd"], 11, "0", STR_PAD_LEFT);
                }
                else{
                    $value =$searchPersonnel["personcd"];
                }
                //$personnelResult['personcd'] = $searchPersonnel["personcd"];

                $personnelResult['personcd'] = $value;
                //$personnelResult['officecd'] = $searchPersonnel["officecd"];
                $nestedData[]=$personnelResult;
            }
            $response=array('options'=>$nestedData);
        













            //     if (empty($term)) {
            //         return \Response::json([]);
            //     }
            //     $searchPersonnel = Personnel::where('personnel.personcd', 'like', $term . '%')->where('districtcd', '=', $dist)->groupBy('personcd')->select('personcd')->get();
            //      dd($searchPersonnel[0]);
            //     $dist = \Session::get('districtcd_ppds');
            //     // dd($dist);
            //     // $int = (int)$dist;
            //     // dd($int);
            //  $nestedData=array();
            //  if($dist=="06"){
            //     //  echo "hi";
            //     // dd($searchPersonnel);
            //     foreach ($searchPersonnel as $searchPersonnel) {
            //         // echo "0".$searchPersonnel["personcd"];die;
            //         // $personnelResult['personcd'] = '0'.$searchPersonnel["personcd"];
                   
            //         // dd($searchPersonnel["personcd"]);
            //         // $personnelResult['personcd'] = $searchPersonnel["personcd"];
            //         //$personnelResult['officecd'] = $searchPersonnel["officecd"];
            //         $nestedData[]=$personnelResult;
            //     }
            //  } else{
            //     //  echo "dhyduyds";
            //     foreach ($searchPersonnel as $searchPersonnel) {
                   
            //         // dd($searchPersonnel["personcd"]);
            //         $personnelResult['personcd'] = $searchPersonnel["personcd"];
            //         //$personnelResult['officecd'] = $searchPersonnel["officecd"];
            //         $nestedData[]=$personnelResult;
            //     }
            //  }
               
                $response=array('options'=>$nestedData);
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }
    
    public function officerSearch(Request $request) {
        //echo"hi";die;
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            $term = trim($request->q);
            $dist = \Session::get('districtcd_ppds');
            $validator1 = \Validator::make(compact('term'), [
                        'term' => 'required|regex:/(^[-0-9A-Za-z().,&\/ ]+$)/',
                            ], [
                        'term.required' => 'Office name must be an alpha numeric characters',
                        'term.regex' => 'Office name contains alphanumeric characters',
            ]);
            $this->validateWith($validator1);
            try {
                $officerResult = [];

                $term = trim($request->q);
             //   echo $term;die;
                if (empty($term)) {
                    return \Response::json([]);
                }
                $searchOfficer = Personnel::where('personnel.officer_name', 'like', $term . '%')->where('districtcd', '=', $dist)->groupBy('personcd')->select('officer_name','personcd')->get();
                // dd($searchOfficer[0]);
         
                //  dd($searchOfficer[0]->personcd);
                $dist = \Session::get('districtcd_ppds');
             $nestedData=array();
                foreach ($searchOfficer as $searchOfficer1) {
                // dd(is_numeric($searchOfficer1->personcd));
                    $officerResult['officer_name'] = $searchOfficer1->officer_name;

                    if ($dist < 10) {  
                        $value = str_pad($searchOfficer1->personcd, 11, "0", STR_PAD_LEFT);
                    }
                    else{
                        $value =$searchOfficer1->personcd;
                    }
                    //$personnelResult['personcd'] = $searchPersonnel["personcd"];
    
                    $officerResult['personcd'] = $value;



                    // $officerResult['personcd'] = $searchOfficer1->personcd;
                    $nestedData[]=$officerResult;
                }
            //    dd($nestedData);
                $response=array('options'=>$nestedData);
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }

    public function assemblysearch(Request $request){
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            $term = trim($request->q);
            // echo $term;die;
            $dist = \Session::get('districtcd_ppds');
            $validator1 = \Validator::make(compact('term'), [
                        'term' => 'required|regex:/(^[-0-9A-Za-z().,&\/ ]+$)/',
                            ], [
                        'term.required' => 'Office name must be an alpha numeric characters',
                        'term.regex' => 'Office name contains alphanumeric characters',
            ]);
            $this->validateWith($validator1);
            try {
                $assemblyResult = [];

                $term = trim($request->q);
           
                if (empty($term)) {
                    return \Response::json([]);
                }
              
                $searchAssembly = tbl_assembly::where(function ($query) use($term){
                    $query->where('assemblycd', 'like', $term . '%')
                          ->orWhere('assemblyname', 'like', $term . '%');
                })
                
                ->groupBy('assemblycd')->select('assemblyname','assemblycd')->get();
              
                $nestedData=array();
                foreach ($searchAssembly as $searchAssembly1) {
                   
                    $assemblyResult['assemblyname'] = $searchAssembly1["assemblyname"];
                    $assemblyResult['assemblycd'] = $searchAssembly1["assemblycd"];
                    $nestedData[]=$assemblyResult;
                }
                $response=array('options'=>$nestedData);
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }



    public function update_revoke_exemption(Request $request)
    {
    //    dd($request->all());
    $statusCode = 200;
    if (!$request->ajax()) {
        $statusCode = 400;
        $response = array('error' => 'Error occured in Ajax Call.');
        return response()->json($response, $statusCode);
    } else {
        try{
           // dd($request->all());
            $personnelId=$request->personeel_id;
          //dd($personnelId);
            $dist = \Session::get('districtcd_ppds');

            if ($dist < 10) {  
                $value = str_pad($personnelId, 11, "0", STR_PAD_LEFT);
            }
            else{
                $value =$personnelId;
            }

            $personCd1 = $value;
  
       $updatePersoneel= DB::table('personnela'.session()->get('districtcd_ppds'))->where('personcd', $personCd1)->update([
           'booked'=>''
       ]);
        $response=array(
                    'updatePersoneel'=>$updatePersoneel,
                    'status'=>2
        );      
                
        }  catch (\Exception $e) {
                    $response = array(
                        'exception' => true,
                        'exception_message' => $e->getMessage()
                    );
                    $statusCode = 400;
                } finally {
                    return response()->json($response, $statusCode);
                }
        }
    }     
}
