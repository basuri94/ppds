<?php

namespace App\Http\Controllers;
use App\tbl_assembly;
use App\tbl_district_priority_ac;
use DB;
use Illuminate\Http\Request;

class SetPriorityACController extends Controller
{
   
    public function getDistrictWisePCdata(Request $request) { 
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        }
        else
        {
            $this->validate($request, [            
            'districtcd' => 'required|alpha_num|min:2|max:2'
            ], [           
            'districtcd.required' => 'Districtcd is required',
            'districtcd.alpha_num' => 'Districtcd must be an alpha numeric characters'
            ]);
            $districtcd=$request->districtcd;
            try{
            $filtered="";
            $filtered = tbl_assembly::join('pc', 'assembly.pccd', '=', 'pc.pccd')    
                ->where('assembly.districtcd', '=', $districtcd)
    		->groupBy('assembly.pccd')
                ->pluck('pc.pcname','pc.pccd')
                ->all();
            
                $response = array(
                   'options' => $filtered,
                   'status' => 1 
                );          
            }catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
              $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        
        }
    }
    public function getDistrictWisePCwiseACdata(Request $request) {
   $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        }
        else
        {
            $this->validate($request, [            
            'districtcd' => 'required|alpha_num|min:2|max:2',
            'pccd' => 'required|alpha_num|min:2|max:2'
            ], [           
            'districtcd.required' => 'Districtcd is required',
            'districtcd.alpha_num' => 'Districtcd must be an alpha numeric characters',
            'pccd.required' => 'PC code is required',
            'pccd.alpha_num' => 'PC code must be an alpha numeric characters'
            ]);
            $districtcd=$request->districtcd;
            $pccd=$request->pccd;
            try{
            $filtered="";
            $filtered = tbl_assembly::where('assembly.districtcd', '=', $districtcd)
                //->where('assembly.pccd', '=', $pccd)
                ->pluck('assemblyname','assemblycd')
                ->all();
            
                $response = array(
                   'options' => $filtered,
                   'status' => 1 
                );          
            }catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
              $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        
        }     
    }
    public function getDistrictACPriorityRecords(Request $request){
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        }
        else
        {
            $this->validate($request, [            
            'districtcd' => 'required|alpha_num|min:2|max:2'
            ], [           
            'districtcd.required' => 'Districtcd is required',
            'districtcd.alpha_num' => 'Districtcd must be an alpha numeric characters'
            ]);
            
            try
            {
              $districtcd=$request->districtcd;
              $forzone = tbl_district_priority_ac::join('pc as pc1', 'district_priority_ac.pc', '=', 'pc1.pccd')
                    ->join('assembly', 'district_priority_ac.ac_priority', '=', 'assembly.assemblycd')
                    ->where('district_priority_ac.districtcd','=', $districtcd)                   
                    ->select('district_priority_ac.pc', 'pc1.pcname as pc_name','district_priority_ac.ac_priority','assembly.assemblyname as ac_prio_name','priority_no')
                    ->orderBy('district_priority_ac.pc')->orderBy('priority_no')->get();
              $zoneRD="";
              $zoAr=json_decode($forzone);
              $zoneRD.="<table class='table table-bordered table-striped1' width='100%' style='border-top: 2px solid #009fe8;'>";
              $zoneRD.="<tr style='background-color: #f5f8fa'>";
              $zoneRD.="<th colspan='5'>List of Records";
              $zoneRD.="</th>";
              $zoneRD.="</tr>";
              $zoneRD.="<tr style='background-color: #f5f8fa'>";
              $zoneRD.="<td width='10%'><b>SL#</b></td><td><b>Name of PC</b></td><td><b>Name of Priority AC </b></td><td><b>Priority No</b></td><td width='10%'><b>Action</b></td>";
              $zoneRD.="</tr>";
              $count=0;
               foreach($zoAr as $fz){
                   $count++;
                   $ucode=$districtcd.$fz->pc.$fz->ac_priority;
                    $zoneRD.="<tr><td>". $count ."</td>";
                    $zoneRD.="<td>".$fz->pc_name ."</td>";
                    $zoneRD.="<td>".$fz->ac_prio_name ."</td>";
                    $zoneRD.="<td>".$fz->priority_no ."</td>";
                  //  $zoneRD.="<td><a title='Edit'  onclick='edit_zone(".json_encode($ucode) .");'><i class='fa fa-pencil-alt' style='color:green;cursor:pointer;' value=".json_encode($fz->pc)."></i></a>&nbsp;&nbsp;";
                    $zoneRD.="<td><a title='Delete'  onclick='delete_priority_pc(".json_encode($ucode) .");'><i class='fa fa-trash-alt' style='color:red;cursor:pointer;' value=".json_encode($ucode)."></i></td></tr>"; 
                } 
             
              $zoneRD.="</table>";
              $response = array(
                   'options' =>  $zoneRD,
                   'status' => 1);          
            }catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
              $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
      
        }
    }
    public function save_priority_ac(Request $request){
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        }
        else
        {
            $this->validate($request, [            
            'districtcd' => 'required|alpha_num|min:2|max:2',
            'pc' => 'required|alpha_num|min:2|max:2',
            'ac_priority' => 'required|alpha_num|min:2|max:3',
            'priority_no' => 'required|digits_between:1,1'
            ], [
            
            'districtcd.required' => 'District is required',
            'districtcd.alpha_num' => 'District must be an alpha numeric',
            'pc.required' => 'PC is required',
            'pc.alpha_num' => 'PC must be an alpha numeric',
            'ac_priority.required' => 'Priority AC is required',
            'ac_priority.alpha_num' => 'Priority AC must be an alpha numeric',
            'priority_no.required' => 'Priority No is required',
            'priority_no.digits_between' => 'Priority No must be integer',
            ]);
            try
            {
                $districtcd=$request->districtcd;
                $pc=$request->pc;
                $ac_priority=$request->ac_priority;
                $priority_no=$request->priority_no;
                $dcPNO=tbl_district_priority_ac::where('districtcd','=', $districtcd)
                        ->where('pc','=', $pc)
                        ->where('priority_no','=', $priority_no)
                        ->value(DB::raw('count(*)'));
                $dcPy=tbl_district_priority_ac::where('districtcd','=', $districtcd)
                        ->where('pc','=', $pc)
                        ->where('ac_priority','=', $ac_priority)
                        ->value(DB::raw('count(*)'));
                if($dcPNO==0){
                    if($dcPy==0){
                        $save_ac = new tbl_district_priority_ac();
                        $save_ac->districtcd = $districtcd;
                        $save_ac->pc = $pc;
                        $save_ac->ac_priority = $ac_priority;
                        $save_ac->priority_no = $priority_no;
                        $save_ac->save();
                        $response = array(
                           'options' => $save_ac,
                           'status' => 1);   
                    }else{
                        $response = array(
                           'options' => $dcPy,
                           'status' => 0); 
                    }
                }else{
                    $response = array(
                       'options' => $dcPNO,
                       'status' => 2); 
                }
            }catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
              $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
      
        }
    }
    public function delete_priority_ac(Request $request) {
        
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        }
        else
        {
           $this->validate($request, [            
            'data' => 'required|alpha_num|min:7|max:7'    
            ], [
            
            'data.required' => 'Code is required',
            'data.alpha_num' => 'Code must be an alpha numeric'
            ]);
           try 
           {
            $data=$request->data;
            $districtcd = substr($data, 0, 2);
            $pc = substr($data, 2, 2);
            $pc_priority = substr($data, 4, 3);
             //if($zoned[0]->cnt==0){
               $tblR = tbl_district_priority_ac::where('districtcd','=', $districtcd)
                        ->where('pc','=', $pc)
                        ->where('ac_priority','=', $pc_priority)
                             ->delete();
               $response = array(
                   'options' => $tblR,
                   'status' => 1);
               
//             }else{
//                 $response = array(
//                   'options' => "Zone exist in assembly_zone table",
//                   'status' => 2);
//             }
            
             
              
             }catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
              $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }  

}
