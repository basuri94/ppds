<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\tbl_assembly_zone;
use App\tbl_personnela;
use App\tbl_assembly_party;
use App\tbl_pollingstation;
use App\tbl_assembly;
use DB;

class ThirdRandController extends Controller {

    //::::::::Third Randomisation(Lock/Unlock):::::::://

    public function thirdzone_wise_assembly(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            $this->validate($request, [
                'zone' => 'required|alpha_num|min:4|max:4',
                'forDist' => 'required|alpha_num|min:2|max:2',
                'pc' => 'nullable|alpha_num|min:2|max:2'
                    ], [
                'zone.required' => 'Zone is required',
                'zone.alpha_num' => 'Zone must be an alpha numeric characters',
                'forDist.required' => 'District is required',
                'forDist.alpha_num' => 'District must be an alpha numeric characters',
                'pc.alpha_num' => 'Pc must be an alpha numeric character'
            ]);
            $zone = $request->zone;
            $forDist = $request->forDist;
            $pccd = $request->pc;
            try {
                $filtered = "";
                $filtered = tbl_assembly_zone::join('assembly', 'assembly.assemblycd', '=', 'assembly_zone.assemblycd')
                        ->where('assembly_zone.districtcd', '=', $forDist)
                        ->where('assembly_zone.rand_status2', '=', 'Y')
                        ->where('assembly_zone.rand_status1', '=', 'Y')
                        ->select('assembly_zone.assemblycd', 'assembly_zone.rand_status1', 'assembly_zone.rand_status2', 'assembly_zone.rand_status3', 'assembly.assemblyname');
                if ($zone != "") {
                    $filtered = $filtered->where('assembly_zone.zone', '=', $zone);
                }
                if ($pccd != "") {
                    $filtered = $filtered->where('assembly.pccd', '=', $pccd);
                }
                $filtered = $filtered->groupBy('assembly_zone.assemblycd')->get();
                /* $data = array();
                  if (!empty($filtered)) {
                  foreach ($filtered as $post) {
                  $nestedData['assemblycd'] = $post->assemblycd;
                  $nestedData['assemblyname'] = $post->assemblyname;
                  $nestedData['rand_status1'] = $post->rand_status1;
                  $nestedData['status'] =  tbl_personnela::where('personnela.forassembly','=',$post->assemblycd)
                  ->where('personnela.forzone','=',$zone)
                  ->select(DB::raw('Count(personnela.personcd) as total'))
                  ->get();
                  $data[]=$nestedData;
                  }
                  } */
                // print_r($data);
                $response = array(
                    'options' => $filtered,
                    'status' => 1
                );
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }

    public function thirdrand_lock_unlock(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            $validate_array = ['zone' => 'required|alpha_num|min:4|max:4', 'districtcd' => 'required|alpha_num|min:2|max:2'];
            for ($x = 0; $x <= $request->row_count; $x++) {
                $validate_array['myCheck' . $x] = 'nullable|alpha_dash';
            }
            $validate_array1 = ['zone.required' => 'Zone is required',
                'zone.alpha_num' => 'Zone must be an alpha numeric characters',
                'districtcd.required' => 'District is required',
                'districtcd.alpha_num' => 'District must be an alpha numeric characters'];
            for ($y = 0; $y <= $request->row_count; $y++) {
                $validate_array1['myCheck' . $y . '.alpha_dash'] = 'Assembly must be an alpha numeric characters';
                //  $validate_array1['myCheck'. $y.'.unique'] = 'Already created some assembly(s) for this zone';
            }
            $this->validate($request, $validate_array, $validate_array1);

            try {
                $row_count = $request->row_count;
                $zone = $request->zone;
//                $filtered0=tbl_assembly_zone::where('assembly_zone.zone','=',$zone)
//                         ->where('assembly_zone.rand_status3','!=','Y')
//                         ->update(['rand_status3'=>'N']);
                $count = 0;
                $totalCount = 0;
                for ($i = 1; $i <= $row_count; $i++) {
                    $count1 = 0;
                    $asmcd = 'myCheck' . $i;
                    if ($request->$asmcd != "") {
                        $asmstat = explode('-', $request->$asmcd);
                        $asmcode = $asmstat[0];
                        $asmstatus = $asmstat[1];
                        if ($asmstatus == "A") {
                            $randstat = 'Y';
                        } else if ($asmstatus == "Y") {
                            $randstat = 'N';
                        } else if ($asmstatus == "N") {
                            $count1++;
                            $totalCount++;
                        }
                        if ($count1 == 0) {
                            tbl_assembly_zone::where('assembly_zone.assemblycd', '=', $asmcode)
                                    ->where('assembly_zone.zone', '=', $zone)
                                    ->update(['rand_status3' => $randstat]);
                            $count++;
                        }
                    }
                }
                $response = array(
                    'options' => $count,
                    'totalC' => $totalCount,
                    'status' => 1
                );
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }

    //:::::::::::::::::Third Rand:::::::::::::::://
    public function third_rand_zone_wise_assembly(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            $this->validate($request, [
                'zone' => 'required|alpha_num|min:4|max:4',
                'forDist' => 'required|alpha_num|min:2|max:2',
                'pc' => 'nullable|alpha_num|min:2|max:2'
                    ], [
                'zone.required' => 'Zone is required',
                'zone.alpha_num' => 'Zone must be an alpha numeric characters',
                'forDist.required' => 'District is required',
                'forDist.alpha_num' => 'District must be an alpha numeric characters',
                'pc.alpha_num' => 'PC must be an alpha numeric characters'
            ]);
            $zone = $request->zone;
            $forDist = $request->forDist;
            $pccd = $request->pc;
            try {
                $filtered = "";
                $filtered = tbl_assembly_zone::join('assembly', 'assembly.assemblycd', '=', 'assembly_zone.assemblycd')
                        ->where('assembly_zone.districtcd', '=', $forDist)
                        ->where('assembly_zone.zone', '=', $zone)
                        ->where('assembly_zone.rand_status3', '!=', 'Y')
                        ->where('assembly_zone.rand_status1', '=', 'Y')
                        ->where('assembly_zone.rand_status2', '=', 'Y')
                        ->select('assembly_zone.assemblycd', 'assembly_zone.rand_status1', 'assembly_zone.rand_status2', 'assembly_zone.rand_status3', 'assembly.assemblyname');

                if ($pccd != "") {
                    $filtered = $filtered->where('assembly.pccd', '=', $pccd);
                }
                $filtered = $filtered->get();
                $data = array();
                if (!empty($filtered)) {
                    foreach ($filtered as $post) {
                        $nestedData['assemblycd'] = $post->assemblycd;
                        $nestedData['assemblyname'] = $post->assemblyname;
                        $nestedData['rand_status3'] = $post->rand_status3;

                        $data[] = $nestedData;
                    }
                }
                // print_r($data);
                $response = array(
                    'options' => $data,
                    'status' => 1
                );
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }

    public function thirdrand(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            $validate_array = ['zone' => 'required|alpha_num|min:4|max:4', 'districtcd' => 'required|alpha_num|min:2|max:2', 'pc' => 'required|alpha_num|min:2|max:2'];
            for ($x = 0; $x <= $request->row_count; $x++) {
                $validate_array['myCheck' . $x] = 'nullable|alpha_dash';
            }
            $validate_array1 = ['zone.required' => 'Zone is required',
                'zone.alpha_num' => 'Zone must be an alpha numeric characters',
                'districtcd.required' => 'District is required',
                'districtcd.alpha_num' => 'District must be an alpha numeric characters',
                'pc.required' => 'PC is required',
                'pc.alpha_num' => 'PC must be an alpha numeric characters'];
            for ($y = 0; $y <= $request->row_count; $y++) {
                $validate_array1['myCheck' . $y . '.alpha_dash'] = 'Assembly must be an alpha numeric characters';
                //  $validate_array1['myCheck'. $y.'.unique'] = 'Already created some assembly(s) for this zone';
            }
            $this->validate($request, $validate_array, $validate_array1);

            try {
                $row_count = $request->row_count;
                $zone = $request->zone;
                $districtcd = $request->districtcd;
                $filtered0 = tbl_assembly_zone::where('assembly_zone.zone', '=', $zone)
                        ->where('assembly_zone.rand_status1', '=', 'Y')
                        ->where('assembly_zone.rand_status2', '=', 'Y')
                        ->where('assembly_zone.rand_status3', '!=', 'Y')
                        ->update(['rand_status3' => 'N']);
                $count = 0;
                for ($i = 1; $i <= $row_count; $i++) {
                    $asmcd = 'myCheck' . $i;
                    if ($request->$asmcd != "") {
                        $asmstat = explode('-', $request->$asmcd);
                        $asmcode = $asmstat[0];
                        $asmstatus = $asmstat[1];
                        $randstat = 'A';

                        $filtered = tbl_assembly_zone::where('assembly_zone.assemblycd', '=', $asmcode)
                                ->where('assembly_zone.zone', '=', $zone)
                                ->update(['rand_status3' => $randstat]);
                        $count++;
                    }
                }
                DB::select('call pollingstn_dcrc_add(?,?)', [$zone, $districtcd]);
                // $firstRand=DB::select('call swapping(?)',[$zone]);
                //  $pcount=$personnelCheck[0]->t_Count;
//                  $filteredUp=tbl_assembly_zone::where('assembly_zone.rand_status3','=','A')
//                         ->where('assembly_zone.zone','=',$zone)
//                         ->update(['rand_status3'=>'N']);

                $response = array(
                    'options' => $count,
                    'status' => 1
                );
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }

    //::::::::::::::::::::::: Booth Tagging List :::::::::::::::::::://
    public function getBoothTaggingPDF(Request $request) {
        $this->validate($request, [
            'forZone' => 'required|alpha_num|min:4|max:4',
            'assembly' => 'required|alpha_num|min:3|max:3'
                ], [
            'forZone.required' => 'For Zone is required',
            'forZone.alpha_num' => 'For Zone must be an alpha numeric characters',
            'assembly.required' => 'Assembly is required',
            'assembly.alpha_num' => 'Assembly must be an alpha numeric characters'
        ]);
        $forzone = $request->forZone;
        $assembly = $request->assembly;

        $ApptRecordAssembly = tbl_assembly::where('assemblycd', '=', $assembly)
                ->select('assemblycd', 'assembly.assemblyname')
                ->get();
        foreach ($ApptRecordAssembly as $ApptRecordAssemblyName) {
            $assemblyname = $ApptRecordAssemblyName->assemblyname;
        }
        $session_personnela = session()->get('personnela_ppds');
        $ApptRecord = tbl_pollingstation::
                join($session_personnela, function($join) use($session_personnela) {
                    $join->on($session_personnela . '.groupid', '=', 'pollingstation.groupid');
                    $join->on($session_personnela . '.forassembly', '=', 'pollingstation.forassembly');
                })
                ->join('personnel', '' . $session_personnela . '.personcd', '=', 'personnel.personcd')
                ->where('pollingstation.forassembly', '=', $assembly)
                ->where('pollingstation.forzone', '=', $forzone)
                ->where($session_personnela . '.selected', '=', 1)
                ->where($session_personnela . '.poststat', '=', "PR")
                ->where($session_personnela . '.booked', '=', "P")
                ->select('pollingstation.groupid', 'pollingstation.psname', 'pollingstation.psno', 'pollingstation.psfix', 'personnel.officer_name', 'personnel.off_desg', $session_personnela . '.personcd')
                ->groupby($session_personnela . '.personcd')
              //  ->orderBy('groupid')
                //   ->orderBy('psno')
//                  ->orderBy('psfix')
                        ->orderBy('personnel.officer_name')
                ->get();
        $subV = json_decode($ApptRecord);
        //   print_r($subV);

        $pdf = app('FPDF');
        $pdf->AddPage();
        $pdf->SetTitle('Booth Tagging List');
        $count = 0;
        $per_page = 40;

        $assem = "ASSEMBLY : " . $assembly . " - " . $assemblyname;
        $pdf->SetFont('Arial', 'B', 8.5);
        $pdf->Cell(190, 4, $assem, 0, 0, 'C');
        $pdf->Ln(6);

        $pdf->SetFillColor(255, 255, 255);
        //	$this->SetTextColor(0,0,0);
        $pdf->SetDrawColor(0, 0, 0);
        $pdf->SetLineWidth(.3);
        $pdf->SetFont('Arial', 'B', 6);
        $head = array('MO Details', 'PS No', 'PS Name');
        $w = array(60, 20, 90);
        $pdf->SetFont('Arial', 'B', 9);
        for ($j = 0; $j < count($head); $j++)
            $pdf->Cell($w[$j], 7, $head[$j], 1, 0, 'C', true);

        $pdf->Ln();
        // $slNo=0;
        foreach ($subV as $subTe) {
            if ($count < $per_page) {
                //  $slNo++;
                $fill = false;
                $pdf->SetFont('Arial', '', 7);
                // $pdf->Cell($w[0], 6, $slNo, 'LRT', 0, 'C', $fill);


                $x_axis = $pdf->getx();
                $aa = $subTe->officer_name . ", PIN-" . $subTe->personcd;
                //echo $aa;die;
                $c_height = 6;
                $c_width = $w[0];
                $w_w = $c_height / 3;
                $w_w_1 = $w_w + 2;
                $w_w1 = $w_w + $w_w + $w_w + 3;
// $w_w2=$w_w+$w_w+$w_w+$w_w+3;// for 3 rows wrap
                $text = $aa;
                $len = strlen($text); // check the length of the cell and splits the text into 7 character each and saves in a array 
                if ($len > 38) {
                    $w_text = str_split($text, 38); // splits the text into length of 7 and saves in a array since we need wrap cell of two cell we took $w_text[0], $w_text[1] alone.
// if we need wrap cell of 3 row then we can go for    $w_text[0],$w_text[1],$w_text[2]
                    $pdf->SetX($x_axis);
                    $pdf->Cell($c_width, $w_w_1, $w_text[0], '', '', '');
                    $pdf->SetX($x_axis);
                    $pdf->Cell($c_width, $w_w1, $w_text[1], '', '', '');
//$this->SetX($x_axis);
// $this->Cell($c_width,$w_w2,$w_text[2],'','','');// for 3 rows wrap but increase the $c_height it is very important.
                    $pdf->SetX($x_axis);
                    $pdf->Cell($c_width, $c_height, '', 'LTRB', 0, 'L', 0);
                } else {
                    $pdf->SetX($x_axis);
                    $pdf->Cell($c_width, $c_height, $text, 'LTRB', 0, 'L', 0);
                }

                //$pdf->Cell($w[5], 7, $stud_sub->pr_officename, 'LRT', 0, 'L', $fill);
                ////  $pdf->Cell($w[5], 7, $stud_sub->pr_mobno, 'LRT', 0, 'L', $fill);
                //count1++;
                //$pdf->Cell($w[5], 7,'', 'LRT', 0, 'L', $fill);
                //$pdf->Ln();
                //$pdf->Cell(array_sum($w), 0, '', 1, 0, 'L', $fill);
                //$pdf->Ln();
                //$pdf->Cell($w[3], 6, "Name-".$subTe->pr_name, 'LRT', 0, 'L', $fill);
                //$pdf->Cell(10, 6, ",Designation-".$subTe->pr_designation, 'LRT', 0, 'L', $fill);
//count1++;
                $pdf->Cell($w[1], 6, $subTe->psno . "" . $subTe->psfix, 'LRT', 0, 'C', $fill);
                $pdf->Cell($w[2], 6, $subTe->psname, 'LRT', 0, 'C', $fill);

                $pdf->Ln();
                $pdf->Cell(array_sum($w), 0, '', 1, 0, 'L', $fill);
                $pdf->Ln();

                $fill = !$fill;
                $count++;
            }
            if ($count == $per_page) {
                $per_page = $per_page + 40;
                if ($count != Count($subV)) {
                    $pdf->AddPage();
                    $assem = "ASSEMBLY : " . $assembly . " - " . $assemblyname;
                    $pdf->SetFont('Arial', 'B', 8.5);
                    $pdf->Cell(190, 4, $assem, 0, 0, 'C');
                    $pdf->Ln(6);
                }
            }
        }
        $pdf->Output();
        exit;
    }

    public function getAssemblyDetailsForPsvspolling(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            $this->validate($request, [
                'forZone' => 'required|alpha_num|min:4|max:4'
                    ], [
                'forZone.required' => 'For Zone is required',
                'forZone.alpha_num' => 'For Zone must be an alpha numeric'
            ]);
            try {
                $forZone = $request->forZone;
                $list = '';
                $asmData = tbl_assembly_zone::where('zone', '=', $forZone)
                                ->join('assembly', 'assembly.assemblycd', '=', 'assembly_zone.assemblycd')
                                ->select('assembly_zone.assemblycd', 'assembly.assemblyname')->get();
                $trArray = json_decode($asmData);
                $list .= "<option value>[Select]</option>";
                foreach ($trArray as $trDate) {
                    $list .= "<option value='" . $trDate->assemblycd . "'>" . $trDate->assemblycd . " - " . $trDate->assemblyname . "</option>";
                }
                $response = array(
                    'options' => $list,
                    'status' => 1);
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }

    //::::::::::::::::::::::: Booth Tagging List(PS no vs Polling Party) :::::::::::::::::::://
    public function getBoothTaggingPDFpsvspolling(Request $request) {
        $this->validate($request, [
            'forZone' => 'required|alpha_num|min:4|max:4',
            'assembly' => 'required|alpha_num|min:3|max:3'
                ], [
            'forZone.required' => 'For Zone is required',
            'forZone.alpha_num' => 'For Zone must be an alpha numeric characters',
            'assembly.required' => 'Assembly is required',
            'assembly.alpha_num' => 'Assembly must be an alpha numeric characters'
        ]);
        $forzone = $request->forZone;
        $assembly = $request->assembly;

        $ApptRecordAssembly = tbl_assembly::where('assemblycd', '=', $assembly)
                ->select('assemblycd', 'assembly.assemblyname')
                ->get();
        foreach ($ApptRecordAssembly as $ApptRecordAssemblyName) {
            $assemblyname = $ApptRecordAssemblyName->assemblyname;
        }
        $session_personnela = session()->get('personnela_ppds');
        $ApptRecord = tbl_pollingstation::
                join($session_personnela, function($join) use($session_personnela) {
                    $join->on($session_personnela . '.groupid', '=', 'pollingstation.groupid');
                    $join->on($session_personnela . '.forassembly', '=', 'pollingstation.forassembly');
                })
                ->join('personnel', '' . $session_personnela . '.personcd', '=', 'personnel.personcd')
                ->where('pollingstation.forassembly', '=', $assembly)
                ->where('pollingstation.forzone', '=', $forzone)
                ->where($session_personnela . '.selected', '=', 1)
                ->where($session_personnela . '.poststat', '=', "PR")
                         ->where($session_personnela . '.booked', '=', "P")
                ->select('pollingstation.groupid', 'pollingstation.psname', 'pollingstation.psno', 'pollingstation.psfix', 'personnel.officer_name', 'personnel.off_desg', $session_personnela . '.personcd')
                ->groupby($session_personnela . '.personcd')
              //  ->orderBy('psno')
//                 ->orderBy('groupid')
//                  ->orderBy('psfix')
                        ->orderBy('personnel.officer_name')
                ->get();
        $subV = json_decode($ApptRecord);
        //   print_r($subV);

        $pdf = app('FPDF');
        $pdf->AddPage();
        $pdf->SetTitle('Booth Tagging List');
        $count = 0;
        $per_page = 40;

        $assem = "ASSEMBLY : " . $assembly . " - " . $assemblyname;
        $pdf->SetFont('Arial', 'B', 8.5);
        $pdf->Cell(190, 4, $assem, 0, 0, 'C');
        $pdf->Ln(6);

        $pdf->SetFillColor(255, 255, 255);
        //	$this->SetTextColor(0,0,0);
        $pdf->SetDrawColor(0, 0, 0);
        $pdf->SetLineWidth(.3);
        $pdf->SetFont('Arial', 'B', 6);
         $head = array('MO Details', 'PS No', 'PS Name');
        $w = array(60, 20, 90);
        $pdf->SetFont('Arial', 'B', 9);
        for ($j = 0; $j < count($head); $j++)
            $pdf->Cell($w[$j], 7, $head[$j], 1, 0, 'C', true);

        $pdf->Ln();
       // $slNo = 0;
        foreach ($subV as $subTe) {
            if ($count < $per_page) {
               // $slNo++;
                $fill = false;
                $pdf->SetFont('Arial', '', 7);
               // $pdf->Cell($w[0], 6, $slNo, 'LRT', 0, 'C', $fill);
               

                $x_axis = $pdf->getx();
                $aa = "Name-" . $subTe->officer_name . ", PIN-" . $subTe->personcd;
                //echo $aa;die;
                $c_height = 6;
                $c_width = $w[0];
                $w_w = $c_height / 3;
                $w_w_1 = $w_w + 2;
                $w_w1 = $w_w + $w_w + $w_w + 3;
// $w_w2=$w_w+$w_w+$w_w+$w_w+3;// for 3 rows wrap
                $text = $aa;
                $len = strlen($text); // check the length of the cell and splits the text into 7 character each and saves in a array 
                if ($len > 38) {
                    $w_text = str_split($text, 38); // splits the text into length of 7 and saves in a array since we need wrap cell of two cell we took $w_text[0], $w_text[1] alone.
// if we need wrap cell of 3 row then we can go for    $w_text[0],$w_text[1],$w_text[2]
                    $pdf->SetX($x_axis);
                    $pdf->Cell($c_width, $w_w_1, $w_text[0], '', '', '');
                    $pdf->SetX($x_axis);
                    $pdf->Cell($c_width, $w_w1, $w_text[1], '', '', '');
//$this->SetX($x_axis);
// $this->Cell($c_width,$w_w2,$w_text[2],'','','');// for 3 rows wrap but increase the $c_height it is very important.
                    $pdf->SetX($x_axis);
                    $pdf->Cell($c_width, $c_height, '', 'LTRB', 0, 'L', 0);
                } else {
                    $pdf->SetX($x_axis);
                    $pdf->Cell($c_width, $c_height, $text, 'LTRB', 0, 'L', 0);
                }
                // $fill = false;
//                $pdf->SetFont('Arial', '', 7);
//                
//                $pdf->Cell($w[0], 6, $subTe->psno . "" . $subTe->psfix, 'LRT', 0, 'C', $fill);
//                $pdf->Cell($w[1], 6, $subTe->groupid, 'LRT', 0, 'C', $fill);
//                $pdf->Cell($w[2], 6, $subTe->psname, 'LRT', 0, 'L', $fill);
                //count1++;
                 $pdf->Cell($w[1], 6, $subTe->psno . "" . $subTe->psfix, 'LRT', 0, 'C', $fill);
                $pdf->Cell($w[2], 6, $subTe->psname, 'LRT', 0, 'C', $fill);
                $pdf->Ln();
                $pdf->Cell(array_sum($w), 0, '', 1, 0, 'L', $fill);
                $pdf->Ln();

                $fill = !$fill;
                $count++;
            }
            if ($count == $per_page) {
                $per_page = $per_page + 40;
                if ($count != Count($subV)) {
                    $pdf->AddPage();
                    $assem = "ASSEMBLY : " . $assembly . " - " . $assemblyname;
                    $pdf->SetFont('Arial', 'B', 8.5);
                    $pdf->Cell(190, 4, $assem, 0, 0, 'C');
                    $pdf->Ln(6);
                }
            }
        }
        $pdf->Output();
        exit;
    }

}
