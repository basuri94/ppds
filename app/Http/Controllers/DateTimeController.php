<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\tbl_first_training_date_time;
use DB;
use App\tbl_first_training_schedule;
use App\tbl_second_training_date_time;
use App\tbl_second_training_schedule;

class DateTimeController extends Controller {

    public function first_training_date_entry(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            $this->validate($request, [
                'districtcd' => 'required|alpha_num|min:2|max:2',
                'first_train_dt' => 'required|date_format:d/m/Y|min:10|max:10',
                'first_train_tm' => 'required|regex:/^[A-Za-z0-9.\s-]+$/|max:15'
                    ], [
                'districtcd.required' => 'District is required',
                'districtcd.alpha_num' => 'District must be an alpha numeric',
                'first_train_dt.required' => ' Date is required',
                'first_train_dt.date_format' => 'Date format should be d/m/y',
                'first_train_tm.required' => ' Time is required',
                'first_train_tm.regex' => 'Time  must be an alpha numeric',
                'first_train_tm.max' => 'Time must not be greater than 15 characters'
            ]);
            try {
                $forDist = $request->districtcd;
                $first_train_dt = $request->first_train_dt;
                $first_train_tm = $request->first_train_tm;
                $user_code = session()->get("code_ppds");
                $tbl_first_train_da_tm = new tbl_first_training_date_time();
                $max_datetime_code = $tbl_first_train_da_tm->where('districtcd', '=', $forDist)
                        ->max('datetime_cd');

                if ($max_datetime_code == "") {
                    $dateTimeCode = $forDist . "01";
                } else {
                    $tmp_code = 100 + substr($max_datetime_code, -2) + 1;

                    $dateTimeCode = $forDist . substr($tmp_code, -2);
                }

                $save_first_dt_tm = new tbl_first_training_date_time();
                $save_first_dt_tm->districtcd = $forDist;
                $save_first_dt_tm->datetime_cd = $dateTimeCode;
                $save_first_dt_tm->training_dt = date('Y-m-d', strtotime(trim(str_replace('/', '-', $first_train_dt))));
                $save_first_dt_tm->training_time = $first_train_tm;
                $save_first_dt_tm->usercode = $user_code;

                $save_first_dt_tm->save();
                $response = array(
                    'options' => $save_first_dt_tm,
                    'status' => 1);
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }

    public function getFirstTraningDateRecords(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            $this->validate($request, [
                'forDist' => 'required|alpha_num|min:2|max:2'
                    ], [
                'forDist.required' => 'District is required',
                'forDist.alpha_num' => 'District code must be an alpha numeric'
            ]);
            try {
                $forDist = $request->forDist;
                $forDateTime = tbl_first_training_date_time::where('districtcd', '=', $forDist)
                                ->select('training_dt', 'training_time', 'datetime_cd')
                                ->orderBy('datetime_cd')->get();
                //print_r($forDateTime);die;
                $DateTime = "";
                $zoAr = json_decode($forDateTime);
                //  print_r($zoAr);die;
                $DateTime .= "<table class='table table-bordered table-striped1' width='100%' style='border-top: 2px solid #009fe8;'>";
                $DateTime .= "<tr style='background-color: #f5f8fa'>";
                $DateTime .= "<th colspan='4'><span class='highlight'>List of First Training date & time</span>";
                $DateTime .= "</th>";
                $DateTime .= "</tr>";
                $DateTime .= "<tr style='background-color: #f5f8fa'>";
                $DateTime .= "<td width='10%'><b>SL#</b></td><td><b>Training Date</b></td><td><b>Training Time </b></td><td width='10%'><b>Action</b></td>";
                $DateTime .= "</tr>";
                $count = 0;
                foreach ($zoAr as $fz) {
                    $count++;
                    $DateTime .= "<tr><td>" . $count . "</td>";
                    $DateTime .= "<td>" . date('d/m/Y', strtotime(trim(str_replace('/', '-', $fz->training_dt)))) . "</td>";
                    $DateTime .= "<td>" . $fz->training_time . "</td>";
                    $DateTime .= "<td><a title='Edit'  onclick='edit_first(" . json_encode($fz->datetime_cd) . ");'><i class='fa fa-pencil-alt' style='color:green;cursor:pointer;' value=" . json_encode($fz->datetime_cd) . "></i></a>&nbsp;&nbsp;";
                    $DateTime .= "<a title='Delete'  onclick='delete_first(" . json_encode($fz->datetime_cd) . ");'><i class='fa fa-trash-alt' style='color:red;cursor:pointer;' value=" . json_encode($fz->datetime_cd) . "></i></td></tr>";
                }

                $DateTime .= "</table>";
                $response = array(
                    'options' => $DateTime,
                    'status' => 1);
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }

    public function first_date_time_edit(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            $this->validate($request, [
                'date_time_cd' => 'required|alpha_num|min:4|max:4'
                    ], [
                'date_time_cd.required' => 'Date time code is required',
                'date_time_cd.alpha_num' => 'Date time code must be an alpha numeric'
            ]);
            try {
                $date_time_cd = $request->date_time_cd;
                $First_date_time_show = tbl_first_training_date_time::select(DB::raw("(DATE_FORMAT(first_training_date_time.training_dt,'%d/%m/%Y')) as trai_date"), 'training_time', 'datetime_cd')
                                ->where('datetime_cd', '=', $date_time_cd)->get();
                $response = array(
                    'options' => $First_date_time_show,
                    'status' => 1);
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }

    public function first_date_time_delete(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            $this->validate($request, [
                'date_time_cd' => 'required|alpha_num|min:4|max:4'
                    ], [
                'date_time_cd.required' => 'Date time code is required',
                'date_time_cd.alpha_num' => 'Date time code must be an alpha numeric'
            ]);
            try {
                $date_time_cd = $request->date_time_cd;

                $tbl_first_training_date_time_chking = tbl_first_training_schedule::select('datetimecd')
                        ->where('datetimecd', '=', $date_time_cd);
                $tbl_first_training_date_time_chking_count = $tbl_first_training_date_time_chking->count();

                if ($tbl_first_training_date_time_chking_count == 0) {
                    $record = tbl_first_training_date_time::where('datetime_cd', '=', $date_time_cd); //Should be changed #27

                    if (!empty($record)) {//Should be changed #30
                        $record = $record->delete();
                    }

                    $response = array(
                        'record' => $record, //Should be changed #32
                        'status' => 1
                    );
                } else {
                    $response = array(
                        'options' => "Record exist in another table, you cannot delete this record",
                        'status' => 2);
                }
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }

    public function update_first_date_time_delete(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            $this->validate($request, [
                'districtcd' => 'required|alpha_num|min:2|max:2',
                'first_train_dt' => 'required|date_format:d/m/Y|min:10|max:10',
                'first_train_tm' => 'required|regex:/^[A-Za-z0-9.\s-]+$/|max:15',
                'first_training_date_time_code' => 'required|alpha_num|min:4|max:4'
                    ], [
                'districtcd.required' => 'District is required',
                'districtcd.alpha_num' => 'District must be an alpha numeric',
                'first_train_dt.required' => ' Date is required',
                'first_train_dt.date_format' => 'Date format should be d/m/y',
                'first_train_tm.required' => ' Time is required',
                'first_train_tm.regex' => 'Time must be an alpha numeric',
                'first_train_tm.max' => 'Time must not be greater than 15 characters',
                'first_training_date_time_code.required' => 'Code is required',
                'first_training_date_time_code.alpha_num' => 'Code should be an alpha numeric',
            ]);
            try {
                $forDist = $request->districtcd;

                $user_code = session()->get("code_ppds");
                $save_first_dt_tm = new tbl_first_training_date_time();

                $datetimecd = $request->first_training_date_time_code;
                $save_first_dt_tm->training_dt = date('Y-m-d', strtotime(trim(str_replace('/', '-', $request->first_train_dt))));
                $save_first_dt_tm->training_time = $request->first_train_tm;


                $upadtez = tbl_first_training_date_time::where('datetime_cd', '=', $datetimecd)
                        ->update(['training_dt' => $save_first_dt_tm->training_dt, 'training_time' => $save_first_dt_tm->training_time, 'usercode' => $user_code]);

                $response = array(
                    'options' => $upadtez,
                    'status' => 2);
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }

    public function second_training_date_entry(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            $this->validate($request, [
                'districtcd' => 'required|alpha_num|min:2|max:2',
                'second_train_dt' => 'required|date_format:d/m/Y|min:10|max:10',
                'second_train_tm' => 'required|regex:/^[A-Za-z0-9.\s-]+$/|max:15'
                    ], [
                'districtcd.required' => 'District is required',
                'districtcd.alpha_num' => 'District must be an alpha numeric',
                'second_train_dt.required' => ' Date is required',
                'second_train_dt.date_format' => 'Date format should be d/m/y',
                'second_train_tm.required' => ' Time is required',
                'second_train_tm.regex' => 'Time  must be an alpha numeric',
                'second_train_tm.max' => 'Time must not be greater than 15 characters'
            ]);
            try {
                $forDist = $request->districtcd;
                $second_train_dt = $request->second_train_dt;
                $second_train_tm = $request->second_train_tm;
                $user_code = session()->get("code_ppds");
                $tbl_second_train_da_tm = new tbl_second_training_date_time();
                $max_datetime_code = $tbl_second_train_da_tm->where('districtcd', '=', $forDist)
                        ->max('datetime_cd');

                if ($max_datetime_code == "") {
                    $dateTimeCode = $forDist . "01";
                } else {
                    $tmp_code = 100 + substr($max_datetime_code, -2) + 1;
                   
                    $dateTimeCode = $forDist . substr($tmp_code, -2);
                   
                }
                $save_second_dt_tm = new tbl_second_training_date_time();
                $save_second_dt_tm->districtcd = $forDist;
                $save_second_dt_tm->datetime_cd = $dateTimeCode;
                $save_second_dt_tm->training_dt = date('Y-m-d', strtotime(trim(str_replace('/', '-', $second_train_dt))));
                $save_second_dt_tm->training_time = $second_train_tm;
                $save_second_dt_tm->usercode = $user_code;

                $save_second_dt_tm->save();
                $response = array(
                    'options' => $save_second_dt_tm,
                    'status' => 1);
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }

    public function getSecondTraningDateRecords(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            $this->validate($request, [
                'forDist' => 'required|alpha_num|min:2|max:2'
                    ], [
                'forDist.required' => 'District is required',
                'forDist.alpha_num' => 'District code must be an alpha numeric'
            ]);
            try {
                $forDist = $request->forDist;
                $forDateTime = tbl_second_training_date_time::where('districtcd', '=', $forDist)
                                ->select('training_dt', 'training_time', 'datetime_cd')
                                ->orderBy('datetime_cd')->get();
                //print_r($forDateTime);die;
                $DateTime = "";
                $zoAr = json_decode($forDateTime);
                //  print_r($zoAr);die;
                $DateTime .= "<table class='table table-bordered table-striped1' width='100%' style='border-top: 2px solid #009fe8;'>";
                $DateTime .= "<tr style='background-color: #f5f8fa'>";
                $DateTime .= "<th colspan='4'><span class='highlight'>List of Second Training date & time</span>";
                $DateTime .= "</th>";
                $DateTime .= "</tr>";
                $DateTime .= "<tr style='background-color: #f5f8fa'>";
                $DateTime .= "<td width='10%'><b>SL#</b></td><td><b>Training Date</b></td><td><b>Training Time </b></td><td width='10%'><b>Action</b></td>";
                $DateTime .= "</tr>";
                $count = 0;
                foreach ($zoAr as $fz) {
                    $count++;
                    $DateTime .= "<tr><td>" . $count . "</td>";
                    $DateTime .= "<td>" . date('d/m/Y', strtotime(trim(str_replace('/', '-', $fz->training_dt)))) . "</td>";
                    $DateTime .= "<td>" . $fz->training_time . "</td>";
                    $DateTime .= "<td><a title='Edit'  onclick='edit_second(" . json_encode($fz->datetime_cd) . ");'><i class='fa fa-pencil-alt' style='color:green;cursor:pointer;' value=" . json_encode($fz->datetime_cd) . "></i></a>&nbsp;&nbsp;";
                    $DateTime .= "<a title='Delete'  onclick='delete_second(" . json_encode($fz->datetime_cd) . ");'><i class='fa fa-trash-alt' style='color:red;cursor:pointer;' value=" . json_encode($fz->datetime_cd) . "></i></td></tr>";
                }

                $DateTime .= "</table>";
                $response = array(
                    'options' => $DateTime,
                    'status' => 1);
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }

    public function second_date_time_edit(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            $this->validate($request, [
                'date_time_cd' => 'required|alpha_num|min:4|max:4'
                    ], [
                'date_time_cd.required' => 'Date time code is required',
                'date_time_cd.alpha_num' => 'Date time code must be an alpha numeric'
            ]);
            try {
                $date_time_cd = $request->date_time_cd;
                $Second_date_time_show = tbl_second_training_date_time::select(DB::raw("(DATE_FORMAT(second_training_date_time.training_dt,'%d/%m/%Y')) as trai_date"), 'training_time', 'datetime_cd')
                                ->where('datetime_cd', '=', $date_time_cd)->get();
                $response = array(
                    'options' => $Second_date_time_show,
                    'status' => 1);
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }

    public function update_second_date_time(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            $this->validate($request, [
                'districtcd' => 'required|alpha_num|min:2|max:2',
                'second_train_dt' => 'required|date_format:d/m/Y|min:10|max:10',
                'second_train_tm' => 'required|regex:/^[A-Za-z0-9.\s-]+$/|max:15',
                'second_training_date_time_code' => 'required|alpha_num|min:4|max:4'
                    ], [
                'districtcd.required' => 'District is required',
                'districtcd.alpha_num' => 'District must be an alpha numeric',
                'second_train_dt.required' => ' Date is required',
                'second_train_dt.date_format' => 'Date format should be d/m/y',
                'second_train_tm.required' => ' Time is required',
                'second_train_tm.regex' => 'Time must be an alpha numeric',
                'second_train_tm.max' => 'Time must not be greater than 15 characters',
                'second_training_date_time_code.required' => 'Code is required',
                'second_training_date_time_code.alpha_num' => 'Code should be an alpha numeric'
            ]);
            try {
                $forDist = $request->districtcd;

                $user_code = session()->get("code_ppds");
                $save_secondt_dt_tm = new tbl_second_training_date_time();
                $datetimecd = $request->second_training_date_time_code;
                $save_secondt_dt_tm->training_dt = date('Y-m-d', strtotime(trim(str_replace('/', '-', $request->second_train_dt))));
                $save_secondt_dt_tm->training_time = $request->second_train_tm;


                $upadtez = tbl_second_training_date_time::where('datetime_cd', '=', $datetimecd)
                        ->update(['training_dt' => $save_secondt_dt_tm->training_dt, 'training_time' => $save_secondt_dt_tm->training_time, 'usercode' => $user_code]);

                $response = array(
                    'options' => $upadtez,
                    'status' => 2);
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }

    public function second_date_time_delete(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            $this->validate($request, [
                'date_time_cd' => 'required|alpha_num|min:4|max:4'
                    ], [
                'date_time_cd.required' => 'Date time code is required',
                'date_time_cd.alpha_num' => 'Date time code must be an alpha numeric'
            ]);
            try {
                $date_time_cd = $request->date_time_cd;

                $tbl_second_training_date_time_chking = tbl_second_training_schedule::select('datetimecd')
                        ->where('datetimecd', '=', $date_time_cd);
                $tbl_second_training_date_time_chking_count = $tbl_second_training_date_time_chking->count();

                if ($tbl_second_training_date_time_chking_count == 0) {
                    $record = tbl_second_training_date_time::where('datetime_cd', '=', $date_time_cd); //Should be changed #27

                    if (!empty($record)) {//Should be changed #30
                        $record = $record->delete();
                    }

                    $response = array(
                        'record' => $record, //Should be changed #32
                        'status' => 1
                    );
                } else {
                    $response = array(
                        'options' => "Record exist in another table, you cannot delete this record",
                        'status' => 2);
                }
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }

}
