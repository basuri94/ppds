<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\tbl_assembly_zone;
use App\tbl_personnela;
use App\tbl_assembly_party;
use App\tbl_pollingstation;
use App\tbl_assembly;
use DB;

class ThirdRandController extends Controller {

    //::::::::Third Randomisation(Lock/Unlock):::::::://
    public function thirdzone_phase_wise_assembly(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            $this->validate($request, [
                'zone' => 'required|alpha_num|min:4|max:4',
                'forDist' => 'required|alpha_num|min:2|max:2',
                'phase' => 'nullable|integer'
                    ], [
                'zone.required' => 'Zone is required',
                'zone.alpha_num' => 'Zone must be an alpha numeric characters',
                'forDist.required' => 'District is required',
                'forDist.alpha_num' => 'District must be an alpha numeric characters',
                'phase.integer' => 'Phase must be an integer'
            ]);
            $zone = $request->zone;
            $forDist = $request->forDist;
            $phase = $request->phase;
            try {
                $filtered = "";
                $filtered = tbl_assembly_zone::join('assembly', 'assembly.assemblycd', '=', 'assembly_zone.assemblycd')
                        ->where('assembly_zone.districtcd', '=', $forDist)
                        ->where('assembly_zone.rand_status2', '=', 'Y')
                        ->where('assembly_zone.rand_status1', '=', 'Y')
                        ->select('assembly_zone.assemblycd', 'assembly_zone.rand_status1', 'assembly_zone.rand_status2', 'assembly_zone.rand_status3', 'assembly.assemblyname');
                if ($zone != "") {
                    $filtered = $filtered->where('assembly_zone.zone', '=', $zone);
                }
                if ($phase != "") {
                    $filtered = $filtered->where('assembly_zone.phase_id', '=', $phase);
                }
                $filtered = $filtered->groupBy('assembly_zone.assemblycd')->get();
               
                $response = array(
                    'options' => $filtered,
                    'status' => 1
                );
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }

    public function thirdzone_wise_assembly(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            $this->validate($request, [
                'zone' => 'required|alpha_num|min:4|max:4',
                'forDist' => 'required|alpha_num|min:2|max:2',
                'pc' => 'nullable|alpha_num|min:2|max:2'
                    ], [
                'zone.required' => 'Zone is required',
                'zone.alpha_num' => 'Zone must be an alpha numeric characters',
                'forDist.required' => 'District is required',
                'forDist.alpha_num' => 'District must be an alpha numeric characters',
                'pc.alpha_num' => 'Pc must be an alpha numeric character'
            ]);
            $zone = $request->zone;
            $forDist = $request->forDist;
            $pccd = $request->pc;
            try {
                $filtered = "";
                $filtered = tbl_assembly_zone::join('assembly', 'assembly.assemblycd', '=', 'assembly_zone.assemblycd')
                        ->where('assembly_zone.districtcd', '=', $forDist)
                        ->where('assembly_zone.rand_status2', '=', 'Y')
                        ->where('assembly_zone.rand_status1', '=', 'Y')
                        ->select('assembly_zone.assemblycd', 'assembly_zone.rand_status1', 'assembly_zone.rand_status2', 'assembly_zone.rand_status3', 'assembly.assemblyname');
                if ($zone != "") {
                    $filtered = $filtered->where('assembly_zone.zone', '=', $zone);
                }
                if ($pccd != "") {
                    $filtered = $filtered->where('assembly.pccd', '=', $pccd);
                }
                $filtered = $filtered->groupBy('assembly_zone.assemblycd')->get();
                /* $data = array();
                  if (!empty($filtered)) {
                  foreach ($filtered as $post) {
                  $nestedData['assemblycd'] = $post->assemblycd;
                  $nestedData['assemblyname'] = $post->assemblyname;
                  $nestedData['rand_status1'] = $post->rand_status1;
                  $nestedData['status'] =  tbl_personnela::where('personnela.forassembly','=',$post->assemblycd)
                  ->where('personnela.forzone','=',$zone)
                  ->select(DB::raw('Count(personnela.personcd) as total'))
                  ->get();
                  $data[]=$nestedData;
                  }
                  } */
                // print_r($data);
                $response = array(
                    'options' => $filtered,
                    'status' => 1
                );
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }

    public function thirdrand_lock_unlock(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            $validate_array = ['zone' => 'required|alpha_num|min:4|max:4', 'districtcd' => 'required|alpha_num|min:2|max:2'];
            for ($x = 0; $x <= $request->row_count; $x++) {
                $validate_array['myCheck' . $x] = 'nullable|alpha_dash';
            }
            $validate_array1 = ['zone.required' => 'Zone is required',
                'zone.alpha_num' => 'Zone must be an alpha numeric characters',
                'districtcd.required' => 'District is required',
                'districtcd.alpha_num' => 'District must be an alpha numeric characters'];
            for ($y = 0; $y <= $request->row_count; $y++) {
                $validate_array1['myCheck' . $y . '.alpha_dash'] = 'Assembly must be an alpha numeric characters';
                //  $validate_array1['myCheck'. $y.'.unique'] = 'Already created some assembly(s) for this zone';
            }
            $this->validate($request, $validate_array, $validate_array1);

            try {
                $row_count = $request->row_count;
                $zone = $request->zone;
//                $filtered0=tbl_assembly_zone::where('assembly_zone.zone','=',$zone)
//                         ->where('assembly_zone.rand_status3','!=','Y')
//                         ->update(['rand_status3'=>'N']);
                $count = 0;
                $totalCount = 0;
                for ($i = 1; $i <= $row_count; $i++) {
                    $count1 = 0;
                    $asmcd = 'myCheck' . $i;
                    if ($request->$asmcd != "") {
                        $asmstat = explode('-', $request->$asmcd);
                        $asmcode = $asmstat[0];
                        $asmstatus = $asmstat[1];
                        if ($asmstatus == "A") {
                            $randstat = 'Y';
                        } else if ($asmstatus == "Y") {
                            $randstat = 'N';
                        } else if ($asmstatus == "N") {
                            $count1++;
                            $totalCount++;
                        }
                        if ($count1 == 0) {
                            tbl_assembly_zone::where('assembly_zone.assemblycd', '=', $asmcode)
                                    ->where('assembly_zone.zone', '=', $zone)
                                    ->update(['rand_status3' => $randstat]);
                            $count++;
                        }
                    }
                }
                $response = array(
                    'options' => $count,
                    'totalC' => $totalCount,
                    'status' => 1
                );
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }

    //:::::::::::::::::Third Rand:::::::::::::::://
    public function third_rand_zone_wise_assembly(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            $this->validate($request, [
                'zone' => 'required|alpha_num|min:4|max:4',
                'forDist' => 'required|alpha_num|min:2|max:2',
                'pc' => 'nullable|alpha_num|min:2|max:2'
                    ], [
                'zone.required' => 'Zone is required',
                'zone.alpha_num' => 'Zone must be an alpha numeric characters',
                'forDist.required' => 'District is required',
                'forDist.alpha_num' => 'District must be an alpha numeric characters',
                'pc.alpha_num' => 'PC must be an alpha numeric characters'
            ]);
            $zone = $request->zone;
            $forDist = $request->forDist;
            $pccd = $request->pc;
            try {
                $filtered = "";
                $filtered = tbl_assembly_zone::join('assembly', 'assembly.assemblycd', '=', 'assembly_zone.assemblycd')
                        ->where('assembly_zone.districtcd', '=', $forDist)
                        ->where('assembly_zone.zone', '=', $zone)
                        ->where('assembly_zone.rand_status3', '!=', 'Y')
                        ->where('assembly_zone.rand_status1', '=', 'Y')
                        ->where('assembly_zone.rand_status2', '=', 'Y')
                        ->select('assembly_zone.assemblycd', 'assembly_zone.rand_status1', 'assembly_zone.rand_status2', 'assembly_zone.rand_status3', 'assembly.assemblyname');

                if ($pccd != "") {
                    $filtered = $filtered->where('assembly.pccd', '=', $pccd);
                }
                $filtered = $filtered->get();
                $data = array();
                if (!empty($filtered)) {
                    foreach ($filtered as $post) {
                        $nestedData['assemblycd'] = $post->assemblycd;
                        $nestedData['assemblyname'] = $post->assemblyname;
                        $nestedData['rand_status3'] = $post->rand_status3;

                        $data[] = $nestedData;
                    }
                }
                // print_r($data);
                $response = array(
                    'options' => $data,
                    'status' => 1
                );
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }
    public function third_rand_phase_zone_wise_assembly(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            $this->validate($request, [
                'zone' => 'required|alpha_num|min:4|max:4',
                'forDist' => 'required|alpha_num|min:2|max:2',
                'phase' => 'nullable|integer'
                    ], [
                'zone.required' => 'Zone is required',
                'zone.alpha_num' => 'Zone must be an alpha numeric characters',
                'forDist.required' => 'District is required',
                'forDist.alpha_num' => 'District must be an alpha numeric characters',
                'phase.integer' => 'Phase must be an integer'
            ]);
            $zone = $request->zone;
            $forDist = $request->forDist;
            $phase = $request->phase;
            try {
                $filtered = "";
                $filtered = tbl_assembly_zone::join('assembly', 'assembly.assemblycd', '=', 'assembly_zone.assemblycd')
                        ->where('assembly_zone.districtcd', '=', $forDist)
                        ->where('assembly_zone.zone', '=', $zone)
                        ->where('assembly_zone.rand_status3', '!=', 'Y')
                        ->where('assembly_zone.rand_status1', '=', 'Y')
                        ->where('assembly_zone.rand_status2', '=', 'Y')
                        ->select('assembly_zone.assemblycd', 'assembly_zone.rand_status1', 'assembly_zone.rand_status2', 'assembly_zone.rand_status3', 'assembly.assemblyname');
                
                if ($phase != "") {
                    $filtered = $filtered->where('assembly_zone.phase_id', '=', $phase);
                }
                $filtered = $filtered->get();
                //  dd($filtered);
                $data = array();
                if (!empty($filtered)) {
                    foreach ($filtered as $post) {
                        $nestedData['assemblycd'] = $post->assemblycd;
                        $nestedData['assemblyname'] = $post->assemblyname;
                        $nestedData['rand_status3'] = $post->rand_status3;

                        $data[] = $nestedData;
                    }
                }
                // print_r($data);
                $response = array(
                    'options' => $data,
                    'status' => 1
                );
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }
    public function thirdrand(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            $validate_array = ['zone' => 'required|alpha_num|min:4|max:4', 'districtcd' => 'required|alpha_num|min:2|max:2', 'phase' => 'required|integer'];
            for ($x = 0; $x <= $request->row_count; $x++) {
                $validate_array['myCheck' . $x] = 'nullable|alpha_dash';
            }
            $validate_array1 = ['zone.required' => 'Zone is required',
                'zone.alpha_num' => 'Zone must be an alpha numeric characters',
                'districtcd.required' => 'District is required',
                'districtcd.alpha_num' => 'District must be an alpha numeric characters',
                'phase.required' => 'Phase is required',
                'phase.alpha_num' => 'Phase must be an integer'];
            for ($y = 0; $y <= $request->row_count; $y++) {
                $validate_array1['myCheck' . $y . '.alpha_dash'] = 'Assembly must be an alpha numeric characters';
                //  $validate_array1['myCheck'. $y.'.unique'] = 'Already created some assembly(s) for this zone';
            }
            $this->validate($request, $validate_array, $validate_array1);

            try {
                $row_count = $request->row_count;
                $zone = $request->zone;
                $phase = $request->phase;
                $districtcd = $request->districtcd;
                $filtered0 = tbl_assembly_zone::where('assembly_zone.zone', '=', $zone)
                        ->where('assembly_zone.rand_status1', '=', 'Y')
                        ->where('assembly_zone.rand_status2', '=', 'Y')
                        ->where('assembly_zone.rand_status3', '!=', 'Y')
                        ->update(['rand_status3' => 'N']);
                $count = 0;
                for ($i = 1; $i <= $row_count; $i++) {
                    $asmcd = 'myCheck' . $i;
                    if ($request->$asmcd != "") {
                        $asmstat = explode('-', $request->$asmcd);
                        $asmcode = $asmstat[0];
                        $asmstatus = $asmstat[1];
                        $randstat = 'A';

                        $filtered = tbl_assembly_zone::where('assembly_zone.assemblycd', '=', $asmcode)
                                ->where('assembly_zone.zone', '=', $zone)
                                ->update(['rand_status3' => $randstat]);
                        $count++;
                    }
                }
                DB::select('call pollingstn_dcrc_add(?,?,?)', [$zone, $districtcd,$phase]);
                // $firstRand=DB::select('call swapping(?)',[$zone]);
                //  $pcount=$personnelCheck[0]->t_Count;
//                  $filteredUp=tbl_assembly_zone::where('assembly_zone.rand_status3','=','A')
//                         ->where('assembly_zone.zone','=',$zone)
//                         ->update(['rand_status3'=>'N']);

                $response = array(
                    'options' => $count,
                    'status' => 1
                );
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }

    //::::::::::::::::::::::: Booth Tagging List :::::::::::::::::::://
    public function getBoothTaggingPDF(Request $request) {
        $this->validate($request, [
            'forZone' => 'required|alpha_num|min:4|max:4',
            'assembly' => 'required|alpha_num|min:3|max:3',
            'dc_data_time_party_vs_ps' => 'nullable|alpha_num',
            'phase' => 'required|integer'

                ], [
            'forZone.required' => 'For Zone is required',
            'forZone.alpha_num' => 'For Zone must be an alpha numeric characters',
            'assembly.required' => 'Assembly is required',
            'assembly.alpha_num' => 'Assembly must be an alpha numeric characters',
            'dc_data_time_party_vs_ps.alpha_num' => 'DC Date Time must be an alpha numeric characters',
            'phase.required' => 'Phase is required',
            'phase.alpha_num' => 'Phase must be an integer'
        ]);
        $forzone = $request->forZone;
        $phase = $request->phase;
        $assembly = $request->assembly;
        $dc_data_time_party_vs_ps=$request->dc_data_time_party_vs_ps;
        $ApptRecordAssembly = tbl_assembly::where('assemblycd', '=', $assembly)
                ->select('assemblycd', 'assembly.assemblyname')
                ->get();
        foreach ($ApptRecordAssembly as $ApptRecordAssemblyName) {
            $assemblyname = $ApptRecordAssemblyName->assemblyname;
        }
        $session_personnela = session()->get('personnela_ppds');
        $ApptRecord = tbl_pollingstation::where('forassembly', '=', $assembly)
                ->where('forzone', '=', $forzone)
                ->where('phase', '=', $phase)
                ->select('groupid', 'psname', 'psno', 'psfix')
                ->orderBy('groupid');
//                  ->orderBy('psno')
//                  ->orderBy('psfix')
               
        if($dc_data_time_party_vs_ps!=""){
            $ApptRecord=$ApptRecord->where('pollingstation.dcrccd', '=', $dc_data_time_party_vs_ps);
        }
             $ApptRecord=$ApptRecord->get();
        $subV = json_decode($ApptRecord);
        //   print_r($subV);

        $pdf = app('FPDF');
        $pdf->AddPage();
        $pdf->SetTitle('Booth Tagging List');
        $count = 0;
        $per_page = 40;

        $assem = "ASSEMBLY : " . $assembly . " - " . $assemblyname;
        $pdf->SetFont('Arial', 'B', 8.5);
        $pdf->Cell(190, 4, $assem, 0, 0, 'C');
        $pdf->Ln(6);

        $pdf->SetFillColor(255, 255, 255);
        //	$this->SetTextColor(0,0,0);
        $pdf->SetDrawColor(0, 0, 0);
        $pdf->SetLineWidth(.3);
        $pdf->SetFont('Arial', 'B', 6);
        $head = array('Polling Party', 'PS No', 'PS Name');
        $w = array(30, 20, 140);
        $pdf->SetFont('Arial', 'B', 9);
        for ($j = 0; $j < count($head); $j++)
            $pdf->Cell($w[$j], 7, $head[$j], 1, 0, 'C', true);

        $pdf->Ln();
        foreach ($subV as $subTe) {
            if ($count < $per_page) {
                $fill = false;
                $pdf->SetFont('Arial', '', 7);
                $pdf->Cell($w[0], 6, $subTe->groupid, 'LRT', 0, 'C', $fill);
                $pdf->Cell($w[1], 6, $subTe->psno . "" . $subTe->psfix, 'LRT', 0, 'C', $fill);
                $pdf->Cell($w[2], 6, $subTe->psname, 'LRT', 0, 'L', $fill);
                //count1++;

                $pdf->Ln();
                $pdf->Cell(array_sum($w), 0, '', 1, 0, 'L', $fill);
                $pdf->Ln();

                $fill = !$fill;
                $count++;
            }
            if ($count == $per_page) {
                $per_page = $per_page + 40;
                if ($count != Count($subV)) {
                    $pdf->AddPage();
                    $assem = "ASSEMBLY : " . $assembly . " - " . $assemblyname;
                    $pdf->SetFont('Arial', 'B', 8.5);
                    $pdf->Cell(190, 4, $assem, 0, 0, 'C');
                    $pdf->Ln(6);
                }
            }
        }
        $pdf->Output();
        exit;
    }

    public function get_boothtaging_AssemblyDetails(Request $request){
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        }
        else
        {
            $this->validate($request, [            
            'forZone' => 'required|alpha_num|min:4|max:4',
           
            ], [
            
            'forZone.required' => 'For Zone is required',
            'forZone.alpha_num' => 'For Zone must be an alpha numeric',
           
            ]);
            try
            {
              $forZone=$request->forZone;

              $phase = $request->phase;
              $list = '';
              $asmData = tbl_assembly_zone::
                                            join('assembly','assembly.assemblycd','=','assembly_zone.assemblycd')
                                               ->select('assembly_zone.assemblycd','assembly.assemblyname');
            
            if($forZone!=''){
                $asmData = $asmData->where('zone','=',$forZone);
            }
            if($phase!=''){
                $asmData = $asmData->where('phase_id','=',$phase);
            }

            $asmData = $asmData->get();


               $trArray=json_decode($asmData);
              $list .= "<option value>[Select]</option>";
               foreach($trArray as $trDate){
                  $list .= "<option value='" . $trDate->assemblycd . "'>" . $trDate->assemblycd." - ".$trDate->assemblyname . "</option>";
                }
              $response = array(
                   'options' => $list,
                   'status' => 1);          
            }catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
              $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
      
        }
    }






public function getAssemblyDetailsForPsvspolling(Request $request){
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        }
        else
        {
            $this->validate($request, [            
            'forZone' => 'required|alpha_num|min:4|max:4',
           
            ], [
            
            'forZone.required' => 'For Zone is required',
            'forZone.alpha_num' => 'For Zone must be an alpha numeric',
           
            ]);
            try
            {
              $forZone=$request->forZone;
              $phase = $request->phase;
              $list = '';
              $asmData = tbl_assembly_zone::
                                               join('assembly','assembly.assemblycd','=','assembly_zone.assemblycd')
                                               ->select('assembly_zone.assemblycd','assembly.assemblyname');
            if($forZone!=''){
                    $asmData = $asmData->where('zone','=',$forZone);
                }
                if($phase!=''){
                    $asmData = $asmData->where('phase_id','=',$phase);
                }

                $asmData = $asmData->get();


            $trArray=json_decode($asmData);
              $list .= "<option value>[Select]</option>";
               foreach($trArray as $trDate){
                  $list .= "<option value='" . $trDate->assemblycd . "'>" . $trDate->assemblycd." - ".$trDate->assemblyname . "</option>";
                }
              $response = array(
                   'options' => $list,
                   'status' => 1);          
            }catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
              $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
      
        }
    }
     //::::::::::::::::::::::: Booth Tagging List(PS no vs Polling Party) :::::::::::::::::::://
     public function getBoothTaggingPDFpsvspolling(Request $request) {
        $this->validate($request, [
            'forZone' => 'required|alpha_num|min:4|max:4',
            'assembly' => 'required|alpha_num|min:3|max:3',
            'dc_date_time_ps_vs_party' => 'nullable|alpha_num'
                ], [
            'forZone.required' => 'For Zone is required',
            'forZone.alpha_num' => 'For Zone must be an alpha numeric characters',
            'assembly.required' => 'Assembly is required',
            'assembly.alpha_num' => 'Assembly must be an alpha numeric characters',
            'dc_date_time_ps_vs_party.alpha_num' => 'Assembly must be an alpha numeric characters'
        ]);
        $forzone = $request->forZone;
        $assembly = $request->assembly;
 $dc_date_time_ps_vs_party = $request->dc_date_time_ps_vs_party;
        $ApptRecordAssembly = tbl_assembly::where('assemblycd', '=', $assembly)
                ->select('assemblycd', 'assembly.assemblyname')
                ->get();
        foreach ($ApptRecordAssembly as $ApptRecordAssemblyName) {
            $assemblyname = $ApptRecordAssemblyName->assemblyname;
        }
        $session_personnela = session()->get('personnela_ppds');
        $ApptRecord = tbl_pollingstation::where('forassembly', '=', $assembly)
                ->where('forzone', '=', $forzone)
                ->select('groupid', 'psname', 'psno', 'psfix')
               ->orderBy('psno');
//                 ->orderBy('groupid')
//                  ->orderBy('psfix')
               
        if($dc_date_time_ps_vs_party!=""){
            $ApptRecord=$ApptRecord->where('pollingstation.dcrccd', '=', $dc_date_time_ps_vs_party);
        }
               $ApptRecord=$ApptRecord->get();
        $subV = json_decode($ApptRecord);
        //   print_r($subV);

        $pdf = app('FPDF');
        $pdf->AddPage();
        $pdf->SetTitle('Booth Tagging List');
        $count = 0;
        $per_page = 40;

        $assem = "ASSEMBLY : " . $assembly . " - " . $assemblyname;
        $pdf->SetFont('Arial', 'B', 8.5);
        $pdf->Cell(190, 4, $assem, 0, 0, 'C');
        $pdf->Ln(6);

        $pdf->SetFillColor(255, 255, 255);
        //	$this->SetTextColor(0,0,0);
        $pdf->SetDrawColor(0, 0, 0);
        $pdf->SetLineWidth(.3);
        $pdf->SetFont('Arial', 'B', 6);
        $head = array( 'PS No','Polling Party','PS Name');
        $w = array(20, 30, 140);
        $pdf->SetFont('Arial', 'B', 9);
        for ($j = 0; $j < count($head); $j++)
            $pdf->Cell($w[$j], 7, $head[$j], 1, 0, 'C', true);

        $pdf->Ln();
        foreach ($subV as $subTe) {
            if ($count < $per_page) {
                $fill = false;
                $pdf->SetFont('Arial', '', 7);
                
                $pdf->Cell($w[0], 6, $subTe->psno . "" . $subTe->psfix, 'LRT', 0, 'C', $fill);
                $pdf->Cell($w[1], 6, $subTe->groupid, 'LRT', 0, 'C', $fill);
                $pdf->Cell($w[2], 6, $subTe->psname, 'LRT', 0, 'L', $fill);
                //count1++;

                $pdf->Ln();
                $pdf->Cell(array_sum($w), 0, '', 1, 0, 'L', $fill);
                $pdf->Ln();

                $fill = !$fill;
                $count++;
            }
            if ($count == $per_page) {
                $per_page = $per_page + 40;
                if ($count != Count($subV)) {
                    $pdf->AddPage();
                    $assem = "ASSEMBLY : " . $assembly . " - " . $assemblyname;
                    $pdf->SetFont('Arial', 'B', 8.5);
                    $pdf->Cell(190, 4, $assem, 0, 0, 'C');
                    $pdf->Ln(6);
                }
            }
        }
        $pdf->Output();
        exit;
    }
}
