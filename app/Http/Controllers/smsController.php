<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\tbl_training_type;
use App\tbl_first_training_date_time;
use App\tbl_poststat;
use App\tbl_personnel;
use App\tbl_first_rand_table;
use App\tbl_second_rand_table;

use App\tbl_tblsms1;
use DB;


class smsController extends Controller
{

    public function getSmsRelatedData()
    {
        $dataAllforSMS = $this->getTrainingGroupData();
        return view('save_sms', compact('dataAllforSMS'));
    }



    public function getSendSmsRelatedData()
    {
        $dataAllforSMS = $this->getTrainingGroupData();
        return view('sendSmsForGeneralTraining', compact('dataAllforSMS'));
    }


    public function getTrainingGroupData()
    {
        $desc = tbl_training_type::pluck('training_desc', 'training_code')->all();

        $districtCd = session()->get('districtcd_ppds');
        $trainingDateTime = tbl_first_training_date_time::where('districtcd', '=', $districtCd)
            // ->select('training_time', 'DATE_FORMAT(training_dt, "%d-%m-%Y")," ", training_time) AS label', 'datetime_cd')->get();
            ->select(DB::raw('concat(DATE_FORMAT(training_dt, "%d-%m-%Y")," ",training_time) as names'), 'datetime_cd')->pluck('names', 'datetime_cd');

        $poststatus = tbl_poststat::pluck('poststatus', 'post_stat')->all();
        // $data = array('poststatus' => $poststatus);


        $data = array('trainingDateTime' => $trainingDateTime, 'desc' => $desc, 'poststatus' => $poststatus);
        //print_r(json_encode($data));die;
        return $data;
    }

    public function sms_entry_details(Request $request)
    {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            $this->validate($request, [
                'forZone' => 'required|alpha_num|min:4|max:4',
                //'poststatus' => 'required|alpha_num',
                'user_message_box' => 'required|regex:/^[A-Za-z0-9\s,-.]+$/i',
                'training_type' => 'required|alpha_num|min:2|max:2',
                'phase' => 'required',
            ], [
                'forZone.required' => 'Zone is required',
                'forZone.alpha_num' => 'Zone must be an alpha numeric characters',
                //  'poststatus.required' => 'Poststatus is required',
                // 'poststatus.alpha_num' => 'Poststatus must be an alpha numeric characters',
                'user_message_box.required' => 'Message box is required',
                'user_message_box.regex' => 'Message box contain alphanumeric character ans these special characters ,-. only',
                'training_type.required' => 'Training Type is required',
                'training_type.alpha_num' => 'Training Type must be an alpha numeric characters',
                'phase.required' => 'Phase is required',
            ]);
            $forZone = $request->forZone;
            //      $poststatus = $request->poststatus;
            $message = '';
            $user_message_box = $request->user_message_box;

            $user_message_box = "General Election to WBLA-2021. Please attend  Training on";
            $phase = $request->phase;
            $training_type = $request->training_type;
            $training_type_text = $request->training_type_text;
            $session_personnela = session()->get('personnela_ppds');
            try {



                $SmsRecord = DB::select('call sms_make_test(?,?,?,?,?)', [$forZone, $user_message_box, $training_type, $training_type_text, $phase]);
                $Smscount = $SmsRecord[0]->t_Count;

                $response = array(
                    'options' => $SmsRecord,
                    'status' => 1,
                    'countTotal' => $Smscount
                );
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }

    public function getMessageShow(Request $request)
    {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            $this->validate($request, [
                'forZone' => 'required|alpha_num|min:4|max:4',
                // 'user_message_box' => 'required|regex:/^[A-Za-z0-9\s,-.]+$/i',
                'training_type' => 'required|alpha_num|min:2|max:2',
                'phase' => 'required',
            ], [
                'forZone.required' => 'Zone is required',
                'forZone.alpha_num' => 'Zone must be an alpha numeric',
                // 'user_message_box.required' => 'Message box is required',
                // 'user_message_box.regex' => 'Message box contain alphanumeric character ans these special characters ,-. only',
                'training_type.required' => 'Training Type is required',
                'training_type.alpha_num' => 'Training Type must be an alpha numeric characters',
                'phase.required' => 'Phase is required',
            ]);
            try {
                $message = '';
                $forZone = $request->forZone;
                $phase = $request->phase;
                $user_message_box = $request->user_message_box;
                $user_message_box = 'General Election to WBLA-2021. Please attend  Training on ';
                $training_type = $request->training_type;
                $training_type_text = $request->training_type_text;
                $PersonData = tbl_first_rand_table::where('first_rand_table.forzone', '=', $forZone)
                    //->where('first_rand_table.poststat', '=', $poststatus)
                    ->select('first_rand_table.mob_no', 'first_rand_table.officer_name', 'first_rand_table.personcd', 'first_rand_table.venuename', 'first_rand_table.training_desc', 'first_rand_table.venueaddress', 'first_rand_table.training_dt', 'first_rand_table.training_time', 'first_rand_table.training_desc_2', 'first_rand_table.venuename_2', 'first_rand_table.venueaddress_2', 'first_rand_table.training_dt_2', 'first_rand_table.training_time_2', 'first_rand_table.poststat', 'forzone');
                if ($training_type == '01' || $training_type=='03' || $training_type=='05') {
                    $PersonData = $PersonData->where('first_rand_table.training_desc', '=', $training_type_text);
                }
                if ($training_type == '02' || $training_type=='04' || $training_type=='06') {
                    $PersonData = $PersonData->where('first_rand_table.training_desc_2', '=', $training_type_text);
                }
                if (!empty($phase)) {
                    $PersonData = $PersonData->where('first_rand_table.phase', '=', $phase);
                }

                $PersonData = $PersonData->inRandomOrder()
                    ->take(1)->get();

                if (count($PersonData) > 0) {
                    foreach ($PersonData as $PersonData1) {
                        $poststatus = $PersonData1->poststat;
                        // if ($poststatus == 'P1') {
                        //     $poststatus = "1st Polling Officer";
                        // } else if ($poststatus == 'P2') {
                        //     $poststatus = "2nd Polling Officer";
                        // } else if ($poststatus == 'P3') {
                        //     $poststatus = "3rd Polling Officer";
                        // } else if ($poststatus == 'PR') {
                        //     $poststatus = "Presiding Officer";
                        // } else if ($poststatus == 'PA') {
                        //     $poststatus = "Addl.-P2(1)";
                        // } else if ($poststatus == 'PB') {
                        //     $poststatus = "Addl.-P2(2)";
                        // }

                        // if ($training_type == 01) {
                        //     $message = $user_message_box . "\n Name: " . $PersonData1->officer_name . ". \n Post: " . $poststatus . ".\n First Training venue: " . $PersonData1->venuename . ".\n Training Date: " . $PersonData1->training_dt . ".\n Training Time: " . $PersonData1->training_time . ".";
                        // } else {
                        //     $message = $user_message_box . "\n Name: " . $PersonData1->officer_name . ". \n Post: " . $poststatus .
                        //         ".\n Second training venue: " . $PersonData1->venuename_2 . ".\n Training Date: " . $PersonData1->training_dt_2 . ".\n Training Time: " . $PersonData1->training_time_2 . ".";
                        // }

                        if ($training_type == '01' || $training_type=='03' || $training_type=='05') {
                            $message = $user_message_box . $PersonData1->training_dt . " at " . $PersonData1->venuename . ',' . $PersonData1->venueaddress . ' at ' . $PersonData1->training_time;
                        } else if($training_type == '02' || $training_type=='04' || $training_type=='06'){
                            $message = $user_message_box . $PersonData1->training_dt_2 . " at " . $PersonData1->venuename_2 . ',' . $PersonData1->venueaddress_2 . ' at ' . $PersonData1->training_time_2;
                        }
                    }
                } else {
                    $message = 'No Record Found';
                }

                //  $pos=strpos($message, ' ', 160);
                $final_msg =  substr($message, 0, 160);

                $response = array(
                    'options' => $final_msg,
                    'status' => 1
                );
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }

    public function getTrainingRecordAvailable(Request $request)
    {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {



            try {
                $phase = $request->phase;
                /*******************General Training********************* */

                $tbl_sms=tbl_tblsms1::groupBy('training_code')
                ->select('training_code');
                if (!empty($phase)) {
                    $tbl_sms = $tbl_sms->where('phase_id', '=', $phase);
                }
                $tbl_sms = $tbl_sms->get();
                $requirement = "";
                foreach($tbl_sms as $tbl_sms1){
                    $trainingName="";
                    $trainingVenue='';
                    $totalUsers='';
                    $completeVenue='';
                    $completeUsers='';

                    $trainingName=tbl_training_type::where('training_code',$tbl_sms1->training_code)->value('training_desc');
                    $trainingVenueData=tbl_tblsms1::where('training_code', '=', $tbl_sms1->training_code)->select(DB::raw('distinct schedulecode'))
                    ->groupBy('schedulecode')->get();
                    $trainingVenue=count($trainingVenueData);
                    $totalUsers=tbl_tblsms1::where('training_code', '=', $tbl_sms1->training_code)->value(DB::raw('count(phone_no)'));
                    $completeVenueData= tbl_tblsms1::where('smscount', '=', '1')->where('training_code', '=', $tbl_sms1->training_code)
                    ->select(DB::raw('distinct schedulecode'))
                    ->groupBy('schedulecode')->get();
                    $completeVenue=count($completeVenueData);

                    $completeUsers=tbl_tblsms1::where('smscount', '=', '1')->where('training_code', '=', $tbl_sms1->training_code)->value(DB::raw('count(phone_no)'));
                  
                
                  
                    
                    $requirement .= '<tr>';
                    $requirement .= '<td >' .  $trainingName . '</td>';

                    $requirement .= "<td>" . $trainingVenue . '</td>';
                    $requirement .= "<td>" .  ($trainingVenue - $completeVenue) . '</td>';
                    $requirement .= "<td>" . $totalUsers . '</td>';
                    $requirement .= "<td>" . ($totalUsers - $completeUsers). '</td>';
                  
                  
                   


                    // $requirement .= "<td>" . $totalp1firstassign . '</td>';
                    // $requirement .= "<td>" . $totalp2firstassign . '</td>';
                    // $requirement .= "<td>" . $totalp3firstassign . '</td>';
                    // $requirement .= "<td>" . $totalprfirstassign . '</td>';

                    $requirement .= '</tr>';
                   
                }

                // $firstTraining = tbl_tblsms1::where('training_code', '=', '01')->where('smscount', '=', '0');
                // if (!empty($phase)) {
                //     $firstTraining = $firstTraining->where('phase_id', '=', $phase);
                // }
                // $firstTraining = $firstTraining->value(DB::raw('count(phone_no)'));

                // $secondTraining = tbl_tblsms1::where('training_code', '=', '02')->where('smscount', '=', '0');
                // if (!empty($phase)) {
                //     $secondTraining = $secondTraining->where('phase_id', '=', $phase);
                // }
                // $secondTraining = $secondTraining->value(DB::raw('count(phone_no)'));
                
                // $firstTrainingVenue = tbl_tblsms1::where('training_code', '=', '01')->where('smscount', '=', '0')->select('schedulecode')->groupBy('schedulecode');
                // if (!empty($phase)) {
                //     $firstTrainingVenue = $firstTrainingVenue->where('phase_id', '=', $phase);
                // }

                // $firstTrainingVenue = $firstTrainingVenue->get();

                // $secondTrainingVenue = tbl_tblsms1::where('training_code', '=', '02')->where('smscount', '=', '0')->select('schedulecode')->groupBy('schedulecode');
                // if (!empty($phase)) {
                //     $secondTrainingVenue = $secondTrainingVenue->where('phase_id', '=', $phase);
                // }

                // $secondTrainingVenue = $secondTrainingVenue->get();


                /*******************Special Training1********************* */








                $response = array(
                    'requirement'=>$requirement,
                    // 'firstTraining' => $firstTraining,
                    // 'secondTraining' => $secondTraining,
                    // 'firstTrainingVenue'=>count($firstTrainingVenue),
                    // 'secondTrainingVenue'=>count($secondTrainingVenue),
                    
                    'status' => 1
                );
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }

    public function send_sms_for_general(Request $request)
    {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            $this->validate($request, [
                'from' => 'required',
                'to' => 'required|integer',
                'training_type' => 'required',
                'phase' => 'required',
            ], [
                'from.required' => 'From PP is required',
                'to.required' => 'Count PP is required',
                'to.integer' => 'Count PP  must be an integer',
                'training_type.required' => 'Training type is required',
                'phase.required' => 'Phase is required',
            ]);

            try {
                $training_type = $request->training_type;
                $phase = $request->phase;
                $from = $request->from;
                $to = $request->to;
                // dd($to);
                $fetchTblSms = tbl_tblsms1::where('training_code', '=', $training_type)
                    ->where('phase_id', '=', $phase)
                  //  ->where('smscount', '=', 0)
                      ->offset($from - 1)->limit($to-$from+1)
                      ->select(DB::raw('distinct schedulecode'))
                      ->groupBy('schedulecode')->get();
              //  echo json_encode($fetchTblSms);die;


                $count = 0;
                $mobileUser=0;
                $message_arr = array();
                $databasemobie_arr = array();
                foreach ($fetchTblSms as $fetchTblSms1) {
                    $mobie_arr = array();
                    $count++;
                    $mobileno = '';
                    $message = '';
                    $message = tbl_tblsms1::where('schedulecode', $fetchTblSms1->schedulecode)->value('message');
                    $getmobileno = tbl_tblsms1::where('schedulecode', $fetchTblSms1->schedulecode)->select('phone_no')->get();
                    foreach ($getmobileno as $getmobileno) {
                        $mobileUser++;
                        array_push($mobie_arr, $getmobileno->phone_no);
                       
                    }
                  //  array_push($message_arr, $message);
                     $this->sendSingleSMS($mobie_arr, $message);

                    $updateTblSMS = tbl_tblsms1::whereIn('phone_no', $mobie_arr)
                        ->where('training_code', '=', $training_type)
                        ->where('phase_id', '=', $phase)
                        ->where('schedulecode',$fetchTblSms1->schedulecode)
                        ->update(['smscount' => DB::raw('smscount+1')]);

                        
                    // array_push($message_arr, $message);
                    // array_push($databasemobie_arr, $fetchTblSms1->phone_no);
                    // $this->sendSingleSMS($mobileno, $message);
                    // include('sms/smsclient.php');
                    // 

                }
              //  echo json_encode($message_arr);die;

                $response = array(
                    'sendSms' => $count,
                    'mobileUser'=>$mobileUser,
                    'status' => 1
                );
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }

    public function SMSRecordssend(Request $request)
    {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            $this->validate($request, [
                'training_type' => 'required|alpha_num|min:2|max:2',
                'phase' => 'required',
            ], [
                'training_type.required' => 'Training Type is required',
                'training_type.alpha_num' => 'Training Type must be an alpha numeric characters',
                'phase.required' => 'Phase is required',
            ]);


            try {
                $training_type = $request->training_type;
                $phase = $request->phase;
                // $SmsSendAlready = tbl_tblsms1::where('training_code', '=', $training_type)->where('smscount', '=', 1)
                // ->where('phase_id', '=', $phase)
                // ->value(DB::raw('count(phone_no)'));

                $SmsSendAlreadyData = tbl_tblsms1::where('training_code', '=', $training_type)->where('smscount', '=', 1)
                    ->where('phase_id', '=', $phase)
                    ->select(DB::raw('distinct schedulecode'))
                      ->groupBy('schedulecode')->get();

                      $SmsSendAlready=count($SmsSendAlreadyData);
//$SmsSendAlready=  $SmsSendAlready->value(DB::raw('count(schedulecode)'));


                $response = array(
                    'SmsSendAlready' => $SmsSendAlready + 1,
                    // 'SmsSendAlready' => $SmsSendAlready,
                    'nextSmsLot' => $SmsSendAlready + 250,
                    'status' => 1
                );
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }

    public function sendSingleSMS($mobileno, $message)
    {  $mobileNos='';
        $username = config('constants.USERNAME'); //username of the departmen

        $password = config('constants.PASSWORD'); //password of the department
        $senderid =  config('constants.SENDERID'); //senderid of the deparment
        // $message = "Your Normal message here "; //message content
        $messageUnicode = "मोबाइलसेवामेंआपकास्वागतहै "; //message content in unicode
        //   $mobileno=""; //if single sms need to be send use mobileno keyword
        $mobileNos = implode(',', $mobileno);
        //print_r($mobileNos);die;
       // $mobileNos= "7980417810,7501386334"; //if bulk sms need to send use mobileNos as keyword and mobile number seperated by commas as value
        $deptSecureKey =  config('constants.SECUREKEY'); //departsecure key for encryption of message...
           $templateid= config('constants.TEMPLATEID');

        $encryp_password = sha1(trim($password));
        $key = hash('sha512', trim($username) . trim($senderid) . trim($message) . trim($deptSecureKey));

        $data = array(
            "username" => trim($username),
            "password" => trim($encryp_password),
            "senderid" => trim($senderid),
            "content" => trim($message),
            "smsservicetype" => "bulkmsg",
            "bulkmobno" => trim($mobileNos),
           //  "templateid"=>$templateid,
            "key" => trim($key)
        );
        $url = "https://msdgweb.mgov.gov.in/esms/sendsmsrequest";
      // $url = "https://msdgweb.mgov.gov.in/esms/sendsmsrequestDLT";

        $fields = '';
        foreach ($data as $key => $value) {
            $fields .= $key . '=' . urlencode($value) . '&';
        }
        rtrim($fields, '&');
        $post = curl_init();
        //curl_setopt($post, CURLOPT_SSLVERSION, 5); // uncomment for systems supporting TLSv1.1 only
        curl_setopt($post, CURLOPT_SSLVERSION, 6); // use for systems supporting TLSv1.2 or comment the line
        curl_setopt($post, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($post, CURLOPT_URL, $url);
        curl_setopt($post, CURLOPT_POST, count($data));
        curl_setopt($post, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($post, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($post); //result from mobile seva server
         //output from server displayed 
        curl_close($post);
        // echo $result;
        // die; 
        return 1;
    }
    public function sendSingleSMS1($message, $mobileno)
    {
        $username = "ceowb-ppms"; //username of the department
        $password = "Pp\$pms@2019"; //password of the department
        $senderid = "WBELEC"; //senderid of the deparment
        $deptSecureKey = "e8f89960-3044-4b1f-8ff1-9121b90f547f"; //departsecure key for encryption of message...
        $encryp_password = sha1(trim($password));
        $key = hash('sha512', trim($username) . trim($senderid) . trim($message) . trim($deptSecureKey));

        $data = array(
            "username" => trim($username),
            "password" => trim($encryp_password),
            "senderid" => trim($senderid),
            "content" => trim($message),
            "smsservicetype" => "singlemsg",
            "mobileno" => trim($mobileno),
            "key" => trim($key)
        );
        $fields = '';
        $url = "https://msdgweb.mgov.gov.in/esms/sendsmsrequest";
        foreach ($data as $key => $value) {
            $fields .= $key . '=' . urlencode($value) . '&';
        }
        rtrim($fields, '&');
        $post = curl_init();
        //curl_setopt($post, CURLOPT_SSLVERSION, 5); // uncomment for systems supporting TLSv1.1 only
        curl_setopt($post, CURLOPT_SSLVERSION, 6); // use for systems supporting TLSv1.2 or comment the line
        curl_setopt($post, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($post, CURLOPT_URL, $url);
        curl_setopt($post, CURLOPT_POST, count($data));
        curl_setopt($post, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($post, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($post); //result from mobile seva server
        //echo $result; //output from server displayed
        curl_close($post);
    }

    public function sms_entry_details_for_group(Request $request)
    {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            $this->validate($request, [
                'forZone' => 'required|alpha_num|min:4|max:4',
                //'poststatus' => 'required|alpha_num',
                'user_message_box' => 'required|regex:/^[A-Za-z0-9\s,-.]+$/i',

            ], [
                'forZone.required' => 'Zone is required',
                'forZone.alpha_num' => 'Zone must be an alpha numeric characters',
                //  'poststatus.required' => 'Poststatus is required',
                // 'poststatus.alpha_num' => 'Poststatus must be an alpha numeric characters',
                'user_message_box.required' => 'Message box is required',
                'user_message_box.regex' => 'Message box contain alphanumeric character ans these special characters ,-. only',

            ]);
            $forZone = $request->forZone;
            //      $poststatus = $request->poststatus;
            $message = '';
            $user_message_box = $request->user_message_box;

            $session_personnela = session()->get('personnela_ppds');
            try {



                $SmsRecord = DB::select('call sms_make_test_for_group(?,?)', [$forZone, $user_message_box]);
                $Smscount = $SmsRecord[0]->t_Count;

                $response = array(
                    'options' => $SmsRecord,
                    'status' => 1,
                    'countTotal' => $Smscount
                );
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }

    public function getMessageShowForGroup(Request $request)
    {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            $this->validate($request, [
                'forZone' => 'required|alpha_num|min:4|max:4',
                'user_message_box' => 'required|regex:/^[A-Za-z0-9\s,-.]+$/i',

            ], [
                'forZone.required' => 'Zone is required',
                'forZone.alpha_num' => 'Zone must be an alpha numeric',
                'user_message_box.required' => 'Message box is required',
                'user_message_box.regex' => 'Message box contain alphanumeric character ans these special characters ,-. only',

            ]);
            try {
                $message = '';
                $forZone = $request->forZone;
                $user_message_box = $request->user_message_box;

                $PersonData = tbl_second_rand_table::where('second_rand_table.forzone', '=', $forZone)
                    //->where('first_rand_table.poststat', '=', $poststatus)
                    ->select(
                        'second_rand_table.pr_name',
                        'second_rand_table.p1_name',
                        'second_rand_table.p2_name',
                        'second_rand_table.p3_name',
                        'second_rand_table.pa_name',
                        'second_rand_table.pb_name',
                        'second_rand_table.pr_post_stat',
                        'second_rand_table.p1_post_stat',
                        'second_rand_table.p2_post_stat',
                        'second_rand_table.p3_post_stat',
                        'second_rand_table.pa_post_stat',
                        'second_rand_table.pb_post_stat',
                        'second_rand_table.training_venue',
                        'second_rand_table.training_date',
                        'second_rand_table.training_time'
                    );
                //                if ($training_type == 01) {
                //                    $PersonData = $PersonData->where('first_rand_table.training_desc', '=', $training_type_text);
                //                }
                //                if ($training_type == 02) {
                //                    $PersonData = $PersonData->where('first_rand_table.training_desc_2', '=', $training_type_text);
                //                }
                $PersonData = $PersonData->inRandomOrder()
                    ->take(1)->get();

                if (count($PersonData) > 0) {
                    foreach ($PersonData as $PersonData1) {
                        $poststatus = $PersonData1->per_poststat;
                        if ($poststatus == 'P1') {
                            //    $poststatus = "1st Polling Officer";
                            $message = $user_message_box . "\n Name: " . $PersonData1->p1_name . ". \n Post: " . $PersonData1->p1_post_stat . ".\n First Training venue: " . $PersonData1->training_venue . ".\n Training Date: " . $PersonData1->training_date . ".\n Training Time: " . $PersonData1->training_time . ".";
                        } else if ($poststatus == 'P2') {
                            // $poststatus = "2nd Polling Officer";
                            $message = $user_message_box . "\n Name: " . $PersonData1->p2_name . ". \n Post: " . $PersonData1->p2_post_stat . ".\n First Training venue: " . $PersonData1->training_venue . ".\n Training Date: " . $PersonData1->training_date . ".\n Training Time: " . $PersonData1->training_time . ".";
                        } else if ($poststatus == 'P3') {
                            //  $poststatus = "3rd Polling Officer";
                            $message = $user_message_box . "\n Name: " . $PersonData1->p3_name . ". \n Post: " . $PersonData1->p3_post_stat . ".\n First Training venue: " . $PersonData1->training_venue . ".\n Training Date: " . $PersonData1->training_date . ".\n Training Time: " . $PersonData1->training_time . ".";
                        } else if ($poststatus == 'PR') {
                            // $poststatus = "Presiding Officer";
                            $message = $user_message_box . "\n Name: " . $PersonData1->pr_name . ". \n Post: " . $PersonData1->pr_post_stat . ".\n First Training venue: " . $PersonData1->training_venue . ".\n Training Date: " . $PersonData1->training_date . ".\n Training Time: " . $PersonData1->training_time . ".";
                        } else if ($poststatus == 'PA') {
                            // $poststatus = "Addl.-P2(1)";
                            $message = $user_message_box . "\n Name: " . $PersonData1->pa_name . ". \n Post: " . $PersonData1->pa_post_stat . ".\n First Training venue: " . $PersonData1->training_venue . ".\n Training Date: " . $PersonData1->training_date . ".\n Training Time: " . $PersonData1->training_time . ".";
                        } else if ($poststatus == 'PB') {
                            // $poststatus = "Addl.-P2(2)";
                            $message = $user_message_box . "\n Name: " . $PersonData1->pb_name . ". \n Post: " . $PersonData1->pb_post_stat . ".\n First Training venue: " . $PersonData1->training_venue . ".\n Training Date: " . $PersonData1->training_date . ".\n Training Time: " . $PersonData1->training_time . ".";
                        }
                    }
                } else {
                    $message = $user_message_box;
                }


                $response = array(
                    'options' => $message,
                    'status' => 1
                );
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }
}
