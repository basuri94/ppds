<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\tbl_dcrcmaster;
use App\tbl_dcrc_party;
use DB;

class DCRCMasterController extends Controller {

    public function DCRCMasterForm(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else { 
            
            $myArray1 = json_decode($request->dc_venue_array);
            
            $myArray2 = json_decode($request->dc_address_array);
           
            $myArray3 = json_decode($request->rc_venue_array);
            
            $myArray4 = json_decode($request->rc_address_array);

            
            $validator1 = \Validator::make(compact('myArray1'), [
            'myArray1' => 'required|array',
            'myArray1.*' => 'regex:/^[A-Za-z0-9\s,-.]+$/i|max:60'
            ],[
             'myArray1.*.regex'=>'DC Venue must be an alpha numeric characters',
                'myArray1.*.max'=>'DC Venue Length must not be greater than 60'
            ]);
            $this->validateWith($validator1);
            
            $validator2 = \Validator::make(compact('myArray2'), [
            'myArray2' => 'required|array',
            'myArray2.*' => 'regex:/^[A-Za-z0-9\s,-.]+$/i|max:60'
            ],[
             'myArray2.*.regex'=>'DC Address must be an alpha numeric character',
                'myArray2.*.max'=>'DC Address Length must not be greater than 60'
            ]);          
            $this->validateWith($validator2);
            
             $validator3 = \Validator::make(compact('myArray3'), [
            'myArray3' => 'required|array',
            'myArray3.*' => 'regex:/^[A-Za-z0-9\s,-.]+$/i|max:60'
            ],[
             'myArray3.*.regex'=>'RC Venue must be an alpha numeric character',
                 'myArray3.*.max'=>'RC Venue Length must not be greater than 60'
            ]);            
            $this->validateWith($validator3);
            
             $validator4 = \Validator::make(compact('myArray4'), [
            'myArray4' => 'required|array',
            'myArray4.*' => 'regex:/^[A-Za-z0-9\s,-.]+$/i|max:60'
            ],[
             'myArray4.*.regex'=>'RC Address must be an alpha numeric character' ,
                 'myArray4.*.max'=>'RC Address Length must not be greater than 60'
            ]);
            $this->validateWith($validator4);

            $this->validate($request, [
                'forZone' => 'required|alpha_num|min:4|max:4',
                'districtcd' => 'required|alpha_num|min:2|max:2',
                'dcdate' => 'required|date_format:d/m/Y|min:10|max:10',
                // 'dctime' => 'required|regex:/^[A-Za-z0-9\s-.]+$/i|max:15'
                'dctime' => 'required|max:15'
                    ], [
                'forZone.required' => 'For Zone is required',
                'forZone.alpha_num' => 'For Zone must be an alpha numeric characters',
                'districtcd.required' => 'District is required',
                'districtcd.alpha_num' => 'District must be an alpha numeric characters',
                'dcdate.required' => 'DC Date is required',
                'dcdate.date_format' => 'DC Date format should be d/m/y',
                'dctime.required' => 'DC Time is required',
                // 'dctime.regex' => 'DC Time must be an alpha numeric characters',
                'dctime.max' => 'DC Time must not be greater than 15 characters'
            ]);
            try {
                // dd($request->all());
                $user_code = session()->get('code_ppds');
                $forZone = $request->forZone;
                $districtcd = $request->districtcd;
                $list = '';
                if (sizeof($myArray1) == sizeof($myArray2) && sizeof($myArray1) > 0) {
                    $myArraylength1 = sizeof($myArray1);
                    $count1 = 0;
                    $countDU = 0;
                    $countSM = 0;
                    for ($i = 1; $i <= $myArraylength1; $i++) {

                        $dc_venue = $myArray1[$count1];
                        $dc_address = $myArray2[$count1];
                        $rc_venue = $myArray3[$count1];
                        $rc_address = $myArray4[$count1];

                        $tbl_dcrcmaster = new tbl_dcrcmaster();
                        $max_code = $tbl_dcrcmaster->where('zone', '=', $forZone)
                                        ->select(DB::raw('max(dcrcgrp) as cnt'))->get();
                        $max_subcode = json_decode($max_code);
                        if ($max_subcode[0]->cnt == "") {
                            $dcrcgrp = $forZone . "01";
                        } else {
                            $tmp_subcode = 100 + substr($max_subcode[0]->cnt, -2) + 1;
                            $dcrcgrp = $forZone . substr($tmp_subcode, -2);
                        }

                        $tbl_dcrcmaster->dcrcgrp = $dcrcgrp;
                        $tbl_dcrcmaster->dc_venue = $dc_venue;
                        $tbl_dcrcmaster->dc_address = $dc_address;
                        $tbl_dcrcmaster->rc_venue = $rc_venue;
                        $tbl_dcrcmaster->rc_address = $rc_address;
                        $tbl_dcrcmaster->zone = $forZone;
                        $tbl_dcrcmaster->districtcd = $districtcd;
                        $tbl_dcrcmaster->usercode = $user_code;
                        $tbl_dcrcmaster->dc_date = date('Y-m-d', strtotime(trim(str_replace('/', '-', $request->dcdate))));
                        $tbl_dcrcmaster->dc_time = $request->dctime;
                        $tbl_dcrcmaster->save();
                        $count1++;
                    }
                }
                $response = array(
                    'options' => $count1,
                    'status' => 1);
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }

    //:::::Pradyut::://
    public function dcrc_master_list_datatable(Request $request) {
        $response = [];
        $perm = null;
        $statusCode = 200;
        $users = array(); //Should be changed #4
        $search_val = array();
        $this->validate($request, [
            'search.*' => 'nullable|regex:/^[A-Za-z0-9\s]+$/i',
            'draw' => 'required|integer',
            'start' => 'required|integer',
            'length' => 'required|integer',
            'zone' => 'nullable|alpha_num|min:4|max:4'
                ], [
            'search.*.regex' => 'Special Charecters not allowed',
            'draw.required' => 'Draw is required',
            'start.integer' => 'Start must be an integer',
            'start.required' => 'Start is required',
            'length.integer' => 'Length must be an integer',
            'length.required' => 'Length is required',
            'zone.alpha_num' => 'Zone code must be an alpha numeric'    
        ]);
        try {
            $draw = $request->draw;
            $offset = $request->start;
            $length = $request->length;
            $search = $request->search ["value"];
            $order = $request->order;
            //print_r($order);die;
            $forZone = $request->zone;
            $dcrcs = tbl_dcrcmaster::all();
            //  $categ= \Session::get('category_ppds');
            $dist = \Session::get('districtcd_ppds');
            $filtered = tbl_dcrcmaster::join('zone', 'zone.zone', '=', 'dcrcmaster.zone')
                    ->select('dcrcmaster.dc_venue', 'dcrcmaster.dc_address', 'dcrcmaster.rc_venue', 'dcrcmaster.rc_address', 'dcrcmaster.dcrcgrp', 'zone.zonename', 'dcrcmaster.dc_date', 'dcrcmaster.dc_time')
                    ->where('dcrcmaster.districtcd', '=', $dist)
                    ->orderBy('dcrcmaster.dcrcgrp')
                    ->where(function($q) use ($search) {
                $q->orwhere('dc_venue', 'like', '%' . $search . '%')
                ->orwhere('dc_address', 'like', '%' . $search . '%')
                ->orwhere('rc_venue', 'like', '%' . $search . '%')
                ->orwhere('rc_address', 'like', '%' . $search . '%')
                ->orwhere('dc_date', 'like', '%' . $search . '%')
                ->orwhere('dc_time', 'like', '%' . $search . '%');
            });
            if ($forZone != '') {
                $filtered = $filtered->where('dcrcmaster.zone', '=', $forZone);
            }
//                            ->where('category','>=',$categ);
//                    if($categ!=0){
//                           $filtered=$filtered->where('districtcd','=',$dist);
//                    }
            $ordered = $filtered;
            $filtered_count = $filtered->count();
            //echo count ( $order );die;
            for ($i = 0; $i < count($order); $i ++) {
                $ordered = $ordered->orderBy($request->columns [$order [$i] ['column']] ['data'], strtoupper($order [$i] ['dir']));
            }
            $page_displayed = $ordered->offset($offset)->limit($length)->get();
            $data = array();
            if (!empty($page_displayed)) {
                foreach ($page_displayed as $dcrc_mas) {
                    //$cat_arr = config('constants.USER_CATEGORY');

                    $nestedData['dc_venue'] = $dcrc_mas->dc_venue;
                    $nestedData['dc_address'] = $dcrc_mas->dc_address;
                    $nestedData['rc_venue'] = $dcrc_mas->rc_venue;
                    $nestedData['rc_address'] = $dcrc_mas->rc_address;
                    $nestedData['dc_date'] = date('d/m/Y', strtotime(trim(str_replace('/', '-', $dcrc_mas->dc_date))));
                    $nestedData['dc_time'] = $dcrc_mas->dc_time;
                    $nestedData['zonename'] = $dcrc_mas->zonename;
                    $edit_button = $delete_button = $nestedData['dc_venue'] . '/' . $nestedData['dc_address'] . '/' . $nestedData['rc_venue'] . '/' . $nestedData['rc_address'] . '/' . $nestedData['zonename'] . '/' . $dcrc_mas->dcrcgrp;

                    $nestedData['action'] = array('e' => $edit_button, 'd' => $delete_button);
                    $data[] = $nestedData;
                }
            }
            $response = array(
                "draw" => $draw,
                "recordsTotal" => $dcrcs->count(), //Should be changed #7
                "recordsFiltered" => $filtered_count,
                'dcrc_masters' => $data //Should be changed #8
            );
        } catch (\Exception $e) {
            $response = array(
                'exception' => true,
                'exception_message' => $e->getMessage()
            );
            $statusCode = 400;
        } finally {
            return response()->json($response, $statusCode);
        }
    }

    public function check_for_edit_delete_dcrc(Request $request) {
        $statusCode = 200;
        $this->validate($request, [
            'data' => 'required|alpha_num|min:6|max:6',
                ], [
            'data.required' => 'DCRC code is required',
            'data.alpha_num' => 'DCRC code must be alpha numeric characters'
        ]);
        $dcrccd = $request->data;
        $check_exist = tbl_dcrc_party::where('dcrcgrp', '=', $dcrccd)->get();
        $check_count = $check_exist->count();
        if ($check_count == 0) {
            $data = tbl_dcrcmaster::select('dcrcmaster.dc_venue', 'dcrcmaster.dc_address', 'dcrcmaster.rc_venue', 'dcrcmaster.rc_address', 'dcrcmaster.dcrcgrp', DB::raw("(DATE_FORMAT(dcrcmaster.dc_date,'%d/%m/%Y')) as dc_date1"), 'dcrcmaster.dc_time')
                            ->where('dcrcmaster.dcrcgrp', '=', $dcrccd)->get();
        } else {
            $data = '';
        }
        $response = [
            'count' => $check_count,
            'rec' => $data
        ];
        return response()->json($response, $statusCode);
    }

    public function edit_drdc(Request $request) {
        $statusCode = 200;
        $update = null;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in form submit.');
            return response()->json($response, $statusCode);
        }


        $this->validate($request, [
            'dc_ven' => 'required|regex:/^[A-Za-z0-9\s,-.]+$/i|max:60',
            'dc_add' => 'required|regex:/^[A-Za-z0-9\s,-.]+$/i|max:60',
            'rc_ven' => 'required|regex:/^[A-Za-z0-9\s,-.]+$/i|max:60',
            'rc_add' => 'required|regex:/^[A-Za-z0-9\s,-.]+$/i|max:60',
            'dc_dt' => 'required|date_format:d/m/Y|min:10|max:10',
            'dc_tm' => 'required|max:15',
            // 'dc_tm' => 'required|regex:/^[A-Za-z0-9\s-.]+$/i|max:15',
            'dcrcgrp_code' => 'required|alpha_num|min:6|max:6',
                ], [
            'dc_ven.required' => 'DC venue is Required',
            'dc_ven.regex' => 'DC venue contain only alpha numeric characters',
            'dc_ven.max' => 'DC venue may not be greater than 60 characters',
            'dc_add.required' => 'DC address is Required',
            'dc_add.regex' => 'DC address contain only alpha numeric characters',
            'dc_add.max' => 'DC address may not be greater than 60 characters',
            'rc_ven.required' => 'RC venue is Required',
            'rc_ven.regex' => 'RC Venue contain only alpha numeric characters',
            'rc_ven.max' => 'RC Venue may not be greater than 60 characters',
            'rc_add.required' => 'RC address is Required',
            'rc_add.regex' => 'RC address contain only alpha numeric characters',
            'rc_add.max' => 'RC address may not be greater than 60 characters',
            'dcrcgrp_code.required' => 'DCRC code is required',
            'dcrcgrp_code.alpha_num' => 'DCRC code must be alpha numeric characters',
            'dc_dt.required' => 'DC Date is required',
            'dc_dt.date_format' => 'DC Date format should be d/m/y',
            'dc_tm.required' => 'DC Time is required',
            // 'dc_tm.regex' => 'DC Time must be an alpha numeric characters',        
            'dc_tm.max' => 'DC Time must not be greater than 15 characters'
        ]);
        $dcrcgrpcd = $request->dcrcgrp_code;
        $dcvenue = $request->dc_ven;
        $dcaddress = $request->dc_add;
        $rcvenue = $request->rc_ven;
        $rcaddress = $request->rc_add;
        $dcdate = $request->dc_dt;
        $dctime = $request->dc_tm;
        try {
             tbl_dcrcmaster::where('dcrcgrp', '=', $dcrcgrpcd)->update(['dc_venue' => $dcvenue, 'dc_address' => $dcaddress, 'rc_venue' => $rcvenue, 'rc_address' => $rcaddress, 'dc_date' => date('Y-m-d', strtotime(trim(str_replace('/', '-', $dcdate)))), 'dc_time' => $dctime]);

            $response = array(
                'status' => 1
            );
        } catch (\Exception $e) {
            $response = array(
                'exception' => true,
                'exception_message' => $e->getMessage(),
            );
            $statusCode = 400;
        } finally {
            return response()->json($response, $statusCode);
        }
    }

    public function delete_dcrc(Request $request) {
        $statusCode = 200;
        $record = null; //Should be changed #26
        $this->validate($request, [
            'data' => 'required|alpha_num|min:6|max:6'
                ], [
            'data.required' => 'DCRC Code is required',
            'data.alpha_num' => 'DCRC Code must be an alpha numeric characters'
        ]);
        try {
            $dcrcgrpcd = $request->data;
            $record = tbl_dcrcmaster::where('dcrcgrp', '=', $dcrcgrpcd); //Should be changed #27

            if (!empty($record)) {//Should be changed #30
                $record = $record->delete();
            }

            $response = array(
                'record' => $record //Should be changed #32
            );
        } catch (\Exception $e) {
            $response = array(
                'exception' => true,
                'exception_message' => $e->getMessage()
            );
            $statusCode = 400;
        } finally {
            return response()->json($response, $statusCode);
        }
    }

}
