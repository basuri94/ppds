<?php

namespace App\Http\Controllers;
use App\Http\Controllers\FirstTrainingAllocationController;
use App\tbl_block_muni;
use App\tbl_first_training_schedule;
use App\tbl_personnela;
use DB;
use Illuminate\Http\Request;

class FirstTrainingAllocationControllerblock extends Controller
{
    public function getMaxCPData() {
        $data=new FirstTrainingAllocationController;
        $training_type = $data->getTrainingTypeData();
        return view('first_training_allocation_maxcp_block', compact('training_type'));
    }
   public function getOtherblockData(Request $request){
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        }
        else
        {
             $this->validate($request, [
                'zone' => 'required|alpha_num|min:4|max:4'
                    ], [
                'zone.required' => 'Zone is required',
                'zone.alpha_num' => 'Zone must be an alpha numeric characters'
            ]);
            $zone = $request->zone;
            try {
                $filtered = "";
                $tbl_personnela = new tbl_personnela();
                $session_personnela = session()->get('personnela_ppds');
                $filtered = $tbl_personnela->join('office','office.officecd','=',$session_personnela . ".officecd")
                                ->join('block_muni', 'block_muni.blockminicd', '=','office.blockormuni_cd')
                                ->where('forzone', '=', $zone)
                                ->groupBy('block_muni.blockminicd')
                                ->pluck('block_muni.blockmuni','block_muni.blockminicd')->all();
                $response = array(
                    'options' => $filtered,
                    'status' => 1
                );
            }catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
              $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }
    public function firstTrAllocation_block(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            $validate_array = ['zone' => 'required|alpha_num|min:4|max:4',
            'phase' => 'required|integer',
                'subdivision' => 'required|alpha_num|min:4|max:4',
                'venuename' => 'required|alpha_num|min:6|max:6',
                'trainingtype' => 'required|alpha_num|min:2|max:2',
                'trainingdatetime' => 'required|alpha_num|min:4|max:4',
                //'othersubdiv' => 'required|alpha_num|min:1|max:1',
                'choiceblock' => 'nullable|alpha_num|required_if:othersubdiv,B|min:6|max:6',
                'assign' => 'required|integer',
                'maxcp' => 'required|digits_between:1,4'];
            for ($x = 1; $x <= $request->row_count; $x++) {
                $validate_array['s_pr' . $x] = 'nullable|integer';
                $validate_array['s_p1' . $x] = 'nullable|integer';
                $validate_array['s_p2' . $x] = 'nullable|integer';
                $validate_array['s_p3' . $x] = 'nullable|integer';
                $validate_array['s_pa' . $x] = 'nullable|integer';
                $validate_array['s_pb' . $x] = 'nullable|integer';
                $validate_array['subVenue' . $x] = 'nullable|alpha_num';
            }
            $validate_array1 = ['zone.required' => 'Zone is required',
            'phase.required'=> 'Phase is required',
            'phase.integer'=> 'Phase must be an integer',
                'zone.alpha_num' => 'Zone must be an alpha numeric characters',
                'subdivision.required' => 'Subdivision is required',
                'subdivision.alpha_num' => 'Subdivision must be an alpha numeric characters',
                'venuename.required' => 'Venuename is required',
                'venuename.alpha_num' => 'Venuename must be an alpha numeric characters',
                'trainingtype.required' => 'Training Type is required',
                'trainingtype.alpha_num' => 'Training Type must be an alpha numeric characters',
                'trainingdatetime.required' => 'Training Date & Time is required',
                'trainingdatetime.alpha_num' => 'Training Date & Time must be an alpha numeric characters',
                // 'othersubdiv.required' => 'Assign for other Subdivision is required',
                // 'othersubdiv.alpha_num' => 'Assign for other Subdivision must be an alpha numeric characters',
                'choiceblock.required_if' => 'Other Subdivision is required',
                'choiceblock.alpha_num' => 'Other Subdivision must be an alpha numeric characters',
                'assign.required' => 'Assigned atleast for one Subvenue',
                'assign.integer' => 'Assigned field must be an integer',
                'maxcp.required' => 'Max Capacity is required',
                'maxcp.digits_between' => 'Max Capacity must be an integer'];
            for ($y = 1; $y <= $request->row_count; $y++) {
                $validate_array1['s_pr' . $y . '.integer'] = 'PR must be an integer';
                $validate_array1['s_p1' . $y . '.integer'] = 'P1 must be an integer';
                $validate_array1['s_p2' . $y . '.integer'] = 'P2 must be an integer';
                $validate_array1['s_p3' . $y . '.integer'] = 'P3 must be an integer';
                $validate_array1['s_pa' . $y . '.integer'] = 'PA must be an integer';
                $validate_array1['s_pb' . $y . '.integer'] = 'PB must be an integer';
                $validate_array1['subVenue' . $y . '.alpha_num'] = 'Subvenue must be an alpha numeric characters';
            }
            $this->validate($request, $validate_array, $validate_array1);

            try {
                $row_count = $request->row_count;
                $subdivision = $request->subdivision;
                $venuename = $request->venuename;
                $trainingtype = $request->trainingtype;
                $trainingdatetime = $request->trainingdatetime;
               // $othersubdiv = $request->othersubdiv;
                $choiceblock = $request->choiceblock;
                $phase = $request->phase;
                $othersubdiv = 'B';
                $assign = $request->assign;
                $maxcp = $request->maxcp;
                $zone = $request->zone;
                $count = 0;
                $totalAssign = 0;
                for ($j = 1; $j <= $row_count; $j++) {
                    $t_assign = 's_assign' . $j;
                    $totalAssign = $totalAssign + $request->$t_assign;
                }

                if ($totalAssign == $assign && $maxcp >= $assign) {
                    $tbl_first_training_schedule = new tbl_first_training_schedule();
                    $duplicate_v_code = $tbl_first_training_schedule->where('tr_venue_cd', '=', $venuename)
                                    ->where('datetimecd', '=', $trainingdatetime)
                                    ->select(DB::raw('count(schedule_code) as cnt'))->get();
                    $du_code = json_decode($duplicate_v_code);
                    if ($du_code[0]->cnt != "0") {
                        $tbl_first_training_schedule->where('tr_venue_cd', '=', $venuename)
                                ->where('datetimecd', '=', $trainingdatetime)->delete();
                    }
                    for ($i = 1; $i <= $row_count; $i++) {
                        $s_pr = 's_pr' . $i;
                        $s_p1 = 's_p1' . $i;
                        $s_p2 = 's_p2' . $i;
                        $s_p3 = 's_p3' . $i;
                        $s_pa = 's_pa' . $i;
                        $s_pb = 's_pb' . $i;
                        $s_assign = 's_assign' . $i;
                        $s_subVenue = 'subVenue' . $i;
                        if ($request->$s_assign != "0") {
                            $count++;
                            $tbl_first_training_schedule = new tbl_first_training_schedule();
                            $max_sch_code = $tbl_first_training_schedule->where('tr_venue_cd', '=', $venuename)
                                            ->where('datetimecd', '=', $trainingdatetime)
                                            ->select(DB::raw('max(schedule_code) as cnt'))->get();
                            $max_code = json_decode($max_sch_code);
                            if ($max_code[0]->cnt == "") {
                                $schcode = $venuename . $trainingdatetime . "01";
                            } else {
                                $tmp_code = 100 + substr($max_code[0]->cnt, -2) + 1;
                                $schcode = $venuename . $trainingdatetime . substr($tmp_code, -2);
                            }

                            $s_pr_v = $request->$s_pr;
                            $s_p1_v = $request->$s_p1;
                            $s_p2_v = $request->$s_p2;
                            $s_p3_v = $request->$s_p3;
                            $s_pa_v = $request->$s_pa;
                            $s_pb_v = $request->$s_pb;
                            $s_subVenue_v = $request->$s_subVenue;
                            // if ($othersubdiv == 'S') {
                            //     $choice_block = $subdivision;
                            // } else {
                            //     $choice_block = $choiceblock;
                            // }
                            $choice_block = $choiceblock;
                            $user_code = session()->get('code_ppds');
                            $tbl_first_training_schedule->schedule_code = $schcode;
                            $tbl_first_training_schedule->forzone = $zone;
                            $tbl_first_training_schedule->subdivisioncd = $subdivision;
                            $tbl_first_training_schedule->tr_venue_cd = $venuename;
                            $tbl_first_training_schedule->tr_subvenue_cd = $s_subVenue_v;
                            $tbl_first_training_schedule->training_type = $trainingtype;
                            $tbl_first_training_schedule->datetimecd = $trainingdatetime;
                            $tbl_first_training_schedule->no_of_PR = $s_pr_v;
                            $tbl_first_training_schedule->no_of_P1 = $s_p1_v;
                            $tbl_first_training_schedule->no_of_P2 = $s_p2_v;
                            $tbl_first_training_schedule->no_of_P3 = $s_p3_v;
                            $tbl_first_training_schedule->no_of_PA = $s_pa_v;
                            $tbl_first_training_schedule->no_of_PB = $s_pb_v;
                            $tbl_first_training_schedule->choice_type = $othersubdiv;
                            $tbl_first_training_schedule->choice_block = $choice_block;
                            $tbl_first_training_schedule->usercode = $user_code;
                            $tbl_first_training_schedule->phase_id = $phase;

                            $tbl_first_training_schedule->save();
                        }
                    }
                    $response = array(
                        'options' => $count,
                        'status' => 1
                    );
//               }else{
//                   //::::::::::::::update:::::::::::::://
//                    $response = array(
//                       'options' => $count,
//                       'status' => 2 
//                    );
//               }
                } else {

                    $response = array(
                        'options' => $assign,
                        'status' => 0
                    );
                }
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }
}
