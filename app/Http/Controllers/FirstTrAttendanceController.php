<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\FirstTrainingAllocationController;
use App\tbl_first_training_schedule;
use App\tbl_training_attendance;
use App\tbl_personnela;
use DB;

class FirstTrAttendanceController extends Controller
{
    private $firstTrainingAllocation;
    public function initVenueTrData(FirstTrainingAllocationController $firstTrainingAllocation){
        $this->firstTrainingAllocation=$firstTrainingAllocation;
        $training_type= $this->firstTrainingAllocation->getTrainingTypeData();
        return view('first_training_attendance', compact('training_type'));
    }
    public function getPPForTrAttendance(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
             $this->validate($request, [ 
                'subdivision' => 'required|alpha_num|min:4|max:4',
                'venuename' => 'required|alpha_num|min:6|max:6',
                'subvenuename' => 'required|alpha_num|min:8|max:8',
                'trainingtype' => 'required|alpha_num|min:2|max:2',
                'trainingdatetime' => 'required|alpha_num|min:4|max:4'
             ], [
                'subdivision.required' => 'Subdivision is required',
                'subdivision.alpha_num' => 'Subdivision must be an alpha numeric',
                'venuename.required' => 'Venuename is required',
                'venuename.alpha_num' => 'Venuename must be an alpha numeric characters',
                'subvenuename.required' => 'SubVenue name is required',
                'subvenuename.alpha_num' => 'SubVenue name must be an alpha numeric characters',
                'trainingtype.required' => 'Training Type is required',
                'trainingtype.alpha_num' => 'Training Type must be an alpha numeric characters',
                'trainingdatetime.required' => 'Training Date & Time is required',
                'trainingdatetime.alpha_num' => 'Training Date & Time must be an alpha numeric characters'
            ]);
            $subdivision=$request->subdivision;
            $venuename=$request->venuename;
            $subvenuename=$request->subvenuename;
            $trainingtype=$request->trainingtype;
            $trainingdatetime=$request->trainingdatetime;
            try {
                $filtered = '';
                $tr_array = config('constants.PP_TRAINING');
                // dd($tr_array);
                $tr_Name = $tr_array[$trainingtype];
                // dd($tr_Name);
                $tbl_first_training_schedule = new tbl_first_training_schedule;
                if($tr_Name=="1stTrainingSch"){
                    $tbl_first_training_schedule = $tbl_first_training_schedule->with('ppDetailVenueBrief');
                }else{
                    $tbl_first_training_schedule = $tbl_first_training_schedule->with('ppDetailVenueBrief2');
                }
                 $ApptRecord= $tbl_first_training_schedule->where('first_training_schedule.tr_venue_cd','=', $venuename)
                        ->where('first_training_schedule.datetimecd','=', $trainingdatetime)
                        ->where('first_training_schedule.training_type','=', $trainingtype)
                        ->where('first_training_schedule.subdivisioncd','=', $subdivision)
                         ->where('first_training_schedule.tr_subvenue_cd','=', $subvenuename) 
                        //->where(''.$session_personnela.'.officecd','=',$officeName) 
                        ->select('first_training_schedule.schedule_code')
                        ->get();
                $subV=json_decode($ApptRecord); 
                $data = array();
                if (!empty($subV)) {
                    foreach ($subV as $subTe) {
                        
                        if($tr_Name=="1stTrainingSch"){ 
                        $subTe_array=$subTe->pp_detail_venue_brief;
                       }else{
                        $subTe_array=$subTe->pp_detail_venue_brief2;   
                       }
                        $subTe_array_count = count($subTe_array);
                        if ($subTe_array_count > 0) {
                            foreach ($subTe_array as $stud_sub) {
                                if($tr_Name=="1stTrainingSch"){ 
                                   $sch="1stTrainingSch";
                                }else{
                                    $sch="1stTrainingSch_2"; 
                                }
                                $nestedData['schedule_code'] = $stud_sub->$sch;
                                $nestedData['personcd'] = $stud_sub->personcd;
                                $nestedData['officer_name'] = $stud_sub->officer_name;
                                $partyReserve = tbl_training_attendance::where('per_code', '=', $stud_sub->personcd)
                                        ->where('training_type', '=', $trainingtype)
                                        ->select(DB::raw('count(*) as cnt'))
                                        ->get();
                                $nestedData['pStatus'] = $partyReserve[0]->cnt;
                                $data[] = $nestedData;
                            }
                        }
                      
                    }
                }
                $response = array(
                    'options' => $data,
                    'status' => 1);
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }
    
    public function first_tr_attendance(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
        // echo $request->dcvenue;die;
            $validate_array = ['subdivision' => 'required|alpha_num|min:4|max:4',
                'venuename' => 'required|alpha_num|min:6|max:6',
                'subvenuename' => 'required|alpha_num|min:8|max:8',
                'trainingtype' => 'required|alpha_num|min:2|max:2',
                'schedule_code' => 'required|alpha_num|min:12|max:12',
                'trainingdatetime' => 'required|alpha_num|min:4|max:4'];

            for ($x = 0; $x <= $request->row_count; $x++) {
                $validate_array['myCheck' . $x] = 'nullable|alpha_num|min:11|max:11';
            }
            $validate_array1 = [ 'subdivision.required' => 'Subdivision is required',
                'subdivision.alpha_num' => 'Subdivision must be an alpha numeric',
                'venuename.required' => 'Venuename is required',
                'venuename.alpha_num' => 'Venuename must be an alpha numeric characters',
                'trainingtype.required' => 'Training Type is required',
                'trainingtype.alpha_num' => 'Training Type must be an alpha numeric characters',
                'schedule_code.required' => 'Schedule Code is required',
                'schedule_code.alpha_num' => 'Schedule Code must be an alpha numeric characters',
                'trainingdatetime.required' => 'Training Date & Time is required',
                'trainingdatetime.alpha_num' => 'Training Date & Time must be an alpha numeric characters'
            ];
            for ($y = 0; $y <= $request->row_count; $y++) {
                $validate_array1['myCheck' . $y . '.alpha_num'] = 'Personnel ID must be an alpha numeric characters';
            }
            $this->validate($request, $validate_array, $validate_array1);
            try {
                $user_code = session()->get('code_ppds');
                $row_count = $request->row_count;
                $subdivision=$request->subdivision;
                $schedule_code=$request->schedule_code;
                $venuename=$request->venuename;
                $trainingtype=$request->trainingtype;
                $trainingdatetime=$request->trainingdatetime;
                $count = 0;
                $status = 0;
                $Ofccount = 0;
                for ($i = 1; $i <= $row_count; $i++) {
                    $pID = 'myCheck' . $i;
                    if ($request->$pID != "") {
                        $per_id = $request->$pID;
                        $tbl_tr_at = new tbl_training_attendance();
                        $PCODE = tbl_training_attendance::where('per_code', '=', $per_id)
                                        ->where('training_type', '=', $trainingtype)
                                        ->value(DB::raw('count(*)'));
                        if($PCODE==0){
                            $tbl_tr_at->per_code = $per_id;
                            $tbl_tr_at->subdivision = $subdivision;
                            $tbl_tr_at->training_type = $trainingtype;
                            $tbl_tr_at->usercode = $user_code;
                            $tbl_tr_at->schedule_code = $schedule_code;
                            $tbl_tr_at->save();
                            $count++;
                        }
                      $person[$Ofccount]=$per_id;
                      $Ofccount++;
                    }
                }
                 tbl_training_attendance::where('schedule_code', '=', $schedule_code)
                             ->where('training_type', '=', $trainingtype)
                             ->whereNotIn('per_code',$person)
                             ->delete();
                $response = array(
                    'options' => $count,
                    'status' => 1);
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }
    
    public function getFirstTrAttenExcel(Request $request) {
            $this->validate($request, [ 
                'subdivision' => 'required|alpha_num|min:4|max:4',
                'venuename' => 'required|alpha_num|min:6|max:6',
                'subvenuename' => 'required|alpha_num|min:8|max:8',
                'records' => 'required|alpha|min:1|max:1',
                'trainingtype' => 'required|alpha_num|min:2|max:2',
                'trainingdatetime' => 'required|alpha_num|min:4|max:4'
                ], [
                'subdivision.required' => 'Subdivision is required',
                'subdivision.alpha_num' => 'Subdivision must be an alpha numeric',
                'venuename.required' => 'Venuename is required',
                'venuename.alpha_num' => 'Venuename must be an alpha numeric characters',
                'trainingtype.required' => 'Training Type is required',
                'trainingtype.alpha_num' => 'Training Type must be an alpha numeric characters',
                'trainingdatetime.required' => 'Training Date & Time is required',
                'trainingdatetime.alpha_num' => 'Training Date & Time must be an alpha numeric characters'
            ]);            
            
            try
            {
                $subdivision=$request->subdivision;
                $venuename=$request->venuename;
                $trainingtype=$request->trainingtype;
                $trainingdatetime=$request->trainingdatetime;
                $subvenuename=$request->subvenuename;
                $records=$request->records;
                $session_personnela=session()->get('personnela_ppds');
                $data = array();
                $tbl_personnela = new tbl_personnela;
                if($records=="C")
                {
                   $tbl_personnela = $tbl_personnela->where('training_attended','=', 'C');
                }else if($records=="Y"){
                   $tbl_personnela = $tbl_personnela->where('training_attended','=', 'Y'); 
                }
                $tr_array = config('constants.PP_TRAINING');
                $tr_Name = $tr_array[$trainingtype];
                if($tr_Name=="1stTrainingSch"){ 
                    $scCode = ''.$session_personnela.'.1stTrainingSch';
                }else{
                    $scCode = ''.$session_personnela.'.1stTrainingSch_2';
                }
                $ApptRecord = $tbl_personnela->join('office',''.$session_personnela.'.officecd','=','office.officecd')
                           ->join('personnel',''.$session_personnela.'.personcd','=','personnel.personcd')
                           ->join('poststat',''.$session_personnela.'.poststat','=','poststat.post_stat')
                           ->leftjoin('policestation','office.policestn_cd','=','policestation.policestationcd')
                           ->join('district','personnel.districtcd','=','district.districtcd')
                           ->join('subdivision','office.subdivisioncd','=','subdivision.subdivisioncd')
                           ->join('training_attendance',''.$session_personnela.'.personcd','=','training_attendance.per_code')
                           ->leftjoin('first_training_schedule',$scCode,'=','first_training_schedule.schedule_code')
                           ->join('first_training_venue','first_training_schedule.tr_venue_cd','=','first_training_venue.venue_cd')
                           ->join('first_training_subvenue','first_training_schedule.tr_subvenue_cd','=','first_training_subvenue.subvenue_cd')
                           ->join('first_training_date_time','first_training_schedule.datetimecd','=','first_training_date_time.datetime_cd')
                           ->join('training_type','first_training_schedule.training_type','=','training_type.training_code')
                            ->where('first_training_schedule.tr_venue_cd','=', $venuename)
                            ->where('first_training_schedule.tr_subvenue_cd','=', $subvenuename)
                            ->where('first_training_schedule.datetimecd','=', $trainingdatetime)
                            ->where('first_training_schedule.training_type','=', $trainingtype)
                            ->where('training_attendance.training_type', '=', $trainingtype)
                            ->where('first_training_schedule.subdivisioncd','=', $subdivision)
                            ->select(DB::raw("DATE_FORMAT(personnel.dateofbirth,'%d/%m/%Y') as dateofbirth"),''.$session_personnela.'.personcd','district.district','office.address1','office.address2','postoffice','pin'
                                    ,'policestation.policestation','personnel.mob_no','subdivision.subdivision','personnel.officecd','personnel.officer_name','personnel.off_desg','personnel.present_addr1','personnel.present_addr2'
                                    ,'poststat.poststatus',''.$session_personnela.'.poststat',''.$session_personnela.'.booked','personnel.gender','personnel.epic','forassembly','groupid','personnel.subdivisioncd','office.office'
                                    ,'first_training_venue.venuename','first_training_subvenue.subvenue','first_training_date_time.training_dt','training_time','training_type.training_desc')
                            ->get();
                foreach ($ApptRecord as $ApptDetail)
                {
                    //$nestedData['#SL']=$ApptDetail->sl_no;
//                    $nestedData['Token']=$ApptDetail->token;
//                    $nestedData['PC Code']=$ApptDetail->forpc;
//                    $nestedData['PC Name']=$ApptDetail->pcname;
                    $nestedData['PID']=$ApptDetail->personcd;
                    $nestedData['Person Name']=$ApptDetail->officer_name;
                    $nestedData['Person Designation']=$ApptDetail->off_desg;                   
                    $nestedData['Office Code']=$ApptDetail->officecd;
                    $nestedData['Office Name']=$ApptDetail->office;
                    $nestedData['Office Address']=$ApptDetail->address1;
//                    $nestedData['Office Address2']=$ApptDetail->address2;
                    $nestedData['Post Office']=$ApptDetail->postoffice;
                    $nestedData['Subdivision']=$ApptDetail->subdivision;
                    $nestedData['Pin']=$ApptDetail->pin;
                    $nestedData['Post Status']=$ApptDetail->poststatus;
                    $nestedData['Mobile No']=$ApptDetail->mob_no;
                    
                    $nestedData['Training Type']=$ApptDetail->training_desc;
                    $nestedData['Venue Name']=$ApptDetail->venuename;
                    $nestedData['Subvenue']=$ApptDetail->subvenue;
                    $nestedData['Training Date']=$ApptDetail->training_dt;
                    $nestedData['Training Time']=$ApptDetail->training_time;
//                    $nestedData['BlockMuni Code']=$ApptDetail->block_muni;
//                    $nestedData['BlockMuni Name']=$ApptDetail->block_muni_name;
//                    //$nestedData['District Code']=$ApptDetail->fordistrict;
//                    $nestedData['District Name']=$ApptDetail->district;
//                    $nestedData['Pin']=$ApptDetail->pin;
//                    $nestedData['Poststat']=$ApptDetail->poststat;
//                    $nestedData['Post Status']=$ApptDetail->post_stat;
//                    $nestedData['Mobile No']=$ApptDetail->mob_no;
//                    
//                    $nestedData['Epic']=$ApptDetail->epic;
//                    $nestedData['AC NO']=$ApptDetail->acno;
//                    $nestedData['PART NO']=$ApptDetail->partno;
//                    $nestedData['SL NO']=$ApptDetail->slno;
//                    
//                    $nestedData['Bank']=$ApptDetail->bank_name;
//                    $nestedData['Branch']=$ApptDetail->branch_name;
//                    $nestedData['A/C No']=$ApptDetail->bank_acc_no;
//                    $nestedData['IFSC']=$ApptDetail->ifsc_code;
//                    
//                    $nestedData['Training Type']=$ApptDetail->training_desc;
//                    $nestedData['Venue Name']=$ApptDetail->venuename." , ".$ApptDetail->sub_venuename;
//                    $nestedData['Venue Address']=$ApptDetail->venueaddress;
//                    $nestedData['Training Date']=$ApptDetail->training_dt;
//                    $nestedData['Training Time']=$ApptDetail->training_time;
                    $data[]=$nestedData;
                }
                if($records!="Y"){
                 tbl_training_attendance::join('first_training_schedule','training_attendance.schedule_code','=','first_training_schedule.schedule_code')
                          ->where('first_training_schedule.tr_venue_cd','=', $venuename)
                          ->where('first_training_schedule.tr_subvenue_cd','=', $subvenuename)
                          ->where('first_training_schedule.datetimecd','=', $trainingdatetime)
                          ->where('training_attendance.training_type', '=', $trainingtype)
                          ->where('training_attendance.training_attended','=', 'C')
                          ->update(['training_attended'=>'Y']); 
                }
                
                return \Excel::create('1stTrainingAttendanceExcel', function($excel) use ($data) {
                    $excel->sheet(' ', function($sheet) use ($data) {
                        $sheet->fromArray($data);
                    });
                })->download('xlsx');          
                
            }
            catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
             // $statusCode = 400;
                return $response;
            } 
        
        
    }
}
