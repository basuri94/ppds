<?php

namespace App\Http\Controllers;
use App\Http\Controllers\FirstTrainingAllocationController;
use App\tbl_first_training_schedule;
use App\tbl_training_attendance;
use App\tbl_personnela;
use DB;
use App\Personnel;
use App\Office;
use App\tbl_assembly;
use App\Language;
use App\Qualification;
use App\Remark;
use App\Poststatus;
use Session;
use Validator;
use App\tbl_office;
use Illuminate\Http\Request;

class PersonalControllerAfterFirst extends Controller
{
  private $firstTrainingAllocation;
    public function index_personal(FirstTrainingAllocationController $firstTrainingAllocation){
        $this->firstTrainingAllocation=$firstTrainingAllocation;
        $training_type= $this->firstTrainingAllocation->getTrainingTypeData();
        return view('personal_after_first_training', compact('training_type'));
    }
    public function getPPForTrAttendance(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
             $this->validate($request, [ 
                'subdivision' => 'required|alpha_num|min:4|max:4',
                'venuename' => 'required|alpha_num|min:6|max:6',
                'subvenuename' => 'required|alpha_num|min:8|max:8',
                'trainingtype' => 'required|alpha_num|min:2|max:2',
                'trainingdatetime' => 'required|alpha_num|min:4|max:4'
             ], [
                'subdivision.required' => 'Subdivision is required',
                'subdivision.alpha_num' => 'Subdivision must be an alpha numeric',
                'venuename.required' => 'Venuename is required',
                'venuename.alpha_num' => 'Venuename must be an alpha numeric characters',
                'subvenuename.required' => 'SubVenue name is required',
                'subvenuename.alpha_num' => 'SubVenue name must be an alpha numeric characters',
                'trainingtype.required' => 'Training Type is required',
                'trainingtype.alpha_num' => 'Training Type must be an alpha numeric characters',
                'trainingdatetime.required' => 'Training Date & Time is required',
                'trainingdatetime.alpha_num' => 'Training Date & Time must be an alpha numeric characters'
            ]);
            $subdivision=$request->subdivision;
            $venuename=$request->venuename;
            $subvenuename=$request->subvenuename;
            $trainingtype=$request->trainingtype;
            // dd($trainingtype);
            $trainingdatetime=$request->trainingdatetime;
            try {
                $filtered = '';
                $tr_array = config('constants.PP_TRAINING');
                // dd($tr_array[$trainingtype]);
                $tr_Name = $tr_array[$trainingtype];
                $tbl_first_training_schedule = new tbl_first_training_schedule;
                if($tr_Name=="1stTrainingSch"){
                    $tbl_first_training_schedule = $tbl_first_training_schedule->with('ppDetailVenueBrief');
                }else{
                    $tbl_first_training_schedule = $tbl_first_training_schedule->with('ppDetailVenueBrief2');
                }
                 $ApptRecord= $tbl_first_training_schedule->where('first_training_schedule.tr_venue_cd','=', $venuename)
                        ->where('first_training_schedule.datetimecd','=', $trainingdatetime)
                        ->where('first_training_schedule.training_type','=', $trainingtype)
                        ->where('first_training_schedule.subdivisioncd','=', $subdivision)
                         ->where('first_training_schedule.tr_subvenue_cd','=', $subvenuename) 
                        //->where(''.$session_personnela.'.officecd','=',$officeName) 
                        ->select('first_training_schedule.schedule_code')
                        ->get();
                        // dd($ApptRecord);
                $subV=json_decode($ApptRecord); 
                // dd($subV);
                $data = array();
                if (!empty($subV)) {
                    foreach ($subV as $subTe) {
                        
                        if($tr_Name=="1stTrainingSch"){ 
                        $subTe_array=$subTe->pp_detail_venue_brief;
                       }else{
                        $subTe_array=$subTe->pp_detail_venue_brief2;   
                       }
                        $subTe_array_count = count($subTe_array);
                        if ($subTe_array_count > 0) {
                            foreach ($subTe_array as $stud_sub) {
                                if($tr_Name=="1stTrainingSch"){ 
                                   $sch="1stTrainingSch";
                                }else{
                                    $sch="1stTrainingSch_2"; 
                                }
                                $nestedData['schedule_code'] = $stud_sub->$sch;
                                $nestedData['personcd'] = $stud_sub->personcd;
                                $nestedData['officer_name'] = $stud_sub->officer_name;
                                
                                $data[] = $nestedData;
                            }
                        }
                      
                    }
                }
                $response = array(
                    'options' => $data,
                    'status' => 1);
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }
     public function getPersonnelByid(Request $request){
      
        //  $data=Personnel::leftjoin('pmid_15pp','personnel.epic','=','pmid_15pp.IDCARD_NO')->where('personcd',$request->edit_code)->get();
         $data=Personnel::where('personcd',$request->edit_code)->get();
        //$dataOf=$this->getAllOffice();
        return view('Personnel_edit',['person' => $data,'ac'=> tbl_assembly::where('districtcd',session()->get('districtcd_ppds'))->get(),'language'=> Language::get(),'qualification'=> Qualification::get(),'remark'=> Remark::get(),'poststatus'=> Poststatus::get()]);

// return view('Personnel_edit',compact('data'))->with('personnelDataEdit',$personnelDataEdit);
        } 
    // public function update(Request $request)
//     {
//         $statusCode = 200;
//         if (!$request->ajax()) {
//             $statusCode = 400;
//             $response = array('error' => 'Error occured in Ajax Call.');
//             return response()->json($response, $statusCode);
//         }
//         else
//         { 
//             $validator =Validator::make($request->all(),[
//              'officer_name' => 'required|string|max:50',
//              'off_desg' => 'required|string|max:50',
//              'resi_no'=> 'max:15',
//              'present_addr1' => 'required|string|max:100',
//              'perm_addr1' => 'required|string|max:100',
//              'dateofbirth' => 'required|date',
//              'gender' => 'required',
//              'scale' => 'required|max:15',
//              'basic_pay' => 'required|numeric|max:9999999',
//              'grade_pay' => 'required|numeric',
//              'pgroup' => 'required',
//              'workingstatus' => 'required',
//              'mob_no' => 'required|digits:10',
//              'email' => 'required|email',
//              'qualificationcd' => 'required',
//              'languagecd' => 'required',
//              'epic' => 'required|max:20',
//              'partno' => 'numeric|max:9999',
//              'slno' => 'numeric|max:9999',
//              'assembly_temp' => 'required|numeric',
//              'assembly_off' => 'required|numeric',
//              'assembly_perm' => 'required|numeric',
//              'branch_ifsc' => 'required|max:11',
//              //'bank_acc_no' => 'required|unique:personnel,bank_acc_no|max:16'



//          ]);
//         try{
//          //   echo 'hi';die;
      
// //if($validator->fails()) {
// //			return redirect()->back()->withErrors($validator)->withInput();
// //          }
//     $personnelId=$request->personnel_id;

//         $personnel =array();

       
//         $personnel['officer_name']= strip_tags($request->officer_name,'');
//         $personnel['off_desg']= strip_tags($request->off_desg,'');
//        /* $personnel['qualificationcd']= strip_tags($request->qualificationcd,'');
//         $personnel['languagecd']= strip_tags($request->languagecd,'');
//         $personnel['dateofbirth']= strip_tags($request->dateofbirth,'');
//         $personnel['gender']= strip_tags($request->gender,'');

//         $personnel['scale']= strip_tags($request->scale,'');
//         $personnel['basic_pay']= strip_tags($request->basic_pay,'');
//         $personnel['grade_pay']= strip_tags($request->grade_pay,'');
//         $personnel['workingstatus']= strip_tags($request->workingstatus,'');
//         $personnel['pgroup']= strip_tags($request->pgroup,'');

//         $personnel['email']= strip_tags($request->email,'');
//         $personnel['resi_no']= strip_tags($request->resi_no,'');*/
//         $personnel['mob_no']= strip_tags($request->mob_no,'');
//        /* $personnel['present_addr1']=  strip_tags($request->present_addr1,'');
//         $personnel['perm_addr1']=strip_tags($request->perm_addr1,'');
//         $personnel['present_addr2']=  strip_tags($request->present_addr2,'');
//         $personnel['perm_addr2']=strip_tags($request->perm_addr2,'');*/

//         $personnel['epic']= strip_tags($request->epic,'');
//         $personnel['partno']= strip_tags($request->partno,'');
//         $personnel['slno']= strip_tags($request->slno,'');
//         //$personnel['poststat']= strip_tags($request->poststat,'');
//         //$personnel['assembly_off']= strip_tags($request->assembly_off,'');
//         //$personnel['assembly_perm']= strip_tags($request->assembly_perm,'');
//         $personnel['assembly_temp']= strip_tags($request->assembly_temp,'');
// 		$personnel['acno']= strip_tags($request->assembly_temp,'');
//         $personnel['branch_ifsc']= strip_tags($request->branch_ifsc,'');
//         $personnel['bank_acc_no']= strip_tags($request->bank_acc_no,'');
//         $personnel['updated']='Y';
    
       
		
// 		$personnela =array();
//        // $personnela['gender']= strip_tags($request->gender,'');
//        // $personnela['poststat']= strip_tags($request->poststat,'');
//         //$personnela['assembly_off']= strip_tags($request->assembly_off,'');
//         //$personnela['assembly_perm']= strip_tags($request->assembly_perm,'');
//         $personnela['assembly_temp']= strip_tags($request->assembly_temp,'');
// 		$personnela['updated']='Y';
//         // echo '<hr>';
//         // print_r($personnel);
//         // die();
//         //Update personnel
// 		//echo json_encode(personnel);die;
// 		//echo json_encode(personnela);die;
//         DB::table('personnel')->where('personcd', $personnelId)->update($personnel);
// 		//Update personnela
// 	$updatePersoneel=DB::table('personnela'.session()->get('districtcd_ppds'))->where('personcd', $personnelId)->update($personnela);
//         //return redirect('edit_personnel/'.$personnelId.'?save=1');
    
//         $response=array('updatePersoneel'=>$updatePersoneel,
//             'status'=>1);      
                
// }  catch (\Exception $e) {
//             $response = array(
//                 'exception' => true,
//                 'exception_message' => $e->getMessage()
//             );
//             $statusCode = 400;
//         } finally {
//             return response()->json($response, $statusCode);
//         }
//         }
        
// }
public function get_epic_det(Request $request) {
   $data=DB::table('pmid_15pp')->where('IDCARD_NO','=',$request->epic)->get(); 
  
     return $data;  
   
}
}
