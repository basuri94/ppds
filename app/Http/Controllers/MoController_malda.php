<?php

namespace App\Http\Controllers;

use App\tbl_personnela;
use DB;
use Illuminate\Http\Request;

class MoController extends Controller {

    public function mo_detailsCount(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            $validate_array = ['forZone' => 'required|alpha_num|min:4|max:4',
                'assembly' => 'nullable|alpha_num|min:3|max:3',
            ];

            $validate_array1 = ['forZone.required' => 'For Zone is required',
                'forZone.alpha_num' => 'For Zone must be an alpha numeric characters',
                'assembly.alpha_num' => 'Assembly must be an alpha numeric characters'];

            $this->validate($request, $validate_array, $validate_array1);

            try {
                $forzone = $request->forZone;
                $assembly = $request->assembly;
                $count = 0;
                $max_count = "";
                $session_personnela = session()->get('personnela_ppds');
                $tbl_personnela1 = new tbl_personnela();
                $recordsPost = $tbl_personnela1->rightjoin('second_rand_table', '' . $session_personnela . '.personcd', '=', 'second_rand_table.pr_personcd');

                if ($assembly != '') {
                    $recordsPost = $recordsPost->where('' . $session_personnela . '.forassembly', $assembly)->where('' . $session_personnela . '.forzone', $forzone)->where('second_rand_table.per_poststat', 'PR');
                }

                $recordsPost = $recordsPost->select(DB::raw('count(*) as cnt'))->get();
                $max_count = json_decode($recordsPost);
                $count = $max_count[0]->cnt;
                $response = array(
                    'options' => $count,
                    'status' => 1
                );
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }

    public function moDetailsEXCEL(Request $request) {
        $validate_array = ['forZone' => 'required|alpha_num|min:4|max:4',
            'assembly' => 'nullable|alpha_num|min:3|max:3',
        ];

        $validate_array1 = ['forZone.required' => 'For Zone is required',
            'forZone.alpha_num' => 'For Zone must be an alpha numeric characters',
            'assembly.alpha_num' => 'Assembly must be an alpha numeric characters'
        ];

        $this->validate($request, $validate_array, $validate_array1);
        try {
            $forzone = $request->forZone;
            $assembly = $request->assembly;
            $count = 0;
            $session_personnela = session()->get('personnela_ppds');
            $tbl_personnela1 = new tbl_personnela();
            $recordsPost = $tbl_personnela1->rightjoin('second_rand_table', '' . $session_personnela . '.personcd', '=', 'second_rand_table.pr_personcd');

            if ($assembly != '') {
                $recordsPost = $recordsPost->where('' . $session_personnela . '.forassembly', $assembly)->where('' . $session_personnela . '.forzone', $forzone)->where('second_rand_table.per_poststat', 'PR');
            }

            $recordsPost = $recordsPost->select('second_rand_table.assembly_name', 'second_rand_table.pr_personcd', 'second_rand_table.pr_name', 'second_rand_table.pr_designation', 'second_rand_table.pr_officecd', 'second_rand_table.pr_officename', 'second_rand_table.pr_mobno')->orderBy(DB::raw("second_rand_table.assembly,rand()"))->get();
            //dd($recordsPost);
            $data = array();
            foreach ($recordsPost as $ApptDetail) {
                $nestedData['Assembly Name'] = $ApptDetail->assembly_name;
                $nestedData['Person Code'] = $ApptDetail->pr_personcd;
                $nestedData['Name'] = $ApptDetail->pr_name;
                $nestedData['Designation'] = $ApptDetail->pr_designation;
                $nestedData['Office Code'] = $ApptDetail->pr_officecd;
                $nestedData['Office Name'] = $ApptDetail->pr_officename;
                $nestedData['Mobile No'] = $ApptDetail->pr_mobno;

                $data[] = $nestedData;
            }
            return \Excel::create('AC_wise_Mo_Details(' . $assembly . ')', function($excel) use ($data) {
                        $excel->sheet(' ', function($sheet) use ($data) {
                            $sheet->fromArray($data);
                        });
                    })->download('xlsx');
        } catch (\Exception $e) {
            $response = array(
                'exception' => true,
                'exception_message' => $e->getMessage(),
            );
            // $statusCode = 400;
            return $response;
        }
    }

    public function mo_pdf(Request $request) {
        $validate_array = ['forZone' => 'required|alpha_num|min:4|max:4',
            'assembly' => 'nullable|alpha_num|min:3|max:3',
        ];

        $validate_array1 = ['forZone.required' => 'For Zone is required',
            'forZone.alpha_num' => 'For Zone must be an alpha numeric characters',
            'assembly.alpha_num' => 'Assembly must be an alpha numeric characters'
        ];

        $this->validate($request, $validate_array, $validate_array1);
        $forzone = $request->forZone;
        $assembly = $request->assembly;
        $session_personnela = session()->get('personnela_ppds');
        $tbl_personnela1 = new tbl_personnela();
        $recordsPost = $tbl_personnela1->rightjoin('second_rand_table', '' . $session_personnela . '.personcd', '=', 'second_rand_table.pr_personcd');

        if ($assembly != '') {
            $recordsPost = $recordsPost->where('' . $session_personnela . '.forassembly', $assembly)->where('' . $session_personnela . '.forzone', $forzone)->where('second_rand_table.per_poststat', 'PR');
        }

        $recordsPost = $recordsPost->select('second_rand_table.assembly_name', 'second_rand_table.pr_personcd', 'second_rand_table.pr_name', 'second_rand_table.pr_designation', 'second_rand_table.pr_officecd', 'second_rand_table.pr_officename', 'second_rand_table.pr_mobno')->orderBy(DB::raw("second_rand_table.assembly,rand()"))->get();

        $pdf = app('FPDF');
        $pdf->AddPage();
        $count = 0;
        $per_page = 1;
        $subTe1 = [$recordsPost[0]->assembly_name];
        foreach ($subTe1 as $subTe) {
            if ($count < $per_page) {
                $fill = false;

                $venue = "Assembly Name : " . $subTe1[0];


                $pdf->Ln();
                $pdf->SetFont('Arial', 'B', 8);
                $pdf->Cell(195, 5, $venue, "LTR", 0, 'L');

                $pdf->Ln();

                $pdf->SetFillColor(255, 255, 255);
                //	$this->SetTextColor(0,0,0);
                $pdf->SetDrawColor(0, 0, 0);
                $pdf->SetLineWidth(.3);
                $pdf->SetFont('Arial', 'B', 8);

                $head = array('SL#', 'Person ID', 'Name', 'Designation', 'Office Name','Signature',);
                //    $header1 = array('','','','','','AC / Part /Sl No.','');
                $w = array(8,15,33,47,58,34);
                $pdf->SetFont('Arial', '', 7.9);
                for ($j = 0; $j < count($head); $j++)
                $pdf->Cell($w[$j], 7, $head[$j], 'LTR', 0, 'L', true);
                $pdf->Ln();
                // for($l=0;$l<count($header1);$l++)
                //   $pdf->Cell($w[$l],4,$header1[$l],'LR',0,'L',true);
                //  $pdf->Ln();

                $subTe_array_count = count($recordsPost);
                if ($subTe_array_count > 0) {
                    $count1 = 0;
                    $per_page1 = 35;
                    foreach ($recordsPost as $stud_sub) {
                        if ($count1 < $per_page1) {
                            $count1++;
                            // $enrolmentjoin = $stud_sub->acno."/".$stud_sub->partno."/".$stud_sub->slno;
                            $pdf->SetFont('', '', 6);
                            $pdf->Cell($w[0], 7, $count1, 'LRT', 0, 'L', $fill);
                            $pdf->Cell($w[1], 7, $stud_sub->pr_personcd, 'LRT', 0, 'L', $fill);
                            $x_axis1 = $pdf->getx();
                            $c_height1 = 7;
                            $c_width1 = $w[2];
                            $w_w1 = $c_height1 / 3;
                            $w_w_11 = $w_w1 + 2;
                            $w_w11 = $w_w1 + $w_w1 + $w_w1 + 3;
// $w_w2=$w_w+$w_w+$w_w+$w_w+3;// for 3 rows wrap
                            $text1 = $stud_sub->pr_name.",".$stud_sub->pr_mobno;
                            $len1 = strlen($text1); // check the length of the cell and splits the text into 7 character each and saves in a array 
                            if ($len1 > 20) {
                                $w_text1 = str_split($text1, 20); // splits the text into length of 7 and saves in a array since we need wrap cell of two cell we took $w_text[0], $w_text[1] alone.
// if we need wrap cell of 3 row then we can go for    $w_text[0],$w_text[1],$w_text[2]
                                $pdf->SetX($x_axis1);
                                $pdf->Cell($c_width1, $w_w_11, $w_text1[0], '', '', '');
                                $pdf->SetX($x_axis1);
                                $pdf->Cell($c_width1, $w_w11, $w_text1[1], '', '', '');
//$this->SetX($x_axis);
// $this->Cell($c_width,$w_w2,$w_text[2],'','','');// for 3 rows wrap but increase the $c_height it is very important.
                                $pdf->SetX($x_axis1);
                                $pdf->Cell($c_width1, $c_height1, '', 'LTRB', 0, 'L', 0);
                            } else {
                                $pdf->SetX($x_axis1);
                                $pdf->Cell($c_width1, $c_height1, $text1, 'LTRB', 0, 'L', 0);
                            }
                            // $pdf->Cell($w[2], 7, $stud_sub->pr_name, 'LRT', 0, 'L', $fill);
                            //$pdf->Cell($w[3], 7, $stud_sub->pr_designation, 'LRT', 0, 'L', $fill);
                            $x_axis2 = $pdf->getx();
                            $c_height2 = 7;
                            $c_width2 = $w[3];
                            $w_w2 = $c_height2 / 3;
                            $w_w_12 = $w_w2 + 2;
                            $w_w12 = $w_w2 + $w_w2 + $w_w2 + 3;
// $w_w2=$w_w+$w_w+$w_w+$w_w+3;// for 3 rows wrap
                            $text2 = $stud_sub->pr_designation;
                            $len2 = strlen($text2); // check the length of the cell and splits the text into 7 character each and saves in a array 
                            if ($len2 > 32) {
                                $w_text2 = str_split($text2,32); // splits the text into length of 7 and saves in a array since we need wrap cell of two cell we took $w_text[0], $w_text[2] alone.
// if we need wrap cell of 3 row then we can go for    $w_text[0],$w_text[2],$w_text[2]
                                $pdf->SetX($x_axis2);
                                $pdf->Cell($c_width2, $w_w_12, $w_text2[0], '', '', '');
                                $pdf->SetX($x_axis2);
                                $pdf->Cell($c_width2, $w_w12, $w_text2[1], '', '', '');
//$this->SetX($x_axis);
// $this->Cell($c_width,$w_w2,$w_text[2],'','','');// for 3 rows wrap but increase the $c_height it is very important.
                                $pdf->SetX($x_axis2);
                                $pdf->Cell($c_width2, $c_height2, '', 'LTRB', 0, 'L', 0);
                            } else {
                                $pdf->SetX($x_axis2);
                                $pdf->Cell($c_width2, $c_height2, $text2, 'LTRB', 0, 'L', 0);
                            }
                          
                            $x_axis = $pdf->getx();
                            $aa = $stud_sub->pr_officename;
                            //echo $aa;die;
                            $c_height = 7;
                            $c_width = $w[4];
                            $w_w = $c_height / 3;
                            $w_w_1 = $w_w + 2;
                            $w_w1 = $w_w + $w_w + $w_w + 3;
// $w_w2=$w_w+$w_w+$w_w+$w_w+3;// for 3 rows wrap
                            $text = $aa;
                            $len = strlen($text); // check the length of the cell and splits the text into 7 character each and saves in a array 
                            if ($len > 38) {
                                $w_text = str_split($text, 38); // splits the text into length of 7 and saves in a array since we need wrap cell of two cell we took $w_text[0], $w_text[1] alone.
// if we need wrap cell of 3 row then we can go for    $w_text[0],$w_text[1],$w_text[2]
                                $pdf->SetX($x_axis);
                                $pdf->Cell($c_width, $w_w_1, $w_text[0], '', '', '');
                                $pdf->SetX($x_axis);
                                $pdf->Cell($c_width, $w_w1, $w_text[1], '', '', '');
//$this->SetX($x_axis);
// $this->Cell($c_width,$w_w2,$w_text[2],'','','');// for 3 rows wrap but increase the $c_height it is very important.
                                $pdf->SetX($x_axis);
                                $pdf->Cell($c_width, $c_height, '', 'LTRB', 0, 'L', 0);
                            } else {
                                $pdf->SetX($x_axis);
                                $pdf->Cell($c_width, $c_height, $text, 'LTRB', 0, 'L', 0);
                            }

                            //$pdf->Cell($w[5], 7, $stud_sub->pr_officename, 'LRT', 0, 'L', $fill);
                          ////  $pdf->Cell($w[5], 7, $stud_sub->pr_mobno, 'LRT', 0, 'L', $fill);
                            //count1++;
                              $pdf->Cell($w[5], 7,'', 'LRT', 0, 'L', $fill);
                            $pdf->Ln();
                            $pdf->Cell(array_sum($w), 0, '', 1, 0, 'L', $fill);
                            $pdf->Ln();
                        }
                        if ($count1 == $per_page1) {
                            $per_page1 = $per_page1 + 35;
                            if ($count1 != $subTe_array_count) {
                                $pdf->AddPage();
                                $pdf->Ln();
                                $pdf->SetFont('Arial', 'B', 8);
                                $pdf->Cell(195, 5, $venue, "LTR", 0, 'L');
                                $pdf->Ln(4);


                                $pdf->SetFillColor(255, 255, 255);
                                //	$this->SetTextColor(0,0,0);
                                $pdf->SetDrawColor(0, 0, 0);
                                $pdf->SetLineWidth(.3);
                                $pdf->SetFont('Arial', 'B', 8);

                                $head = array('SL#', 'Person ID', 'Name', 'Designation', 'Office Name','Signature',);
                //    $header1 = array('','','','','','AC / Part /Sl No.','');
                $w = array(8,15,33,47,58,34);
                                $pdf->SetFont('Arial', '', 7.9);
                                for ($j = 0; $j < count($head); $j++)
                                    $pdf->Cell($w[$j], 7, $head[$j], 'LTR', 0, 'L', true);
                                $pdf->Ln();
                            }
                        }
                    }
                }

                $fill = !$fill;
                $count++;
            }
            if ($count == $per_page) {
                $per_page = $per_page + 1;
                if ($count != Count($subTe1)) {
                    $pdf->AddPage();
                }
            }
        }
        $pdf->Output();
        exit;
    }

    public function vcell($c_width, $c_height, $x_axis, $tt) {

        $w_w = $c_height / 3;
        $w_w_1 = $w_w + 2;
        $w_w1 = $w_w + $w_w + $w_w + 3;
// $w_w2=$w_w+$w_w+$w_w+$w_w+3;// for 3 rows wrap
        $text = "$tt";
        $len = strlen($text); // check the length of the cell and splits the text into 7 character each and saves in a array 
        if ($len > 15) {
            $w_text = str_split($text, 15); // splits the text into length of 7 and saves in a array since we need wrap cell of two cell we took $w_text[0], $w_text[1] alone.
// if we need wrap cell of 3 row then we can go for    $w_text[0],$w_text[1],$w_text[2]
            $this->SetX($x_axis);
            $this->Cell($c_width, $w_w_1, $w_text[0], '', '', '');
            $this->SetX($x_axis);
            $this->Cell($c_width, $w_w1, $w_text[1], '', '', '');
//$this->SetX($x_axis);
// $this->Cell($c_width,$w_w2,$w_text[2],'','','');// for 3 rows wrap but increase the $c_height it is very important.
            $this->SetX($x_axis);
            $this->Cell($c_width, $c_height, '', 'LTRB', 0, 'L', 0);
        } else {
            $this->SetX($x_axis);
            $this->Cell($c_width, $c_height, $text, 'LTRB', 0, 'L', 0);
        }
    }

}
