<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\tbl_second_training_date_time;
use App\tbl_second_training_venue;
use App\tbl_second_training_subvenue;
use App\tbl_assembly_zone;
use App\tbl_second_training_schedule;
use App\tbl_personnela;
use App\tbl_poststat;

use DB;

class SecondTrainingAllocationController extends Controller
{
   public function get_used_postat_second_training($schedule_cd, $postat,$booked) {
        $tbl_personnela = new tbl_personnela();
        $result = tbl_personnela::where('2ndTrainingSch', '=', $schedule_cd)->where('poststat', '=', $postat)->where('booked',$booked)->get();
        $res_count = $result->count();
        return $res_count;
    }
    public function get2ndTrainingDateTime(Request $request){
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        }
        else
        {
            $this->validate($request, [            
            'forDist' => 'required|alpha_num|min:2|max:2'
            ], [
            
            'forDist.required' => 'District is required',
            'forDist.alpha_num' => 'District must be an alpha numeric'
            ]);
            try
            {
              $forDist=$request->forDist;
              $list = '';
              $trainingData = tbl_second_training_date_time::where('districtcd','=',$forDist)
                                               ->select('training_time',DB::raw("Date_Format(training_dt, '%d/%m/%Y') As tr_date"), 'datetime_cd')->get();
               $trArray=json_decode($trainingData);
              $list .= "<option value>[Select]</option>";
               foreach($trArray as $trDate){
                  $list .= "<option value='" . $trDate->datetime_cd . "'>" . $trDate->tr_date." - ".$trDate->training_time . "</option>";
                }
              $response = array(
                   'options' => $list,
                   'status' => 1);          
            }catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
              $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
      
        }
    }
      public function get2ndVenueName(Request $request){
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        }
        else
        {
             $this->validate($request, [            
            'subdivision' => 'required|alpha_num|min:4|max:4'
            ], [

            'subdivision.required' => 'Subdivision is required',
            'subdivision.alpha_num' => 'Subdivision must be an alpha numeric'
            ]);
            try
            {
              $subdivision=$request->subdivision;
              $list = '';
              $list = tbl_second_training_venue::where('subdivision','=',$subdivision)
                                            -> pluck('venuename', 'venue_cd')->all();
              
              $response = array(
                   'options' => $list,
                   'status' => 1);          
            }catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
              $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
      
        }
    }
    public function getSubAssemblyDetails(Request $request){
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        }
        else
        {
             $this->validate($request, [            
            'subdivision' => 'required|alpha_num|min:4|max:4'
            ], [

            'subdivision.required' => 'Subdivision is required',
            'subdivision.alpha_num' => 'Subdivision must be an alpha numeric'
            ]);
            try
            {
              $subdivision=$request->subdivision;
              $list = '';
              $asmData = tbl_assembly_zone::join('assembly','assembly.assemblycd','=','assembly_zone.assemblycd')
                                ->where('assembly_zone.subdivisioncd','=',$subdivision)                      
                                -> select('assembly.assemblyname', 'assembly.assemblycd')->get();
              $trArray=json_decode($asmData);
              $list .= "<option value>[Select]</option>";
               foreach($trArray as $trDate){
                  $list .= "<option value='" . $trDate->assemblycd . "'>" . $trDate->assemblycd." - ".$trDate->assemblyname . "</option>";
                }
              
              $response = array(
                   'options' => $list,
                   'status' => 1);          
            }catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
              $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
      
        }
    }
     public function get2ndSubVenue(Request $request){
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        }
        else
        {
            $this->validate($request, [            
            'venuename' => 'required|alpha_num|min:6|max:6',
            'trainingdatetime' => 'required|alpha_num|min:4|max:4'   
            ], [

            'venuename.required' => 'Venue name is required',
            'venuename.alpha_num' => 'Venue name must be an alpha numeric',
            'trainingdatetime.required' => 'Training Date & Time is required',
            'trainingdatetime.alpha_num' => 'Training Date & Time must be an alpha numeric'
            ]);
            try
            {
              $venuename=$request->venuename;
              $trainingdatetime=$request->trainingdatetime;
              $data = array();
              $list = '';
              $list = tbl_second_training_venue::where('second_training_venue.venue_cd','=',$venuename)                                        
                                        ->with('subVenueDetailBrief') 
                                        ->select('second_training_venue.venue_cd','second_training_venue.maximumcapacity')
                                        ->get();                           
              $subV=json_decode($list);
              foreach($subV as $subTe){
                  $nestedData['venue_cd'] = $subTe->venue_cd;                 
                  $nestedData['maximumcapacity'] = $subTe->maximumcapacity;
                  $subTe_array=$subTe->sub_venue_detail_brief;
                  $subTe_array_count = count($subTe_array);
                  $nestedData['sub_venue_detail_brief']=array();
                  if ($subTe_array_count > 0) {
                    $count = 1;
                     foreach ($subTe_array as $stud_sub) { 
                        $nestedSubData['venue_cd']=$stud_sub->venue_cd; 
                        $nestedSubData['subvenue_cd']=$stud_sub->subvenue_cd;
                        $nestedSubData['subvenue']=$stud_sub->subvenue;
                        $nestedSubData['maxcapacity']=$stud_sub->maxcapacity; 
                        $tbl_second_training_schedule=new tbl_second_training_schedule;
                        $str_array=tbl_second_training_schedule::
                                where('second_training_schedule.tr_subvenue_cd','=',$stud_sub->subvenue_cd)
                                ->where('second_training_schedule.datetimecd','=',$trainingdatetime)
                                ->select('party_reserve','group_from','group_to','no_of_rsvPR','no_of_rsvP1','no_of_rsvP2','no_of_rsvP3','no_of_rsvPA','no_of_rsvPB','assembly')->get();
                      
                        if($str_array->count()>0){
                        foreach ($str_array as $str_row) {                          
                            $nestedSubData['party_reserve']=$str_row->party_reserve; 
                            $nestedSubData['group_from']=$str_row->group_from;
                            $nestedSubData['group_to']=$str_row->group_to; 
                            $nestedSubData['pr']=$str_row->no_of_rsvPR; 
                            $nestedSubData['p1']=$str_row->no_of_rsvP1;
                            $nestedSubData['p2']=$str_row->no_of_rsvP2;
                            $nestedSubData['p3']=$str_row->no_of_rsvP3; 
                            $nestedSubData['pa']=$str_row->no_of_rsvPA;
                            $nestedSubData['pb']=$str_row->no_of_rsvPB;
                            $nestedSubData['assemblycd'] = $str_row->assembly;
                        }
                        }else{
                            $nestedSubData['party_reserve']=""; 
                            $nestedSubData['group_from']="";
                            $nestedSubData['group_to']=""; 
                            $nestedSubData['pr']=""; 
                            $nestedSubData['p1']="";
                            $nestedSubData['p2']="";
                            $nestedSubData['p3']=""; 
                            $nestedSubData['pa']="";
                            $nestedSubData['pb']="";
                            $nestedSubData['assemblycd'] =""; 
                        }
                      $nestedData['sub_venue_detail_brief'][]= $nestedSubData; 
                     }
                  }
                $data[]=$nestedData;
              }
             // echo json_encode($data);
              $response = array(
                   'options' => $data,
                   'status' => 1);          
            }catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
              $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
      
        }
    }
     public function get2ndVenuePPTotal(Request $request){
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        }
        else
        {
            $this->validate($request, [            
            's_party_re' => 'required|alpha',
            'f_gr' => 'required|integer' ,
            'e_gr' => 'required|integer',
            'assembly' => 'required|alpha_num'    
            ], [

            's_party_re.required' => 'Party/Reserve is required',
            's_party_re.alpha' => 'Party/Reserve must be an alpha',
            'f_gr.required' => 'From Group is required',
            'f_gr.integer' => 'From Group must be an integer',
            'e_gr.required' => 'To Group is required',
            'e_gr.integer' => 'To Group must be an integer',
            'assembly.required' => 'Assembly is required',
            'assembly.alpha_num' => 'Assembly must be an alpha numeric'
            ]);
            try
            {
              $s_party_re=$request->s_party_re;
              $f_gr=$request->f_gr;
              $e_gr=$request->e_gr;
              $assembly=$request->assembly;
              $data = array();
              $list = '';
              $list = tbl_personnela::where('forassembly','=',$assembly)                                        
                        ->where('booked','=',$s_party_re)
                        ->whereBetween('groupid', array($f_gr,$e_gr))
                        ->select('poststat',DB::raw('count(personcd) as cnt'))
                        ->groupby('poststat')
                        ->get();
               // print_r(json_encode($list));die;
              $poststat=tbl_poststat::select('post_stat')->get();
            // echo josn_encode($list);die;
              foreach($poststat as $poststat1){
                 foreach($list as $l1){
                     $nested['poststat']=$poststat1->post_stat;
                   if($poststat1->post_stat==$l1->poststat) {
                      $nested['cnt']=$l1->cnt;
                      break;
                   }else{
                       $nested['cnt']=0;
                   } 
                 }
                  
                 
                    $data[]=$nested;
              }
           // echo(json_encode($data));die;

              $response = array(
                   'options' => $data,
                   'status' => 1);          
            }catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
              $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
      
        }
    }
     public function secondTrAllocation(Request $request) { 
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        }
        else
        {
            $validate_array=['subdivision' => 'required|alpha_num|min:4|max:4',
               'venuename' => 'required|alpha_num|min:6|max:6',
               'assembly' => 'required|alpha_num|min:3|max:3',
               'trainingdatetime' => 'required|alpha_num',
               'assign' => 'required|integer'];
            for($x=1; $x<=$request->row_count; $x++) {
                $validate_array['s_party_re'. $x] = 'nullable|alpha';
                $validate_array['f_gr'. $x] = 'nullable|integer';
                $validate_array['e_gr'. $x] = 'nullable|integer';
                $validate_array['s_pr'. $x] = 'nullable|integer';
                $validate_array['s_p1'. $x] = 'nullable|integer';
                $validate_array['s_p2'. $x] = 'nullable|integer';
                $validate_array['s_p3'. $x] = 'nullable|integer';
                $validate_array['s_pa'. $x] = 'nullable|integer';
                $validate_array['s_pb'. $x] = 'nullable|integer';
                $validate_array['subVenue'. $x] = 'nullable|alpha_num';
            }
            $validate_array1=['subdivision.required' => 'Subdivision is required',
                              'subdivision.alpha_num' => 'Subdivision must be an alpha numeric characters',
                              'venuename.required' => 'Venuename is required',
                              'venuename.alpha_num' => 'Venuename must be an alpha numeric characters',
                              'assembly.required' => 'Assembly is required',
                              'assembly.alpha_num' => 'Assembly must be an alpha characters',
                              'trainingdatetime.required' => 'Training Date & Time is required',
                              'trainingdatetime.alpha_num' => 'Training Date & Time must be an alpha numeric characters',       
                              'assign.required' => 'Assigned atleast for one Subvenue',
                              'assign.integer' => 'Assigned field must be an integer'];
            for($y=1; $y<=$request->row_count; $y++) {
                $validate_array1['s_party_re'. $y.'.alpha'] = 'Party/Reserve must be an alpha characters';
                $validate_array1['f_gr'. $y.'.integer'] = 'From Group must be an integer';
                $validate_array1['e_gr'. $y.'.integer'] = 'To Group must be an integer';
                $validate_array1['s_pr'. $y.'.integer'] = 'PR must be an integer';
                $validate_array1['s_p1'. $y.'.integer'] = 'P1 must be an integer';
                $validate_array1['s_p2'. $y.'.integer'] = 'P2 must be an integer';
                $validate_array1['s_p3'. $y.'.integer'] = 'P3 must be an integer';
                $validate_array1['s_pa'. $y.'.integer'] = 'PA must be an integer';
                $validate_array1['s_pb'. $y.'.integer'] = 'PB must be an integer';
                $validate_array1['subVenue'. $y.'.alpha_num'] = 'Subvenue must be an alpha numeric characters';
            }
            $this->validate($request,$validate_array,$validate_array1);
            
            try{
             $row_count=$request->row_count;
             $subdivision=$request->subdivision;
             $venuename=$request->venuename;
             $assembly=$request->assembly;
             $trainingdatetime=$request->trainingdatetime;
             $assign=$request->assign;
             $count=0;
             $totalAssign=0;
             for($j=1;$j<=$row_count;$j++){
                $t_assign='s_assign'.$j;
                $totalAssign= $totalAssign + $request->$t_assign;
             }
            if($totalAssign==$assign){
               $tbl_second_training_schedule = new tbl_second_training_schedule();
               $duplicate_v_code=$tbl_second_training_schedule->where('tr_venue_cd','=', $venuename)
                        ->where('datetimecd','=', $trainingdatetime)
                      // ->where('assembly','=', $assembly)
                        ->select(DB::raw('count(schedule_code) as cnt'))->get();
               $du_code=json_decode($duplicate_v_code); 
               if($du_code[0]->cnt!="0")  
               {
                   $tbl_second_training_schedule->where('tr_venue_cd','=', $venuename)
                        ->where('datetimecd','=', $trainingdatetime)
                           // ->where('assembly','=', $assembly)
                           ->delete();
               }
                   for($i=1;$i<=$row_count;$i++){
                       $assembly_code_done_var='assembly_code_done'.$i;
                       $assembly_code_done=$request->$assembly_code_done_var;
                       if($assembly_code_done!=''){
                           $assembly_new=$assembly_code_done;
                       }else{
                          $assembly_new=$assembly; 
                       }
                       
                       $s_party_re='s_party_re'.$i;
                       $f_gr='f_gr'.$i;
                       $e_gr='e_gr'.$i; 
                       $s_pr='s_pr'.$i;
                       $s_p1='s_p1'.$i;
                       $s_p2='s_p2'.$i;  
                       $s_p3='s_p3'.$i;  
                       $s_pa='s_pa'.$i;
                       $s_pb='s_pb'.$i;
                       $s_assign='s_assign'.$i;
                       $s_subVenue='subVenue'.$i;
                       if($request->$s_assign!=""){
                        $count++;
                        $tbl_second_training_schedule = new tbl_second_training_schedule();
                        $max_sch_code=$tbl_second_training_schedule->where('tr_venue_cd','=', $venuename)  
                            ->where('datetimecd','=',  $trainingdatetime)
                                //->where('assembly','=',  $assembly)
                            ->select(DB::raw('max(schedule_code) as cnt'))->get();
                        $max_code=json_decode($max_sch_code); 
                        if($max_code[0]->cnt=="")
                        {
                            $schcode=$venuename.$trainingdatetime."01";
                        }
                        else
                        {
                            $tmp_code=100+substr($max_code[0]->cnt,-2)+1;
                            $schcode=$venuename.$trainingdatetime.substr($tmp_code,-2);
                        }
                        $s_party_re_v=$request->$s_party_re;
                        $f_gr_v=$request->$f_gr;
                        $e_gr_v=$request->$e_gr;
                        $s_pr_v=$request->$s_pr;
                        $s_p1_v=$request->$s_p1;
                        $s_p2_v=$request->$s_p2;
                        $s_p3_v=$request->$s_p3;
                        $s_pa_v=$request->$s_pa;
                        $s_pb_v=$request->$s_pb;
                        $s_subVenue_v=$request->$s_subVenue;

                        $user_code=session()->get('code_ppds');
                        $tbl_second_training_schedule->schedule_code = $schcode;
                        $tbl_second_training_schedule->subdivision = $subdivision;
                        $tbl_second_training_schedule->tr_venue_cd = $venuename;
                        $tbl_second_training_schedule->tr_subvenue_cd = $s_subVenue_v;
                        $tbl_second_training_schedule->assembly = $assembly_new;
                        $tbl_second_training_schedule->datetimecd = $trainingdatetime;
                        $tbl_second_training_schedule->no_of_rsvPR = $s_pr_v;
                        $tbl_second_training_schedule->no_of_rsvP1 = $s_p1_v;
                        $tbl_second_training_schedule->no_of_rsvP2 = $s_p2_v;
                        $tbl_second_training_schedule->no_of_rsvP3 = $s_p3_v;
                        $tbl_second_training_schedule->no_of_rsvPA = $s_pa_v;
                        $tbl_second_training_schedule->no_of_rsvPB = $s_pb_v;
                        $tbl_second_training_schedule->party_reserve = $s_party_re_v;
                        $tbl_second_training_schedule->group_from = $f_gr_v;
                        $tbl_second_training_schedule->group_to = $e_gr_v;
                        $tbl_second_training_schedule->usercode = $user_code;


                        $tbl_second_training_schedule->save();

                       }
                    }
                     $response = array(
                       'options' => $count,
                       'status' => 1 
                    );
                   //::::::::::::::update:::::::::::::://
//                    $response = array(
//                       'options' => $count,
//                       'status' => 2 
//                    );
               
            }else{
                
                $response = array(
                   'options' => $assign,
                   'status' => 0 
                );
            }
                    
            }catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
              $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        
        }
    }
    public function second_training_venue_allocation_datatable(Request $request) {
        $response = [];
        $perm = null;
        $statusCode = 200;
        $users = array(); //Should be changed #4
        $search_val = array();
        $this->validate($request, [
            'search.*' => 'nullable|regex:/^[A-Za-z0-9\s]+$/i',
            'draw' => 'required|integer',
            'start' => 'required|integer',
            'length' => 'required|integer',
            'subdivision' => 'nullable|alpha_num',
            'venuename' => 'nullable|alpha_num',
            'assembly' => 'nullable|alpha_num',
            'trainingdatetime' => 'nullable|alpha_num'
                ], [
             'search.*.regex' => 'Special Charecters not allowed',
            'draw.required' => 'Draw is required',
            'start.integer' => 'Start must be an integer',
            'start.required' => 'Start is required',
            'length.integer' => 'Length must be an integer',
            'length.required' => 'Length is required',
            'subdivision.alpha_num' => 'Subdivision code must be an alpha numeric',
            'venuename.alpha_num' => 'Venue code must be an alpha numeric',
            'assembly.alpha_num' => 'Assembly code must be an alpha numeric',
            'trainingdatetime.alpha_num' => 'Training date time code must be an alpha numeric'
        ]);
        try {
            $draw = $request->draw;
            $offset = $request->start;
            $length = $request->length;
            $search = $request->search ["value"];
            $order = $request->order;
            $subdivision = $request->subdivision;
            $assembly = $request->assembly;
            $venuename = $request->venuename;
            $trainingdatetime = $request->trainingdatetime;
            //print_r($order);die;
            $categ = \Session::get('category_ppds');
            $dist = \Session::get('districtcd_ppds');

            $all = tbl_second_training_subvenue::all();
            $filtered = tbl_second_training_schedule::join('second_training_venue', 'second_training_venue.venue_cd', '=', 'second_training_schedule.tr_venue_cd')
                    ->join('second_training_subvenue', 'second_training_schedule.tr_subvenue_cd', '=', 'second_training_subvenue.subvenue_cd')
                    ->join('second_training_date_time', 'second_training_date_time.datetime_cd', '=', 'second_training_schedule.datetimecd')
                    ->select('party_reserve','group_from','group_to','no_of_rsvPR', 'no_of_rsvP1', 'no_of_rsvP2', 'no_of_rsvP3', 'no_of_rsvPA', 'no_of_rsvPB', 'second_training_schedule.schedule_code', 'second_training_venue.venuename', 'second_training_subvenue.subvenue', 'second_training_subvenue.maxcapacity')
                    ->where(function($q) use ($search) {
                $q->orwhere('second_training_subvenue.subvenue', 'like', '%' . $search . '%')
                        ->orwhere('second_training_venue.venuename', 'like', '%' . $search . '%')
                ->orwhere('second_training_subvenue.maxcapacity', 'like', '%' . $search . '%');
            });
            if ($dist != '') {
                $filtered = $filtered->where(DB::raw("substring(second_training_schedule.schedule_code,1,2)"), '=', $dist);
            }
            if ($subdivision != '') {
                $filtered = $filtered->where('second_training_schedule.subdivision', '=', $subdivision);
            }
            if ($venuename != '') {
                $filtered = $filtered->where('second_training_schedule.tr_venue_cd', '=', $venuename);
            }
            if ($assembly != '') {
                $filtered = $filtered->where('second_training_schedule.assembly', '=', $assembly);
            }
           
            if ($trainingdatetime != '') {
                $filtered = $filtered->where('second_training_schedule.datetimecd', '=', $trainingdatetime);
            }
            $ordered = $filtered;
            $filtered_count = $filtered->count();
            //echo count ( $order );die;
//            for ($i = 0; $i < count($order); $i ++) {
//                $ordered = $ordered->orderBy($request->columns [$order [$i] ['column']] ['data'], strtoupper($order [$i] ['dir']));
//            }
            $page_displayed = $ordered->orderBy('second_training_schedule.subdivision')
                    ->orderBy('second_training_schedule.tr_venue_cd')
                    ->offset($offset)->limit($length)
                    ->get();
            $data = array();
            $i = 1;
            if (!empty($page_displayed)) {
                foreach ($page_displayed as $assm) {
                    $nestedData['code'] = $i;
                    $nestedData['venue'] = $assm->venuename . ', ' . $assm->subvenue;
                    $nestedData['party'] = $assm->party_reserve;
                    $nestedData['group'] = $assm->group_from . '-' . $assm->group_to;
                    $nestedData['PR_A'] = $assm->no_of_rsvPR;
                    $nestedData['PR_U'] = $this->get_used_postat_second_training($assm->schedule_code, 'PR',$assm->party_reserve);
                    $nestedData['P1_A'] = $assm->no_of_rsvP1;
                    $nestedData['P1_U'] = $this->get_used_postat_second_training($assm->schedule_code, 'P1',$assm->party_reserve);
                    $nestedData['P2_A'] = $assm->no_of_rsvP2;
                    $nestedData['P2_U'] = $this->get_used_postat_second_training($assm->schedule_code, 'P2',$assm->party_reserve);
                    $nestedData['P3_A'] = $assm->no_of_rsvP3;
                    $nestedData['P3_U'] = $this->get_used_postat_second_training($assm->schedule_code, 'P3',$assm->party_reserve);
                    $nestedData['PA_A'] = $assm->no_of_rsvPA;
                    $nestedData['PA_U'] = $this->get_used_postat_second_training($assm->schedule_code, 'PA',$assm->party_reserve);
                    $nestedData['PB_A'] = $assm->no_of_rsvPB;
                    $nestedData['PB_U'] = $this->get_used_postat_second_training($assm->schedule_code, 'PB',$assm->party_reserve);
                    $nestedData['TOT_A'] = $nestedData['PR_A'] + $nestedData['P1_A'] + $nestedData['P2_A'] + $nestedData['P3_A'] + $nestedData['PA_A'] + $nestedData['PB_A'];
                    $nestedData['TOT_U'] = $nestedData['PR_U'] + $nestedData['P1_U'] + $nestedData['P2_U'] + $nestedData['P3_U'] + $nestedData['PA_U'] + $nestedData['PB_U'];
                    $nestedData['TOT_m'] = $assm->maxcapacity;
                    if ($nestedData['TOT_U'] != 0) {
                        $edit_button = $delete_button = 'NA';
                    } else {
                        $edit_button = $delete_button = $assm->schedule_code;
                    }


                    $nestedData['action'] = array('e' => $edit_button, 'd' => $delete_button);
                    $i++;
                    $data[] = $nestedData;
                }
            }
            $response = array(
                "draw" => $draw,
                "recordsTotal" => $filtered_count, //Should be changed #7
                "recordsFiltered" => $filtered_count,
                'fisrt_training_venue_alloc' => $data //Should be changed #8
            );
        } catch (\Exception $e) {
            $response = array(
                'exception' => true,
                'exception_message' => $e->getMessage()
            );
            $statusCode = 400;
        } finally {
            return response()->json($response, $statusCode);
        }
    }

    public function delete_second_training_venue_alloc(Request $request) {

        $statusCode = 200;
        $record = null; //Should be changed #26
        $this->validate($request, [
            'data' => 'required|alpha_num'
            
                ], [
            
            'data.required' => 'Schedule code is required',
            'data.alpha_num' => 'Schedule code must be an alpha numeric',
                  
        ]);
        try {
            $schedulecd = $request->data;
            $record = tbl_second_training_schedule::where('schedule_code', '=', $schedulecd); //Should be changed #27

            if (!empty($record)) {//Should be changed #30
                $record = $record->delete();
            }

            $response = array(
                'record' => $record //Should be changed #32
            );
        } catch (\Exception $e) {
            $response = array(
                'exception' => true,
                'exception_message' => $e->getMessage()
            );
            $statusCode = 400;
        } finally {
            return response()->json($response, $statusCode);
        }
    }
}
