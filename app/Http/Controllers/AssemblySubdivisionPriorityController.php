<?php

namespace App\Http\Controllers;

use App\PriorityAssembly;
use App\tbl_assembly;
use App\tbl_assembly_zone;
use App\tbl_office;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class AssemblySubdivisionPriorityController extends Controller
{
   
    public function saveAssemblySubdivisionPriority(Request $request) {  //dd($request->all());
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {

            $myArray1 = json_decode($request->assembly_array);
            $myArray2 = json_decode($request->subdivision_array);
            $myArray3 = json_decode($request->priority_array);

            $validator2 = Validator::make(compact('myArray1'), [
                        'myArray1' => 'required|array',
                        //'myArray1.*' => 'regex:/^[A-Za-z0-9\s,-.]+$/i|max:30'
                            ], [
                        'myArray1.*.regex' => 'Subvenue must be alpha numeric',
                        'myArray1.*.max' => 'Subvenue must not be greater than 30 characters'
            ]);
            $this->validateWith($validator2);

            $validator3 = \Validator::make(compact('myArray2'), [
                        'myArray2' => 'required|array',
                        //'myArray2.*' => 'digits_between:1,4'
                            ], [
                        //'myArray2.*.digits_between' => 'Maximum Capacity must be numeric'
            ]);
            $validator4 = \Validator::make(compact('myArray3'), [
                'myArray3' => 'required|array',
                //'myArray2.*' => 'digits_between:1,4'
                    ], [
               // 'myArray2.*.digits_between' => 'Maximum Capacity must be numeric'
    ]);
            $this->validateWith($validator4);

//             $validate_array = ['subdivision' => 'required|alpha_num|min:4|max:4',
//                 'venuename' => 'required|regex:/^[A-Za-z0-9\s,-.]+$/i|max:100',
//                 'venueaddress' => 'required|regex:/^[A-Za-z0-9\s,-.]+$/i|max:100',
//                 'maxcapacity' => 'required|digits_between:1,5'
//             ];

//             $validate_array1 = ['subdivision.required' => 'Subdivision is required',
//                 'subdivision.alpha_num' => 'Subdivision must be an alpha numeric characters',
//                 'venuename.required' => 'Venuename is required',
//                 'venuename.regex' => 'Venuename must be alpha numeric',
//                 'venuename.max' => 'Venuename may not be greater than 100 characters',
//                 'venueaddress.required' => 'Venueaddress is required',
//                 'venueaddress.regex' => 'Venueaddress must be alpha numeric',
//                 'venueaddress.max' => 'Venueaddress may not be greater than 100 characters',
//                 'maxcapacity.required' => 'Maximum Capacity is required',
//                 'maxcapacity.digits_between' => 'Maximum Capacity must be numeric'
// //                              'maxcapacity.max' => 'Maximum Capacity may not be greater than 4',
//             ];
//             $this->validate($request, $validate_array, $validate_array1);

            try {
               // $user_code = session()->get('code_ppds');

                // $subdivision = $request->subdivision;
                // $venuename = $request->venuename;
                // $venueaddress = $request->venueaddress;
                // $maxcapacity = $request->maxcapacity;

                // $tbl_second_training_venue = new tbl_second_training_venue();
                // $max_venue_code = $tbl_second_training_venue->where('subdivision', '=', $subdivision)
                //                 ->select(DB::raw('max(venue_cd) as cnt'))->get();
                // $max_code = json_decode($max_venue_code);
                // if ($max_code[0]->cnt == "") {
                //     $venuecode = $subdivision . "01";
                // } else {
                //     $tmp_code = 100 + substr($max_code[0]->cnt, -2) + 1;
                //     $venuecode = $subdivision . substr($tmp_code, -2);
                // }
                // $tbl_second_training_venue->venue_cd = $venuecode;
                // $tbl_second_training_venue->subdivision = $subdivision;
                // $tbl_second_training_venue->venuename = $venuename;
                // $tbl_second_training_venue->venueaddress = $venueaddress;
                // $tbl_second_training_venue->maximumcapacity = $maxcapacity;
                // $tbl_second_training_venue->usercode = $user_code;


                // $tbl_second_training_venue->save(); //save Fees Details

                if (sizeof($myArray1) == sizeof($myArray2) && sizeof($myArray2)==sizeof($myArray3) && sizeof($myArray1) > 0) {
                    $myArraylength1 = sizeof($myArray1);
                    $count1 = 0;
                    $pass=0;
                    for ($i = 1; $i <= $myArraylength1; $i++) {
                        $assembly = $myArray1[$count1];
                        $subdivision = $myArray2[$count1];
                        $priority = $myArray3[$count1];
                        $check=PriorityAssembly::where('assemblycd', $assembly)->where('subdivcd', $subdivision)->count();
                        if($check==0){
                            $pass++;
                            $priority_assembly = new PriorityAssembly();
                      
                            $priority_assembly->assemblycd = $assembly;
                            $priority_assembly->subdivcd = $subdivision;
                            $priority_assembly->priority	 = $priority;
                            $priority_assembly->usercode	 = Session::get('user_id_ppds');
                            $priority_assembly->save();
                           
                        }
                        $count1++;
                        
                    }
                }
                $response = array(
                    'options' => $pass,
                    'status' => 1
                );
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }




    public function assemblySubdivisionWisePriorityDatatable(Request $request)
    {
        $response = [];
        $perm = null;
        $statusCode = 200;
        $users = array(); //Should be changed #4
        $search_val = array();
        $this->validate($request, [
            'search.*' => 'nullable|regex:/^[A-Za-z0-9\s]+$/i',
            'draw' => 'required|integer',
            'start' => 'required|integer',
            'length' => 'required|integer',
          //  'zone' => 'nullable|alpha_num|min:4|max:4'
                ], [
            'search.*.regex' => 'Special Charecters not allowed',
            'draw.required' => 'Draw is required',
            'start.integer' => 'Start must be an integer',
            'start.required' => 'Start is required',
            'length.integer' => 'Length must be an integer',
            'length.required' => 'Length is required',
           // 'zone.alpha_num' => 'Zone code must be an alpha numeric'    
        ]);
        try {
            $draw = $request->draw;
            $offset = $request->start;
            $length = $request->length;
            $search = $request->search ["value"];
            $order = $request->order;
            //print_r($order);die;
            // $forZone = $request->zone;
            // $dcrcs = tbl_dcrcmaster::all();
            //  $categ= \Session::get('category_ppds');
            $dist = \Session::get('districtcd_ppds');
            $filtered = PriorityAssembly::leftjoin('assembly', 'assembly.assemblycd', '=', 'priority_asm.assemblycd')
                   ->leftjoin('subdivision','subdivision.subdivisioncd','=','priority_asm.subdivcd')
                ->select('priority_asm.id','assembly.assemblyname','assembly.assemblycd','subdivision.subdivisioncd','subdivision.subdivision','priority_asm.priority')
                    ->where('assembly.districtcd', '=', $dist)
                  
                    ->where(function($q) use ($search) {
                $q->orwhere('assembly.assemblyname', 'like', '%' . $search . '%')
                ->orwhere('assembly.assemblycd', 'like', '%' . $search . '%')
                ->orwhere('subdivision.subdivision', 'like', '%' . $search . '%')
                ->orwhere('priority_asm.priority', 'like', '%' . $search . '%');
                // ->orwhere('dc_date', 'like', '%' . $search . '%')
                // ->orwhere('dc_time', 'like', '%' . $search . '%');
            });
  
            $ordered = $filtered;
            $filtered_count = $filtered->count();
            // //echo count ( $order );die;
            // for ($i = 0; $i < count($order); $i ++) {
            //     $ordered = $ordered->orderBy($request->columns [$order [$i] ['column']] ['data'], strtoupper($order [$i] ['dir']));
            // }
            $page_displayed = $ordered->offset($offset)->limit($length)->get();
            $data = array();
            $count = $offset + 1;
            if (!empty($page_displayed)) {
                foreach ($page_displayed as $assembly_priority) {
                    //$cat_arr = config('constants.USER_CATEGORY');
                    $nestedData['code'] = $count;

                    $nestedData['assemblycd'] = $assembly_priority->assemblycd;
                    $nestedData['assemblyname'] = $assembly_priority->assemblyname;
                    $nestedData['subdivisioncd'] = $assembly_priority->subdivisioncd;
                    $nestedData['subdivision'] = $assembly_priority->subdivision;
                    $nestedData['priority'] = $assembly_priority->priority;
                    
                    $edit_button = $delete_button = $assembly_priority->id;
                    $nestedData['action'] = array('e' => $edit_button, 'd' => $delete_button);
                    $count++;
                    $data[] = $nestedData;
                }
            }
            // dd($data);
            $response = array(
                "draw" => $draw,
                "recordsTotal" => $filtered_count,
                "recordsFiltered" => $filtered_count,
                'record_details' => $data
            );
        } catch (\Exception $e) {
            $response = array(
                'exception' => true,
                'exception_message' => $e->getMessage()
            );
            $statusCode = 400;
        } finally {
            return response()->json($response, $statusCode);
        }
    }


    public function editAssemblySubdivisionWisePriority(Request $request)
    {
        $statusCode = 200;
        // $this->validate($request, [
        //     'data' => 'required|alpha_num|min:8|max:8'
        //         ], [
        //     'data.required' => 'code is required',
        //     'data.alpha_num' => 'code must be an alpha numeric',
        // ]);
        $edit_cd = $request->data;
        // echo $edit_cd;die;
        $check_exist = PriorityAssembly::where('id', '=', $edit_cd)->get();
        $check_count = $check_exist->count();
        // dd($check_count);
        if ($check_count != 0) {
            $data = PriorityAssembly::leftjoin('subdivision','subdivision.subdivisioncd','=','priority_asm.subdivcd')
                            ->where('id', '=', $edit_cd)->select('subdivision.subdivision','priority_asm.assemblycd','priority_asm.id','priority_asm.subdivcd','priority_asm.priority')->first();
            $assemblydata=tbl_assembly::select('assemblyname','assemblycd')->where('districtcd',session()->get('districtcd_ppds'))->get();
        } else {
            $data = '';
        }
        $response = [
            'count' => $check_count,
            'rec' => $data,
            'assemblydata'=>$assemblydata
        ];
        return response()->json($response, $statusCode);
    }



    public function getAllAssembly(Request $request)
    {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        }
        else
        {
        //    $this->validate($request, [            
        //     'forDist' => 'required|alpha_num|min:2|max:2',
        //     ], [
            
        //     'forDist.required' => 'Subdivision is required',
        //     'forDist.alpha_num' => 'Subdivision code must be an alpha numeric',
        //     ]);
            
            // dd($forDist);
            try
            {
                $forDist=$request->forDist;
                $filtered="";
                // $assembly = new tbl_assembly();
                $filtered = tbl_assembly::where('districtcd','=', $forDist)
                                           ->select('assemblyname', 'assemblycd')->get();
                $response = array(
                   'options' => $filtered,
                   'status' => 1 
                );
            }catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
              $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }  
    }




    public function deleteAssemblysubdivisionWisPriority(Request $request)
    {
       $statusCode = 200;
         
         if (!$request->ajax()) {
             $statusCode = 400;
             $response = array('error' => 'Error occured in form submit.');
             return response()->json($response, $statusCode);
         }
         $this->validate($request, [
             'data' => 'required|integer',
                 ], [
             'data.required' => 'Delete Code is required',
             'data.integer' => 'Delete Code Accepted Only Integer',
         ]);
         try {
          $dlt_data = PriorityAssembly::where('id', '=', $request->data); 
          if (!empty($dlt_data)) {//Should be changed #30
              $dlt_data = $dlt_data->delete();
          }
          $response = array(
              'status' => 1 //Should be changed #32
          );
      } catch (\Exception $e) {
          $response = array(
              'exception' => true,
              'exception_message' => $e->getMessage(),
          );
          $statusCode = 400;
      } finally {
          return response()->json($response, $statusCode);
      }
    }


    public function update_assmbly_eise_prioriy(Request $request)
    {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {

            try {
                // dd($request->all());
                $update_priority = PriorityAssembly::where('id',$request->edit_code)->update([
                    'priority' => $request->priority
                ]);
             
                $response = array(
                    'options' => $update_priority,
                    'status' => 1
                );
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }
 
}
