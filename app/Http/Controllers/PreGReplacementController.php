<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\tbl_personnela;
use App\tbl_first_training_schedule;
use App\tbl_replacement_log_pregroup;
use App\tbl_first_rand_table;
use App\tbl_cancellation_log_pregroup;
use Validator;
use App\Http\Controllers\FirstTrainingAllocationController;
use App\tbl_poststat;
use DB;

class PreGReplacementController extends Controller
{
    public function pre_group_replacement(){
     $new_c=new FirstTrainingAllocationController();
        $training_type = $new_c->getTrainingGroupData();
        return view('pre_group_replacement', compact('training_type'));    
    }

    //::::::::::::::: Search Polling Personnel :::::::::::::://
    public function searchPPDetails(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        }
        else
        {
           $this->validate($request, [            
            'personnel_id' => 'required|alpha_num|max:11|min:11'
            ], [
            
            'personnel_id.required' => 'Personnel ID is required',
            'personnel_id.alpha_num' => 'Personnel ID must be an alpha numeric'
            ]);
            $personnel_id=$request->personnel_id;
            try
            {
                $filtered=""; 
                $resMessage="";
                $tbl_personnela = new tbl_personnela();             
                $session_personnela=session()->get('personnela_ppds');
                $filtered = $tbl_personnela
                           ->join('office',''.$session_personnela.'.officecd','=','office.officecd')
                           ->join('personnel',''.$session_personnela.'.personcd','=','personnel.personcd')
                           ->join('poststat',''.$session_personnela.'.poststat','=','poststat.post_stat')
                           ->leftjoin('policestation','office.policestn_cd','=','policestation.policestationcd')
                           ->join('district','personnel.districtcd','=','district.districtcd')
                           ->join('subdivision','office.subdivisioncd','=','subdivision.subdivisioncd')
                           ->leftjoin('assembly as ass_pre','ass_pre.assemblycd','=','personnel.assembly_temp') 
                            ->leftjoin('assembly as ass_ofc','ass_ofc.assemblycd','=','personnel.assembly_off')  
                            ->leftjoin('assembly as ass_per','ass_per.assemblycd','=','personnel.assembly_perm')  
                            ->where(''.$session_personnela.'.personcd','=', $personnel_id)                     
                            ->groupBy(''.$session_personnela.'.personcd')
                            ->select(DB::raw("DATE_FORMAT(personnel.dateofbirth,'%d/%m/%Y') as dateofbirth"),''.$session_personnela.'.personcd','district.district','address1','address2','postoffice','pin'
                                    ,'policestation.policestation','subdivision.subdivision','personnel.officecd','personnel.officer_name','personnel.off_desg','personnel.present_addr1','personnel.present_addr2'
                                    ,'personnel.assembly_temp as pre_ass_cd','ass_pre.assemblyname AS pre_ass', 'personnel.assembly_perm as per_ass_cd','ass_per.assemblyname AS per_ass', 'personnel.assembly_off as post_ass_cd','ass_ofc.assemblyname AS post_ass'
                                    ,'poststat.poststatus',''.$session_personnela.'.poststat',''.$session_personnela.'.booked','personnel.gender','personnel.epic','forassembly','forzone','groupid','personnel.subdivisioncd','1stTrainingSch','2ndTrainingSch','office.office')->get();
           
                if(count($filtered)>0){
                   foreach($filtered as $rwfilter){
//                       if($rwfilter->booked=="C" || $rwfilter->booked==""){
//                          $resMessage="Not Available for Selected Operation";
//                          $status = 0;
//                       }else{
                            if($rwfilter->booked!=''){
                                if($rwfilter->booked=='C'){
                                    $booked="CANCEL";
                                }else{
                                    $booked="YES";
                                }                          
                            }else{
                                $booked="NO";
                            }
                           $fst='1stTrainingSch';
                           $sst='2ndTrainingSch';
                            $resMessage.="<table width='100%' style='font-family: verdana,arial,sans-serif;font-size: 11px;color: #000;'>\n";
                            $resMessage.="<tr><td align='left'>Name: </td><td align='left' colspan='3'>".$rwfilter->officer_name."</td></tr>\n";
                            $resMessage.="<tr><td align='left'>Office ID: </td><td align='left' colspan='3'>".$rwfilter->officecd."</td></tr>\n";
                             $resMessage.="<tr><td align='left'>Name of Office: </td><td align='left' colspan='3'>".$rwfilter->office."</td></tr>\n";
                            $resMessage.="<tr><td align='left'>Designation: </td><td align='left' colspan='3'>".$rwfilter->off_desg."</td></tr>\n";
                            $resMessage.="<tr><td align='left'>Office Address: </td><td align='left' colspan='3'>".$rwfilter->address1.",".$rwfilter->address2.", PO-".$rwfilter->postoffice.", PS-".$rwfilter->policestation.", Subdiv-".$rwfilter->subdivision.", Dist.-".$rwfilter->district.",".$rwfilter->pin."</td></tr>\n";
                            $resMessage.="<tr><td align='left'>Date of Birth: </td><td align='left'>".$rwfilter->dateofbirth."</td><td align='left'>Gender: </td><td align='left'>".$rwfilter->gender."<input type='hidden' id='gender' name='gender' value='".$rwfilter->gender."'></td></tr>\n";
                            $resMessage.="<tr><td align='left'>EPIC No: </td><td align='left'>".$rwfilter->epic."</td><td align='left'>Posting Status: </td><td align='left'>".$rwfilter->poststatus."<input type='hidden' id='post_stat' name='post_stat' value='".$rwfilter->poststat."'></td></tr>\n";
                            $resMessage.="<tr><td align='left'>Present Address: </td><td align='left' colspan='3'>".$rwfilter->present_addr1.", ".$rwfilter->present_addr2."</td></tr>\n";
                            $resMessage.="<tr><td align='left' colspan='4'><b>Assembly of</b></td></tr>\n";
                            $resMessage.="<tr><td align='left' colspan='2'>Present Address: </td><td align='left' colspan='2'>".$rwfilter->pre_ass."<hidden id='hid_pre_ass' name='hid_pre_ass' style='display:none;'>".$rwfilter->pre_ass_cd."</hidden></td></tr>\n";
                            $resMessage.="<tr><td align='left' colspan='2'>Permanent Address: </td><td align='left' colspan='2'>".$rwfilter->per_ass."<hidden id='hid_per_ass' name='hid_per_ass' style='display:none;'>".$rwfilter->per_ass_cd."</hidden></td></tr>\n";
                            $resMessage.="<tr><td align='left' colspan='2'>Place of Posting: </td><td align='left' colspan='2'>".$rwfilter->post_ass."<hidden id='hid_post_ass' name='hid_post_ass' style='display:none;'>".$rwfilter->post_ass_cd."</hidden></td></tr>\n";
                            $resMessage.="<tr><td align='left' colspan='4'><input type='hidden' id='forassembly' name='forassembly'  value='".$rwfilter->forassembly."'>\n<input type='hidden' id='groupid' name='groupid' value='".$rwfilter->groupid."'>\n<input type='hidden' id='booked' name='booked' value='".$rwfilter->booked."'>\n<input type='hidden' id='per_cd' name='per_cd' value='".$rwfilter->personcd."'>\n <input type='hidden' id='zone' name='zone' value='".$rwfilter->forzone."'>\n <input type='hidden' id='dcrccd' name='dcrccd' value='".$rwfilter->dcrccd."'>\n <input type='hidden' id='sub_div' name='sub_div' value='".$rwfilter->subdivisioncd."'>\n <input type='hidden' id='ofc_cd' name='ofc_cd' value='".$rwfilter->officecd."'>\n <input type='hidden' id='training1_sch' name='training1_sch' value='".$rwfilter->$fst."'> <input type='hidden' id='training2_sch' name='training2_sch' value='".$rwfilter->$sst."'></td></tr>\n";

                            $resMessage.="<tr><td align='right' colspan='2'>Booked : </td><td colspan='2' align='left' id='o_booked'>".$booked."</td></tr>\n";
                            $resMessage.="</table>";
                            $status = 1;
//                       }
                   }                    
                }else{
                   $resMessage="No records found"; 
                   $status = 0;
                }

                $response = array(
                   'options' => $resMessage,
                   'status' => $status
                );
            }catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
              $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
        
    } 
    
    //::::::::::::::: Pre group Replacement ::::::::::::::::://
    public function getOldPersonnelDetails(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        }
        else
        {
           $this->validate($request, [            
            'personnel_id' => 'required|alpha_num|max:11|min:11',
            'zone' => 'required|alpha_num|max:4|min:4'
            ], [
            
            'personnel_id.required' => 'Personnel ID is required',
            'personnel_id.alpha_num' => 'Personnel ID must be an alpha numeric',
              'zone.required' => 'Zone is required',
            'zone.alpha_num' => 'Zone must be an alpha numeric characters'
            ]);
            // dd($request->all());
            $personnel_id=$request->personnel_id;
            $zone=$request->zone;
            $phase=$request->phase;
            // dd($request->phase);
            try
            {
                $filtered=""; 
                $resMessage="";
                $tbl_personnela = new tbl_personnela();             
                $session_personnela=session()->get('personnela_ppds');
                $filtered = $tbl_personnela
                           ->join('office',''.$session_personnela.'.officecd','=','office.officecd')
                           ->join('personnel',''.$session_personnela.'.personcd','=','personnel.personcd')
                           ->join('poststat',''.$session_personnela.'.poststat','=','poststat.post_stat')
                           ->leftjoin('policestation','office.policestn_cd','=','policestation.policestationcd')
                           ->join('district','personnel.districtcd','=','district.districtcd')
                           ->join('subdivision','office.subdivisioncd','=','subdivision.subdivisioncd')
                           ->leftjoin('assembly as ass_pre','ass_pre.assemblycd','=','personnel.assembly_temp') 
                            ->leftjoin('assembly as ass_ofc','ass_ofc.assemblycd','=','personnel.assembly_off')  
                            ->leftjoin('assembly as ass_per','ass_per.assemblycd','=','personnel.assembly_perm')  
                            ->where(''.$session_personnela.'.personcd','=', $personnel_id)  
                            ->where(''.$session_personnela.'.forzone', '=', $zone)
                            ->where(''.$session_personnela.'.phase', '=', $phase)
                            
                            ->groupBy(''.$session_personnela.'.personcd')
                            ->select(DB::raw("DATE_FORMAT(personnel.dateofbirth,'%d/%m/%Y') as dateofbirth"),''.$session_personnela.'.personcd','district.district','address1','address2','postoffice','pin'
                                    ,'policestation.policestation','subdivision.subdivision','personnel.officecd','personnel.officer_name','personnel.off_desg','personnel.present_addr1','personnel.present_addr2'
                                    ,'personnel.assembly_temp as pre_ass_cd','ass_pre.assemblyname AS pre_ass', 'personnel.assembly_perm as per_ass_cd','ass_per.assemblyname AS per_ass', 'personnel.assembly_off as post_ass_cd','ass_ofc.assemblyname AS post_ass'
                                    ,'training_gr_cd','poststat.poststatus',''.$session_personnela.'.poststat',''.$session_personnela.'.booked','personnel.gender','personnel.epic','forassembly','forzone','groupid','personnel.subdivisioncd','office.office','1stTrainingSch','1stTrainingSch_2',''.$session_personnela.'.dcrccd','2ndTrainingSch',''.$session_personnela.'.no_of_member')->get();
           
                if(count($filtered)>0){
                   foreach($filtered as $rwfilter){
                       if($rwfilter->booked=="C" || $rwfilter->booked==""){
                          $resMessage="Not Available for Selected Operation";
                          $status = 0;
                       }else{
                           $fst='1stTrainingSch';
                           $fstSec='1stTrainingSch_2';
                           $sst='2ndTrainingSch';
                            $resMessage.="<table width='100%' style='font-family: verdana,arial,sans-serif;font-size: 11px;color: #000;'>\n";
                           // $resMessage.="<tr><td align='left'><input type='text'  value='".$rwfilter->assembly_temp."' name='assembly_temp'></td><td align='left' colspan='3'><input type='text'  value='".$rwfilter->assembly_perm."' name='assembly_perm'></td></tr>\n";
                            $resMessage.="<tr><td align='left'>Name: </td><td align='left' colspan='3'>".$rwfilter->officer_name."</td></tr>\n";
                            $resMessage.="<tr><td align='left'>Office ID: </td><td align='left' colspan='3'>".$rwfilter->officecd."</td></tr>\n";
                            $resMessage.="<tr><td align='left'>Designation: </td><td align='left' colspan='3'>".$rwfilter->off_desg."</td></tr>\n";
                            
                            $resMessage.="<tr><td align='left'>Office Name: </td><td align='left' colspan='3'>".$rwfilter->office."</td></tr>\n";
                            
                            
                            $resMessage.="<tr><td align='left'>Office Address: </td><td align='left' colspan='3'>".$rwfilter->address1.",".$rwfilter->address2.", PO-".$rwfilter->postoffice.", PS-".$rwfilter->policestation.", Subdiv-".$rwfilter->subdivision.", Dist.-".$rwfilter->district.",".$rwfilter->pin."</td></tr>\n";
                            $resMessage.="<tr><td align='left'>Date of Birth: </td><td align='left'>".$rwfilter->dateofbirth."</td><td align='left'>Gender: </td><td align='left'>".$rwfilter->gender."<input type='hidden' id='gender' name='gender' value='".$rwfilter->gender."'></td></tr>\n";
                            $resMessage.="<tr><td align='left'>EPIC No: </td><td align='left'>".$rwfilter->epic."</td><td align='left'>Posting Status: </td><td align='left'>".$rwfilter->poststatus."<input type='hidden' id='post_stat' name='post_stat' value='".$rwfilter->poststat."'></td></tr>\n";
                            $resMessage.="<tr><td align='left'>Present Address: </td><td align='left' colspan='3'>".$rwfilter->present_addr1.", ".$rwfilter->present_addr2."</td></tr>\n";
                            $resMessage.="<tr><td align='left' colspan='4'><b>Assembly of</b></td></tr>\n";
                            $resMessage.="<tr><td align='left' colspan='2'>Present Address: </td><td align='left' colspan='2'>".$rwfilter->pre_ass."<input type='hidden' id='hid_pre_ass' name='hid_pre_ass' value='".$rwfilter->pre_ass_cd."'></td></tr>\n";
                            $resMessage.="<tr><td align='left' colspan='2'>Permanent Address: </td><td align='left' colspan='2'>".$rwfilter->per_ass."<input type='hidden' id='hid_per_ass' name='hid_per_ass' value='".$rwfilter->per_ass_cd."'></td></tr>\n";
                            $resMessage.="<tr><td align='left' colspan='2'>Place of Posting: </td><td align='left' colspan='2'>".$rwfilter->post_ass."<input type='hidden' id='hid_post_ass' name='hid_post_ass' value='".$rwfilter->post_ass_cd."'></td></tr>\n";
                            $resMessage.="<tr><td align='left' colspan='4'><input type='hidden' id='forassembly' name='forassembly'  value='".$rwfilter->forassembly."'>\n<input type='hidden' id='groupid' name='groupid' value='".$rwfilter->groupid."'>\n<input type='hidden' id='training_gr_cd' name='training_gr_cd' value='".$rwfilter->training_gr_cd."'>\n<input type='hidden' id='booked' name='booked' value='".$rwfilter->booked."'>\n <input type='hidden' id='no_of_member' name='no_of_member' value='".$rwfilter->no_of_member."'>\n <input type='hidden' id='per_cd' name='per_cd' value='".$rwfilter->personcd."'>\n <input type='hidden' id='zone' name='zone' value='".$rwfilter->forzone."'>\n <input type='hidden' id='dcrccd' name='dcrccd' value='".$rwfilter->dcrccd."'>\n <input type='hidden' id='sub_div' name='sub_div' value='".$rwfilter->subdivisioncd."'>\n <input type='hidden' id='ofc_cd' name='ofc_cd' value='".$rwfilter->officecd."'>\n <input type='hidden' id='training1_sch' name='training1_sch' value='".$rwfilter->$fst."'>\n <input type='hidden' id='training1_sch_sec' name='training1_sch_sec' value='".$rwfilter->$fstSec."'> <input type='hidden' id='training2_sch' name='training2_sch' value='".$rwfilter->$sst."'></td></tr>\n";

                            $resMessage.="<tr><td align='right' colspan='2'>Booked : </td><td colspan='2' align='left' id='o_booked'>Yes</td></tr>\n";
                            $resMessage.="</table>";
                            $status = 1;
                       }
                   }                    
                }else{
                   $resMessage="No records found"; 
                   $status = 0;
                }

                $response = array(
                   'options' => $resMessage,
                   'status' => $status
                );
            }catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
              $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
        
    }
    public function searchNewPPData(Request $request) { //dd($request->all());
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        }
        else
        {
           $this->validate($request, [            
            'zone' => 'required|alpha_num|min:4|max:4',
            'forassembly' => 'nullable|alpha_num|min:3|max:3',
            'ofc_cd' => 'required|alpha_num|min:10|max:10',
            'booked' => 'required|alpha|min:1|max:1',
            'post_stat' => 'required|alpha_num|min:2|max:2',
            'gender' => 'required|alpha|min:1|max:1',
            'phase' => 'required|digits_between:1,1|min:1|max:1'
            ], [
            
            'zone.required' => 'Zone is required',
            'zone.alpha_num' => 'Zone must be an alpha numeric',
            'forassembly.alpha_num' => 'Forassembly must be an alpha numeric',
            'ofc_cd.required' => 'Office ID is required',
            'ofc_cd.alpha_num' => 'Office ID must be an alpha numeric',
            'booked.required' => 'Booked is required',
            'booked.alpha' => 'Booked must be an alpha',
            'post_stat.required' => 'Post Status is required',
            'post_stat.alpha_num' => 'Post Status must be an alpha numeric',
            'gender.required' => 'Gender is required',
            'gender.alpha' => 'Gender must be an alpha',
            'phase.required' => 'Phase is required',
            'phase.digits_between' => 'Phase must be integer',
            ]);
            
            try
            {
                $zone=$request->zone;
                $forassembly=$request->forassembly;
                $ofc_id=$request->ofc_cd;
                $booked=$request->booked;
                $post_stat=$request->post_stat;
                $gender=$request->gender;
                $phase=$request->phase;

                $pin_cd = $request->pin_cd;



$pre_ass_cd=$request->hid_pre_ass;
$per_ass_cd=$request->hid_per_ass;
                $filtered=""; 
                $resMessage="";
                $tbl_personnela1 = new tbl_personnela();             
                $session_personnela=session()->get('personnela_ppds');
                $ppcount = $tbl_personnela1
                           ->join('office',''.$session_personnela.'.officecd','=','office.officecd')
                           ->join('personnel',''.$session_personnela.'.personcd','=','personnel.personcd')
                           ->join('poststat',''.$session_personnela.'.poststat','=','poststat.post_stat')
                           ->leftjoin('policestation','office.policestn_cd','=','policestation.policestationcd')
                           ->join('district','personnel.districtcd','=','district.districtcd')
                           ->join('subdivision','office.subdivisioncd','=','subdivision.subdivisioncd')
                           ->leftjoin('assembly as ass_pre','ass_pre.assemblycd','=','personnel.assembly_temp') 
                            ->leftjoin('assembly as ass_ofc','ass_ofc.assemblycd','=','personnel.assembly_off')  
                            ->leftjoin('assembly as ass_per','ass_per.assemblycd','=','personnel.assembly_perm')
                           ->where('personnel.subdivisioncd','=', substr($ofc_id,0,4))
                            ->where(''.$session_personnela.'.gender','=', $gender) 
                           ->where(''.$session_personnela.'.poststat','=', $post_stat)
                           ->where(''.$session_personnela.'.phase','=', $phase)
                            //->where(''.$session_personnela.'.forassembly','=', $forassembly)
                            ->where(''.$session_personnela.'.forzone','=', $zone)
                            ->where(function($q) {
                                $q->orwhereNull('booked')
                                ->orwhere('booked', '=', '');
                            })
                            ->value(DB::raw("count(personnel.personcd)"));
                $mode="own-sub";
                if($ppcount==0){
                  $mode="other-sub";  
                }
                $tbl_personnela = new tbl_personnela();
                if($mode=="own-sub")
                {
                   $tbl_personnela = $tbl_personnela->where('personnel.subdivisioncd','=', substr($ofc_id,0,4));
                }
                if($gender=="F"){
                    $tbl_personnela = $tbl_personnela ->where(function($query) use ($pre_ass_cd,$per_ass_cd,$session_personnela){
                        $query->orWhere(''.$session_personnela.'.assembly_temp', $pre_ass_cd)
                              ->orWhere(''.$session_personnela.'.assembly_perm', $per_ass_cd);
                    });
                }
                if($pin_cd!=''){
                    $filtered = $tbl_personnela
                           ->join('office',''.$session_personnela.'.officecd','=','office.officecd')
                           ->join('personnel',''.$session_personnela.'.personcd','=','personnel.personcd')
                           ->join('poststat',''.$session_personnela.'.poststat','=','poststat.post_stat')
                           ->leftjoin('policestation','office.policestn_cd','=','policestation.policestationcd')
                           ->join('district','personnel.districtcd','=','district.districtcd')
                           ->join('subdivision','office.subdivisioncd','=','subdivision.subdivisioncd')
                           ->leftjoin('assembly as ass_pre','ass_pre.assemblycd','=','personnel.assembly_temp') 
                            ->leftjoin('assembly as ass_ofc','ass_ofc.assemblycd','=','personnel.assembly_off')  
                            ->leftjoin('assembly as ass_per','ass_per.assemblycd','=','personnel.assembly_perm')
                           
                            // ->where(''.$session_personnela.'.gender','=', $gender) 
                        //    ->where(''.$session_personnela.'.poststat','=', $post_stat)
                        //    ->where(''.$session_personnela.'.phase','=', $phase)
                           // ->where(''.$session_personnela.'.forassembly','=', $forassembly)


                           ->where(''.$session_personnela.'.personcd','=', $pin_cd)

                            // ->where(''.$session_personnela.'.forzone','=', $zone)
                            ->where(function($q) {
                                $q->orwhereNull('booked')
                                ->orwhere('booked', '=', '');
                            })
                            ->select(DB::raw("DATE_FORMAT(personnel.dateofbirth,'%d/%m/%Y') as dateofbirth"),''.$session_personnela.'.personcd','district.district','address1','address2','postoffice','pin'
                                    ,'policestation.policestation','subdivision.subdivision','personnel.officecd','personnel.officer_name','personnel.off_desg','personnel.present_addr1','personnel.present_addr2'
                                    ,'personnel.assembly_temp as pre_ass_cd','ass_pre.assemblyname AS pre_ass', 'personnel.assembly_perm as per_ass_cd','ass_per.assemblyname AS per_ass', 'personnel.assembly_off as post_ass_cd','ass_ofc.assemblyname AS post_ass'
                                    ,'poststat.poststatus',''.$session_personnela.'.poststat',''.$session_personnela.'.booked','personnel.gender','personnel.epic','forassembly','forzone','office.office','groupid','personnel.subdivisioncd','1stTrainingSch')
                            ->get();
                } else{
                    $filtered = $tbl_personnela
                           ->join('office',''.$session_personnela.'.officecd','=','office.officecd')
                           ->join('personnel',''.$session_personnela.'.personcd','=','personnel.personcd')
                           ->join('poststat',''.$session_personnela.'.poststat','=','poststat.post_stat')
                           ->leftjoin('policestation','office.policestn_cd','=','policestation.policestationcd')
                           ->join('district','personnel.districtcd','=','district.districtcd')
                           ->join('subdivision','office.subdivisioncd','=','subdivision.subdivisioncd')
                           ->leftjoin('assembly as ass_pre','ass_pre.assemblycd','=','personnel.assembly_temp') 
                            ->leftjoin('assembly as ass_ofc','ass_ofc.assemblycd','=','personnel.assembly_off')  
                            ->leftjoin('assembly as ass_per','ass_per.assemblycd','=','personnel.assembly_perm')
                           
                            ->where(''.$session_personnela.'.gender','=', $gender) 
                           ->where(''.$session_personnela.'.poststat','=', $post_stat)
                        //    ->where(''.$session_personnela.'.phase','=', $phase)
                           // ->where(''.$session_personnela.'.forassembly','=', $forassembly)
                            // ->where(''.$session_personnela.'.forzone','=', $zone)
                            ->where(function($q) {
                                $q->orwhereNull('booked')
                                ->orwhere('booked', '=', '');
                            })
                            ->select(DB::raw("DATE_FORMAT(personnel.dateofbirth,'%d/%m/%Y') as dateofbirth"),''.$session_personnela.'.personcd','district.district','address1','address2','postoffice','pin'
                                    ,'policestation.policestation','subdivision.subdivision','personnel.officecd','personnel.officer_name','personnel.off_desg','personnel.present_addr1','personnel.present_addr2'
                                    ,'personnel.assembly_temp as pre_ass_cd','ass_pre.assemblyname AS pre_ass', 'personnel.assembly_perm as per_ass_cd','ass_per.assemblyname AS per_ass', 'personnel.assembly_off as post_ass_cd','ass_ofc.assemblyname AS post_ass'
                                    ,'poststat.poststatus',''.$session_personnela.'.poststat',''.$session_personnela.'.booked','personnel.gender','personnel.epic','forassembly','forzone','office.office','groupid','personnel.subdivisioncd','1stTrainingSch')
                            ->inRandomOrder()
                            ->take(1)->get();
                }
                
                if(count($filtered)>0){
                   foreach($filtered as $rwfilter){
//                       if($rwfilter->booked=="C" || $rwfilter->booked==""){
//                          $resMessage="Not Available for Selected Operation";
//                          $status = 0;
//                       }else{
                           $fst='1stTrainingSch';
                            $resMessage.="<table width='100%' style='font-family: verdana,arial,sans-serif;font-size: 11px;color: #000;'>\n";
                            $resMessage.="<tr><td align='left'>Name: </td><td align='left' colspan='3'>".$rwfilter->officer_name."</td></tr>\n";
                            $resMessage.="<tr><td align='left'>Office ID: </td><td align='left' colspan='3'>".$rwfilter->officecd."</td></tr>\n";
                            $resMessage.="<tr><td align='left'>Designation: </td><td align='left' colspan='3'>".$rwfilter->off_desg."</td></tr>\n";
                            
                            $resMessage.="<tr><td align='left'>Office Name: </td><td align='left' colspan='3'>".$rwfilter->office."</td></tr>\n";
                            
                            
                            
                            $resMessage.="<tr><td align='left'>Office Address: </td><td align='left' colspan='3'>".$rwfilter->address1.",".$rwfilter->address2.", PO-".$rwfilter->postoffice.", PS-".$rwfilter->policestation.", Subdiv-".$rwfilter->subdivision.", Dist.-".$rwfilter->district.",".$rwfilter->pin."</td></tr>\n";
                            $resMessage.="<tr><td align='left'>Date of Birth: </td><td align='left'>".$rwfilter->dateofbirth."</td><td align='left'>Gender: </td><td align='left'>".$rwfilter->gender."<hidden id='hid_gender' name='hid_gender' style='display:none;'>".$rwfilter->gender."</hidden></td></tr>\n";
                            $resMessage.="<tr><td align='left'>EPIC No: </td><td align='left'>".$rwfilter->epic."</td><td align='left'>Posting Status: </td><td align='left'>".$rwfilter->poststatus."<hidden id='hid_post_stat' name='hid_post_stat' style='display:none;'>".$rwfilter->poststat."</hidden></td></tr>\n";
                            $resMessage.="<tr><td align='left'>Present Address: </td><td align='left' colspan='3'>".$rwfilter->present_addr1.", ".$rwfilter->present_addr2."</td></tr>\n";
                            $resMessage.="<tr><td align='left' colspan='4'><b>Assembly of</b></td></tr>\n";
                            $resMessage.="<tr><td align='left' colspan='2'>Present Address: </td><td align='left' colspan='2'>".$rwfilter->pre_ass."<hidden id='hid_pre_ass' name='hid_pre_ass' style='display:none;'>".$rwfilter->pre_ass_cd."</hidden></td></tr>\n";
                            $resMessage.="<tr><td align='left' colspan='2'>Permanent Address: </td><td align='left' colspan='2'>".$rwfilter->per_ass."<hidden id='hid_per_ass' name='hid_per_ass' style='display:none;'>".$rwfilter->per_ass_cd."</hidden></td></tr>\n";
                            $resMessage.="<tr><td align='left' colspan='2'>Place of Posting: </td><td align='left' colspan='2'>".$rwfilter->post_ass."<hidden id='hid_post_ass' name='hid_post_ass' style='display:none;'>".$rwfilter->post_ass_cd."</hidden></td></tr>\n";
                            $resMessage.="<tr><td align='left' colspan='4'><hidden id='hid_forassembly' name='hid_forassembly' style='display:none;'>".$rwfilter->forassembly."</hidden>\n<hidden id='hid_groupid' name='hid_groupid' style='display:none;'>".$rwfilter->groupid."</hidden>\n<hidden id='hid_booked' name='hid_booked' style='display:none;'>".$rwfilter->booked."</hidden>\n<hidden id='hid_per_cd' name='hid_per_cd' style='display:none;'>".$rwfilter->personcd."</hidden>\n <hidden id='hid_for_zone' name='hid_for_zone' style='display:none;'>".$rwfilter->forzone."</hidden>\n <hidden id='hid_dcrccd' name='hid_dcrccd' style='display:none;'>".$rwfilter->dcrccd."</hidden>\n <hidden id='hid_sub_div' name='hid_sub_div' style='display:none;'>".$rwfilter->subdivisioncd."</hidden>\n <hidden id='hid_ofc_cd' name='hid_ofc_cd' style='display:none;'>".$rwfilter->officecd."</hidden>\n <hidden id='hid_training1_sch' name='hid_training1_sch' style='display:none;'>".$rwfilter->$fst."</hidden></td></tr>\n";

                            $resMessage.="<tr><td align='right' colspan='2'>Booked : </td><td colspan='2' align='left' id='n_booked'>No</td></tr>\n";
                            $resMessage.="</table>";
                            $pid = $rwfilter->personcd;
                            $status = 1;
                       //}
                            
                            $tbl_firstTrSCH = new tbl_first_training_schedule();
                            if($rwfilter->poststat=='P1'){
                                $tbl_firstTrSCH = $tbl_firstTrSCH->where(function($q) {
                                    $q->orwhereNotNull('no_of_P1')
                                    ->orwhere('no_of_P1', '<>', '')
                                    ->orwhere('no_of_P1', '<>', '0');
                                    }); 
                            }
                            if($rwfilter->poststat=='P2'){
                                $tbl_firstTrSCH = $tbl_firstTrSCH->where(function($q) {
                                    $q->orwhereNotNull('no_of_P2')
                                    ->orwhere('no_of_P2', '<>', '')
                                    ->orwhere('no_of_P2', '<>', '0');
                                    }); 
                            }
                            if($rwfilter->poststat=='P3'){
                                $tbl_firstTrSCH = $tbl_firstTrSCH->where(function($q) {
                                    $q->orwhereNotNull('no_of_P3')
                                    ->orwhere('no_of_P3', '<>', '')
                                    ->orwhere('no_of_P3', '<>', '0');
                                    }); 
                            }
                            if($rwfilter->poststat=='PA'){
                                $tbl_firstTrSCH = $tbl_firstTrSCH->where(function($q) {
                                    $q->orwhereNotNull('no_of_PA')
                                    ->orwhere('no_of_PA', '<>', '')
                                    ->orwhere('no_of_PA', '<>', '0');
                                    }); 
                            }
                            if($rwfilter->poststat=='PB'){
                                $tbl_firstTrSCH = $tbl_firstTrSCH->where(function($q) {
                                    $q->orwhereNotNull('no_of_PB')
                                    ->orwhere('no_of_PB', '<>', '')
                                    ->orwhere('no_of_PB', '<>', '0');
                                    }); 
                            }
                            if($rwfilter->poststat=='PR'){
                                $tbl_firstTrSCH = $tbl_firstTrSCH->where(function($q) {
                                    $q->orwhereNotNull('no_of_PR')
                                    ->orwhere('no_of_PR', '<>', '')
                                    ->orwhere('no_of_PR', '<>', '0');
                                    }); 
                            }
                            $filteredTR = $tbl_firstTrSCH->join('first_training_venue', 'first_training_venue.venue_cd', '=', 'first_training_schedule.tr_venue_cd')
                                ->join('first_training_subvenue', 'first_training_schedule.tr_subvenue_cd', '=', 'first_training_subvenue.subvenue_cd')
                                ->join('first_training_date_time', 'first_training_date_time.datetime_cd', '=', 'first_training_schedule.datetimecd')
                                ->where('first_training_schedule.forzone','=', $zone)     
                                ->select('no_of_PR', 'no_of_P1', 'no_of_P2', 'no_of_P3', 'no_of_PA', 'no_of_PB', 'first_training_schedule.schedule_code', 'first_training_venue.venuename', 'first_training_subvenue.subvenue', 'first_training_subvenue.maxcapacity','first_training_date_time.training_dt','first_training_date_time.training_time')->get();
                            
                            $subV=json_decode($filteredTR);
                            if($filteredTR->count()>0)
                            {
                                $data = array();
                               foreach($subV as $subTe){                                
                                 if($rwfilter->poststat=='P1'){
                                     $tbl_personnelaP1=new tbl_personnela; 
                                     $total_P1 = $tbl_personnelaP1->where('selected','=',1)
                                              ->where('forzone','=',$zone)
                                             ->where('poststat','=','P1')
                                            ->where('1stTrainingSch','=',$subTe->schedule_code)
                                            ->value(DB::raw("count(personcd)"));
                                    $sch_P1 = $subTe->no_of_P1;
                                    if(($sch_P1 - $total_P1)>0){
                                        $nestedData['schedule_code'] = $subTe->schedule_code;
                                        $nestedData['vs_name'] = $subTe->venuename." - ".$subTe->subvenue." [".date('d/m/Y', strtotime(trim(str_replace('/', '-', $subTe->training_dt))))." - ".$subTe->training_time."]";
                                        $data[] = $nestedData;
                                    }else{
                                        continue;
                                    }
                                 }else if($rwfilter->poststat=='P2'){
                                     $tbl_personnelaP2=new tbl_personnela; 
                                     $total_P2 = $tbl_personnelaP2->where('selected','=',1)
                                              ->where('forzone','=',$zone)
                                             ->where('poststat','=','P2')
                                            ->where('1stTrainingSch','=',$subTe->schedule_code)
                                            ->value(DB::raw("count(personcd)"));
                                    $sch_P2 = $subTe->no_of_P2;
                                    if(($sch_P2 - $total_P2)>0){
                                        $nestedData['schedule_code'] = $subTe->schedule_code;
                                        $nestedData['vs_name'] = $subTe->venuename." - ".$subTe->subvenue." [".date('d/m/Y', strtotime(trim(str_replace('/', '-', $subTe->training_dt))))." - ".$subTe->training_time."]";
                                        $data[] = $nestedData;
                                    }else{
                                        continue;
                                    }
                                 }else if($rwfilter->poststat=='P3'){
                                     $tbl_personnelaP3=new tbl_personnela; 
                                     $total_P3 = $tbl_personnelaP3->where('selected','=',1)
                                              ->where('forzone','=',$zone)
                                             ->where('poststat','=','P3')
                                            ->where('1stTrainingSch','=',$subTe->schedule_code)
                                            ->value(DB::raw("count(personcd)"));
                                    $sch_P3 = $subTe->no_of_P3;
                                    if(($sch_P3 - $total_P3)>0){
                                        $nestedData['schedule_code'] = $subTe->schedule_code;
                                        $nestedData['vs_name'] = $subTe->venuename." - ".$subTe->subvenue." [".date('d/m/Y', strtotime(trim(str_replace('/', '-', $subTe->training_dt))))." - ".$subTe->training_time."]";
                                        $data[] = $nestedData;
                                    }else{
                                        continue;
                                    }
                                 }else if($rwfilter->poststat=='PR'){
                                     $tbl_personnelaPR=new tbl_personnela; 
                                     $total_PR = $tbl_personnelaPR->where('selected','=',1)
                                              ->where('forzone','=',$zone)
                                             ->where('poststat','=','PR')
                                            ->where('1stTrainingSch','=',$subTe->schedule_code)
                                            ->value(DB::raw("count(personcd)"));
                                    $sch_PR = $subTe->no_of_PR;
                                    if(($sch_PR - $total_PR)>0){
                                        $nestedData['schedule_code'] = $subTe->schedule_code;
                                        $nestedData['vs_name'] = $subTe->venuename." - ".$subTe->subvenue." [".date('d/m/Y', strtotime(trim(str_replace('/', '-', $subTe->training_dt))))." - ".$subTe->training_time."]";
                                        $data[] = $nestedData;
                                    }else{
                                        continue;
                                    }
                                 }else if($rwfilter->poststat=='PA'){
                                     $tbl_personnelaPA=new tbl_personnela; 
                                     $total_PA = $tbl_personnelaPA->where('selected','=',1)
                                              ->where('forzone','=',$zone)
                                             ->where('poststat','=','PA')
                                            ->where('1stTrainingSch','=',$subTe->schedule_code)
                                            ->value(DB::raw("count(personcd)"));
                                    $sch_PA = $subTe->no_of_PA;
                                    if(($sch_PA - $total_PA)>0){
                                        $nestedData['schedule_code'] = $subTe->schedule_code;
                                        $nestedData['vs_name'] = $subTe->venuename." - ".$subTe->subvenue." [".date('d/m/Y', strtotime(trim(str_replace('/', '-', $subTe->training_dt))))." - ".$subTe->training_time."]";
                                        $data[] = $nestedData;
                                    }else{
                                        continue;
                                    }
                                 }else if($rwfilter->poststat=='PB'){
                                     $tbl_personnelaPB=new tbl_personnela; 
                                     $total_PB = $tbl_personnelaPB->where('selected','=',1)
                                              ->where('forzone','=',$zone)
                                             ->where('poststat','=','PB')
                                            ->where('1stTrainingSch','=',$subTe->schedule_code)
                                            ->value(DB::raw("count(personcd)"));
                                    $sch_PB = $subTe->no_of_PB;
                                    if(($sch_PB - $total_PB)>0){
                                        $nestedData['schedule_code'] = $subTe->schedule_code;
                                        $nestedData['vs_name'] = $subTe->venuename." - ".$subTe->subvenue." [".date('d/m/Y', strtotime(trim(str_replace('/', '-', $subTe->training_dt))))." - ".$subTe->training_time."]";
                                        $data[] = $nestedData;
                                    }else{
                                        continue;
                                    }
                                 }
                                 
                               }
                               $trschedule = $data;
                            }else{
                                $trschedule = "";
                            }
                   }                    
                }else{
                   $pid = "";
                   $trschedule="";
                   $resMessage="No records found"; 
                   $status = 0;
                }

                $response = array(
                   'options' => $resMessage,
                   'trschedule' =>$trschedule,
                   'perid' =>$pid,
                   'status' => $status
                );
            }catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
              $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
        
    }
    public function preReplacePPData(Request $request) {// dd($request->all());
        //die;
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        }
        else
        {
           $this->validate($request, [            
                'zone' => 'required|alpha_num|min:4|max:4',
                'forassembly' => 'nullable|alpha_num|min:3|max:3',
                'ofc_cd' => 'required|alpha_num|min:10|max:10',
                'booked' => 'required|alpha|min:1|max:1',
                'post_stat' => 'required|alpha_num|min:2|max:2',
                'gender' => 'required|alpha|min:1|max:1',
                'chkSameVenueTraining' => 'nullable|alpha',
//                'training_sch' => 'nullable|min:12|max:12|alpha_num|required_if:chkSameVenueTraining,',
                'reason' => 'required|regex:/^[A-Za-z0-9\s,-.]+$/i|max:30',
                'personnel_id' => 'required|alpha_num|min:11|max:11',
                'newpersonnel_id' => 'required|alpha_num|min:11|max:11',
                'training1_sch' => 'nullable|alpha_num|min:12|max:12',
                'training1_sch_sec' => 'nullable|alpha_num|min:12|max:12',
                'training_gr_cd' => 'required|digits_between:1,1',
               'subdivision' => 'nullable|alpha_num|min:4|max:4',
            'v_sub'=>'nullable|alpha_num|max:12',
            'phase' => 'required|digits_between:1,1|min:1|max:1'
            ], [
            
                'zone.required' => 'Zone is required',
                'zone.alpha_num' => 'Zone must be an alpha numeric',
                'forassembly.required' => 'Forassembly is required',
                'forassembly.alpha_num' => 'Forassembly must be an alpha numeric',
                'ofc_cd.required' => 'Office ID is required',
                'ofc_cd.alpha_num' => 'Office ID must be an alpha numeric',
                'booked.required' => 'Booked is required',
                'booked.alpha' => 'Booked must be an alpha',
                'post_stat.required' => 'Post Status is required',
                'post_stat.alpha_num' => 'Post Status must be an alpha numeric',
                'gender.required' => 'Gender is required',
                'gender.alpha' => 'Gender must be an alpha',
                'chkSameVenueTraining.alpha' => 'Training Venue checkbox value must be an alpha',
//                'training_sch.alpha_num' => 'Training Schedule must be an alpha numeric',
                'reason.required' => 'Reason is required',
                'reason.regex' => 'Reason must not be special characters',
                'reason.max' => 'Reason must be 30 characters',
                'personnel_id.required' => 'OLD Personnel ID is required',
                'personnel_id.alpha_num' => 'OLD Personnel ID must be an alpha numeric',
                'newpersonnel_id.required' => 'NEW Personnel ID is required',
                'newpersonnel_id.alpha_num' => 'NEW Personnel ID must be an alpha numeric',
                //'training1_sch.required' => '1st Training Schedule is required',
                'training1_sch.alpha_num' => '1st Training Schedule must be an alpha numeric',
             //   'training1_sch_sec.required' => '2nd Training Schedule is required',
                'training1_sch_sec.alpha_num' => '2nd Training Schedule must be an alpha numeric',
                'training_gr_cd.required' => 'Training Group Code is required',
                'training_gr_cd.digits_between' => 'Training Group Code must be an integer',
                'subdivision.required' => 'subdivision is required',
            'subdivision.alpha_num' => 'Subdivision must be an alpha numeric characters',
            'phase.required' => 'Phase is required',
            'phase.digits_between'=>'Phase must be an integer',
               
            ]);
            
            try
            {
                $zone=$request->zone;
                $forassembly=$request->forassembly;
                $ofc_id=$request->ofc_cd;
                $booked=$request->booked;
                $post_stat=$request->post_stat;
                $gender=$request->gender;
                $phase=$request->phase;
                $oldpersonnel_id=$request->personnel_id;
                $newpersonnel_id=$request->newpersonnel_id;
                $reason=$request->reason;
                $chkSameVenueTraining=$request->chkSameVenueTraining;
               // echo  $chkSameVenueTraining;die;
                $oldtraining_sch=$request->training1_sch;
                $oldtraining_sch_sec=$request->training1_sch_sec;
                $training_gr_cd=$request->training_gr_cd;
                $orderno=$request->orderno;
              $v_sub=$request->v_sub;
              if($v_sub==''){
                  $v_sub=NULL;
              }
              $v_sub2=$request->v_sub2;
              if($v_sub2==''){
                  $v_sub2=NULL;
              }
            $districtcd=$request->districtcd;
            $trainingHead=$request->trainingtype;
           $session_personnela=session()->get('personnela_ppds');
           $trainingtype=
                $filtered=""; 
                $resMessage="";
               // $tbl_personnela1 = new tbl_personnela();             
                $session_personnela=session()->get('personnela_ppds');
                
                $duplicate_id = tbl_replacement_log_pregroup::where('new_personnel','=', $newpersonnel_id)
                                                      ->value(DB::raw("count(*)"));
                                               
                if($duplicate_id == 0){
                     $tbl_personnela=new tbl_personnela; 
                   if($chkSameVenueTraining=='true'){                      
                      $tbl_personnela
                         ->where('forzone','=',$zone)
                         ->where('phase','=',$phase)
                         ->where('personcd','=',$newpersonnel_id)
                         ->update(['1stTrainingSch'=>$oldtraining_sch,'1stTrainingSch_2'=>$oldtraining_sch_sec,
                                   'training_gr_cd' => $training_gr_cd]); 
                    }
                    
                    $user_code=session()->get("code_ppds");
                    $tbl_replacement_log_pregroup = new tbl_replacement_log_pregroup();
                    $tbl_replacement_log_pregroup->old_personnel = $oldpersonnel_id;
                    $tbl_replacement_log_pregroup->new_personnel = $newpersonnel_id;
                    $tbl_replacement_log_pregroup->forassembly = $forassembly;
                    $tbl_replacement_log_pregroup->phase_id = $phase;
                    $tbl_replacement_log_pregroup->reason = $reason;
                    $tbl_replacement_log_pregroup->usercode = $user_code;
                    $tbl_replacement_log_pregroup->save();
                    
                    tbl_personnela::where('forzone','=',$zone)
                         ->where('personcd','=',$oldpersonnel_id)
                         ->where('phase','=',$phase)
                         ->update(['booked'=>'C','selected'=>'0','1stTrainingSch'=>null,'1stTrainingSch_2'=>null,'training_gr_cd'=>null,'forassembly'=>'','groupid'=>'0']); 
                    tbl_personnela::where('forzone','=',$zone)
                         ->where('personcd','=',$newpersonnel_id)
                         ->where('phase','=',$phase)
                         ->update(['booked'=>$booked,'forassembly'=>$forassembly,'selected'=>'1']);
//                    $ccount = tbl_first_rand_table::where('personcd', '=', $oldpersonnel_id)
//                             ->where('forzone','=',$zone)
//                             ->value(DB::raw("count(personcd)"));
//                    if($ccount>0){
//                       $slno = tbl_first_rand_table::where('personcd', '=', $oldpersonnel_id)
//                             ->where('forzone','=',$zone)
//                             ->value('sl_no');
//                    }else{
//                        $slno = 0;
//                    }
//                    tbl_first_rand_table::where('personcd', '=', $oldpersonnel_id)
//                             ->where('forzone','=',$zone)
//                             ->delete();
//                    tbl_first_rand_table::where('personcd', '=', $newpersonnel_id)
//                            ->where('forzone','=',$zone)
//                             ->delete();
                    //$del_ret1=delete_prev_data_single($oldpersonnel_id);                   
                    //$del_ret=delete_prev_data_single($newpersonnel_id);
                    //$sub_divcd = substr($newpersonnel_id,0,4);
                   // DB::select('call first_replace_pid(?,?,?,?,?)',[$zone,$newpersonnel_id,$session_personnela,"",$slno]);
                    if($chkSameVenueTraining=='true'){ 
                     DB::select('call first_update_replace_pid(?,?,?,?,?)',[$zone,$newpersonnel_id,$session_personnela,$oldpersonnel_id,$phase]);
                     $resMessage = "NEW Personnel ID has been replaced successfuly";
                     $status = 1;
                    }else{ 
                      tbl_first_rand_table::where('personcd', '=', $oldpersonnel_id)
                             ->where('forzone','=',$zone)
                             ->where('phase','=',$phase)
                             ->delete();
                             if(empty($v_sub)){ 
                                $tbl_personnela
                                ->where('forzone','=',$zone)
                                ->where('phase','=',$phase)
                                ->where('personcd','=',$newpersonnel_id)
                                ->update(['1stTrainingSch'=>$oldtraining_sch,
                                          'training_gr_cd' => $training_gr_cd]); 
                              }
                              if(empty($v_sub2)){ 
                                $tbl_personnela
                                ->where('forzone','=',$zone)
                                ->where('phase','=',$phase)
                                ->where('personcd','=',$newpersonnel_id)
                                ->update(['1stTrainingSch_2'=>$oldtraining_sch_sec,
                                          'training_gr_cd' => $training_gr_cd]); 
                              }
                             
                      if(!empty($v_sub) || !empty($v_sub2)){
                          
                        DB::select('call first_appt_replace_different_traing(?,?,?,?,?,?,?,?,?)',[$newpersonnel_id,$zone,$districtcd,$session_personnela,$v_sub,$v_sub2,$trainingHead,$orderno,$phase]);
                     
                    
                   
                   }
                     $resMessage = "NEW Personnel ID has been replaced successfuly";
                     $status = 2;  
                   }
               }else{
                   $resMessage = "NEW Personnel ID already replaced.Please try again";
                   $status = 0;
               }

                $response = array(
                   'options' => $resMessage,
                   'status' => $status
                );
            }catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
              $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
        
    }
    public function preCancelPPData(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        }
        else
        {
           $this->validate($request, [            
                'personnel_id' => 'required|alpha_num|max:11|min:11',
                'zone' => 'required|alpha_num|min:4|max:4',
                'forassembly' => 'required|alpha_num|min:3|max:3',
                'post_stat' => 'required|alpha_num|min:2|max:2',
               'reason' => 'required|regex:/^[A-Za-z0-9\s,-.]+$/i|max:30',
               'phase' => 'required|digits_between:1,1|min:1|max:1',
            ], [           
                'personnel_id.required' => 'OLD Personnel ID is required',
                'personnel_id.alpha_num' => 'OLD Personnel ID must be an alpha numeric',
                'zone.required' => 'Zone is required',
                'zone.alpha_num' => 'Zone must be an alpha numeric',
                'forassembly.required' => 'Forassembly is required',
                'forassembly.alpha_num' => 'Forassembly must be an alpha numeric',
                'post_stat.required' => 'Post Status is required',
                'post_stat.alpha_num' => 'Post Status must be an alpha numeric',
                'reason.required' => 'Reason is required',
                'reason.regex' => 'Reason must not be special characters',
                'reason.max' => 'Reason must be 30 characters',
                'phase.required' => 'Phase is required',
                'phase.digits_between'=>'Phase must be an integer',
            ]);
            
            try
            {
                $oldpersonnel_id=$request->personnel_id;
                $zone=$request->zone;
                $forassembly=$request->forassembly;
                $post_stat=$request->post_stat;
                $reason=$request->reason;
                $phase=$request->phase;
                $filtered=""; 
                $resMessage="";
               // $tbl_personnela1 = new tbl_personnela();             
                $session_personnela=session()->get('personnela_ppds');
                
                 $user_code=session()->get("code_ppds");  
                  tbl_personnela::where('personcd','=',$oldpersonnel_id)
                          ->where('forzone','=',$zone)
                          ->where('phase','=',$phase)
                        
                         ->update(['booked'=>'C','selected'=>'0','1stTrainingSch'=>null,'1stTrainingSch_2'=>null,'training_gr_cd'=>null,'usercode'=>$user_code,'forassembly'=>'','groupid'=>'0']);
                   tbl_first_rand_table::where('personcd', '=', $oldpersonnel_id)
                            ->where('forzone','=',$zone)
                            ->where('phase','=',$phase)
                             ->delete();
                $tbl_cancellation_log_pregroup = new tbl_cancellation_log_pregroup();
                $tbl_cancellation_log_pregroup->personnel_id = $oldpersonnel_id;
                $tbl_cancellation_log_pregroup->forzone = $zone;
                $tbl_cancellation_log_pregroup->phase_id = $phase;
                $tbl_cancellation_log_pregroup->reason = $reason;
                $tbl_cancellation_log_pregroup->usercode = $user_code;
                $tbl_cancellation_log_pregroup->save();
                
                $resMessage = "Personnel ID has been cancelled successfuly";
                $status = 1;
                $response = array(
                   'options' => $resMessage,
                   'status' => $status
                );
            }catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
              $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
        
    }
    
       //:::::::::::::::::::::::: Excel upload ::::::::::::::::::::://
    public function excelFormatForCancellation(Request $request) {
        $payments = ['Personnel Id', 'Reason'];
        return \Excel::create('CancellationFormat', function($excel) use ($payments) {
                    $excel->sheet(' ', function($sheet) use ($payments) {
                        $sheet->fromArray($payments);
                    });
                })->download('xlsx');
    }

    public function preGroupCancellationExcel(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
//            $validator = Validator::make(
//                [
//                    'file'      => $request->excelup,
//                    'extension' => strtolower($request->excelup->getClientOriginalExtension()),
//                ],
//                [
//                    'file'          => 'required',
//                    'extension'      => 'required|in:xlsx,xls',
//                ]
//              );
            $this->validate($request, [
                'excelup' => 'required',
                'zone' => 'required|alpha_num|min:4|max:4',
                'phase' => 'required|digits_between:1,1'
                    ], [
                'excelup.required' => 'File is required',
                'zone.required' => 'Zone is required',
                'zone.alpha_num' => 'Zone must be an alpha numeric',
                'phase.required'=>'Phase is required',
                'phase.digits_between'=>'phase must be integer and should be one digit'
            ]);


            try {
                if ($request->hasFile('excelup')) {
                    $path = $request->file('excelup')->getRealPath();
//                    $records = \Excel::load($path, function($reader) { $reader->noHeading = true; })->get();  
                    $records = \Excel::load($path)->get();
                    $records_count = count($records);
                  //  echo $records_count;die;

                    if ($records_count > 0) {
                        $firstrow = $records->first()->keys()->toArray();
                        $excelColumn = $records->first()->toArray();
                        $pp = count($excelColumn);
                       // print_r($firstrow);die;
                        // echo $pp;die;
                        if ($pp == 2) {

                            if ($firstrow[0] == "personnel_id" && $firstrow[1] == "reason" ) {

                                if ($records->count()) {
                                    $v = Validator::make($records->toArray(), [
                                                '*.' . $firstrow[0] => 'required|digits:11',
                                                '*.' . $firstrow[1] => 'required|regex:/(^[-0-9A-Za-z,\s]+$)/|max:50',
                                                    ], [
                                                '*.' . $firstrow[0] . '.required' => 'Personnel id is required.',
                                                '*.' . $firstrow[0] . '.digits' => 'Personnel id must be 11 digit.',
                                                '*.' . $firstrow[1] . '.required' => 'Reason is required.',
                                                '*.' . $firstrow[1] . '.regex' => 'Reason contains alphanumeric characters and , space only .',
                                                '*.' . $firstrow[1] . '.max' => 'Reason length may not be greater than 50 characters.',
                                    ]);
                                    $data = array();
                                    foreach ($v->errors()->toArray() as $key => $value) {
                                        $x = explode('.', $key);

                                        $data["$x[1]" . " Row " . "$x[0]"] = $value;
                                    }
                                    $response = $data;

                                    if ($v->fails()) {
                                        $statusCode = 422;
                                        return response()->json($response, $statusCode);
                                    }
                                    $filtered = "";
                                    $resMessage = "";
                                    
                                    $forzone = $request->zone;
                                    $phase = $request->phase;
                                    $session_personnela = session()->get('personnela_ppds');
                                    $user_code = session()->get("code_ppds");
                                    $count = 0;
                                    $Duplicatecount = 0;
                                    $Entrycount = 0;
                                    $arr = array();

                                    foreach ($records as $key => $value) {
                                        $count++;

                                        $tbl_personnela_check = tbl_personnela::where('personcd', '=', $value[$firstrow[0]])
                                                        ->where('forzone', '=', $forzone)
                                                        ->where('phase', '=', $phase)->where('booked', '=', 'C')->get();
                                       
                                        if (count($tbl_personnela_check) == 0) {
                                            $tbl_personnela = tbl_personnela::where('personcd', '=', $value[$firstrow[0]])
                                                    ->where('forzone', '=', $forzone)
                                                    ->where('phase', '=', $phase)
                                                    ->update(['booked' => 'C', 'selected' => '0', '1stTrainingSch' => null, '1stTrainingSch_2' => null, 'training_gr_cd' => null, 'usercode' => $user_code, 'forassembly' => '', 'groupid' => '0']);

                                            $tbl_first_rand_table = tbl_first_rand_table::where('personcd', '=', $value[$firstrow[0]])
                                                    ->where('forzone', '=', $forzone)
                                                    ->where('phase', '=', $phase)
                                                    ->delete();
                                            if ($tbl_first_rand_table == 1 && $tbl_first_rand_table == 1) {
                                                $tbl_cancellation_log_pregroup = new tbl_cancellation_log_pregroup();
                                                $tbl_cancellation_log_pregroup->personnel_id = $value[$firstrow[0]];
                                                $tbl_cancellation_log_pregroup->forzone = $forzone;
                                                $tbl_cancellation_log_pregroup->phase_id = $phase;
                                                $tbl_cancellation_log_pregroup->reason = $value[$firstrow[1]];
                                                $tbl_cancellation_log_pregroup->usercode = $user_code;
                                                $tbl_cancellation_log_pregroup->save();
                                            }
                                        }
                                        else{
                                            $Duplicatecount++;
                                        }
                                    }
                                   // echo $Duplicatecount;die;
                                    $resMessage="";
                                    $Entrycount = $count - $Duplicatecount;
                                    
                                    if($Entrycount>0){
                                        $resMessage .= $Entrycount." Record(s) cancelled successfully";
                                    }
                                    if($Duplicatecount>0){                                        
                                        $resMessage .= "</br><span style='color:red'>".$Duplicatecount." Record(s) are found duplicate</span>";
                                    }
                                    
                                    $status = 1;
                                    $response = array(
                                        'options' => $resMessage,
                                        'status' => $status
                                        );
                                    
                                }
                                
                            } else {
                                $response = array(
                                    'options' => "Check column name",
                                    'status' => 2
                                );
                            }
                        } else {
                            $response = array(
                                'options' => "kindly check no of column",
                                'status' => 4
                            );
                        }
                    } else {
                        $response = array(
                            'options' => "No Record found",
                            'status' => 3
                        );
                    }
                }
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }
//    public function delete_prev_data_single($pid){
//        $return = tbl_first_rand_table::where('personcd', '=', $pid)
//                             ->delete();
//        return $return;
//    }
//    public function first_appointment_letter_replace_new_id($newpersonnel_id){
//        DB::select('call randomisation1(?,?)',[$zone,$newpersonnel_id]);
//    }
    public function get_venue_with_not_fill_for_replace_old(Request $request){
    $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        }
        else
        {
           $this->validate($request, [            
            'subdivision' => 'required|alpha_num|min:4|max:4',
             ], [           
            'subdivision.required' => 'subdivision is required',
            'subdivision.alpha_num' => 'Subdivision must be an alpha numeric characters',
            ]);
            // dd($request->all());
            $subdivision=$request->subdivision;
            $trainingtype=$request->trainingtype;
            $post_stat=$request->post_stat;
            
            // echo $post_stat;die;
            $capacity=''; 
            if($post_stat=='P1'){
             $capacity='no_of_P1';   
            }else if($post_stat=='P2'){
             $capacity='no_of_P2';    
            }else if($post_stat=='P3'){
             $capacity='no_of_P3';    
            }else if($post_stat=='PA'){
              $capacity='no_of_PA';   
            }else if($post_stat=='PB'){
              $capacity='no_of_PB';   
            }else{
              $capacity='no_of_PR';   
            }
            try
            {
                $filtered=tbl_first_rand_table::rightjoin('first_training_schedule', function ($join) {
                    $join->on('first_training_schedule.schedule_code','=','first_rand_table.schedule_code')->orOn('first_training_schedule.schedule_code','=','first_rand_table.schedule_code_2');
                    })->leftjoin('first_training_venue','first_training_venue.venue_cd','=','first_training_schedule.tr_venue_cd')
                        ->leftjoin('first_training_date_time','first_training_date_time.datetime_cd','=','first_training_schedule.datetimecd')
                        ->leftjoin('first_training_subvenue','first_training_subvenue.subvenue_cd','=','first_training_schedule.tr_subvenue_cd')
                        ->leftjoin('training_type','training_type.training_code','=','first_training_schedule.training_type')
                        ->where('training_type.training_group_cd',$trainingtype)
                        ->where('first_training_schedule.subdivisioncd',$subdivision)
                        // ->where('first_training_venue.status','1')
                        ->groupBy('first_training_schedule.training_type','first_training_schedule.schedule_code','first_training_venue.venuename','first_training_subvenue.subvenue','first_training_date_time.training_dt','first_training_date_time.training_time','first_training_schedule.'.$capacity)
                        ->havingRaw('used<first_training_schedule.'.$capacity)->select('first_training_schedule.training_type','first_training_schedule.schedule_code','first_training_venue.venuename','first_training_subvenue.subvenue',DB::raw("DATE_FORMAT(first_training_date_time.training_dt,'%d/%m/%Y') as training_dt"),'first_training_date_time.training_time',DB::raw('COUNT(first_rand_table.personcd) as used'),'first_training_schedule.'.$capacity)
                        ->get();
                        // dd($filtered);
                //echo json_encode($filtered);
                $response = array(
                   'options' => $filtered,
                   'status' => 1
                );
            }catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
              $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }   
   }


   public function get_venue_with_not_fill_for_replace(Request $request){
    $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        }
        else
        {
           $this->validate($request, [            
            'subdivision' => 'required|alpha_num|min:4|max:4',
             ], [           
            'subdivision.required' => 'subdivision is required',
            'subdivision.alpha_num' => 'Subdivision must be an alpha numeric characters',
            ]);
           
            $subdivision=$request->subdivision;
            $trainingHead=$request->trainingtype;
            $post_stat=$request->post_stat;
            
          
            try
            {
                $firsttraining='';
                $secondtraining='';
                $firstscheduleArr=array();
                $secondscheduleArr=array();
                if($trainingHead==1){
                    $firsttraining='01';
                    $secondtraining='02';
                } else if($trainingHead==2){
                    $firsttraining='03';
                    $secondtraining='04';
                }
                else if($trainingHead==3){
                    $firsttraining='05';
                    $secondtraining='06';
                }
                if($post_stat=='P1'){
                    $capacity='no_of_P1';   
                   }else if($post_stat=='P2'){
                    $capacity='no_of_P2';    
                   }else if($post_stat=='P3'){
                    $capacity='no_of_P3';    
                   }else if($post_stat=='PA'){
                     $capacity='no_of_PA';   
                   }else if($post_stat=='PB'){
                     $capacity='no_of_PB';   
                   }else{
                     $capacity='no_of_PR';   
                   }
                $getScheduleCode1=tbl_first_training_schedule::where('subdivisioncd',$subdivision)
                ->where('training_type',$firsttraining)
                ->whereNotNull($capacity)->select('schedule_code','no_of_PR','no_of_P1','no_of_P2','no_of_P3')->get();
                $getScheduleCode2=tbl_first_training_schedule::where('subdivisioncd',$subdivision)
                ->whereNotNull($capacity)->where('training_type',$secondtraining)->get();

                foreach( $getScheduleCode1 as  $getScheduleCode1){
                    $firstRandTableCount=tbl_first_rand_table::where('traininghead',$trainingHead)
                    ->where('poststat',$post_stat) ->where('schedule_code',$getScheduleCode1->schedule_code)->count();
                    $firstScheduleCount=$getScheduleCode1->$capacity;
                    if($firstScheduleCount> $firstRandTableCount){
                        array_push($firstscheduleArr,$getScheduleCode1->schedule_code);
                    }

                }
                foreach( $getScheduleCode2 as  $getScheduleCode2){
                    $firstRandTableCount2=tbl_first_rand_table::where('traininghead',$trainingHead)
                    ->where('poststat',$post_stat) ->where('schedule_code_2',$getScheduleCode2->schedule_code)->count();
                    $secondScheduleCount=$getScheduleCode2->$capacity;
                    if($secondScheduleCount> $firstRandTableCount2){
                        array_push($secondscheduleArr,$getScheduleCode2->schedule_code);
                    }

                }
$array_merge=array_merge($firstscheduleArr,$secondscheduleArr);
// dd($array_merge);
//($array_merge);die;
                $filtered=tbl_first_training_schedule::leftjoin('first_training_venue','first_training_venue.venue_cd','=','first_training_schedule.tr_venue_cd')
                        ->leftjoin('first_training_date_time','first_training_date_time.datetime_cd','=','first_training_schedule.datetimecd')
                        ->leftjoin('first_training_subvenue','first_training_subvenue.subvenue_cd','=','first_training_schedule.tr_subvenue_cd')
                        ->leftjoin('training_type','training_type.training_code','=','first_training_schedule.training_type')
                        ->where('training_type.training_group_cd',$trainingHead)
                        ->where('first_training_schedule.subdivisioncd',$subdivision)
                        ->whereIn('schedule_code',$array_merge)
                        ->groupBy('first_training_schedule.training_type','first_training_schedule.schedule_code',
                        'first_training_venue.venuename','first_training_subvenue.subvenue',
                        'first_training_date_time.training_dt','first_training_date_time.training_time')
                        ->select('first_training_schedule.training_type','first_training_schedule.schedule_code',
                        'first_training_venue.venuename','first_training_subvenue.subvenue',
                        DB::raw("DATE_FORMAT(first_training_date_time.training_dt,'%d/%m/%Y') as training_dt"),
                        'first_training_date_time.training_time')->get();

                        
                // $filtered=tbl_first_rand_table::rightjoin('first_training_schedule', function ($join) {
                //     $join->on('first_training_schedule.schedule_code','=','first_rand_table.schedule_code')->orOn('first_training_schedule.schedule_code','=','first_rand_table.schedule_code_2');
                //     })->leftjoin('first_training_venue','first_training_venue.venue_cd','=','first_training_schedule.tr_venue_cd')
                //         ->leftjoin('first_training_date_time','first_training_date_time.datetime_cd','=','first_training_schedule.datetimecd')
                //         ->leftjoin('first_training_subvenue','first_training_subvenue.subvenue_cd','=','first_training_schedule.tr_subvenue_cd')
                //         ->leftjoin('training_type','training_type.training_code','=','first_training_schedule.training_type')
                //         ->where('training_type.training_group_cd',$trainingtype)
                //         ->where('first_training_schedule.subdivisioncd',$subdivision)
                //         // ->where('first_training_venue.status','1')
                //         ->groupBy('first_training_schedule.training_type','first_training_schedule.schedule_code','first_training_venue.venuename','first_training_subvenue.subvenue','first_training_date_time.training_dt','first_training_date_time.training_time','first_training_schedule.'.$capacity)
                //         ->havingRaw('used<first_training_schedule.'.$capacity)->select('first_training_schedule.training_type','first_training_schedule.schedule_code','first_training_venue.venuename','first_training_subvenue.subvenue',DB::raw("DATE_FORMAT(first_training_date_time.training_dt,'%d/%m/%Y') as training_dt"),'first_training_date_time.training_time',DB::raw('COUNT(first_rand_table.personcd) as used'),'first_training_schedule.'.$capacity)
                //         ->get();
                        // dd($filtered);
                //echo json_encode($filtered);

                    $poststat = tbl_poststat::where('post_stat',$post_stat)->select('post_stat')->get();
                   // dd($poststat);

                $response = array(
                   'options' => $filtered,
                   'poststat'=> $poststat,
                   'status' => 1
                );
            }catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
              $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }   
   }
}