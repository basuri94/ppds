<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\tbl_personnela;
use App\tbl_assembly_district;
use App\tbl_second_training_subvenue;

use DB;

class edpdExcelController extends Controller {

    public function getExcelOfPDED(Request $request) {
        $this->validate($request, [
            'typeOfEdPd' => 'required|alpha|min:1|max:1'
                ], [
            'typeOfEdPd.required' => 'Type is required',
            'typeOfEdPd.alpha' => 'Type must be an alpha characters'
        ]);

        $typeOfEdPd = $request->typeOfEdPd;
        if ($typeOfEdPd == "E") {
            $reportName = "ED";
        } else {
            $reportName = "PB";
        }

        $data = array();
        $session_personnela = session()->get('personnela_ppds');
        $districtcd_ppds = session()->get('districtcd_ppds');
        $tbl_personnela = new tbl_personnela();



        $recordsTotal = tbl_personnela::join('office', '' . $session_personnela . '.officecd', '=', 'office.officecd')
                ->join('personnel', '' . $session_personnela . '.personcd', '=', 'personnel.personcd')
                ->join('poststat', '' . $session_personnela . '.poststat', '=', 'poststat.post_stat')
                ->orderBy('' . $session_personnela .'.forassembly')
                ->select('' . $session_personnela . '.personcd', 'personnel.officer_name', 'personnel.off_desg', 'office.office', 'poststat.poststatus', 'personnel.gender', 'personnel.acno', 'personnel.slno', 'personnel.partno','' . $session_personnela .'.forassembly');
        
        if ($typeOfEdPd != "") {
            $recordsTotal = $recordsTotal->where('ed_pb', '=', $typeOfEdPd);
        }
        $recordsTotal = $recordsTotal->get();
        foreach ($recordsTotal as $recordsTo) {
            $nestedData1['Forassembly'] = $recordsTo->forassembly;
            $nestedData1['Personcd'] = $recordsTo->personcd;
            $nestedData1['Officer Name'] = $recordsTo->officer_name;
            $nestedData1['Designation'] = $recordsTo->off_desg;
            $nestedData1['Office Name'] = $recordsTo->office;
            $nestedData1['Poststat'] = $recordsTo->poststatus;
            $nestedData1['Gender'] = $recordsTo->gender;
            $nestedData1['Ac No'] = $recordsTo->acno;
            $nestedData1['Sl No'] = $recordsTo->slno;
            $nestedData1['part No'] = $recordsTo->partno;

            $data[] = $nestedData1;
        }

        //$data1=$nestedData1;       
        // array_push($data,$data1);
        date_default_timezone_set('Asia/Calcutta');
        $dt = date_create('now')->format('YmdHi');
        $reportNameF = $reportName . $dt;
        return \Excel::create($reportNameF, function($excel) use ($data) {
                    $excel->sheet(' ', function($sheet) use ($data) {
                        $sheet->fromArray($data);
                    });
                })->download('xlsx');
    }

    public function updateHomePC(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            DB::beginTransaction();
            try {

                $data = array();
                $session_personnela = session()->get('personnela_ppds');
                
                
                $tbl_personnela = new tbl_personnela();
                $updatePersonnela=$tbl_personnela::whereRaw('1 = 1')
                        ->update(['' . $session_personnela . '.home_pc' => NULL]);
                if($updatePersonnela>0){
                    $filtered = $tbl_personnela->join('personnel', 'personnel.personcd', '=', '' . $session_personnela . '.personcd')
                        ->join('assembly_district', 'assembly_district.assemblycd', '=', 'personnel.acno')
                        
        
       ->whereIn('personnel.acno', function($query){
            $districtcd_ppds = session()->get('districtcd_ppds');
    $query->select('assembly_district.assemblycd')
    ->from('assembly_district')->where('assembly_district.districtcd','=',$districtcd_ppds)
      ->groupBY('assembly_district.assemblycd');
    })
    
                        ->update(['' . $session_personnela . '.home_pc' => DB::raw('assembly_district.pccd')]);
                }
                else{
                    $filtered=0;
                }
                $updateOtherHomePc = $tbl_personnela::whereNull('home_pc')
                        ->update(['' . $session_personnela . '.home_pc' => 99]);

//                           if ($filtered > 0 || $filtered==0) {
//
//                        $filteredLa = $tbl_personnela->whereNotIn('' . $session_personnela . '.home_pc', function($query) {
//                                    $districtcd = session()->get('districtcd_ppds');
//                                    $query->select('assembly_districts.pccd')->from('assembly_district')
//                                    ->where('assembly_district.districtcd', '=', $districtcd)
//                                    ->groupBy('assembly_district.pccd');
//                                })
//                                ->update(['' . $session_personnela . '.home_pc' => 99]);
//                    }


                $response = array(
                    'filtered' => $filtered,
                      'filteredLa' => $updateOtherHomePc,
                    'status' => 1);
                 
                DB::commit();
            } catch (\Exception $e) {
               DB::rollback();
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {

                return response()->json($response, $statusCode);
            }
        }
    }

    public function edpdGen(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            DB::beginTransaction();
            try {

                $data = array();
                $session_personnela = session()->get('personnela_ppds');
                $districtcd_ppds = session()->get('districtcd_ppds');
                $tbl_personnela = new tbl_personnela();
                $tbl_assembly_district = new tbl_assembly_district();
                $tbl_assembly_district_details = $tbl_assembly_district->join('pc','pc.pccd','=','assembly_district.pccd')
                        ->select('assembly_district.pccd','pc.pcname')->where('districtcd', '=', $districtcd_ppds)
                                ->groupBy('assembly_district.pccd')->get();
               // echo json_encode($tbl_assembly_district_details);die;
                $sum = 0;
                $data = array();
                foreach ($tbl_assembly_district_details as $tbl_assembly_district_details1) {
                    $fetchAssembly = DB::table('assembly_district')->where('assembly_district.pccd', '=', $tbl_assembly_district_details1->pccd)
                                    ->where('districtcd', '=', $districtcd_ppds)->pluck('assembly_district.assemblycd')->toArray();

                    $updateED = $tbl_personnela::whereIn('' . $session_personnela . '.forassembly', $fetchAssembly)
                            ->where('' . $session_personnela . '.home_pc', '=', $tbl_assembly_district_details1->pccd)
                             ->where('' . $session_personnela . '.selected', '=',1)
                            ->update(['' . $session_personnela . '.ed_pb' => "E"]);
                    $dataPccd['dataPccd'] = $tbl_assembly_district_details1->pccd;
                    $dataPccd['count'] = $updateED;
                    $dataPccd['pcname'] = $tbl_assembly_district_details1->pcname;
                    array_push($data, $dataPccd);
                     // echo json_encode($fetchAssembly);die;
                }
              
                
                $updatePB = $tbl_personnela::whereNull('ed_pb')
                        ->where('' . $session_personnela . '.selected', '=',1)
                        ->update(['' . $session_personnela . '.ed_pb' => "P"]);

                $response = array(
                    // 'updateRecord' => $updateRecord,
                    //'filteredLa' => $filteredLa,
                    'totalDataPc' => $data,
                    'pb_count' => $updatePB,
                    'status' => 1);
                 
                DB::commit();
            } catch (\Exception $e) {
               DB::rollback();

                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {

                return response()->json($response, $statusCode);
            }
        }
    }
    
    public function get2ndSubVenueName(Request $request){
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        }
        else
        {
            $this->validate($request, [            
            'venuename' => 'nullable|alpha_num|min:6|max:6',
            'trainingdatetime' => 'nullable|alpha_num|min:4|max:4'   
            ], [

            //'venuename.required' => 'Venue name is required',
            'venuename.alpha_num' => 'Venue name must be an alpha numeric',
         
            'trainingdatetime.alpha_num' => 'Training Date & Time must be an alpha numeric'
            ]);
            try
            {
              $venuename=$request->venuename;
              $trainingdatetime=$request->trainingdatetime;
              $data = array();
              $list = '';
              $list = tbl_second_training_subvenue::join('second_training_venue','second_training_venue.venue_cd','=','second_training_subvenue.venue_cd')
                      ->where('second_training_venue.venue_cd','=',$venuename)                                        
                   
                       -> pluck('second_training_subvenue.subvenue','second_training_subvenue.subvenue_cd')->all();
                                       
                                       
              
             
              $response = array(
                   'options' => $list,
                   'status' => 1);          
            }catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
              $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
    }
    }
    
    public function getEDPBEXCEL(Request $request){
         $this->validate($request, [
            'forZone' => 'required|alpha_num'
                ], [
            'forZone.required' => 'Type is required',
            'forZone.alpha_num' => 'Type must be an alphanumeric characters'
        ]);

 $forZone = $request->forZone;
          $assembly = $request->assembly;
            $subdivision = $request->subdivision;
              $subvenuename = $request->subvenuename;
              $venuename = $request->venuename;
                  $trainingdatetime = $request->trainingdatetime;
                  $typeOfEdPd=$request->typeOfEdPd;
       
        $data = array();
        $session_personnela = session()->get('personnela_ppds');
        $districtcd_ppds = session()->get('districtcd_ppds');
        $tbl_personnela = new tbl_personnela();

if($typeOfEdPd=="E"){
    $recordsTotal = tbl_personnela::join('second_training_schedule','second_training_schedule.schedule_code','=', '' . $session_personnela . '.2ndTrainingSch')
               ->join('second_training_subvenue','second_training_subvenue.subvenue_cd','=','second_training_schedule.tr_subvenue_cd')
                ->join('second_training_venue','second_training_venue.venue_cd','=','second_training_schedule.tr_venue_cd')
                 ->join('second_training_date_time','second_training_date_time.datetime_cd','=','second_training_schedule.datetimecd')
                ->join('office', '' . $session_personnela . '.officecd', '=', 'office.officecd')
                ->join('personnel', '' . $session_personnela . '.personcd', '=', 'personnel.personcd')
                ->join('poststat', '' . $session_personnela . '.poststat', '=', 'poststat.post_stat')
                ->orderBy('second_training_date_time.training_dt')
                 ->orderBy('second_training_date_time.training_time')
              ->orderBy('second_training_venue.venuename')
                ->orderBy('' . $session_personnela .'.forassembly')
              
                  ->orderBy('' . $session_personnela . '.personcd')
                ->select('' . $session_personnela . '.personcd', 'personnel.officer_name', 'personnel.off_desg', 'office.office', 'poststat.poststatus', 'personnel.gender', 'personnel.acno', 'personnel.slno', 'personnel.partno','' . $session_personnela .'.forassembly',
                        'second_training_venue.venuename','second_training_subvenue.subvenue','second_training_date_time.training_time',DB::raw("(DATE_FORMAT(second_training_date_time.training_dt,'%d/%m/%Y')) as training_dt"),
                       '' . $session_personnela . '.ed_pb');
}
else{
    $recordsTotal = tbl_personnela::join('second_training_schedule','second_training_schedule.schedule_code','=', '' . $session_personnela . '.2ndTrainingSch')
               ->join('second_training_subvenue','second_training_subvenue.subvenue_cd','=','second_training_schedule.tr_subvenue_cd')
                ->join('second_training_venue','second_training_venue.venue_cd','=','second_training_schedule.tr_venue_cd')
                 ->join('second_training_date_time','second_training_date_time.datetime_cd','=','second_training_schedule.datetimecd')
                ->join('office', '' . $session_personnela . '.officecd', '=', 'office.officecd')
                ->join('personnel', '' . $session_personnela . '.personcd', '=', 'personnel.personcd')
                ->join('poststat', '' . $session_personnela . '.poststat', '=', 'poststat.post_stat')
                
              ->orderBy('second_training_venue.venuename')
            ->orderBy('personnel.officer_name')
            
               ->orderBy('' . $session_personnela . '.personcd')
                ->select('' . $session_personnela . '.personcd', 'personnel.officer_name', 'personnel.off_desg', 'office.office', 'poststat.poststatus', 'personnel.gender', 'personnel.acno', 'personnel.slno', 'personnel.partno','' . $session_personnela .'.forassembly',
                        'second_training_venue.venuename','second_training_subvenue.subvenue','second_training_date_time.training_time',DB::raw("(DATE_FORMAT(second_training_date_time.training_dt,'%d/%m/%Y')) as training_dt"),
                       '' . $session_personnela . '.ed_pb');
}

        
         if ($forZone != '') {
                $recordsTotal = $recordsTotal->where('' . $session_personnela .'.forzone', '=', $forZone);
            }
            if ($assembly != '') {
                $recordsTotal = $recordsTotal->where('' . $session_personnela .'.forassembly', '=', $assembly);
            }
            if ($subdivision != '') {
                $recordsTotal = $recordsTotal->where('second_training_schedule.subdivision', '=', $subdivision);
            }
            if ($subvenuename != '') {
                $recordsTotal = $recordsTotal->where('second_training_schedule.tr_subvenue_cd', '=', $subvenuename);
            }
            if ($venuename != '') {
                $recordsTotal = $recordsTotal->where('second_training_schedule.tr_venue_cd', '=', $venuename);
            }
            if ($trainingdatetime != '') {
                $recordsTotal = $recordsTotal->where('second_training_schedule.datetimecd', '=', $trainingdatetime);
            }
             if ($typeOfEdPd != '') {
                $recordsTotal = $recordsTotal->where('' . $session_personnela .'.ed_pb', '=', $typeOfEdPd);
            }
            
       
        $recordsTotal = $recordsTotal->get();
        foreach ($recordsTotal as $recordsTo) {
            $nestedData1['Venue Details'] = $recordsTo->venuename." , ". $recordsTo->subvenue." , ".$recordsTo->training_dt." , ".$recordsTo->training_time;
            $nestedData1['Personcd'] = $recordsTo->personcd;
            $nestedData1['Officer Name'] = $recordsTo->officer_name;
            $nestedData1['Designation'] = $recordsTo->off_desg;
           
            $nestedData1['Poststat'] = $recordsTo->poststatus;
             $nestedData1['ED & PB'] = $recordsTo->ed_pb;
              $nestedData1['AC No'] = $recordsTo->acno;
               $nestedData1['Sl No'] = $recordsTo->slno;
          $nestedData1['Part No'] = $recordsTo->partno;
         
             $nestedData1['Forassembly'] = $recordsTo->forassembly;
             

            $data[] = $nestedData1;
        }

        //$data1=$nestedData1;       
        // array_push($data,$data1);
        date_default_timezone_set('Asia/Calcutta');
        $dt = date_create('now')->format('YmdHi');
        $reportNameF = "EDPBExcelVenue" . $dt;
        return \Excel::create($reportNameF, function($excel) use ($data) {
                    $excel->sheet(' ', function($sheet) use ($data) {
                        $sheet->fromArray($data);
                    });
                })->download('xlsx');
    }
}
