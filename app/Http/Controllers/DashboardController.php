<?php

namespace App\Http\Controllers;

use App\tbl_block_muni;
use App\tbl_subdivision;
use App\tbl_institute;
use App\tbl_office;
use App\tbl_personnela;
use App\tbl_govtcategory;
use App\tbl_poststat;
use App\tbl_assembly;
use App\tbl_district;
use App\tbl_zone;
use Illuminate\Http\Request;
use DB;

class DashboardController extends Controller {
    
 //:::::::::::::::Download Excel Repoort::::::::::::::::::::://
    public function personnelaReport(Request $request) {
        $this->validate($request, [ 
            'subdivid' => 'required|alpha_num|min:4|max:4'
            ], [
            'subdivid.required' => 'Subdivision is required',
            'subdivid.alpha_num' => 'Subdivision code must be an alpha numeric'
           ]);
        $subdivid=$request->subdivid;
        date_default_timezone_set('Asia/Calcutta'); 
        $dt = date_create('now')->format('YmdHi');
        $excelTitle=$subdivid."Excel".$dt;
        $data = array();
         $recordsTotal = tbl_personnela
                        ::where(DB::raw('substring(personcd,1,4)'),'=', $subdivid) 
                       ->select('personcd','phase','officecd','poststat','no_of_member','gender',
                                'assembly_temp','assembly_off','assembly_perm','fordistrict','forzone',
                                'forassembly','groupid','booked','dcrccd','selected','home_pc','priority','training_gr_cd','1stTrainingSch','1stTrainingSch_2','2ndTrainingSch')->get();
        foreach($recordsTotal as $recordsTo){
             $fst='1stTrainingSch';
             $fst_1='1stTrainingSch_2';
             $sst='2ndTrainingSch';
             $nestedData1['personcd']=$recordsTo->personcd; 
             $nestedData1['phase']=$recordsTo->phase; 
             $nestedData1['officecd']=$recordsTo->officecd; 
             $nestedData1['poststat']=$recordsTo->poststat; 
             $nestedData1['no_of_member']=$recordsTo->no_of_member; 
             $nestedData1['gender']=$recordsTo->gender; 
             $nestedData1['assembly_temp']=$recordsTo->assembly_temp; 
             $nestedData1['assembly_off']=$recordsTo->assembly_off;
             $nestedData1['assembly_perm']=$recordsTo->assembly_perm;
             $nestedData1['Fordistrict']=$recordsTo->fordistrict; 
             $nestedData1['Forzone']=$recordsTo->forzone;

             $nestedData1['Forassembly']=$recordsTo->forassembly;
             $nestedData1['Groupid']=$recordsTo->groupid; 
             $nestedData1['Booked']=$recordsTo->booked;
             $nestedData1['Dcrccd']=$recordsTo->dcrccd;
             $nestedData1['Selected']=$recordsTo->selected;
             $nestedData1['Home PC']=$recordsTo->home_pc;
             $nestedData1['Priority']=$recordsTo->priority;
             $nestedData1['1stTraining Group Code']=$recordsTo->training_gr_cd;
             $nestedData1['1stTrainingSch']=$recordsTo->$fst; 
             $nestedData1['1stTrainingSch_2']=$recordsTo->$fst_1; 
             $nestedData1['2ndTrainingSch']=$recordsTo->$sst; 
             $data[]=$nestedData1; 
        }
        
        return \Excel::create($excelTitle, function($excel) use ($data) {
                    $excel->sheet(' ', function($sheet) use ($data) {
                        $sheet->fromArray($data);
                    });
                })->download('xlsx');
    }
    public static function Get_block_all() {
        $dist = \Session::get('districtcd_ppds');
        $session_personnela = session()->get('personnela_ppds');
         $block = tbl_personnela::join('office', '' . $session_personnela . '.officecd', '=', 'office.officecd')
                ->join('block_muni', 'office.blockormuni_cd', '=', 'block_muni.blockminicd')
                ->distinct()
                ->pluck('block_muni.blockmuni')->toArray();
//        $block = new tbl_block_muni();
//        if ($dist != NULL) {
//            $block = $block->where('districtcd', $dist);
//        }
//        $block = $block->pluck('blockmuni')->toArray();

        return $block;
    }

    public static function get_block_wise_report($data_block) {
        $block_count = count($data_block);
        $arr_select = array("poststat");

        for ($i = 0; $i < $block_count; $i++) {
            $select = DB::raw("Count(case when blockmuni ='$data_block[$i]' then poststat end) as 'C" . $i . "'");
            array_push($arr_select, $select);
        }
        $session_personnela = session()->get('personnela_ppds');
        $data = tbl_personnela::join('office', '' . $session_personnela . '.officecd', '=', 'office.officecd')->join('block_muni', 'office.blockormuni_cd', '=', 'block_muni.blockminicd')
                        ->select($arr_select)->groupBy('poststat')->get();
        if (count($data) != 0) {
            foreach ($data as $arr) {
                $new['name'] = $arr->poststat;
                $data_new = array();
                for ($j = 0; $j < $block_count; $j++) {
                    $k = 'C' . $j;
                    array_push($data_new, $arr->$k);
                }
                //array_push($data_new,array_sum($data_new));
                $new['data'] = $data_new;
                $data_final[] = $new;
            }
        } else {
            $data_final[] = "";
        }
        return $data_final;
    }

    public static function get_all_institute() {
        $institute = tbl_institute::pluck('institute')->toArray();
        return $institute;
    }

    public static function get_institution_wise_report_pie($ins) {
        $ins_count = count($ins);
        $arr_select = array("poststat");
        $data_new = array();
        for ($i = 0; $i < $ins_count; $i++) {
            $select = DB::raw("Count(case when institute ='$ins[$i]' then poststat end) as 'C" . $i . "'");
            array_push($arr_select, $select);
        }
        $session_personnela = session()->get('personnela_ppds');
        $data = tbl_personnela::join('office', '' . $session_personnela . '.officecd', '=', 'office.officecd')->join('institute', 'office.institutecd', '=', 'institute.institutecd')
                        ->select($arr_select)->get();
        if (count($data) != 0) {
            foreach ($data as $arr) {


                for ($j = 0; $j < $ins_count; $j++) {
                    $k = 'C' . $j;

                    array_push($data_new, $arr->$k);
                }
                //array_push($data_new,array_sum($data_new));
                // $new['data']=$data_new;
                // $data_final[] = $new;
            }
        }
        return $data_new;
    }

    public static function get_institution_wise_report($ins) {

        $ins_count = count($ins);
        $arr_select = array("poststat");

        for ($i = 0; $i < $ins_count; $i++) {
            $select = DB::raw("Count(case when institute ='$ins[$i]' then poststat end) as 'C" . $i . "'");
            array_push($arr_select, $select);
        }
        $session_personnela = session()->get('personnela_ppds');
        $data = tbl_personnela::join('office', '' . $session_personnela . '.officecd', '=', 'office.officecd')->join('institute', 'office.institutecd', '=', 'institute.institutecd')
                        ->select($arr_select)->groupBy('poststat')->get();
        if (count($data) != 0) {
            foreach ($data as $arr) {
                $new['name'] = $arr->poststat;
                $data_new = array();
                for ($j = 0; $j < $ins_count; $j++) {
                    $k = 'C' . $j;

                    array_push($data_new, $arr->$k);
                }
                //array_push($data_new,array_sum($data_new));
                $new['data'] = $data_new;
                $data_final[] = $new;
            }
        } else {
            $data_final[] = "";
        }
        return $data_final;
    }

    public static function get_all_govt() {
        $govt = tbl_govtcategory::pluck('govt_description')->toArray();
        return $govt;
    }

    public static function get_govt_wise_report_pie($data_govt) {
        $gov_count = count($data_govt);
        $arr_select = array("poststat");
        $data_new = array();
        for ($i = 0; $i < $gov_count; $i++) {
            $select = DB::raw("Count(case when govt_description ='$data_govt[$i]' then poststat end) as 'C" . $i . "'");
            array_push($arr_select, $select);
        }
        $session_personnela = session()->get('personnela_ppds');
        $data = tbl_personnela::join('office', '' . $session_personnela . '.officecd', '=', 'office.officecd')->join('govtcategory', 'office.govt', '=', 'govtcategory.govt')
                        ->select($arr_select)->get();
        if (count($data) != 0) {
            foreach ($data as $arr) {
                for ($j = 0; $j < $gov_count; $j++) {
                    $k = 'C' . $j;

                    array_push($data_new, $arr->$k);
                }
                //array_push($data_new,array_sum($data_new));
            }
        }
        return $data_new;
    }

    public static function get_govt_wise_report($data_govt) {
        $gov_count = count($data_govt);
        $arr_select = array("poststat");

        for ($i = 0; $i < $gov_count; $i++) {
            $select = DB::raw("Count(case when govt_description='$data_govt[$i]' then poststat end) as 'C" . $i . "'");
            array_push($arr_select, $select);
        }
        $session_personnela = session()->get('personnela_ppds');
        $data = tbl_personnela::join('office', '' . $session_personnela . '.officecd', '=', 'office.officecd')->join('govtcategory', 'office.govt', '=', 'govtcategory.govt')
                        ->select($arr_select)->groupBy('poststat')->get();
        if (count($data) != 0) {
            foreach ($data as $arr) {
                $new['name'] = $arr->poststat;
                $data_new = array();
                for ($j = 0; $j < $gov_count; $j++) {
                    $k = 'C' . $j;

                    array_push($data_new, $arr->$k);
                }
                //array_push($data_new,array_sum($data_new));
                $new['data'] = $data_new;
                $data_final[] = $new;
            }
        } else {
            $data_final[] = "";
        }
        return $data_final;
    }

    public static function get_all_postat() {
        $postat_a = tbl_poststat::orderBy('post_stat')->pluck('poststatus')->toArray();
        $postat_b = tbl_poststat::orderBy('post_stat')->pluck('post_stat')->toArray();
        $postat = [$postat_a, $postat_b];
        return $postat;
    }

    public static function district_wise_postat_required($data_postat) {
        $dataP = array();
        $dataR = array();
        foreach ($data_postat as $postat) {
            $dist = \Session::get('districtcd_ppds');
            $data = tbl_assembly::where('districtcd', '=', $dist)->join('assembly_party', 'assembly_party.assemblycd', '=', 'assembly.assemblycd')
                            ->join('reserve', function($join) {
                                $join->on('reserve.forassembly', '=', 'assembly_party.assemblycd');
                                $join->on('reserve.number_of_member', '=', 'assembly_party.no_of_member');
                                $join->on('reserve.gender', '=', 'assembly_party.gender');
                            })->select(DB::raw("SUM(no_party) AS P"),DB::raw("SUM(case when no_or_pc ='P' then ROUND(no_party * numb/100) ELSE numb end) AS R"));
            if ($postat != "Total") {
                $data = $data->where('poststat', '=', $postat);
            } else {
                $data = $data->where('assembly.assemblycd', '>', 0);
            }
            $data = $data->get();
            if ($data[0]->P == NULL) {
                $req_p = "0";
            } else {
                $req_p = $data[0]->P;
            }
            if ($data[0]->R == NULL) {
                $req_r = "0";
            } else {
                $req_r = $data[0]->R;
            }

            array_push($dataP, $req_p);
            array_push($dataR, $req_r);
        }

        $data_req = [
            'P' => $dataP,
            'R' => $dataR
        ];
        return $data_req;
    }

    public static function district_wise_postat_assigned($data_postat) {
        $dataP = array();
        $dataR = array();
        foreach ($data_postat as $postat) {
            $dist = \Session::get('districtcd_ppds');
            $data = tbl_personnela::where('fordistrict', '=', $dist)
                    ->select(DB::raw("Count(case when booked ='P' then personcd end) as P"), DB::raw("Count(case when booked ='R' then personcd end) as R"));
            if ($postat != "Total") {
                $data = $data->where('poststat', '=', $postat);
            } else {
                $data = $data->where('personcd', '>', 0);
            }
            $data = $data->get();
            if ($data[0]->P == NULL) {
                $req_p = 0;
            } else {
                $req_p = $data[0]->P;
            }
            if ($data[0]->R == NULL) {
                $req_r = 0;
            } else {
                $req_r = $data[0]->R;
            }

            array_push($dataP, $req_p);
            array_push($dataR, $req_r);
        }
        $data_req = [
            'P' => $dataP,
            'R' => $dataR
        ];
        return $data_req;
    }
     public static function Get_distName() {
        $dist = \Session::get('districtcd_ppds');
        $data = tbl_district::where('districtcd','=',$dist)->value('district');
        return $data;
    }
    public static function Get_subdivName() {
        $subdivSessionCode = \Session::get('subdivisioncd_ppds');
        $data = tbl_subdivision::where('subdivisioncd','=',$subdivSessionCode)->value('subdivision');
        return $data;
    }
    public static function Get_zoneName() {
        $zoneSessionCode = \Session::get('zonecd_ppds');
        $data = tbl_zone::where('zone','=',$zoneSessionCode)->value('zonename');
        return $data;
    }
    public static function Get_total_office_all() {
        $dist = \Session::get('districtcd_ppds');
         $session_personnela = session()->get('personnela_ppds');
         $data = tbl_personnela::select(DB::raw('count(distinct(officecd)) as cnt'))->get();
         
        return json_decode($data);
    }
     public static function Get_total_pollingpp_all() {
        $dist = \Session::get('districtcd_ppds');
         $session_personnela = session()->get('personnela_ppds');
         $data = tbl_personnela::select(DB::raw('count(personcd) as cnt'))->get();  
        return json_decode($data);
    }
    public static function Get_subdivision_all() {
        $dist = \Session::get('districtcd_ppds');
//        $subdivision = new tbl_subdivision();
//        if ($dist != NULL) {
//            $subdivision = $subdivision->where('districtcd', $dist);
//        }
//        $subdivision = $subdivision->pluck('subdivision')->toArray();
         $session_personnela = session()->get('personnela_ppds');
         $data = tbl_personnela::join('office', '' . $session_personnela . '.officecd', '=', 'office.officecd')
                ->join('subdivision', 'office.subdivisioncd', '=', 'subdivision.subdivisioncd')
                ->distinct()
                ->select('subdivision.subdivision','subdivision.subdivisioncd')->get();
    
            $data_new = array();
            $i=0;
            if(count($data)>0)
            {
              foreach ($data as $arr) {             
                  $nestData[$i]=$arr->subdivision;
                  $nestData1[$i]=$arr->subdivisioncd;
                  $data_new[0]=$nestData;
                  $data_new[1]=$nestData1;
                  $i++;        
              }
            }else{
                $data_new[0]='';
                $data_new[1]='';
            }
        return $data_new;
    }

    public static function get_subdivision_wise_report($sub) {
        if($sub!=""){
           $block_count = count($sub);
        }else{
           $block_count = 0; 
        }
        $arr_select = array("poststat");

        for ($i = 0; $i < $block_count; $i++) {
            $select = DB::raw("Count(case when subdivision ='$sub[$i]' then poststat end) as 'C" . $i . "'");
            array_push($arr_select, $select);
        }
        $session_personnela = session()->get('personnela_ppds');
        $data = tbl_personnela::join('office', '' . $session_personnela . '.officecd', '=', 'office.officecd')->join('subdivision', 'office.subdivisioncd', '=', 'subdivision.subdivisioncd')
                        ->select($arr_select)->groupBy('poststat')->get();
        if (count($data) != 0) {
            foreach ($data as $arr) {
                $new['name'] = $arr->poststat;
                $data_new = array();
                for ($j = 0; $j < $block_count; $j++) {
                    $k = 'C' . $j;
                    array_push($data_new, $arr->$k);
                }
                //array_push($data_new,array_sum($data_new));
                $new['data'] = $data_new;
                $data_final[] = $new;
            }
        } else {
            $data_final[] = "";
        }
        return $data_final;
    }

    public static function get_pstat_wise_report_pie($sub) {
       // print_r($sub);die;
      
        $pstat_count = count($sub);

        $data_new = array();
        for ($i = 0; $i < $pstat_count; $i++) {
            $data = tbl_personnela::where('poststat', $sub[$i])->get();
            array_push($data_new, count($data));
        }
        return $data_new;
    }

}
