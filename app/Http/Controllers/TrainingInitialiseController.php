<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\tbl_training_type;
use App\tbl_first_rand_table;
use App\tbl_first_training_schedule;
use App\tbl_first_training_venue;
use App\tbl_personnela;
use DB;


class TrainingInitialiseController extends Controller
{

    private $firstTrainingAllocation;

    public function getTrData(FirstTrainingAllocationController $firstTrainingAllocation)
    {
        $this->firstTrainingAllocation = $firstTrainingAllocation;
        $training_type = $this->firstTrainingAllocation->getTrainingGroupData();
        return view('firstTrainingInitialize', compact('training_type'));
    }

    public function firstTrInitialize(Request $request)
    {
        // echo 'hi';die;
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            $validate_array = [
                'zone' => 'required|alpha_num|min:4|max:4',
                'trainingtype' => 'required|digits_between:1,1',
                'phase' => 'required|digits_between:1,1'
            ];

            $validate_array1 = [
                'zone.required' => 'Zone is required',
                'phase.required' => 'Phase is required',
                'zone.alpha_num' => 'Zone must be an alpha numeric characters',

                'trainingtype.required' => 'Training Head is required',
                'trainingtype.digits_between' => 'Training Head must be numeric'
            ];

            $this->validate($request, $validate_array, $validate_array1);
            try {
                $trainingHead = $request->trainingtype;
                $zone = $request->zone;
                $phase = $request->phase;
                // $totalInitializePP=0;
                $totalTrainingTypeWisePP = tbl_personnela::where('selected', '=', 1)
                    ->where('forzone', '=', $zone)
                    ->where('training_gr_cd', '=', $trainingHead)
                    ->where('phase', '=',  $phase)

                    ->where(function ($query) {
                        $query  ->whereNotNull('1stTrainingSch')
                        ->orwhereNull('1stTrainingSch');
                    })
                    ->where(function ($query) {
                        $query  ->whereNotNull('1stTrainingSch_2')
                        ->orwhereNull('1stTrainingSch_2');
                    })
                    ->where(function ($query) {
                        $query  ->whereNotNull('training_gr_cd')
                        ->orwhereNull('training_gr_cd');
                    })             
                    
                //     ->whereNotNull('training_gr_cd')
                //    ->whereNotNull('1stTrainingSch')
                //     ->orwhereNotNull('1stTrainingSch_2s')
                    ->value(DB::raw('count(personcd)'));
                // echo $totalTrainingTypeWisePP;die;
                if ($totalTrainingTypeWisePP > 0) {
                    $getScheduleCodes= tbl_personnela::where('selected', '=', 1)
                    ->where('forzone', '=', $zone)
                    ->where('phase', '=',  $phase)
                    ->where('training_gr_cd', '=', $trainingHead)
                    ->select('1stTrainingSch','1stTrainingSch_2')
                    ->groupBy('1stTrainingSch')
                    ->groupBy('1stTrainingSch_2')
                    ->get();
                    $scheduleFirstArr=array();
                    $scheduleSecondArr=array();
                    $venueFirstArr=array();
                    $venueSecondArr=array();
                    foreach($getScheduleCodes as $val){
                        array_push($scheduleFirstArr,$val['1stTrainingSch']);
                        array_push($scheduleSecondArr,$val['1stTrainingSch_2']);
                    }
                    $venueFirstCode=tbl_first_training_schedule::whereIn('schedule_code',$scheduleFirstArr)->select('tr_venue_cd')->groupBy('tr_venue_cd')->get();
                    $venuSecondCode=tbl_first_training_schedule::whereIn('schedule_code',$scheduleSecondArr)->select('tr_venue_cd')->groupBy('tr_venue_cd')->get();
                    foreach($venueFirstCode as $venueFirstCode){
                        array_push($venueFirstArr,$venueFirstCode['tr_venue_cd']);
                        
                    }
                    foreach($venuSecondCode as $venuSecondCode){
                      
                        array_push($venueSecondArr,$venuSecondCode['tr_venue_cd']);
                    }
                  // echo "<pre>"; print_r($venueFirstArr); echo "<br>";
                  // echo "<pre>"; print_r($venueSecondArr); die;
                    $venueFirstUpdate = DB::table('first_training_venue')->whereIn('venue_cd',$venueFirstArr)->update(['status' => 1]);
                    $venueSecondUpdate = DB::table('first_training_venue')->whereIn('venue_cd',$venueSecondArr)->update(['status' => 1]);

                    $personnelaUpdate = tbl_personnela::where('selected', '=', 1)
                        ->where('forzone', '=', $zone)
                        ->where('phase', '=',  $phase)
                        ->where('training_gr_cd', '=', $trainingHead)
                       // ->whereNotNull('1stTrainingSch')
                       // ->orwhereNotNull('1stTrainingSch_2')
                    //    ->where(function ($query) {
                    //     $query  ->whereNotNull('1stTrainingSch')
                    //     ->orwhereNotNull('1stTrainingSch_2');
                    // })
                    ->where(function ($query) {
                        $query  ->whereNotNull('1stTrainingSch')
                        ->orwhereNull('1stTrainingSch');
                    })
                    ->where(function ($query) {
                        $query  ->whereNotNull('1stTrainingSch_2')
                        ->orwhereNull('1stTrainingSch_2');
                    })
                    ->where(function ($query) {
                        $query  ->whereNotNull('training_gr_cd')
                        ->orwhereNull('training_gr_cd');
                    })      
                        ->update(['training_gr_cd' => null, '1stTrainingSch' => null, '1stTrainingSch_2' => null]);
                    //print_r($personnelaUpdate);die;
                    if (!empty($personnelaUpdate)) {
                        $firstRandTableDelete = tbl_first_rand_table::where('traininghead', '=', $trainingHead)
                            ->where('forzone', '=', $zone)
                            ->where('phase', '=',  $phase)
                            ->delete();
                    }
                } else {
                    $personnelaUpdate = 0;
                    $totalTrainingTypeWisePP = 0;
                }
               
                $response = array(
                    'totalInitializePP' => $personnelaUpdate,

                    'status' => 1
                );
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }

    public function check_for_training_initialise_password(Request $request)
    {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            $this->validate($request, [
                'password' => 'required|regex:/^[a-zA-Z0-9!$#%_@]+$/'
            ], [
                'password.required' => 'Password is required'
            ]);

            try {
                $password = $request->password;
                if ($password == "B%E9@cL") {
                    $count = 1;
                } else {
                    $count = 0;
                }

                $response = array(
                    'options' => $count,
                    'status' => 1
                );
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }
}
