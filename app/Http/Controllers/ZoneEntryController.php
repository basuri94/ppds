<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\tbl_zone;
use App\tbl_assembly_zone;
use DB;

class ZoneEntryController extends Controller
{
     public function getZoneRecords(Request $request){
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        }
        else
        {
            $this->validate($request, [            
            'forDist' => 'required|alpha_num|min:2|max:2'
            ], [
            
            'forDist.required' => 'District is required',
            'forDist.alpha_num' => 'District must be an alpha numeric'
            ]);
            try
            {
              $forDist=$request->forDist;
              $forzone = tbl_zone::where('districtcd','=', $forDist)
                    ->select('zonename', 'zone')
                    ->orderBy('zone')->get();
              $zoneRD="";
              $zoAr=json_decode($forzone);
              $zoneRD.="<table class='table table-bordered table-striped1' width='100%' style='border-top: 2px solid #009fe8;'>";
              $zoneRD.="<tr style='background-color: #f5f8fa'>";
              $zoneRD.="<th colspan='4'>List of Zone";
              $zoneRD.="</th>";
              $zoneRD.="</tr>";
              $zoneRD.="<tr style='background-color: #f5f8fa'>";
              $zoneRD.="<td width='10%'><b>SL#</b></td><td><b>Zone</b></td><td><b>Name of Zone </b></td><td width='10%'><b>Action</b></td>";
              $zoneRD.="</tr>";
              $count=0;
               foreach($zoAr as $fz){
                   $count++;
                    $zoneRD.="<tr><td>". $count ."</td>";
                    $zoneRD.="<td>".$fz->zone ."</td>";
                    $zoneRD.="<td>".$fz->zonename ."</td>";
                    $zoneRD.="<td><a title='Edit'  onclick='edit_zone(".json_encode($fz->zone) .");'><i class='fa fa-pencil-alt' style='color:green;cursor:pointer;' value=".json_encode($fz->zone)."></i></a>&nbsp;&nbsp;";
                    $zoneRD.="<a title='Delete'  onclick='delete_zone(".json_encode($fz->zone) .");'><i class='fa fa-trash-alt' style='color:red;cursor:pointer;' value=".json_encode($fz->zone)."></i></td></tr>"; 
                } 
             
              $zoneRD.="</table>";
              $response = array(
                   'options' =>  $zoneRD,
                   'status' => 1);          
            }catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
              $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
      
        }
    }
     public function su_zone_entry(Request $request){
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        }
        else
        {
            $this->validate($request, [            
            'districtcd' => 'required|alpha_num|min:2|max:2',
            'zone' => 'required|regex:/^[A-Za-z0-9\s]+$/i|max:50'    
            ], [
            
            'districtcd.required' => 'District is required',
            'districtcd.alpha_num' => 'District must be an alpha numeric',
            'zone.required' => 'Zone is required',
            'zone.regex' => 'Zone must be an alpha numeric'
            ]);
            try
            {
                $forDist=$request->districtcd;
                $zonename=$request->zone;
                $user_code=session()->get("code_ppds");
                $tbl_zone = new tbl_zone();
                $max_zone_code=$tbl_zone->where('districtcd','=', $forDist)
                        ->select(DB::raw('max(zone) as cnt'))->get();
                $max_code=json_decode($max_zone_code);
                $short_code="A";
                if($max_code[0]->cnt=="")
                {
                    $zonecode=$forDist.$short_code."1";
                }
                else
                {
                    $tmp_code=10+substr($max_code[0]->cnt,-1)+1;
                    $zonecode=$forDist.$short_code.substr($tmp_code,-1);
                }	
                $save_zone = new tbl_zone();
                $save_zone->districtcd = $forDist;
                $save_zone->zone = $zonecode;
                $save_zone->zonename = $zonename;
                $save_zone->usercode = $user_code;
                $save_zone->save();
                $response = array(
                   'options' => $save_zone,
                   'status' => 1);          
            }catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
              $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
      
        }
    }
     public function zone_edit(Request $request) {
       $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        }
        else
        {
            $this->validate($request, [            
            'zone' => 'required|alpha_num|min:4|max:4'    
            ], [
            
            'zone.required' => 'Zone is required',
            'zone.alpha_num' => 'Zone must be an alpha numeric'
            ]);
            try
            {
              $zone=$request->zone;
              $zone_show = tbl_zone::where('zone', '=', $zone)->get();
              $response = array(
                   'options' => $zone_show,
                   'status' => 1);
              
             }catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
              $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
     }
     public function update_zone(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        }
        else
        {
            $this->validate($request, [            
            'districtcd' => 'required|alpha_num|min:2|max:2',
            'zone' => 'required|regex:/^[A-Za-z0-9\s]+$/i|max:50' ,
            'edit_zone' => 'required|alpha_num|min:4|max:4'  
            ], [
            
            'districtcd.required' => 'District is required',
            'districtcd.alpha_num' => 'District must be an alpha numeric',
            'zone.required' => 'Zone is required',
            'zone.regex' => 'Zone must be an alpha numeric',
            'edit_zone.required' => 'Zonecode is required',
            'edit_zone.alpha_num' => 'Zonecode must be an alpha numeric'
            ]);
            try
            {
                $forDist=$request->districtcd;
                $zonename=$request->zone;
                $zonecode=$request->edit_zone;
                $user_code=session()->get("code_ppds");
                $tbl_zone = new tbl_zone();
             
                $upadtez = tbl_zone::where('zone', '=', $zonecode)
                    ->update(['zonename' => $zonename, 'usercode' => $user_code]);

              $response = array(
                   'options' => $upadtez,
                   'status' => 2);
              
             }catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
              $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }
     public function zone_delete(Request $request) {
        
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        }
        else
        {
           $this->validate($request, [            
            'zone' => 'required|alpha_num|min:4|max:4'    
            ], [
            
            'zone.required' => 'Zone is required',
            'zone.alpha_num' => 'Zone must be an alpha numeric'
            ]);
           try 
           {
            $zone=$request->zone;
            $zone_array=tbl_assembly_zone::where('zone','=', $zone)
                        ->select(DB::raw('count(zone) as cnt'))->get();
             $zoned=json_decode($zone_array);
             if($zoned[0]->cnt==0){
               $tblR = tbl_zone::where('zone', '=', $zone)
                             ->delete();
               $response = array(
                   'options' => $tblR,
                   'status' => 1);
               
             }else{
                 $response = array(
                   'options' => "Zone exist in assembly_zone table",
                   'status' => 2);
             }
            
             
              
             }catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
              $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }
}
