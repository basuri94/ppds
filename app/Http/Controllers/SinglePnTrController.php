<?php

namespace App\Http\Controllers;

use App\tbl_first_training_schedule;
use App\tbl_personnela;
use App\tbl_subdivision;
use App\tbl_training_type;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SinglePnTrController extends Controller
{
    public function single_person_training_assign(){
        $new_c=new FirstTrainingAllocationController();
        $training_type = $new_c->getTrainingGroupData();
        return view('single_person_training_assign',compact('training_type'));
    }


    public function getDistrictWiseSubdivision(Request $request)
    {
        
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            // $this->validate($request, [
            //     'zone' => 'required|alpha_num|min:4|max:4'
            //         ], [
            //     'zone.required' => 'Zone is required',
            //     'zone.alpha_num' => 'Zone must be an alpha numeric characters'
            // ]);
            // $zone = $request->zone;
            $forDist = $request->forDist;
            
            try {
                $filtered = "";
              
                $tbl_subdivision = new tbl_subdivision();
                $filtered = $tbl_subdivision
                                ->leftjoin('zone', 'zone.districtcd', '=', 'subdivision.districtcd')
                                // ->where('zone.zone', '=', $zone)
                                ->where('subdivision.districtcd', '=', $forDist)
                                ->groupBy('subdivision.subdivisioncd')
                                ->pluck('subdivision.subdivision', 'subdivision.subdivisioncd')->all();
                $response = array(
                    'options' => $filtered,
                    'status' => 1
                );
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }

    public function getSinglePersonnelDetails(Request $request)
    {
    //    dd($request->all());
    $statusCode = 200;
    if (!$request->ajax()) {
        $statusCode = 400;
        $response = array('error' => 'Error occured in Ajax Call.');
        return response()->json($response, $statusCode);
    }
    else
    {
       $this->validate($request, [            
        'personnel_id' => 'required|alpha_num|max:11|min:11',
        ], [
        
        'personnel_id.required' => 'Personnel ID is required',
        'personnel_id.alpha_num' => 'Personnel ID must be an alpha numeric',
       
        ]);
        $personnel_id=$request->personnel_id;
      
        try
        {
            $filtered=""; 
            $resMessage="";
            $tbl_personnela = new tbl_personnela();             
            $session_personnela=session()->get('personnela_ppds');
            $filtered = $tbl_personnela
                       ->join('office',''.$session_personnela.'.officecd','=','office.officecd')
                       ->join('personnel',''.$session_personnela.'.personcd','=','personnel.personcd')
                       ->join('poststat',''.$session_personnela.'.poststat','=','poststat.post_stat')
                       ->leftjoin('policestation','office.policestn_cd','=','policestation.policestationcd')
                       ->join('district','personnel.districtcd','=','district.districtcd')
                       ->join('subdivision','office.subdivisioncd','=','subdivision.subdivisioncd')
                       ->leftjoin('assembly as ass_pre','ass_pre.assemblycd','=','personnel.assembly_temp') 
                        ->leftjoin('assembly as ass_ofc','ass_ofc.assemblycd','=','personnel.assembly_off')  
                        ->leftjoin('assembly as ass_per','ass_per.assemblycd','=','personnel.assembly_perm')  
                        ->where(''.$session_personnela.'.selected','=',1)
                        ->where(''.$session_personnela.'.personcd','=', $personnel_id)  
                       
                        ->select(DB::raw("DATE_FORMAT(personnel.dateofbirth,'%d/%m/%Y') as dateofbirth"),''.$session_personnela.'.personcd','district.district','address1','address2','postoffice','pin'
                                ,'policestation.policestation','subdivision.subdivision','personnel.officecd','personnel.officer_name','personnel.off_desg','personnel.present_addr1','personnel.present_addr2'
                                ,'personnel.assembly_temp as pre_ass_cd','ass_pre.assemblyname AS pre_ass', 'personnel.assembly_perm as per_ass_cd','ass_per.assemblyname AS per_ass', 'personnel.assembly_off as post_ass_cd','ass_ofc.assemblyname AS post_ass'
                                ,'training_gr_cd','poststat.poststatus',''.$session_personnela.'.poststat',''.$session_personnela.'.booked','personnel.gender','personnel.epic','forassembly','forzone','phase','groupid','personnel.subdivisioncd','office.office','1stTrainingSch','1stTrainingSch_2',''.$session_personnela.'.dcrccd','2ndTrainingSch',''.$session_personnela.'.no_of_member')->get();
       
            if(count($filtered)>0){
               foreach($filtered as $rwfilter){
                   if($rwfilter->booked=="C" || $rwfilter->booked==""){
                      $resMessage="Not Available for Selected Operation";
                      $status = 0;
                   }else{
                       $fst='1stTrainingSch';
                       $fstSec='1stTrainingSch_2';
                       $sst='2ndTrainingSch';
                        $resMessage.="<table width='100%' style='font-family: verdana,arial,sans-serif;font-size: 11px;color: #000;'>\n";
                       // $resMessage.="<tr><td align='left'><input type='text'  value='".$rwfilter->assembly_temp."' name='assembly_temp'></td><td align='left' colspan='3'><input type='text'  value='".$rwfilter->assembly_perm."' name='assembly_perm'></td></tr>\n";
                        $resMessage.="<tr><td align='left'>Name: </td><td align='left' colspan='3'>".$rwfilter->officer_name."</td></tr>\n";
                        $resMessage.="<tr><td align='left'>Office ID: </td><td align='left' colspan='3'>".$rwfilter->officecd."</td></tr>\n";
                        $resMessage.="<tr><td align='left'>Designation: </td><td align='left' colspan='3'>".$rwfilter->off_desg."</td></tr>\n";
                        
                        $resMessage.="<tr><td align='left'>Office Name: </td><td align='left' colspan='3'>".$rwfilter->office."</td></tr>\n";
                        
                        
                        $resMessage.="<tr><td align='left'>Office Address: </td><td align='left' colspan='3'>".$rwfilter->address1.",".$rwfilter->address2.", PO-".$rwfilter->postoffice.", PS-".$rwfilter->policestation.", Subdiv-".$rwfilter->subdivision.", Dist.-".$rwfilter->district.",".$rwfilter->pin."</td></tr>\n";
                        $resMessage.="<tr><td align='left'>Date of Birth: </td><td align='left'>".$rwfilter->dateofbirth."</td><td align='left'>Gender: </td><td align='left'>".$rwfilter->gender."<input type='hidden' id='gender' name='gender' value='".$rwfilter->gender."'></td></tr>\n";
                        // $resMessage.="<tr><td align='left'>EPIC No: </td><td align='left'>".$rwfilter->epic."</td><td align='left'>Posting Status: </td><td align='left'>".$rwfilter->poststatus."<input type='hidden' id='post_stat' name='post_stat' value='".$rwfilter->poststat."'></td></tr>\n";
                            $resMessage.="<tr><td align='left'>EPIC No: </td><td align='left'>".$rwfilter->epic."</td><td align='left'>Posting Status: </td><td align='left'>".$rwfilter->poststatus."<hidden id='hid_post_stat' name='hid_post_stat' style='display:none;'>".$rwfilter->poststat."</hidden></td></tr>\n";
                        
                        
                        
                        
                        $resMessage.="<tr><td align='left'>Present Address: </td><td align='left' colspan='3'>".$rwfilter->present_addr1.", ".$rwfilter->present_addr2."</td></tr>\n";
                        $resMessage.="<tr><td align='left' colspan='4'><b>Assembly of</b></td></tr>\n";
                        $resMessage.="<tr><td align='left' colspan='2'>Present Address: </td><td align='left' colspan='2'>".$rwfilter->pre_ass."<input type='hidden' id='hid_pre_ass' name='hid_pre_ass' value='".$rwfilter->pre_ass_cd."'></td></tr>\n";
                        $resMessage.="<tr><td align='left' colspan='2'>Permanent Address: </td><td align='left' colspan='2'>".$rwfilter->per_ass."<input type='hidden' id='hid_per_ass' name='hid_per_ass' value='".$rwfilter->per_ass_cd."'></td></tr>\n";
                        $resMessage.="<tr><td align='left' colspan='2'>Place of Posting: </td><td align='left' colspan='2'>".$rwfilter->post_ass."<input type='hidden' id='hid_post_ass' name='hid_post_ass' value='".$rwfilter->post_ass_cd."'></td></tr>\n";
                        $resMessage.="<tr><td align='left' colspan='4'><input type='hidden' id='forassembly' name='forassembly'  value='".$rwfilter->forassembly."'>\n<input type='hidden' id='groupid' name='groupid' value='".$rwfilter->groupid."'>\n<input type='hidden' id='training_gr_cd' name='training_gr_cd' value='".$rwfilter->training_gr_cd."'>\n<input type='hidden' id='booked' name='booked' value='".$rwfilter->booked."'>\n <input type='hidden' id='no_of_member' name='no_of_member' value='".$rwfilter->no_of_member."'>\n <input type='hidden' id='per_cd' name='per_cd' value='".$rwfilter->personcd."'>\n <input type='hidden' id='zone' name='zone' value='".$rwfilter->forzone."'>\n <input type='hidden' id='dcrccd' name='dcrccd' value='".$rwfilter->dcrccd."'>\n <input type='hidden' id='sub_div' name='sub_div' value='".$rwfilter->subdivisioncd."'>\n <input type='hidden' id='ofc_cd' name='ofc_cd' value='".$rwfilter->officecd."'>\n <input type='hidden' id='training1_sch' name='training1_sch' value='".$rwfilter->$fst."'>\n <input type='hidden' id='training1_sch_sec' name='training1_sch_sec' value='".$rwfilter->$fstSec."'> <input type='hidden' id='training2_sch' name='training2_sch' value='".$rwfilter->$sst."'><input type='hidden' id='phase' name='phase' value='".$rwfilter->phase."'></td></tr>\n";

                        $resMessage.="<tr><td align='right' colspan='2'>Booked : </td><td colspan='2' align='left' id='o_booked'>Yes</td></tr>\n";
                        $resMessage.="</table>";
                        $status = 1;
                   }
               }                    
            }else{
               $resMessage="No records found"; 
               $status = 0;
            }

            $response = array(
               'options' => $resMessage,
               'status' => $status
            );
        }catch (\Exception $e) {
            $response = array(
                'exception' => true,
                'exception_message' => $e->getMessage(),
            );
          $statusCode = 400;
        } finally {
            return response()->json($response, $statusCode);
        }
    }
    }



    public function singlePPTrDtAllocation(Request $request)
    {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        }
        else
        {
          
            
            try
            {
            //    dd($request->all());
            //     $zone=$request->zone;
            //     $forassembly=$request->forassembly;
            //     $ofc_id=$request->ofc_cd;
            //     $booked=$request->booked;
            //     $post_stat=$request->post_stat;
            //     $gender=$request->gender;
            //     $phase=$request->phase;
            //     $oldpersonnel_id=$request->personnel_id;
            //     // $newpersonnel_id=$request->newpersonnel_id;
            //     $reason=$request->reason;
            //     $chkSameVenueTraining=$request->chkSameVenueTraining;
            //    // echo  $chkSameVenueTraining;die;
            //     $oldtraining_sch=$request->training1_sch;
            //     $oldtraining_sch_sec=$request->training1_sch_sec;
            //     $training_gr_cd=$request->training_gr_cd;
            //     $orderno=$request->orderno;
            //     $trainingdatetime = $request->trainingdatetime;
                // dd($trainingdatetime);
                $zone=$request->zone;
                $phase=$request->phase;
                $training_gr_cd = $request->training_gr_cd;
                $trainingdatetime = $request->trainingdatetime;
                $subdivision = $request->subdivision;

                
               

                if(!empty($request->v_sub)){
                    $training_sch_1 = $request->v_sub;
                } else{
                    $training_sch_1 = null;
                }

                if(!empty($request->v_sub2)){
                    $training_sch_2 = $request->v_sub2;
                } else{
                    $training_sch_2 = null;
                }
                $orderno = $request->orderno;
                $personnel_id = $request->personnel_id;
                $training_desc1="";
                $training_desc2="";
                    if(!empty($training_sch_1)){
                        $training_desc1= tbl_training_type::where('training_group_cd',$training_gr_cd)
                        ->where(function ($query) {
                            $query->where('training_code', '01')
                                  ->orWhere('training_code', '03')
                                  ->orWhere('training_code', '05');
                        })->value('training_desc');

                    }


                    if(!empty( $training_sch_2 )){
                        $training_desc2= tbl_training_type::where('training_group_cd',$training_gr_cd)
                        ->where(function ($query) {
                            $query->where('training_code', '02')
                                  ->orWhere('training_code', '04')
                                  ->orWhere('training_code', '06');
                        })->value('training_desc');

                       
                    }
                $filtered=tbl_first_training_schedule::leftjoin('first_training_venue','first_training_venue.venue_cd','=','first_training_schedule.tr_venue_cd')
                                ->leftjoin('first_training_date_time','first_training_date_time.datetime_cd','=','first_training_schedule.datetimecd')
                                ->leftjoin('first_training_subvenue','first_training_subvenue.subvenue_cd','=','first_training_schedule.tr_subvenue_cd')
                                ->leftjoin('training_type','training_type.training_code','=','first_training_schedule.training_type')
                                ->where('training_type.training_group_cd',$training_gr_cd)
                                ->where('first_training_schedule.subdivisioncd',$subdivision)
                                ->where('first_training_schedule.schedule_code',$training_sch_1)

                                // ->where('first_training_venue.status','1')
                                // ->groupBy('first_training_schedule.training_type','first_training_schedule.schedule_code','first_training_venue.venuename','first_training_subvenue.subvenue','first_training_date_time.training_dt','first_training_date_time.training_time')
                                ->select('first_training_schedule.training_type','first_training_schedule.schedule_code','first_training_venue.venuename','first_training_subvenue.subvenue','first_training_venue.venueaddress',DB::raw("DATE_FORMAT(first_training_date_time.training_dt,'%d/%m/%Y') as training_dt"),'first_training_date_time.training_time')
                                ->first();




                                $filtered1=tbl_first_training_schedule::leftjoin('first_training_venue','first_training_venue.venue_cd','=','first_training_schedule.tr_venue_cd')
                                ->leftjoin('first_training_date_time','first_training_date_time.datetime_cd','=','first_training_schedule.datetimecd')
                                ->leftjoin('first_training_subvenue','first_training_subvenue.subvenue_cd','=','first_training_schedule.tr_subvenue_cd')
                                ->leftjoin('training_type','training_type.training_code','=','first_training_schedule.training_type')
                                ->where('training_type.training_group_cd',$training_gr_cd)
                                ->where('first_training_schedule.subdivisioncd',$subdivision)
                                ->where('first_training_schedule.schedule_code',$training_sch_2)

                                // ->where('first_training_venue.status','1')
                                // ->groupBy('first_training_schedule.training_type','first_training_schedule.schedule_code','first_training_venue.venuename','first_training_subvenue.subvenue','first_training_date_time.training_dt','first_training_date_time.training_time')
                                ->select('first_training_schedule.training_type','first_training_schedule.schedule_code','first_training_venue.venuename','first_training_subvenue.subvenue','first_training_venue.venueaddress',DB::raw("DATE_FORMAT(first_training_date_time.training_dt,'%d/%m/%Y') as training_dt"),'first_training_date_time.training_time')
                                ->first();
               
                $updatePersoneel2 = DB::table('personnela'.session()->get('districtcd_ppds'))->where('personcd', $personnel_id)
                            ->update([
                                '1stTrainingSch'=>$training_sch_1,
                                '1stTrainingSch_2'=>$training_sch_2,
                                'training_gr_cd'=>$training_gr_cd
                                ]);


                                

                                if(!empty( $training_sch_1 )){
                                    $updateFirstRandTbl1 = DB::table('first_rand_table')->where('personcd', $personnel_id)->update([
                                         'schedule_code'=>$training_sch_1,
                                         'training_desc'=>$training_desc1,
                                         'venuename'=>$filtered->venuename,
                                         'sub_venuename'=>$filtered->subvenue,
                                         'venueaddress'=>$filtered->venueaddress,
                                         'training_dt'=>$filtered->training_dt,
                                         'training_time'=>$filtered->training_time
                                         ]);

                                    }
                                    $updateFirstRandTbl2 = DB::table('first_rand_table')->where('personcd', $personnel_id);
                                if(!empty( $training_sch_2 )){
                                    
                                    
                                    $updateFirstRandTbl2= $updateFirstRandTbl2->update([
                                                    'schedule_code_2'=>$training_sch_2,
                                                    'training_desc_2'=>$training_desc2,
                                                    'venuename_2'=>$filtered1->venuename,
                                                    'sub_venuename_2'=>$filtered1->subvenue,
                                                    'venueaddress_2'=>$filtered1->venueaddress,
                                                    'training_dt_2'=>$filtered1->training_dt,
                                                    'training_time_2'=>$filtered1->training_time
                                                        ]);

                                                   }
                                                   else{
                                                    $updateFirstRandTbl2=$updateFirstRandTbl2->update([
                                                        'schedule_code_2'=>null,
                                                        'training_desc_2'=>null,
                                                        'venuename_2'=>null,
                                                        'sub_venuename_2'=>null,
                                                        'venueaddress_2'=>null,
                                                        'training_dt_2'=>null,
                                                        'training_time_2'=>null
                                                            ]);
    
                                                       }
                                               
                                                       $person_first_rand_tbl = DB::table('first_rand_table')->where('personcd',$personnel_id)
                                                       ->select('training_desc','venuename','sub_venuename','venueaddress','training_dt','training_time')->get();

                $response = array(
                    'updatePersoneel2' =>$updatePersoneel2,
                    //'updateFirstRandTbl' =>$updateFirstRandTbl,
                    // 'updatePersoneel2' =>$updatePersoneel2,
                    'person_first_rand_tbl'=>$person_first_rand_tbl,
                    'status' => 1
                );

               
            }catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
              $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }

}
