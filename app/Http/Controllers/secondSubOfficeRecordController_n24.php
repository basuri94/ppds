<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\tbl_personnela;
use App\tbl_office;
use DB;

class secondSubOfficeRecordController extends Controller {

    public function getOfficeNameUserwiseForSecondRandomization(Request $request) {

        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {

            $this->validate($request, [
                'forDist' => 'required|alpha_num|min:2|max:2',
                'subdivision' => 'required|alpha_num|min:4|max:4',
                'blockmuni' => 'nullable|alpha_num|min:6|max:6',
                'trainingtype' => 'nullable|digits_between:1,1'
                    ], [
                'forDist.required' => 'District is required',
                'forDist.alpha_num' => 'District code must be an alpha numeric',
                'subdivision.required' => 'Subdivision is required',
                'subdivision.alpha_num' => 'Subdivision must be an alpha numeric',
                'blockmuni.alpha_num' => 'Block/Municipality must be an alpha numeric',
                'trainingtype.digits_between' => 'Training Head must be numeric',
            ]);
            try {

                $subdivision = $request->subdivision;
                $blockmuni = $request->blockmuni;
                $forDist = $request->forDist;
                $orderno = $request->orderno;

                $officeName = $request->officeName;
                $office = '';
                $blockmuni_sessioncd = session()->get('blockmunicd_ppds');
                $tbl_personnela = new tbl_personnela();

                if ($blockmuni_sessioncd != NULL || $blockmuni_sessioncd != "") {
                    $tbl_personnela = $tbl_personnela->where('office.blockormuni_cd', '=', $blockmuni_sessioncd);
                }
                if ($blockmuni != '') {
                    $tbl_personnela = $tbl_personnela->where('office.blockormuni_cd', '=', $blockmuni);
                }

                if ($officeName != '') {
                    $tbl_personnela = $tbl_personnela->where('office.officecd', '=', $officeName);
                }
                $session_personnela = session()->get('personnela_ppds');

                $office = $tbl_personnela::join('office', '' . $session_personnela . '.officecd', '=', 'office.officecd')
//                               ->join('second_rand_table', function($join)
// {
//   $join->or('second_rand_table.p1_personcd', '=', '' . $session_personnela . '.personcd')
//           ->orOn('second_rand_table.p2_personcd', '=', '' . $session_personnela . '.personcd')
//           ->orOn('second_rand_table.p3_personcd', '=', '' . $session_personnela . '.personcd')
//         ->orOn('second_rand_table.pr_personcd', '=', '' . $session_personnela . '.personcd')
//           ->orOn('second_rand_table.pa_personcd', '=', '' . $session_personnela . '.personcd')
//           ->orOn('second_rand_table.pb_personcd', '=', '' . $session_personnela . '.personcd');
//
// })
                        ->leftjoin('second_rand_table', 'second_rand_table.pers_off', '=', '' . $session_personnela . '.officecd')
                        ->where('' . $session_personnela . '.fordistrict', '=', $forDist)
                        //   ->where('' . $session_personnela . '.training_gr_cd', '=', $trainingtype)
                        ->where('selected', '=', 1)
                        ->where(function ($query) {
                    $query->where('booked', '=', "P")
                    ->orwhere('booked', '=', "R")
                    ->orwhere('booked', '=', "R");
                });

                if ($orderno != "") {
                    $office = $office->where('second_rand_table.order_no', '=', $orderno);
                }
                $office = $office->where(DB::raw("SUBSTRING(" . $session_personnela . ".personcd,1,4)"), '=', $subdivision)
                                ->groupBy('office.officecd')
                                ->pluck('office.office', 'office.officecd')->all();

                $response = array(
                    'options' => $office,
                    'status' => 1);
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }

    public function getSecondSubOffPPReport(Request $request) {
        $this->validate($request, [
            'subdivision' => 'required|alpha_num|min:4|max:4',
            'officeName' => 'nullable|alpha_num|min:10|max:10',
            'from' => 'required|digits_between:1,4',
            'to' => 'required|digits_between:1,4',
                ], [
            'subdivision.required' => 'Subdivision is required',
            'subdivision.alpha_num' => 'Subdivision must be an alpha numeric',
            'officeName.alpha_num' => 'Office Code must be an alpha numeric',
            'from.digits_between' => 'Records From must be an integer',
            'to.digits_between' => 'Records To must be an integer',
        ]);

        $subdivision = $request->subdivision;
        $officeName = $request->officeName;
        $from = $request->from;
        $to = $request->to;
        $pfrom = $from - 1;
        $pto = $to - $from + 1;
        $orderno = $request->orderno;
        $tbl_office = new tbl_office();
        $session_personnela = session()->get('personnela_ppds');

//        if ($pfrom != '-1') {
//            $tbl_office = $tbl_office->orderBy('office.blockormuni_cd')
//                   ->orderBy('office.officecd')
//                     ->orderBy('' . $session_personnela . '.personcd')
//                   
//                            ->offset($pfrom)->take($pto);
//            
//        } else {
//            $tbl_office = $tbl_office->orderBy('office.subdivisioncd', 'office.blockormuni_cd', 'office.officecd', 'DESC');
//        
//            
//        }

        $ApptRecord = $tbl_office->join('subdivision', 'subdivision.subdivisioncd', '=', 'office.subdivisioncd')
                ->join('district', 'district.districtcd', '=', 'office.districtcd')
                ->join('block_muni', 'block_muni.blockminicd', '=', 'office.blockormuni_cd')
                ->Leftjoin('policestation', 'policestation.policestationcd', '=', 'office.policestn_cd')
                ->join('' . $session_personnela . '', '' . $session_personnela . '.officecd', '=', 'office.officecd')
                ->leftjoin('second_rand_table', 'second_rand_table.pers_off', '=', '' . $session_personnela . '.officecd')
                ->where(DB::raw('SUBSTRING(' . $session_personnela . '.personcd,1,4)'), '=', $subdivision)
                ->where('' . $session_personnela . '.selected', '=', 1)
                ->groupBy('office.officecd', 'office.officer_desg', 'office.office', 'office.address1', 'office.address2', 'office.postoffice', 'subdivision.subdivision', 'policestation.policestation', 'district.district', 'office.pin', 'block_muni.blockmuni')
                //->where(''.$session_personnela.'.officecd','=',$officeName)
                //->with('ppDetailBrief') 
                // ->orderBy(DB::raw('second_rand_table.assembly,second_rand_table.party_reserve,lpad (second_rand_table.groupid,3,0)'))
                ->orderBy('second_rand_table.block_muni_cd')
                ->orderBy('second_rand_table.pers_off')
                ->orderBy('second_rand_table.party_reserve')
                ->select('office.officecd', 'office.officer_desg as designation', 'office.office', 'office.address1', 'office.address2', 'office.postoffice', 'subdivision.subdivision', 'policestation.policestation', 'district.district', 'office.pin', 'block_muni.blockmuni');
        if ($officeName != '') {
            $ApptRecord = $ApptRecord->where('' . $session_personnela . '.officecd', '=', $officeName);
        }
        if ($orderno != "") {
            $ApptRecord = $ApptRecord->where('second_rand_table.order_no', '=', $orderno);
        }
        $ApptRecord = $ApptRecord->get();
        $subV = json_decode($ApptRecord);
//echo json_encode($subV);die;
        $pdf = app('FPDF');
        $pdf->AddPage();
        $count = 0;
        $per_page = 1;
        foreach ($subV as $subTe) {
            if ($count < $per_page) {
                $fill = false;
                $office_dtl = "OFFICE: (" . $subTe->officecd . "), " . $subTe->office . ", " . $subTe->address1 . ", " . $subTe->address2;
                $office_dtl1 = " P.O. - " . $subTe->postoffice . ", Subdivision - " . $subTe->subdivision . ", Block/Muni - " . $subTe->blockmuni . ", P.S. - " . $subTe->policestation . ", Dist. - " . $subTe->district . ", PIN - " . $subTe->pin;
                $pdf->Ln();
                $pdf->SetFont('Arial', 'B', 7.3);
                $pdf->MultiCell(190, 4, $office_dtl . $office_dtl1, 'LTR', 'J');

                $pdf->SetFillColor(255, 255, 255);
                //	$this->SetTextColor(0,0,0);
                $pdf->SetDrawColor(0, 0, 0);
                $pdf->SetLineWidth(.3);
                $pdf->SetFont('Arial', 'B', 8);

                $head = array('PIN', 'Name', 'Designation', 'Posting Status');
                $w = array(25, 60, 77, 28);
                //	$this->SetFont('Arial','',9);
                for ($j = 0; $j < count($head); $j++)
                    $pdf->Cell($w[$j], 7, $head[$j], 1, 0, 'C', true);

                $pdf->Ln();

                // $subTe_array=$subTe->pp_detail_brief;
                $tbl_office1 = new tbl_office();
                $subTe_array = $tbl_office1->join('' . $session_personnela . '', '' . $session_personnela . '.officecd', '=', 'office.officecd')
                        ->join('personnel', 'personnel.personcd', '=', '' . $session_personnela . '.personcd')
                        ->join('poststat', 'poststat.post_stat', '=', '' . $session_personnela . '.poststat')

                        //  ->leftjoin('second_rand_table','second_rand_table.pers_off','=','office.officecd')
                        ->where('' . $session_personnela . '.selected', '=', 1)
                        ->where(function ($query) use ($session_personnela) {
                            $query->orwhere('' . $session_personnela . '.booked', '=', "P")
                            ->orWhere('' . $session_personnela . '.booked', '=', "R");
                        })

                        //->where('' . $session_personnela . '.training_gr_cd', '=', $trainingtype)
                        ->where('' . $session_personnela . '.officecd', '=', $subTe->officecd)
                        ->select('personnel.officecd', '' . $session_personnela . '.personcd', 'personnel.officer_name', 'personnel.off_desg as designation', 'poststat.poststatus', '' . $session_personnela . '.booked')
                        ->orderBy('' . $session_personnela . '.booked')
                        ->orderBy('' . $session_personnela . '.personcd')
                        ->get();
//              
                $subTe_array_count = count($subTe_array);
                if ($subTe_array_count > 0) {
                    foreach ($subTe_array as $stud_sub) {

                        $pdf->SetFont('', '', 6);
                        $pdf->Cell($w[0], 6, $stud_sub->personcd, 'LRT', 0, 'L', $fill);
                        $pdf->Cell($w[1] - 5, 6, $stud_sub->officer_name, 'LRT', 0, 'L', $fill);
                        $pdf->Cell(5, 6, $stud_sub->booked, 'LRT', 0, 'C', $fill);
                        //  $pdf->Cell(0, 6, $pdf->Cell($w[1]-10, 6, 'p', 'LRT', 0, 'L', $fill).$pdf->Cell($w[1]-40, 6, 'p', 'LRT', 0, 'L', $fill), 'LRT', 0, 'L', $fill);
                        $pdf->Cell($w[2], 6, $stud_sub->designation, 'LRT', 0, 'L', $fill);
                        $pdf->Cell($w[3], 6, $stud_sub->poststatus, 'LRT', 0, 'L', $fill);
                        //count1++;

                        $pdf->Ln();
                        $pdf->Cell(array_sum($w), 0, '', 1, 0, 'L', $fill);
                        $pdf->Ln();
                    }
                }
                $pdf->Ln(10);
                $pdf->SetFont('Arial', 'B', 9);
                $pdf->Cell(50, 5, "Head of the office signature  .................................", 0, 0, 'L');
                $fill = !$fill;
                $count++;
            }
            if ($count == $per_page) {
                $per_page = $per_page + 1;
                if ($count != Count($subV)) {
                    $pdf->AddPage();
                }
            }
        }
        $pdf->Output();
        exit;
    }

}
