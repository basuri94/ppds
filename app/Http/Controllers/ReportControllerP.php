<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\tbl_personnela;
use DB;

class ReportControllerP extends Controller {

    private $swapping;

    public function getQueryInit(SwappingController $swapping) {
        $this->swapping = $swapping;
        $poststat = $this->swapping->getPostStatus();
        return view('subdivisionWisePPReportAfterFirstRandomisation', compact('poststat'));
    }

    public function getSubDivisionPoststatusReport(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            $this->validate($request, [
                'zone' => 'required|alpha_num|min:4|max:4'
                    ], [
                'zone.required' => 'For Zone is required',
                'zone.alpha_num' => 'For Zone must be an alpha numeric'
            ]);

            try {
                $forZone = $request->zone;
                $subdivision=$request->subdivision;
                $session_personnela = session()->get('personnela_ppds');
                $ptable = "";
                $SubDivisionAvailable = tbl_personnela::join('subdivision', 'subdivision.subdivisioncd', '=', DB::raw("SUBSTRING(" . $session_personnela . ".personcd,1,4)"))
                        ->select('subdivision.subdivision', 'subdivision.subdivisioncd')
                        ->where('forzone', '=', $forZone)
                         ->where('selected','=',1)
                        ->groupBy('subdivision');
                
                 if ($forZone!=''){
                    $SubDivisionAvailable=$SubDivisionAvailable->where('forzone', '=', $forZone);
                }
                 if ($subdivision!=''){
                    $SubDivisionAvailable=$SubDivisionAvailable->where(DB::raw("SUBSTRING(" . $session_personnela . ".personcd,1,4)"), '=', $subdivision);
                }
$SubDivisionAvailable=$SubDivisionAvailable->get();
                $PostStatAvailable = tbl_personnela::join('subdivision', 'subdivision.subdivisioncd', '=', DB::raw("SUBSTRING(" . $session_personnela . ".personcd,1,4)"))
                                ->select('subdivision.subdivision', 'poststat', DB::raw('count(personcd) as total'))
                               
                                ->where('selected','=',1)
                                ->groupBy('subdivision')
                                ->groupBy('poststat')
                                ->orderBy('poststat')->get();

                if(count($SubDivisionAvailable)>0 && count($PostStatAvailable)>0){
                    foreach ($SubDivisionAvailable as $sy) {
                    $P1Available = tbl_personnela::join('subdivision', 'subdivision.subdivisioncd', '=', DB::raw("SUBSTRING(" . $session_personnela . ".personcd,1,4)"))->where('forzone', '=', $forZone)
                            ->where('poststat', '=', 'P1')
                            ->where('selected', '=', 1)
                            ->where(DB::raw("SUBSTRING(" . $session_personnela . ".personcd,1,4)"), '=', $sy->subdivisioncd)
                            ->value(DB::raw('count(personcd)'));

                    $P2Available = tbl_personnela::join('subdivision', 'subdivision.subdivisioncd', '=', DB::raw("SUBSTRING(" . $session_personnela . ".personcd,1,4)"))->where('forzone', '=', $forZone)
                            ->where('poststat', '=', 'P2')
                            ->where(DB::raw("SUBSTRING(" . $session_personnela . ".personcd,1,4)"), '=', $sy->subdivisioncd)
                            ->where('selected', '=', 1)
                            ->value(DB::raw('count(personcd)'));
                    $P3Available = tbl_personnela::join('subdivision', 'subdivision.subdivisioncd', '=', DB::raw("SUBSTRING(" . $session_personnela . ".personcd,1,4)"))->where('forzone', '=', $forZone)
                            ->where('poststat', '=', 'P3')
                            ->where(DB::raw("SUBSTRING(" . $session_personnela . ".personcd,1,4)"), '=', $sy->subdivisioncd)
                            ->where('selected', '=', 1)
                            ->value(DB::raw('count(personcd)'));
                    $PRAvailable = tbl_personnela::join('subdivision', 'subdivision.subdivisioncd', '=', DB::raw("SUBSTRING(" . $session_personnela . ".personcd,1,4)"))->where('forzone', '=', $forZone)
                            ->where('poststat', '=', 'PR')
                            ->where(DB::raw("SUBSTRING(" . $session_personnela . ".personcd,1,4)"), '=', $sy->subdivisioncd)
                            ->where('selected', '=', 1)
                            ->value(DB::raw('count(personcd)'));
                    $PAAvailable = tbl_personnela::join('subdivision', 'subdivision.subdivisioncd', '=', DB::raw("SUBSTRING(" . $session_personnela . ".personcd,1,4)"))->where('forzone', '=', $forZone)
                            ->where('poststat', '=', 'PA')
                            ->where(DB::raw("SUBSTRING(" . $session_personnela . ".personcd,1,4)"), '=', $sy->subdivisioncd)
                            ->where('selected', '=', 1)
                            ->value(DB::raw('count(personcd)'));
                    $PBAvailable = tbl_personnela::join('subdivision', 'subdivision.subdivisioncd', '=', DB::raw("SUBSTRING(" . $session_personnela . ".personcd,1,4)"))->where('forzone', '=', $forZone)
                            ->where('poststat', '=', 'PB')
                            ->where(DB::raw("SUBSTRING(" . $session_personnela . ".personcd,1,4)"), '=', $sy->subdivisioncd)
                            ->where('selected', '=', 1)
                            ->value(DB::raw('count(personcd)'));
                    $ptable .= "<table class='table table-bordered table-striped1' width='100%' style='border-top: 2px solid #009fe8;'> ";
                    $ptable .= "<tr style='background-color: #f5f8fa'>";
                    $ptable .= "<th colspan=" . count($PostStatAvailable) . ">Subdivision:" . $sy->subdivision . "</th>";
                    $ptable .= "</tr>";
                    $ptable .= "<tr>";

                    if ($P1Available != 0) {

                        $ptable .= "<td style='padding:0px;border-collapse: collapse;'>";
                        $ptable .= "<table width='100%'>";
                        $ptable .= "<tr><td> P1";
                        $ptable .= "</td></tr>";
                        $ptable .= "<tr><td>" . $P1Available;
                        $ptable .= "</td></tr>";
                        $ptable .= "</table>";
                        $ptable .= "</td>";
                        // echo "P1: ".$p1_stat_count.";&nbsp;";
                    }
                    if ($P2Available != 0) {
                        $ptable .= "<td style='padding:0px;border-collapse: collapse;'>";
                        $ptable .= "<table width='100%'>";
                        $ptable .= "<tr><td> P2";
                        $ptable .= "</td></tr>";
                        $ptable .= "<tr><td>" . $P2Available;
                        $ptable .= "</td></tr>";
                        $ptable .= "</table>";
                        $ptable .= "</td>";
                    }
                    if ($P3Available != 0) {
                        $ptable .= "<td style='padding:0px;border-collapse: collapse;'>";
                        $ptable .= "<table width='100%'>";
                        $ptable .= "<tr><td> P3";
                        $ptable .= "</td></tr>";
                        $ptable .= "<tr><td>" . $P3Available;
                        $ptable .= "</td></tr>";
                        $ptable .= "</table>";
                        $ptable .= "</td>";
                    }
                    if ($PAAvailable != 0) {
                        $ptable .= "<td style='padding:0px;border-collapse: collapse;'>";
                        $ptable .= "<table width='100%' >";
                        $ptable .= "<tr><td> PA";
                        $ptable .= "</td></tr>";
                        $ptable .= "<tr><td>" . $PAAvailable;
                        $ptable .= "</td></tr>";
                        $ptable .= "</table>";
                        $ptable .= "</td>";
                    }
                    if ($PBAvailable != 0) {
                        $ptable .= "<td style='padding:0px;border-collapse: collapse;'>";
                        $ptable .= "<table width='100%'>";
                        $ptable .= "<tr><td> PB";
                        $ptable .= "</td></tr>";
                        $ptable .= "<tr><td>" . $PBAvailable;
                        $ptable .= "</td></tr>";
                        $ptable .= "</table>";
                        $ptable .= "</td>";
                    }
                    if ($PRAvailable != 0) {
                        $ptable .= "<td style='padding:0px;border-collapse: collapse;'>";
                        $ptable .= "<table width='100%'>";
                        $ptable .= "<tr><td> PR";
                        $ptable .= "</td></tr>";
                        $ptable .= "<tr><td>" . $PRAvailable;
                        $ptable .= "</td></tr>";
                        $ptable .= "</table>";
                        $ptable .= "</td>";
                    }
                    $ptable .= "<tr>";
                    $ptable .= "</table>";
                }


                $response = array(
                    'options' => $ptable,
                    'status' => 1
                );
                }
                else{
                     $response = array(
                  
                    'status' => 2
                );
                }


                
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }
    
    public function getSubDivisionWisePPExcel(Request $request){
          $this->validate($request, [ 
            'subdivision' => 'nullable|alpha_num|min:4|max:4',
            'zone' => 'required|alpha_num|min:4|max:4',
            ], [
            
            'subdivision.alpha_num' => 'Subdivision must be an alpha numeric',
            'zone.required' => 'Zone is required',
            'zone.alpha_num' => 'Zone must be an alpha numeric characters',
               
           ]);
 $subdivision=$request->subdivision;
        $zone=$request->zone;
   
     
        $data = array();
        $session_personnela=session()->get('personnela_ppds');
        $tbl_personnela = new tbl_personnela();
       
       
        // $records = $tbl_personnela->join('office', '' . $session_personnela . '.officecd', '=', 'office.officecd')
        //                 ->join('subdivision', 'office.subdivisioncd', '=', 'subdivision.subdivisioncd')
        //                 ->join('block_muni', 'office.blockormuni_cd', '=', 'block_muni.blockminicd')
        //                 ->join('zone', '' . $session_personnela . '.forzone', '=', 'zone.zone')
        //                  ->join('personnel', 'personnel.personcd', '=', '' . $session_personnela . '.personcd')
        //         ->where(''.$session_personnela.'.selected','=',1)
        //               //  ->groupBy('block_muni.blockminicd')
        //                 ->select('zone.zonename','subdivision.subdivision','subdivision.subdivisioncd','block_muni.blockmuni','block_muni.blockminicd','office.office',''.$session_personnela.'.poststat','personnel.officer_name');
        
        $records = $tbl_personnela->join('office', '' . $session_personnela . '.officecd', '=', 'office.officecd')
                            ->join('subdivision', 'office.subdivisioncd', '=', 'subdivision.subdivisioncd')
                            ->join('block_muni', 'office.blockormuni_cd', '=', 'block_muni.blockminicd')
                            ->join('zone', '' . $session_personnela . '.forzone', '=', 'zone.zone')
                            ->join('personnel', 'personnel.personcd', '=', '' . $session_personnela . '.personcd')
                            ->where(''.$session_personnela.'.selected','=',1)
                            ->orderBy('block_muni.blockmuni','asc')
                            ->select('zone.zonename','subdivision.subdivision','subdivision.subdivisioncd','block_muni.blockmuni','block_muni.blockminicd','office.office',''.$session_personnela.'.poststat','personnel.officer_name');
        
        
        
        if($zone!="")
        {
           $tbl_personnela = $tbl_personnela->where("".$session_personnela.'.forzone','=', $zone);
        } 
        if($subdivision!="")
        {
           $records = $records->where('office.subdivisioncd','=',$subdivision);
        } 
        $records=$records->get();
       
        foreach($records as $recordspp){
            $nestedData['Officer Name']=$recordspp->officer_name;
             $nestedData['Office Name']=$recordspp->office;
              $nestedData['Subdivision']=$recordspp->subdivision;
            $nestedData['Block/Municipality']=$recordspp->blockmuni;
            $nestedData['Post Status']=$recordspp->poststat;
            $nestedData['Zone']=$recordspp->zonename;
          
            $data[]=$nestedData;
           
        }
       
       date_default_timezone_set('Asia/Calcutta'); 
        $dt = date_create('now')->format('YmdHi');
        $reportName='SubBlockReport'.$dt;
        return \Excel::create($reportName, function($excel) use ($data) {
                    $excel->sheet(' ', function($sheet) use ($data) {
                        $sheet->fromArray($data);
                    });
                })->download('xlsx');
    }
    
    public function getAssemblywisereport(Request $request){
        $this->validate($request, [ 
            'assembly' => 'nullable|alpha_num',
            'forZone' => 'required|alpha_num|min:4|max:4',
            ], [
            
            'assembly.alpha_num' => 'Assembly must be an alpha numeric',
            'forZone.required' => 'Zone is required',
            'forZone.alpha_num' => 'Zone must be an alpha numeric characters',
               
           ]);
        try{
         $forZone = $request->zone;
                $subdivision=$request->subdivision;
                $session_personnela = session()->get('personnela_ppds');
                $ptable = "";
                $SubDivisionAvailable = tbl_personnela::join('subdivision', 'subdivision.subdivisioncd', '=', DB::raw("SUBSTRING(" . $session_personnela . ".personcd,1,4)"))
                        ->select('subdivision.subdivision', 'subdivision.subdivisioncd')
                        ->where('forzone', '=', $forZone)
                         ->where('selected','=',1)
                        ->groupBy('subdivision');
                
                 if ($forZone!=''){
                    $SubDivisionAvailable=$SubDivisionAvailable->where('forzone', '=', $forZone);
                }
                 if ($subdivision!=''){
                    $SubDivisionAvailable=$SubDivisionAvailable->where(DB::raw("SUBSTRING(" . $session_personnela . ".personcd,1,4)"), '=', $subdivision);
                }
$SubDivisionAvailable=$SubDivisionAvailable->get();
                $PostStatAvailable = tbl_personnela::join('subdivision', 'subdivision.subdivisioncd', '=', DB::raw("SUBSTRING(" . $session_personnela . ".personcd,1,4)"))
                                ->select('subdivision.subdivision', 'poststat', DB::raw('count(personcd) as total'))
                               
                                ->where('selected','=',1)
                                ->groupBy('subdivision')
                                ->groupBy('poststat')
                                ->orderBy('poststat')->get();

                if(count($SubDivisionAvailable)>0 && count($PostStatAvailable)>0){
                    foreach ($SubDivisionAvailable as $sy) {
                    $P1Available = tbl_personnela::join('subdivision', 'subdivision.subdivisioncd', '=', DB::raw("SUBSTRING(" . $session_personnela . ".personcd,1,4)"))->where('forzone', '=', $forZone)
                            ->where('poststat', '=', 'P1')
                            ->where('selected', '=', 1)
                            ->where(DB::raw("SUBSTRING(" . $session_personnela . ".personcd,1,4)"), '=', $sy->subdivisioncd)
                            ->value(DB::raw('count(personcd)'));

                    $P2Available = tbl_personnela::join('subdivision', 'subdivision.subdivisioncd', '=', DB::raw("SUBSTRING(" . $session_personnela . ".personcd,1,4)"))->where('forzone', '=', $forZone)
                            ->where('poststat', '=', 'P2')
                            ->where(DB::raw("SUBSTRING(" . $session_personnela . ".personcd,1,4)"), '=', $sy->subdivisioncd)
                            ->where('selected', '=', 1)
                            ->value(DB::raw('count(personcd)'));
                    $P3Available = tbl_personnela::join('subdivision', 'subdivision.subdivisioncd', '=', DB::raw("SUBSTRING(" . $session_personnela . ".personcd,1,4)"))->where('forzone', '=', $forZone)
                            ->where('poststat', '=', 'P3')
                            ->where(DB::raw("SUBSTRING(" . $session_personnela . ".personcd,1,4)"), '=', $sy->subdivisioncd)
                            ->where('selected', '=', 1)
                            ->value(DB::raw('count(personcd)'));
                    $PRAvailable = tbl_personnela::join('subdivision', 'subdivision.subdivisioncd', '=', DB::raw("SUBSTRING(" . $session_personnela . ".personcd,1,4)"))->where('forzone', '=', $forZone)
                            ->where('poststat', '=', 'PR')
                            ->where(DB::raw("SUBSTRING(" . $session_personnela . ".personcd,1,4)"), '=', $sy->subdivisioncd)
                            ->where('selected', '=', 1)
                            ->value(DB::raw('count(personcd)'));
                    $PAAvailable = tbl_personnela::join('subdivision', 'subdivision.subdivisioncd', '=', DB::raw("SUBSTRING(" . $session_personnela . ".personcd,1,4)"))->where('forzone', '=', $forZone)
                            ->where('poststat', '=', 'PA')
                            ->where(DB::raw("SUBSTRING(" . $session_personnela . ".personcd,1,4)"), '=', $sy->subdivisioncd)
                            ->where('selected', '=', 1)
                            ->value(DB::raw('count(personcd)'));
                    $PBAvailable = tbl_personnela::join('subdivision', 'subdivision.subdivisioncd', '=', DB::raw("SUBSTRING(" . $session_personnela . ".personcd,1,4)"))->where('forzone', '=', $forZone)
                            ->where('poststat', '=', 'PB')
                            ->where(DB::raw("SUBSTRING(" . $session_personnela . ".personcd,1,4)"), '=', $sy->subdivisioncd)
                            ->where('selected', '=', 1)
                            ->value(DB::raw('count(personcd)'));
                    $ptable .= "<table class='table table-bordered table-striped1' width='100%' style='border-top: 2px solid #009fe8;'> ";
                    $ptable .= "<tr style='background-color: #f5f8fa'>";
                    $ptable .= "<th colspan=" . count($PostStatAvailable) . ">Subdivision:" . $sy->subdivision . "</th>";
                    $ptable .= "</tr>";
                    $ptable .= "<tr>";

                    if ($P1Available != 0) {

                        $ptable .= "<td style='padding:0px;border-collapse: collapse;'>";
                        $ptable .= "<table width='100%'>";
                        $ptable .= "<tr><td> P1";
                        $ptable .= "</td></tr>";
                        $ptable .= "<tr><td>" . $P1Available;
                        $ptable .= "</td></tr>";
                        $ptable .= "</table>";
                        $ptable .= "</td>";
                        // echo "P1: ".$p1_stat_count.";&nbsp;";
                    }
                    if ($P2Available != 0) {
                        $ptable .= "<td style='padding:0px;border-collapse: collapse;'>";
                        $ptable .= "<table width='100%'>";
                        $ptable .= "<tr><td> P2";
                        $ptable .= "</td></tr>";
                        $ptable .= "<tr><td>" . $P2Available;
                        $ptable .= "</td></tr>";
                        $ptable .= "</table>";
                        $ptable .= "</td>";
                    }
                    if ($P3Available != 0) {
                        $ptable .= "<td style='padding:0px;border-collapse: collapse;'>";
                        $ptable .= "<table width='100%'>";
                        $ptable .= "<tr><td> P3";
                        $ptable .= "</td></tr>";
                        $ptable .= "<tr><td>" . $P3Available;
                        $ptable .= "</td></tr>";
                        $ptable .= "</table>";
                        $ptable .= "</td>";
                    }
                    if ($PAAvailable != 0) {
                        $ptable .= "<td style='padding:0px;border-collapse: collapse;'>";
                        $ptable .= "<table width='100%' >";
                        $ptable .= "<tr><td> PA";
                        $ptable .= "</td></tr>";
                        $ptable .= "<tr><td>" . $PAAvailable;
                        $ptable .= "</td></tr>";
                        $ptable .= "</table>";
                        $ptable .= "</td>";
                    }
                    if ($PBAvailable != 0) {
                        $ptable .= "<td style='padding:0px;border-collapse: collapse;'>";
                        $ptable .= "<table width='100%'>";
                        $ptable .= "<tr><td> PB";
                        $ptable .= "</td></tr>";
                        $ptable .= "<tr><td>" . $PBAvailable;
                        $ptable .= "</td></tr>";
                        $ptable .= "</table>";
                        $ptable .= "</td>";
                    }
                    if ($PRAvailable != 0) {
                        $ptable .= "<td style='padding:0px;border-collapse: collapse;'>";
                        $ptable .= "<table width='100%'>";
                        $ptable .= "<tr><td> PR";
                        $ptable .= "</td></tr>";
                        $ptable .= "<tr><td>" . $PRAvailable;
                        $ptable .= "</td></tr>";
                        $ptable .= "</table>";
                        $ptable .= "</td>";
                    }
                    $ptable .= "<tr>";
                    $ptable .= "</table>";
                }


                $response = array(
                    'options' => $ptable,
                    'status' => 1
                );
                }
                else{
                     $response = array(
                  
                    'status' => 2
                );
                }


                
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    

}
