<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\tbl_district;
use App\tbl_pc;
use App\tbl_user;
use App\tbl_subdivision;
use App\tbl_assembly;
use App\tbl_block_muni;
use App\tbl_zone;
use App\tbl_menu;
use App\tbl_user_permission;
use App\Http\Controllers\LoginController;
use Validator;

class UserController extends Controller {

    public function getDistrict() {
        $dist = \Session::get('districtcd_ppds');
        $district = new tbl_district();
        if ($dist != NULL) {
            $district = $district->where('districtcd', '=', $dist);
        }
        $district = $district->pluck('district', 'districtcd')->all();
        $data = array('district' => $district);
        return $data;
    }

    public function getPc() {
        $pc = tbl_pc::pluck('pcname', 'pccd')->all();
        $data = array('pc' => $pc);
        return $data;
    }

    public function district_wise_sub_block($sub) {
        $result = tbl_district::select('districtcd', 'district');
        if ($sub != '') {
            $result = $result->where('subdivisioncd', '=', $sub);
        }
        $result = $result->with('district_wise_sub')->get();
        return ($result);
    }

    public function index() {

        $dist = $this->getDistrict();
        $pc = $this->getPc();
        $get_all = $this->district_wise_sub_block('');
        return view('create_user')->with('dist', $dist)->with('pc', $pc)->with('get_all', $get_all);
    }

    public function select_subdivision(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        }
        $this->validate($request, [
            'districtcd' => 'required|alpha_num',
                ], [
            'districtcd.required' => 'District is required',
            'districtcd.alpha_num' => 'District code must be an alpha numeric',
        ]);
        try {
            $district = $request->districtcd;
            $subdiv = \Session::get('subdivisioncd_ppds');
            $subdivision = tbl_subdivision::where('districtcd', '=', $district);
            if ($subdiv != NULL) {
                $subdivision = $subdivision->where('subdivisioncd', '=', $subdiv);
            }
            $subdivision = $subdivision->pluck('subdivision', 'subdivisioncd')->all();
            $response = array(
                'options' => $subdivision
            );
        } catch (\Exception $e) {
            $response = array(
                'exception' => true,
                'exception_message' => $e->getMessage(),
            );
            $statusCode = 400;
        } finally {
            return response()->json($response, $statusCode);
        }
    }

    public function select_block(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        }
        $this->validate($request, [
            'subdivisioncd' => 'required|alpha_num',
                ], [
            'subdivisioncd.required' => 'Subdivision is required',
            'subdivisioncd.alpha_num' => 'Subdivision code must be an alpha numeric',
        ]);
        try {
            $subdivision = $request->subdivisioncd;
            $block = tbl_block_muni::where('subdivisioncd', '=', $subdivision)->pluck('blockmuni', 'blockminicd')->all();
            $response = array(
                'options' => $block
            );
        } catch (\Exception $e) {
            $response = array(
                'exception' => true,
                'exception_message' => $e->getMessage(),
            );
            $statusCode = 400;
        } finally {
            return response()->json($response, $statusCode);
        }
    }

    public function select_assebly(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        }
        $this->validate($request, [
            'districtcd' => 'required|alpha_num',
                ], [
            'districtcd.required' => 'District is required',
            'districtcd.alpha_num' => 'District code must be an alpha numeric',
        ]);
        try {
            $district = $request->districtcd;
            $assembly = tbl_assembly::where('districtcd', '=', $district)->pluck('assemblyname', 'assemblycd')->all();
            $response = array(
                'options' => $assembly
            );
        } catch (\Exception $e) {
            $response = array(
                'exception' => true,
                'exception_message' => $e->getMessage(),
            );
            $statusCode = 400;
        } finally {
            return response()->json($response, $statusCode);
        }
    }

    public function select_zone(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        }
        $this->validate($request, [
            'districtcd' => 'required|alpha_num',
                ], [
            'districtcd.required' => 'District is required',
            'districtcd.alpha_num' => 'District code must be an alpha numeric',
        ]);
        try {
            $district = $request->districtcd;
            $zone = tbl_zone::where('districtcd', '=', $district)->pluck('zonename', 'zone')->all();
            $response = array(
                'options' => $zone
            );
        } catch (\Exception $e) {
            $response = array(
                'exception' => true,
                'exception_message' => $e->getMessage(),
            );
            $statusCode = 400;
        } finally {
            return response()->json($response, $statusCode);
        }
    }

    public function user_list_datatable(Request $request) {
        $response = [];
        $perm = null;
        $statusCode = 200;
        $users = array(); //Should be changed #4
        $search_val = array();
        $this->validate($request, [
            'search.*' => 'nullable|regex:/^[A-Za-z0-9\s]+$/i'
                ], [
            'search.*.regex' => 'Special Charecters not allowed'
        ]);
        try {
            $draw = $request->draw;
            $offset = $request->start;
            $length = $request->length;
            $search = $request->search ["value"];
            $order = $request->order;
            //print_r($order);die;

            $users = tbl_user::all();
            $categ = \Session::get('category_ppds');
            $dist = \Session::get('districtcd_ppds');
            $ucode = \Session::get('code_ppds');
            $filtered = tbl_user::where(function($q) use ($search) {
                        $q->orwhere('user_id', 'like', '%' . $search . '%')
                                ->orwhere('category', 'like', '%' . $search . '%');
                    })->where('category', '>=', $categ);
            if ($categ != 0) {
                $filtered = $filtered->where('districtcd', '=', $dist);
            }
            $filtered = $filtered->where('code', '!=', $ucode);
            $ordered = $filtered;
            $filtered_count = $filtered->count();
            //echo count ( $order );die;
            for ($i = 0; $i < count($order); $i ++) {
                $ordered = $ordered->orderBy($request->columns [$order [$i] ['column']] ['data'], strtoupper($order [$i] ['dir']));
            }
            $page_displayed = $ordered->get();
            $data = array();
            if (!empty($page_displayed)) {
                foreach ($page_displayed as $user) {
                    $cat_arr = config('constants.USER_CATEGORY');
                    $nestedData['user_id'] = $user->user_id;
                    $nestedData['category'] = $cat_arr[$user->category];
                    $nestedData['creation_date'] = date('d/m/Y', strtotime(trim(str_replace('-', '/', $user->creation_date))));
                    if ($user->category != 0) {
                        $permission_button = $edit_button = $delete_button = $user->code;
                    } else {
                        $permission_button = $edit_button = $user->code;
                        $delete_button = '';
                    }
                    $nestedData['action'] = array('p' => $permission_button, 'e' => $edit_button, 'd' => $delete_button);
                    $data[] = $nestedData;
                }
            }
            $response = array(
                "draw" => $draw,
                "recordsTotal" => $users->count(), //Should be changed #7
                "recordsFiltered" => $filtered_count,
                'user_masters' => $data //Should be changed #8
            );
        } catch (\Exception $e) {
            $response = array(
                'exception' => true,
                'exception_message' => $e->getMessage()
            );
            $statusCode = 400;
        } finally {
            return response()->json($response, $statusCode);
        }
    }

    public function add_user_details(Request $request) {
        $statusCode = 200;
        $usermaster = null;
        $filename = "";
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in form submit.');
            return response()->json($response, $statusCode);
        }
        $response = [
            'usermaster' => [] //Should be changed #9
        ];
        /*         * ****Validation****** */
        $this->validate($request, [
            'full_name' => "required|max:100|regex:/^[A-Za-z\s]+$/i|",
            'username' => "required|min:3|max:10|alpha_num|unique:user,user_id,null,code",
            'pass' => "required|min:6|max:10|regex: /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@$#%\-\_])(?=.{6,})/",
            'con_pass' => "required|same:pass|min:6|max:10|regex: /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@$#%\-\_])(?=.{6,})/",
            'mobileno' => "required|min:10|max:10|digits:10",
            'email_user' => "required|max:50|email",
            'designation' => "required|max:50|regex:/^[A-Za-z0-9\s]+$/i|",
            'category' => 'required|integer',
            'districtcd' => 'required|alpha_num',
            'subdivisioncd' => 'alpha_num|nullable|required_if:category,3,4,5',
            'blockcd' => 'alpha_num|nullable|required_if:category,5',
            'assemblycd' => 'alpha_num|nullable|required_if:category,7',
            'zonecd' => 'alpha_num|nullable|required_if:category,6',
            'parliamentcd' => 'alpha_num|nullable|required_if:category,4',
                ], [
            'full_name.required' => 'Full Name is required.',
            'full_name.max' => 'Full Name must not be greater than 100 characters.',
            'full_name.regex' => 'Full Name must be Alphabetic.',
            'username.required' => 'User Name is required.',
            'username.min' => 'User Name must be at least 3 characters.',
            'username.max' => 'User Name must not be greater than 10 characters.',
            'username.alpha_num' => 'User Name must be alpha numeric.',
            'username.unique' => 'User Name has already been taken.',
            'pass.required' => 'Password field is required.',
            'pass.min' => 'Password must be at least 6 characters.',
            'pass.max' => 'Password may not be greater than 10 characters.',
            'pass.regex' => 'Password contain alteast six characters, one uppercase letter, one lowercase letter, one number and one special character[!@#$%^&*_-].',
            'con_pass.required' => 'Confirm Password field is required.',
            'con_pass.min' => 'Confirm Password must be at least 6 characters.',
            'con_pass.max' => 'Confirm Password may not be greater than 10 characters.',
            'con_pass.regex' => 'Confirm Password contain alteast six characters, one uppercase letter, one lowercase letter, one number and one special character[!@#$%^&*_-].',
            'con_pass.same' => 'Password does not match the confirm password.',
            'mobileno.required' => 'Mobile No. field is required',
            'mobileno.digits' => 'Mobile No. filed should be in digits and may not be greater than 10 characters',
            'mobileno.min' => 'Minimum 10 digits mobile no is required',
            'mobileno.max' => 'Maximum mobile number should be 10 digits',
            // 'mobileno.unique'=>'This mobile no has already been taken,try another mobile no',
            'email_user.required' => 'User email required',
            'email_user.email' => 'User email must be a valid email address',
            // 'email_user.unique' => 'User email must be a unique',
            'email_user.max' => 'User email may not be greater than 50 characters',
            'designation.required' => 'Designation is required.',
            'designation.max' => 'Designation must not be greater than 50 characters.',
            'designation.regex' => 'Designation must be Alpha numeric.',
            'category.required' => 'Category is required.',
            'category.integer' => 'Category may only contain Integer.',
            'districtcd.required' => 'District is required',
            'districtcd.alpha_num' => 'District Code must be an Alpha numeric',
            'subdivisioncd.required_if' => 'Subdivision is required',
            'subdivisioncd.alpha_num' => 'Subdivision Code must be an Alpha numeric',
            'blockcd.required_if' => 'Block is required',
            'blockcd.alpha_num' => 'Block Code must be an Alpha numeric',
            'assemblycd.required_if' => 'Assembly is required',
            'assemblycd.alpha_num' => 'Assembly Code must be an Alpha numeric',
            'zonecd.required_if' => 'Zone is required',
            'zonecd.alpha_num' => 'Zone Code must be an Alpha numeric',
            'parliamentcd.required_if' => 'Parliament is required',
            'parliamentcd.alpha_num' => 'Parliament Code must be an Alpha numeric',
        ]);
        try {
            $password = md5($request->con_pass);
            $full_name = $request->full_name;
            $designation = $request->designation;
            $username = $request->username;
            $mobileno = $request->mobileno;
            $email = $request->email_user;

            $category = $request->category;
            $districtcd = $request->districtcd;
            $parliamentcd = $request->parliamentcd;
            $assemblycd = $request->assemblycd;
            $blockcd = $request->blockcd;
            $zonecd = $request->zonecd;
            $subdivisioncd = $request->subdivisioncd;

            $usermaster = tbl_user::insert(['full_name' => $full_name, 'user_id' => $username, 'password' => $password, 'mobileno' => $mobileno, 'email' => $email, 'designation' => $designation, 'category' => $category, 'districtcd' => $districtcd, 'subdivisioncd' => $subdivisioncd, 'parliamentcd' => $parliamentcd, 'assemblycd' => $assemblycd, 'blockmunicd' => $blockcd, 'zonecd' => $zonecd]);
            if ($usermaster == true) {
                $response = array(
                    'status' => 1 //Should be changed #13
                );
            } else {
                $response = array(
                    'status' => 0 //Should be changed #13
                );
            }
        } catch (\Exception $e) {
            $response = array(
                'exception' => true,
                'exception_message' => $e->getMessage(),
            );
            $statusCode = 400;
        } finally {
            return response()->json($response, $statusCode);
        }
    }

    public function menuwise_submenu($menu_code, $user_code) {

        $tbl_menu = new tbl_menu();
        if ($menu_code != '') {
            $tbl_menu = $tbl_menu->where('menu_cd', '=', $menu_code);
        }
        $tbl_menu = $tbl_menu->with(array('menu_wise_submenu.user_permission_submenu_cd' => function($query) use($user_code) {
                        if ($user_code != '') {
                            $query->where('user_cd', '=', $user_code);
                        }
                    }))->get();
        return $tbl_menu;
    }

    public function create_permission(Request $request) {
        $statusCode = 200;
        $response = array();
        $v = Validator::make($request->all(), [
                    'user_code' => 'required|integer',
                        ], [
                    'user_code.required' => 'User code is required',
                    'user_code.integer' => 'User code must be an integer',
        ]);

        if ($v->fails()) {
            $statusCode = 400;
            $response = $v->errors()->all();
            return view('error.redirect_error')->with('response', $response)->with('statusCode', $statusCode);
        }
        try {
            $uid = $request->user_code;
            $user_data = tbl_user::select('code', 'user_id')
                            ->where('code', '=', $uid)->get();
            $data1 = $this->menuwise_submenu('', '');
            $ucode = \Session::get('code_ppds');
            $logincontroller = new LoginController();
            $data = $logincontroller->get_permission('', 'permission', '');
            // print_r(json_encode($data));die;
        } catch (\Exception $e) {
            $response = array(
                'exception' => true,
                'exception_message' => $e->getMessage(),
            );
            $statusCode = 400;
        } finally {
            return view('user_permission', compact('data'))->with('user_data', $user_data)->with('response', $response)->with('statusCode', $statusCode);
        }
    }

    public function user_permission_sub_menu_list(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $data = array('error' => 'Error occured in Ajax Call.');
            return response()->json($data, $statusCode);
        }
        $this->validate($request, [
            'menu_code' => 'required|integer',
            'user_code' => 'required|integer',
                ], [
            'menu_code.required' => 'Menu is required',
            'menu_code.integer' => 'Menu code must be an integer',
            'user_code.required' => 'User Code is required',
            'user_code.integer' => 'User code must be an integer',
        ]);
        try {
            $menucode = $request->menu_code;
            $user_code = $request->user_code;
            $data = $this->menuwise_submenu($menucode, $user_code);
        } catch (\Exception $e) {
            $data = array(
                'exception' => true,
                'exception_message' => $e->getMessage(),
            );
            $statusCode = 400;
        } finally {
            return response()->json($data, $statusCode);
        }
    }

    public function user_permission_sub_menu_list_create(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $data = array('error' => 'Error occured in Ajax Call.');
            return response()->json($data, $statusCode);
        }
        $this->validate($request, [
            'menu_code' => 'required|integer',
            'user_code' => 'required|integer',
                ], [
            'menu_code.required' => 'Menu is required',
            'menu_code.integer' => 'Menu code must be an integer',
            'user_code.required' => 'User Code is required',
            'user_code.integer' => 'User code must be an integer',
        ]);
        try {
            $menucode = $request->menu_code;
            $user_code = $request->user_code;
            $logincontroller = new LoginController();
            $data = $logincontroller->get_permission('', 'permission', $menucode);
        } catch (\Exception $e) {
            $data = array(
                'exception' => true,
                'exception_message' => $e->getMessage(),
            );
            $statusCode = 400;
        } finally {
            return response()->json($data, $statusCode);
        }
    }

    public function create_user_permission(Request $request) { //print_r($request);
        $permission = array();
        $statusCode = 200;
        $menu = null;
        $filename = "";
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in form submit.');
            return response()->json($response, $statusCode);
        }
        $response = [
            'menu' => [] //Should be changed #9
        ];

        $this->validate($request, [
            'menucd' => ' ',
                ], [
            'menucd.required' => 'The Menu Name field is required',
        ]);
        try {
            // dd($request->all());
            $user_code = session()->get('cams_code');
            $row_count = $request->row_count;
            $menuname = $request->menucd;
            $user_cd = $request->user_code;
            $get_val = tbl_user_permission::where('user_cd', $request->user_code)->where('menu_cd', $menuname)->delete();
//           $dataSet = [];
//foreach ($checkBox as $safety) {
//    $dataSet[] = [
//        'offer_id'  => $id,
//        'car_id'    => $safety,
//        'car'       => 15,
//    ];
//}
//
//DB::table('extra')->insert($dataSet);
            $count = $row_count;
            // echo $count;die;
            for ($i = 1; $i <= $row_count; $i++) {
                $test1 = 'myCheck_' . $i;
                $test2 = 'view_' . $i;
//echo $test1.'/'.$test2;
                if ($request->$test1 != '') {
                    $sub = $request->$test1;
                    //echo $sub;die;
                    if ($request->$test2 != '') {
                        $show = $request->$test2;
                    } else {
                        $show = 0;
                    }

                    $permission = new tbl_user_permission();

                    $permission->insert(['user_cd' => $user_cd, 'menu_cd' => $menuname, 'submenu_cd' => $sub, 'show_status' => $show]);
                }
                $count--;
            }
            //$permission->save();
            if ($count == 0) {
                $response = array(
                    'menu' => $permission, 'status' => 1 //Should be changed #13
                );
            }
        } catch (\Exception $e) {
            $response = array(
                'exception' => true,
                'exception_message' => $e->getMessage(),
            );
            $statusCode = 400;
        } finally {
            return response()->json($response, $statusCode);
        }
    }

    public function user_permission_alloted(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $alloted = array('error' => 'Error occured in Ajax Call.');
            return response()->json($alloted, $statusCode);
        }
        $this->validate($request, [
            'user_code' => 'required|integer',
                ], [
            'user_code.required' => 'User Code is required',
            'user_code.integer' => 'User code must be an integer',
        ]);
        try {
            $uid = $request->user_code;
            $login = new LoginController();
            $alloted = $login->get_permission($uid, 'permission', '');
        } catch (\Exception $e) {
            $alloted = array(
                'exception' => true,
                'exception_message' => $e->getMessage(),
            );
            $statusCode = 400;
        } finally {
            return response()->json($alloted, $statusCode);
        }
    }

    public function user_list_modify(Request $request) {
        $statusCode = 200;
        $response = array();

        $v = Validator::make($request->all(), [
                    'user_code' => 'nullable|integer',
                        ], [
                    'user_code.required' => 'User code is required',
                    'user_code.integer' => 'User code must be an integer',
        ]);

        if ($v->fails()) {
            $statusCode = 400;
            $response = $v->errors()->all();
            return view('error.redirect_error')->with('response', $response)->with('statusCode', $statusCode);
        }
        try {
            $uid = $request->user_code;
            if ($uid != 0) {
                $user_data = tbl_user::where('code', '=', $uid)->get();
            } else {
                $user_data = array();
            }

            $dist = $this->getDistrict();
            $pc = $this->getPc();
        } catch (\Exception $e) {
            $response = array(
                'exception' => true,
                'exception_message' => $e->getMessage(),
            );
            $statusCode = 400;
        } finally {
            return view('create_user')->with('dist', $dist)->with('pc', $pc)->with('user_data', $user_data)->with('response', $response)->with('statusCode', $statusCode);
        }
    }

    public function add_user_update(Request $request) {
        $statusCode = 200;
        $usermaster = null;
        $filename = "";
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in form submit.');
            return response()->json($response, $statusCode);
        }
        $response = [
            'usermaster' => [] //Should be changed #9
        ];
        /*         * ****Validation****** */
        $mobileno = $request->mobileno;
        $email = $request->email_user;
        $this->validate($request, [
            'full_name' => "required|max:100|regex:/^[A-Za-z\s]+$/i|",
            'pass' => "required|min:6|max:10|regex: /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@$#%\-\_])(?=.{6,})/",
            'con_pass' => "required|same:pass|min:6|max:10|regex: /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@$#%\-\_])(?=.{6,})/",
            'designation' => "required|max:50|regex:/^[A-Za-z0-9\s]+$/i|",
            'mobileno' => "required|min:10|max:10|digits:10",
            'email_user' => "required|max:50|email",
                ], [
            'full_name.required' => 'Full Name is required.',
            'full_name.max' => 'Full Name must not be greater than 100 characters.',
            'full_name.regex' => 'Full Name must be Alphabetic.',
            'pass.required' => 'Password field is required.',
            'pass.min' => 'Password must be at least 6 characters.',
            'pass.max' => 'Password may not be greater than 10 characters.',
            'pass.regex' => 'Password Atleast Contain Six characters, at least one uppercase letter, one lowercase letter, one number and one special character[!@#$%^&*_-].',
            'con_pass.required' => 'Confirm Password field is required.',
            'con_pass.min' => 'Confirm Password must be at least 6 characters.',
            'con_pass.max' => 'Confirm Password may not be greater than 10 characters.',
            'con_pass.regex' => 'Confirm Password Atleast Contain Six characters, at least one uppercase letter, one lowercase letter, one number and one special character[!@#$%^&*_-].',
            'con_pass.same' => 'Confirm Password does not match with Confirm Password.',
            'mobileno.required' => 'Mobile No. field is required.',
            'mobileno.digits' => 'Mobile No. filed should be in digits and may not be greater than 10 characters.',
            'mobileno.min' => 'Minimum 10 digits mobile no is required.',
            'mobileno.max' => 'Maximum mobile number should be 10 digits.',
            //  'mobileno.unique'=>'This mobile no has already been taken,try another mobile no.',
            'email_user.required' => 'User email required.',
            'email_user.email' => 'User email must be a valid email address.',
            // 'email_user.unique' => 'User email must be a unique.',
            'email_user.max' => 'User email may not be greater than 50 characters.',
            'designation.required' => 'Designation is required.',
            'designation.max' => 'Designation must not be greater than 50 characters.',
            'designation.regex' => 'Designation must be alpha numeric.',
        ]);
        try {
            $uid = $request->code;
            $password = md5($request->con_pass);
            $full_name = $request->full_name;
            $designation = $request->designation;

            $usermaster = tbl_user::where('code', '=', $uid)->update(['password' => $password, 'full_name' => $full_name, 'designation' => $designation, 'mobileno' => $mobileno, 'email' => $email]);
            if ($usermaster == true) {
                $response = array(
                    'status' => 2 //Should be changed #13
                );
            }
        } catch (\Exception $e) {
            $response = array(
                'exception' => true,
                'exception_message' => $e->getMessage(),
            );
            $statusCode = 400;
        } finally {
            return response()->json($response, $statusCode);
        }
    }

    public function user_list_delete(Request $request) {
        $response = [
            'usermaster' => [] //Should be changed #25
        ];
        $statusCode = 200;
        $users = null; //Should be changed #26
        $user_code = $request->user_code;
        try {
            $users = tbl_user::where('code', '=', $user_code); //Should be changed #27

            if (!empty($users)) {//Should be changed #30
                $users = $users->delete();
                tbl_user_permission::where('user_cd', '=', $user_code)->delete();
            }
            $response = array(
                'users' => $users //Should be changed #32
            );
        } catch (\Exception $e) {
            $response = array(
                'exception' => true,
                'exception_message' => $e->getMessage(),
                'exception_code' => $e->getCode()
            );
            $statusCode = 400;
        } finally {
            return response()->json($response, $statusCode);
        }
    }

}
