<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\tbl_pollingstation;
use App\tbl_assembly_zone;
use App\tbl_dcrc_party;
use Validator;
use DB;

class PollingStationController extends Controller {

    //:::::::::::::::Download Excel Format::::::::::::::::::::://
    public function excelformat(Request $request) {

//             $payments = tbl_pollingstation::
//                       // ->join('assembly','assembly.assemblycd','=','assembly_zone.assemblycd')
//                       //->Rightjoin('assembly_party','assembly_party.assemblycd','=','assembly_zone.assemblycd')     
//                        select('psno','psfix','forassembly','gender','member','psname','dcrccd')
//                        ->get();
        $payments = ['PS No', 'PSfix', 'Assembly Code', 'Member', 'PS Name', 'Gender'];
        return \Excel::create('PSExcel', function($excel) use ($payments) {
                    $excel->sheet(' ', function($sheet) use ($payments) {
                        $sheet->fromArray($payments);
                    });
                })->download('xlsx');
    }

    //::::::::Polling Station File Upload:::::::://   
    public function pollingStationAdd(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
//            $validator = Validator::make(
//                [
//                    'file'      => $request->excelup,
//                    'extension' => strtolower($request->excelup->getClientOriginalExtension()),
//                ],
//                [
//                    'file'          => 'required',
//                    'extension'      => 'required|in:xlsx,xls',
//                ]
//              );
            $this->validate($request, [
                'excelup' => 'required',
                'districtcd' => 'required|alpha_num|min:2|max:2'
                    ], [
                'excelup.required' => 'File is required',
                'districtcd.required' => 'District is required',
                'districtcd.alpha_num' => 'District must be an alpha numeric characters'
            ]);
            $forDist = $request->districtcd;
            $user_code = session()->get('code_ppds');
            try {
                if ($request->hasFile('excelup')) {
                    $path = $request->file('excelup')->getRealPath();
                    
//                    $records = \Excel::load($path, function($reader) { $reader->noHeading = true; })->get();  
                    $records = \Excel::load($path)->get();
                    if(count($records)>0){
                        $firstrow = $records->first()->keys()->toArray();
                        if ($firstrow[0] == "ps_no" && $firstrow[1] == "psfix" && $firstrow[2] == "assembly_code" && $firstrow[3] == "member" && $firstrow[4] == "ps_name" && $firstrow[5] == "gender") {

                            if ($records->count()) {
                                $v = Validator::make($records->toArray(), [
                                            '*.' . $firstrow[0] => 'required|digits_between:1,4',
                                            '*.' . $firstrow[1] => 'nullable|alpha_num|max:2',
                                            '*.' . $firstrow[2] => 'required|alpha_num|max:3|min:3',
                                            '*.' . $firstrow[3] => 'required|digits:1',
                                            '*.' . $firstrow[4] => 'required|regex:/[a-zA-Z0-9\s(),\-.]+/|Max:150',
                                    //regex:/^[A-Za-z0-9\s(),\-.]+$/i
                                            '*.' . $firstrow[5] => 'required|alpha|max:1|min:1',
                                                ], [
                                            '*.' . $firstrow[0] . '.required' => 'PS No is required.',
                                            '*.' . $firstrow[0] . '.digits_between' => 'PS No must be integer.',
                                            '*.' . $firstrow[1] . '.alpha_num' => 'PSFix must be alpha numeric.',
                                            '*.' . $firstrow[1] . '.max' => 'PSFix length must be less than 3 character.',
                                            '*.' . $firstrow[2] . '.required' => 'Assembly code is required.',
                                            '*.' . $firstrow[2] . '.alpha_num' => 'Assembly code must be alpha numeric.',
                                            '*.' . $firstrow[2] . '.max' => 'Assembly code should be 3 digits.',
                                            '*.' . $firstrow[2] . '.min' => 'Assembly code should be 3 digits.',
                                            '*.' . $firstrow[3] . '.required' => 'Member is required.',
                                            '*.' . $firstrow[3] . '.digits' => 'Member must be 1 digit',
                                            '*.' . $firstrow[4] . '.required' => 'PS Name is required.',
                                            '*.' . $firstrow[4] . '.regex' => 'PS Name must not contain any special charecter',
                                            '*.' . $firstrow[4] . '.max' => 'PS Name length must not be greater than 150.',
                                            '*.' . $firstrow[5] . '.required' => 'Gender is required.',
                                            '*.' . $firstrow[5] . '.alpha' => 'Gender must be alpha character.',
                                            '*.' . $firstrow[5] . '.max' => 'Gender length must be 1 character.',
                                ]);
                                $data = array();
                                foreach ($v->errors()->toArray() as $key => $value) {
                                    $x = explode('.', $key);
                                    $data["$x[1]" . " Row " . "$x[0]"] = $value;
                                }
                                $response = $data;
                                if ($v->fails()) {
                                    $statusCode = 422;
                                    return response()->json($response, $statusCode);
                                }
                                $tbl_pollingstation = new tbl_pollingstation;
                                $tbl_pollingstation->where(DB::raw('SUBSTRING(forzone,1,2)'), '=', $forDist)
                                        ->delete();

                                $count = 0;
                                $arr = array();
                                foreach ($records as $key => $value) {
                                    $count++;
                                    $assembly_zone = new tbl_assembly_zone;
    //                          echo $value[$firstrow[0]];
                                    $zone = $assembly_zone->where('assemblycd', '=', $value[$firstrow[2]])
                                            ->value('zone');
                                    $phase = $assembly_zone->where('assemblycd', '=', $value[$firstrow[2]])
                                    ->value('phase_id');
                                    $arr[] = ['psno' => $value[$firstrow[0]], 'psfix' => $value[$firstrow[1]], 'forzone' => $zone,'phase'=> $phase,'forassembly' => $value[$firstrow[2]], 'member' => $value[$firstrow[3]], 'psname' => $value[$firstrow[4]], 'gender' => $value[$firstrow[5]], 'usercode' => $user_code];
                                }
                                if (!empty($arr)) {
                                    tbl_pollingstation::insert($arr);
                                }
                            }
                            $response = array(
                                'options' => $count,
                                'status' => 1
                            );
                        } else {
                            $response = array(
                                'options' => "Check column name or no of column",
                                'status' => 2
                            );
                        }
                    }else {
                        $response = array(
                            'options' => "No records found",
                            'status' => 2
                        );
                    }
                }
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }
    
    //::::::::Polling Stationwise DCRC:::::::::://
    public function getDCAssemblyVenueDetails(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            $this->validate($request, [
                'forZone' => 'required|alpha_num|min:4|max:4',
                'assembly' => 'required|alpha_num|min:3|max:3'
                    ], [
                'forZone.required' => 'For Zone is required',
                'forZone.alpha_num' => 'For Zone must be an alpha numeric',
                'assembly.required' => 'Assembly is required',
                'assembly.alpha_num' => 'Assembly must be an alpha numeric'
            ]);
            try {
                $forZone = $request->forZone;
                $phase = $request->phase;
                $assembly = $request->assembly;
                $dcVenue = tbl_dcrc_party::join('dcrcmaster','dcrc_party.dcrcgrp','=','dcrcmaster.dcrcgrp')
                                ->select(DB::raw("CONCAT(dc_venue,'-',DATE_FORMAT(dcrcmaster.dc_date,'%d/%m/%Y'),'-',dc_time) AS dc_venue"), 'dcrcmaster.dcrcgrp')                   
                                ->where('dcrc_party.zone', '=', $forZone)
                                // ->where('dcrc_party.zone', '=', $forZone)
                                ->where('dcrc_party.assemblycd', '=', $assembly)
                                ->groupBy('dcrcmaster.dcrcgrp')
                                ->pluck('dcrcmaster.dc_venue', 'dcrcmaster.dcrcgrp')->all();
                $response = array('options' => $dcVenue,'status' => 1);
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }
    
    public function getPollingStationDetailsForDCRC(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            $this->validate($request, [
                'forZone' => 'required|alpha_num|min:4|max:4',
                'assembly' => 'required|alpha_num|min:3|max:3',
                'frompsno' => 'nullable|digits_between:1,4',
                'topsno' => 'nullable|digits_between:1,4'
                    ], [
                'forZone.required' => 'For Zone is required',
                'forZone.alpha_num' => 'For Zone must be an alpha numeric',
                'assembly.required' => 'Assembly is required',
                'assembly.alpha_num' => 'Assembly must be an alpha characters',
                'frompsno.digits_between' => 'From PS No must be an integer',
                'topsno.digits_between' => 'To PS No must be an integer'
            ]);
            try {
                $forZone = $request->forZone;
                $phase = $request->phase;
                $assembly = $request->assembly;
                $frompsno = $request->frompsno;
                $topsno = $request->topsno;
                $filtered = '';
                $tbl_pollingstation = new tbl_pollingstation;
                if ($frompsno != '') {
                    $tbl_pollingstation = $tbl_pollingstation->where('psno', '>=', $frompsno);
                }
                if ($topsno != '') {
                    $tbl_pollingstation = $tbl_pollingstation->where('psno', '<=', $topsno);
                }
                $filtered = $tbl_pollingstation->where('forzone', '=', $forZone)
                        ->where('phase', '=', $phase)
                        ->where('forassembly', '=', $assembly)
                        // ->join('assembly','assembly.assemblycd','=','assembly_zone.assemblycd')
                        //->Rightjoin('assembly_party','assembly_party.assemblycd','=','assembly_zone.assemblycd')     
                        ->select('psno', 'psfix', 'forassembly', 'gender', 'member', 'psname', 'dcrccd')
                        ->get();
                $response = array(
                    'options' => $filtered,
                    'status' => 1);
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }

    public function PSDCRCUpdate(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            $validate_array = ['forZone' => 'required|alpha_num|min:4|max:4',
                'dcvenue' => 'required|alpha_num|min:6|max:6',
                'assembly' => 'required|alpha_num|min:3|max:3'

            ];
            for ($x = 0; $x <= $request->row_count; $x++) {
                $validate_array['myCheck' . $x] = 'nullable|alpha_dash|max:20';
            }
            $validate_array1 = ['forZone.required' => 'Zone is required',
                'forZone.alpha_num' => 'Zone must be an alpha numeric characters',
                'dcvenue.required' => 'DC Venue is required',
                'dcvenue.alpha_num' => 'DC Venue must be an alpha numeric characters',
                'assembly.required' => 'Assembly is required',
                'assembly.alpha_num' => 'Assembly must be an alpha characters'];
            for ($y = 0; $y <= $request->row_count; $y++) {
                $validate_array1['myCheck' . $y . '.alpha_dash'] = 'PS No,Assembly,member,gender must be an alpha numeric characters';
            }
            $this->validate($request, $validate_array, $validate_array1);
            try {
                $user_code = session()->get('code_ppds');
                $row_count = $request->row_count;
                $forZone = $request->forZone;
                $dcvenue = $request->dcvenue;
                $assembly = $request->assembly;
                $phase = $request->phase;

                $count = 0;
                $status = 0;
                for ($i = 1; $i <= $row_count; $i++) {
                    $asmID = 'myCheck' . $i;
                    if ($request->$asmID != "") {
                        $asmstat_id = $request->$asmID;
                        $asmstat = explode('-', $asmstat_id);
                        $assembly = $asmstat[0];
                        $member = $asmstat[1];
                        $gender = $asmstat[2];
                        $psno = $asmstat[3];
                        $psfix = $asmstat[4];
                        $filtered = "";
                        $tbl_pollingstation = new tbl_pollingstation;
                        $filtered = $tbl_pollingstation->where('forzone', '=', $forZone)
                                ->where('phase', '=', $phase)
                                ->where('forassembly', '=', $assembly)
                                ->where('member', '=', $member)
                                ->where('gender', '=', $gender)
                                ->where('psno', '=', $psno)
                                ->where('psfix', '=', $psfix)
                                ->update(['dcrccd' => $dcvenue]);
                        $count++;
                    }
                }

                $response = array(
                    'options' => $count,
                    'status' => 1);
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }


    // public function get_assembly_details_phase_wise(Request $request)
    // {
        
    // }

    public function polling_list_datatable(Request $request) {
        $response = [];
        $perm = null;
        $statusCode = 200;
        $users = array(); //Should be changed #4
        $search_val = array();
        $this->validate($request, [
            'search.*' => 'nullable|regex:/^[A-Za-z0-9\s]+$/i',
            'draw' => 'required|integer',
            'start' => 'required|integer',
            'length' => 'required|integer',
            'zone' => 'nullable|alpha_num|min:4|max:4',
            'dcvenue' => 'nullable|alpha_num|min:6|max:6',
            'assembly' => 'nullable|alpha_num|min:3|max:3',
                ], [
            'search.*.regex' => 'Special Characters not allowed',
            'draw.required' => 'Draw is required',
            'start.integer' => 'Start must be an integer',
            'start.required' => 'Start is required',
            'length.integer' => 'Length must be an integer',
            'length.required' => 'Length is required',
            'zone.alpha_num' => 'Zone code must be an alpha numeric',
            'dcvenue.alpha_num' => 'DC Venue code must be an alpha numeric',
            'assembly.alpha_num' => 'Assembly code must be an alpha numeric'      
        ]);
        try {
            $draw = $request->draw;
            $offset = $request->start;
            $length = $request->length;
            $search = $request->search ["value"];
            $order = $request->order;
            $forZone = $request->zone;
            $phase = $request->phase;
            $dcvenue = $request->dcvenue;
            $assembly = $request->assembly;
            $polling = tbl_pollingstation::all();
            //  $categ= \Session::get('category_ppds');
            $dist = \Session::get('districtcd_ppds');
            $filtered = tbl_pollingstation::join('zone', 'zone.zone', '=', 'pollingstation.forzone')
                            ->join('assembly', 'assembly.assemblycd', '=', 'pollingstation.forassembly')
                            ->Leftjoin('dcrcmaster', 'dcrcmaster.dcrcgrp', '=', 'pollingstation.dcrccd')
                            ->select('pollingstation.psno', 'pollingstation.forassembly', 'pollingstation.member', 'pollingstation.psname', 'pollingstation.gender', 'zone.zonename', 'assembly.assemblyname', 'dcrcmaster.dc_date', 'dcrcmaster.dc_time', 'dcrcmaster.dc_venue', 'pollingstation.forzone', 'pollingstation.groupid', 'pollingstation.psfix')
                            ->where(\DB::raw('SUBSTRING(pollingstation.forzone,1,2)'), '=', $dist)
                            // ->orderBy('dcrcmaster.dcrcgrp')
                            ->where(function($q) use ($search) {
                                $q->orwhere('forassembly', 'like', '%' . $search . '%')
                                ->orwhere('psno', 'like', '%' . $search . '%')
                                ->orwhere('psname', 'like', '%' . $search . '%')
                                ->orwhere('assemblyname', 'like', '%' . $search . '%')
                                ->orwhere('groupid', 'like', '%' . $search . '%');
                            })->orderBy('pollingstation.psno');
            if ($forZone != '') {
                $filtered = $filtered->where('pollingstation.forzone', '=', $forZone);
            }
            if ($phase != '') {
                $filtered = $filtered->where('pollingstation.phase', '=', $phase);
            }
            if ($dcvenue != '') {
                $filtered = $filtered->where('pollingstation.dcrccd', '=', $dcvenue);
            }
            if ($assembly != '') {
                $filtered = $filtered->where('pollingstation.forassembly', '=', $assembly);
            }


//                            ->where('category','>=',$categ);
//                    if($categ!=0){
//                           $filtered=$filtered->where('districtcd','=',$dist);
//                    }
            $ordered = $filtered;
            $filtered_count = $filtered->count();
            //echo count ( $order );die;
            for ($i = 0; $i < count($order); $i ++) {
                $ordered = $ordered->orderBy($request->columns [$order [$i] ['column']] ['data'], strtoupper($order [$i] ['dir']));
            }
            $page_displayed = $ordered->offset($offset)->limit($length)->get();
            $data = array();
            if (!empty($page_displayed)) {
                foreach ($page_displayed as $poll_mars) {
                    //$cat_arr = config('constants.USER_CATEGORY');

                    $nestedData['psno_prefix'] = $poll_mars->psno . '' . $poll_mars->psfix;
                    $nestedData['psfix'] = $poll_mars->psfix;
                    $nestedData['psno'] = $poll_mars->psno;
                    $nestedData['zonename'] = $poll_mars->zonename;
                    $nestedData['assemblyname'] = $poll_mars->forassembly . '-' . $poll_mars->assemblyname;
                    $nestedData['forassembly'] = $poll_mars->forassembly;
                    $nestedData['psname'] = $poll_mars->psname;
                    if ($poll_mars->gender == 'M') {
                        $nestedData['gender'] = 'Male';
                    } else {
                        $nestedData['gender'] = 'Female';
                    }
                    $nestedData['member'] = $poll_mars->member;
                    $nestedData['groupid'] = $poll_mars->groupid;
                    $nestedData['forzone'] = $poll_mars->forzone;
                    if ($poll_mars->dc_venue != "") {
                        $nestedData['dc_venue'] = $poll_mars->dc_venue . '-' . date('d/m/Y', strtotime(trim(str_replace('/', '-', $poll_mars->dc_date)))) . '-' . $poll_mars->dc_time;
                    } else {
                        $nestedData['dc_venue'] = "";
                    }
                    $edit_button = $delete_button = $nestedData['psno'] . '/' . $nestedData['psfix'] . '/' . $nestedData['forassembly'] . '/' . $nestedData['member'] . '/' . $nestedData['gender'] . '/' . $nestedData['groupid'];

                    $nestedData['action'] = array('e' => $edit_button, 'd' => $delete_button);
                    $data[] = $nestedData;
                }
            }
            $response = array(
                "draw" => $draw,
                "recordsTotal" => $polling->count(), //Should be changed #7
                "recordsFiltered" => $filtered_count,
                'polling_masters' => $data //Should be changed #8
            );
        } catch (\Exception $e) {
            $response = array(
                'exception' => true,
                'exception_message' => $e->getMessage()
            );
            $statusCode = 400;
        } finally {
            return response()->json($response, $statusCode);
        }
    }

    public function polling_modify(Request $request) {
        $statusCode = 200;
//         $this->validate($request, [
//            'data' => 'required|alpha_num',
//            
//                ], [
//            
//            'data.required' => 'data is Required',
//            'data.alpha_num' => 'data may only contain letters and numbers',
//                  
//        ]);
        $all_value = $request->data;
        $data_all = explode("/", $all_value);
        $psno = $data_all[0];
        $psfix = $data_all[1];
        $forassembly = $data_all[2];
        $member = $data_all[3];
        $gender = $data_all[4];
        if ($gender == 'Male') {
            $gender_chk = 'M';
        } else {
            $gender_chk = 'F';
        }
        $groupid = $data_all[5];
        $data = tbl_pollingstation::
                        join('zone', 'zone.zone', '=', 'pollingstation.forzone')
                        ->select('pollingstation.psno', 'pollingstation.psfix', 'pollingstation.forassembly', 'pollingstation.member', 'pollingstation.psname', 'pollingstation.gender', 'pollingstation.forzone', 'pollingstation.groupid', 'zone.zonename')
                        ->where('pollingstation.psno', '=', $psno)
                        ->where('pollingstation.forassembly', '=', $forassembly)
                        ->where('pollingstation.gender', '=', $gender_chk)
                        ->where('pollingstation.psfix', '=', $psfix)
                        ->where('pollingstation.member', '=', $member)->get();

        $response = [
            //'count'=>$check_count,
            'rec' => $data
        ];
        return response()->json($response, $statusCode);
    }

    public function update_polling(Request $request) {
        $statusCode = 200;
        $update = null;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in form submit.');
            return response()->json($response, $statusCode);
        }
        
        echo 
        $this->validate($request, [
            'ps_no' => 'required|digits_between:1,4',
            'forzone_code' => 'required|alpha_num|min:4|max:4',
            'assem_code' => 'required|alpha_num|min:3|max:3',
            'ps_name' => 'required|regex:/^[A-Za-z0-9\s(),\-.]+$/i|max:150',
            'mem_ber' => 'required|digits:1',
            'gen_der' => 'required|alpha|max:6|min:4'
                // 'dcrcgrp_code' => 'required|alpha_num',
                ], [
            'ps_no.required' => 'PS no is Required',
            'ps_no.integer' => 'PS no should be integer',
            'forzone_code.required' => 'Zone no is Required',
            'forzone_code.alpha_num' => 'Zone should be integer',
            'ps_name.required' => 'Polling station name is required',
            'ps_name.regex' => 'Polling station contain alphbatical Characters',
            'assem_code.required' => 'Asembly code is Required',
            'assem_code.alpha_num' => 'Asembly code should be alpha numeric',
            'mem_ber.required' => 'Member is required',
            'mem_ber.integer' => 'Member should be integer',
            'gen_der.required' => 'Gender is required',
            'ps_name.max' => 'Polling station name may not be greater than 150 characters'
        ]);
        try {
//echo $request->psfix;die;
            if ($request->gen_der == 'Male') {
                $gender_chk = 'M';
            } else {
                $gender_chk = 'F';
            }
            $psfix_t = $request->ps_code;
            if ($psfix_t == '') {
                $psfix_chk = '';
            } else {
                $psfix_chk = $psfix_t;
            }
            $asm_code = $request->assem_code;
            $member = $request->mem_ber;
            $psno = $request->ps_no;
            $psname = $request->ps_name;
            $update_dcrc = tbl_pollingstation::where('forassembly', '=', $asm_code)
                    ->where('member', '=', $member)
                    ->where('psno', '=', $psno)
                    ->where('gender', '=', $gender_chk)
                    ->where('psfix', '=', $psfix_chk)
                    ->update(['psname' => $psname]);

            $response = array(
                'status' => 1
            );
        } catch (\Exception $e) {
            $response = array(
                'exception' => true,
                'exception_message' => $e->getMessage(),
            );
            $statusCode = 400;
        } finally {
            return response()->json($response, $statusCode);
        }
    }

    public function delete_polling_chking(Request $request) {
        $statusCode = 200;

        $this->validate($request, [
            'data' => 'required|regex:/^[A-Za-z0-9\s\/\(\)]+$/i|max:21',
                ], [
            'data.required' => 'Data is required To Delete',
            'data.regex' => 'Special Character not allow in Data',
        ]);
        try {
            $all_value = $request->data;
            $data_all = explode("/", $all_value);
            
            $psno = $data_all[0];
            $psfix = $data_all[1];
            $forassembly = $data_all[2];
            $member = $data_all[3];
            $gender = $data_all[4];
            if ($gender == 'Male') {
                $gender_chk = 'M';
            } else {
                $gender_chk = 'F';
            }
            $groupid = $data_all[5];
            $record = tbl_pollingstation::where('pollingstation.psno', '=', $psno)
                            ->where('pollingstation.forassembly', '=', $forassembly)
                            ->where('pollingstation.gender', '=', $gender_chk)
                            ->where('pollingstation.psfix', '=', $psfix)
                            ->where(function($q) {
                                $q->orwhereNotNull('groupid')
                                  ->orwhere('groupid','!=','');})
                            ->where('pollingstation.member', '=', $member)->get(); //Should be changed #27
            $record_count = $record->count();
            $response = array(
                'count' => $record_count
                    //Should be changed #32
            );
        } catch (\Exception $e) {
            $response = array(
                'exception' => true,
                'exception_message' => $e->getMessage(),
            );
            $statusCode = 400;
        } finally {
            return response()->json($response, $statusCode);
        }
    }

    public function delete_polling(Request $request) {
        $statusCode = 200;

        $this->validate($request, [
            'data' => 'required|regex:/^[A-Za-z0-9\s\/\(\)]+$/i|max:21',
                ], [
            'data.required' => 'Data is required To Delete',
            'data.regex' => 'Special Character not allow in Data',
        ]);
        try {
            $all_value = $request->data;
            echo $all_value;
            exit();
            $data_all = explode("/", $all_value);
            $psno = $data_all[0];
            $psfix = $data_all[1];
            $forassembly = $data_all[2];
            $member = $data_all[3];
            $gender = $data_all[4];

            $con_cat = $psno . '' . $psfix;
            // print_r($con_cat);die;
            if ($gender == 'Male') {
                $gender_chk = 'M';
            } else {
                $gender_chk = 'F';
            }
            // $psfix_t=$request->ps_code;

            $groupid = $data_all[5];


            $record = tbl_pollingstation::
                    where('pollingstation.psno', '=', $psno)
                    ->where('pollingstation.forassembly', '=', $forassembly)
                    ->where('pollingstation.gender', '=', $gender_chk)
                    ->where('pollingstation.psfix', '=', $psfix)
                    ->where('pollingstation.member', '=', $member);


            if (!empty($record)) {//Should be changed #30
                $record = $record->delete();
            }

            $response = array(
                'record' => $record,
                'status' => 2//Should be changed #32
            );
        } catch (\Exception $e) {
            $response = array(
                'exception' => true,
                'exception_message' => $e->getMessage(),
            );
            $statusCode = 400;
        } finally {
            return response()->json($response, $statusCode);
        }
    }

}
