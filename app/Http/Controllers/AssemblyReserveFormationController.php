<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\tbl_assembly_zone;
use App\tbl_reserve;
use App\tbl_personnela;
use DB;

class AssemblyReserveFormationController extends Controller {

    public function getAssemblyPartyDetails(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            $this->validate($request, [
                'forZone' => 'required|alpha_num|min:4|max:4'
                    ], [
                'forZone.required' => 'For Zone is required',
                'forZone.alpha_num' => 'For Zone must be an alpha numeric'
            ]);
            try {
                $forZone = $request->forZone;
                $filtered = '';
                $filtered = tbl_assembly_zone::where('zone', '=', $forZone)
                        ->join('assembly', 'assembly.assemblycd', '=', 'assembly_zone.assemblycd')
                        ->Rightjoin('assembly_party', 'assembly_party.assemblycd', '=', 'assembly_zone.assemblycd')
                        ->select('assembly_party.assemblycd', 'assembly.assemblyname', 'assembly_party.no_of_member', 'assembly_party.gender', 'assembly_party.no_party')
                        ->get();
                $data = array();
                if (!empty($filtered)) {
                    foreach ($filtered as $post) {
                        $nestedData['assemblycd'] = $post->assemblycd;
                        $nestedData['assemblyname'] = $post->assemblyname;
                        $nestedData['gender'] = $post->gender;
                        $nestedData['no_of_member'] = $post->no_of_member;
                        $nestedData['no_party'] = $post->no_party;

                        $partyReserve = tbl_reserve::where('forassembly', '=', $post->assemblycd)
                                ->where('number_of_member', '=', $post->no_of_member)
                                ->where('gender', '=', $post->gender)
//                                ->where('forzone', '=', $forZone)
                                ->select(DB::raw('count(*) as cnt'))
                                ->get();
                        $nestedData['reserveStatus'] = $partyReserve[0]->cnt;
                        $data[] = $nestedData;
                    }
                }
                $response = array(
                    'options' => $data,
                    'status' => 1);
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }

    public function assemblyReserveForm(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            $validate_array = ['forZone' => 'required|alpha_num|min:4|max:4',
                'type' => 'required|alpha|min:1|max:1',
                'pr' => 'required|digits_between:1,4',
                'p1' => 'required|digits_between:1,4',
                'p2' => 'required|digits_between:1,4',
                'p3' => 'required|digits_between:1,4',
                'pa' => 'nullable|digits_between:1,4',
                'pb' => 'nullable|digits_between:1,4'];
            for ($x = 0; $x <= $request->row_count; $x++) {
                $validate_array['myCheck' . $x] = 'nullable|alpha_num|min:5|max:5';
            }
            $validate_array1 = ['forZone.required' => 'Zone is required',
                'forZone.alpha_num' => 'Zone must be an alpha numeric characters',
                'type.required' => 'Type is required',
                'type.alpha' => 'Type must be an alpha characters',
                'pr.required' => 'PR is required',
                'pr.digits_between' => 'PR must be an integer and length should not be greater than 4',
                'p1.required' => 'P1 is required',
                'p1.digits_between' => 'P1 must be an integer and length should not be greater than 4',
                'p2.required' => 'P2 is required',
                'p2.digits_between' => 'P2 must be an integer and length should not be greater than 4',
                'p3.required' => 'P3 is required',
                'p3.digits_between' => 'P3 must be an integer and length should not be greater than 4',
                'pa.digits_between' => 'PA must be an integer and length should not be greater than 4',
                'pb.digits_between' => 'PB must be an integer and length should not be greater than 4'];
            for ($y = 0; $y <= $request->row_count; $y++) {
                $validate_array1['myCheck' . $y . '.alpha_num'] = 'Assembly code must be an alpha numeric characters';
            }
            $this->validate($request, $validate_array, $validate_array1);
            try {
                $user_code = session()->get('code_ppds');
                $row_count = $request->row_count;
                $forZone = $request->forZone;
                $type = $request->type;
                $PR = $request->pr;
                $P1 = $request->p1;
                $P2 = $request->p2;
                $P3 = $request->p3;
                $PA = $request->pa;
                $PB = $request->pb;
                $count = 0;
                $countPA = 0;
                $countPB = 0;
                $status = 0;
                for ($i = 1; $i <= $row_count; $i++) {
                    $asmID = 'myCheck' . $i;
                    if ($request->$asmID != "") {
                        $asmstat_id = $request->$asmID;
                        $member = substr($asmstat_id, 3, 1);
                        if ($member == "5") {
                            if ($PA == "" || $PA == "0") {
                                $countPA++;
                            }
                        } else if ($member == "6") {
                            if ($PA == "" || $PA == "0") {
                                $countPA++;
                            }
                            if ($PB == "" || $PB == "0") {
                                $countPB++;
                            }
                        }
                    }
                }
                switch (1) {
                    case ($countPA == 0 && $countPB == 0):
                        for ($j = 1; $j <= $row_count; $j++) {
                            $asmPartyID = 'myCheck' . $j;
                            if ($request->$asmPartyID != "") {
                                $asm_party_id = $request->$asmPartyID;
                                $assembly = substr($asm_party_id, 0, 3);
                                $party_member = substr($asm_party_id, 3, 1);
                                $gender = substr($asm_party_id, 4, 1);

                                $tbl_reserve_delete = new tbl_reserve();
                                $tbl_reserve_delete->where('forassembly', $assembly)
                                        ->where('number_of_member', $party_member)
                                        ->where('gender', $gender)
                                        ->delete();
                                switch ($party_member) {
                                    case ($party_member == 4):
                                        for ($k = 1; $k <= $party_member; $k++) {
                                            $poststat = "";
                                            $numb = "";
                                            if ($k == 1) {
                                                $poststat = "P1";
                                                $numb = $P1;
                                            } else if ($k == 2) {
                                                $poststat = "P2";
                                                $numb = $P2;
                                            } else if ($k == 3) {
                                                $poststat = "P3";
                                                $numb = $P3;
                                            } else {
                                                $poststat = "PR";
                                                $numb = $PR;
                                            }
                                            $tbl_reserve = new tbl_reserve();
                                            $tbl_reserve->forassembly = $assembly;
                                            $tbl_reserve->number_of_member = $party_member;
                                            $tbl_reserve->gender = $gender;
                                            $tbl_reserve->no_or_pc = $type;
                                           // $tbl_reserve->forzone = $forZone;
                                            $tbl_reserve->usercode = $user_code;
                                            $tbl_reserve->poststat = $poststat;
                                            $tbl_reserve->numb = $numb;

                                            $tbl_reserve->save();
                                        }
                                        break;
                                    case ($party_member == 5):
                                        for ($k = 1; $k <= $party_member; $k++) {
                                            $poststat = "";
                                            $numb = "";
                                            if ($k == 1) {
                                                $poststat = "P1";
                                                $numb = $P1;
                                            } else if ($k == 2) {
                                                $poststat = "P2";
                                                $numb = $P2;
                                            } else if ($k == 3) {
                                                $poststat = "P3";
                                                $numb = $P3;
                                            } else if ($k == 4) {
                                                $poststat = "PR";
                                                $numb = $PR;
                                            } else {
                                                $poststat = "PA";
                                                $numb = $PA;
                                            }
                                            $tbl_reserve = new tbl_reserve();
                                            $tbl_reserve->forassembly = $assembly;
                                            $tbl_reserve->number_of_member = $party_member;
                                            $tbl_reserve->gender = $gender;
                                            $tbl_reserve->no_or_pc = $type;
                                           // $tbl_reserve->forzone = $forZone;
                                            $tbl_reserve->usercode = $user_code;
                                            $tbl_reserve->poststat = $poststat;
                                            $tbl_reserve->numb = $numb;

                                            $tbl_reserve->save();
                                        }
                                        break;
                                    case ($party_member == 6):
                                        for ($k = 1; $k <= $party_member; $k++) {
                                            $poststat = "";
                                            $numb = "";
                                            if ($k == 1) {
                                                $poststat = "P1";
                                                $numb = $P1;
                                            } else if ($k == 2) {
                                                $poststat = "P2";
                                                $numb = $P2;
                                            } else if ($k == 3) {
                                                $poststat = "P3";
                                                $numb = $P3;
                                            } else if ($k == 4) {
                                                $poststat = "PR";
                                                $numb = $PR;
                                            } else if ($k == 5) {
                                                $poststat = "PA";
                                                $numb = $PA;
                                            } else {
                                                $poststat = "PB";
                                                $numb = $PB;
                                            }
                                            $tbl_reserve = new tbl_reserve();
                                            $tbl_reserve->forassembly = $assembly;
                                            $tbl_reserve->number_of_member = $party_member;
                                            $tbl_reserve->gender = $gender;
                                            $tbl_reserve->no_or_pc = $type;
                                            //$tbl_reserve->forzone = $forZone;
                                            $tbl_reserve->usercode = $user_code;
                                            $tbl_reserve->poststat = $poststat;
                                            $tbl_reserve->numb = $numb;

                                            $tbl_reserve->save();
                                        }
                                        break;
                                    default:
                                        break;
                                }
                            }
                        }
                        $status = 0;
                        break;
                    case ($countPA > 0 && $countPB > 0):
                        $status = 1;
                        break;
                    case ($countPA == 0 && $countPB > 0):
                        $status = 2;
                        break;
                    case ($countPA > 0 && $countPB == 0):
                        $status = 3;
                        break;
                    default:
                        break;
                }
                $response = array(
                    'options' => $status,
                    'status' => 1);
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }

    public function assem_party_reserve_list_datatable(Request $request) {
        $response = [];
        $perm = null;
        $statusCode = 200;
        $users = array(); //Should be changed #4
        $search_val = array();
        $this->validate($request, [
            'search.*' => 'nullable|regex:/^[A-Za-z0-9\s]+$/i',
            'draw' => 'required|integer',
            'start' => 'required|integer',
            'length' => 'required|integer',
            'zone' => 'nullable|alpha_num|min:4|max:4',
            'assembly' => 'nullable|alpha_num|min:3|max:3',
                ], [
            'search.*.regex' => 'Special Charecters not allowed',
            'draw.required' => 'Draw is required',
            'start.integer' => 'Start must be an integer',
            'start.required' => 'Start is required',
            'length.integer' => 'Length must be an integer',
            'length.required' => 'Length is required',
            'zone.alpha_num' => 'Zone code must be an alpha numeric',
            'assembly.alpha_num' => 'Assembly code must be an alpha numeric'
        ]);
        try {

            $draw = $request->draw;
            $offset = $request->start;
            $length = $request->length;
            $search = $request->search ["value"];
            $order = $request->order;
            $zone = $request->zone;
            $assembly = $request->assembly;
            //print_r($order);die;
            $categ = \Session::get('category_ppds');
            $dist = \Session::get('districtcd_ppds');
            $tbl_reserve = tbl_reserve::all();
            $filtered = tbl_assembly_zone::where('assembly_zone.districtcd', '=', $dist)
                        ->join('assembly', 'assembly.assemblycd', '=', 'assembly_zone.assemblycd')
                        ->Rightjoin('assembly_party', 'assembly_party.assemblycd', '=', 'assembly_zone.assemblycd')
                        ->Rightjoin('reserve', function($join) {
                            $join->on('assembly_party.assemblycd', '=', 'reserve.forassembly');
                            $join->on('assembly_party.no_of_member', '=', 'reserve.number_of_member');
                            $join->on('assembly_party.gender', '=', 'reserve.gender');
                        })->join('zone', 'zone.zone', '=', 'assembly_zone.zone') 
                        ->select('assembly_party.no_party', 'reserve.number_of_member', 'reserve.gender', 'zone.zonename', 'assembly.assemblyname', 'reserve.forassembly', 'reserve.no_or_pc', 'reserve.numb', 'reserve.poststat')
                        ->where(function($q) use ($search) {
                            $q->orwhere('no_party', 'like', '%' . $search . '%')
                            ->orwhere('number_of_member', 'like', '%' . $search . '%')
                            ->orwhere('forassembly', 'like', '%' . $search . '%');
                        });
                if ($zone != '') {
                    $filtered = $filtered->where('assembly_zone.zone', '=', $zone);
                }
                if ($assembly != '') {
                    $filtered = $filtered->where('reserve.forassembly', '=', $assembly);
                }
//            $filtered = tbl_reserve::join('assembly_party', function($join) {
//                        $join->on('assembly_party.assemblycd', '=', 'reserve.forassembly');
//                        $join->on('assembly_party.no_of_member', '=', 'reserve.number_of_member');
//                        $join->on('assembly_party.gender', '=', 'reserve.gender');
//                    })->join('zone', 'zone.zone', '=', 'reserve.forzone')
//                    ->join('assembly', 'assembly.assemblycd', '=', 'reserve.forassembly')
//                    ->where(\DB::raw('SUBSTRING(reserve.forzone,1,2)'), '=', $dist)
//                    ->select('assembly_party.no_party', 'reserve.number_of_member', 'reserve.gender', 'zone.zonename', 'assembly.assemblyname', 'reserve.forassembly', 'reserve.no_or_pc', 'reserve.numb', 'reserve.poststat')
//                    ->where(function($q) use ($search) {
//                        $q->orwhere('no_party', 'like', '%' . $search . '%')
//                        ->orwhere('number_of_member', 'like', '%' . $search . '%')
//                        ->orwhere('forassembly', 'like', '%' . $search . '%');
//                    });
//
//            if ($zone != '') {
//                $filtered = $filtered->where('reserve.forzone', '=', $zone);
//            }
//            if ($assembly != '') {
//                $filtered = $filtered->where('reserve.forassembly', '=', $assembly);
//            }
            $ordered = $filtered;
            $filtered_count = $filtered->count();
            //echo count ( $order );die;
            for ($i = 0; $i < count($order); $i ++) {
                $ordered = $ordered->orderBy($request->columns [$order [$i] ['column']] ['data'], strtoupper($order [$i] ['dir']));
            }
            $page_displayed = $ordered->offset($offset)->limit($length)
                    ->get();
            $data = array();
            if (!empty($page_displayed)) {
                foreach ($page_displayed as $assm) {

                    $nestedData['zonename'] = $assm->zonename;
                    $nestedData['forassembly'] = $assm->forassembly;
                    $nestedData['assemblyname'] = $assm->assemblyname;
                    $nestedData['number_of_member'] = $assm->number_of_member;
                    if ($assm->gender == 'M') {
                        $nestedData['gender'] = 'Male';
                    } else {
                        $nestedData['gender'] = 'Female';
                    }

                    $nestedData['no_party'] = $assm->no_party;
                    $nestedData['no_or_pc'] = $assm->no_or_pc;
                    $nestedData['numb'] = $assm->numb;
                    $nestedData['poststat'] = $assm->poststat;
                    $edit_button = $nestedData['forassembly'] . '/' . $nestedData['number_of_member'] . '/' . $nestedData['gender'] . '/' . $nestedData['no_party'] . '/' . $nestedData['no_or_pc'] . '/' . $nestedData['numb'] . '/' . $nestedData['poststat'];

                    $nestedData['action'] = array('e' => $edit_button);
                    $data[] = $nestedData;
                }
            }
            //  echo 'hi';die;
            $response = array(
                "draw" => $draw,
                "recordsTotal" => $tbl_reserve->count(), //Should be changed #7
                "recordsFiltered" => $filtered_count,
                'reserve_part_form' => $data //Should be changed #8
            );
        } catch (\Exception $e) {
            $response = array(
                'exception' => true,
                'exception_message' => $e->getMessage()
            );
            $statusCode = 400;
        } finally {
            return response()->json($response, $statusCode);
        }
    }

    public function check_for_edit_reserve(Request $request) {
        $statusCode = 200;
        $all_value = $request->data;
        //   print_r($all_value);die;
        $data_all = explode("/", $all_value);

        $assemblycd = $data_all[0];
        $no_of_member = $data_all[1];
        $gender = $data_all[2];
        $no_party = $data_all[3];
        $tbl_personnela = tbl_personnela::where('gender', '=', $gender)->where('no_of_member', '=', $no_of_member)->where('forassembly', '=', $assemblycd)->get();
        $count = $tbl_personnela->count();
        $response = array(
            'count' => $count,
        );
        return response()->json($response, $statusCode);
    }

    public function edit_reserve_party_form(Request $request) {
        $statusCode = 200;
        $update = null;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in form submit.');
            return response()->json($response, $statusCode);
        }
        $response = [
            'first_train' => [] //Should be changed #9
        ];

        $this->validate($request, [
            'assem_code' => 'required|alpha_num|min:3|max:3',
            'mem_ber' => 'required|digits_between:1,1',
            'post_status' => 'required|regex:/^[A-Za-z0-9\s]+$/i|min:2|max:2',
            'gen_der' => 'required|alpha|max:6',
            'type' => 'required|min:1|max:1',
            'num_ber' => 'required|digits_between:1,4',
                // 'dcrcgrp_code' => 'required|alpha_num',
                ], [
            'assem_code.required' => 'Asembly code is Required',
            'assem_code.alpha_num' => 'Assembly code must be an alpha numeric',
            'mem_ber.required' => 'Member is required',
            'mem_ber.digits_between' => 'Member should be integer',
            'post_status.required' => 'Post status is required',
            'post_status.regex' => 'Post status should be alphanumerical characters',
            'gen_der.required' => 'Gender is required',
            'type.required' => 'Type is required',
            'num_ber.required' => 'Number is required',
            'num_ber.digits_between' => 'Number must be integer and length should not be greater than 4 characters'        
        ]);
        try {

            if ($request->gen_der == 'Male') {
                $gender_chk = 'M';
            } else {
                $gender_chk = 'F';
            }
            $assembly = $request->assem_code;
            $mem_ber = $request->mem_ber;
            $post_status = $request->post_status;
            $type = $request->type;
            $num_ber = $request->num_ber;
            
            $update_dcrc = tbl_reserve::where('forassembly', '=', $assembly)
                    ->where('number_of_member', '=', $mem_ber)
                    ->where('poststat', '=', $post_status)
                    ->where('gender', '=', $gender_chk)
                    ->update(['no_or_pc' => $type, 'numb' => $num_ber]);
            $response = array(
                'status' => 1,
            );
        } catch (\Exception $e) {
            $response = array(
                'exception' => true,
                'exception_message' => $e->getMessage(),
            );
            $statusCode = 400;
        } finally {
            return response()->json($response, $statusCode);
        }
    }

}
