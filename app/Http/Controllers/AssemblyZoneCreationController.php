<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\tbl_assembly_zone;
use App\tbl_assembly_district;
use App\tbl_personnela;
use DB;

class AssemblyZoneCreationController extends Controller
{
   public function getAssemblyZone(Request $request) { 
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        }
        else
        {
            $this->validate($request, [            
            'forDist'=> 'required|alpha_num|min:2|max:2'
            ], [           
            'forDist.required' => 'District is required',
            'forDist.alpha_num' => 'District must be an alpha numeric characters'
            ]);
            $forDist=$request->forDist;
            try{
            $filtered="";
            $filtered = tbl_assembly_district::join('assembly', 'assembly.assemblycd', '=', 'assembly_district.assemblycd')
                ->leftjoin('assembly_zone', 'assembly_district.assemblycd', '=', 'assembly_zone.assemblycd')
                ->leftjoin('zone', 'zone.zone', '=', 'assembly_zone.zone')
                ->leftjoin('phase', 'phase.code', '=', 'assembly_zone.phase_id')
                ->join('subdivision', 'subdivision.subdivisioncd', '=', 'assembly_district.subdivisioncd')
                ->where('assembly_district.districtcd', '=', $forDist) 
    		->select('assembly_district.assemblycd', 'zone.zonename','assembly.assemblyname','subdivision.subdivision','phase.name')
                ->orderBy('assembly_district.assemblycd')
                ->get();
            
                $response = array(
                   'options' => $filtered,
                   'status' => 1 
                );
           
            }catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
              $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        
        }
    }
   public function createAssemblyZone(Request $request) { 
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        }
        else
        {
            $validate_array=['zone' => 'required|alpha_num|min:4|max:4',
            'phase' => 'required|integer'
        ];
           
            for($x=0; $x<=$request->row_count; $x++) {
                $validate_array['myCheck'. $x] = 'nullable|alpha_num|unique:assembly_zone,assemblycd|min:3|max:3';
            }
            $validate_array1=['zone.required' => 'Zone is required',
                                'zone.alpha_num' => 'Zone must be an alpha numeric characters',
                                'phase.required'=>'Phase is required',
                                'phase.integer'=>'Invalid phase select'
                            ];
            for($y=0; $y<=$request->row_count; $y++) {
                $validate_array1['myCheck'. $y.'.alpha_num'] = 'Assembly must be an alpha numeric characters';
                $validate_array1['myCheck'. $y.'.unique'] = 'Already created some assembly(s) for this zone';
            }
            $this->validate($request,$validate_array,$validate_array1);
            
            try{
             $row_count=$request->row_count;
             $zone=$request->zone;
             $phase=$request->phase;
             $user_code = session()->get('code_ppds');
             $count=0;
             for($i=1;$i<=$row_count;$i++){
                    $asmcd='myCheck'.$i;                   
                    if($request->$asmcd!=""){
                     $asmstat=$request->$asmcd;
                     
                   //  DB::enableQueryLog();
                      $rec_count=DB::select('call assemblyWiseZoneCreation(?,?,?,?)',[$asmstat,$zone,$phase,$user_code]);
                    //  dd(DB::getQueryLog());
                      $pcount=$rec_count[0]->t_Count;
                      if($pcount==1){
                         $count++; 
                      }
                    }
                                
              }
            
                $response = array(
                   'options' => $count,
                   'status' => 1 
                );
           
            }catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
              $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        
        }
    }
   public function deleteAssemblyZone(Request $request) { 
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        }
        else
        {
            $validate_array=['zone' => 'nullable|alpha_num|min:4|max:4'];
            for($x=0; $x<=$request->row_count; $x++) {
                $validate_array['myCheck'. $x] = 'nullable|alpha_num|min:3|max:3';
            }
            $validate_array1=['zone.alpha_num' => 'Zone must be an alpha numeric characters'];
            for($y=0; $y<=$request->row_count; $y++) {
                $validate_array1['myCheck'. $y.'.alpha_num'] = 'Assembly must be an alpha numeric characters';
            }
            $this->validate($request,$validate_array,$validate_array1);
            
            try{
             $row_count=$request->row_count;
             $zone=$request->zone;
             $count=0;
             $count1=0;
             for($i=1;$i<=$row_count;$i++){
                    $asmcd='myCheck'.$i;                   
                    if($request->$asmcd!=""){
                     $asmstat=$request->$asmcd;
                     $p_count=tbl_personnela::where('forassembly', $asmstat)->value(DB::raw('count(personcd)'));
                     if($p_count>0){
                        $count1++; 
                     }else{
                        $res_count=tbl_assembly_zone::where('assemblycd', $asmstat)->delete();
                         if($res_count==1){
                            $count++; 
                         }
                     }
                }
                                
              }
            
                $response = array(
                   'options' => $count,
                   'options1' => $count1,
                   'status' => 1 
                );
           
            }catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
              $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        
        }
    }


    public function updateAssemblyZone(Request $request){ //dd($request->all());
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        }
        else
        {
            $validate_array=['zone' => 'nullable|alpha_num|min:4|max:4'];
            for($x=0; $x<=$request->row_count; $x++) {
                $validate_array['myCheck'. $x] = 'nullable|alpha_num|min:3|max:3';
            }
            $validate_array1=['zone.alpha_num' => 'Zone must be an alpha numeric characters'];
            for($y=0; $y<=$request->row_count; $y++) {
                $validate_array1['myCheck'. $y.'.alpha_num'] = 'Assembly must be an alpha numeric characters';
            }
            $this->validate($request,$validate_array,$validate_array1);
            
            try{
             $row_count=$request->row_count;
             $zone=$request->zone;
             $phase=$request->phase;
             $count=0;
             $count1=0;
             for($i=1;$i<=$row_count;$i++){
                    $asmcd='myCheck'.$i;                   
                    if($request->$asmcd!=""){
                     $asmstat=$request->$asmcd;
                   
                        $res_count=tbl_assembly_zone::where('assemblycd', $asmstat)
                        ->where('zone', $zone)->update([
                            'phase_id'=>$phase
                        ]);
                         if($res_count==1){
                            $count++; 
                         }
                     
                }
                                
              }
            
                $response = array(
                   'options' => $count,
                   'options1' => $count1,
                   'status' => 1 
                );
           
            }catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
              $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        
        }
    }
}
