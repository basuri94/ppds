<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\tbl_training_type;
use App\tbl_first_training_date_time;
use App\tbl_first_training_venue;
use App\tbl_first_training_subvenue;
use App\tbl_subdivision;
use App\tbl_first_training_schedule;
use App\tbl_assembly_zone;
use App\tbl_personnela;
use DB;

class FirstTrainingAllocationController extends Controller {

    public function get_used_postat_first_training($schedule_cd, $postat, $training_type) {
        $tr_array = config('constants.PP_TRAINING');
        $tr_Name = $tr_array[$training_type];
        $result = tbl_personnela::where($tr_Name, '=', $schedule_cd)->where('poststat', '=', $postat)->get();
        $res_count = $result->count();
        return $res_count;
    }
    

    public function getData() {
        //$training_datetime= $this->getTrainingDateTime();
        $training_type = $this->getTrainingTypeData();
        return view('first_training_allocation', compact('training_type'));
    }

    public function getDatafor_list() {
        //$training_datetime= $this->getTrainingDateTime();
        $training_type = $this->getTrainingTypeData();
        return view('first_training_venue_allocation_list', compact('training_type'));
    }

    public function getMaxCPData() {
        $training_type = $this->getTrainingTypeData();
        return view('first_training_allocation_maxcp', compact('training_type'));
    }

    public function getTrainingGroupData() {
        $desc = tbl_training_type::groupBy('training_group')
                      ->pluck('training_group', 'training_group_cd')->all();
        $data = array('desc' => $desc);
        return $data;
    }
    public function getTrainingTypeData() {
        $desc = tbl_training_type::where('status','1')->pluck('training_desc', 'training_code')->all();
       
        $data = array('desc' => $desc);
        return $data;
    }
    public function getZonewiseSubdivisionData(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            $this->validate($request, [
                'zone' => 'required|alpha_num|min:4|max:4'
                    ], [
                'zone.required' => 'Zone is required',
                'zone.alpha_num' => 'Zone must be an alpha numeric characters'
            ]);
            $zone = $request->zone;

            try {
                $filtered = "";
                $tbl_assembly_zone = new tbl_assembly_zone();
                $filtered = $tbl_assembly_zone
                                ->join('subdivision', 'subdivision.subdivisioncd', '=', 'assembly_zone.subdivisioncd')
                                ->where('zone', '=', $zone)
                                ->groupBy('assembly_zone.subdivisioncd')
                                ->pluck('subdivision.subdivision', 'subdivision.subdivisioncd')->all();
                $response = array(
                    'options' => $filtered,
                    'status' => 1
                );
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }

    public function getZonewiseSubdivisionall_Data(Request $request)
    {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            $this->validate($request, [
                'zone' => 'required|alpha_num|min:4|max:4'
                    ], [
                'zone.required' => 'Zone is required',
                'zone.alpha_num' => 'Zone must be an alpha numeric characters'
            ]);
            $zone = $request->zone;
            $forDist = $request->forDist;
            
            try {
                $filtered = "";
              
                $tbl_subdivision = new tbl_subdivision();
                $filtered = $tbl_subdivision
                                ->leftjoin('zone', 'zone.districtcd', '=', 'subdivision.districtcd')
                                ->where('zone.zone', '=', $zone)
                                ->where('subdivision.districtcd', '=', $forDist)
                                ->groupBy('subdivision.subdivisioncd')
                                ->pluck('subdivision.subdivision', 'subdivision.subdivisioncd')->all();
                $response = array(
                    'options' => $filtered,
                    'status' => 1
                );
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }

    public function getTrainingDateTime(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            $this->validate($request, [
                'forDist' => 'required|alpha_num|min:2|max:2'
                    ], [
                'forDist.required' => 'District is required',
                'forDist.alpha_num' => 'District must be an alpha numeric'
            ]);
            try {
                $forDist = $request->forDist;
                $list = '';
                $trainingData = tbl_first_training_date_time::where('districtcd', '=', $forDist)
                                ->select('training_time', DB::raw("Date_Format(training_dt, '%d/%m/%Y') As tr_date"), 'datetime_cd')->get();
                $trArray = json_decode($trainingData);
                $list .= "<option value>[Select]</option>";
                foreach ($trArray as $trDate) {
                    $list .= "<option value='" . $trDate->datetime_cd . "'>" . $trDate->tr_date . " - " . $trDate->training_time . "</option>";
                }
                $response = array(
                    'options' => $list,
                    'status' => 1);
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }

    public function getOtherSubdivisionData(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            $this->validate($request, [
                'zone' => 'required|alpha_num|min:4|max:4'
                    ], [
                'zone.required' => 'Zone is required',
                'zone.alpha_num' => 'Zone must be an alpha numeric characters'
            ]);
            $zone = $request->zone;
            try {
                $filtered = "";
                $tbl_personnela = new tbl_personnela();
                $session_personnela = session()->get('personnela_ppds');
                $filtered = $tbl_personnela
                                ->join('subdivision', 'subdivision.subdivisioncd', '=', DB::raw("SUBSTRING(" . $session_personnela . ".personcd,1,4)"))
                                ->where('forzone', '=', $zone)
                                ->groupBy('subdivision.subdivisioncd')
                                ->pluck('subdivision.subdivision', 'subdivision.subdivisioncd')->all();
                $response = array(
                    'options' => $filtered,
                    'status' => 1
                );
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }
    //:::::::::::::::::::: Get Venue Name ::::::::::::::::::::://
    public function getVenueName(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            $this->validate($request, [
                'subdivision' => 'required|alpha_num|min:4|max:4'
                    ], [
                'subdivision.required' => 'Subdivision is required',
                'subdivision.alpha_num' => 'Subdivision must be an alpha numeric'
            ]);
            try {
                $subdivision = $request->subdivision;
                $list = '';
                $list = tbl_first_training_venue::where('subdivision', '=', $subdivision);
                if(!empty($request->flag)){
                $list= $list->where('status', '=','1');
                }
                $list= $list ->pluck('venuename', 'venue_cd')->all();
                $response = array(
                    'options' => $list,
                    'status' => 1);
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }
   
     //:::::::::::::::::::: Get SubVenue Name ::::::::::::::::::::://
    public function getSubVenueName(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            $this->validate($request, [
                'subdivision' => 'required|alpha_num|min:4|max:4',
                'venuename' => 'required|alpha_num|min:6|max:6'
                    ], [
                'subdivision.required' => 'Subdivision is required',
                'subdivision.alpha_num' => 'Subdivision must be an alpha numeric',
                'venuename.required' => 'Venue name is required',
                'venuename.alpha_num' => 'Venue name must be an alpha numeric'
            ]);
            try {
                $subdivision = $request->subdivision;
                $venuename = $request->venuename;
                $list = '';
                $list = tbl_first_training_subvenue::where('venue_cd', '=', $venuename)
                                ->pluck('subvenue', 'subvenue_cd')->all();

                $response = array(
                    'options' => $list,
                    'status' => 1);
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }
//    public function getSubVenue(Request $request){
//        $statusCode = 200;
//        if (!$request->ajax()) {
//            $statusCode = 400;
//            $response = array('error' => 'Error occured in Ajax Call.');
//            return response()->json($response, $statusCode);
//        }
//        else
//        {
//            $this->validate($request, [            
//            'venuename' => 'required|alpha_num'
//            ], [
//
//            'venuename.required' => 'Venue name is required',
//            'venuename.alpha_num' => 'Venue name must be an alpha numeric'
//            ]);
//            try
//            {
//              $venuename=$request->venuename;
//              $list = '';
//              $list = tbl_first_training_venue::where('first_training_venue.venue_cd','=',$venuename)
////                                            ->Rightjoin('first_training_subvenue','first_training_subvenue.venue_cd','=','first_training_venue.venue_cd')
//                                             ->with('subVenueDetailBrief') 
//                                             ->select('first_training_venue.venue_cd','first_training_venue.maximumcapacity')
//                                             ->get();
//              $response = array(
//                   'options' => $list,
//                   'status' => 1);          
//            }catch (\Exception $e) {
//                $response = array(
//                    'exception' => true,
//                    'exception_message' => $e->getMessage(),
//                );
//              $statusCode = 400;
//            } finally {
//                return response()->json($response, $statusCode);
//            }
//      
//        }
//    }
    public function getSubVenueRoomWise(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            $this->validate($request, [
                'venuename' => 'required|alpha_num|min:6|max:6',
                'trainingdatetime' => 'required|alpha_num|min:4|max:4'
                    ], [
                'venuename.required' => 'Venue name is required',
                'venuename.alpha_num' => 'Venue name must be an alpha numeric',
                'trainingdatetime.required' => 'Training Date & Time is required',
                'trainingdatetime.alpha_num' => 'Training Date & Time must be an alpha numeric'
            ]);
            try {
                $venuename = $request->venuename;
                $trainingdatetime = $request->trainingdatetime;
                $data = array();
                $list = '';
                $list = tbl_first_training_venue::where('first_training_venue.venue_cd', '=', $venuename)
                        ->with('subVenueDetailBrief')
                        ->select('first_training_venue.venue_cd', 'first_training_venue.maximumcapacity')
                        ->get();
                $subV = json_decode($list);
                foreach ($subV as $subTe) {
                    $nestedData['venue_cd'] = $subTe->venue_cd;
                    $nestedData['maximumcapacity'] = $subTe->maximumcapacity;
                    $subTe_array = $subTe->sub_venue_detail_brief;
                    $subTe_array_count = count($subTe_array);
                    $nestedData['sub_venue_detail_brief'] = array();
                    if ($subTe_array_count > 0) {
                        $count = 1;
                        foreach ($subTe_array as $stud_sub) {
                            $nestedSubData['venue_cd'] = $stud_sub->venue_cd;
                            $nestedSubData['subvenue_cd'] = $stud_sub->subvenue_cd;
                            $nestedSubData['subvenue'] = $stud_sub->subvenue;
                            $nestedSubData['maxcapacity'] = $stud_sub->maxcapacity;
                            $tbl_first_training_schedule = new tbl_first_training_schedule;
                            $str_array = $tbl_first_training_schedule
                                            ->where('first_training_schedule.tr_subvenue_cd', '=', $stud_sub->subvenue_cd)
                                            ->where('first_training_schedule.datetimecd', '=', $trainingdatetime)
                                            ->select('no_of_PR', 'no_of_P1', 'no_of_P2', 'no_of_P3', 'no_of_PA', 'no_of_PB')->get();

                            if ($str_array->count() > 0) {
                                foreach ($str_array as $str_row) {
                                    $nestedSubData['pr'] = $str_row->no_of_PR;
                                    $nestedSubData['p1'] = $str_row->no_of_P1;
                                    $nestedSubData['p2'] = $str_row->no_of_P2;
                                    $nestedSubData['p3'] = $str_row->no_of_P3;
                                    $nestedSubData['pa'] = $str_row->no_of_PA;
                                    $nestedSubData['pb'] = $str_row->no_of_PB;
                                }
                            } else {
                                $nestedSubData['pr'] = "";
                                $nestedSubData['p1'] = "";
                                $nestedSubData['p2'] = "";
                                $nestedSubData['p3'] = "";
                                $nestedSubData['pa'] = "";
                                $nestedSubData['pb'] = "";
                            }
                            $nestedData['sub_venue_detail_brief'][] = $nestedSubData;
                        }
                    }
                    $data[] = $nestedData;
                }
                // echo json_encode($data);
                $response = array(
                    'options' => $data,
                    'status' => 1);
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }

    public function firstTrAllocation(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            $validate_array = ['zone' => 'required|alpha_num|min:4|max:4',
            'phase' => 'required|integer',
                'subdivision' => 'required|alpha_num|min:4|max:4',
                'venuename' => 'required|alpha_num|min:6|max:6',
                'trainingtype' => 'required|alpha_num|min:2|max:2',
                'trainingdatetime' => 'required|alpha_num|min:4|max:4',
                'othersubdiv' => 'required|alpha_num|min:1|max:1',
                'choicesubdiv' => 'nullable|alpha_num|required_if:othersubdiv,D|min:4|max:4',
                'assign' => 'required|integer',
                'maxcp' => 'required|digits_between:1,4'];
            for ($x = 1; $x <= $request->row_count; $x++) {
                $validate_array['s_pr' . $x] = 'nullable|integer';
                $validate_array['s_p1' . $x] = 'nullable|integer';
                $validate_array['s_p2' . $x] = 'nullable|integer';
                $validate_array['s_p3' . $x] = 'nullable|integer';
                $validate_array['s_pa' . $x] = 'nullable|integer';
                $validate_array['s_pb' . $x] = 'nullable|integer';
                $validate_array['subVenue' . $x] = 'nullable|alpha_num';
            }
            $validate_array1 = ['zone.required' => 'Zone is required',
            'phase.required'=> 'Phase is required',
            'phase.integer'=> 'Phase must be an integer',
                'zone.alpha_num' => 'Zone must be an alpha numeric characters',
                'subdivision.required' => 'Subdivision is required',
                'subdivision.alpha_num' => 'Subdivision must be an alpha numeric characters',
                'venuename.required' => 'Venuename is required',
                'venuename.alpha_num' => 'Venuename must be an alpha numeric characters',
                'trainingtype.required' => 'Training Type is required',
                'trainingtype.alpha_num' => 'Training Type must be an alpha numeric characters',
                'trainingdatetime.required' => 'Training Date & Time is required',
                'trainingdatetime.alpha_num' => 'Training Date & Time must be an alpha numeric characters',
                'othersubdiv.required' => 'Assign for other Subdivision is required',
                'othersubdiv.alpha_num' => 'Assign for other Subdivision must be an alpha numeric characters',
                'choicesubdiv.required_if' => 'Other Subdivision is required',
                'choicesubdiv.alpha_num' => 'Other Subdivision must be an alpha numeric characters',
                'assign.required' => 'Assigned atleast for one Subvenue',
                'assign.integer' => 'Assigned field must be an integer',
                'maxcp.required' => 'Max Capacity is required',
                'maxcp.digits_between' => 'Max Capacity must be an integer'];
            for ($y = 1; $y <= $request->row_count; $y++) {
                $validate_array1['s_pr' . $y . '.integer'] = 'PR must be an integer';
                $validate_array1['s_p1' . $y . '.integer'] = 'P1 must be an integer';
                $validate_array1['s_p2' . $y . '.integer'] = 'P2 must be an integer';
                $validate_array1['s_p3' . $y . '.integer'] = 'P3 must be an integer';
                $validate_array1['s_pa' . $y . '.integer'] = 'PA must be an integer';
                $validate_array1['s_pb' . $y . '.integer'] = 'PB must be an integer';
                $validate_array1['subVenue' . $y . '.alpha_num'] = 'Subvenue must be an alpha numeric characters';
            }
            $this->validate($request, $validate_array, $validate_array1);

            try {
                $row_count = $request->row_count;
                $subdivision = $request->subdivision;
                $venuename = $request->venuename;
                $trainingtype = $request->trainingtype;
                $trainingdatetime = $request->trainingdatetime;
                $othersubdiv = $request->othersubdiv;
                $choicesubdiv = $request->choicesubdiv;
                $assign = $request->assign;
                $maxcp = $request->maxcp;
                $zone = $request->zone;
                $phase = $request->phase;
                $count = 0;
                $totalAssign = 0;
                for ($j = 1; $j <= $row_count; $j++) {
                    $t_assign = 's_assign' . $j;
                    $totalAssign = $totalAssign + $request->$t_assign;
                }

                if ($totalAssign == $assign && $maxcp >= $assign) {
                    $tbl_first_training_schedule = new tbl_first_training_schedule();
                    $duplicate_v_code = $tbl_first_training_schedule->where('tr_venue_cd', '=', $venuename)
                                    ->where('datetimecd', '=', $trainingdatetime)
                                  
                                    ->select(DB::raw('count(schedule_code) as cnt'))->get();
                    $du_code = json_decode($duplicate_v_code);
                    if ($du_code[0]->cnt != "0") {
                        $tbl_first_training_schedule->where('tr_venue_cd', '=', $venuename)
                       
                                ->where('datetimecd', '=', $trainingdatetime)->delete();
                    }
                    for ($i = 1; $i <= $row_count; $i++) {
                        $s_pr = 's_pr' . $i;
                        $s_p1 = 's_p1' . $i;
                        $s_p2 = 's_p2' . $i;
                        $s_p3 = 's_p3' . $i;
                        $s_pa = 's_pa' . $i;
                        $s_pb = 's_pb' . $i;
                        $s_assign = 's_assign' . $i;
                        $s_subVenue = 'subVenue' . $i;
                        if ($request->$s_assign != "0") {
                            $count++;
                            $tbl_first_training_schedule = new tbl_first_training_schedule();
                            $max_sch_code = $tbl_first_training_schedule->where('tr_venue_cd', '=', $venuename)
                                            ->where('datetimecd', '=', $trainingdatetime)
                                            
                                            ->select(DB::raw('max(schedule_code) as cnt'))->get();
                            $max_code = json_decode($max_sch_code);
                            if ($max_code[0]->cnt == "") {
                                $schcode = $venuename . $trainingdatetime . "01";
                            } else {
                                $tmp_code = 100 + substr($max_code[0]->cnt, -2) + 1;
                                $schcode = $venuename . $trainingdatetime . substr($tmp_code, -2);
                            }

                            $s_pr_v = $request->$s_pr;
                            $s_p1_v = $request->$s_p1;
                            $s_p2_v = $request->$s_p2;
                            $s_p3_v = $request->$s_p3;
                            $s_pa_v = $request->$s_pa;
                            $s_pb_v = $request->$s_pb;
                            $s_subVenue_v = $request->$s_subVenue;
                            if ($othersubdiv == 'S') {
                                $choice_subdiv = $subdivision;
                            } else {
                                $choice_subdiv = $choicesubdiv;
                            }
                            $user_code = session()->get('code_ppds');
                            $tbl_first_training_schedule->schedule_code = $schcode;
                            $tbl_first_training_schedule->forzone = $zone;
                            $tbl_first_training_schedule->subdivisioncd = $subdivision;
                            $tbl_first_training_schedule->tr_venue_cd = $venuename;
                            $tbl_first_training_schedule->tr_subvenue_cd = $s_subVenue_v;
                            $tbl_first_training_schedule->training_type = $trainingtype;
                            $tbl_first_training_schedule->datetimecd = $trainingdatetime;
                            $tbl_first_training_schedule->no_of_PR = $s_pr_v;
                            $tbl_first_training_schedule->no_of_P1 = $s_p1_v;
                            $tbl_first_training_schedule->no_of_P2 = $s_p2_v;
                            $tbl_first_training_schedule->no_of_P3 = $s_p3_v;
                            $tbl_first_training_schedule->no_of_PA = $s_pa_v;
                            $tbl_first_training_schedule->no_of_PB = $s_pb_v;
                            $tbl_first_training_schedule->choice_type = $othersubdiv;
                            $tbl_first_training_schedule->choice_subdiv = $choice_subdiv;
                            $tbl_first_training_schedule->usercode = $user_code;
                            $tbl_first_training_schedule->phase_id = $phase;
                          

                            $tbl_first_training_schedule->save();
                        }
                    }
                    $response = array(
                        'options' => $count,
                        'status' => 1
                    );
//               }else{
//                   //::::::::::::::update:::::::::::::://
//                    $response = array(
//                       'options' => $count,
//                       'status' => 2 
//                    );
//               }
                } else {

                    $response = array(
                        'options' => $assign,
                        'status' => 0
                    );
                }
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }

    public function first_training_venue_allocation_datatable(Request $request) { //dd($request->all());
        $response = [];
        $perm = null;
        $statusCode = 200;
        $users = array(); //Should be changed #4
        $search_val = array();
        $this->validate($request, [
            'search.*' => 'nullable|regex:/^[A-Za-z0-9\s]+$/i',
            'draw' => 'required|integer',
            'start' => 'required|integer',
            'length' => 'required|integer',
            'subdivision' => 'nullable|alpha_num|min:4|max:4',
            'venuename' => 'nullable|alpha_num|min:6|max:6',
            'zone' => 'nullable|alpha_num|min:4|max:4',
            'trainingtype' => 'nullable|alpha_num|min:2|max:2',
            'trainingdatetime' => 'nullable|alpha_num|min:4|max:4',
            'phase' => 'nullable|integer'
                ], [
             'search.*.regex' => 'Special Charecters not allowed',
            'draw.required' => 'Draw is required',
            'start.integer' => 'Start must be an integer',
            'start.required' => 'Start is required',
            'length.integer' => 'Length must be an integer',
            'length.required' => 'Length is required',
            'subdivision.alpha_num' => 'Subdivision code must be an alpha numeric',
            'venuename.alpha_num' => 'Venue code must be an alpha numeric',
            'zone.alpha_num' => 'Zone code must be an alpha numeric',
            'trainingtype.alpha_num' => 'Training Type must be an alpha numeric',
            'trainingdatetime.alpha_num' => 'Training date time code must be an alpha numeric',
            'phase.integer'=>'Phase must be an integer'
        ]);
        try {
            $draw = $request->draw;
            $offset = $request->start;
            $length = $request->length;
            $search = $request->search ["value"];
            $order = $request->order;
            $subdivision = $request->subdivision;
            $zone = $request->zone;
            $phase = $request->phase;
            $venuename = $request->venuename;
            $trainingtype = $request->trainingtype;
            $trainingdatetime = $request->trainingdatetime;
            //print_r($order);die;
            $categ = \Session::get('category_ppds');
            $dist = \Session::get('districtcd_ppds');

            $all = tbl_first_training_subvenue::all();
            $filtered = tbl_first_training_schedule::join('first_training_venue', 'first_training_venue.venue_cd', '=', 'first_training_schedule.tr_venue_cd')
                    ->join('first_training_subvenue', 'first_training_schedule.tr_subvenue_cd', '=', 'first_training_subvenue.subvenue_cd')
                    ->join('first_training_date_time', 'first_training_date_time.datetime_cd', '=', 'first_training_schedule.datetimecd')
                    ->join('phase', 'phase.code', '=', 'first_training_schedule.phase_id')
                    ->select('training_type','no_of_PR', 'no_of_P1', 'no_of_P2', 'no_of_P3', 'no_of_PA', 'no_of_PB', 'first_training_schedule.schedule_code', 'first_training_venue.venuename', 'first_training_subvenue.subvenue', 'first_training_subvenue.maxcapacity','first_training_date_time.training_dt','first_training_date_time.training_time','phase.name')
                    ->where(function($q) use ($search) {
                       $q->orwhere('first_training_subvenue.subvenue', 'like', '%' . $search . '%')
                         ->orwhere('first_training_venue.venuename', 'like', '%' . $search . '%')
                         ->orwhere('first_training_subvenue.maxcapacity', 'like', '%' . $search . '%');
                    });
            if ($dist != '') {
                $filtered = $filtered->where(DB::raw("substring(first_training_schedule.schedule_code,1,2)"), '=', $dist);
            }
            if ($subdivision != '') {
                $filtered = $filtered->where('first_training_schedule.subdivisioncd', '=', $subdivision);
            }
            if ($venuename != '') {
                $filtered = $filtered->where('first_training_schedule.tr_venue_cd', '=', $venuename);
            }
            if ($zone != '') {
                $filtered = $filtered->where('first_training_schedule.forzone', '=', $zone);
            }
            if ($trainingtype != '') {
                $filtered = $filtered->where('first_training_schedule.training_type', '=', $trainingtype);
            }
            if ($trainingdatetime != '') {
                $filtered = $filtered->where('first_training_schedule.datetimecd', '=', $trainingdatetime);
            }
            if ($phase != '') {
                $filtered = $filtered->where('first_training_schedule.phase_id', '=', $phase);
            }
            
            $ordered = $filtered;
            $filtered_count = $filtered->count();
            //echo count ( $order );die;
//            for ($i = 0; $i < count($order); $i ++) {
//                $ordered = $ordered->orderBy($request->columns [$order [$i] ['column']] ['data'], strtoupper($order [$i] ['dir']));
//            }
            $page_displayed = $ordered->orderBy('first_training_schedule.forzone')
                    ->orderBy('first_training_schedule.subdivisioncd')
                    ->orderBy('first_training_schedule.tr_venue_cd')
                    ->orderBy('first_training_schedule.tr_subvenue_cd')
                    ->orderBy('first_training_schedule.datetimecd')
                    ->offset($offset)->limit($length)
                    ->get();
            $data = array();
            $i = 1;
            if (!empty($page_displayed)) {
                foreach ($page_displayed as $assm) {
                    
                    $nestedData['code'] = $i;
                    $nestedData['name'] = $assm->name;
                    $nestedData['venue'] = $assm->venuename . ', ' . $assm->subvenue;
                    $nestedData['PR_A'] = $assm->no_of_PR;
                    $nestedData['PR_U'] = $this->get_used_postat_first_training($assm->schedule_code, 'PR',$assm->training_type);
                    $nestedData['P1_A'] = $assm->no_of_P1;
                    $nestedData['P1_U'] = $this->get_used_postat_first_training($assm->schedule_code, 'P1',$assm->training_type);
                    $nestedData['P2_A'] = $assm->no_of_P2;
                    $nestedData['P2_U'] = $this->get_used_postat_first_training($assm->schedule_code, 'P2',$assm->training_type);
                    $nestedData['P3_A'] = $assm->no_of_P3;
                    $nestedData['P3_U'] = $this->get_used_postat_first_training($assm->schedule_code, 'P3',$assm->training_type);
                    $nestedData['PA_A'] = $assm->no_of_PA;
                    $nestedData['PA_U'] = $this->get_used_postat_first_training($assm->schedule_code, 'PA',$assm->training_type);
//                    $nestedData['PB_A'] = $assm->no_of_PB;
//                    $nestedData['PB_U'] = $this->get_used_postat_first_training($assm->schedule_code, 'PB',$assm->training_type);
                    $nestedData['TOT_A'] = $nestedData['PR_A'] + $nestedData['P1_A'] + $nestedData['P2_A'] + $nestedData['P3_A'] + $nestedData['PA_A'];
                    $nestedData['TOT_U'] = $nestedData['PR_U'] + $nestedData['P1_U'] + $nestedData['P2_U'] + $nestedData['P3_U'] + $nestedData['PA_U'];
                    
                    $nestedData['TOT_m'] = $assm->maxcapacity;
                    $nestedData['TR_D_T'] = date('d/m/Y', strtotime(trim(str_replace('/', '-', $assm->training_dt))))." ".$assm->training_time; 
                    if ($nestedData['TOT_U'] != 0) {
                        $edit_button = $delete_button = 'NA';
                    } else {
                        $edit_button = $delete_button = $assm->schedule_code;
                    }


                    $nestedData['action'] = array('e' => $edit_button, 'd' => $delete_button);
                    $i++;
                    $data[] = $nestedData;
                }
            }
            $response = array(
                "draw" => $draw,
                "recordsTotal" => $filtered_count, //Should be changed #7
                "recordsFiltered" => $filtered_count,
                'fisrt_training_venue_alloc' => $data //Should be changed #8
            );
        } catch (\Exception $e) {
            $response = array(
                'exception' => true,
                'exception_message' => $e->getMessage()
            );
            $statusCode = 400;
        } finally {
            return response()->json($response, $statusCode);
        }
    }

    public function delete_first_training_venue_alloc(Request $request) {

        $statusCode = 200;
        $record = null; //Should be changed #26
        $this->validate($request, [
            'data' => 'required|alpha_num|min:12|max:12'
            
                ], [
            
            'data.required' => 'Schedule code is required',
            'data.alpha_num' => 'Schedule code must be an alpha numeric',
                  
        ]);
        try {
            $schedulecd = $request->data;
            $record = tbl_first_training_schedule::where('schedule_code', '=', $schedulecd); //Should be changed #27

            if (!empty($record)) {//Should be changed #30
                $record = $record->delete();
            }

            $response = array(
                'record' => $record //Should be changed #32
            );
        } catch (\Exception $e) {
            $response = array(
                'exception' => true,
                'exception_message' => $e->getMessage()
            );
            $statusCode = 400;
        } finally {
            return response()->json($response, $statusCode);
        }
    }

}
