<?php

namespace App\Http\Controllers;
use App\tbl_first_rand_table;
use App\tbl_personnela;
use App\Http\Controllers\FirstTrainingAllocationController;
use Illuminate\Http\Request;
use DB;
class TrainingChangeController extends Controller
{
    public function getDataForChange() {
        //$training_datetime= $this->getTrainingDateTime();
        $new_c=new FirstTrainingAllocationController();
        $training_type = $new_c->getTrainingTypeData();
        return view('first_training_change', compact('training_type'));
    }
    public function getOldPersonnelDetails_for_change(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        }
        else
        {
           $this->validate($request, [            
            'personnel_id' => 'required|alpha_num|max:11|min:11',
            'zone' => 'required|alpha_num|max:4|min:4'
            ], [
            
            'personnel_id.required' => 'Personnel ID is required',
            'personnel_id.alpha_num' => 'Personnel ID must be an alpha numeric',
              'zone.required' => 'Zone is required',
            'zone.alpha_num' => 'Zone must be an alpha numeric characters'
            ]);
            $personnel_id=$request->personnel_id;
            $zone=$request->zone;
            try
            {
                $filtered=""; 
                $resMessage="";
                $tbl_personnela = new tbl_personnela();             
                $session_personnela=session()->get('personnela_ppds');
                $filtered = $tbl_personnela
                           ->join('office',''.$session_personnela.'.officecd','=','office.officecd')
                           ->join('personnel',''.$session_personnela.'.personcd','=','personnel.personcd')
                           ->join('poststat',''.$session_personnela.'.poststat','=','poststat.post_stat')
                           ->leftjoin('policestation','office.policestn_cd','=','policestation.policestationcd')
                           ->join('district','personnel.districtcd','=','district.districtcd')
                           ->join('subdivision','office.subdivisioncd','=','subdivision.subdivisioncd')
                           ->leftjoin('assembly as ass_pre','ass_pre.assemblycd','=','personnel.assembly_temp') 
                            ->leftjoin('assembly as ass_ofc','ass_ofc.assemblycd','=','personnel.assembly_off')  
                            ->leftjoin('assembly as ass_per','ass_per.assemblycd','=','personnel.assembly_perm')  
                            ->where(''.$session_personnela.'.personcd','=', $personnel_id)  
                            ->where(''.$session_personnela.'.forzone', '=', $zone)
                            ->groupBy(''.$session_personnela.'.personcd')
                            ->select(DB::raw("DATE_FORMAT(personnel.dateofbirth,'%d/%m/%Y') as dateofbirth"),''.$session_personnela.'.personcd','district.district','address1','address2','postoffice','pin'
                                    ,'policestation.policestation','subdivision.subdivision','personnel.officecd','personnel.officer_name','personnel.off_desg','personnel.present_addr1','personnel.present_addr2'
                                    ,'personnel.assembly_temp as pre_ass_cd','ass_pre.assemblyname AS pre_ass', 'personnel.assembly_perm as per_ass_cd','ass_per.assemblyname AS per_ass', 'personnel.assembly_off as post_ass_cd','ass_ofc.assemblyname AS post_ass'
                                    ,'training_gr_cd','poststat.poststatus',''.$session_personnela.'.poststat',''.$session_personnela.'.booked','personnel.gender','personnel.epic','forassembly','forzone','groupid','personnel.subdivisioncd','1stTrainingSch','1stTrainingSch_2',''.$session_personnela.'.dcrccd','2ndTrainingSch',''.$session_personnela.'.no_of_member')->get();

                // dd($filtered);
           
                if(count($filtered)>0){
                   foreach($filtered as $rwfilter){
                       if($rwfilter->booked=="C" || $rwfilter->booked==""){
                          $resMessage="Not Available for Selected Operation";
                          $status = 0;
                       }else{
                           $fst='1stTrainingSch';
                           $fstSec='1stTrainingSch_2';
                           $sst='2ndTrainingSch';
                            $resMessage.="<table width='100%' style='font-family: verdana,arial,sans-serif;font-size: 11px;color: #000;'>\n";
                            $resMessage.="<tr><td align='left'>Name: </td><td align='left' colspan='3'>".$rwfilter->officer_name."</td></tr>\n";
                            $resMessage.="<tr><td align='left'>Office ID: </td><td align='left' colspan='3'>".$rwfilter->officecd."</td></tr>\n";
                            $resMessage.="<tr><td align='left'>Designation: </td><td align='left' colspan='3'>".$rwfilter->off_desg."</td></tr>\n";
                            $resMessage.="<tr><td align='left'>Office Address: </td><td align='left' colspan='3'>".$rwfilter->address1.",".$rwfilter->address2.", PO-".$rwfilter->postoffice.", PS-".$rwfilter->policestation.", Subdiv-".$rwfilter->subdivision.", Dist.-".$rwfilter->district.",".$rwfilter->pin."</td></tr>\n";
                            $resMessage.="<tr><td align='left'>Date of Birth: </td><td align='left'>".$rwfilter->dateofbirth."</td><td align='left'>Gender: </td><td align='left'>".$rwfilter->gender."<input type='hidden' id='gender' name='gender' value='".$rwfilter->gender."'></td></tr>\n";
                            $resMessage.="<tr><td align='left'>EPIC No: </td><td align='left'>".$rwfilter->epic."</td><td align='left'>Posting Status: </td><td align='left'>".$rwfilter->poststatus."<input type='hidden' id='post_stat' name='post_stat' value='".$rwfilter->poststat."'></td></tr>\n";
                            $resMessage.="<tr><td align='left'>Present Address: </td><td align='left' colspan='3'>".$rwfilter->present_addr1.", ".$rwfilter->present_addr2."</td></tr>\n";
                            $resMessage.="<tr><td align='left' colspan='4'><b>Assembly of</b></td></tr>\n";
                            $resMessage.="<tr><td align='left' colspan='2'>Present Address: </td><td align='left' colspan='2'>".$rwfilter->pre_ass."<hidden id='hid_pre_ass' name='hid_pre_ass' style='display:none;'>".$rwfilter->pre_ass_cd."</hidden></td></tr>\n";
                            $resMessage.="<tr><td align='left' colspan='2'>Permanent Address: </td><td align='left' colspan='2'>".$rwfilter->per_ass."<hidden id='hid_per_ass' name='hid_per_ass' style='display:none;'>".$rwfilter->per_ass_cd."</hidden></td></tr>\n";
                            $resMessage.="<tr><td align='left' colspan='2'>Place of Posting: </td><td align='left' colspan='2'>".$rwfilter->post_ass."<hidden id='hid_post_ass' name='hid_post_ass' style='display:none;'>".$rwfilter->post_ass_cd."</hidden></td></tr>\n";
                            $resMessage.="<tr><td align='left' colspan='4'><input type='hidden' id='forassembly' name='forassembly'  value='".$rwfilter->forassembly."'>\n<input type='hidden' id='groupid' name='groupid' value='".$rwfilter->groupid."'>\n<input type='hidden' id='training_gr_cd' name='training_gr_cd' value='".$rwfilter->training_gr_cd."'>\n<input type='hidden' id='booked' name='booked' value='".$rwfilter->booked."'>\n <input type='hidden' id='no_of_member' name='no_of_member' value='".$rwfilter->no_of_member."'>\n <input type='hidden' id='per_cd' name='per_cd' value='".$rwfilter->personcd."'>\n <input type='hidden' id='zone' name='zone' value='".$rwfilter->forzone."'>\n <input type='hidden' id='dcrccd' name='dcrccd' value='".$rwfilter->dcrccd."'>\n <input type='hidden' id='sub_div' name='sub_div' value='".$rwfilter->subdivisioncd."'>\n <input type='hidden' id='ofc_cd' name='ofc_cd' value='".$rwfilter->officecd."'>\n <input type='hidden' id='training1_sch' name='training1_sch' value='".$rwfilter->$fst."'>\n <input type='hidden' id='training1_sch_sec' name='training1_sch_sec' value='".$rwfilter->$fstSec."'> <input type='hidden' id='training2_sch' name='training2_sch' value='".$rwfilter->$sst."'></td></tr>\n";

                            $resMessage.="<tr><td align='right' colspan='2'>Booked : </td><td colspan='2' align='left' id='o_booked'>Yes</td></tr>\n";
                            $resMessage.="</table>";
                            $status = 1;
                       }
                   }                    
                }else{
                   $resMessage="No records found"; 
                   $status = 0;
                }

                $response = array(
                   'options' => $resMessage,
                   'status' => $status
                );
            }catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
              $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
        
    }
   public function get_venue_with_not_fill(Request $request){
    $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        }
        else
        {
           $this->validate($request, [            
            'subdivision' => 'required|alpha_num|min:4|max:4',
            'trainingtype'=> 'required|alpha_num|min:2|max:2'
            ], [           
            'subdivision.required' => 'subdivision is required',
            'subdivision.alpha_num' => 'Subdivision must be an alpha numeric characters',
            'trainingtype.required' => 'Training Type is required',
            'trainingtype.alpha_num' => 'Training Type must be an alpha numeric characters'
            ]);
            $subdivision=$request->subdivision;
            $trainingtype=$request->trainingtype;
            $post_stat=$request->post_stat;
            //echo $post_stat;die;
            $capacity=''; 
            if($post_stat=='P1'){
             $capacity='no_of_P1';   
            }else if($post_stat=='P2'){
             $capacity='no_of_P2';    
            }else if($post_stat=='P3'){
             $capacity='no_of_P3';    
            }else if($post_stat=='PA'){
              $capacity='no_of_PA';   
            }else if($post_stat=='PB'){
              $capacity='no_of_PB';   
            }else{
              $capacity='no_of_PR';   
            }
            try
            {
                $filtered=tbl_first_rand_table::rightjoin('first_training_schedule', function ($join) {
            $join->on('first_training_schedule.schedule_code','=','first_rand_table.schedule_code')->orOn('first_training_schedule.schedule_code','=','first_rand_table.schedule_code_2');
        })->leftjoin('first_training_venue','first_training_venue.venue_cd','=','first_training_schedule.tr_venue_cd')
                        ->leftjoin('first_training_date_time','first_training_date_time.datetime_cd','=','first_training_schedule.datetimecd')
                        ->leftjoin('first_training_subvenue','first_training_subvenue.subvenue_cd','=','first_training_schedule.tr_subvenue_cd')
                        ->where('first_training_schedule.training_type',$trainingtype)
                        ->where('first_training_schedule.subdivisioncd',$subdivision)
                        ->groupBy('first_training_schedule.schedule_code','first_training_venue.venuename','first_training_subvenue.subvenue','first_training_date_time.training_dt','first_training_date_time.training_time','first_training_schedule.'.$capacity)
                        ->havingRaw('used<first_training_schedule.'.$capacity)->select('first_training_schedule.schedule_code','first_training_venue.venuename','first_training_subvenue.subvenue',DB::raw("DATE_FORMAT(first_training_date_time.training_dt,'%d/%m/%Y') as training_dt"),'first_training_date_time.training_time',DB::raw('COUNT(first_rand_table.personcd) as used'),'first_training_schedule.'.$capacity)
                        ->get();
                //echo json_encode($filtered);
                $response = array(
                   'options' => $filtered,
                   'status' => 1
                );
            }catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
              $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }   
   }
   public function changeTrainingData(Request $request) {
       $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        }
        else
        {
           $this->validate($request, [
            'personnel_id'=>'required|alpha_num|max:11',
            'subdivision' => 'required|alpha_num|min:4|max:4',
            'trainingtype'=> 'required|alpha_num|min:2|max:2',
            'zone'=>'required|alpha_num|min:4|max:4',
            'v_sub'=>'required|alpha_num|max:12'
            ], [           
            'subdivision.required' => 'subdivision is required',
            'subdivision.alpha_num' => 'Subdivision must be an alpha numeric characters',
            'trainingtype.required' => 'Training Type is required',
            'trainingtype.alpha_num' => 'Training Type must be an alpha numeric characters',
                'personnel_id.required' => 'Personnel ID is required',
            'personnel_id.alpha_num' => 'Personnel ID must be an alpha numeric characters',
                'zone.required' => 'Zone is required',
            'zone.alpha_num' => 'Zone must be an alpha numeric characters',
                'v_sub.required' => 'Subvenue is required',
            'v_sub.alpha_num' => 'Subvenue Code must be an alpha numeric characters'
            ]);
            $subdivision=$request->subdivision;
            $trainingtype=$request->trainingtype;
            $personnel_id=$request->personnel_id;
            $zone=$request->zone;
            $v_sub=$request->v_sub;
            $districtcd=$request->districtcd;
            $trainingHead=$request->trainingtype;
           $trainging=$request->trainging;
           $orderno = $request->orderno;
        //    $phase = $request->phase;
        //    dd($orderno);
            $session_personnela=session()->get('personnela_ppds');
            try
            {
             $firstRand = DB::select('call first_appt_change(?,?,?,?,?,?,?,?)',[$personnel_id,$zone,$districtcd,$session_personnela,$v_sub,$trainingHead,$trainging,$orderno]);
                $response = array(
                   'options' => 1,
                   'status' => 1
                );
            }catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
              $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
   }
}
