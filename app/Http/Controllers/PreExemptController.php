<?php

namespace App\Http\Controllers;

use App\tbl_personnel;
use App\tbl_district;
use DB;

use Illuminate\Http\Request;

class PreExemptController extends Controller {

    public function getPPDetailsForExempt(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            $this->validate($request, [
                'office_cd' => 'nullable|alpha_num|min:10|max:10',
                'districtcd' => 'required|alpha_num|min:2|max:2',
                'searchDesignationValue' => 'nullable|regex:/(^[-0-9A-Za-z().,&\/ ]+$)/',
                'age_filter'=>'nullable|integer'
                    ], [
                // 'office_cd.required' => 'Office Code is required',
                'office_cd.alpha_num' => 'Office Code must be an alpha numeric',
//                'searchDesignationValue.regex' => 'Designation should be alphabatic only',
                'districtcd.required' => 'District Code is required',
                'districtcd.alpha_num' => 'District Code must be an alpha numeric characters',
                        'age_filter.integer'=>'Age should be an integer'
            ]);
            $office_cd = $request->office_cd;
            $districtcd = $request->districtcd;
            $searchDesignationValue = $request->searchDesignationValue;
            $age_filter = $request->age_filter;
           // echo $age_filter;die;
            $current_date= date("d/m/Y");
         //   echo $current_date;die;
            
            try {
                $filtered = '';
                $ApptRecord = tbl_personnel::where('districtcd', '=', $districtcd)
                        ->select('personcd', 'officer_name', 'off_desg', 'basic_pay', 'grade_pay', 'preexempt', 'prereason'
                                );
               // print_r($ApptRecord);die;
                if ($office_cd != '') {
                    $ApptRecord = $ApptRecord->where('officecd', '=', $office_cd);
                }

                if ($searchDesignationValue != '') {
                    $ApptRecord = $ApptRecord->where('off_desg', '=', $searchDesignationValue);
                }
                 if ($age_filter != '') {
                    $ApptRecord = $ApptRecord//->where('TIMESTAMPDIFF(YEAR, personnel.dateofbirths, now() )>58')
                             ->whereRaw('ABS(TIMESTAMPDIFF(YEAR, personnel.dateofbirth, now() ))>58')
                             ->whereRaw('ABS(TIMESTAMPDIFF(MONTH, personnel.dateofbirth, now() )) % 12>0');
                }
               
                $filtered = $ApptRecord->get();
                $subV = json_decode($filtered);
                // print_r($subV);die;
                $data = array();
                if (!empty($subV)) {
                    foreach ($subV as $subTe) {

                        $nestedData['personcd'] = $subTe->personcd;
                        $nestedData['officer_name'] = $subTe->officer_name;
                        $nestedData['off_desg'] = $subTe->off_desg;
                        $nestedData['basic_pay'] = $subTe->basic_pay;
                        $nestedData['grade_pay'] = $subTe->grade_pay;
                        $nestedData['pStatus'] = $subTe->preexempt;
                        $nestedData['prereason'] = $subTe->prereason;
                        $data[] = $nestedData;
                    }
                    $response = array(
                        'options' => $data,
                        'status' => 1);
                } else {
                    $response = array(
                        'options' => "No Records Found",
                        'status' => 0);
                }
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }

    public function save_pre_exemption(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            // echo $request->dcvenue;die;
            $validate_array = ['office_cd' => 'nullable|alpha_num|min:10|max:10',
                'districtcd' => 'required|alpha_num|min:2|max:2',
                'reason' => 'required|regex:/^[A-Za-z0-9\s,-.]+$/i|max:30'];

            for ($x = 0; $x <= $request->row_count; $x++) {
                $validate_array['myCheck' . $x] = 'nullable|alpha_num|min:11|max:11';
            }
            $validate_array1 = ['office_cd.required' => 'Office Code is required',
                'office_cd.alpha_num' => 'Office Code must be an alpha numeric',
                'districtcd.required' => 'District Code is required',
                'districtcd.alpha_num' => 'District Code must be an alpha numeric characters',
                'reason.required' => 'Reason is required',
                'reason.regex' => 'Reason must not be special characters',
                'reason.max' => 'Reason must be 30 characters'
            ];
            for ($y = 0; $y <= $request->row_count; $y++) {
                $validate_array1['myCheck' . $y . '.alpha_num'] = 'Personnel ID must be an alpha numeric characters';
            }
            $this->validate($request, $validate_array, $validate_array1);
            try {
                $user_code = session()->get('code_ppds');
                $row_count = $request->row_count;
                $districtcd = $request->districtcd;
                $office_cd = $request->office_cd;
                $reason = $request->reason;
                $count = 0;
                $status = 0;
//                tbl_personnel::where('districtcd','=',$districtcd)
//                         ->where('officecd','=',$office_cd)
//                         ->update(['preexempt'=>null]);
                for ($i = 1; $i <= $row_count; $i++) {
                    $pID = 'myCheck' . $i;
                    if ($request->$pID != "") {
                        $per_id = $request->$pID;
                        tbl_personnel::where('districtcd', '=', $districtcd)
                                ->where('personcd', '=', $per_id)
                                ->update(['preexempt' => 'Y', 'prereason' => $reason]);
                        $count++;
                    }
                }
                $response = array(
                    'options' => $count,
                    'status' => 1);
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }

    public function getCheckedPPDetailsForExempt(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            $this->validate($request, [
                'office_cd' => 'nullable|alpha_num|min:10|max:10',
                'districtcd' => 'required|alpha_num|min:2|max:2',
                'searchDesignationValue' => 'nullable|regex:/(^[-0-9A-Za-z().,&\/ ]+$)/',
                'age_filter'=>'nullable|integer'
                    ], [
                // 'office_cd.required' => 'Office Code is required',
                'office_cd.alpha_num' => 'Office Code must be an alpha numeric',
                'searchDesignationValue.regex' => 'Designation should be alphabatic only',
                'districtcd.required' => 'District Code is required',
                'districtcd.alpha_num' => 'District Code must be an alpha numeric characters',
                        'age_filter.integer'=>'Age should be an integer'
            ]);
            $office_cd = $request->office_cd;
            $districtcd = $request->districtcd;
             $searchDesignationValue = $request->searchDesignationValue;
           $age_filter = $request->age_filter;
            try {
                $filtered = '';
                $ApptRecord = tbl_personnel::where('districtcd', '=', $districtcd)
                        ->where('preexempt', '=', 'Y')
                        ->select('personcd', 'officer_name', 'off_desg', 'basic_pay', 'grade_pay', 'preexempt', 'prereason');
               if ($office_cd != '') {
                    $ApptRecord = $ApptRecord->where('officecd', '=', $office_cd);
                }

                if ($searchDesignationValue != '') {
                    $ApptRecord = $ApptRecord->where('off_desg', '=',  $searchDesignationValue);
                }
                  if ($age_filter != '') {
                    $ApptRecord = $ApptRecord//->where('TIMESTAMPDIFF(YEAR, personnel.dateofbirths, now() )>58')
                             ->whereRaw('ABS(TIMESTAMPDIFF(YEAR, personnel.dateofbirth, now() ))>58')
                             ->whereRaw('ABS(TIMESTAMPDIFF(MONTH, personnel.dateofbirth, now() )) % 12>0');
                }
                $filtered=$ApptRecord->get();
                $subV = json_decode($filtered);
                $data = array();
                if (!empty($subV)) {
                    foreach ($subV as $subTe) {

                        $nestedData['personcd'] = $subTe->personcd;
                        $nestedData['officer_name'] = $subTe->officer_name;
                        $nestedData['off_desg'] = $subTe->off_desg;
                        $nestedData['basic_pay'] = $subTe->basic_pay;
                        $nestedData['grade_pay'] = $subTe->grade_pay;
                        $nestedData['pStatus'] = $subTe->preexempt;
                        $nestedData['prereason'] = $subTe->prereason;
                        $data[] = $nestedData;
                    }
                    $response = array(
                        'options' => $data,
                        'status' => 1);
                } else {
                    $response = array(
                        'options' => "No Records Found",
                        'status' => 0);
                }
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }

    public function edit_pre_exemption(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            // echo $request->dcvenue;die;
            $validate_array = ['office_cd' => 'nullable|alpha_num|min:10|max:10',
                'districtcd' => 'required|alpha_num|min:2|max:2',
                'searchDesignationValue' => 'nullable|regex:/(^[-0-9A-Za-z().,&\/ ]+$)/'
                ];

            for ($x = 0; $x <= $request->row_count; $x++) {
                $validate_array['myCheck' . $x] = 'nullable|alpha_num|min:11|max:11';
            }
            $validate_array1 = ['office_cd.required' => 'Office Code is required',
                'office_cd.alpha_num' => 'Office Code must be an alpha numeric',
                'districtcd.required' => 'District Code is required',
                'districtcd.alpha_num' => 'District Code must be an alpha numeric characters'
            ];
            for ($y = 0; $y <= $request->row_count; $y++) {
                $validate_array1['myCheck' . $y . '.alpha_num'] = 'Personnel ID must be an alpha numeric characters';
            }
            $this->validate($request, $validate_array, $validate_array1);
            try {
                $user_code = session()->get('code_ppds');
                $row_count = $request->row_count;
                $districtcd = $request->districtcd;
                $office_cd = $request->office_cd;
                $searchDesignationValue = $request->searchDesignationValue;
                
                $Ofccount = 0;
                $count = 0;
                $status = 0;
                $person = array();
                for ($i = 1; $i <= $row_count; $i++) {
                    $pID = 'myCheck' . $i;
                    if ($request->$pID != "") {
                        $per_id = $request->$pID;
                        $person[$Ofccount] = $per_id;
                        $Ofccount++;
                        $count++;
                    }
                }
                $tbl_personnel = new tbl_personnel();
                if($office_cd !=""){
                    $tbl_personnel=$tbl_personnel->where('officecd', '=', $office_cd);
                }else if($searchDesignationValue !=""){
                    $tbl_personnel=$tbl_personnel->where('off_desg', '=', $searchDesignationValue);
                }
                $perData = $tbl_personnel->where('districtcd', '=', $districtcd);
                        
                if (count($person) > 0) {
                    $perData = $perData->whereIn('personcd', $person);
                }
                $perData->update(['preexempt' => null, 'prereason' => null]);

                $response = array(
                    'options' => $count,
                    'status' => 1);
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }

    public function officeCodeSearch(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            $term = trim($request->q);
            $dist = \Session::get('districtcd_ppds');
            $validator1 = \Validator::make(compact('term'), [
                        'term' => 'required|digits_between:1,10',
                            ], [
                        'term.required' => 'Office code must be an alpha numeric characters',
                        'term.digits_between' => 'Office code  may not be greater than 10 characters',
            ]);
            $this->validateWith($validator1);
            try {
                $officeResult = [];

                $term = trim($request->q);
                if (empty($term)) {
                    return \Response::json([]);
                }
                $searchOffice = tbl_personnel::where('personnel.officecd', 'like', $term . '%')->where('districtcd', '=', $dist)->groupBy('officecd')->select('officecd')->get();
             $nestedData=array();
                foreach ($searchOffice as $searchOffice1) {
                    $officeResult['officecd'] = $searchOffice1["officecd"];
                    $nestedData[]=$officeResult;
                }
                $response=array('options'=>$nestedData);
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }

    public function designationSearch(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            $term = trim($request->q);
            $dist = \Session::get('districtcd_ppds');
            $validator1 = \Validator::make(compact('term'), [
                        'term' => 'required',
                            //  'term.*' => 'integer|max:10'
                            ], [
                        'term.required' => 'Office code must be an alpha numeric characters',
                            // 'search_param.*.max' => 'Office code  may not be greater than 10 characters'
            ]);
            $this->validateWith($validator1);
            try {
                $designationResult = [];
                $term = trim($request->q);
                if (empty($term)) {
                    return \Response::json([]);
                }
                $searchDesignation = tbl_personnel::where('personnel.off_desg', 'like', $term . '%')->where('districtcd', '=', $dist)->groupBy('off_desg')->get();
               $nestedData=array();
                foreach ($searchDesignation as $searchDesignation1) {
                    $designationResult['off_desg'] = $searchDesignation1["off_desg"];
                    $nestedData[]=$designationResult;
                }
                $response=array('options'=>$nestedData);
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }
    
    public function pre_exemption_export(Request $request){
       // echo 'hi';die;
//        $this->validate($request, [ 
//            'subdivid' => 'required|alpha_num|min:4|max:4'
//            ], [
//            'subdivid.required' => 'Subdivision is required',
//            'subdivid.alpha_num' => 'Subdivision code must be an alpha numeric'
//           ]);
  
        //$session_personnela=session()->get('personnela_ppds');
       
         $district_code=session()->get('districtcd_ppds');
        $district_name=tbl_district::where('districtcd','=',$district_code)->value(DB::raw('district'));
       // $district_name_encode=$district_name[0]->district;
        $excelTitle=$district_name."_pre_exempt_reports";
        
        $data = array();
         $recordsTotal = tbl_personnel
                        ::where('personnel'.'.preexempt','=', 'Y') 
                       
        ->where('personnel'.'.districtcd','=', $district_code)
                 ->select('personcd','officecd','officer_name','off_desg','basic_pay','grade_pay','prereason')->get();
         foreach($recordsTotal as $recordsTo){
             //$fst='1stTrainingSch';
             //$sst='2ndTrainingSch';
             $nestedData1['Personnel ID']=$recordsTo->personcd; 
             $nestedData1['officecd']=$recordsTo->officecd; 
             $nestedData1['Name']=$recordsTo->officer_name; 
             $nestedData1['Designation']=$recordsTo->off_desg; 
             $nestedData1['Basic Pay']=$recordsTo->basic_pay; 
             $nestedData1['Grade Pay']=$recordsTo->grade_pay; 
             $nestedData1['Reason']=$recordsTo->prereason; 
            
             $data[]=$nestedData1; 
        }
        
        return \Excel::create($excelTitle, function($excel) use ($data) {
                    $excel->sheet(' ', function($sheet) use ($data) {
                        $sheet->fromArray($data);

                    });
                })->download('xlsx');
    }

}
