<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\tbl_assembly;
use App\tbl_assembly_zone;
use App\tbl_subdivision;
use App\tbl_block_muni;
use App\tbl_personnela;
use App\tbl_assembly_party;
use App\tbl_first_training_schedule;
use App\tbl_office;
use App\tbl_poststat;
use App\tbl_district;
use App\tbl_district_random_pwd;
use App\tbl_training_type;
use App\tbl_order_no_date;
use DB;
use Mail;
use App\tbl_user;
use App\Http\Controllers\FirstTrainingAllocationController;

class FirstRandController extends Controller {

    private $firstTrainingAllocation;

    public function getTrData(FirstTrainingAllocationController $firstTrainingAllocation) {
        $this->firstTrainingAllocation = $firstTrainingAllocation;
        $training_type = $this->firstTrainingAllocation->getTrainingGroupData();
        return view('first_letter_populate', compact('training_type'));
    }

    public function getApptTrData(FirstTrainingAllocationController $firstTrainingAllocation) {
        $this->firstTrainingAllocation = $firstTrainingAllocation;
        $training_type = $this->firstTrainingAllocation->getTrainingGroupData();
        return view('first_appt_letter', compact('training_type'));
    }

    public function getVenueTrData(FirstTrainingAllocationController $firstTrainingAllocation) {
        $this->firstTrainingAllocation = $firstTrainingAllocation;
        $training_type = $this->firstTrainingAllocation->getTrainingTypeData();
        return view('first_venue_pp_report', compact('training_type'));
    }

    public function getOffTrainingData(FirstTrainingAllocationController $firstTrainingAllocation) {
        $this->firstTrainingAllocation = $firstTrainingAllocation;
        $training_type = $this->firstTrainingAllocation->getTrainingGroupData();
        return view('first_sub_off_pp_report', compact('training_type'));
    }

    //::::::::First Randomisation(Check OTP or Password)(Lock/Unlock)::::::::// 
    public function check_for_status_password(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            $this->validate($request, [
                'districtcd' => 'required|alpha_num|min:2|max:2',
                'type' => 'required|digits_between:1,1'
                    ], [
                'districtcd.required' => 'Districtcd is required',
                'type.required' => 'Type is required'
            ]);

            try {
                //$districtcd=$request->districtcd;
                $type = $request->type;
                $dist = \Session::get('districtcd_ppds');
                $count = tbl_district_random_pwd::
                                where('districtcd', '=', $dist)
                                ->where('type', '=', $type)->value(DB::raw("status"));

                $response = array(
                    'options' => $count,
                    'status' => 1
                );
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }

    public function send_otp_password(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            $this->validate($request, [
                'otp_status' => 'required|alpha|min:4|max:4',
                'type' => 'required|digits_between:1,1'
                    ], [
                'otp_status.required' => 'OTP Status is required',
                'type.required' => 'Type is required'
            ]);

            try {
                $otp_status = $request->otp_status;
                $type = $request->type;
                $dist = \Session::get('districtcd_ppds');
                $usercode = \Session::get('code_ppds');
                if ($otp_status == "lock") {
                    $otpLR = "lock_otp";
                    $otpDate = "posted_date_lock_unlock";
                    $otpName = "Lock/Unlock";
                } else {
                    $otpLR = "rand_otp";
                    $otpDate = "posted_date_rand";
                    $otpName = "Randomisation";
                }
                $typeName = (($type == "1") ? "First" : (($type == "2") ? "Second" : "Third"));

                $messageC = $typeName . " " . $otpName . " OTP is :";
                $interval_count = tbl_district_random_pwd::where($otpDate, '>', DB::raw("DATE_SUB(NOW(), INTERVAL 1 HOUR)"))
                                ->where('districtcd', '=', $dist)
                                ->where('type', '=', $type)->value(DB::raw("count(*)"));
                if ($interval_count == 0) {
                    $mob_no = tbl_user::where('districtcd', '=', $dist)
                                    ->where('code', '=', $usercode)->value(DB::raw("mobileno"));
                    $email_id = tbl_user::where('districtcd', '=', $dist)
                                    ->where('code', '=', $usercode)->value(DB::raw("email"));
                    $rand = rand(1000, 9000);
                    date_default_timezone_set('Asia/Calcutta');
                    $dt = date_create('now')->format('Y-m-d H:i:s');
                    tbl_district_random_pwd::where('districtcd', '=', $dist)->where('type', '=', $type)
                            ->update([$otpLR => $rand, $otpDate => $dt]);
                    $messageBody = $messageC . $rand;

                    include_once("sms/test_sms_2.php");
//                   Mail::send([],['name','Anupam'],
//                            function($message) use ($email_id,$messageBody){
//                                $message->to($email_id)->subject('PPDS')->setBody($messageBody);
//                                $message->from('anupammondal221@gmail.com','CEO OFFICE');
//                             });                   
                    $count = 2;
                } else {
                    $count = 1;
                }

                $response = array(
                    'options' => $count,
                    'status' => 1
                );
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }

    public function resend_otp_password(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            $this->validate($request, [
                'otp_status' => 'required|alpha|min:4|max:4',
                'type' => 'required|digits_between:1,1'
                    ], [
                'otp_status.required' => 'OTP Status is required',
                'type.required' => 'Type is required'
            ]);

            try {
                $otp_status = $request->otp_status;
                $type = $request->type;
                $usercode = \Session::get('code_ppds');
                $dist = \Session::get('districtcd_ppds');
                if ($otp_status == "lock") {
                    $otpLR = "lock_otp";
                    $otpDate = "posted_date_lock_unlock";
                    $otpName = "Lock/Unlock";
                } else {
                    $otpLR = "rand_otp";
                    $otpDate = "posted_date_rand";
                    $otpName = "Randomisation";
                }
                $typeName = (($type == "1") ? "First" : (($type == "2") ? "Second" : "Third"));

                $messageC = $typeName . " " . $otpName . " OTP is :";

                $mob_no = tbl_user::where('districtcd', '=', $dist)
                                ->where('code', '=', $usercode)->value(DB::raw("mobileno"));
                $email_id = tbl_user::where('districtcd', '=', $dist)
                                ->where('code', '=', $usercode)->value(DB::raw("email"));
                $rand = tbl_district_random_pwd::where('districtcd', '=', $dist)
                                ->where('type', '=', $type)->value(DB::raw($otpLR));
//                    date_default_timezone_set('Asia/Calcutta'); 
//                    $dt = date_create('now')->format('Y-m-d H:i:s');
//                    tbl_district_random_pwd::where('districtcd','=',$dist)->where('type','=',$type)
//                         ->update([$otpLR=>$rand,$otpDate=>$dt]);
                $messageBody = $messageC . $rand;

                include_once("sms/test_sms_2.php");
//                   Mail::send([],['name','Anupam'],
//                            function($message) use ($email_id,$messageBody){
//                                $message->to($email_id)->subject('PPDS')->setBody($messageBody);
//                                $message->from('anupammondal221@gmail.com','CEO OFFICE');
//                             });                   
//                   
                $count = 1;
                $response = array(
                    'options' => $count,
                    'status' => 1
                );
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }

    public function check_for_lu_rand_otp(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            $this->validate($request, [
                'otp_status' => 'required|alpha|min:4|max:4',
                'otp' => 'required|digits_between:1,4',
                'type' => 'required|digits_between:1,1'
                    ], [
                'otp_status.required' => 'Status is required',
                'type.required' => 'Type is required',
                'OTP.required' => 'OTP is required',
                'otp.digits_between' => 'OTP must be numeric'
            ]);

            try {
                $otp_status = $request->otp_status;
                $otp = $request->otp;
                $type = $request->type;
                $dist = \Session::get('districtcd_ppds');
                if ($otp_status == "lock") {
                    $otpLR = "lock_otp";
                    $otpDate = "posted_date_lock_unlock";
                } else {
                    $otpLR = "rand_otp";
                    $otpDate = "posted_date_rand";
                }
                $count = tbl_district_random_pwd::where($otpLR, '=', $otp)
                                ->where('districtcd', '=', $dist)
                                ->where('type', '=', $type)->value(DB::raw("count(*)"));

                $response = array(
                    'options' => $count,
                    'status' => 1
                );
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }

    //::::::::First Randomisation(Lock/Unlock):::::::://   
    public function firstzone_wise_assembly(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            $this->validate($request, [
                'zone' => 'required|alpha_num|min:4|max:4',
                'forDist' => 'required|alpha_num|min:2|max:2'
                    ], [
                'zone.required' => 'Zone is required',
                'zone.alpha_num' => 'Zone must be an alpha numeric characters',
                'forDist.required' => 'District is required',
                'forDist.alpha_num' => 'District must be an alpha numeric characters'
            ]);
            $zone = $request->zone;
            $forDist = $request->forDist;
            $pccd = $request->pc;
            try {
                $filtered = "";
                $filtered = tbl_assembly_zone::join('assembly', 'assembly.assemblycd', '=', 'assembly_zone.assemblycd')
                        ->where('assembly_zone.districtcd', '=', $forDist)
                        ->select('assembly_zone.assemblycd', 'assembly_zone.rand_status1', 'assembly_zone.rand_status2', 'assembly_zone.rand_status3', 'assembly.assemblyname');

                if ($zone != "") {
                    $filtered = $filtered->where('assembly_zone.zone', '=', $zone);
                }
                if ($pccd != "") {
                    $filtered = $filtered->where('assembly.pccd', '=', $pccd);
                }
                $filtered = $filtered->groupBy('assembly_zone.assemblycd')->get();
                /* $data = array();
                  if (!empty($filtered)) {
                  foreach ($filtered as $post) {
                  $nestedData['assemblycd'] = $post->assemblycd;
                  $nestedData['assemblyname'] = $post->assemblyname;
                  $nestedData['rand_status1'] = $post->rand_status1;
                  $nestedData['status'] =  tbl_personnela::where('personnela.forassembly','=',$post->assemblycd)
                  ->where('personnela.forzone','=',$zone)
                  ->select(DB::raw('Count(personnela.personcd) as total'))
                  ->get();
                  $data[]=$nestedData;
                  }

                  } */
                // print_r($data);
                $response = array(
                    'options' => $filtered,
                    'status' => 1
                );
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }

    public function check_for_lu_password(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            $this->validate($request, [
                'password' => 'required|regex:/^[a-zA-Z0-9!$#%_@]+$/',
                'type' => 'required|digits_between:1,1'
                    ], [
                'password.required' => 'Password is required',
                'type.required' => 'Type is required'
            ]);

            try {
                $password = $request->password;
                $type = $request->type;
                $dist = \Session::get('districtcd_ppds');
                $count = tbl_district_random_pwd::where('lock_unlock_pwd', '=', md5($password))
                                ->where('districtcd', '=', $dist)
                                ->where('type', '=', $type)->value(DB::raw("count(*)"));

                $response = array(
                    'options' => $count,
                    'status' => 1
                );
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }

    public function firstrand_lock_unlock(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            $validate_array = ['zone' => 'required|alpha_num|min:4|max:4', 'districtcd' => 'required|alpha_num|min:2|max:2'];
            for ($x = 0; $x <= $request->row_count; $x++) {
                $validate_array['myCheck' . $x] = 'nullable|alpha_dash';
            }
            $validate_array1 = ['zone.required' => 'Zone is required',
                'zone.alpha_num' => 'Zone must be an alpha numeric characters',
                'districtcd.required' => 'District is required',
                'districtcd.alpha_num' => 'District must be an alpha numeric characters'];
            for ($y = 0; $y <= $request->row_count; $y++) {
                $validate_array1['myCheck' . $y . '.alpha_dash'] = 'Assembly must be an alpha numeric characters';
                //  $validate_array1['myCheck'. $y.'.unique'] = 'Already created some assembly(s) for this zone';
            }
            $this->validate($request, $validate_array, $validate_array1);

            try {
                $password = $request->password;
                $row_count = $request->row_count;
                $zone = $request->zone;
//              $filtered0=tbl_assembly_zone::where('assembly_zone.zone','=',$zone)
//                         ->where('assembly_zone.rand_status1','!=','Y')
//                         ->update(['rand_status1'=>'N']);
                $count = 0;
                $totalCount = 0;
                for ($i = 1; $i <= $row_count; $i++) {
                    $count1 = 0;
                    $asmcd = 'myCheck' . $i;
                    if ($request->$asmcd != "") {
                        $asmstat = explode('-', $request->$asmcd);
                        $asmcode = $asmstat[0];
                        $asmstatus = $asmstat[1];
                        if ($asmstatus == "A") {
                            $randstat = 'Y';
                        } else if ($asmstatus == "Y") {
                            $randstat = 'N';
                        } else if ($asmstatus == "N") {
                            $count1++;
                            $totalCount++;
                        }
                        if ($count1 == 0) {
                            tbl_assembly_zone::where('assembly_zone.assemblycd', '=', $asmcode)
                                    ->where('assembly_zone.zone', '=', $zone)
                                    ->update(['rand_status1' => $randstat]);
                            $count++;
                        }
                    }
                }
                $response = array(
                    'options' => $count,
                    'totalC' => $totalCount,
                    'status' => 1
                );
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }

    //:::::::::::::::::First Rand:::::::::::::::://
    public function check_for_rand_password(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            $this->validate($request, [
                'password' => 'required|regex:/^[a-zA-Z0-9!$#%_@]+$/',
                'type' => 'required|digits_between:1,1'
                    ], [
                'password.required' => 'Password is required',
                'type.required' => 'Type is required'
            ]);

            try {
                $password = $request->password;
                $type = $request->type;
                $dist = \Session::get('districtcd_ppds');
                $count = tbl_district_random_pwd::where('rand_pwd', '=', md5($password))
                                ->where('districtcd', '=', $dist)
                                ->where('type', '=', $type)->value(DB::raw("count(*)"));

                $response = array(
                    'options' => $count,
                    'status' => 1
                );
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }

    public function first_rand_zone_wise_assembly(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            $this->validate($request, [
                'zone' => 'required|alpha_num|min:4|max:4',
                'forDist' => 'required|alpha_num|min:2|max:2',
                'pc' => 'nullable|alpha_num|min:2|max:2'
                    ], [
                'zone.required' => 'Zone is required',
                'zone.alpha_num' => 'Zone must be an alpha numeric characters',
                'forDist.required' => 'District is required',
                'forDist.alpha_num' => 'District must be an alpha numeric characters',
                'pc.required' => 'PC is required',
                'pc.alpha_num' => 'PC must be an alpha numeric characters'
            ]);
            $zone = $request->zone;
            $forDist = $request->forDist;
            $pccd = $request->pc;
            $dataP = array();
            $dataR = array();
            $dist = \Session::get('districtcd_ppds');

            try {
                $filtered = tbl_assembly_zone::join('assembly', 'assembly.assemblycd', '=', 'assembly_zone.assemblycd')
                                ->join('assembly_party', 'assembly_party.assemblycd', '=', 'assembly.assemblycd')
                                ->join('reserve', function($join) {
                                    $join->on('reserve.forassembly', '=', 'assembly_party.assemblycd');
                                    $join->on('reserve.number_of_member', '=', 'assembly_party.no_of_member');
                                    $join->on('reserve.gender', '=', 'assembly_party.gender');
                                })->select('assembly_zone.assemblycd', 'assembly_zone.rand_status1', 'assembly.assemblyname', DB::raw("SUM(no_party) AS P"), DB::raw("SUM(case when no_or_pc ='P' then ROUND(no_party * numb/100) ELSE numb end) AS R"));
                if ($zone != "") {
                    $filtered = $filtered
                            ->where('assembly_zone.districtcd', '=', $forDist)
                            ->where('assembly_zone.zone', '=', $zone)
                            ->where('assembly_zone.rand_status1', '!=', 'Y');
                }
                if ($pccd != "") {
                    $filtered = $filtered->where('assembly.pccd', '=', $pccd);
                }
                $filtered = $filtered->groupBy('assembly_zone.assemblycd')->get();
                $data = array();
                if (!empty($filtered)) {
                    foreach ($filtered as $post) {
                        $nestedData['assemblycd'] = $post->assemblycd;
                        $nestedData['assemblyname'] = $post->assemblyname;
                        $nestedData['rand_status1'] = $post->rand_status1;
//                    $toPARTY = $post->P;
//                    $toRESERVE = $post->R;
//                    $nestedData['totRES']=$toRESERVE;
//                    $nestedData['totPAR']=$toPARTY;
//                    $shortfallREQ=$toPARTY+$toRESERVE;
//                    $partyReserve=tbl_personnela::where('forassembly','=',$post->assemblycd)
//                        ->where('forzone','=',$zone)
//                        ->where(function($q) {
//                           $q->orwhere('booked','=','P')
//                             ->orwhere('booked','=','R');})
//                        ->select('booked')
//                        ->get();
//                    
//                    $toparty=0;$toreserve=0;        
//                    if(!empty($partyReserve)){
//                       foreach ($partyReserve as $paRes) {
//                         //$totreq=$totreq+$asmParty->no_of_member*$asmParty->no_party;  
//                         if($paRes->booked=='P')
//                         {
//                             $toparty=$toparty+1;
//                         }
//                         else
//                         {
//                              $toreserve=$toreserve+1;
//                         }
//                       } 
//                    }
//                    $nestedData['toparty']=$toparty;
//                    $nestedData['toreserve']=$toreserve;
//                    $shortfallASS=$toparty+$toreserve;
//                    $nestedData['shortfall']=$shortfallREQ - $shortfallASS;
                        $data[] = $nestedData;
                    }
                }
//            if ($data[0]->P == NULL) {
//                $req_p = "0";
//            } else {
//                $req_p = $data[0]->P;
//            }
//            if ($data[0]->R == NULL) {
//                $req_r = "0";
//            } else {
//                $req_r = $data[0]->R;
//            }
//
//            array_push($dataP, $req_p);
//            array_push($dataR, $req_r);
//            
//             $data_req = [
//            'P' => $dataP,
//            'R' => $dataR
//            ];
//            try{
//            $filtered="";
//            $filtered = tbl_assembly_zone::join('assembly', 'assembly.assemblycd', '=', 'assembly_zone.assemblycd')
//                ->where('assembly_zone.districtcd', '=', $forDist)
//                ->where('assembly_zone.zone', '=', $zone) 
//                ->where('assembly_zone.rand_status1', '!=', 'Y') 
//    		->select('assembly_zone.assemblycd', 'assembly_zone.rand_status1','assembly_zone.rand_status2','assembly_zone.rand_status3','assembly.assemblyname')
//                ->get();
//            
//            $data = array();
//            if (!empty($filtered)) {
//                foreach ($filtered as $post) {
//                    $nestedData['assemblycd'] = $post->assemblycd;
//                    $nestedData['assemblyname'] = $post->assemblyname;
//                    $nestedData['rand_status1'] = $post->rand_status1;
//                    
//                    $ZoneRecord=DB::select('call ZoneDetailsReqiured(?,?)',[$zone,$post->assemblycd]);
//                    $totres=0;
//                    $toRESERVE=0;
//                    $toPARTY=0;
//                    $shortfallREQ=0;
//                    $shortfallASS=0;
//                    foreach ($ZoneRecord as $ZoneDetail){
//                        $membno=$ZoneDetail->memb;
//                        $n_o_p=$ZoneDetail->npc;
//                        $p_numb=$ZoneDetail->pnumb;
//                        $pst=$ZoneDetail->pst;
//                        $preqd=$ZoneDetail->ptyrqd;
//                       
//                        if(strcmp($n_o_p,'N')==0)
//                        {
//                            $totres=$p_numb;
//                        }
//                        else
//                        {
//                            $totres=round($p_numb*$preqd/100,0);
//                        }
//                        $toRESERVE=$totres+$toRESERVE;
//                        $toPARTY=$preqd+$toPARTY;
//                    }
//                    $nestedData['totRES']=$toRESERVE;
//                    $nestedData['totPAR']=$toPARTY;
//                    $shortfallREQ=$toPARTY+$toRESERVE;
//                    $partyReserve=tbl_personnela::where('forassembly','=',$post->assemblycd)
//                        ->where('forzone','=',$zone)
//                        ->where(function($q) {
//                           $q->orwhere('booked','=','P')
//                             ->orwhere('booked','=','R');})
//                        ->select('booked')
//                        ->get();
//                    
//                    $toparty=0;$toreserve=0;        
//                    if(!empty($partyReserve)){
//                       foreach ($partyReserve as $paRes) {
//                         if($paRes->booked=='P')
//                         {
//                             $toparty=$toparty+1;
//                         }
//                         else
//                         {
//                              $toreserve=$toreserve+1;
//                         }
//                       } 
//                    }
//                    $nestedData['toparty']=$toparty;
//                    $nestedData['toreserve']=$toreserve;
//                    $shortfallASS=$toparty+$toreserve;
//                    $nestedData['shortfall']=$shortfallREQ-$shortfallASS;
//                  $data[]=$nestedData;  
//                }
//            }
                // print_r($data);
                $response = array(
                    'options' => $data,
                    'status' => 1
                );
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }

    public function firstrand(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            $validate_array = ['zone' => 'required|alpha_num|min:4|max:4', 'districtcd' => 'required|alpha_num|min:2|max:2',
                'pc' => 'required|alpha_num|min:2|max:2'];
            for ($x = 0; $x <= $request->row_count; $x++) {
                $validate_array['myCheck' . $x] = 'nullable|alpha_dash';
            }
            $validate_array1 = ['zone.required' => 'Zone is required',
                'zone.alpha_num' => 'Zone must be an alpha numeric characters',
                'districtcd.required' => 'District is required',
                'districtcd.alpha_num' => 'District must be an alpha numeric characters',
                'pc.required' => 'PC is required',
                'pc.alpha_num' => 'PC must be an alpha numeric characters'];
            for ($y = 0; $y <= $request->row_count; $y++) {
                $validate_array1['myCheck' . $y . '.alpha_dash'] = 'Assembly must be an alpha numeric characters';
                //  $validate_array1['myCheck'. $y.'.unique'] = 'Already created some assembly(s) for this zone';
            }
            $this->validate($request, $validate_array, $validate_array1);

            try {
                $row_count = $request->row_count;
                $zone = $request->zone;
                $districtcd = $request->districtcd;
                $pccd = $request->pc;
                $filtered0 = tbl_assembly_zone::where('assembly_zone.zone', '=', $zone)
                        ->where('assembly_zone.rand_status1', '!=', 'Y')
                        ->update(['rand_status1' => 'N']);
                $count = 0;
                for ($i = 1; $i <= $row_count; $i++) {
                    $asmcd = 'myCheck' . $i;
                    if ($request->$asmcd != "") {
                        $asmstat = explode('-', $request->$asmcd);
                        $asmcode = $asmstat[0];
                        $asmstatus = $asmstat[1];
                        $randstat = 'A';

                        $filtered = tbl_assembly_zone::where('assembly_zone.assemblycd', '=', $asmcode)
                                ->where('assembly_zone.zone', '=', $zone)
                                ->update(['rand_status1' => $randstat]);
                        $count++;
                    }
                }

                DB::select('call randomisation1(?,?,?)', [$zone, $districtcd, $pccd]);
                $session_personnela = session()->get('personnela_ppds');
                $tbl_personnela1 = new tbl_personnela();
                $recordsPost = $tbl_personnela1->join('assembly_zone', '' . $session_personnela . '.forassembly', '=', 'assembly_zone.assemblycd')
                                ->where('selected', '=', 1)
                                ->where('assembly_zone.zone', '=', $zone)
                                ->where('assembly_zone.rand_status1', '=', 'A')
                                ->groupBy($session_personnela . '.poststat')
                                ->orderBy($session_personnela . '.poststat')
                                ->select(DB::raw('Count(personcd) as cnt'), '' . $session_personnela . '.poststat')->get();
                $data = "<table width='100%'>";
                $rcount = 0;

                foreach ($recordsPost as $recordsPstat) {
                    //$nestedData[$recordsPstat->poststat]=$recordsPstat->cnt;
                    if ($rcount % 4 == 0) {
                        $data .= "<tr><td style='font-size:13px;'>";
                    } else {
                        $data .= " :: ";
                    }
                    $data .= $recordsPstat->poststat . "";
                    $data .= " => " . $recordsPstat->cnt . " ";
                    $rcount++;
                    if ($rcount % 4 == 0) {
                        $data .= "</tr></td>";
                    }
                }

                $data .= "</table>";
                // $data[]=$nestedData;
//                foreach($ZoneAvailable as $zv){
//                  $ptable.="<td style='padding:0px;border-collapse: collapse;'>";
//                  $ptable.="<table width='100%'>";
//                  $ptable.="<tr><td>".$zv->poststat."</td></tr>";
//                  $ptable.="<tr><td>".$zv->total."</td></tr>";
//                  $ptable.="</table>";
//                  $ptable.="</td>";
//                }
                // print_r($data);
                //  $pcount=$personnelCheck[0]->t_Count;
                //   print($firstRand[0]);
                //                  $filteredUp=tbl_assembly_zone::where('assembly_zone.rand_status1','=','A')
                //                         ->where('assembly_zone.zone','=',$zone)
                //                         ->update(['rand_status1'=>'N']);
                // echo $filteredUp;
                $response = array(
                    'options' => $count,
                    'datas' => $data,
                    'status' => 1
                );
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }

    //:::::::::::::::::::::First Appointment Letter Populate:::::::::://
    public function getOrderNoDate(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            $this->validate($request, [
                'forDist' => 'required|alpha_num|min:2|max:2'
                    ], [
                'forDist.required' => 'District is required',
                'forDist.alpha_num' => 'District must be an alpha numeric'
            ]);
            try {
                $forDist = $request->forDist;
                $list = '';
                $trainingData = tbl_order_no_date::where('districtcd', '=', $forDist)
                                ->select('order_no', DB::raw("Date_Format(order_dt, '%d/%m/%Y') As tr_date"), 'order_cd')->get();
                $trArray = json_decode($trainingData);
                $list .= "<option value>[Select]</option>";
                foreach ($trArray as $trDate) {
                    $list .= "<option value='" . $trDate->order_cd . "'>" . $trDate->tr_date . " - " . $trDate->order_no . "</option>";
                }
                $response = array(
                    'options' => $list,
                    'status' => 1);
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }

    public function first_appt_letter_populate(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            $this->validate($request, [
                'zone' => 'required|alpha_num|min:4|max:4',
                'districtcd' => 'required|alpha_num|min:2|max:2',
                'trainingtype' => 'required|digits_between:1,1',
                'orderno' => 'required|alpha_num|min:4|max:4'
                    ], [
                'zone.required' => 'Zone is required',
                'zone.alpha_num' => 'Zone must be an alpha numeric characters',
                'districtcd.required' => 'District is required',
                'districtcd.alpha_num' => 'District must be an alpha numeric characters',
                'trainingtype.required' => 'Training Head is required',
                'trainingtype.digits_between' => 'Training Head must be numeric',
                'orderno.required' => 'Order Date & No is required',
                'orderno.alpha_num' => 'Order Date & No be an alpha numeric characters'
            ]);

            try {
                $zone = $request->zone;
                $districtcd = $request->districtcd;
                $trainingHead = $request->trainingtype;
                $orderno = $request->orderno;
                $count = 0;
                $pcount = 0;
                $session_personnela = session()->get('personnela_ppds');
//                $trTypeArray = tbl_training_type::where('training_group_cd','=',$trainingHead)
//                     ->orderBy('training_code')->pluck('training_code')->toArray();
//                $dataAr=array();
//                foreach($trTypeArray as $trTypeArray1){
//                    $dataAr[]=$trTypeArray1;
//                }
//                if(count($dataAr[0])>0){
                $firstRand = DB::select('call first_appt_populate(?,?,?,?,?)', [$zone, $districtcd, $session_personnela, $trainingHead, $orderno]);
                $pcount = $firstRand[0]->t_Count;
//                }
//                if(count($dataAr[1])>0){
//                     
//                }
                //  $firstRand=DB::select('call first_appt_populate(?,?,?,?)',[$zone,$districtcd,$trainingHead,$session_personnela]);
                //  $pcount=$firstRand[0]->t_Count;
                if ($pcount > 0) {
                    DB::select('call first_rand_loop_slno_token(?,?)', [$zone, $trainingHead]);
                }
//              print($firstRand[0]);
//              $filteredUp=tbl_assembly_zone::where('assembly_zone.rand_status1','=','A')
//                         ->where('assembly_zone.zone','=',$zone)
//                         ->update(['rand_status1'=>'N']);
//              echo $filteredUp;
                $response = array(
                    'options' => $pcount,
                    'status' => 1
                );
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }

    //:::::::::::::::::: Subdivision/Blockmuni/Office user wise (Appt Letter)::::::::::::::://
    public function getSubdivisionDataUserwise(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            $this->validate($request, [
                'forDist' => 'required|alpha_num|min:2|max:2'
                    ], [
                'forDist.required' => 'District is required',
                'forDist.alpha_num' => 'District code must be an alpha numeric'
            ]);
            $forDist = $request->forDist;
            try {
                $filtered = "";
                $subdiv_sessioncd = session()->get('subdivisioncd_ppds');
                $tbl_personnela = new tbl_personnela();
                if ($subdiv_sessioncd != NULL || $subdiv_sessioncd != "") {
                    $tbl_personnela = $tbl_personnela->where('subdivision.subdivisioncd', '=', $subdiv_sessioncd);
                }
                $session_personnela = session()->get('personnela_ppds');
                $filtered = $tbl_personnela
                                ->join('subdivision', 'subdivision.subdivisioncd', '=', DB::raw("SUBSTRING(" . $session_personnela . ".personcd,1,4)"))
                                ->where('fordistrict', '=', $forDist)
                                ->where('selected', '=', 1)
                                ->groupBy('subdivision.subdivisioncd')
                                ->pluck('subdivision.subdivision', 'subdivision.subdivisioncd')->all();
//              $filtered = $subdivision->where('districtcd','=', $forDist)
//                           ->pluck('subdivision', 'subdivisioncd')->all();
                $response = array(
                    'options' => $filtered,
                    'status' => 1
                );
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }

    public function getBlockMuniUserwise(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            $this->validate($request, [
                'forDist' => 'required|alpha_num|min:2|max:2',
                'subdivision' => 'required|alpha_num|min:4|max:4',
                'trainingtype' => 'nullable|digits_between:1,1'
                    ], [
                'forDist.required' => 'District is required',
                'forDist.alpha_num' => 'District code must be an alpha numeric',
                'subdivision.required' => 'Subdivision is required',
                'subdivision.alpha_num' => 'Subdivision must be an alpha numeric',
                'trainingtype.digits_between' => 'Training Head must be numeric',
            ]);
            try {
                $subdivision = $request->subdivision;
                $forDist = $request->forDist;
                $trainingtype = $request->trainingtype;
                $list = '';
                $blockmuni_sessioncd = session()->get('blockmunicd_ppds');
                $session_personnela = session()->get('personnela_ppds');
                $tbl_personnela = new tbl_personnela();
                if ($blockmuni_sessioncd != NULL || $blockmuni_sessioncd != "") {
                    $tbl_personnela = $tbl_personnela->where('block_muni.blockminicd', '=', $blockmuni_sessioncd);
                }
                if ($trainingtype != '') {
                    $tbl_personnela = $tbl_personnela->where('' . $session_personnela . '.training_gr_cd', '=', $trainingtype);
                }

                $blockmuni = $tbl_personnela
                                ->join('office', '' . $session_personnela . '.officecd', '=', 'office.officecd')
                                ->join('block_muni', 'office.blockormuni_cd', '=', 'block_muni.blockminicd')
                                ->where('' . $session_personnela . '.fordistrict', '=', $forDist)
                                ->where('selected', '=', 1)
                                ->where(DB::raw("SUBSTRING(" . $session_personnela . ".personcd,1,4)"), '=', $subdivision)
                                ->groupBy('block_muni.blockminicd')
                                ->pluck('block_muni.blockmuni', 'block_muni.blockminicd')->all();


//                $blockmuni = $tbl_block_muni->where('subdivisioncd','=', $subdivision)->pluck('blockmuni', 'blockminicd')->all();

                $list .= "<option value>[Select]</option>";
                foreach ($blockmuni as $key => $value) {
                    $list .= "<option value='" . $key . "'>" . $value . "</option>";
                }
                $response = array(
                    'options' => $list,
                    'status' => 1);
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }

    public function getOfficeNameUserwise(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            $this->validate($request, [
                'forDist' => 'required|alpha_num|min:2|max:2',
                'subdivision' => 'required|alpha_num|min:4|max:4',
                'blockmuni' => 'nullable|alpha_num|min:6|max:6',
                'trainingtype' => 'nullable|digits_between:1,1'
                    ], [
                'forDist.required' => 'District is required',
                'forDist.alpha_num' => 'District code must be an alpha numeric',
                'subdivision.required' => 'Subdivision is required',
                'subdivision.alpha_num' => 'Subdivision must be an alpha numeric',
                'blockmuni.alpha_num' => 'Block/Municipality must be an alpha numeric',
                'trainingtype.digits_between' => 'Training Head must be numeric',
            ]);
            try {
                $subdivision = $request->subdivision;
                $blockmuni = $request->blockmuni;
                $forDist = $request->forDist;
                $trainingtype = $request->trainingtype;
                $officeName = $request->officeName;
                $office = '';
                $blockmuni_sessioncd = session()->get('blockmunicd_ppds');
                $tbl_personnela = new tbl_personnela();
                if ($blockmuni_sessioncd != NULL || $blockmuni_sessioncd != "") {
                    $tbl_personnela = $tbl_personnela->where('office.blockormuni_cd', '=', $blockmuni_sessioncd);
                }
                if ($blockmuni != '') {
                    $tbl_personnela = $tbl_personnela->where('office.blockormuni_cd', '=', $blockmuni);
                }
                if ($officeName != '') {
                    $tbl_personnela = $tbl_personnela->where('office.officecd', '=', $officeName);
                }
                $session_personnela = session()->get('personnela_ppds');
                $office = $tbl_personnela
                                ->join('office', '' . $session_personnela . '.officecd', '=', 'office.officecd')
                                ->where('' . $session_personnela . '.fordistrict', '=', $forDist)
                                ->where('' . $session_personnela . '.training_gr_cd', '=', $trainingtype)
                                ->where('selected', '=', 1)
                                ->where(DB::raw("SUBSTRING(" . $session_personnela . ".personcd,1,4)"), '=', $subdivision)
                                ->groupBy('office.officecd')
                                ->pluck('office.office', 'office.officecd')->all();
//                $office=$office->pluck('office', 'officecd')->all();               
                // $data_sub = view('ajax.stream_wise_subject', compact('subjects'))->render();
                $response = array(
                    'options' => $office,
                    'status' => 1);
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }

    //::::::::::::::::::::::: First Appt Letter PDF :::::::::::::::::::://
    public function getFirstRecordAvailable(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            $this->validate($request, [
                'districtcd' => 'required|alpha_num|min:2|max:2',
                'subdivision' => 'nullable|alpha_num|min:4|max:4',
                'blockMunicipality' => 'nullable|alpha_num|min:6|max:6',
                'officeName' => 'nullable|alpha_num|min:10|max:10',
                'PersonnelId' => 'nullable|alpha_num|min:11|max:11',
                'trainingtype' => 'nullable|digits_between:1,1'
                    ], [
                'districtcd.required' => 'District is required',
                'districtcd.alpha_num' => 'District code must be an alpha numeric',
                'subdivision.alpha_num' => 'Subdivision must be an alpha numeric',
                'blockMunicipality.alpha_num' => 'Block/Municipality must be an alpha numeric',
                'officeName.alpha_num' => 'Office Code must be an alpha numeric',
                'PersonnelId.alpha_num' => 'Personnel ID must be an alpha numeric',
                'trainingtype.digits_between' => 'Training Head must be integer'
            ]);


            try {
                $districtcd = $request->districtcd;
                $subdivision = $request->subdivision;
                $blockcd = $request->blockMunicipality;
                $officeName = $request->officeName;
                $personnelId = $request->PersonnelId;
                $trainingHead = $request->trainingtype;
                $subdiv_sessioncd = session()->get('subdivisioncd_ppds');
                $blockmuni_sessioncd = session()->get('blockmunicd_ppds');
                if ($subdiv_sessioncd != NULL || $subdiv_sessioncd != "") {
                    $subdivision = $subdiv_sessioncd;
                }

                if ($blockmuni_sessioncd != NULL || $blockmuni_sessioncd != "") {
                    $blockcd = $blockmuni_sessioncd;
                }
                $max_code = DB::select('call FirstApptLetterRecords(?,?,?,?,?,?)', [$districtcd, $subdivision, $blockcd, $officeName, $personnelId, $trainingHead]);
                $count = $max_code[0]->cnt;
                $response = array(
                    'options' => $count,
                    'status' => 1
                );
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }

    //::::::::::::::::::::::: Download First Appt Letter Excel Format::::::::::::::::::::://
    public function getFirstApptExcel(Request $request) {

        $this->validate($request, [
            'districtcd' => 'required|alpha_num|min:2|max:2',
            'subdivision' => 'nullable|alpha_num|min:4|max:4',
            'blockMunicipality' => 'nullable|alpha_num|min:6|max:6',
            'officeName' => 'nullable|alpha_num|min:10|max:10',
            'PersonnelId' => 'nullable|alpha_num|min:11|max:11',
            'trainingtype' => 'nullable|digits_between:1,1',
            'from' => 'required|digits_between:1,4',
            'to' => 'required|digits_between:1,4'
                ], [
            'districtcd.required' => 'District is required',
            'districtcd.alpha_num' => 'District code must be an alpha numeric',
            'subdivision.alpha_num' => 'Subdivision must be an alpha numeric',
            'blockMunicipality.alpha_num' => 'Block/Municipality must be an alpha numeric',
            'officeName.alpha_num' => 'Office Code must be an alpha numeric',
            'PersonnelId.alpha_num' => 'Personnel ID must be an alpha numeric',
            'trainingtype.digits_between' => 'Training Head must be an integer',
            'from.required' => 'Records From is required',
            'from.digits_between' => 'Records From must be an integer',
            'to.required' => 'Records To is required',
            'to.digits_between' => 'Records To must be an integer'
        ]);


        try {
            $districtcd = $request->districtcd;
            $subdivision = $request->subdivision;
            $blockcd = $request->blockMunicipality;
            $officeName = $request->officeName;
            $personnelId = $request->PersonnelId;
            $trainingHead = $request->trainingtype;
            $pfrom = $request->from;
            $pto = $request->to;
            $subdiv_sessioncd = session()->get('subdivisioncd_ppds');
            $blockmuni_sessioncd = session()->get('blockmunicd_ppds');
            if ($subdiv_sessioncd != NULL || $subdiv_sessioncd != "") {
                $subdivision = $subdiv_sessioncd;
            }

            if ($blockmuni_sessioncd != NULL || $blockmuni_sessioncd != "") {
                $blockcd = $blockmuni_sessioncd;
            }
            $data = array();
            $ApptRecord = DB::select('call FirstApptLetter(?,?,?,?,?,?,?,?)', [$districtcd, $subdivision, $blockcd, $officeName, $personnelId, $trainingHead, $pfrom, $pto]);
            foreach ($ApptRecord as $ApptDetail) {
                //$nestedData['#SL']=$ApptDetail->sl_no;
                $nestedData['Token'] = $ApptDetail->token;
//                    $nestedData['PC Code']=$ApptDetail->forpc;
//                    $nestedData['PC Name']=$ApptDetail->pcname;
                $nestedData['PID'] = $ApptDetail->personcd;
                $nestedData['Person Name'] = $ApptDetail->officer_name;
                $nestedData['Person Designation'] = $ApptDetail->person_desig;
                $nestedData['Office Code'] = $ApptDetail->officecd;
                $nestedData['Office Name'] = $ApptDetail->office;
                $nestedData['Office Address'] = $ApptDetail->address;
                $nestedData['Post Office'] = $ApptDetail->postoffice;
                $nestedData['Subdivision'] = $ApptDetail->subdivision;
                $nestedData['BlockMuni Code'] = $ApptDetail->block_muni;
                $nestedData['BlockMuni Name'] = $ApptDetail->block_muni_name;
                //$nestedData['District Code']=$ApptDetail->fordistrict;
                $nestedData['District Name'] = $ApptDetail->district;
                $nestedData['Pin'] = $ApptDetail->pin;
                $nestedData['Poststat'] = $ApptDetail->poststat;
                $nestedData['Post Status'] = $ApptDetail->post_stat;
                $nestedData['Mobile No'] = $ApptDetail->mob_no;

                $nestedData['Epic'] = $ApptDetail->epic;
//                    $nestedData['AC NO']=$ApptDetail->acno;
                $nestedData['PART NO'] = $ApptDetail->partno;
                $nestedData['SL NO'] = $ApptDetail->slno;

//                    $nestedData['Bank']=$ApptDetail->bank_name;
//                    $nestedData['Branch']=$ApptDetail->branch_name;
                $nestedData['A/C No'] = $ApptDetail->bank_acc_no;
                $nestedData['IFSC'] = $ApptDetail->ifsc_code;

                $nestedData['(1st)Training Type'] = $ApptDetail->training_desc;
                $nestedData['(1st)Venue Name'] = $ApptDetail->venuename . " , " . $ApptDetail->sub_venuename;
                $nestedData['(1st)Venue Address'] = $ApptDetail->venueaddress;
                $nestedData['(1st)Training Date'] = $ApptDetail->training_dt;
                $nestedData['(1st)Training Time'] = $ApptDetail->training_time;

                $nestedData['(2nd)Training Type'] = $ApptDetail->training_desc_2;
                $nestedData['(2nd)Venue Name'] = $ApptDetail->venuename_2 . " , " . $ApptDetail->sub_venuename_2;
                $nestedData['(2nd)Venue Address'] = $ApptDetail->venueaddress_2;
                $nestedData['(2nd)Training Date'] = $ApptDetail->training_dt_2;
                $nestedData['(2nd)Training Time'] = $ApptDetail->training_time_2;

                $nestedData['Order No'] = $ApptDetail->order_no;
                $nestedData['Order Date'] = $ApptDetail->order_date;
                $data[] = $nestedData;
            }

            return \Excel::create('1stApptExcel', function($excel) use ($data) {
                        $excel->sheet(' ', function($sheet) use ($data) {
                            $sheet->fromArray($data);
                        });
                    })->download('xlsx');
        } catch (\Exception $e) {
            $response = array(
                'exception' => true,
                'exception_message' => $e->getMessage(),
            );
            // $statusCode = 400;
            return $response;
        }
    }

    //::::::::::::::::::::::: First Appt letter (PC Election):::::::::::::::::::::::::::::://
    public function getFirstAppointmentLetterPDF2019(Request $request) {
        $this->validate($request, [
            'districtcd' => 'required|alpha_num|min:2|max:2',
            'subdivision' => 'nullable|alpha_num|min:4|max:4',
            'blockMunicipality' => 'nullable|alpha_num|min:6|max:6',
            'officeName' => 'nullable|alpha_num|min:10|max:10',
            'PersonnelId' => 'nullable|alpha_num|min:11|max:11',
            'trainingtype' => 'nullable|digits_between:1,1',
            'from' => 'required|digits_between:1,5',
            'to' => 'required|digits_between:1,5'
                ], [
            'districtcd.required' => 'District is required',
            'districtcd.alpha_num' => 'District code must be an alpha numeric',
            'subdivision.alpha_num' => 'Subdivision must be an alpha numeric',
            'blockMunicipality.alpha_num' => 'Block/Municipality must be an alpha numeric',
            'officeName.alpha_num' => 'Office Code must be an alpha numeric',
            'PersonnelId.alpha_num' => 'Personnel ID must be an alpha numeric',
            'trainingtype.digits_between' => 'Training Head must be an integer',
            'from.required' => 'Records From is required',
            'from.digits_between' => 'Records From must be an integer',
            'to.required' => 'Records To is required',
            'to.digits_between' => 'Records To must be an integer'
        ]);
        $districtcd = $request->districtcd;
        $subdivision = $request->subdivision;
        $blockcd = $request->blockMunicipality;
        $officeName = $request->officeName;
        $personnelId = $request->PersonnelId;
        $trainingHead = $request->trainingtype;
        $pfrom = $request->from;
        $pto = $request->to;

        $subdiv_sessioncd = session()->get('subdivisioncd_ppds');
        $blockmuni_sessioncd = session()->get('blockmunicd_ppds');
        if ($subdiv_sessioncd != NULL || $subdiv_sessioncd != "") {
            $subdivision = $subdiv_sessioncd;
        }

        if ($blockmuni_sessioncd != NULL || $blockmuni_sessioncd != "") {
            $blockcd = $blockmuni_sessioncd;
        }

        $ApptRecord = DB::select('call FirstApptLetter(?,?,?,?,?,?,?,?)', [$districtcd, $subdivision, $blockcd, $officeName, $personnelId, $trainingHead, $pfrom, $pto]);
        $pdf = app('FPDF');
        $pdf->AddPage();
        $count = 0;
        $per_page = 1;
        $signaturePath = config('constants.DEOSIGNATURE');
        foreach ($ApptRecord as $ApptDetail) {
            if ($count < $per_page) {
                $fill = false;
                $euname = "ELECTION URGENT";
                $euname1 = "ORDER OF APPOINTMENT FOR TRAINING";
                $euname2 = "BYE-ELECTION TO THE WEST BENGAL LEGISTATIVE ASSEMBLY - 2019";
                $euname3 = "Token No. " . $ApptDetail->token;
                $euname4 = "Order No: " . $ApptDetail->order_no;
                $euname5 = "Date: " . date('d/m/Y', strtotime(trim(str_replace('/', '-', $ApptDetail->order_date))));
                $euname6 = "          In pursuance of Section 26 of the Representation of the People Act 1951 I do hereby  appoint the officer specified below as " . $ApptDetail->poststatus . " for the polling station to be informed later in connection with the conduct from 43 - Habibpur (ST) Assembly Constituency.";
                //$euname8="$row[forpc]-$row[pcname] PC";			
                $euname9 = "The officer should report for training as per the following schedule .";


                $euname10 = "Now the officer shall be under control, superintendence and discipline of the Election Commission of India till election is over as per";
                $euname11 = "provision of Section 28A of RP Act 1951.";
                $euname12 = "The officer should comply the under noted instruction.";
                $euname13 = "Place: " . $ApptDetail->fordist_name;
                $euname14 = "Date: " . date('d/m/Y', strtotime(trim(str_replace('/', '-', $ApptDetail->order_date))));
                //$districtName = tbl_district::where('districtcd','=',$ApptDetail->fordistrict)->value('district');
                $euname15 = "District: " . $ApptDetail->fordist_name;
                $nb1 = "Please fill up Form 12/12A (for postal ballot / EDC) annexed herewith and submit at the Training Venue on the 1st day of training along with";
                $nb2 = "the duplicate copy of your appointment letter and a copy of EPIC.";
                $nb3 = "Please indicate your PIN Number as given in your appointment letter on the body of Form 12/12A to help us locate you for delivery of";
                $nb4 = "postal ballot.Also indicate your EPIC Number on the body of Form 12/12A for verification of your Electoral roll entry.";
                $nb5 = "Please fill up the blank identity card sent herewith and paste your recent colour photograph and bring it at training venue for";
                $nb6 = "attestation.";
                //  $nb7="If the mobile no. is not submitted or incorrect, please inform PP cell.";
                $nb7 = "Please check your mobile number, electoral data and bank account details given below. If any correction is needed, correct with";
                $nb8 = "red ink and submit the same at training venue.";
                $nb9 = "Mobile Number. - $ApptDetail->mob_no";
                $nb10 = "EPIC No. - $ApptDetail->epic, Assembly - $ApptDetail->acno, Part No. - $ApptDetail->partno, Sl. No .- $ApptDetail->slno ";
                //$nb89="Bank - $ApptDetail->bank_name, Branch - $ApptDetail->branch_name";
                $nb11 = "A/c No.- $ApptDetail->bank_acc_no, IFSC - $ApptDetail->ifsc_code";

                $pdf->SetFont('Arial', 'B', 8);
                $pdf->Cell(30, 5, $euname, 1, 0, 'L');
                $pdf->SetFont('Arial', 'B', 10);
                $pdf->Cell(50);
                $pdf->Cell(40, 6, $euname1, 0, 0, 'C');
                $pdf->SetFont('Arial', 'B', 8);
                $pdf->Cell(58);
                $pdf->Cell(10, 7, "", 0, 0, 'R');

                // Line break
                $pdf->Ln(5);

                $pdf->SetFont('Arial', 'B', 8);

                $pdf->Cell(15, 10, $euname4, 0, 0, 'L');
                //$pdf->SetFont('Arial', 'B', 6.5);
                $pdf->SetFont('Arial','BU',6.5);
                $pdf->Cell(60);
                $pdf->Cell(50, 4, $euname2, '', 0, 'C');
                $pdf->SetFont('Arial', 'B', 8);
                $pdf->Cell(50);
                $pdf->Cell(10, 10, $euname5, 0, 0, 'R');
                // Line break
                $pdf->Ln(16);

                $pdf->SetFont('Arial', '', 8.7);
                $pdf->MultiCell(190, 4, $euname6, '', 'J');

                $pdf->Ln();


                $pdf->SetFillColor(255, 255, 255);
                $pdf->SetDrawColor(0, 0, 0);
                $pdf->SetLineWidth(.3);
                $pdf->SetFont('', 'B');

                $head = array('Name of Polling Officer');
                $w = array(190);
                //	$this->SetFont('Arial','',9);
                for ($j = 0; $j < count($head); $j++) {
                    $pdf->Cell($w[$j], 7, $head[$j], 1, 0, 'C', true);
                }
                $pdf->Ln();

                $name = "" . $ApptDetail->officer_name . ", " . $ApptDetail->person_desig . ", PIN - " . $ApptDetail->personcd . " ";
                //$name1=$row['0'].", ".$row['1'].", PIN - (".$row['2'].") ";
                $address = "  " . $ApptDetail->office . ", " . $ApptDetail->address . ", P.O. - " . $ApptDetail->postoffice;
                $ppo = " Subdivision - " . $ApptDetail->subdivision . ",  District. - " . $ApptDetail->district . ", PIN - " . $ApptDetail->pin;
                $odetails = "  OFFICE - (" . $ApptDetail->officecd . "), Post Status - " . $ApptDetail->poststatus . ", Mobile No : " . $ApptDetail->mob_no;
                $pdf->SetFont('Arial', 'B', 10);
                $pdf->Cell($w[0], 5, $name, 'LR', 0, 'L', $fill);
                $pdf->Ln();
                $pdf->SetFont('Arial', '', 8);
                $pdf->Cell($w[0], 3, '', 'LR', 'L');
                $pdf->Ln();
                $pdf->MultiCell($w[0], 4, $address . $ppo, 'LR', 'J');

                $pdf->Cell($w[0], 6, $odetails, 'LRB', 0, 'L', $fill);
                $pdf->Ln(5);

                $pdf->SetFont('Arial', '', 8.7);
                $pdf->Cell(50, 10, $euname9, 0, 0, 'L');

                // Line break
                $pdf->Ln();

                $pdf->SetFont('', 'B');
                $header1 = array('Training Schedule');
                $header2 = array('Training', 'Venue & Address', 'Date & Time');
                $w1 = array(190);
                $w2 = array(35, 125, 30);
                $w3 = array(35, 125, 30);
                for ($k = 0; $k < count($header1); $k++) {
                    $pdf->Cell($w1[$k], 7, $header1[$k], 1, 0, 'C', true);
                }
                $pdf->Ln();
                for ($l = 0; $l < count($header2); $l++) {
                    $pdf->Cell($w2[$l], 7, $header2[$l], 1, 0, 'C', true);
                }
                $pdf->Ln();
//                for($m=0;$m<count($header2);$m++){
//                        $pdf->Cell($w3[$m],7,$header2[$m],1,0,'C',true);
//                }
                $pdf->SetFont('Arial', '', 7.5);

                $pdf->Cell($w2[0], 5, $ApptDetail->training_desc, 'LTR', 0, 'L', $fill);
                $pdf->Cell($w2[1], 5, $ApptDetail->venuename . "-" . $ApptDetail->sub_venuename, 'LTR', 0, 'L', $fill);
                $pdf->Cell($w2[2], 5, $ApptDetail->training_dt, 'LTR', 0, 'L', $fill);
                $pdf->Ln(4);
                $pdf->Cell($w2[0], 6, '', 'LR', 0, 'L', $fill);
                $pdf->Cell($w2[1], 6, $ApptDetail->venueaddress, 'LR', 0, 'L', $fill);
                $pdf->Cell($w2[2], 6, $ApptDetail->training_time, 'LR', 0, 'L', $fill);
                $pdf->Ln();
                $pdf->Cell(array_sum($w2), 0, '', 1, 0, 'L', $fill);
                $pdf->Ln();
                if ($ApptDetail->training_desc_2 != "") {
                    $pdf->Cell($w3[0], 5, $ApptDetail->training_desc_2, 'LTR', 0, 'L', $fill);
                    $pdf->Cell($w3[1], 5, $ApptDetail->venuename_2 . "-" . $ApptDetail->sub_venuename_2, 'LTR', 0, 'L', $fill);
                    $pdf->Cell($w3[2], 5, $ApptDetail->training_dt_2, 'LTR', 0, 'L', $fill);
                    $pdf->Ln(4);
                    $pdf->Cell($w3[0], 6, '', 'LR', 0, 'L', $fill);
                    $pdf->Cell($w3[1], 6, $ApptDetail->venueaddress_2, 'LR', 0, 'L', $fill);
                    $pdf->Cell($w3[2], 6, $ApptDetail->training_time_2, 'LR', 0, 'L', $fill);
                    $pdf->Ln();
                    $pdf->Cell(array_sum($w3), 0, '', 1, 0, 'L', $fill);
                    $pdf->Ln();
                }

//                $pdf->SetFont('Arial','',8.7);
//                $pdf->Cell(50,10,$euname10,0,0,'L');

                $pdf->SetFont('Arial', '', 9);

                $pdf->Cell(50, 10, $euname10, 0, 0, 'L');
                $pdf->Ln(6);
                $pdf->SetFont('Arial', '', 9);
                $pdf->Cell(50, 10, $euname11, 0, 0, 'L');

                // Line break
                $pdf->Ln(6);

                $pdf->SetFont('Arial', '', 8.7);
                $pdf->Cell(50, 10, $euname12, 0, 0, 'L');

                // Line break
                $pdf->Ln(11);

                $pdf->SetFont('Arial', '', 9);
                //$pdf->Cell(80);
                $pdf->Cell(30, 10, $euname13, 0, 0, 'L');
                $pdf->SetFont('Arial', '', 10);
                $pdf->Cell(120);
                $pdf->Cell(10, 10, "Signature", 0, 0, 'R');
                // Line break
                $pdf->Ln(9);

                $signature = $signaturePath . $districtcd . ".jpg";
                $pdf->SetFont('Arial', '', 9);
                $pdf->Cell(30, 1, $euname14, 0, 0, 'L');
                $pdf->SetFont('Arial', '', 10);
                $pdf->Cell(110);
                //	$this->Cell(10,10,"yuyu",0,0,'R');
                $pdf->Cell(10, 10, $pdf->Image($signature, $pdf->GetX(), $pdf->GetY(), 30.78), 0, 0, 'R', false);
                // Line break
                $pdf->Ln(5);

                $pdf->SetFont('Arial', '', 9.6);
                $pdf->Cell(160);
                $pdf->Cell(10, 12, "District Election Officer", 0, 0, 'R');

                // Line break
                $pdf->Ln(4);
                $pdf->SetFont('Arial', '', 9);
                $pdf->Cell(129);
                $pdf->Cell(36, 12, $euname15, 0, 0, 'R');

                // Line break
                $pdf->Ln();
                $pdf->Cell(190, 0, '', 1, 0, 'L', $fill);
                $pdf->Ln();

                $pdf->SetFont('Arial', '', 10);
                //$pdf->Cell(160);
                $pdf->Cell(10, 10, "NB.", 0, 0, 'L');
                $pdf->Ln(4);

                $pdf->SetFont('Arial', '', 9);
                $pdf->Cell(10, 10, "1.", 0, 0, 'L');
                $pdf->Cell(10, 10, $nb1, 0, 0, 'L');
                $pdf->Ln(4);
                $pdf->SetFont('Arial', '', 9);
                $pdf->Cell(10, 10, "", 0, 0, 'L');
                $pdf->Cell(10, 10, $nb2, 0, 0, 'L');
                $pdf->Ln(5);

                $pdf->SetFont('Arial', '', 9);
                $pdf->Cell(10, 10, "2.", 0, 0, 'L');
                $pdf->Cell(10, 10, $nb3, 0, 0, 'L');
                $pdf->Ln(4);
                $pdf->SetFont('Arial', '', 9);
                $pdf->Cell(10, 10, "", 0, 0, 'L');
                $pdf->Cell(10, 10, $nb4, 0, 0, 'L');
                $pdf->Ln(5);

                $pdf->SetFont('Arial', '', 9);
                $pdf->Cell(10, 10, "3.", 0, 0, 'L');
                $pdf->Cell(10, 10, $nb5, 0, 0, 'L');
                $pdf->Ln(4);
                $pdf->SetFont('Arial', '', 9);
                $pdf->Cell(10, 10, "", 0, 0, 'L');
                $pdf->Cell(10, 10, $nb6, 0, 0, 'L');
                $pdf->Ln(5);

                $pdf->SetFont('Arial', '', 9);
                $pdf->Cell(10, 10, "4.", 0, 0, 'L');
                $pdf->Cell(10, 10, $nb7, 0, 0, 'L');
                $pdf->Ln(4);
                $pdf->SetFont('Arial', '', 9);
                $pdf->Cell(10, 10, "", 0, 0, 'L');
                $pdf->Cell(10, 10, $nb8, 0, 0, 'L');
                $pdf->Ln(4);
                $pdf->SetFont('Arial', '', 9);
                $pdf->Cell(10, 10, "a)", 0, 0, 'R');
                $pdf->Cell(10, 10, $nb9, 0, 0, 'L');
                $pdf->Ln(4);
//                $pdf->SetFont('Arial','',9);
//                $pdf->Cell(10,10,"",0,0,'L');
//                $pdf->Cell(10,10,$nb89,0,0,'L');
//                $pdf->Ln(4);

                $pdf->SetFont('Arial', '', 9);
                $pdf->Cell(10, 10, "b)", 0, 0, 'R');
                $pdf->Cell(10, 10, $nb10, 0, 0, 'L');
                $pdf->Ln(4);
                $pdf->SetFont('Arial', '', 9);
                $pdf->Cell(10, 10, "c)", 0, 0, 'R');
                $pdf->Cell(10, 10, $nb11, 0, 0, 'L');
                $pdf->Ln();
                //$pdf->Cell(190,0,'',1,0,'L',$fill);
                //$pdf->Ln(2);
                $pdf->Cell(190, 0, '- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -', 0, 0, 'L', $fill);
                $pdf->Ln();
                $pdf->SetFont('Arial', '', 9);
                $pdf->Cell(10, 10, "Copy to DDO / Head of Office to serve the Letter and submit service return.", 0, 0, 'L');
                $pdf->Ln();
                //	$pdf->Cell(190,0,'',1,0,'L',$fill);
                //	$pdf->Ln(2);
                $pdf->Cell(190, 0, '- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -', 0, 0, 'L', $fill);
                $pdf->Ln(12);

                $pdf->SetFont('Arial', '', 9);
                $pdf->Cell(30, 10, "Receipt of Appointment Letter", 0, 0, 'L');
                $pdf->SetFont('Arial', '', 10);
                $pdf->Cell(140);
                $pdf->Cell(10, 10, "Signature of the Recipient", 0, 0, 'R');
                $pdf->Ln(4);

                $pdf->SetFont('Arial', '', 9);
                $pdf->Cell(136);
                $pdf->Cell(10, 10, "Date:", 0, 0, 'R');
                $pdf->Ln();

                $pdf->SetFont('Arial', '', 9);
                $pdf->Cell(190, 5, "Block/Municipality: " . $ApptDetail->block_muni_name, 0, 0, 'C');
                $count++;
            }
            if ($count == $per_page) {
                $per_page = $per_page + 1;
                if ($count != Count($ApptRecord)) {
                    $pdf->AddPage();
                }
            }
        }
        $pdf->Output();
        exit;
    }

    //::::::::::::::::::::::: First Appt letter (Assembly Election) ::::::::::::::::::::::://
    public function getFirstAppointmentLetterPDF(Request $request) {
        $this->validate($request, [
            'districtcd' => 'required|alpha_num|min:2|max:2',
            'subdivision' => 'nullable|alpha_num|min:4|max:4',
            'blockMunicipality' => 'nullable|alpha_num|min:6|max:6',
            'officeName' => 'nullable|alpha_num|min:10|max:10',
            'PersonnelId' => 'nullable|alpha_num|min:11|max:11',
            'trainingtype' => 'nullable|alpha_num|min:2|max:2',
            'from' => 'required|digits_between:1,4',
            'to' => 'required|digits_between:1,4'
                ], [
            'districtcd.required' => 'District is required',
            'districtcd.alpha_num' => 'District code must be an alpha numeric',
            'subdivision.alpha_num' => 'Subdivision must be an alpha numeric',
            'blockMunicipality.alpha_num' => 'Block/Municipality must be an alpha numeric',
            'officeName.alpha_num' => 'Office Code must be an alpha numeric',
            'PersonnelId.alpha_num' => 'Personnel ID must be an alpha numeric',
            'trainingtype.alpha_num' => 'Training Type must be an alpha numeric',
            'from.required' => 'Records From is required',
            'from.digits_between' => 'Records From must be an integer',
            'to.required' => 'Records To is required',
            'to.digits_between' => 'Records To must be an integer'
        ]);
        $districtcd = $request->districtcd;
        $subdivision = $request->subdivision;
        $blockcd = $request->blockMunicipality;
        $officeName = $request->officeName;
        $personnelId = $request->PersonnelId;
        $trainingtype = $request->trainingtype;
        $pfrom = $request->from;
        $pto = $request->to;

        $subdiv_sessioncd = session()->get('subdivisioncd_ppds');
        $blockmuni_sessioncd = session()->get('blockmunicd_ppds');
        if ($subdiv_sessioncd != NULL || $subdiv_sessioncd != "") {
            $subdivision = $subdiv_sessioncd;
        }

        if ($blockmuni_sessioncd != NULL || $blockmuni_sessioncd != "") {
            $blockcd = $blockmuni_sessioncd;
        }

        function Header() {
            
        }

        // Page footer
        function Footer() {
            
        }

        $ApptRecord = DB::select('call FirstApptLetter(?,?,?,?,?,?,?,?)', [$districtcd, $subdivision, $blockcd, $officeName, $personnelId, $trainingtype, $pfrom, $pto]);
        $pdf = app('FPDF');
        $pdf->AddPage();
        $count = 0;
        $per_page = 1;
        $signature = config('constants.DEOSIGNATURE');
        foreach ($ApptRecord as $ApptDetail) {
            if ($count < $per_page) {
                $fill = false;
                $euname = "ELECTION URGENT";
                $euname1 = "ORDER OF APPOINTMENT FOR TRAINING";
                $euname2 = "GENERAL ELECTION TO WEST BENGAL LEGISLATIVE ASSEMBLY ELECTION, 2016";
                $euname3 = "Token No. ";
                $euname4 = "Order No: ";
                $euname5 = "Date: ";
                $euname6 = "          In exercise of the power conferred upon vide Section 26 of the R. P. Act, 1951, I do hereby appoint the officer specified below as " . $ApptDetail->poststatus . " for undergoing training in connection with the conduct of General Election to West Bengal Legistlative Assembly Election, 2016.";
                //$euname8="$row[forpc]-$row[pcname] PC";			
                $euname9 = "The Officer should report for Training as per following Schedule.";
                $euname10 = "This is a compulsory duty on your part to attend the said programme, as per the provisions of The Representation of the People Act, 1951.";
                $euname11 = "You are directed to bring your Elector's Photo Identity Card (EPIC) or any proof of Identity affixed with your Photograph.";
                $euname12 = "Place: ";
                $euname13 = "Date: ";
                $euname14 = "District ";
                $nb1 = "Please fillup Form 12 (for postal ballot) annexed herewith and submit at the Training Venue on the 1st day of training along with";
                $nb2 = "the duplicate copy of your appointment letter and a copy of EPIC or any other ID including service ID.";
                $nb3 = "Please indicate your PIN Number as given in your appointment letter on the body of Form 12 to help us locate you for delivery of";
                $nb4 = "postal ballot.Also indicate your EPIC Number on the body of Form 12 for verification of your Electoral roll entry.";
                $nb5 = "Please fill up the blank identity card sent herewith and paste your recent colour photograph and bring it at training venue for";
                $nb6 = "attestation.";
                $nb7 = "If the mobile no. is not submitted or incorrect, please inform PP cell.";
                $nb8 = "Please check your electoral data and bank details given below. For any inconsistancy please inform PP Cell.";
                $nb9 = "EPIC No. - $ApptDetail->epic, Assembly - $ApptDetail->acno, Part No. - $ApptDetail->partno, Sl. No .- $ApptDetail->slno ";
                $nb89 = "Bank - $ApptDetail->bank_name, Branch - $ApptDetail->branch_name";
                $nb10 = "A/c No.- $ApptDetail->bank_acc_no, IFS Code- $ApptDetail->ifsc_code";

                $pdf->SetFont('Arial', 'B', 8);
                $pdf->Cell(30, 5, $euname, 1, 0, 'L');
                $pdf->SetFont('Arial', 'B', 10);
                $pdf->Cell(50);
                $pdf->Cell(40, 6, $euname1, 0, 0, 'C');
                $pdf->SetFont('Arial', 'B', 8);
                $pdf->Cell(58);
                $pdf->Cell(10, 7, $euname3, 0, 0, 'R');

                // Line break
                $pdf->Ln(5);

                $pdf->SetFont('Arial', 'B', 8);
                $pdf->Cell(15, 10, $euname4, 0, 0, 'L');
                $pdf->SetFont('Arial', 'B', 6.5);
                $pdf->Cell(38);
                $pdf->Cell(92, 4, $euname2, 'B', 0, 'C');
                $pdf->SetFont('Arial', 'B', 8);
                $pdf->Cell(36);
                $pdf->Cell(10, 10, $euname5, 0, 0, 'R');
                // Line break
                $pdf->Ln(16);

                $pdf->SetFont('Arial', '', 8.7);
                $pdf->MultiCell(190, 4, $euname6, '', 'J');

                $pdf->Ln();
                $pdf->Ln(7);

                $pdf->SetFillColor(255, 255, 255);
                $pdf->SetDrawColor(0, 0, 0);
                $pdf->SetLineWidth(.3);
                $pdf->SetFont('', 'B');

                $head = array('Name of Polling Officer');
                $w = array(190);
                //	$this->SetFont('Arial','',9);
                for ($j = 0; $j < count($head); $j++)
                    $pdf->Cell($w[$j], 7, $head[$j], 1, 0, 'C', true);

                $pdf->Ln();

                $name = "" . $ApptDetail->officer_name . ", " . $ApptDetail->person_desig . "PID - " . $ApptDetail->personcd . " ";
                //$name1=$row['0'].", ".$row['1'].", PIN - (".$row['2'].") ";
                $address = "  " . $ApptDetail->office . ", " . $ApptDetail->address . ", P.O. - " . $ApptDetail->postoffice;
                $ppo = " Subdiv - " . $ApptDetail->subdivision . ",  Dist. - " . $ApptDetail->district . ", PIN - " . $ApptDetail->pin;
                $odetails = "  OFFICE - (" . $ApptDetail->officecd . ")Post Status - " . $ApptDetail->poststatus . " Mobile No : " . $ApptDetail->mob_no;
                $pdf->SetFont('Arial', 'B', 10);
                $pdf->Cell($w[0], 5, $name, 'LR', 0, 'L', $fill);
                $pdf->Ln();
                $pdf->SetFont('Arial', '', 8);
                $pdf->Cell($w[0], 3, '', 'LR', 'L');
                $pdf->Ln();
                $pdf->MultiCell($w[0], 4, $address . $ppo, 'LR', 'J');

                $pdf->Cell($w[0], 6, $odetails, 'LRB', 0, 'L', $fill);
                $pdf->Ln(5);

                $pdf->SetFont('Arial', '', 8.7);
                $pdf->Cell(50, 10, $euname9, 0, 0, 'L');

                // Line break
                $pdf->Ln();

                $pdf->SetFont('', 'B');
                $header1 = array('Training Schedule');
                $header2 = array('Training', 'Venue & Address', 'Date & Time');
                $w1 = array(190);
                $w2 = array(24, 136, 30);
                for ($k = 0; $k < count($header1); $k++)
                    $pdf->Cell($w1[$k], 7, $header1[$k], 1, 0, 'C', true);
                $pdf->Ln();
                for ($l = 0; $l < count($header2); $l++)
                    $pdf->Cell($w2[$l], 7, $header2[$l], 1, 0, 'C', true);
                $pdf->Ln();

                $pdf->SetFont('Arial', '', 7.5);

                $pdf->Cell($w2[0], 5, $ApptDetail->training_desc, 'LTR', 0, 'L', $fill);
                $pdf->Cell($w2[1], 5, $ApptDetail->venuename . "-" . $ApptDetail->sub_venuename, 'LTR', 0, 'L', $fill);
                $pdf->Cell($w2[2], 5, $ApptDetail->training_dt, 'LTR', 0, 'L', $fill);
                $pdf->Ln(4);
                $pdf->Cell($w2[0], 6, '', 'LR', 0, 'L', $fill);
                $pdf->Cell($w2[1], 6, $ApptDetail->venueaddress, 'LR', 0, 'L', $fill);
                $pdf->Cell($w2[2], 6, $ApptDetail->training_time, 'LR', 0, 'L', $fill);
                $pdf->Ln();
                $pdf->Cell(array_sum($w2), 0, '', 1, 0, 'L', $fill);
                $pdf->Ln();

                $pdf->SetFont('Arial', '', 8.7);
                $pdf->Cell(50, 10, $euname10, 0, 0, 'L');

                // Line break
                $pdf->Ln(6);

                $pdf->SetFont('Arial', '', 8.7);
                $pdf->Cell(50, 10, $euname11, 0, 0, 'L');

                // Line break
                $pdf->Ln(11);

                $pdf->SetFont('Arial', '', 9);
                //$pdf->Cell(80);
                $pdf->Cell(30, 10, $euname12, 0, 0, 'L');
                $pdf->SetFont('Arial', '', 10);
                $pdf->Cell(120);
                $pdf->Cell(10, 10, "Signature", 0, 0, 'R');
                // Line break
                $pdf->Ln(9);

                $pdf->SetFont('Arial', '', 9);
                $pdf->Cell(30, 1, $euname13, 0, 0, 'L');
                $pdf->SetFont('Arial', '', 10);
                $pdf->Cell(110);
                //	$this->Cell(10,10,"yuyu",0,0,'R');
                $pdf->Cell(10, 10, $pdf->Image($signature, $pdf->GetX(), $pdf->GetY(), 30.78), 0, 0, 'R', false);
                // Line break
                $pdf->Ln(5);

                $pdf->SetFont('Arial', '', 9.6);
                $pdf->Cell(160);
                $pdf->Cell(10, 12, "District Election Officer", 0, 0, 'R');

                // Line break
                $pdf->Ln(4);
                $pdf->SetFont('Arial', '', 9);
                $pdf->Cell(129);
                $pdf->Cell(40, 12, $euname14, 0, 0, 'R');

                // Line break
                $pdf->Ln();
                $pdf->Cell(190, 0, '', 1, 0, 'L', $fill);
                $pdf->Ln();

                $pdf->SetFont('Arial', '', 10);
                //$pdf->Cell(160);
                $pdf->Cell(10, 10, "NB.", 0, 0, 'L');
                $pdf->Ln(4);

                $pdf->SetFont('Arial', '', 9);
                $pdf->Cell(10, 10, "1.", 0, 0, 'L');
                $pdf->Cell(10, 10, $nb1, 0, 0, 'L');
                $pdf->Ln(4);
                $pdf->SetFont('Arial', '', 9);
                $pdf->Cell(10, 10, "", 0, 0, 'L');
                $pdf->Cell(10, 10, $nb2, 0, 0, 'L');
                $pdf->Ln(5);

                $pdf->SetFont('Arial', '', 9);
                $pdf->Cell(10, 10, "2.", 0, 0, 'L');
                $pdf->Cell(10, 10, $nb3, 0, 0, 'L');
                $pdf->Ln(4);
                $pdf->SetFont('Arial', '', 9);
                $pdf->Cell(10, 10, "", 0, 0, 'L');
                $pdf->Cell(10, 10, $nb4, 0, 0, 'L');
                $pdf->Ln(5);

                $pdf->SetFont('Arial', '', 9);
                $pdf->Cell(10, 10, "3.", 0, 0, 'L');
                $pdf->Cell(10, 10, $nb5, 0, 0, 'L');
                $pdf->Ln(4);
                $pdf->SetFont('Arial', '', 9);
                $pdf->Cell(10, 10, "", 0, 0, 'L');
                $pdf->Cell(10, 10, $nb6, 0, 0, 'L');
                $pdf->Ln(5);

                $pdf->SetFont('Arial', '', 9);
                $pdf->Cell(10, 10, "4.", 0, 0, 'L');
                $pdf->Cell(10, 10, $nb7, 0, 0, 'L');
                $pdf->Ln(5);
                $pdf->SetFont('Arial', '', 9);
                $pdf->Cell(10, 10, "5.", 0, 0, 'L');
                $pdf->Cell(10, 10, $nb8, 0, 0, 'L');
                $pdf->Ln(4);
                $pdf->SetFont('Arial', '', 9);
                $pdf->Cell(10, 10, "", 0, 0, 'L');
                $pdf->Cell(10, 10, $nb9, 0, 0, 'L');
                $pdf->Ln(4);
                $pdf->SetFont('Arial', '', 9);
                $pdf->Cell(10, 10, "", 0, 0, 'L');
                $pdf->Cell(10, 10, $nb89, 0, 0, 'L');
                $pdf->Ln(4);

                $pdf->SetFont('Arial', '', 9);
                $pdf->Cell(10, 10, "", 0, 0, 'L');
                $pdf->Cell(10, 10, $nb10, 0, 0, 'L');
                $pdf->Ln();
                //$pdf->Cell(190,0,'',1,0,'L',$fill);
                //$pdf->Ln(2);
                $pdf->Cell(190, 0, '- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -', 0, 0, 'L', $fill);
                $pdf->Ln();
                $pdf->SetFont('Arial', '', 9);
                $pdf->Cell(10, 10, "Copy to DDO / Head of Office to serve the Letter and submit the service return.", 0, 0, 'L');
                $pdf->Ln();
                //	$pdf->Cell(190,0,'',1,0,'L',$fill);
                //	$pdf->Ln(2);
                $pdf->Cell(190, 0, '- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -', 0, 0, 'L', $fill);
                $pdf->Ln(12);

                $pdf->SetFont('Arial', '', 9);
                $pdf->Cell(30, 10, "Receipt of Appointment Letter", 0, 0, 'L');
                $pdf->SetFont('Arial', '', 10);
                $pdf->Cell(140);
                $pdf->Cell(10, 10, "Signature of the Recepient", 0, 0, 'R');
                $pdf->Ln(4);

                $pdf->SetFont('Arial', '', 9);
                $pdf->Cell(136);
                $pdf->Cell(10, 10, "Date:", 0, 0, 'R');
                $pdf->Ln();

                $pdf->SetFont('Arial', '', 9);
                $pdf->Cell(190, 5, "Block/Municipality: " . $ApptDetail->block_muni_name, 0, 0, 'C');
                $count++;
            }
            if ($count == $per_page) {
                $per_page = $per_page + 1;
                if ($count != Count($ApptRecord)) {
                    $pdf->AddPage();
                }
            }
        }

//        $pdf->SetFont('Arial','B',18);
//        $pdf->Cell(0,10,"Title",0,"","C");
//        $pdf->Ln();
//        $pdf->Ln();
//        $pdf->SetFont('Arial','B',12);
//        $pdf->cell(25,8,"ID",1,"","C");
//        $pdf->cell(45,8,"Name",1,"","L");
//        $pdf->cell(35,8,"Address",1,"","L");
//        $pdf->Ln();
//        $pdf->SetFont("Arial","",10);
//        $pdf->cell(25,8,"1",1,"","C");
//        $pdf->cell(45,8,$request->subdivision,1,"","L");
//        $pdf->cell(35,8,"New York",1,"","L");
//        $pdf->Ln();

        $pdf->Output();
        exit;
    }

    //::::::::::::::::::::::: First Appt Letter PDF (Office):::::::::::::::::::://
    public function getFirstSubOffPPReport(Request $request) {
        $this->validate($request, [
            'subdivision' => 'required|alpha_num|min:4|max:4',
            'officeName' => 'nullable|alpha_num|min:10|max:10',
            'from' => 'required|digits_between:1,4',
            'to' => 'required|digits_between:1,4',
            'trainingtype' => 'required|digits_between:1,1'
                ], [
            'subdivision.required' => 'Subdivision is required',
            'subdivision.alpha_num' => 'Subdivision must be an alpha numeric',
            'officeName.alpha_num' => 'Office Code must be an alpha numeric',
            'from.digits_between' => 'Records From must be an integer',
            'to.digits_between' => 'Records To must be an integer',
            'trainingtype.required' => 'Training head is required',
            'trainingtype.digits_between' => 'training head code should be 1 digit'
        ]);
        $trainingtype = $request->trainingtype;
        $subdivision = $request->subdivision;
        $officeName = $request->officeName;
        $from = $request->from;
        $to = $request->to;
        $pfrom = $from - 1;
        $pto = $to - $from + 1;
        $tbl_office = new tbl_office();
        $session_personnela = session()->get('personnela_ppds');
        if ($officeName != '') {
            $tbl_office = $tbl_office->where('' . $session_personnela . '.officecd', '=', $officeName);
        }
        if ($trainingtype != '') {
            $tbl_office = $tbl_office->where('' . $session_personnela . '.training_gr_cd', '=', $trainingtype);
        }
        if ($pfrom != '-1') {
            $tbl_office = $tbl_office->orderBy('office.blockormuni_cd')
                   ->orderBy('office.officecd')
                     ->orderBy('' . $session_personnela . '.personcd')
                            ->offset($pfrom)->take($pto);
        } else {
            $tbl_office = $tbl_office->orderBy('office.subdivisioncd', 'office.blockormuni_cd', 'office.officecd', 'DESC');
        }

        $ApptRecord = $tbl_office->join('subdivision', 'subdivision.subdivisioncd', '=', 'office.subdivisioncd')
                ->join('district', 'district.districtcd', '=', 'office.districtcd')
                ->join('block_muni', 'block_muni.blockminicd', '=', 'office.blockormuni_cd')
                ->Leftjoin('policestation', 'policestation.policestationcd', '=', 'office.policestn_cd')
                ->join('' . $session_personnela . '', '' . $session_personnela . '.officecd', '=', 'office.officecd')
                ->where(DB::raw('SUBSTRING(' . $session_personnela . '.personcd,1,4)'), '=', $subdivision)
                ->where('' . $session_personnela . '.selected', '=', 1)
                ->groupBy('office.officecd', 'office.officer_desg', 'office.office', 'office.address1', 'office.address2', 'office.postoffice', 'subdivision.subdivision', 'policestation.policestation', 'district.district', 'office.pin', 'block_muni.blockmuni')
                //->where(''.$session_personnela.'.officecd','=',$officeName)
                //->with('ppDetailBrief') 
                ->select('office.officecd', 'office.officer_desg as designation', 'office.office', 'office.address1', 'office.address2', 'office.postoffice', 'subdivision.subdivision', 'policestation.policestation', 'district.district', 'office.pin', 'block_muni.blockmuni')
                ->get();
        $subV = json_decode($ApptRecord);

        $pdf = app('FPDF');
        $pdf->AddPage();
        $count = 0;
        $per_page = 1;
        foreach ($subV as $subTe) {
            if ($count < $per_page) {
                $fill = false;
                $office_dtl = "OFFICE: (" . $subTe->officecd . "), " . $subTe->office . ", " . $subTe->address1 . ", " . $subTe->address2;
                $office_dtl1 = " P.O. - " . $subTe->postoffice . ", Subdivision - " . $subTe->subdivision . ", Block/Muni - " . $subTe->blockmuni . ", P.S. - " . $subTe->policestation . ", Dist. - " . $subTe->district . ", PIN - " . $subTe->pin;
                $pdf->Ln();
                $pdf->SetFont('Arial', 'B', 7.3);
                $pdf->MultiCell(190, 4, $office_dtl . $office_dtl1, 'LTR', 'J');

                $pdf->SetFillColor(255, 255, 255);
                //	$this->SetTextColor(0,0,0);
                $pdf->SetDrawColor(0, 0, 0);
                $pdf->SetLineWidth(.3);
                $pdf->SetFont('Arial', 'B', 8);

                $head = array('PIN', 'Name', 'Designation', 'Posting Status');
                $w = array(25, 60, 77, 28);
                //	$this->SetFont('Arial','',9);
                for ($j = 0; $j < count($head); $j++)
                    $pdf->Cell($w[$j], 7, $head[$j], 1, 0, 'C', true);

                $pdf->Ln();

                // $subTe_array=$subTe->pp_detail_brief;
                $tbl_personnela = new tbl_personnela();
                $subTe_array = $tbl_personnela->join('personnel', 'personnel.personcd', '=', '' . $session_personnela . '.personcd')
                                ->join('poststat', 'poststat.post_stat', '=', '' . $session_personnela . '.poststat')
                                ->where('selected', '=', 1)
                                ->where('' . $session_personnela . '.training_gr_cd', '=', $trainingtype)
                                ->where('' . $session_personnela . '.officecd', '=', $subTe->officecd)
                                ->select('personnel.officecd', '' . $session_personnela . '.personcd', 'personnel.officer_name', 'personnel.off_desg as designation', 'poststat.poststatus')
                                ->orderBy('' . $session_personnela . '.personcd')->get();
//                print_r($subTe_array);
//                exit;
                $subTe_array_count = count($subTe_array);
                if ($subTe_array_count > 0) {
                    foreach ($subTe_array as $stud_sub) {

                        $pdf->SetFont('', '', 6);
                        $pdf->Cell($w[0], 6, $stud_sub->personcd, 'LRT', 0, 'L', $fill);
                        $pdf->Cell($w[1], 6, $stud_sub->officer_name, 'LRT', 0, 'L', $fill);
                        $pdf->Cell($w[2], 6, $stud_sub->designation, 'LRT', 0, 'L', $fill);
                        $pdf->Cell($w[3], 6, $stud_sub->poststatus, 'LRT', 0, 'L', $fill);
                        //count1++;

                        $pdf->Ln();
                        $pdf->Cell(array_sum($w), 0, '', 1, 0, 'L', $fill);
                        $pdf->Ln();
                    }
                }
                $pdf->Ln(10);
                $pdf->SetFont('Arial', 'B', 9);
                $pdf->Cell(50, 5, "Head of the office signature  .................................", 0, 0, 'L');
                $fill = !$fill;
                $count++;
            }
            if ($count == $per_page) {
                $per_page = $per_page + 1;
                if ($count != Count($subV)) {
                    $pdf->AddPage();
                }
            }
        }
        $pdf->Output();
        exit;
    }

    //::::::::::::::::::::::: First Appt Letter PDF (Venue):::::::::::::::::::://
    public function getFirstVenuePPReport(Request $request) {
        $this->validate($request, [
            'subdivision' => 'required|alpha_num|min:4|max:4',
            'venuename' => 'required|alpha_num|min:6|max:6',
            'trainingtype' => 'required|alpha_num|min:2|max:2',
            'trainingdatetime' => 'required|alpha_num|min:4|max:4'
                ], [
            'subdivision.required' => 'Subdivision is required',
            'subdivision.alpha_num' => 'Subdivision must be an alpha numeric',
            'venuename.required' => 'Venuename is required',
            'venuename.alpha_num' => 'Venuename must be an alpha numeric characters',
            'trainingtype.required' => 'Training Type is required',
            'trainingtype.alpha_num' => 'Training Type must be an alpha numeric characters',
            'trainingdatetime.required' => 'Training Date & Time is required',
            'trainingdatetime.alpha_num' => 'Training Date & Time must be an alpha numeric characters'
        ]);
        $subdivision = $request->subdivision;
        $venuename = $request->venuename;
        $trainingtype = $request->trainingtype;
        $trainingdatetime = $request->trainingdatetime;
        /* $pfrom=$from-1;
          $pto=$to-$from+1; */

        $tr_array = config('constants.PP_TRAINING');
        $tr_Name = $tr_array[$trainingtype];
        $tbl_first_training_schedule = new tbl_first_training_schedule;
        if ($tr_Name == "1stTrainingSch") {
            $tbl_first_training_schedule = $tbl_first_training_schedule->with('ppDetailVenueBrief');
        } else {
            $tbl_first_training_schedule = $tbl_first_training_schedule->with('ppDetailVenueBrief2');
        }
        $ApptRecord = $tbl_first_training_schedule->join('first_training_venue', 'first_training_venue.venue_cd', '=', 'first_training_schedule.tr_venue_cd')
                ->join('first_training_date_time', 'first_training_date_time.datetime_cd', '=', 'first_training_schedule.datetimecd')
                ->join('first_training_subvenue', 'first_training_subvenue.subvenue_cd', '=', 'first_training_schedule.tr_subvenue_cd')
                ->where('first_training_schedule.tr_venue_cd', '=', $venuename)
                ->where('first_training_schedule.datetimecd', '=', $trainingdatetime)
                ->where('first_training_schedule.training_type', '=', $trainingtype)
                ->where('first_training_schedule.subdivisioncd', '=', $subdivision)
                //->where(''.$session_personnela.'.officecd','=',$officeName)
                ->select('first_training_schedule.schedule_code', 'first_training_venue.venuename', 'first_training_venue.venueaddress', 'first_training_date_time.training_dt', 'first_training_date_time.training_time', 'first_training_subvenue.subvenue')
                ->get();
        $subV = json_decode($ApptRecord);


        $pdf = app('FPDF');
        $pdf->AddPage();
        $count = 0;
        $per_page = 1;
        foreach ($subV as $subTe) {
            if ($count < $per_page) {
                $fill = false;
                $received_date = date('d/m/Y', strtotime(trim(str_replace('-', '/', $subTe->training_dt))));
                $venue = "VENUE : " . $subTe->venuename . ", " . $subTe->subvenue . ", " . $subTe->venueaddress;
                $venue1 = "on " . $received_date . " from " . $subTe->training_time;

                $pdf->Ln();
                $pdf->SetFont('Arial', 'B', 8);
                $pdf->Cell(195, 5, $venue, "LTR", 0, 'L');
                $pdf->Ln(4);
                $pdf->Cell(195, 5, $venue1, "LR", 0, 'L');
                $pdf->Ln();

                $pdf->SetFillColor(255, 255, 255);
                //	$this->SetTextColor(0,0,0);
                $pdf->SetDrawColor(0, 0, 0);
                $pdf->SetLineWidth(.3);
                $pdf->SetFont('Arial', 'B', 8);

                $head = array('SL#', 'PIN', 'Name', 'Designation', 'Posting Status', 'Mobile No', 'Signature');
                //    $header1 = array('','','','','','AC / Part /Sl No.','');
                $w = array(8, 15, 43, 47, 22, 16, 44);
                $pdf->SetFont('Arial', '', 7.9);
                for ($j = 0; $j < count($head); $j++)
                    $pdf->Cell($w[$j], 7, $head[$j], 'LTR', 0, 'L', true);
                $pdf->Ln();
                // for($l=0;$l<count($header1);$l++)
                //   $pdf->Cell($w[$l],4,$header1[$l],'LR',0,'L',true);
                //  $pdf->Ln();
                if ($tr_Name == "1stTrainingSch") {
                    $subTe_array = $subTe->pp_detail_venue_brief;
                } else {
                    $subTe_array = $subTe->pp_detail_venue_brief2;
                }
                $subTe_array_count = count($subTe_array);
                if ($subTe_array_count > 0) {
                    $count1 = 0;
                    $per_page1 = 35;
                    foreach ($subTe_array as $stud_sub) {
                        if ($count1 < $per_page1) {
                            $count1++;
                            // $enrolmentjoin = $stud_sub->acno."/".$stud_sub->partno."/".$stud_sub->slno;
                            $pdf->SetFont('', '', 6);
                            $pdf->Cell($w[0], 7, $count1, 'LRT', 0, 'L', $fill);
                            $pdf->Cell($w[1], 7, $stud_sub->personcd, 'LRT', 0, 'L', $fill);
                            $pdf->Cell($w[2], 7, $stud_sub->officer_name, 'LRT', 0, 'L', $fill);
                            $pdf->Cell($w[3], 7, $stud_sub->designation, 'LRT', 0, 'L', $fill);
                            $pdf->Cell($w[4], 7, $stud_sub->poststatus, 'LRT', 0, 'L', $fill);
                            $pdf->Cell($w[5], 7, $stud_sub->mob_no, 'LRT', 0, 'L', $fill);
                            $pdf->Cell($w[6], 7, '', 'LRT', 0, 'L', $fill);
                            //count1++;

                            $pdf->Ln();
                            $pdf->Cell(array_sum($w), 0, '', 1, 0, 'L', $fill);
                            $pdf->Ln();
                        }
                        if ($count1 == $per_page1) {
                            $per_page1 = $per_page1 + 35;
                            if ($count1 != $subTe_array_count) {
                                $pdf->AddPage();
                                $pdf->Ln();
                                $pdf->SetFont('Arial', 'B', 8);
                                $pdf->Cell(195, 5, $venue, "LTR", 0, 'L');
                                $pdf->Ln(4);
                                $pdf->Cell(195, 5, $venue1, "LR", 0, 'L');
                                $pdf->Ln();

                                $pdf->SetFillColor(255, 255, 255);
                                //	$this->SetTextColor(0,0,0);
                                $pdf->SetDrawColor(0, 0, 0);
                                $pdf->SetLineWidth(.3);
                                $pdf->SetFont('Arial', 'B', 8);

                                $head = array('SL#', 'PIN', 'Name', 'Designation', 'Posting Status', 'Mobile No', 'Signature');
                                //    $header1 = array('','','','','','AC / Part /Sl No.','');
                                $w = array(8, 15, 43, 47, 22, 16, 44);
                                $pdf->SetFont('Arial', '', 7.9);
                                for ($j = 0; $j < count($head); $j++)
                                    $pdf->Cell($w[$j], 7, $head[$j], 'LTR', 0, 'L', true);
                                $pdf->Ln();
                            }
                        }
                    }
                }

                $fill = !$fill;
                $count++;
            }
            if ($count == $per_page) {
                $per_page = $per_page + 1;
                if ($count != Count($subV)) {
                    $pdf->AddPage();
                }
            }
        }
        $pdf->Output();
        exit;
    }

    public function getFirstVenuePPReportexcel(Request $request) {

        $this->validate($request, [
            'subdivision' => 'required|alpha_num|min:4|max:4',
            'venuename' => 'required|alpha_num|min:6|max:6',
            'trainingtype' => 'required|alpha_num|min:2|max:2',
            'trainingdatetime' => 'required|alpha_num|min:4|max:4'
                ], [
            'subdivision.required' => 'Subdivision is required',
            'subdivision.alpha_num' => 'Subdivision must be an alpha numeric',
            'venuename.required' => 'Venuename is required',
            'venuename.alpha_num' => 'Venuename must be an alpha numeric characters',
            'trainingtype.required' => 'Training Type is required',
            'trainingtype.alpha_num' => 'Training Type must be an alpha numeric characters',
            'trainingdatetime.required' => 'Training Date & Time is required',
            'trainingdatetime.alpha_num' => 'Training Date & Time must be an alpha numeric characters'
        ]);

        $subdivision = $request->subdivision;
        $venuename = $request->venuename;
        $trainingtype = $request->trainingtype;
        $trainingdatetime = $request->trainingdatetime;

        $tr_array = config('constants.PP_TRAINING');
        $tr_Name = $tr_array[$trainingtype];
        $tbl_first_training_schedule = new tbl_first_training_schedule;
        if ($tr_Name == "1stTrainingSch") {
            $tbl_first_training_schedule = $tbl_first_training_schedule->with('ppDetailVenueBrief');
        } else {
            $tbl_first_training_schedule = $tbl_first_training_schedule->with('ppDetailVenueBrief2');
        }
        $ApptRecord = $tbl_first_training_schedule->join('first_training_venue', 'first_training_venue.venue_cd', '=', 'first_training_schedule.tr_venue_cd')
                ->join('first_training_date_time', 'first_training_date_time.datetime_cd', '=', 'first_training_schedule.datetimecd')
                ->join('first_training_subvenue', 'first_training_subvenue.subvenue_cd', '=', 'first_training_schedule.tr_subvenue_cd')
                ->where('first_training_schedule.tr_venue_cd', '=', $venuename)
                ->where('first_training_schedule.datetimecd', '=', $trainingdatetime)
                ->where('first_training_schedule.training_type', '=', $trainingtype)
                ->where('first_training_schedule.subdivisioncd', '=', $subdivision)
                //->where(''.$session_personnela.'.officecd','=',$officeName)
                ->select('first_training_schedule.schedule_code', 'first_training_venue.venuename', 'first_training_venue.venueaddress', 'first_training_date_time.training_dt', 'first_training_date_time.training_time', 'first_training_subvenue.subvenue')
                ->get();
        $subV = json_decode($ApptRecord);

        $excelTitle = "VenueWisePPExcel";
        $data = array();
        $ppData = array();


        foreach ($subV as $subTe) {

            if ($tr_Name == "1stTrainingSch") {
                $subTe_array = $subTe->pp_detail_venue_brief;
            } else {
                $subTe_array = $subTe->pp_detail_venue_brief2;
            }

            $subTe_array_count = count($subTe_array);

            if ($subTe_array_count > 0) {

                foreach ($subTe_array as $subTeFetch) {

                    $ppData['Venue Name'] = $subTe->venuename;
                    $ppData['Venue Address'] = $subTe->venueaddress;
                    $ppData['Subvenue Name'] = $subTe->subvenue;
                    $ppData['Training Date'] = $subTe->training_dt;
                    $ppData['Training Time'] = $subTe->training_time;
                    $ppData['PIN'] = $subTeFetch->personcd;
                    $ppData['Name'] = $subTeFetch->officer_name;
                    $ppData['Designation'] = $subTeFetch->designation;
                    $ppData['Posting Status'] = $subTeFetch->poststatus;

                    $ppData['Mobile No'] = $subTeFetch->mob_no;

                    $data[] = $ppData;
                }
            }
        }


        return \Excel::create($excelTitle, function($excel) use ($data) {
                    $excel->sheet(' ', function($sheet) use ($data) {
                        $sheet->fromArray($data);
                    });
                })->download('xlsx');
    }

}
