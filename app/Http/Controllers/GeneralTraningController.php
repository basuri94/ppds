<?php

namespace App\Http\Controllers;

use App\tbl_first_rand_table;
use App\tbl_first_training_schedule;
use App\tbl_office;
use App\tbl_personnel;
use App\tbl_personnela;
use App\tbl_poststat;
use App\tbl_subdivision;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

// class GeneralTraningController extends Controller
// {
// use App\Http\Controllers\FirstTrainingAllocationController;
// use App\tbl_assembly_zone;
// use App\tbl_training_type;

class GeneralTraningController extends Controller
{
    private $firstTrainingAllocation;

    public function general_traning_checking(FirstTrainingAllocationController $firstTrainingAllocation)
    {
        $this->firstTrainingAllocation = $firstTrainingAllocation;
        $training_type = $this->firstTrainingAllocation->getTrainingGroupData();
        return view('general_traning_checking', compact('training_type'));
    }
    public function get_subdivison_wise_traning(Request $request)
    {
        // echo "hi";
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            $this->validate($request, [
                'zone' => 'required|alpha_num|min:4|max:4',
                'phase' => 'nullable|digits_between:1,1'
            ], [
                'zone.required' => 'Zone is required',
                'zone.alpha_num' => 'Zone must be an alpha numeric characters',
                'phase.digits_between' => 'Phase must be integer'
            ]);
            try {

                $phase = $request->phase;
                $zone = $request->zone;
                $subdivision = $request->subdivison;
                $training_head = $request->training_head;

                $requirement = "";
                $finaltotalfirstp1assign =0;
                $finaltotalfirstp2assign =0;
                $finaltotalfirstp3assign =0;
                $finaltotalfirstprassign =0;
              
                $finaltotalsecondp1assign =0;
                $finaltotalsecondp2assign =0;
                $finaltotalsecondp3assign =0;
                $finaltotalsecondprassign =0;
               
                $finaltotalp1 =0;
                $finaltotalp2  =0;
                $finaltotalp3 =0;
                $finaltotalpr  =0;

                $session_personnela = session()->get('personnela_ppds');
                $tbl_personnela = new tbl_personnela();
                $records = $tbl_personnela->join('office', '' . $session_personnela . '.officecd', '=', 'office.officecd')
                    ->join('subdivision', 'office.subdivisioncd', '=', 'subdivision.subdivisioncd')

                    ->join('zone', '' . $session_personnela . '.forzone', '=', 'zone.zone')
                   
                    ->groupBy('subdivision.subdivisioncd')
                    ->select('zone.zonename', 'subdivision.subdivision', 'subdivision.subdivisioncd')->get();
                // dd($records);
                //  $records = tbl_office::leftjoin('subdivision','office.subdivisioncd', '=', 'subdivision.subdivisioncd')
                //                         ->select('subdivisioncd',DB::raw('Count(*)'))->groupBy('subdivisioncd')->get();
               
                foreach ($records as $recordspp) {
                    $totalp1 = 0;
                    $totalp2 = 0;
                    $totalp3 = 0;
                    $totalpr = 0;
    
                    $totalp1firstassign = 0;
                    $totalp2firstassign = 0;
                    $totalp3firstassign = 0;
                    $totalprfirstassign = 0;
    
                    $totalp1secondassign = 0;
                    $totalp2secondassign = 0;
                    $totalp3secondassign = 0;
                    $totalprsecondassign = 0;
    
                    $nestedData['Zone'] = $recordspp->zonename;
                    $nestedData['Subdivision'] = $recordspp->subdivision;


                    $rsPostStat = tbl_poststat::orderBy('post_stat')
                        ->select('post_stat')->get();
                    foreach ($rsPostStat as $rowPstat) {
                        $tbl_personnela1 = new tbl_personnela();

                        $tbl_personnela1 = $tbl_personnela1->where('selected', '=', 1);


                        $recordsPost = $tbl_personnela1->join('office', '' . $session_personnela . '.officecd', '=', 'office.officecd')
                            ->where('office.subdivisioncd', '=', $recordspp->subdivisioncd)

                            ->where('forzone', '=', $zone)
                            ->where('phase', '=', $phase)
                            ->where('poststat', '=', $rowPstat->post_stat)
                            ->value(DB::raw('Count(personcd)'));
                     //   dd($recordsPost);
                        // $office = tbl_office::select('subdivisioncd',DB::raw('Count(*)'))->groupBy('subdivisioncd')->get();
                        // dd($office);


                        // $recordsPost = $tbl_personnela1->where('selected','=',1)
                        //                                 ->where('forzone',$zone)
                        //                                 ->where('phase',$phase)
                        //                                 ->where('poststat',$rowPstat->post_stat)
                        //                                 ->where((DB::raw(substr('personcd',1,4))),$recordspp->subdivisioncd)
                        //                                 ->value(DB::raw('Count(personcd)'));

                        // $recordsPost = DB::raw(DB::select("select Count(personcd) as cnt from personnela06 where selected = 1 and forzone = '".$zone."' and phase='.$phase.' and poststat = '".$rowPstat->post_stat."' AND substr(personcd,1,4)='".$recordspp->subdivisioncd."'"));
                       
                        //   dd($recordsPost['cnt']);
                        // dd($recordsPost);
                        $totalPostAssignPP = tbl_first_training_schedule::where('forzone', '=', $zone)
                            ->where('phase_id', '=', $phase)
                            ->where('training_type', '=', '01')
                            ->where('subdivisioncd', '=', $recordspp->subdivisioncd);
                            if($rowPstat->post_stat=='P1'){
                                $totalPostAssignPP = $totalPostAssignPP->sum('no_of_P1');
                            }
                            if($rowPstat->post_stat=='P2'){
                                $totalPostAssignPP = $totalPostAssignPP->sum('no_of_P2');
                            }
                            if($rowPstat->post_stat=='P3'){
                                $totalPostAssignPP = $totalPostAssignPP->sum('no_of_P3');
                            }
                            if($rowPstat->post_stat=='PR'){
                                $totalPostAssignPP = $totalPostAssignPP->sum('no_of_PR');
                            }
                            
                           
                            $totalPostAssignPP_2 = tbl_first_training_schedule::
                            where('forzone', '=', $zone)
                            ->where('phase_id', '=', $phase)
                            ->where('training_type', '=', '02')
                            ->where('subdivisioncd', '=', $recordspp->subdivisioncd);
                            if($rowPstat->post_stat=='P1'){
                                $totalPostAssignPP_2 = $totalPostAssignPP_2->sum('no_of_P1');
                            }
                            if($rowPstat->post_stat=='P2'){
                                $totalPostAssignPP_2 = $totalPostAssignPP_2->sum('no_of_P2');
                            }
                            if($rowPstat->post_stat=='P3'){
                                $totalPostAssignPP_2 = $totalPostAssignPP_2->sum('no_of_P3');
                            }
                            if($rowPstat->post_stat=='PR'){
                                $totalPostAssignPP_2 = $totalPostAssignPP_2->sum('no_of_PR');
                            }
                            
                     
                           



                        if ($rowPstat->post_stat == 'P1') {
                            $totalp1 = $totalp1 + $recordsPost;
                            $totalp1firstassign = $totalp1firstassign + $totalPostAssignPP;
                            
                            $totalp1secondassign = $totalp1secondassign + $totalPostAssignPP_2;

                        } else if ($rowPstat->post_stat == 'P2') {
                            $totalp2 = $totalp2 + $recordsPost;
                            $totalp2firstassign = $totalp2firstassign + $totalPostAssignPP;
                            $totalp2secondassign = $totalp2secondassign + $totalPostAssignPP_2;
                        } else if ($rowPstat->post_stat == 'P3') {
                            $totalp3 = $totalp3 + $recordsPost;
                          //  dd($totalp3);
                            $totalp3firstassign = $totalp3firstassign + $totalPostAssignPP;
                            $totalp3secondassign = $totalp3secondassign + $totalPostAssignPP_2;
                        } else if ($rowPstat->post_stat == 'PR') {
                            $totalpr = $totalpr + $recordsPost;
                            $totalprfirstassign = $totalprfirstassign + $totalPostAssignPP;
                            $totalprsecondassign = $totalprsecondassign + $totalPostAssignPP_2;
                        }
                    }


                    $requirement .= '<tr>';
                    $requirement .= '<td align="left" style="width: 15%;">&nbsp;' . $recordspp->subdivision . '</td>';

                    $requirement .= "<td>" . $totalp1 . '</td>';
                    $requirement .= "<td>" . $totalp2 . '</td>';
                    $requirement .= "<td>" . $totalp3 . '</td>';
                    $requirement .= "<td>" . $totalpr . '</td>';



                    $requirement .= "<td>" . $totalp1firstassign . '</td>';
                    $requirement .= "<td>" . $totalp2firstassign . '</td>';
                    $requirement .= "<td>" . $totalp3firstassign . '</td>';
                    $requirement .= "<td>" . $totalprfirstassign . '</td>';

                    $requirement .= "<td>" . $totalp1secondassign . '</td>';
                    $requirement .= "<td>" . $totalp2secondassign . '</td>';
                    $requirement .= "<td>" . $totalp3secondassign . '</td>';
                    $requirement .= "<td>" . $totalprsecondassign . '</td>';

                    $finaltotalfirstp1assign +=$totalp1firstassign;
                    $finaltotalfirstp2assign +=$totalp2firstassign;
                    $finaltotalfirstp3assign +=$totalp3firstassign;
                    $finaltotalfirstprassign +=$totalprfirstassign;

                    $finaltotalsecondp1assign +=$totalp1secondassign;
                    $finaltotalsecondp2assign +=$totalp2secondassign;
                    $finaltotalsecondp3assign +=$totalp3secondassign;
                    $finaltotalsecondprassign +=$totalprsecondassign;
                    
                    $finaltotalp1 +=$totalp1;
                    $finaltotalp2 +=$totalp2;
                    $finaltotalp3 +=$totalp3;
                    $finaltotalpr +=$totalpr;
                   
                    $requirement .= '</tr>';
                }
                $requirement .= '<td colspan="1">Total</td><td>'.$finaltotalp1.'</td><td>'.$finaltotalp2.'</td><td>'.$finaltotalp3.'</td><td>'.$finaltotalpr.'</td>';
                $requirement .='<td>'. $finaltotalfirstp1assign.'</td><td>'. $finaltotalfirstp2assign.'</td><td>'. $finaltotalfirstp3assign.'</td><td>'. $finaltotalfirstprassign.'</td>';
                $requirement .= '<td>'.$finaltotalsecondp1assign.'</td><td>'.$finaltotalsecondp2assign.'</td><td>'.$finaltotalsecondp3assign.'</td><td>'.$finaltotalsecondprassign.'</td>';


                $response = array(
                    'requirement' => $requirement,
                    'status' => 1
                );
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }


    // public function get_subdivison_wise_traning(Request $request)
    // {
    //     $statusCode = 200;
    //     if (!$request->ajax()) {
    //         $statusCode = 400;
    //         $response = array('error' => 'Error occured in Ajax Call.');
    //         return response()->json($response, $statusCode);
    //     } else {
              
    //         try {
    //             $zone = $request->zone;
    //             $phase = $request->phase;
    //             $subdivison = $request->subdivison;
    //             // dd($subdivison);
    //             $tbl_personnela = new tbl_personnel();
    //             $session_personnela = session()->get('personnela_ppds');
    //             // dd($session_personnela);
    //          $recordsPost = DB::raw(DB::select("select poststat,Count(personcd) as cnt from '". $session_personnela ."' where selected = 1 AND forzone = '".$zone."' AND phase='.$phase.' AND substr(personcd,1,4)='".$subdivison."' GROUP BY poststat"));
    //         //  dd($recordsPost);
            
            
    //         // $recordsPost = $tbl_personnela->where('selected1','=',1)
    //         //                             ->where('forzone','=',$zone)
    //         //                             ->where('phase','=',$phase)
    //         //                             // ->where('SUBSTRING'('personcd', 1, 4),'=',$subdivison)
    //         //                             ->select('poststat',DB::raw('Count(personcd)'))
    //         //                             ->groupBy('poststat')->get();
    //             dd($recordsPost);
    //         } catch (\Exception $e) {
    //             $response = array(
    //                 'exception' => true,
    //                 'exception_message' => $e->getMessage(),
    //             );
    //             $statusCode = 400;
    //         } finally {
    //             return response()->json($response, $statusCode);
    //         }
    //     }
    // }

    public function checking_venue_wise_populate(Request $request){
        return view('checking_venue_wise_populate');
    }

    public function getVenueSubvenueWiseChecking(Request $request){
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
           
            try {
               
                $fordist = $request->fordist;
                $first_training_data = tbl_first_rand_table::leftjoin('phase', 'phase.code', '=', 'first_rand_table.phase')
               ->where('first_rand_table.fordistrict','=',$fordist)
               ->whereNotNull('schedule_code')
               
                ->select('first_rand_table.*',
                'phase.name',
                DB::raw('CASE WHEN first_rand_table.poststat = "P1" then count(1) ELSE 0 END as "P1",
                CASE WHEN first_rand_table.poststat = "P2" then count(1) ELSE 0 END as "P2",  	
                CASE WHEN first_rand_table.poststat = "P3" then count(1) ELSE 0 END as "P3",
                 CASE WHEN first_rand_table.poststat = "PR" then count(1) ELSE 0 END as "PR"'),
                
                
                
                'first_rand_table.poststat')
                ->groupBy('first_rand_table.venuename','first_rand_table.sub_venuename','first_rand_table.training_dt',
                'first_rand_table.training_time','training_desc','schedule_code','poststat')->get();
                // dd($first_training_data);
                $data = [];
                $requirement = '';
                $totalp1assign = 0;
                $totalp2assign = 0;
                $totalp3assign = 0;
                $totalprassign = 0;
                foreach ($first_training_data as $value) {
               

                   $requirement .= '<tr>';
                    $requirement .= '<td align="left" style="width: 15%;">&nbsp;' . $value->venuename . '</td>';

                    $requirement .= "<td>" . $value->sub_venuename . '</td>';
                    $requirement .= "<td>" . $value->name . '</td>';
                    $requirement .= "<td>" . $value->training_dt . ' - ' . $value->training_time . '</td>';
                    
                    $requirement .= "<td>" . $value->P1 . '</td>';
                    $requirement .= "<td>" . $value->P2 . '</td>';
                    $requirement .= "<td>" . $value->P3. '</td>';
                    $requirement .= "<td>" . $value->PR. '</td>';

                    $totalp1assign +=$value->P1;
                    $totalp2assign +=$value->P2;
                    $totalp3assign +=$value->P3;
                    $totalprassign +=$value->PR;


                    $requirement .= '</tr>';
                   
                }
$requirement .='<td colspan="4">Total:-</td><td>'. $totalp1assign.'</td><td>'. $totalp2assign.'</td><td>'. $totalp3assign.'</td><td>'. $totalprassign.'</td>';
                $second_training_data = tbl_first_rand_table::leftjoin('phase', 'phase.code', '=', 'first_rand_table.phase')
                ->where('first_rand_table.fordistrict','=',$fordist)
                ->whereNotNull('schedule_code_2')
                
                 ->select('first_rand_table.*',
                 'phase.name',
                 DB::raw('CASE WHEN first_rand_table.poststat = "P1" then count(1) ELSE 0 END as "P1",
                CASE WHEN first_rand_table.poststat = "P2" then count(1) ELSE 0 END as "P2",  	
                CASE WHEN first_rand_table.poststat = "P3" then count(1) ELSE 0 END as "P3",
                 CASE WHEN first_rand_table.poststat = "PR" then count(1) ELSE 0 END as "PR"'),
                 
                 
                 
                 'first_rand_table.poststat')
                 ->groupBy('first_rand_table.venuename_2','first_rand_table.sub_venuename_2','first_rand_table.training_dt_2',
                 'first_rand_table.training_time_2','training_desc_2','schedule_code_2','poststat')->get();
                // dd($second_training_data);
                $requirement1 = '';
                $totalp1assign1 = 0;
                $totalp2assign1 = 0;
                $totalp3assign1 = 0;
                $totalprassign1 = 0;
                foreach ($second_training_data as $value1) {
                   
    
                       $requirement1 .= '<tr>';
                        $requirement1 .= '<td align="left" style="width: 15%;">&nbsp;' . $value1->venuename_2 . '</td>';
    
                        $requirement1 .= "<td>" . $value1->sub_venuename_2 . '</td>';
                        $requirement1 .= "<td>" . $value1->name . '</td>';
                        $requirement1 .= "<td>" . $value1->training_dt_2 . ' - ' . $value1->training_time_2 . '</td>';
                       
                        $requirement1 .= "<td>" . $value1->P1 . '</td>';
                        $requirement1 .= "<td>" . $value1->P2 . '</td>';
                        $requirement1 .= "<td>" . $value1->P3. '</td>';
                        $requirement1 .= "<td>" . $value1->PR. '</td>';
    
                        $totalp1assign1 +=$value1->P1;
                    $totalp2assign1 +=$value1->P2;
                    $totalp3assign1 +=$value1->P3;
                    $totalprassign1 +=$value1->PR;
                        $requirement1 .= '</tr>';
                       
                    }
                    $requirement1 .='<td colspan="4">Total:-</td><td>'. $totalp1assign1.'</td><td>'. $totalp2assign1.'</td><td>'. $totalp3assign1.'</td><td>'. $totalprassign1.'</td>';
                $response = array(
                    'requirement' => $requirement,
                    'requirement1' => $requirement1,
                    'status' => 1
                );
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        
    }
    }

    public function generaltraningallocationPPExcel(Request $request)
    {
        $this->validate($request, [
            'zone' => 'required|alpha_num|min:4|max:4',
            'phase' => 'nullable|digits_between:1,1'
        ], [
            'zone.required' => 'Zone is required',
            'zone.alpha_num' => 'Zone must be an alpha numeric characters',
            'phase.digits_between' => 'Phase must be integer'
        ]);
        try {

            $phase = $request->phase;
            $zone = $request->zone;
            $subdivision = $request->subdivison;
            $training_head = $request->training_head;

            $requirement = "";


          

            // $totalp1 = 0;
            // $totalp2 = 0;
            // $totalp3 = 0;
            // $totalpr = 0;

            // $totalp1firstassign = 0;
            // $totalp2firstassign = 0;
            // $totalp3firstassign = 0;
            // $totalprfirstassign = 0;

            // $totalp1secondassign = 0;
            // $totalp2secondassign = 0;
            // $totalp3secondassign = 0;
            // $totalprsecondassign = 0;

            // $data = [];
            $session_personnela = session()->get('personnela_ppds');
            $tbl_personnela = new tbl_personnela();
            $records = $tbl_personnela->join('office', '' . $session_personnela . '.officecd', '=', 'office.officecd')
                ->join('subdivision', 'office.subdivisioncd', '=', 'subdivision.subdivisioncd')
                ->join('zone', '' . $session_personnela . '.forzone', '=', 'zone.zone')
                ->groupBy('subdivision.subdivisioncd')
                ->select('zone.zonename', 'subdivision.subdivision', 'subdivision.subdivisioncd')->get();

            foreach ($records as $recordspp) {
               $nestedData['Zone'] = $recordspp->zonename;
                $nestedData['Subdivision'] = $recordspp->subdivision;
                // $nestedData[''] = $recordspp->subdivision;
                $rsPostStat = tbl_poststat::orderBy('post_stat')
                    ->select('post_stat')->get();
                foreach ($rsPostStat as $rowPstat) {
                    $tbl_personnela1 = new tbl_personnela();

                    $tbl_personnela1 = $tbl_personnela1->where('selected', '=', 1);


                    $recordsPost = $tbl_personnela1->join('office', '' . $session_personnela . '.officecd', '=', 'office.officecd')
                        ->where('office.subdivisioncd', '=', $recordspp->subdivisioncd)
                        ->where('forzone', '=', $zone)
                        ->where('phase', '=', $phase)
                        ->where('poststat', '=', $rowPstat->post_stat)
                        ->value(DB::raw('Count(personcd)'));

                    



                        $nestedData[$rowPstat->post_stat]=$recordsPost; 





                        // $totalPostAssignPP = tbl_first_training_schedule::where('forzone', '=', $zone)
                        //     ->where('phase_id', '=', $phase)
                        //     ->where('training_type', '=', '01')
                        //     ->where('subdivisioncd', '=', $recordspp->subdivisioncd);
                            
                        //     if($rowPstat->post_stat=='P1'){
                        //         $totalPostAssignPP = $totalPostAssignPP->sum('no_of_P1');
                        //         $nestedData['No. Of P1']= $totalPostAssignPP; 
                        //     }
                        //     if($rowPstat->post_stat=='P2'){
                        //         $totalPostAssignPP = $totalPostAssignPP->sum('no_of_P2');
                        //         $nestedData['No. Of P2']= $totalPostAssignPP; 
                        //     }
                        //     if($rowPstat->post_stat=='P3'){
                        //         $totalPostAssignPP = $totalPostAssignPP->sum('no_of_P3');
                        //         $nestedData['No. Of P3']= $totalPostAssignPP; 
                        //     }
                        //     if($rowPstat->post_stat=='PR'){
                        //         $totalPostAssignPP = $totalPostAssignPP->sum('no_of_PR');
                        //         $nestedData['No. Of PR']= $totalPostAssignPP; 
                        //     }
                            
                           

                       
                }

                $data[] = $nestedData;
            }
            // dd($data);
            return \Excel::create('afterAllocationReport', function($excel) use ($data) {
                $excel->sheet(' ', function($sheet) use ($data) {
                    $sheet->fromArray($data);
                });
            })->download('xlsx');


        } catch (\Exception $e) {
            $response = array(
                'exception' => true,
                'exception_message' => $e->getMessage(),
            );
            $statusCode = 400;
        } finally {
            return response()->json($response, $statusCode);
        }
       
    }

    public function getFirstTraningPopulateCheckingEXcel(Request $request)
    {
        
        try {

            $fordist = $request->fordist;
               
            $first_training_data = tbl_first_rand_table::leftjoin('phase','phase.code','=','first_rand_table.phase')
                                                        ->select('first_rand_table.schedule_code','first_rand_table.venuename','first_rand_table.sub_venuename','first_rand_table.venueaddress','first_rand_table.training_dt','first_rand_table.training_time','first_rand_table.poststat','phase.name',DB::raw('Count(personcd) as total'))
                                                        ->where('fordistrict','=',$fordist)
                                                        ->groupBy('schedule_code','venuename','sub_venuename','venueaddress','training_dt','training_time','poststat','phase')->get();
            $data = [];
            foreach ($first_training_data as $value) {
                $nestedData['Schedule code'] = $value->schedule_code;
                $nestedData['Venue Name'] = $value->venuename;
                $nestedData['Sub Venue Name'] = $value->sub_venuename;
                $nestedData['Venue Address'] = $value->schedule_code;
                $nestedData['Training Date'] = $value->schedule_code;
                $nestedData['Venue Address'] = $value->schedule_code;
            }
            // return \Excel::create('afterAllocationReport', function($excel) use ($data) {
            //     $excel->sheet(' ', function($sheet) use ($data) {
            //         $sheet->fromArray($data);
            //     });
            // })->download('xlsx');


        } catch (\Exception $e) {
            $response = array(
                'exception' => true,
                'exception_message' => $e->getMessage(),
            );
            $statusCode = 400;
        } finally {
            return response()->json($response, $statusCode);
        }
    }
}
