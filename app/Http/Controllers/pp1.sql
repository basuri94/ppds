DELIMITER $$
CREATE DEFINER=`ppds`@`%` PROCEDURE `rand2`(IN `zone` CHAR(4), IN `dist` CHAR(2), IN `ph` CHAR(1))
    NO SQL
BEGIN

DECLARE flag_ass1 INT DEFAULT 0;
DECLARE flag_ass2 INT DEFAULT 0;
DECLARE flag_ass1p INT DEFAULT 0;
    DECLARE var_no_party INT DEFAULT 0 ;
    DECLARE var_prior int;
    DECLARE var_start_sl INT DEFAULT 0 ;
    DECLARE var_no_of_member INT DEFAULT 0 ;
	DECLARE single_assembly CHAR(3) DEFAULT "" ;
    DECLARE var_gender char(1) ;
    
    
    
 -- set @phv=ph;  
--  DECLARE assembly_cursor_Priority CURSOR FOR      	SELECT priority_asm.assemblycd ,priority_asm.subdivcd,priority_asm.priority  FROM priority_asm where assemblycd=vasm ;
--   DECLARE CONTINUE HANDLER FOR NOT FOUND SET flag_ass1p = 1;
 
 
 
 
    
    DECLARE assembly_cursor CURSOR FOR 
    	SELECT assembly_party.assemblycd ,assembly_party.no_of_member,assembly_party.gender,assembly_party.start_sl,assembly_party.no_party,assembly_party.priority
        FROM assembly_party 
        INNER JOIN assembly_zone  ON assembly_party.assemblycd = assembly_zone.assemblycd 
        WHERE assembly_zone.zone = zone and assembly_zone.districtcd = dist and assembly_zone.rand_status2='A' and  assembly_zone.phase_id=ph
    ORDER BY assembly_party.priority DESC;
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET flag_ass1 = 1;
 
  set @d=dist;
  set @phv=ph;

  
set @inl=1;
set @p2=0;
 call decl_statement_pr_2(@d,@phv);    
  call decl_statement_p2_2(@d,@phv);
 call decl_statement_p3_2(@d,@phv);
 call decl_statement_pa_2(@d,@phv);
 call decl_statement_p1_2(@d,@phv);
 call decl_statement_priority(@d,@phv);
 

 execute stmDefpriorityN;


     OPEN assembly_cursor;
    
   
    loop_assembly: LOOP
    	FETCH assembly_cursor INTO single_assembly,var_no_of_member,var_gender,var_start_sl,var_no_party,var_prior;
        
		IF flag_ass1 = 1 THEN 
    		LEAVE loop_assembly;
    	END IF;
        	      
                SET @p0=var_no_of_member ; 
                SET @p1=single_assembly; 
                SET @p2=var_start_sl; 
                SET @p3=zone; 
                SET @p4=var_no_party; 
                set @p5=dist;
                set @p6=var_gender;
       --         select @p1;
                
              set   @table=concat('personnela',dist);
                
               
-- Subdivision Priority





-- 
 
   
    
   EXECUTE stmtpriority using @p1;
   
    
--  call setprioritysubdiv(@p3,@p5,@phv,@p1);
   
--  select 'LKOL';
  CALL `rand2_main_all`(@p0,@p1,@p2,@p3,@p4,@p5,@p6);
        
    
 
 
    
 
    
 
    
 
   
 
 
   
 
   
                     
    END LOOP loop_assembly;
    select ' xyz';
   commit;
    call stmt_deallocate_p2_2(@p5);
    call stmt_deallocate_p3_2(@p5);
   call stmt_deallocate_p1_2(@p5);
   call stmt_deallocate_pr_2(@p5);
   call stmt_deallocate_pa_2(@p5);
   call stmt_deallocate_priority(@p5);
    CLOSE assembly_cursor;
    
   
   


END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`ppds`@`%` PROCEDURE `stmt_deallocate_priority`(IN `dist` CHAR(2))
    NO SQL
    DETERMINISTIC
BEGIN

deallocate prepare  stmtpriority;
deallocate prepare stmDefpriorityN;



  
END$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`ppds`@`%` PROCEDURE `init_2`(IN `dist` CHAR(2), IN `zone` CHAR(4), IN `pc` CHAR(2))
    NO SQL
BEGIN

set @table=concat('personnela',dist);
set @d=dist;
set @z=zone;
set @pcsel=pc;

set AUTOCOMMIT  = 0;
set @stmDefpriority0=concat("update ", @table , " as pp set pp.priority=99 where fordistrict='",@d,"'");
prepare stmDefpriority0 from @stmDefpriority0;
execute stmDefpriority0;

-- set @stmDefpriority=concat("update ", @table , " as pp set pp.priority=8 where fordistrict='",@d,"'  and home_pc='", @pcsel,"'");
-- prepare stmDefpriority from @stmDefpriority;
-- execute stmDefpriority;






 




 
-- set @stmpriorityp=concat("update ", @table , " as pp inner join  (select districtcd,pc_priority,priority_no  from district_priority_pc where districtcd='",@d,"'  and pc='", @pcsel,"' ) as pp1 on pp.home_pc=pp1.pc_priority set pp.priority=pp1.priority_no+20 ");
-- prepare stmpriorityp from @stmpriorityp;
-- execute stmpriorityp;
-- deallocate prepare stmpriorityp;

-- set @stmpriority=concat("update ", @table , " as pp inner join  (select districtcd,ac_priority,priority_no  from district_priority_ac where districtcd='",@d,"'  and pc='", @pcsel,"' ) as pp1 on pp.assembly_off=pp1.ac_priority set pp.priority=pp1.priority_no+10 ");
-- prepare stmpriority from @stmpriority;
-- execute stmpriority;
-- deallocate prepare stmpriority;







set @stmti=concat("update ",@table , " set booked=' ' , groupid=0 ,forassembly='  ',dcrccd='  ' ,no_of_member= 0  where selected=1 and forzone= '",@z, "' and forassembly not in ( select assemblycd from assembly_zone where rand_status2='Y' and districtcd= '",@d,"' and zone= '",@z, "'  )  " );
prepare stmi from @stmti;

execute stmi;
deallocate prepare stmi;

COMMIT;
end$$
DELIMITER ;

DELIMITER $$
CREATE DEFINER=`ppds`@`%` PROCEDURE `decl_statement_priority`(IN `dist` CHAR(2), IN `ph` CHAR(1))
    NO SQL
    DETERMINISTIC
BEGIN
 set @var_dist=dist;
 set @phv=ph;

 
 set @table=concat('personnela',@var_dist); 
 
--  set @stmt_priority=concat("UPDATE ",@table," set priority = ? where substr(officecd,1,4)= ? and phase='",@phv,"' and fordistrict='",@var_dist,"'" );                  
--  prepare stmtpriority from @stmt_priority;

set @stmt_priority=concat("UPDATE ",@table," as p1 inner join  (SELECT subdivcd,priority   FROM  priority_asm   WHERE assemblycd= ? ) as p2 on substr(p1.officecd,1,4)= p2.subdivcd  set p1.priority=p2.priority" ); 

prepare stmtpriority from @stmt_priority;

 
 
  set @stmDefpriority0=concat("update ", @table , " as pp set pp.priority=99 where fordistrict='",@var_dist,"'");
prepare stmDefpriorityN from @stmDefpriority0;
  



END$$
DELIMITER ;
