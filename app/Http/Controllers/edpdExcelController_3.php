<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\tbl_personnela;
use App\tbl_assembly_district;
use DB;

class edpdExcelController extends Controller {

    public function getExcelOfPDED(Request $request) {
        $this->validate($request, [
            'typeOfEdPd' => 'required|alpha|min:1|max:1'
                ], [
            'typeOfEdPd.required' => 'Type is required',
            'typeOfEdPd.alpha' => 'Type must be an alpha characters'
        ]);

        $typeOfEdPd = $request->typeOfEdPd;
        if ($typeOfEdPd == "E") {
            $reportName = "ED";
        } else {
            $reportName = "PB";
        }

        $data = array();
        $session_personnela = session()->get('personnela_ppds');
        $districtcd_ppds = session()->get('districtcd_ppds');
        $tbl_personnela = new tbl_personnela();

      

        $recordsTotal = tbl_personnela::join('office',''.$session_personnela.'.officecd','=','office.officecd')
                           ->join('personnel',''.$session_personnela.'.personcd','=','personnel.personcd')
                           ->join('poststat',''.$session_personnela.'.poststat','=','poststat.post_stat')
                ->select(''.$session_personnela.'.personcd','personnel.officer_name','personnel.off_desg','office.office','poststat.poststatus','personnel.gender','personnel.acno','personnel.slno','personnel.partno');
  if ($typeOfEdPd != "") {
            $recordsTotal = $recordsTotal->where('ed_pb', '=', $typeOfEdPd);
        }
        $recordsTotal=$recordsTotal->get();
        foreach ($recordsTotal as $recordsTo) {
            $fst = '1stTrainingSch';
            $fst_1 = '1stTrainingSch_2';
            $sst = '2ndTrainingSch';
            $nestedData1['Personcd'] = $recordsTo->personcd;
            $nestedData1['Officer Name'] = $recordsTo->officer_name;
            $nestedData1['Designation'] = $recordsTo->off_desg;
            $nestedData1['Office Name'] = $recordsTo->office;
            $nestedData1['Poststat'] = $recordsTo->poststatus;
            $nestedData1['Gender'] = $recordsTo->gender;
            $nestedData1['Ac No'] = $recordsTo->acno;
            $nestedData1['Sl No'] = $recordsTo->slno;
            $nestedData1['part No'] = $recordsTo->partno;
            
            $data[] = $nestedData1;
        }

        //$data1=$nestedData1;       
        // array_push($data,$data1);
        date_default_timezone_set('Asia/Calcutta');
        $dt = date_create('now')->format('YmdHi');
        $reportNameF = $reportName . $dt;
        return \Excel::create($reportNameF, function($excel) use ($data) {
                    $excel->sheet(' ', function($sheet) use ($data) {
                        $sheet->fromArray($data);
                    });
                })->download('xlsx');
    }

    public function updateHomePC(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
              DB::beginTransaction();
            try {

                $data = array();
                $session_personnela = session()->get('personnela_ppds');

                $tbl_personnela = new tbl_personnela();
                $checkExistED = $tbl_personnela->select('personcd')->where('' . $session_personnela . '.ed_pb', '=', "E")
                        ->get();
                $checkExistPB = $tbl_personnela->select('personcd')->where('' . $session_personnela . '.ed_pb', '=', "P")
                        ->get();
                if (count($checkExistED) > 0 || count($checkExistPB) > 0) {
                    $filtered = 0;
                    $filteredLa = 0;
                } else {
                    $filtered = $tbl_personnela->join('personnel', 'personnel.personcd', '=', '' . $session_personnela . '.personcd')
                            ->join('assembly_district', 'assembly_district.assemblycd', '=', 'personnel.acno')
                            ->update(['' . $session_personnela . '.home_pc' => DB::raw('assembly_district.pccd')]);
                    if ($filtered > 0) {

                        $filteredLa = $tbl_personnela->whereNotIn('' . $session_personnela . '.home_pc', function($query) {
                                    $districtcd = session()->get('districtcd_ppds');
                                    $query->select('assembly_district.pccd')->from('assembly_district')
                                    ->where('assembly_district.districtcd', '=', $districtcd)
                                    ->groupBy('assembly_district.pccd');
                                })
                                ->update(['' . $session_personnela . '.home_pc' => 99]);
                    }
                }
               
                
                $response = array(
                    'filtered' => $filtered,
                    'filteredLa' => $filteredLa,
                    //'ed_count' => $countOfED,
                   // 'pb_count' => $countOfPB,
                    'status' => 1);
                  DB::rollback();
                DB::commit();
            } catch (\Exception $e) {
               
                 
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {

                return response()->json($response, $statusCode);
            }
        }
    }

}
