<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\tblsms3;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class SendSmsController extends Controller
{
    public function sendsms(){
        $dist = Session::get('districtcd_ppds');
        if($dist==18){
            return view('smssend');
        }
        else{
            abort(404);
        }
       

    }

    public function smssent(Request $request){
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            $this->validate($request, [
                'from' => 'required',
                'to' => 'required|integer',
               // 'training_type' => 'required',
              //  'phase' => 'required',
            ], [
                'from.required' => 'From PP is required',
                'to.required' => 'Count PP is required',
                'to.integer' => 'Count PP  must be an integer',
                //'training_type.required' => 'Training type is required',
               // 'phase.required' => 'Phase is required',
            ]);

            try {
                $dist = Session::get('districtcd_ppds');
                if($dist==18){
                    $from = $request->from;
                    $to = $request->to;
                  
                    $fetchTblSms = tblsms3::offset($from - 1)->limit($to-$from+1)
                        ->select('phone_no','message')->groupBy('phone_no')->get();
                   
    
    
                    $count = 0;
                    $mobileUser=0;
                    $message_arr = array();
                    $databasemobie_arr = array();
                    foreach ($fetchTblSms as $fetchTblSms1) {
                     
                        $count++;
                        $mobileUser++;
                        $mobileno = $fetchTblSms1->phone_no;
                        $message = $fetchTblSms1->message;
                       
    
                        $this->singlesms($mobileno, $message);
                      
                        $updateTblSMS = tblsms3::where('phone_no', $mobileno)
                           ->where('smscount',0)
                            ->update(['smscount' => DB::raw('smscount+1')]);
    
                            
                        
                    }
                }
                else{
                    abort(404);
                }
               


                $response = array(
                    'sendSms' => $count,
                    'mobileUser'=>$mobileUser,
                    'status' => 1
                );
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }

    public function getSmsRecordAvailable(Request $request){
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {



            try {
              
                

                $phonedata = tblsms3::where('smscount', '=', '0')->select('phone_no')->groupBy('phone_no')->get();
               

              
               
                $response = array(
                    'phonedata' => count($phonedata),
                   
                    
                    'status' => 1
                );
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }

    public function SMSRecordssendSpecial(Request $request){
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            


            try {
              

                $SmsSendAlready = tblsms3::where('smscount', '=', 1)->get()->count();



                $response = array(
                    'SmsSendAlready' => $SmsSendAlready + 1,
                    'nextSmsLot' => $SmsSendAlready + 250,
                    'status' => 1
                );
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }
    public function singlesms($mobileno, $message)
    {
        $username = config('constants.USERNAME'); //username of the departmen

        $password = config('constants.PASSWORD'); //password of the department
        $senderid =  config('constants.SENDERID'); //senderid of the deparment
        // $message = "Your Normal message here "; //message content
        $messageUnicode = "मोबाइलसेवामेंआपकास्वागतहै "; //message content in unicode
        //   $mobileno=""; //if single sms need to be send use mobileno keyword
        //  $mobileNos = implode(',', $mobileno);
        // print_r($mobileNos);die;
        //$mobileNos= "86XXXXXX72,79XXXXXX00"; //if bulk sms need to send use mobileNos as keyword and mobile number seperated by commas as value
        $deptSecureKey =  config('constants.SECUREKEY'); //departsecure key for encryption of message...
        //   $templateid= config('constants.TEMPLATEID');

        $encryp_password = sha1(trim($password));
        $key = hash('sha512', trim($username) . trim($senderid) . trim($message) . trim($deptSecureKey));

        $data = array(
            "username" => trim($username),
            "password" => trim($encryp_password),
            "senderid" => trim($senderid),
            "content" => trim($message),
            "smsservicetype" => "singlemsg",
            "bulkmobno" => trim($mobileno),
            // "templateid"=>$templateid,
            "key" => trim($key)
        );
        $url = "https://msdgweb.mgov.gov.in/esms/sendsmsrequest";
       

        $fields = '';
        foreach ($data as $key => $value) {
            $fields .= $key . '=' . urlencode($value) . '&';
        }
        rtrim($fields, '&');
        $post = curl_init();
        //curl_setopt($post, CURLOPT_SSLVERSION, 5); // uncomment for systems supporting TLSv1.1 only
        curl_setopt($post, CURLOPT_SSLVERSION, 6); // use for systems supporting TLSv1.2 or comment the line
        curl_setopt($post, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($post, CURLOPT_URL, $url);
        curl_setopt($post, CURLOPT_POST, count($data));
        curl_setopt($post, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($post, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($post); //result from mobile seva server
        // echo $result;
        // die;  //output from server displayed 
        curl_close($post);
        return 1;
    }
}
