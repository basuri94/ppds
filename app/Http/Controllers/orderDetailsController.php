<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\tbl_order_no_date;

class orderDetailsController extends Controller {

    public function order_entry_details(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            
            $this->validate($request, [
                'districtcd' => 'required|alpha_num|min:2|max:2',
                'order_date' => 'required|date_format:d/m/Y|min:10|max:10',
                'order_no' => 'required|regex:/^[A-Za-z0-9\s()\/ ]+$/|max:15|unique:order_no_date,order_no'
                    ], [
                'districtcd.required' => 'District is required',
                'districtcd.alpha_num' => 'District must be an alpha numeric',
                'order_date.required' => 'Order date is required',
                'order_date.date_format' => 'Order date format should be d/m/y',
                'order_no.required' => 'Order no is required',
                'order_no.regex' => 'Order no can consist of alphanumerical characters',
                'order_no.max' => 'Order no must not be greater than 15 characters',
                'order_no.unique'=>'Order no has already been taken'
            ]);
            try {
                $forDist = $request->districtcd;
                $order_date = $request->order_date;
                $order_no = $request->order_no;
                $user_code = session()->get("code_ppds");
                $tbl_order_no_date = new tbl_order_no_date();
                $max_orderdate_time_code = $tbl_order_no_date->where('districtcd', '=', $forDist)
                        ->max('order_cd');
//echo $max_orderdate_time_code;
                if ($max_orderdate_time_code == "") {
                    $orderDateTimeCode = $forDist . "01";
                } else {
                     $tmp_code=100+substr($max_orderdate_time_code,-2)+1;
                   //  echo $tmp_code;
                     $orderDateTimeCode=$forDist.substr($tmp_code,-2);
                  //  echo $orderDateTimeCode;die;
//                    $max_orderdate_time_code = $max_orderdate_time_code + 1;
//                    $orderDateTimeCode = $max_orderdate_time_code;
                }
                $save_order_no_date = new tbl_order_no_date();
                $save_order_no_date->districtcd = $forDist;
                $save_order_no_date->order_cd = $orderDateTimeCode;
                $save_order_no_date->order_dt = date('Y-m-d', strtotime(trim(str_replace('/', '-', $order_date))));
                $save_order_no_date->order_no = $order_no;
                $save_order_no_date->usercode = $user_code;

                $save_order_no_date->save();
                $response = array(
                    'options' => $save_order_no_date,
                    'status' => 1);
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }

    public function getOrderDateAndTimeRecords(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            $this->validate($request, [
                'forDist' => 'required|alpha_num|min:2|max:2'
                    ], [
                'forDist.required' => 'District is required',
                'forDist.alpha_num' => 'District code must be an alpha numeric'
            ]);
            try {
                $forDist = $request->forDist;
                $forDateNo = tbl_order_no_date::where('districtcd', '=', $forDist)
                                ->select('order_dt', 'order_no', 'order_cd')
                                ->orderBy('order_cd')->get();
                //print_r($forDateTime);die;
                $DateNo = "";
                $zoAr = json_decode($forDateNo);
                //  print_r($zoAr);die;
                $DateNo .= "<table class='table table-bordered table-striped1' width='100%' style='border-top: 2px solid #009fe8;'>";
                $DateNo .= "<tr style='background-color: #f5f8fa'>";
                $DateNo .= "<th colspan='4'><span class='highlight'>List of Order date & no</span>";
                $DateNo .= "</th>";
                $DateNo .= "</tr>";
                $DateNo .= "<tr style='background-color: #f5f8fa'>";
                $DateNo .= "<td width='10%'><b>SL#</b></td><td><b>Order Date</b></td><td><b>Order No </b></td><td width='10%'><b>Action</b></td>";
                $DateNo .= "</tr>";
                $count = 0;
                foreach ($zoAr as $fz) {
                    $count++;
                    $DateNo .= "<tr><td>" . $count . "</td>";
                    $DateNo .= "<td>" . date('d/m/Y', strtotime(trim(str_replace('/', '-', $fz->order_dt)))) . "</td>";
                    $DateNo .= "<td>" . $fz->order_no . "</td>";
                    $DateNo .= "<td><a title='Edit'  onclick='editOrderDateTime(" . json_encode($fz->order_cd) . ");'><i class='fa fa-pencil-alt' style='color:green;cursor:pointer;' value=" . json_encode($fz->order_cd) . "></i></a>&nbsp;&nbsp;";
                    $DateNo .= "<a title='Delete'  onclick='deleteOrderDateTime(" . json_encode($fz->order_cd) . ");'><i class='fa fa-trash-alt' style='color:red;cursor:pointer;' value=" . json_encode($fz->order_cd) . "></i></td></tr>";
                }

                $DateNo .= "</table>";
                $response = array(
                    'options' => $DateNo,
                    'status' => 1);
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }

    public function editOrderDateTime(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            $this->validate($request, [
                'order_cd' => 'required|alpha_num|min:4|max:4'
                    ], [
                'order_cd.required' => 'Order code is required',
                'order_cd.alpha_num' => 'Order code must be an alphanumeric',
                'order_cd.min' => 'Order code should be four characters',
                'order_cd.max' => 'Order code may not be greater than four characters'
            ]);
            try {
                $order_cd = $request->order_cd;
                $order_cd_show = tbl_order_no_date::select(DB::raw("(DATE_FORMAT(order_no_date.order_dt,'%d/%m/%Y')) as odDaTi"), 'order_no', 'order_cd')
                                ->where('order_cd', '=', $order_cd)->get();
                $response = array(
                    'options' => $order_cd_show,
                    'status' => 1);
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }

    public function update_order_table(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
             
              $order_no = $request->order_no;
            $this->validate($request, [
                'districtcd' => 'required|alpha_num|min:2|max:2',
                'order_date' => 'required|date_format:d/m/Y|min:10|max:10',
                'order_no' => 'required|regex:/^[A-Za-z0-9\s()\/ ]+$/|max:15|unique:order_no_date,order_no,' . $order_no . ',order_no',
                'order_cd_edit_code' => 'required|alpha_num|min:4|max:4'
                    ], [
                'districtcd.required' => 'District is required',
                'districtcd.alpha_num' => 'District must be an alpha numeric',
                'order_date.required' => 'Order date is required',
                'order_date.date_format' => 'Order date format should be d/m/y',
                'order_no.required' => 'Order no is required',
                'order_no.regex' => 'Order no can consist of alphanumerical characters',
                'order_no.max' => 'Order no must not be greater than 15 characters',
                'order_cd_edit_code.required' => 'Order code is required',
                'order_cd_edit_code.alpha_num' => 'Order code must be an alphanumeric',
                'order_cd_edit_code.min' => 'Order code should be four characters',
                'order_cd_edit_code.max' => 'Order code may not be greater than four characters',
                        'order_no.unique'=>'Order no has already been taken'
            ]);
            try {
                $forDist = $request->districtcd;

                $user_code = session()->get("code_ppds");
                $update_order_date_no = new tbl_order_no_date();
               $order_date = date('Y-m-d', strtotime(trim(str_replace('/', '-', $request->order_date))));
               
                $order_cd = $request->order_cd_edit_code;
                $update_order_date_no->order_dt = $order_date;
                $update_order_date_no->order_no = $order_no;


                $upadtez = tbl_order_no_date::where('order_cd', '=', $order_cd)
                        ->update(['order_dt' => $update_order_date_no->order_dt, 'order_no' => $update_order_date_no->order_no, 'usercode' => $user_code]);

                $response = array(
                    'options' => $upadtez,
                    'status' => 2);
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }

    public function deleteOrderDateTime(Request $request) {
        $statusCode = 200;
        if (!$request->ajax()) {
            $statusCode = 400;
            $response = array('error' => 'Error occured in Ajax Call.');
            return response()->json($response, $statusCode);
        } else {
            $this->validate($request, [
                'order_cd' => 'required|alpha_num|min:4|max:4'
                    ], [
                'order_cd.required' => 'Order code is required',
                'order_cd.alpha_num' => 'Order code must be an alphanumeric',
                'order_cd.min' => 'Order code should be four characters',
                'order_cd.max' => 'Order code may not be greater than four characters'
            ]);
            try {
                $order_cd = $request->order_cd;

//                $tbl_first_training_date_time_chking = tbl_first_training_schedule::select('datetimecd')
//                        ->where('datetimecd', '=', $date_time_cd);
//                $tbl_first_training_date_time_chking_count = $tbl_first_training_date_time_chking->count();
                //  if ($tbl_first_training_date_time_chking_count == 0) {
                $record = tbl_order_no_date::where('order_cd', '=', $order_cd); //Should be changed #27

                if (!empty($record)) {//Should be changed #30
                    $record = $record->delete();
                }

                $response = array(
                    'record' => $record, //Should be changed #32
                    'status' => 1
                );
//                } else {
//                    $response = array(
//                        'options' => "Record exist in another table, you cannot delete this record",
//                        'status' => 2);
//                }
            } catch (\Exception $e) {
                $response = array(
                    'exception' => true,
                    'exception_message' => $e->getMessage(),
                );
                $statusCode = 400;
            } finally {
                return response()->json($response, $statusCode);
            }
        }
    }

}
