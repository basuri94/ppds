<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tbl_assembly_zone extends Model
{
    protected $table ='assembly_zone';
    public $timestamps = false;
    protected $fillable = [
    'assemblycd','districtcd','zone','rand_status1','rand_status2','rand_status3','rand_statusM'
    ];
}
