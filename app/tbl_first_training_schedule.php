<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tbl_first_training_schedule extends Model
{
    protected $table ='first_training_schedule';
    public $timestamps = false;
     public function ppDetailVenueBrief(){
         $filtered="";
         $session_personnela=session()->get('personnela_ppds');
         $filtered=$this->hasMany('App\tbl_personnela','1stTrainingSch','schedule_code')
                 ->join('personnel','personnel.personcd','=',''.$session_personnela.'.personcd')
                 ->join('poststat','poststat.post_stat','=',''.$session_personnela.'.poststat')
                 ->where('selected','=',1)
                 ->select(array('1stTrainingSch',''.$session_personnela.'.personcd','personnel.officer_name','personnel.off_desg as designation','poststat.poststatus','personnel.acno','personnel.partno','personnel.slno','personnel.mob_no','personnel.bank_acc_no', 'personnel.branch_ifsc','personnel.assembly_temp','personnel.assembly_off','personnel.assembly_perm','personnel.partno','personnel.slno','personnel.epic'))
                // ->orderBy(''.$session_personnela.'.personcd',''.$session_personnela.'.poststat')
                 ->orderBy('personnel.officer_name');
                //  dd($filtered);
         return $filtered;
     }
      public function ppDetailVenueBrief2(){
         $filtered="";
         $session_personnela=session()->get('personnela_ppds');
         $filtered=$this->hasMany('App\tbl_personnela','1stTrainingSch_2','schedule_code')
                 ->join('personnel','personnel.personcd','=',''.$session_personnela.'.personcd')
                 ->join('poststat','poststat.post_stat','=',''.$session_personnela.'.poststat')
                 ->where('selected','=',1)
                 ->select(array('1stTrainingSch_2',''.$session_personnela.'.personcd','personnel.officer_name','personnel.off_desg as designation','poststat.poststatus','personnel.acno','personnel.partno','personnel.slno','personnel.mob_no','personnel.bank_acc_no', 'personnel.branch_ifsc','personnel.assembly_temp','personnel.assembly_off','personnel.assembly_perm','personnel.partno','personnel.slno','personnel.epic'))
                 //->orderBy(''.$session_personnela.'.personcd',''.$session_personnela.'.poststat');
         ->orderBy('personnel.officer_name');
         return $filtered;
     }
}
