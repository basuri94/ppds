<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tbl_cancellation_log_reserve extends Model
{
     protected $table ='cancellation_log_reserve';
    public $timestamps = false;
}
