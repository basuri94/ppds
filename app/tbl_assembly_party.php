<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tbl_assembly_party extends Model
{
    protected $table ='assembly_party';
    public $timestamps = false;
    protected $fillable = [
    'assemblycd'
    ];
}
