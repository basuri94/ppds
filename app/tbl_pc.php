<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tbl_pc extends Model
{
    protected $table ='pc';
    
    protected $fillable = [
     'pccd','pcname','usercode','posted_date',
    ];
}
