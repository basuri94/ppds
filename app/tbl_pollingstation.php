<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tbl_pollingstation extends Model
{
    protected $table ='pollingstation';
    public $timestamps = false;
}
