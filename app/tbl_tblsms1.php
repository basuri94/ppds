<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tbl_tblsms1 extends Model
{
    protected $table ='tblsms1';
        public $timestamps =false;
    protected $fillable = [
        'name','phone_no','message','code','training_code','forzone','phase_id', 'smscount'
    ];
}
