<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tbl_replacement_log extends Model
{
    protected $table ='replacement_log';
    public $timestamps = false;
}
