<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tbl_user_permission extends Model
{
   protected $table ='user_permission';
    
    protected $fillable = [
       'code','user_cd','menu_cd','submenu_cd','sub_submenu_cd',
    ];
 
    public function menu() {
         return $this->belongsTo('App\tbl_menu','menu_cd','menu_cd')->select(array('menu_cd','menu','link'))->orderby('menu_cd');
    }
    public function sub_menu() {
         return $this->belongsTo('App\tbl_submenu','submenu_cd','submenu_cd')->select(array('submenu_cd','submenu','link'))->orderby('submenu_cd'); 
    }
  
};
