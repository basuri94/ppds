<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tbl_govtcategory extends Model
{
    protected $table ='govtcategory';
    
protected $fillable = [
'govt','govt_description','usercode','posted_date'
];
}
