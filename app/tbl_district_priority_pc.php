<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tbl_district_priority_pc extends Model
{
    protected $table ='district_priority_pc';
    public $timestamps = false;
}
