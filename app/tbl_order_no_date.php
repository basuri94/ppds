<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tbl_order_no_date extends Model
{
     protected $table ='order_no_date';
     public $timestamps = false;
}
