<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tbl_menu extends Model
{
    protected $table ='menu';
    
    protected $fillable = [
      'menu_cd','menu','link','usercode','posted_date',
    ];
    public function menu_wise_submenu() {
        return $this->hasMany('App\tbl_submenu','menu_cd','menu_cd')->select(array('submenu_cd','menu_cd','submenu','link'));
        
    }
}
