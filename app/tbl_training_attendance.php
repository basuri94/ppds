<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tbl_training_attendance extends Model
{
    protected $table ='training_attendance';
    public $timestamps = false;
}
