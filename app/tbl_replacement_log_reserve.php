<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tbl_replacement_log_reserve extends Model
{
    protected $table ='replacement_log_reserve';
    public $timestamps = false;
}
