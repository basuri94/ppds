@extends('layouts.master')
@section('content')
<!--// top-bar -->
 <div class="wrapper">
<!-- Sidebar Holder -->
@include('layouts.sidebar')

<!-- Page Content Holder -->
<div id="content">           
<section class="tables-section">
    <nav aria-label="breadcrumb" style="width:100%;">
        <div class="breadcrumb pagehead1">
           <span class="headbrand"><i class="fa fa-desktop"></i> Add Party Separation DCRC
            <span class="scoop-mcaret1"></span>
           </span>&nbsp;&nbsp;&nbsp;         
                {!! Form::button(' Add/Edit DCRC Party&nbsp;<i class="fa fa-plus-circle"></i>',['id'=>'add_dcrc_party','class' => 'btn btn-primary-year add-new-button']) !!}  
                &nbsp;{!! Form::button(' List of DCRC party&nbsp;<i class="fa fa-arrow-circle-right"></i>',['id'=>'show_dcrc_party_list','class' => 'btn btn-primary-year add-new-button']) !!}                 
                <div class="clearfix"></div>
        </div>
    </nav>

    <!-- form -->
<div class="outer-w3-agile">
  {!! Form::open(['url' => 'DCRCPartySeparate', 'name' => 'DCRCPartySeparate', 'class' =>'request-info clearfix form-horizontal', 'id' => 'DCRCPartySeparate', 'method' => 'post','role'=>'','files' => true]) !!}
  {!! Form::hidden('districtcd', session()->get('districtcd_ppds'),['id'=>'districtcd']) !!}
  
  <div class="panel-group" id="accordion5">                                                              
   <div class="panel panel-default">
    <div class="panel-heading1">
     <h6 class="panel-title">
         <a data-toggle="#" data-parent="#accordion5" href="#collapsePV"> <span class="fa fa-minus"></span> <span class="highlight">Add Party Separation DCRC</span></a>
     </h6>
    </div>
    <div id="collapsePV" class="panel-collapse collapse5">
      <div class="panel-body">                                   
       <div class="row"> 
         <div class='col-sm-4'>
               {!! Form::label('zone', 'For Zone:', ['class'=>'highlight required']) !!}
              <div class="form-group">
                  <div class=''>
                      {!! Form::select('zone',[''=>'[Select]'],Null,['id'=>'zone','class'=>'form-control','autocomplete'=>'off']) !!}
                  </div>
              </div>
          </div>  
         <div class='col-sm-4'>
            {!! Form::label('assembly', 'Assembly:', ['class'=>'highlight required']) !!}
              <div class="form-group">
                  <div class=''>
                      {!! Form::select('assembly',[''=>'[Select]'],null,['id'=>'assembly','class'=>'form-control','autocomplete'=>'off']) !!}
                  </div>
              </div>
        </div> 


           <div class='col-sm-4'>
               {!! Form::label('no_of_member', 'No of Member:', ['class'=>'highlight required']) !!}
                <div class="form-group">
                    <div class=''>
                        {!! Form::select('no_of_member',['4'=>'4'],Null,['id'=>'no_of_member','class'=>'form-control','autocomplete'=>'off']) !!}
                    </div>
                </div>
          </div>
      </div>
      <div class="row"> 
         <div class='col-sm-4'>
               {!! Form::label('gender', 'Gender:', ['class'=>'highlight required']) !!}
               <div class="form-group">
                   <div class=''>
                       {!! Form::select('gender',config('constants.GENDER'),null,['id'=>'gender','class'=>'form-control','autocomplete'=>'off']) !!}
                   </div>
               </div>
          </div>  
           <div class='col-sm-4'>
                {!! Form::label('Party', 'Number of party:', ['class'=>'highlight required']) !!}
               <div class="form-group">
                   <div class=''>
                        {!! Form::text('Party',null,['id'=>'Party','class'=>'form-control','autocomplete'=>'off','maxlength'=>'4']) !!}
                   </div>
               </div>
           </div>
          <div class='col-sm-4'>
               {!! Form::label('dcvenue', 'DC Venue,Date & Time:', ['class'=>'highlight required']) !!}
              <div class="form-group">
                  <div class=''>
                      {!! Form::select('dcvenue',[''=>'[Select]'],Null,['id'=>'dcvenue','class'=>'form-control','autocomplete'=>'off']) !!}
                  </div>
              </div>
          </div>
      </div>
           
      <div class="row">                  
         <div class='col-sm-12'>
            <div class="form-group text-right permit">                            	
               {{ Form::button('Submit', ['class' => 'btn btn-info', 'type' => 'submit','id'=>'Submit']) }}
               {{ Form::button('Reset', ['class' => 'btn btn-success' ,'type' => 'reset','id'=>'reset']) }}
            </div>
          </div>
      </div>     
       
    </div>                              
   </div>
  </div>  
  </div>
 {!! Form::close() !!}             
</div>
<!--// form -->

</section>

</div>
</div>
<!-- Copyright -->
<script type="text/javascript">
$(document).ready(function () {
  char_num("#Party");
   $("select[name='no_of_member'").append('<option value="5">5</option><option value="6">6</option>');
  getZoneDetails();
$("#add_dcrc_party").click(function () {
     window.location.href = "dcrc_party";
});
$("#show_dcrc_party_list").click(function(){
    window.location.href = "dcrc_party_list"; 
});
$('select[name="zone"]').on('change', function () {
    getAssemblyDetails(); 
 });
$('select[name="assembly"]').on('change', function () {
    getPartyNumber(); 
 });
  $('select[name="gender"]').on('change', function () {
    getPartyNumber();
 });
  $('select[name="no_of_member"]').on('change', function () {
    getPartyNumber();
 }); 
$("#reset").click(function () {
  location.reload(true);  
});


$('#DCRCPartySeparate').bootstrapValidator({

        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
             zone: {
                validators: {
                    notEmpty: {
                        message: 'Zone is required'
                    }
                }
            },
            assembly: {
                validators: {
                    notEmpty: {
                        message: 'Assembly is required'
                    }
                }
            },
            
            gender: {
                validators: {
                    notEmpty: {
                        message: 'Gender is required'
                    }
                }
            },
            Party: {
                validators: {
                    notEmpty: {
                        message: 'Number of party is required'
                    }
                }
            },
            
            dcvenue: {
                validators: {
                    notEmpty: {
                        message: 'DC Venue,Date & Time is required'
                    }
                }
            }           
        }

    }).on('success.form.bv', function (e) {
        e.preventDefault();
        var action = $(this).attr('action');
        var type = $(this).attr('method');
        var formData = new FormData($(this)[0]);
       $(".se-pre-con").fadeIn("slow");
        $.ajax({
            type: type,
            url: action,
            data: formData,
            processData: false,
            contentType: false,
            dataType: "json",
            success: function (data) {
                $(".se-pre-con").fadeOut("slow");

                if (data.status == 1) {        
                     var msg="Record(s) saved successfully";
                     $.confirm({
                        title: 'Success!',
                        type: 'green',
                        icon: 'fa fa-check',
                        content: msg,
                        buttons: {
                            ok: function () {     
                                location.reload(true);
                            }

                        }
                    });

                } else if (data.status == 0){
                   $.alert({
                        title: 'Error!!',
                        type: 'red',
                        icon: 'fa fa-exclamation-triangle',
                        content: 'First Training allocation is empty'
                    });
                    $("#Submit").attr('disabled',false);  
                }else{
                    //var msg="Record(s) updated successfully";
                    var msg="Venue can not be same in same Training Date & Time";
                     $.confirm({
                        title: 'Error!!',
                        type: 'red',
                        icon: 'fa fa-exclamation-triangle',
                        content: msg,
                        buttons: {
                            ok: function () {     
                                location.reload(true);
                            }

                        }
                    });

                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
            $(".se-pre-con").fadeOut("slow");
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-exclamation-triangle',
                  content: msg
              });
              $("#Submit").attr('disabled',false);
           }
        });
    });
});
function getZoneDetails(){
    var forDist = $("#districtcd").val();
    var token = $("input[name='_token']").val();
    $(".se-pre-con").fadeIn("slow");
    $.ajax({
       url: "getZoneDetails",
       method: 'POST',
       data: {forDist: forDist, _token: token},
       success: function (data) {//alert(data.options);
         $(".se-pre-con").fadeOut("slow");
          $("select[name='zone'").html('');
          if(data.status==1)
          {
             $("select[name='zone'").append('<option value>[Select]</option>');
            $.each(data.options,function (k, v)
            {
                $("select[name='zone'").append('<option value='+k+'>'+v+'</option>');
            });
           // getMemberDetails();
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
            $(".se-pre-con").fadeOut("slow");
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-warning',
                  content: msg
              });
         }
   });
 }
function getAssemblyDetails(){
    var forZone = $("#zone").val();
     var token = $("input[name='_token']").val();
     $(".se-pre-con").fadeIn("slow");
     $.ajax({
       url: "getAssemblyDetails",
       method: 'POST',
       data: {forZone: forZone, _token: token},
       success: function (data) {//alert(data.options);
         $(".se-pre-con").fadeOut("slow");
          $("select[name='assembly'").html('');
          if(data.status==1)
          {
              $("select[name='assembly'").html(data.options);
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
            $(".se-pre-con").fadeOut("slow");
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-exclamation-triangle',
                  content: msg
              });
         },complete: function(){
           getDCVenueDetails();
         }
   });
}
function getDCVenueDetails(){
     var forZone = $("#zone").val();
     var token = $("input[name='_token']").val();
    // $(".se-pre-con").fadeIn("slow");
     $.ajax({
       url: "getDCVenueDetails",
       method: 'POST',
       data: {forZone: forZone, _token: token},
       success: function (data) {//alert(data.options);
        // $(".se-pre-con").fadeOut("slow");
          $("select[name='dcvenue'").html('');
          if(data.status==1)
          {
             $("select[name='dcvenue'").append('<option value>[Select]</option>');
            $.each(data.options,function (k, v)
            {
                $("select[name='dcvenue'").append('<option value='+k+'>'+v+'</option>');
            });
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
            $(".se-pre-con").fadeOut("slow");
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-exclamation-triangle',
                  content: msg
              });
         }
   });
}
function getPartyNumber(){
    var assembly = $("#assembly").val();
    var gender = $("#gender").val();
    var no_of_member = $("#no_of_member").val();
    if(assembly !="" && gender !="" && no_of_member !=""){
        var token = $("input[name='_token']").val();
        // $(".se-pre-con").fadeIn("slow");
         $.ajax({
           url: "getPartyNumber",
           method: 'POST',
           data: {gender:gender,assembly:assembly,no_of_member:no_of_member, _token: token},
           success: function (data) {//alert(data.options);
            // $(".se-pre-con").fadeOut("slow");
             $("#Party").val("");
              if(data.status==1)
              {
                  $("#Party").val(data.options);
              }
           },
            error: function (jqXHR, textStatus, errorThrown) {
                  var msg = "";
                  if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                      msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                  } else {
                      if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                          msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                      } else {
                          msg += "Error(s):<strong><ul>";
                          $.each(jqXHR.responseJSON, function (key, value) {
                              msg += "<li>" + value + "</li>";
                          });
                          msg += "</ul></strong>";
                      }
                  }
                  $.alert({
                      title: 'Error!!',
                      type: 'red',
                      icon: 'fa fa-exclamation-triangle',
                      content: msg
                  });
             }
       });
    }
}
</script>
@stop