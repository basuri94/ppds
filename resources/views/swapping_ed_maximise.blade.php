
@extends('layouts.master')
@section('content')

<!--    <div class="container-fluid clearfix">
        <div class="row">
        	<nav aria-label="breadcrumb" style="width:100%;">
                <ol class="breadcrumb" style="width:100%; margin-bottom:0px; background-color: #bad3e8; border-radius:0px;padding-top: 5px;padding-bottom: 5px;font-size: 14px;">
                    <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                    <li class="breadcrumb-item"><a href="#">Form</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Form</li>
                </ol>
            </nav>
        </div>
    </div>-->
 <!--// top-bar -->
 <div class="wrapper">
<!-- Sidebar Holder -->
@include('layouts.sidebar')

<!-- Page Content Holder -->
<div id="content">           
<section class="tables-section">
    <nav aria-label="breadcrumb" style="width:100%;">
        <div class="breadcrumb pagehead1">
           
                {!! Form::button('<i class="fa fa-desktop"></i> Swapping (ED Maximise)',['class' => 'btn btn-primary-header add-new-button']) !!}
            
            <div class="col-md-offset-8 pull-right">
<!--                {!! Form::button('Academic Year:&nbsp;',['id'=>'show_academic','class' => 'btn btn-primary-year']) !!}-->
                
            </div>
                <div class="clearfix"></div>
        </div>
    </nav>

    <!-- form -->
<div class="outer-w3-agile">
  {!! Form::open(['url' => 'swapping_personnel', 'name' => 'swapping_personnel', 'class' =>'request-info clearfix form-horizontal', 'id' => 'swapping_personnel', 'method' => 'post','role'=>'','files' => true]) !!}
  {!! Form::hidden('districtcd', session()->get('districtcd_ppds'),['id'=>'districtcd']) !!}
  {!! Form::hidden('blockMunicipality','',['id'=>'blockMunicipality']) !!} 
  {!! Form::hidden('officeName','',['id'=>'officeName']) !!} 
<!--  <span class="highlight">[Note: Record(s) transfered from Personnel to Zonewise]</span>-->
   <div class="panel-group" id="accordion5">                                                              
    <div class="panel panel-default">
     <div class="panel-heading1">
     <h6 class="panel-title">
         <a data-toggle="#" data-parent="#accordion5" href="#collapseUV"> <span class="fa fa-minus"></span> <span class="highlight">Personnel Records (From same District or others District)</span></a>
     </h6>
     </div>
     <div id="collapseUV" class="panel-collapse collapse5">
      <div class="panel-body">                                   
        <div class="row"> 
           <div class='col-sm-3'>
            {!! Form::label('forDist', 'From District:', ['class'=>'highlight required']) !!}
           <div class="form-group">
               <div class=''>
                   {!! Form::select('forDist',[''=>'[Select]'],Null,['id'=>'forDist','class'=>'form-control','autocomplete'=>'off']) !!}
                    <img src='images/loading_b.gif' width='70px' height='75px' id='loader'> 
               </div>
           </div>
         </div> 
           <div class='col-sm-3'>
               {!! Form::label('subdivision', 'Sub Division:', ['class'=>'highlight required']) !!}
              <!--<span class="highlight">Sub Division</span> -->
              <div class="form-group">
                  <div class=''>
                      {!! Form::select('subdivision',[''=>'[Select]'],null,['id'=>'subdivision','class'=>'form-control','autocomplete'=>'off']) !!}
                  </div>
              </div>
          </div>
            

           <div class='col-sm-3'>
               {!! Form::label('ExAs1', 'Excluding Assembly 1:', ['class'=>'highlight']) !!}
              <div class="form-group">
                  <div class=''>           
                  {!! Form::text('ExAs1',null,['id'=>'ExAs1','class'=>'form-control','autocomplete'=>'off','maxlength' =>'3']) !!}
                 </div>
              </div>
          </div>
            
          <div class='col-sm-3'>
              {!! Form::label('ExAs2', 'Excluding Assembly 2:', ['class'=>'highlight']) !!}
              <div class="form-group">
                  <div class=''>
                      {!! Form::text('ExAs2',null,['id'=>'ExAs2','class'=>'form-control','autocomplete'=>'off','maxlength' =>'3']) !!}
                  </div>
              </div>
          </div> 
         </div>
          
         <div class="row">                                   
           
          <div class='col-sm-3'>
              {!! Form::label('ExAs3', 'Excluding Assembly 3:', ['class'=>'highlight']) !!}
              <div class="form-group">
                  <div class=''>
                      {!! Form::text('ExAs3',null,['id'=>'ExAs3','class'=>'form-control','autocomplete'=>'off','maxlength' =>'3']) !!}
                  </div>
              </div>
          </div>
                                              
            <div class='col-sm-3'>
                {!! Form::label('Employee', 'Number of employee:', ['class'=>'highlight']) !!}
                 <span style="color:red;font-size: 11px;">[Max:4]</span>
               <div class="form-group">
                   <div class=''>
                        {!! Form::text('Employee',null,['id'=>'Employee','class'=>'form-control','autocomplete'=>'off','maxlength' =>'4']) !!}
                   </div>
               </div>
           </div>
            <div class='col-sm-3'>
               {!! Form::label('gender', 'Gender:', ['class'=>'highlight']) !!}
               <div class="form-group">
                   <div class=''>
                       {!! Form::select('gender',config('constants.GENDER'),null,['id'=>'gender','class'=>'form-control','autocomplete'=>'off']) !!}
                   </div>
               </div>
           </div>
           <div class='col-sm-3'>
               {!! Form::label('poststatus', 'Post Status:', ['class'=>'highlight']) !!}
               <div class="form-group">
                   <div class=''>
                       {!! Form::select('poststatus',$poststat['poststatus'],null,['id'=>'poststatus','class'=>'form-control','placeholder'=>'[Select]','autocomplete'=>'off']) !!}
                   </div>
               </div>
           </div>
         </div>
        <div class="row"> 
            <div class='col-sm-12'>
                <div id='memberDetails'></div>
            </div>
        </div>  
          
       </div>
      </div>
    </div>                              
  </div> 
  
  <div class="panel-group" id="accordion5">                                                              
   <div class="panel panel-default">
    <div class="panel-heading1">
     <h6 class="panel-title">
         <a data-toggle="#" data-parent="#accordion5" href="#collapsePV"> <span class="fa fa-minus"></span> <span class="highlight">Zonewise Records</span> </a>
     </h6>
    </div>
    <div id="collapsePV" class="panel-collapse collapse5">
      <div class="panel-body">                                   
       <div class="row">  
         
           
         <div class='col-sm-4'>
            {!! Form::label('forZone', 'For Zone:', ['class'=>'highlight required']) !!}
           <div class="form-group">
               <div class=''>
                   {!! Form::select('forZone',[''=>'[Select]'],Null,['id'=>'forZone','class'=>'form-control','autocomplete'=>'off']) !!}
               </div>
           </div>
        </div> 
      </div>
      <div class="row"> 
            <div class='col-sm-12'>
                <div id='forZoneDetailsReqiured'></div>
            </div>
         
      </div> 
       <div class="row"> 
            <div class='col-sm-12'>                    
            <div class="form-group text-right permit">                            	
               {{ Form::button('Swapping', ['class' => 'btn btn-info', 'type' => 'submit','id'=>'swapping']) }}
               {{ Form::button('Reset', ['class' => 'btn btn-success' ,'type' => 'reset','id'=>'reset']) }}
            </div>
            </div>
      </div>     
       
    </div>                              
   </div>
  </div>  
  </div>
 {!! Form::close() !!}             
</div>
<!--// form -->
</section>

</div>
</div>
<!-- Copyright -->
<script type="text/javascript">
$(document).ready(function () {
  getZoneDetails();
  $("#reset").click(function () {
  location.reload(true);  
});
char_num("#Employee");
 $('select[name="forDist"]').on('change', function () {
    change_subdivision();
 });
 $('select[name="subdivision"]').on('change', function () {
   // change_blockMuni();
   getMemberDetails();
     
 });
  $('select[name="blockMunicipality"]').on('change', function () {
    change_officeName();
 });
  $('select[name="officeName"]').on('change', function () {
    getMemberDetails();
 });
 $('select[name="gender"]').on('change', function () {
    getMemberDetails();
 });
 $('#ExAs1').on('change', function () {
    getMemberDetails();
 });
 $('#ExAs2').on('change', function () {
    getMemberDetails();
 });
 $('#ExAs3').on('change', function () {
    getMemberDetails();
 });

 $('select[name="forZone"]').on('change', function () {
    getZoneDetailsReqiured();
 });
 $("#swapping_personnel").bootstrapValidator({
            message: 'This value is not valid',
            feedbackIcons: {
                valid: 'fa fa-check',
                invalid: 'fa fa-times',
                validating: 'fa fa-refresh',
            },

            fields: {

                subdivision: {
                    validators: {
                        notEmpty: {
                            message: 'Subdivision is required'
                        }
                    }
                },

                forDist: {
                    validators: {
                        notEmpty: {
                            message: 'From District is required'
                        }
                    }
                },
                forZone: {
                    validators: {
                        notEmpty: {
                            message: 'For Zone is required'
                        }
                    }
                },
                ExAs1: {
                    validators: {
                        stringLength: {
                            min: 3,
                            max: 3,
                            message: 'Assembly should be 3 digit'
                        }
                    }
                },
                ExAs2: {
                    validators: {
                        stringLength: {
                            min: 3,
                            max: 3,
                            message: 'Assembly should be 3 digit'
                        }
                    }
                },
                ExAs3: {
                    validators: {
                        stringLength: {
                            min: 3,
                            max: 3,
                            message: 'Assembly should be 3 digit'
                        }
                    }
                },
                Employee: {
                    validators: {
                        stringLength: {
                            min: 1,
                            message: 'Number of employee should be minimum 1 digit'
                        }
                    }
                }

            }
        }).on('success.form.bv', function (e) {
    // Prevent form submission
    e.preventDefault();
    var subdivision = $("#subdivision").val();
    var blockmuni = $("#blockMunicipality").val();
    var officeName = $("#officeName").val();
    var poststatus = $("#poststatus").val();
    var gender = $("#gender").val();
    var ExAs1 = $("#ExAs1").val();
    var ExAs2 = $("#ExAs2").val();
    var ExAs3 = $("#ExAs3").val(); 
    var forZone = $("#forZone").val();
    var Employee = $("#Employee").val();
    var forDist = $("#forDist").val();
    var districtcd = $("#districtcd").val();
     
    var token = $("input[name='_token']").val();
  
     var fd1 = new FormData();
    fd1.append('subdivision', subdivision);
    fd1.append('blockmuni', blockmuni);
    fd1.append('officeName', officeName);
    fd1.append('poststatus', poststatus);
    fd1.append('gender', gender);
    fd1.append('ExAs1', ExAs1);
    fd1.append('ExAs2', ExAs2);
    fd1.append('ExAs3', ExAs3);
    fd1.append('forZone', forZone);
    fd1.append('Employee', Employee);
    fd1.append('forDist', forDist);
    fd1.append('districtcd', districtcd);
    fd1.append('_token', token);

    
    $.ajax({
        type: "post",
        url: "swapping_personnel_ed_maximise",
        data: fd1,
        processData: false,
        contentType: false,
        dataType: 'json',
        beforeSend: function(){
         // Show image container
          $("#swapping").attr('disabled',true);
          $(".se-pre-con1").fadeIn("slow");
        },
        success: function (data) {
           if(data.status==1)
           {
             $(".se-pre-con1").fadeOut("slow");            
             var msg=data.options+" Record(s) swapped successfully";
              $.confirm({
                        title: 'Success!',
                        type: 'green',
                        icon: 'fa fa-check',
                        content: msg,
                        buttons: {
                            ok: function () {
                                /*$("#swapping_personnel")[0].reset();
                                $("#swapping").attr('disabled',true);
                                $('.form-control-feedback').css('display', 'none');
                                $('.has-success').removeClass('has-success');*/
                               // location.reload(true);
                               $("#swapping").attr('disabled',false);
                               getMemberDetails();
                               getZoneDetailsReqiured();
                            }

                        }
                    });
             }

            },
           error: function (jqXHR, textStatus, errorThrown) {
            $(".se-pre-con1").fadeOut("slow");
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-warning',
                  content: msg
              });
         }
    });
 });
});
function change_subdivision(){
  var forDist = $("#forDist").val();
   var token = $("input[name='_token']").val();
  // $(".se-pre-con").fadeIn("slow");
   $.ajax({
       url: "getSubdivisionData",
       method: 'POST',
       data: {forDist: forDist, _token: token},
       success: function (data) {//alert(data.options);
       //   $(".se-pre-con").fadeOut("slow");
          $("select[name='subdivision'").html('');
          
          if(data.status==1)
          {
            $("select[name='subdivision'").append('<option value>[Select]</option>');
            $.each(data.options,function (k, v)
            {
                $("select[name='subdivision'").append('<option value='+k+'>'+v+'</option>');
            });
            //$("select[name='subdivision'").html(data.options);
             // change_officeName();
             
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
         $(".se-pre-con").fadeOut("slow");
           var msg = "";
           if (jqXHR.status !== 422 && jqXHR.status !== 400) {
               msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
           } else {
               if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                   msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
               } else {
                   msg += "Error(s):<strong><ul>";
                   $.each(jqXHR.responseJSON, function (key, value) {
                       msg += "<li>" + value + "</li>";
                   });
                   msg += "</ul></strong>";
               }
           }
           $.alert({
               title: 'Error!!',
               type: 'red',
               icon: 'fa fa-warning',
               content: msg
           });
         },complete: function(){
          getMemberDetails();
         }
   });
}
function change_blockMuni() {
   var subdivision = $("#subdivision").val();
   var token = $("input[name='_token']").val();
  // $(".se-pre-con").fadeIn("slow");
   $.ajax({
       url: "getBlockMuni",
       method: 'POST',
       data: {subdivision: subdivision, _token: token},
       success: function (data) {//alert(data.options);
       //   $(".se-pre-con").fadeOut("slow");
          $("select[name='blockMunicipality'").html('');
          if(data.status==1)
          {
            $("select[name='blockMunicipality'").html(data.options);
             //getMemberDetails();
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
            $(".se-pre-con").fadeOut("slow");
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-warning',
                  content: msg
              });
         },complete: function(){
           change_officeName();
         }
   });
 }
 function change_officeName(){
     var blockmuni = $("#blockMunicipality").val();
     var subdivision = $("#subdivision").val();
     var token = $("input[name='_token']").val();
   // $(".se-pre-con").fadeIn("slow");
     $.ajax({
       url: "getOfficeName",
       method: 'POST',
       data: {subdivision: subdivision,blockmuni: blockmuni, _token: token},
       success: function (data) {//alert(data.options);
       //   $(".se-pre-con").fadeOut("slow");
          $("select[name='officeName'").html('');
          if(data.status==1)
          {
            //$("select[name='officeName'").html(data.options);
             $("select[name='officeName'").append('<option value>[Select]</option>');
            $.each(data.options,function (k, v)
            {
                $("select[name='officeName'").append('<option value='+k+'>'+v+'</option>');
            });
            
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
            $(".se-pre-con").fadeOut("slow");
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-warning',
                  content: msg
              });
         },complete: function(){
           getMemberDetails();
         }
     });
 }
 function getZoneDetails(){
     var forDist = $("#districtcd").val();
     var token = $("input[name='_token']").val();
     $(".se-pre-con").fadeIn("slow");
     $.ajax({
       url: "getZoneDetails",
       method: 'POST',
       data: {forDist: forDist, _token: token},
       success: function (data) {//alert(data.options);
         $(".se-pre-con").fadeOut("slow");
          $("select[name='forZone'").html('');
          if(data.status==1)
          {
             $("select[name='forZone'").append('<option value>[Select]</option>');
            $.each(data.options,function (k, v)
            {
                $("select[name='forZone'").append('<option value='+k+'>'+v+'</option>');
            });
           // getMemberDetails();
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
            $(".se-pre-con").fadeOut("slow");
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-warning',
                  content: msg
              });
         },complete: function(){
           getForDistDetails();
         }
   });
 }
 function getForDistDetails(){
     //var forDist = $("#districtcd").val();
     var token = $("input[name='_token']").val();
     $.ajax({
       url: "getForDistDetails",
       method: 'POST',
       data: { _token: token},
       beforeSend: function(){
        // Show image container
        $("#loader").show();
       },
       success: function (data) {//alert(data.options);
        // $(".se-pre-con").fadeOut("slow");
          $("select[name='forDist'").html('');
          if(data.status==1)
          {
             $("select[name='forDist'").append('<option value>[Select]</option>');
            $.each(data.options,function (k, v)
            {
                $("select[name='forDist'").append('<option value='+k+'>'+v+'</option>');
            });
           // getMemberDetails();
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
            $(".se-pre-con").fadeOut("slow");
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-warning',
                  content: msg
              });
         },complete:function(data){
            // Hide image container
            $("#loader").hide();
         }
   });
 }
 function getMemberDetails(){
    var forDist = $("#forDist").val();
    var subdivision = $("#subdivision").val();
    var blockmuni = $("#blockMunicipality").val();
    var officeName = $("#officeName").val();
    var poststatus = $("#poststatus").val();
    var gender = $("#gender").val();
    var ExAs1 = $("#ExAs1").val();
    var ExAs2 = $("#ExAs2").val();
    var ExAs3 = $("#ExAs3").val();
    
    var token = $("input[name='_token']").val();
   $(".se-pre-con").fadeIn("slow");
   $.ajax({
       url: "getMemberDetails",
       method: 'POST',
       data: {forDist:forDist,subdivision: subdivision,blockmuni: blockmuni,officeName: officeName,poststatus: poststatus,gender: gender,ExAs1: ExAs1,ExAs2: ExAs2,ExAs3: ExAs3, _token: token},
       success: function (json) {//alert(data.options);
          $(".se-pre-con").fadeOut("slow");
          $("#memberDetails").html('');
          if(json.status==1)
          {
              //alert(json.options.length);
              var tr;
              tr="<table class='table table-bordered table-striped1' width='100%' style='border-top: 2px solid #009fe8;'>";
              tr+="<tr style='background-color: #f5f8fa'>";
              tr+="<th colspan='"+json.options.length+"'>Total PP Available";
              tr+="</th>";
//              tr+="<tr><td>aasas</td><td>aasas</td><td>aasas</td>";
//              tr+="<td>aasas</td></tr>";
              tr+="</tr>";
              tr+="<tr>";
                for (var i = 0; i < json.options.length; i++) {
                  tr+="<td style='padding:0px;border-collapse: collapse;'>"; 
                  tr+="<table width='100%' style=''>";
                    tr+="<tr><td>" + json.options[i].poststat + "</td></tr>";
                    tr+="<tr><td>" + json.options[i].total + "</td></tr>";
                  tr+="</table>";
                  tr+="</td>";
                }
              tr+="</tr>"; 
              tr+="</table>";
              $('#memberDetails').append(tr);
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
         $(".se-pre-con").fadeOut("slow");
           var msg = "";
           if (jqXHR.status !== 422 && jqXHR.status !== 400) {
               msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
           } else {
               if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                   msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
               } else {
                   msg += "Error(s):<strong><ul>";
                   $.each(jqXHR.responseJSON, function (key, value) {
                       msg += "<li>" + value + "</li>";
                   });
                   msg += "</ul></strong>";
               }
           }
           $.alert({
               title: 'Error!!',
               type: 'red',
               icon: 'fa fa-warning',
               content: msg
           });
        }
   });
    
 }
 function getZoneDetailsReqiured(){
    var forZone = $("#forZone").val();
    
    var token = $("input[name='_token']").val();
    $(".se-pre-con").fadeIn("slow");
    $.ajax({
       url: "getZoneDetailsReqiured",
       method: 'POST',
       data: {forZone: forZone, _token: token},
       success: function (json) {//alert(data.options);
          $(".se-pre-con").fadeOut("slow");
          //$("#memberDetails").html('');
          if(json.status==1)
          {
              //alert(json.options.length);
    
              $('#forZoneDetailsReqiured').html(json.options);
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
         $(".se-pre-con").fadeOut("slow");
           var msg = "";
           if (jqXHR.status !== 422 && jqXHR.status !== 400) {
               msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
           } else {
               if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                   msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
               } else {
                   msg += "Error(s):<strong><ul>";
                   $.each(jqXHR.responseJSON, function (key, value) {
                       msg += "<li>" + value + "</li>";
                   });
                   msg += "</ul></strong>";
               }
           }
           $.alert({
               title: 'Error!!',
               type: 'red',
               icon: 'fa fa-warning',
               content: msg
           });
        }
   });
 }

</script>
@stop