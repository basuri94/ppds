@extends('layouts.master')
@section('content')
<style>
    .jconfirm-box{
        width: 650px;
        margin-left: -200px !important; 
    }

/*    table.dataTable thead>tr>th.sorting_asc,table.dataTable thead>tr>th.sorting_desc,table.dataTable thead>tr>th.sorting,table.dataTable thead>tr>td.sorting_asc,table.dataTable thead>tr>td.sorting_desc,table.dataTable thead>tr>td.sorting{
        padding-right:0px !important;
    }*/

.dropdown-menu{
        /* right: 67%;*/
        /* left: auto; */
        min-width: 0px !important; 
        width:230px !important;
        padding: 1em !important;
    }
</style>
<!--// top-bar -->
<div class="wrapper">
    <!-- Sidebar Holder -->
    @include('layouts.sidebar')
    <!-- Page Content Holder -->
    <div id="content">           
        <section class="tables-section">

            <div class="breadcrumb pagehead1">
                    <span class="headbrand"><i class="fa fa-desktop"></i> List of DCRC Master
                        <span class="scoop-mcaret1"></span>
                    </span>&nbsp;&nbsp;
                    &nbsp;{!! Form::button(' Add DCRC Master&nbsp;<i class="fa fa-plus-circle"></i>',['id'=>'add_dcrc_master','class' => 'btn btn-primary-year add-new-button']) !!}  

<!--                {!! Form::button('<i class="fa fa-desktop"></i> List of DCRC',['class' => 'btn btn-primary-header add-new-button']) !!}


                {!! Form::button('<i class="fa fa-plus-circle"></i>&nbsp;Add DCRC',['id'=>'add_dcrc_master','class' => 'btn btn-primary-header pull-right', 'style'=>'margin-left: 710px;']) !!}
-->

                <div class="clearfix"></div>
            </div>
            </nav>
            <div class="outer-w3-agile">
                <div class="panel-group" id="accordion5">                                                              
                    <div class="panel panel-default">
                        <div class="panel-heading1">
                            <h6 class="panel-title">
                                <a data-toggle="#" data-parent="#accordion5" href="#collapsePV"> <span class="fa fa-minus"></span> <span class="highlight">Zonewise DCRC Master List</span></a>
                            </h6>
                        </div>
                        <div id="collapsePV" class="panel-collapse collapse5">
                            <div class="panel-body">                                   
                                <div class="row">

                                    <div class='col-sm-3'>
                                        {!! Form::label('forZone', 'For Zone:', ['class'=>'highlight ']) !!}
                                        <div class="form-group">
                                            <div class=''>
                                                {!! Form::select('forZone',[''=>'[ALL]'],Null,['id'=>'forZone','class'=>'form-control','autocomplete'=>'off']) !!}
                                            </div>
                                        </div>
                                    </div> 

                                </div>
                            </div>

                        </div>
                        <div class="form-horizontal">
                            <div class="form-group row">
                                {{csrf_field()}}
                                <div class="datatbl table-responsive" style="width: 96%;margin-left: 20px;">
                                    {!! Form::hidden('districtcd', session()->get('districtcd_ppds'),['id'=>'districtcd']) !!}
                                    <table class="table table-striped table-bordered table-hover notice-types-table" id="datatable-table" style="">
                                        <thead>
                                            <tr>
                                                <th>SL#</th>
                                                
                                                <th>DC Venue</th>
                                                <th>DC Address</th>
                                                <th>RC Venue</th>
                                                <th>RC Address</th>
                                                <th>DC Date</th>
                                                <th>DC Time</th>
                                                <th>Action</th>
                                            </tr>

                                        </thead>
                                        <tbody></tbody>
                                        <!-- Table Footer -->

                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </section>
    </div>
</div>
<script>
    $(document).ready(function () {
        $("#add_dcrc_master").click(function () {
            window.location.href = "dcrc_master";
        });
        getZoneDetails();
        $('select[name="forZone"]').on('change', function () {

            create_table();
        });

        var table = $('#datatable-table').DataTable();
        table.on('draw.dt', function () {

            $('.edit-button').click(function () {
                var value_all = this.id;

                var explode_data = value_all.split("/");
                var dcrcgrp = explode_data[5];

                $.ajax({
                    type: 'post',
                    url: 'check_for_edit_delete_dcrc',
                    data: {'data': dcrcgrp, '_token': $('input[name="_token"]').val()},
                    dataType: 'json',
                    success: function (datam) {
                        $(".se-pre-con").fadeOut("slow");

                        if (datam.count > 0) {
                            $.alert({
                                title: 'Error!!',
                                type: 'red',
                                icon: 'fa fa-exclamation-triangle',
                                content: "Record exist in another table.You can't change this record."
                            });
                            return false;
                        }
                        var edit_val = datam.rec;
                        var table_edit = '<form id="edit_first" action="" method="post" class="form-horizontal">';
                        table_edit += '<input type="hidden" value="' + edit_val[0].dcrcgrp + '" id="dcrcgrp_code">';

                        table_edit += '<div class="form-group">';
                        table_edit += '<label class="control-label col-md-6 required">DC Venue:<span style="color:red;font-size: 11px;font-weight: bold;">[Max:60]</span></label>';
                        table_edit += '<div class="col-md-6"><input type="text" class="form-control" value="' + edit_val[0].dc_venue + '" id="dc_ven" name="dc_ven" style="margin-bottom: 2px;"></div>';
                        table_edit += '</div>';
                        table_edit += '<div class="form-group">';
                        table_edit += '<label class="control-label col-md-6 required">DC Address:<span style="color:red;font-size: 11px;font-weight: bold;">[Max:60]</span></label>';
                        table_edit += '<div class="col-md-6"><input type="text" class="form-control" value="' + edit_val[0].dc_address + '" id="dc_add" name="dc_add" style="margin-bottom: 2px;"></div>';
                        table_edit += '</div>';
                        table_edit += '<div class="form-group">';
                        table_edit += '<label class="control-label col-md-6 required">RC Venue:<span style="color:red;font-size: 11px;font-weight: bold;">[Max:60]</span></label>';
                        table_edit += '<div class="col-md-6"><input type="text" class="form-control" value="' + edit_val[0].rc_venue + '" id="rc_ven" name="rc_ven" style="margin-bottom: 2px;"></div>';
                        table_edit += '</div>';
                        table_edit += '<div class="form-group">';
                        table_edit += '<label class="control-label col-md-6 required">RC Address:<span style="color:red;font-size: 11px;font-weight: bold;">[Max:60]</span></label>';
                        table_edit += '<div class="col-md-6"><input type="text" class="form-control" value="' + edit_val[0].rc_address + '" id="rc_add" name="rc_add" style="margin-bottom: 2px;"></div>';
                        table_edit += '</div>';
                        table_edit += '<div class="form-group">';
                        table_edit += '<label class="control-label col-md-6 required">DC Date:</label>';
                        table_edit += '<div class="col-md-6"><input type="text" class="form-control dc_dt" value="' + edit_val[0].dc_date1 + '" id="dc_dt" name="dc_dt" style="margin-bottom: 2px;"></div>';
                        table_edit += '</div>';
                        table_edit += '<div class="form-group">';
                        table_edit += '<label class="control-label col-md-6 required">DC Time:<span style="color:red;font-size: 11px;font-weight: bold;">[Max:15]</span></label>';
                        table_edit += '<div class="col-md-6"><input type="text" class="form-control" value="' + edit_val[0].dc_time + '" id="dc_tm" name="dc_tm" style="margin-bottom: 2px;"></div>';
                        table_edit += '</div>';
                        table_edit += '</div>';
                        table_edit += '</form>';

                        var jc = $.confirm({
                            title: '<span style="font-size: 17px;">Edit DCRC Master</span>',
                            content: table_edit,
                            type: 'green',
                            icon: 'fa fa-edit',
                            typeAnimated: true,
                            buttons: {

                                edit: {
                                    btnClass: 'btn-primary',
                                    action: function () {
                                        var arr = ['dc_ven', 'dc_add', 'rc_ven', 'rc_add', 'dc_dt', 'dc_tm'];
                                        var flag = 0;
                                        jQuery.each(arr, function (index, item) {
                                            var check = $("#" + item + "").val();
                                            if (check == '') {
                                                $.alert({
                                                    title: 'Error!!',
                                                    type: 'red',
                                                    icon: 'fa fa-exclamation-triangle',
                                                    content: "All fields are mandatory"
                                                });
//                                                $.alert('All fields are mandatory');
                                                flag = 1;
                                                return false;
                                            }
                                        });
                                        if (flag > 0) {
                                            return false;
                                        }
                                        jc.showLoading(true);
                                        var dcrcgrp_code = $("#dcrcgrp_code").val();
                                        var dc_ven = $("#dc_ven").val();
                                        var dc_add = $("#dc_add").val();
                                        var rc_ven = $("#rc_ven").val();
                                        var rc_add = $("#rc_add").val();
                                        var dc_dt = $("#dc_dt").val();
                                        var dc_tm = $("#dc_tm").val();
                                        return $.ajax({
                                            url: 'edit_drdc',
                                            dataType: 'json',
                                            data: {'dcrcgrp_code': dcrcgrp_code, 'dc_ven': dc_ven, 'dc_add': dc_add, 'rc_ven': rc_ven, 'rc_add': rc_add, 'dc_tm': dc_tm, 'dc_dt': dc_dt, '_token': $("input[name='_token']").val()},
                                            method: 'post'
                                        }).done(function (response) {
                                            //alert('hi');
                                            jc.hideLoading(true);
                                            if (response.status == 1) {
                                                var msg_suc = "<strong>SUCCESS: Record edited successfuly</strong>";
                                                $.confirm({
                                                    title: 'Success!',
                                                    type: 'green',
                                                    icon: 'fa fa-check',
                                                    content: msg_suc,
                                                    buttons: {
                                                        ok: function () {  
                                                            //table.ajax.reload();
                                                            create_table();
                                                        }

                                                    }
                                                });
                                            }
                                        }).fail(function (jqXHR, textStatus, errorThrown) {
                                            var msg = "";
                                            if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                                                msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                                            } else {
                                                if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                                                    if (jqXHR.responseJSON.exception_code == 23000) {
                                                        msg += "Record exist in another table.You can't change this record.";
                                                    } else {
                                                        msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                                                    }
                                                } else {
                                                    msg += "Error(s):<strong><ul>";
                                                    $.each(jqXHR.responseJSON, function (key, value) {
                                                        msg += "<li>" + value + "</li>";
                                                    });
                                                    msg += "</ul></strong>";
                                                }
                                            }
                                            $.alert({
                                                title: 'Error!!',
                                                type: 'red',
                                                icon: 'fa fa-warning',
                                                content: msg,
                                            });
                                        });
                                    }

                                },
                                close: function () {
                                }
                            },
                            onContentReady: function () {
// bind to events
                                $("#dc_dt").datepicker({
                                    format: "dd/mm/yyyy",
                                    autoclose: true,
                                    todayHighlight: true,
                                    startDate: new Date(),
                                    //endDate: new Date()
                                });

                            },
                            onOpen: function () {

                            }
                        });
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        $(".se-pre-con").fadeOut("slow");
                        var msg = "";
                        if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                            msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                        } else {
                            if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                                if (jqXHR.responseJSON.exception_code == 23000) {
                                    msg += "Record exist in another table.You can't change this record.";
                                } else {
                                    msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                                }
                            } else {
                                msg += "Error(s):<strong><ul>";
                                $.each(jqXHR.responseJSON, function (key, value) {
                                    msg += "<li>" + value + "</li>";
                                });
                                msg += "</ul></strong>";
                            }
                        }
                        $.alert({
                            title: 'Error!!',
                            type: 'red',
                            icon: 'fa fa-warning',
                            content: msg,
                        });
                    }
                });

            });
            $('.delete-button').click(function () {

                var value_all = this.id;
                var explode_data = value_all.split("/");
                var dcrcgrp = explode_data[5];
                $.ajax({
                    type: 'post',
                    url: 'check_for_edit_delete_dcrc',
                    data: {'data': dcrcgrp, '_token': $('input[name="_token"]').val()},
                    dataType: 'json',
                    success: function (datam) {
                        $(".se-pre-con").fadeOut("slow");
                        //alert(datam.count);
                        if (datam.count > 0) {
                            $.alert({
                                title: 'Error!!',
                                type: 'red',
                                icon: 'fa fa-exclamation-triangle',
                                content: "Record exist in dcrc_party table.You can't delete this record."
                            });
                            return false;
                        }
                        $.confirm({
                            title: 'Attention!',
                            content: 'Do you want to delete this record?',
                            type: 'orange',
                            icon: 'fa fa-exclamation-triangle',
                            buttons: {
                                confirm: function () {

                                    $.ajax({
                                        type: 'post',
                                        url: 'delete_dcrc',
                                        data: {'data': dcrcgrp, '_token': $('input[name="_token"]').val()},
                                        dataType: 'json',
                                        success: function (datam) {

                                            var msg = "<strong>SUCCESS: Record deleted successfully</strong>";
                                            $.confirm({
                                                title: 'Success!',
                                                type: 'green',
                                                icon: 'fa fa-check',
                                                content: msg,
                                                buttons: {
                                                    ok: function () {  
                                                        //table.ajax.reload();
                                                        create_table();
                                                    }
                                                }
                                            });
                                           // table.ajax.reload();
                                        },
                                        error: function (jqXHR, textStatus, errorThrown) {
                                            $(".se-pre-con").fadeOut("slow");
                                            var msg = "";
                                            if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                                                msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                                            } else {
                                                if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                                                    if (jqXHR.responseJSON.exception_code == 23000) {
                                                        msg += "Record exist in another table.You can't delete this record.";
                                                    } else {
                                                        msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                                                    }
                                                } else {
                                                    msg += "Error(s):<strong><ul>";
                                                    $.each(jqXHR.responseJSON, function (key, value) {
                                                        msg += "<li>" + value + "</li>";
                                                    });
                                                    msg += "</ul></strong>";
                                                }
                                            }
                                            $.alert({
                                                title: 'Error!!',
                                                type: 'red',
                                                icon: 'fa fa-warning',
                                                content: msg,
                                            });
                                        }
                                    });
                                },
                                cancel: function () {

                                }
                            }
                        });
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        $(".se-pre-con").fadeOut("slow");
                        var msg = "";
                        if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                            msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                        } else {
                            if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                                if (jqXHR.responseJSON.exception_code == 23000) {
                                    msg += "Record exist in another table.You can't delete this record.";
                                } else {
                                    msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                                }
                            } else {
                                msg += "Error(s):<strong><ul>";
                                $.each(jqXHR.responseJSON, function (key, value) {
                                    msg += "<li>" + value + "</li>";
                                });
                                msg += "</ul></strong>";
                            }
                        }
                        $.alert({
                            title: 'Error!!',
                            type: 'red',
                            icon: 'fa fa-warning',
                            content: msg,
                        });
                    }
                });


            });


        });


    });
    function getZoneDetails() {
        var forDist = $("#districtcd").val();
        var token = $("input[name='_token']").val();
        $(".se-pre-con").fadeIn("slow");
        $.ajax({
            url: "getZoneDetails",
            method: 'POST',
            data: {forDist: forDist, _token: token},
            success: function (data) {
                $(".se-pre-con").fadeOut("slow");

                $("select[name='forZone'").html('');
                if (data.status == 1)
                {
                    $("select[name='forZone'").append('<option value="">[ALL]</option>');
                    $.each(data.options, function (k, v)
                    {
                        $("select[name='forZone'").append('<option value=' + k + '>' + v + '</option>');
                    });

                }

            },
            error: function (jqXHR, textStatus, errorThrown) {
                $(".se-pre-con").fadeOut("slow");
                var msg = "";
                if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                    msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                } else {
                    if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                        msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                    } else {
                        msg += "Error(s):<strong><ul>";
                        $.each(jqXHR.responseJSON, function (key, value) {
                            msg += "<li>" + value + "</li>";
                        });
                        msg += "</ul></strong>";
                    }
                }
                $.alert({
                    title: 'Error!!',
                    type: 'red',
                    icon: 'fa fa-exclamation-triangle',
                    content: msg
                });
            },
            complete: function() {
                 create_table();
            }
        });
    }

    function create_table() {
        var table = "";
        var token = $('input[name="_token"]').val();


        $("#datatable-table").dataTable().fnDestroy();
        table = $('#datatable-table').DataTable({
            "processing": true,
            "serverSide": true,
            "dom": 'lBfrtip',
            "buttons": [
                //'copy', 'csv', 'excel', 'pdf'
                {
                    extend: 'collection',
                    text: 'Export',
                    buttons: [
                        {
                            extend: 'copyHtml5',
                            exportOptions: {
                                columns: ':visible'
                            }
                        },
                        {
                            extend: 'excelHtml5',
                            exportOptions: {
                                columns: ':visible'
                            }
                        },
                        {
                            extend: 'pdfHtml5',
                            exportOptions: {
                                columns: ':visible'
                            }
                        },
                        {
                            extend: 'print',
                            exportOptions: {
                                columns: ':visible'
                            }
                        },
                        {
                            extend: 'csvHtml5',
                            exportOptions: {
                                columns: ':visible'
                            }
                        }

                    ]

                },
                'colvis'
            ],
            "ajax": {
                url: "dcrc_master_list_datatable",
                type: "post",
                data: {'_token': $('input[name="_token"]').val(), 'zone': $("#forZone").val(), 'assembly': $("#assembly").val()},
                dataSrc: "dcrc_masters",
                error: function (jqXHR, textStatus, errorThrown) {
                    var msg = "";
                    if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                        msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                    } else {
                        if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                            if (jqXHR.responseJSON.exception_code == 23000) {
                                msg += "Some Sql Exception Occured";
                            } else {
                                msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                            }
                        } else {
                            msg += "Error(s):<strong><ul>";
                            $.each(jqXHR.responseJSON, function (key, value) {
                                msg += "<li>" + value + "</li>";
                            });
                            msg += "</ul></strong>";
                        }
                    }
                    $.alert({
                        title: 'Error!!',
                        type: 'red',
                        icon: 'fa fa-warning',
                        content: msg,
                    });
                }
            },
            "dataType": 'json',
            "columnDefs":
                    [
                        {className: "table-text", "targets": "_all"},
                        {
                            "targets": 0,
                            "data": "code",
                            "defaultContent": "",
                            "searchable": false,
                            "sortable": false
                        },
                        
                        {
                            "targets": 1,
                            "data": "dc_venue"
                        },
                        {
                            "targets": 2,
                            "data": "dc_address"
                        },
                        {
                            "targets": 3,
                            "data": "rc_venue"
                        },
                        {
                            "targets": 4,
                            "data": "rc_address"
                        },
                        {
                            "targets": 5,
                            "data": "dc_date"
                        },
                        {
                            "targets": 6,
                            "data": "dc_time"
                        },
                        {
                            "targets": -1,
                            "data": 'action',
                            "searchable": false,
                            "sortable": false,
                            "render": function (data, type, full, meta) {
                                var str_btns = "";

                                str_btns += '<button type="button"  class="btn btn-success  edit-button btn_new1" id="' + data.e + '" title="Edit"><i class="fa fa-edit"></i></button>&nbsp&nbsp';



                                str_btns += '<button type="button"  class="btn btn-danger  delete-button btn_new1" id="' + data.d + '" title="Delete"><i class="fa fa-trash"></i></button>';

                                return str_btns;
                            }
                        }
                    ],

            "order": [[1, 'asc']]
        });
        table.on('order.dt search.dt draw.dt', function () {
            $('[data-toggle="tooltip"]').tooltip();
            table.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
                cell.innerHTML = table.page() * table.page.len() + (i + 1);
            });
        });
    }
</script>
<!-- Copyright -->
@stop