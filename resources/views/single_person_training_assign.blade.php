
@extends('layouts.master')
@section('content')

 <!--// top-bar -->
 <div class="wrapper">
<!-- Sidebar Holder -->
@include('layouts.sidebar')

<!-- Page Content Holder -->
<div id="content">           
<section class="tables-section">
    <nav aria-label="breadcrumb" style="width:100%;">
        <div class="breadcrumb pagehead1">
           
                {!! Form::button('<i class="fa fa-desktop"></i> Single PP Training',['class' => 'btn btn-primary-header add-new-button']) !!}
            
            <div class="col-md-offset-8 pull-right">
<!--                {!! Form::button('Academic Year:&nbsp;',['id'=>'show_academic','class' => 'btn btn-primary-year']) !!}-->
                
            </div>
                <div class="clearfix"></div>
        </div>
    </nav>

    <!-- form -->
<div class="outer-w3-agile">
  {!! Form::open(['url' => 'singlePPTr', 'name' => 'singlePPTr', 'class' =>'request-info clearfix form-horizontal', 'id' => 'singlePPTr', 'method' => 'post','role'=>'','files' => true]) !!}
   {!! Form::hidden('districtcd', session()->get('districtcd_ppds'),['id'=>'districtcd']) !!}
  <!-- OLD PWise -->
  {{-- <div class="panel-group" id="accordion5" style="width: 49%;float: left;">                                                               --}}
    <div class="panel-group" id="accordion5" style="">                                                              
    <div class="panel panel-default">
     <div class="panel-heading1">
     <h6 class="panel-title">
         <a data-toggle="#" data-parent="#accordion5" href="#collapsePV"> <span class="fa fa-minus"></span><span class="highlight"> OLD PERSONNEL </span></a>
     </h6>
     </div>
     <div id="collapsePV" class="panel-collapse collapse5">
      <div class="panel-body" style="min-height:75px;">                                   
        <div class="row"> 
         
            <div class='col-sm-6'>
                {!! Form::label('personnel_id', 'Personnel ID:', ['class'=>'highlight required']) !!}
                <div class="form-group">
                    <div class=''>
                        {!! Form::text('personnel_id',null,['id'=>'personnel_id','class'=>'form-control','autocomplete'=>'off','maxlength'=>'11','minlength'=>'11']) !!}
                    </div>
                </div>
            </div> 

        </div>

        <div class="row"> 
            <div class='col-sm-12'> 
               <div id="old_personnel_details" >                            	
                   
               </div>
            </div>
         </div> 

         



                                                                  
    <div class="panel panel-default" id="rw" style="display: none;">
     <div class="panel-heading1">
     <h6 class="panel-title">
         <a data-toggle="#" data-parent="#accordion5" href="#collapse"> <span class="fa fa-minus"></span><span class="highlight"> Training</span></a>
     </h6>
     </div>
     <div id="collapse" class="panel-collapse collapse5">
      <div class="panel-body">                                   
       
    <div id="collapsePV" class="panel-collapse collapse5">
      <div class="panel-body" style="min-height:75px;">                                   
        
          <div class="row" id="rw1" style=""> 
            <div class='col-sm-4'>
              {!! Form::label('trainingtype', 'Training Head:', ['class'=>'highlight required']) !!}
              <div class="form-group">
                  <div class=''>
                       {!! Form::select('trainingtype',$training_type['desc'],null,['id'=>'trainingtype','class'=>'form-control','placeholder'=>'[Select]','autocomplete'=>'off']) !!}
                  </div>
              </div>
          </div>

           {{-- <div class='col-sm-4'>
            {!! Form::label('trainingdatetime', 'Training Date & Time:', ['class'=>'highlight required']) !!}
              <div class="form-group">
                  <div class=''>
                     {!! Form::select('trainingdatetime',[''=>'[Select]'],null,['id'=>'trainingdatetime','class'=>'form-control','placeholder'=>'[Select]','autocomplete'=>'off']) !!}
                  </div>
             </div>
        </div> --}}


        <div class='col-sm-4'>
            {!! Form::label('subdivision', 'Sub Division:', ['class'=>'highlight required']) !!}
                <!--<span class="highlight">Sub Division</span> -->
            <div class="form-group">
                <div class=''>
                    {!! Form::select('subdivision',[''=>'[Select]'],null,['id'=>'subdivision','class'=>'form-control','autocomplete'=>'off']) !!}
                </div>
            </div>
        </div>

        
             <div class='col-sm-4'>
                {!! Form::label('v_sub', '1st Training Venue Subvenue:', ['class'=>'highlight required']) !!}
                    <!--<span class="highlight">Sub Division</span> -->
                <div class="form-group">
                    <div class=''>
                        {!! Form::select('v_sub',[''=>'[Select]'],null,['id'=>'v_sub','class'=>'form-control','autocomplete'=>'off']) !!}
                    </div>
                </div>
            </div>
              <div class='col-sm-4'>
                    {!! Form::label('v_sub2', '2nd Training Venue Subvenue:', ['class'=>'highlight']) !!}
                        <!--<span class="highlight">Sub Division</span> -->
                    <div class="form-group">
                        <div class=''>
                            {!! Form::select('v_sub2',[''=>'[Select]'],null,['id'=>'v_sub2','class'=>'form-control','autocomplete'=>'off']) !!}
                        </div>
                    </div>
                </div>
                <div class='col-sm-4'>
                    {!! Form::label('orderno', 'Order Date & No:', ['class'=>'highlight required']) !!}
                    <div class="form-group">
                        <div class=''>
                            {!! Form::select('orderno',[''=>'[Select]'],Null,['id'=>'orderno','class'=>'form-control','autocomplete'=>'off']) !!}
                        </div>
                    </div>
                </div>
             
          
         </div> 
       </div>
      </div>
         <div class="row" id="rw1" style=""> 
          <div class='col-sm-12'>                    
                <div class="form-group text-right permit">                            	
                   {{ Form::button('Submit', ['class' => 'btn btn-info', 'type' => 'button','id'=>'submit']) }}
                   {{-- {{ Form::button('Replace', ['class' => 'btn btn-info', 'type' => 'button','id'=>'replace','disabled'=>'true']) }} --}}
                    {{-- {{ Form::button('Print', ['class' => 'btn btn-info', 'type' => 'button','id'=>'print','disabled'=>'true']) }} --}}
                   {{ Form::button('Reset', ['class' => 'btn btn-success' ,'type' => 'reset','id'=>'reset']) }}
                </div>
           </div>
         </div> 
             
       </div>
      </div>
    </div>                              
  </div> 







                       
       </div>
      </div>
    </div>                              
  </div> 

 {!! Form::close() !!}             
</div>



</section>

</div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        // change_subdivision();
        $('#personnel_id').on('change', function () {
            var personnel_id = $("#personnel_id").val();
            if(personnel_id!=''){
                get_personnel_details();
                change_subdivision();
                getOrderDetails();
                // getTrainingDateTime();
            }

        });


        $("#subdivision").change(function(){
            get_venue_and_subvenue();
        });

        $("#submit").click(function(){
        
        // var chkSameVenueTraining = $("#chkSameVenueTraining").is(":checked");
       
        var personnel_id = $("#personnel_id").val();
            if(personnel_id == ""){
                $.alert({
                   title: 'Error!!',
                   type: 'red',
                   icon: 'fa fa-exclamation-triangle',
                   content: 'Personnel ID is required'
               });
               return false;
           }
           var trainingtype = $("#trainingtype").val();
         if(trainingtype == ""){
                $.alert({
                   title: 'Error!!',
                   type: 'red',
                   icon: 'fa fa-exclamation-triangle',
                   content: 'Training Type is required'
               });
               return false;
           }


        //    var trainingdatetime = $("#trainingdatetime").val();
        //  if(trainingdatetime == ""){
        //         $.alert({
        //            title: 'Error!!',
        //            type: 'red',
        //            icon: 'fa fa-exclamation-triangle',
        //            content: 'Training Date Time is required'
        //        });
        //        return false;
        //    }

           var subdivision = $("#subdivision").val();
            if(subdivision == ""){
                    $.alert({
                    title: 'Error!!',
                    type: 'red',
                    icon: 'fa fa-exclamation-triangle',
                    content: 'Subdivision is required'
                });
                return false;
            }

            var v_sub = $("#v_sub").val();
            if(v_sub == ""){
                    $.alert({
                    title: 'Error!!',
                    type: 'red',
                    icon: 'fa fa-exclamation-triangle',
                    content: 'Sub Venue is required'
                });
                return false;
            }

            var v_sub2 = $("#v_sub2").val();
            // if(v_sub2 == ""){
            //         $.alert({
            //         title: 'Error!!',
            //         type: 'red',
            //         icon: 'fa fa-exclamation-triangle',
            //         content: 'Sub Venue 2 is required'
            //     });
            //     return false;
            // }

            var orderno = $("#orderno").val();
            if(orderno == ""){
                    $.alert({
                    title: 'Error!!',
                    type: 'red',
                    icon: 'fa fa-exclamation-triangle',
                    content: 'Order No is required'
                });
                return false;
            }
            
            
        var formData = new FormData($("#singlePPTr")[0]);
        // $(".se-pre-con").fadeIn("slow");
        //var formData = {'zone': forzone,'forassembly': forassembly,'ofc_id': ofc_id,'booked': booked, 'gender': gender,'post_stat': post_stat, '_token': token};
        $.ajax({
            type: "post",
            url: "singlePPTrDtAllocation",
            data: formData,
            processData: false,
            contentType: false,
            dataType: 'json',
            success: function (data) {
                if(data.status==1)
                    {
                      $(".se-pre-con").fadeOut("slow");            
                      var msg="Record(s) saved successfully";
                       $.confirm({
                            title: 'Success!',
                            type: 'green',
                            icon: 'fa fa-check',
                            content: msg,
                            buttons: {
                                ok: function () {
                                   // location.reload(true);
                                 //  getZoneWiseAssembly();
                                //  alert(districtcd);
                                // if(data.person_first_rand_tbl==''){
                                //     window.location.assign("single_person_training_assign")
                                   
                                  
                                // }else{
                                    print_appt();
                                    window.location.assign("single_person_training_assign")

                                      
                                // }

                                }

                            }
                        });
                      }
            
            
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $(".se-pre-con").fadeOut("slow");
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-exclamation-triangle',
                  content: msg
              });
         }
     });
  });








    });



    function print_appt() {
    var districtcd = $("#districtcd").val();
    //  var subdivision = "";
    //  var blockMunicipality = "";
    //  var officeName = "";
     var PersonnelId = $("#personnel_id").val();
    //  var trainingtype = "";
     var from = "1";
     var to = "1";
    //  var datas = {'districtcd': districtcd,'subdivision': subdivision,'blockMunicipality': blockMunicipality,'officeName': officeName,'PersonnelId': PersonnelId,'trainingtype': trainingtype,from: from,to: to, '_token': $('input[name="_token"]').val()};
    var datas = {'districtcd': districtcd,'PersonnelId': PersonnelId,from: from,to: to, '_token': $('input[name="_token"]').val()};
     
     redirectPost_newTab('{{url("getFirstAppointmentLetterPDF2019")}}', datas);
}


    function get_personnel_details() {
        var personnel_id = $("#personnel_id").val();
        if(personnel_id == ""){
           $.alert({
              title: 'Error!!',
              type: 'red',
              icon: 'fa fa-exclamation-triangle',
              content: 'Personnel Id is required'
          });
          return false;
      }
      var token = $("input[name='_token']").val();
      $(".se-pre-con").fadeIn("slow");
      $.ajax({
       url: "getSinglePersonnelDetails",
       method: 'POST',
       data: {personnel_id: personnel_id, _token: token},
       success: function (json) {//alert(data.options);
          $(".se-pre-con").fadeOut("slow");
          //$("#memberDetails").html('');
          if(json.status==1)
          {
              $('#old_personnel_details').html(json.options);
               $('#rw').show();
               scrollToElement($('#rw'));
          }
          else if(json.status==0)
          {
              $('#old_personnel_details').html(json.options);
              $('#rw').hide();
          }
          document.getElementById('replace').disabled=true;
          document.getElementById('print').disabled=true;
       },
        error: function (jqXHR, textStatus, errorThrown) {
         $(".se-pre-con").fadeOut("slow");
           var msg = "";
           if (jqXHR.status !== 422 && jqXHR.status !== 400) {
               msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
           } else {
               if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                   msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
               } else {
                   msg += "Error(s):<strong><ul>";
                   $.each(jqXHR.responseJSON, function (key, value) {
                       msg += "<li>" + value + "</li>";
                   });
                   msg += "</ul></strong>";
               }
           }
           $.alert({
               title: 'Error!!',
               type: 'red',
               icon: 'fa fa-warning',
               content: msg
           });
        }
   });
    }



    function change_subdivision(){
  var forDist = $("#districtcd").val();
  
   var token = $("input[name='_token']").val();
   $(".se-pre-con").fadeIn("slow");
   $.ajax({
       url: "getDistrictWiseSubdivision",
       method: 'POST',
       data: {forDist: forDist, _token: token},
       success: function (data) {//alert(data.options);
          $(".se-pre-con").fadeOut("slow");
          $("select[name='subdivision'").html('');
          
          if(data.status==1)
          {
            $("select[name='subdivision'").append('<option value>[Select]</option>');
            $.each(data.options,function (k, v)
            {
                $("select[name='subdivision'").append('<option value='+k+'>'+v+'</option>');
            });
            //$("select[name='subdivision'").html(data.options);
             // change_officeName();
           //  getMemberDetails();
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
         $(".se-pre-con").fadeOut("slow");
           var msg = "";
           if (jqXHR.status !== 422 && jqXHR.status !== 400) {
               msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
           } else {
               if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                   msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
               } else {
                   msg += "Error(s):<strong><ul>";
                   $.each(jqXHR.responseJSON, function (key, value) {
                       msg += "<li>" + value + "</li>";
                   });
                   msg += "</ul></strong>";
               }
           }
           $.alert({
               title: 'Error!!',
               type: 'red',
               icon: 'fa fa-warning',
               content: msg
           });
         }
   });
}




function get_venue_and_subvenue(){
    var subdivision = $("#subdivision").val();
    // alert(subdivision);
    var hid_post_stat=$("#hid_post_stat").html();
    // alert(hid_post_stat);
    var trainingtype=$("#trainingtype").val();
    // alert(trainingtype);
// if(subdivision == ""){
//            $.alert({
//               title: 'Error!!',
//               type: 'red',
//               icon: 'fa fa-exclamation-triangle',
//               content: 'Subdivision is required'
//           });
//           return false;
//       }
//       if(trainingtype == ""){
//            $.alert({
//               title: 'Error!!',
//               type: 'red',
//               icon: 'fa fa-exclamation-triangle',
//               content: 'Training Type is required'
//           });
//           return false;
//       }
    var token = $("input[name='_token']").val();
    // $(".se-pre-con").fadeIn("slow");
    $.ajax({
       url: "get_venue_with_not_fill_for_replace",
       method: 'POST',
       data: {subdivision:subdivision,trainingtype:trainingtype,post_stat:hid_post_stat, _token: token},
       success: function (data) {//alert(data.options);
        //  $(".se-pre-con").fadeOut("slow");
          $("select[name='v_sub'").html('');
          $("select[name='v_sub2'").html('');
          if(data.status==1)
          {
             $("select[name='v_sub'").append('<option value>[Select]</option>');
             $("select[name='v_sub2'").append('<option value>[Select]</option>');
            // $.each(data.options,function (k, v)
            // { 
            //     if(v.training_type=='01' || v.training_type=='03' || v.training_type=='05'){
            //      $("select[name='v_sub'").append('<option value='+v.schedule_code+'>'+v.venuename+'('+v.subvenue+'['+v.training_dt+' '+v.training_time+'])</option>');   
            //     }else{
            //      $("select[name='v_sub2'").append('<option value='+v.schedule_code+'>'+v.venuename+'('+v.subvenue+'['+v.training_dt+' '+v.training_time+'])</option>');   
            //     }
                
            // });
           // getMemberDetails();
           if(data.poststat[0].post_stat=='PR'){
                $.each(data.options,function (k, v)
                { 
                    if(v.training_type=='01' || v.training_type=='03' || v.training_type=='05'){
                    $("select[name='v_sub'").append('<option value='+v.schedule_code+'>'+v.venuename+'('+v.subvenue+'['+v.training_dt+' '+v.training_time+'])</option>');   
                    }else{
                    $("select[name='v_sub2'").append('<option value='+v.schedule_code+'>'+v.venuename+'('+v.subvenue+'['+v.training_dt+' '+v.training_time+'])</option>');   
                    }
                    
                });
            } else if(data.poststat[0].post_stat=='P1'){
                $.each(data.options,function (k, v)
                { 
                    if(v.training_type=='01' || v.training_type=='03' || v.training_type=='05'){
                    $("select[name='v_sub'").append('<option value='+v.schedule_code+'>'+v.venuename+'('+v.subvenue+'['+v.training_dt+' '+v.training_time+'])</option>');   
                    }
                    
                });
            } else if(data.poststat[0].post_stat=='P2'){
                $.each(data.options,function (k, v)
                { 
                    if(v.training_type=='01' || v.training_type=='03' || v.training_type=='05'){
                    $("select[name='v_sub'").append('<option value='+v.schedule_code+'>'+v.venuename+'('+v.subvenue+'['+v.training_dt+' '+v.training_time+'])</option>');   
                    }
                    
                });
            } else if(data.poststat[0].post_stat=='P3'){
                $.each(data.options,function (k, v)
                { 
                    if(v.training_type=='01' || v.training_type=='03' || v.training_type=='05'){
                    $("select[name='v_sub'").append('<option value='+v.schedule_code+'>'+v.venuename+'('+v.subvenue+'['+v.training_dt+' '+v.training_time+'])</option>');   
                    }
                    
                });
            }





          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
            $(".se-pre-con").fadeOut("slow");
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-warning',
                  content: msg
              });
         }
   });
}

function getOrderDetails(){
    var forDist = $("#districtcd").val();
    var token = $("input[name='_token']").val();
    //$(".se-pre-con").fadeIn("slow");
    $.ajax({
       url: "getOrderDetails",
       method: 'POST',
       data: {forDist: forDist, _token: token},
       success: function (data) {//alert(data.options);
        // $(".se-pre-con").fadeOut("slow");
          $("select[name='orderno'").html('');
          if(data.status==1)
          {
              $("select[name='orderno'").html(data.options);
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
            $(".se-pre-con").fadeOut("slow");
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-warning',
                  content: msg
              });
         }
   });
 }




//  function getTrainingDateTime(){
//      var forDist = $("#districtcd").val();
//      var token = $("input[name='_token']").val();
//      //$(".se-pre-con").fadeIn("slow");
//      $.ajax({
//        url: "getTrainingDateTime",
//        method: 'POST',
//        data: {forDist: forDist, _token: token},
//        success: function (data) {//alert(data.options);
//        //  $(".se-pre-con").fadeOut("slow");
//           $("select[name='trainingdatetime'").html('');
//           if(data.status==1)
//           {
//               $("select[name='trainingdatetime'").html(data.options);
//           }
//        },
//         error: function (jqXHR, textStatus, errorThrown) {
//          //   $(".se-pre-con").fadeOut("slow");
//               var msg = "";
//               if (jqXHR.status !== 422 && jqXHR.status !== 400) {
//                   msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
//               } else {
//                   if (jqXHR.responseJSON.hasOwnProperty('exception')) {
//                       msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
//                   } else {
//                       msg += "Error(s):<strong><ul>";
//                       $.each(jqXHR.responseJSON, function (key, value) {
//                           msg += "<li>" + value + "</li>";
//                       });
//                       msg += "</ul></strong>";
//                   }
//               }
//               $.alert({
//                   title: 'Error!!',
//                   type: 'red',
//                   icon: 'fa fa-warning',
//                   content: msg
//               });
//          }
//    });
//  }
</script>
@stop
