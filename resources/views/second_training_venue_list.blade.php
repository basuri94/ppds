@extends('layouts.master')
@section('content')
<style>
    .jconfirm-box{
        width: 600px;
        margin-left: -200px !important; 
    }
</style>
<!--// top-bar -->
<div class="wrapper">
    <!-- Sidebar Holder -->
    @include('layouts.sidebar')
    <!-- Page Content Holder -->
    <div id="content">           
        <section class="tables-section">

            <div class="breadcrumb pagehead1">
               <span class="headbrand"><i class="fa fa-desktop"></i> List of Group Training Venue
                    <span class="scoop-mcaret1"></span>
                </span>&nbsp;&nbsp;
                &nbsp;{!! Form::button(' Add Group Training Venue&nbsp;<i class="fa fa-plus-circle"></i>',['id'=>'add_second_training_venue','class' => 'btn btn-primary-year add-new-button']) !!}  
          
                <div class="clearfix"></div>
            </div>
            </nav>
            <div class="outer-w3-agile">
                <div class="panel-group" id="accordion5">                                                              
                    <div class="panel panel-default">
                        <div class="panel-heading1">
                            <h6 class="panel-title">
                                <a data-toggle="#" data-parent="#accordion5" href="#collapsePV"> <span class="fa fa-minus"></span> <span class="highlight">List of Group Training Venue</span></a>
                            </h6>
                        </div>
                        <div id="collapsePV" class="panel-collapse collapse5">
                            <div class="panel-body">                                   
                                <div class="row">

                                    <div class='col-sm-4'>
                                        {!! Form::label('subdivision', 'Sub Division:', ['class'=>'highlight ']) !!}
                                        <div class="form-group">
                                            <div class=''>
                                                {!! Form::select('subdivision',[''=>'[ALL]'],Null,['id'=>'subdivision','class'=>'form-control','autocomplete'=>'off']) !!}
                                            </div>
                                        </div>
                                    </div> 
                                    <div class='col-sm-4'>
                                        {!! Form::label('venuename', 'Venue:', ['class'=>'highlight ']) !!}
                                        <div class="form-group">
                                            <div class=''>
                                                {!! Form::select('venuename',[''=>'[ALL]'],Null,['id'=>'venuename','class'=>'form-control','autocomplete'=>'off']) !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="form-horizontal">
                            <div class="form-group row">
                                {{csrf_field()}}
                                <div class="datatbl table-responsive" style="width: 96%;margin-left: 20px;">
                                    {!! Form::hidden('districtcd', session()->get('districtcd_ppds'),['id'=>'districtcd']) !!}
                                    <table class="table table-striped table-bordered table-hover notice-types-table" id="datatable-table" style="">
                                        <thead>
                                            <tr>
                                                <th>SL#</th>
                                                <th>Subdivision</th>
                                                <th>Venue Name</th>
                                                <th>Venue Address</th>
                                                <th>Sub-venue Name</th>
                                                <th>Capacity</th>
                                                <th>Action</th>
                                            </tr>

                                        </thead>
                                        <tbody></tbody>
                                        <!-- Table Footer -->

                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </section>
    </div>
</div>
<script>
    $(document).ready(function () {
        $("#add_second_training_venue").click(function () {
            window.location.href = "second_training_venue";
        });
        change_subdivision();
        $('select[name="subdivision"]').on('change', function () {
             var subdivision = $('#subdivision').val();
            if (subdivision == '') {
                create_table();
            } else {
                change_venueName();
            }
        });
        $('select[name="venuename"]').on('change', function () {

            create_table();
        });
        $('#add-new-button').click(function () {
            //$(".se-pre-con").fadeIn("slow"); 
        });
        
        var table = "";
        table = $('#datatable-table').DataTable();
       
        table.on('draw.dt', function () {
//table.fnSetColumnVis( 1, false, false ); 
            $('.edit-button').click(function () {
                var value_all = this.id;
                $.ajax({
                    type: 'post',
                    url: 'check_for_edit_delete_subvenue_second',
                    data: {'data': value_all, '_token': $('input[name="_token"]').val()},
                    dataType: 'json',
                    success: function (datam) {
                        $(".se-pre-con").fadeOut("slow");
                        //alert(datam.count);
                        if (datam.count > 0) {
                            $.alert({
                                title: 'Error!!',
                                type: 'red',
                                icon: 'fa fa-exclamation-triangle',
                                content: "Record exist in another table.You can't change this record."
                            });
                            return false;
                        }
                        var edit_val = datam.rec;
                        var table_edit = '<form id="edit_first" action="" method="post" class="form-horizontal">';
                        table_edit += '<input type="hidden" value="' + edit_val[0].subvenue_cd + '" id="sub_v_code">';
                        table_edit += '<input type="hidden" value="' + edit_val[0].venue_cd + '" id="v_code">';
                        table_edit += '<div class="form-group">';
                        table_edit += '<label class="control-label col-md-6 required">Subdivision Name:</label>';
                        table_edit += '<div class="col-md-6"><input type="text" class="form-control" value="' + edit_val[0].subdivision + '" id="subdiv" name="venue" style="margin-bottom: 2px;" readonly></div>';
                        table_edit += '</div>';
                        table_edit += '<div class="form-group">';
                        table_edit += '<label class="control-label col-md-6 required">Venue Name: <span style="color:red;font-size: 11px;font-weight: bold;">[Max:100]</span></label>';
                        table_edit += '<div class="col-md-6"><input type="text" class="form-control" value="' + edit_val[0].venuename + '" id="venue" name="venue" style="margin-bottom: 2px;"></div>';
                        table_edit += '</div>';
                        table_edit += '<div class="form-group">';
                        table_edit += '<label class="control-label col-md-6 required">Venue Address: <span style="color:red;font-size: 11px;font-weight: bold;">[Max:100]</span></label>';
                        table_edit += '<div class="col-md-6"><input type="text" class="form-control" value="' + edit_val[0].venueaddress + '" id="venue_add" name="venue_add" style="margin-bottom: 2px;"></div>';
                        table_edit += '</div>';
                        table_edit += '<div class="form-group">';
                        table_edit += '<label class="control-label col-md-6 required">Sub-venue Name: <span style="color:red;font-size: 11px;font-weight: bold;">[Max:30]</span></label>';
                        table_edit += '<div class="col-md-6"><input type="text" class="form-control" value="' + edit_val[0].subvenue + '" id="subvenue" name="subvenue" style="margin-bottom: 2px;"></div>';
                        table_edit += '</div>';
                        table_edit += '<div class="form-group">';
                        table_edit += '<label class="control-label col-md-6 required">Max Capacity: <span style="color:red;font-size: 11px;font-weight: bold;">[Max:4]</span></label>';
                        table_edit += '<div class="col-md-6"><input type="text" class="form-control" value="' + edit_val[0].maxcapacity + '" id="mxcapacity" name="mxcapacity" style="margin-bottom: 2px;"></div>';
                        table_edit += '</div>';
                        table_edit += '</div>';
                        table_edit += '</form>';

                        var jc = $.confirm({
                            title: '<span style="font-size: 17px;">Second Training Venue :</span>',
                            content: table_edit,
                            type: 'green',
                            icon: 'fa fa-edit',
                            typeAnimated: true,
                            buttons: {

                                edit: {
                                    btnClass: 'btn-primary',
                                    action: function () {
                                        var arr = ['venue', 'venue_add', 'subvenue', 'mxcapacity'];
                                        var flag = 0;
                                        jQuery.each(arr, function (index, item) {
                                            var check = $("#" + item + "").val();
                                            if (check == '') {
                                                $.alert('All fields are mandatory');
                                                flag = 1;
                                                return false;
                                            }
                                        });
                                        if (flag > 0) {
                                            return false;
                                        }
                                        jc.showLoading(true);

                                        return $.ajax({
                                            url: 'edit_second_train_venue',
                                            dataType: 'json',
                                            data: {'v_code': $("#v_code").val(), 'sub_v_code': $("#sub_v_code").val(), 'venue': $("#venue").val(), 'venue_add': $("#venue_add").val(), 'subvenue': $("#subvenue").val(), 'mxcapacity': $("#mxcapacity").val(), '_token': $("input[name='_token']").val()},
                                            method: 'post'
                                        }).done(function (response) {
                                            //alert('hi');
                                            jc.hideLoading(true);
                                            if (response.status == 1) {
                                               var msg_suc = "<strong>Record edited successfuly</strong>";
                                                 $.confirm({
                                                    title: 'Success!',
                                                    type: 'green',
                                                    icon: 'fa fa-check',
                                                    content: msg_suc,
                                                    buttons: {
                                                        ok: function () {  
                                                            //table.ajax.reload();
                                                            create_table();
                                                        }

                                                    }
                                                });
                                            }
                                        }).fail(function (jqXHR, textStatus, errorThrown) {
                                            var msg = "";
                                            if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                                                msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                                            } else {
                                                if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                                                    if (jqXHR.responseJSON.exception_code == 23000) {
                                                        msg += "Record exist in another table.You can't change this record.";
                                                    } else {
                                                        msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                                                    }
                                                } else {
                                                    msg += "Error(s):<strong><ul>";
                                                    $.each(jqXHR.responseJSON, function (key, value) {
                                                        msg += "<li>" + value + "</li>";
                                                    });
                                                    msg += "</ul></strong>";
                                                }
                                            }
                                            $.alert({
                                                title: 'Error!!',
                                                type: 'red',
                                                icon: 'fa fa-warning',
                                                content: msg,
                                            });
                                        });

                                    }
                                },
                                close: function () {
                                }
                            },
                            onOpen: function () {
                                // after the modal is displayed.
                                //startTimer();
                            }
                        });
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        $(".se-pre-con").fadeOut("slow");
                        var msg = "";
                        if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                            msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                        } else {
                            if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                                if (jqXHR.responseJSON.exception_code == 23000) {
                                    msg += "Record exist in another table.You can't change this record.";
                                } else {
                                    msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                                }
                            } else {
                                msg += "Error(s):<strong><ul>";
                                $.each(jqXHR.responseJSON, function (key, value) {
                                    msg += "<li>" + value + "</li>";
                                });
                                msg += "</ul></strong>";
                            }
                        }
                        $.alert({
                            title: 'Error!!',
                            type: 'red',
                            icon: 'fa fa-warning',
                            content: msg,
                        });
                    }
                });

            });
            $('.delete-button').click(function () {
//                var reply = $.confirm('Are you sure to delete the record?');
//                if (!reply) {
//                    return false;
//   
//                             }
                var value_all = this.id;
                var res = value_all.split("/");
                $.ajax({
                    type: 'post',
                    url: 'check_for_edit_delete_subvenue_second',
                    data: {'data': res[0],'v_code': res[1], '_token': $('input[name="_token"]').val()},
                    dataType: 'json',
                    success: function (datam) {
                        $(".se-pre-con").fadeOut("slow");
                        //alert(datam.count);
                        if (datam.count > 0) {
                            $.alert({
                                title: 'Error!!',
                                type: 'red',
                                icon: 'fa fa-exclamation-triangle',
                                content: "Record exist in another table.You can't delete this record."
                            });
                            return false;
                        }
                        $.confirm({
                            title: 'Attention!',
                            content: 'Do you want to delete this record?',
                            type: 'orange',
                            icon: 'fa fa-exclamation-triangle',
                            buttons: {
                                confirm: function () {
                                    var data_new = value_all;
                                    //alert(data_new);
                                    $.ajax({
                                        type: 'post',
                                        url: 'delete_second_training_venue',
                                        data: {'data': res[0],'v_code': res[1],'_token': $('input[name="_token"]').val()},
                                        dataType: 'json',
                                        success: function (datam) {
                                            var msg = "<strong>Record deleted successfully</strong>";
                                            $.confirm({
                                                title: 'Success!',
                                                type: 'green',
                                                icon: 'fa fa-check',
                                                content: msg,
                                                buttons: {
                                                    ok: function () {  
                                                        //table.ajax.reload();
                                                        create_table();
                                                    }
                                                }
                                            });
                                        },
                                        error: function (jqXHR, textStatus, errorThrown) {
                                            $(".se-pre-con").fadeOut("slow");
                                            var msg = "";
                                            if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                                                msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                                            } else {
                                                if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                                                    if (jqXHR.responseJSON.exception_code == 23000) {
                                                        msg += "Record exist in another table.You can't delete this record.";
                                                    } else {
                                                        msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                                                    }
                                                } else {
                                                    msg += "Error(s):<strong><ul>";
                                                    $.each(jqXHR.responseJSON, function (key, value) {
                                                        msg += "<li>" + value + "</li>";
                                                    });
                                                    msg += "</ul></strong>";
                                                }
                                            }
                                            $.alert({
                                                title: 'Error!!',
                                                type: 'red',
                                                icon: 'fa fa-warning',
                                                content: msg,
                                            });
                                        }
                                    });
                                },
                                cancel: function () {

                                }
                            }
                        });
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        $(".se-pre-con").fadeOut("slow");
                        var msg = "";
                        if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                            msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                        } else {
                            if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                                if (jqXHR.responseJSON.exception_code == 23000) {
                                    msg += "Record exist in another table.You can't delete this record.";
                                } else {
                                    msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                                }
                            } else {
                                msg += "Error(s):<strong><ul>";
                                $.each(jqXHR.responseJSON, function (key, value) {
                                    msg += "<li>" + value + "</li>";
                                });
                                msg += "</ul></strong>";
                            }
                        }
                        $.alert({
                            title: 'Error!!',
                            type: 'red',
                            icon: 'fa fa-warning',
                            content: msg,
                        });
                    }
                });


            });

        });
    });
    function change_subdivision() {
            var forDist = $("#districtcd").val();
            var token = $("input[name='_token']").val();
            // $(".se-pre-con").fadeIn("slow");
            $.ajax({
                url: "getSubdivisionData",
                method: 'POST',
                data: {forDist: forDist, _token: token},
                success: function (data) {//alert(data.options);
                    //   $(".se-pre-con").fadeOut("slow");
                    $("select[name='subdivision'").html('');

                    if (data.status == 1)
                    {
                        $("select[name='subdivision'").append('<option value>[Select]</option>');
                        $.each(data.options, function (k, v)
                        {
                            $("select[name='subdivision'").append('<option value=' + k + '>' + v + '</option>');
                        });
                        //$("select[name='subdivision'").html(data.options);
                        // change_officeName();
                        // getMemberDetails();
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $(".se-pre-con").fadeOut("slow");
                    var msg = "";
                    if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                        msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                    } else {
                        if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                            msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                        } else {
                            msg += "Error(s):<strong><ul>";
                            $.each(jqXHR.responseJSON, function (key, value) {
                                msg += "<li>" + value + "</li>";
                            });
                            msg += "</ul></strong>";
                        }
                    }
                    $.alert({
                        title: 'Error!!',
                        type: 'red',
                        icon: 'fa fa-exclamation-triangle',
                        content: msg
                    });
                },
                complete: function () {
                    create_table();
                }
            });
        }
        function change_venueName() {
            var subdivision = $("#subdivision").val();
            var token = $("input[name='_token']").val();
            //$(".se-pre-con").fadeIn("slow");
            $.ajax({
                url: "getVenueName_second",
                method: 'POST',
                data: {subdivision: subdivision, _token: token},
                success: function (data) {//alert(data.options);
                    $(".se-pre-con").fadeOut("slow");
                    $("select[name='venuename'").html('');

                    if (data.status == 1)
                    {
                        $("select[name='venuename'").append('<option value>[Select]</option>');
                        $.each(data.options, function (k, v)
                        {
                            $("select[name='venuename'").append('<option value=' + k + '>' + v + '</option>');
                        });
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $(".se-pre-con").fadeOut("slow");
                    var msg = "";
                    if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                        msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                    } else {
                        if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                            msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                        } else {
                            msg += "Error(s):<strong><ul>";
                            $.each(jqXHR.responseJSON, function (key, value) {
                                msg += "<li>" + value + "</li>";
                            });
                            msg += "</ul></strong>";
                        }
                    }
                    $.alert({
                        title: 'Error!!',
                        type: 'red',
                        icon: 'fa fa-exclamation-triangle',
                        content: msg
                    });
                },
                complete: function () {
                    create_table();
                }
            });
        }
        function create_table() {
            var table = "";
            var token = $('input[name="_token"]').val();


            $("#datatable-table").dataTable().fnDestroy();
            table = $('#datatable-table').DataTable({
                "processing": true,
                "serverSide": true,
                "dom": 'lBfrtip',
                "buttons": [
                //'copy', 'csv', 'excel', 'pdf'
                {
                    extend: 'collection',
                    text: 'Export',
                    buttons: [
                        {
                            extend: 'copyHtml5',
                            exportOptions: {
                                columns: ':visible'
                            }
                        },
                        {
                            extend: 'excelHtml5',
                            exportOptions: {
                                columns: ':visible'
                            }
                        },
                        {
                            extend: 'pdfHtml5',
                            exportOptions: {
                                columns: ':visible'
                            }
                        },
                        {
                            extend: 'print',
                            exportOptions: {
                                columns: ':visible'
                            }
                        },
                        {
                            extend: 'csvHtml5',
                            exportOptions: {
                                columns: ':visible'
                            }
                        }

                    ]

                },
                'colvis'
            ],
                "ajax": {
                    url: "second_training_venue_list_datatable",
                    type: "post",
                    data: {'_token': $('input[name="_token"]').val(), 'subdivision': $("#subdivision").val(), 'venuename': $("#venuename").val()},
                    dataSrc: "fisrt_training_venue",
                    error: function (jqXHR, textStatus, errorThrown) {
                        var msg = "";
                        if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                            msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                        } else {
                            if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                                if (jqXHR.responseJSON.exception_code == 23000) {
                                    msg += "Some Sql Exception Occured";
                                } else {
                                    msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                                }
                            } else {
                                msg += "Error(s):<strong><ul>";
                                $.each(jqXHR.responseJSON, function (key, value) {
                                    msg += "<li>" + value + "</li>";
                                });
                                msg += "</ul></strong>";
                            }
                        }
                        $.alert({
                            title: 'Error!!',
                            type: 'red',
                            icon: 'fa fa-warning',
                            content: msg,
                        });
                    }
                },
                "dataType": 'json',
                "columnDefs":
                        [
                            {className: "table-text", "targets": "_all"},
                            {
                                "targets": 0,
                                "data": "code",
                                "searchable": false,
                                "sortable": false
                            },
                            {
                                "targets": 1,
                                "data": "subdivision",
                            },
                            {
                                "targets": 2,
                                "data": "venuename",
                            },
                            {
                                "targets": 3,
                                "data": "venueaddress",
                            },
                            {
                                "targets": 4,
                                "data": "subvenue",
                            },
                            {
                                "targets": 5,
                                "data": "maxcapacity",
                            },

                            {
                                "targets": -1,
                                "data": 'action',
                                "searchable": false,
                                "sortable": false,
                                "render": function (data, type, full, meta) {
                                    var str_btns = "";

                                    str_btns += '<button type="button"  class="btn btn-success  edit-button btn_new1" id="' + data.e + '" title="Edit"><i class="fa fa-edit"></i></button>&nbsp&nbsp';



                                    str_btns += '<button type="button"  class="btn btn-danger  delete-button btn_new1" id="' + data.d + '" title="Delete"><i class="fa fa-trash"></i></button>';

                                    return str_btns;
                                }
                            }
                        ],

                "order": [[1, 'asc']]
            });
         table.on('order.dt search.dt draw.dt', function () {
            $('[data-toggle="tooltip"]').tooltip();
            table.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
                cell.innerHTML = table.page() * table.page.len() + (i + 1);
            });
        });
            // table.columns( [-4,-3,-2,-1] ).visible( false );
        }
</script>
<!-- Copyright -->
@stop