@extends('layouts.master')
@section('content')

    <!--// top-bar -->
    <div class="wrapper">
        <!-- Sidebar Holder -->
        @include('layouts.sidebar')

        <!-- Page Content Holder -->
        <div id="content">
            <section class="tables-section">
                <nav aria-label="breadcrumb" style="width:100%;">
                    <div class="breadcrumb pagehead1">

                        {!! Form::button('<i class="fa fa-desktop"></i> Reverse Swapping', [
                        'class' => 'btn
                        btn-primary-header add-new-button',
                        ]) !!}

                        <div class="col-md-offset-8 pull-right">
                            <!--                {!!  Form::button('Academic Year:&nbsp;', ['id' => 'show_academic', 'class' => 'btn btn-primary-year']) !!}-->

                        </div>
                        <div class="clearfix"></div>
                    </div>
                </nav>

                <!-- form -->
                <div class="outer-w3-agile">
                    {!! Form::open(['url' => 'reverse_swapping_personnel', 'name' => 'reverse_swapping_personnel', 'class'
                    => 'request-info clearfix form-horizontal', 'id' => 'reverse_swapping_personnel', 'method' => 'post',
                    'role' => '', 'files' => true]) !!}
                    {!! Form::hidden('districtcd', session()->get('districtcd_ppds'), ['id' => 'districtcd']) !!}
                    <div class="panel-group" id="accordion5">
                        <div class="panel panel-default">
                            <div class="panel-heading1">
                                <h6 class="panel-title">
                                    <a data-toggle="#" data-parent="#accordion5" href="#collapsePV"> <span
                                            class="fa fa-minus"></span> <span class="highlight">Zonewise Records </span></a>
                                </h6>
                            </div>
                            <div id="collapsePV" class="panel-collapse collapse5">
                                <div class="panel-body">
                                    <div class="row">


                                        <div class='col-sm-3'>
                                            {!! Form::label('forZone', 'For Zone:', ['class' => 'highlight required']) !!}
                                            <div class="form-group">
                                                <div class=''>
                                                    {!! Form::select('forZone', ['' => '[Select]'], null, ['id' =>
                                                    'forZone', 'class' => 'form-control', 'autocomplete' => 'off']) !!}
                                                </div>
                                            </div>
                                        </div>
                                        <div class='col-sm-3'>
                                            {!! Form::label('phase', 'Phase:', ['class' => 'highlight ']) !!}
                                            <div class="form-group">
                                                <div class=''>
                                                    @php
                                                    $phase=\App\Phase::get();
                                                    @endphp
                                                    <select id="phase" class="form-control" name="phase">

                                                        <option value="">[Select]</option>
                                                        @foreach ($phase as $phase)
                                                            <option value="{{ $phase->code }}">{{ $phase->name }}</option>
                                                        @endforeach

                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class='col-sm-3'>
                                            {!! Form::label('ExAs1', 'Excluding Assembly:', ['class' => 'highlight']) !!}
                                            <div class="form-group">
                                                <div class=''>
                                                    {!! Form::text('ExAs1', null, ['id' => 'ExAs1', 'class' =>
                                                    'form-control', 'autocomplete' => 'off', 'maxlength' => '3']) !!}
                                                </div>
                                            </div>
                                        </div>
                                        <div class='col-sm-3'>
                                            {!! Form::label('poststatus', 'Post Status:', ['class' => 'highlight']) !!}
                                            <div class="form-group">
                                                <div class=''>
                                                    {!! Form::select('poststatus', $poststat['poststatus'], null, ['id' =>
                                                    'poststatus', 'class' => 'form-control', 'placeholder' => '[Select]',
                                                    'autocomplete' => 'off']) !!}
                                                </div>
                                            </div>
                                        </div>
                                        <div class='col-sm-3'>
                                            {!! Form::label('Employee', 'Number of employee:', ['class' => 'highlight']) !!}
                                            <span style="color:red;font-size: 11px;">[Max:4]</span>
                                            <div class="form-group">
                                                <div class=''>
                                                    {!! Form::text('Employee', null, ['id' => 'Employee', 'class' =>
                                                    'form-control', 'autocomplete' => 'off', 'maxlength' => '4']) !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class='col-sm-12'>
                                            <div id='forZoneDetailsReqiured'></div>
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class='col-sm-12'>
                                            <div class="form-group text-right permit">
                                                {{ Form::button('Reverse ALL', ['class' => 'btn btn-info', 'type' => 'button', 'id' => 'reverse']) }}
                                                {{ Form::button('Reverse', ['class' => 'btn btn-info', 'type' => 'submit', 'id' => 'swapping']) }}
                                                {{ Form::button('Reset', ['class' => 'btn btn-success', 'type' => 'reset', 'id' => 'reset']) }}
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
                <!--// form -->


            </section>

        </div>
    </div>
    <!-- Copyright -->
    <script type="text/javascript">
        $(document).ready(function() {
            $("#reset").click(function() {
                location.reload(true);
            });
            char_num("#ExAs1");
            char_num("#Employee");
            // $('select[name="subdivision"]').on('change', function () {
            //    change_blockMuni();
            //     
            // });
            //  $('select[name="blockMunicipality"]').on('change', function () {
            //    change_officeName();
            // });
            //
            // $('select[name="gender"]').on('change', function () {
            //    getMemberDetails();
            // });
            $('#ExAs1').on('change', function() {
                // getZoneDetailsReqiured();
            });
            getZoneDetails();

            $('select[name="forZone"]').on('change', function() {
                getZoneDetailsReqiured();
            });
            $('select[name="phase"]').on('change', function() {
                getZoneDetailsReqiured();
            });
            $("#reverse").click(function() {

                $("#reserve").attr('disabled', true);
                var formData = new FormData($("#reverse_swapping_personnel")[0]);
                var table_edit =
                    '<form id="firstrand_lock_unlock" action="firstrand_lock_unlock" method="post" class="form-horizontal">';
                table_edit += '<div class="form-group">';
                // table_edit += '<label class="control-label col-md-4 required" style="font-size:12px;">Enter Password:</label>';
                table_edit +=
                    '<div class="col-md-11"><input type="password" class="form-control" id="password" name="password" style="margin-bottom: 2px;" /></div>';
                table_edit += '</div>';
                table_edit += '</form>';
                var jc = $.confirm({
                    title: '<span style="font-size: 17px;">Enter Reverse Password</span>',
                    content: table_edit,
                    type: 'green',
                    icon: 'fa fa-edit',
                    typeAnimated: true,
                    buttons: {

                        proceed: {
                            btnClass: 'btn-primary',
                            action: function() {
                                var flag = 0;
                                var check = $("#password").val();
                                if (check == '') {
                                    $.alert({
                                        title: 'Error!!',
                                        type: 'red',
                                        icon: 'fa fa-exclamation-triangle',
                                        content: "Please enter password"
                                    });
                                    flag = 1;
                                    return false;
                                }
                                if (flag > 0) {
                                    return false;
                                }
                                jc.showLoading(true);
                                var password = $("#password").val();

                                $.ajax({
                                    type: 'post',
                                    url: 'check_for_all_reverse_password',
                                    data: {
                                        'password': password,
                                        '_token': $('input[name="_token"]').val()
                                    },
                                    dataType: 'json',
                                    success: function(datam) {
                                        if (datam.options > 0) {
                                            $(".se-pre-con1").show();
                                            return $.ajax({
                                                type: "post",
                                                url: "all_reverse_swapping_personnel",
                                                data: formData,
                                                processData: false,
                                                contentType: false

                                            }).done(function(response) {
                                                if (response.status == 1) {
                                                    jc.hideLoading(true);
                                                    $(".se-pre-con1").fadeOut("slow");
                                                    var msg = response
                                                        .options +
                                                        " Record(s) reversed successfully for this district";
                                                    $.confirm({
                                                        title: 'Success!',
                                                        type: 'green',
                                                        icon: 'fa fa-check',
                                                        content: msg,
                                                        buttons: {
                                                            ok: function() {
                                                                location
                                                                    .reload(
                                                                        true
                                                                    );

                                                            }

                                                        }
                                                    });
                                                }
                                            }).fail(function(jqXHR, textStatus,
                                                errorThrown) {
                                                    $(".se-pre-con1").fadeOut("slow");
                                                var msg = "";
                                                if (jqXHR.status !== 422 &&
                                                    jqXHR.status !== 400) {
                                                    msg += "<strong>" +
                                                        jqXHR.status +
                                                        ": " + errorThrown +
                                                        "</strong>";
                                                } else {
                                                    if (jqXHR.responseJSON
                                                        .hasOwnProperty(
                                                            'exception')) {
                                                        if (jqXHR
                                                            .responseJSON
                                                            .exception_code ==
                                                            23000) {
                                                            msg +=
                                                                "Check Password.";
                                                        } else {
                                                            msg +=
                                                                "Exception: <strong>" +
                                                                jqXHR
                                                                .responseJSON
                                                                .exception_message +
                                                                "</strong>";
                                                        }
                                                    } else {
                                                        msg +=
                                                            "Error(s):<strong><ul>";
                                                        $.each(jqXHR
                                                            .responseJSON,
                                                            function(
                                                                key,
                                                                value) {
                                                                msg +=
                                                                    "<li>" +
                                                                    value +
                                                                    "</li>";
                                                            });
                                                        msg +=
                                                            "</ul></strong>";
                                                    }
                                                }
                                                $.alert({
                                                    title: 'Error!!',
                                                    type: 'red',
                                                    icon: 'fa fa-warning',
                                                    content: msg,
                                                });
                                            });
                                        } else {
                                            $("#reserve").attr('disabled', false);
                                            $.alert({
                                                title: 'Error!!',
                                                type: 'red',
                                                icon: 'fa fa-exclamation-triangle',
                                                content: "Please enter correct password"
                                            });

                                        }
                                    },
                                    error: function(jqXHR, textStatus, errorThrown) {
                                        var msg = "";
                                        $(".se-pre-con1").fadeOut("slow");
                                        if (jqXHR.status !== 422 && jqXHR.status !==
                                            400) {
                                            msg += "<strong>" + jqXHR.status +
                                                ": " + errorThrown + "</strong>";
                                        } else {
                                            if (jqXHR.responseJSON.hasOwnProperty(
                                                    'exception')) {
                                                msg += "Exception: <strong>" + jqXHR
                                                    .responseJSON
                                                    .exception_message +
                                                    "</strong>";
                                            } else {
                                                msg += "Error(s):<strong><ul>";
                                                $.each(jqXHR.responseJSON, function(
                                                    key, value) {
                                                    msg += "<li>" + value +
                                                        "</li>";
                                                });
                                                msg += "</ul></strong>";
                                            }
                                        }
                                        $.alert({
                                            title: 'Error!!',
                                            type: 'red',
                                            icon: 'fa fa-warning',
                                            content: msg
                                        });
                                        $("#reserve").attr('disabled', false);
                                    }
                                });



                            }
                        },
                        close: function() {
                            //                   $("#submit").attr('disabled',false);
                        }
                    },
                    onContentReady: function() {
                        // bind to events

                    },

                    onOpen: function() {
                        // after the modal is displayed.
                        //startTimer();
                    }
                });

            });
            $("#reverse_swapping_personnel").bootstrapValidator({
                message: 'This value is not valid',
                feedbackIcons: {
                    valid: 'fa fa-check',
                    invalid: 'fa fa-times',
                    validating: 'fa fa-refresh',
                },

                fields: {


                    //                districtcd: {
                    //                    validators: {
                    //                        notEmpty: {
                    //                            message: 'District code is required'
                    //                        }
                    //                    }
                    //                },
                    forZone: {
                        validators: {
                            notEmpty: {
                                message: 'For Zone is required'
                            }
                        }
                    },
                    ExAs1: {
                        validators: {
                            stringLength: {
                                min: 3,
                                max: 3,
                                message: 'Assembly should be 3 digit'
                            }
                        }
                    },

                    Employee: {
                        validators: {
                            stringLength: {
                                min: 1,
                                message: 'Number of employee should be minimum 1 digit'
                            }
                        }
                    }

                }
            }).on('success.form.bv', function(e) {
                // Prevent form submission
                e.preventDefault();
                $(".se-pre-con1").fadeIn("slow");
                var poststatus = $("#poststatus").val();
                var ExAs1 = $("#ExAs1").val();
                var forZone = $("#forZone").val();
                var Employee = $("#Employee").val();
                var forDist = $("#districtcd").val();
                var phase=$('#phase').val();
                var token = $("input[name='_token']").val();

                var fd1 = new FormData();
                fd1.append('poststatus', poststatus);
                fd1.append('ExAs1', ExAs1);
                fd1.append('forZone', forZone);
                fd1.append('Employee', Employee);
                fd1.append('forDist', forDist);
                fd1.append('phase', phase);
                fd1.append('_token', token);

                $("#swapping").attr('disabled', true);
                $.ajax({
                    type: "post",
                    url: "reverse_swapping_personnel",
                    data: fd1,
                    processData: false,
                    contentType: false,
                    dataType: 'json',
                    success: function(data) {
                        if (data.status == 1) {
                            $(".se-pre-con1").fadeOut("slow");
                            var msg = data.options + " Record(s) reversed successfully";
                            $.confirm({
                                title: 'Success!',
                                type: 'green',
                                icon: 'fa fa-check',
                                content: msg,
                                buttons: {
                                    ok: function() {
                                        $("#swapping").attr('disabled', true);
                                        /*$("#swapping_personnel")[0].reset();
                                        $("#swapping").attr('disabled',true);
                                        $('.form-control-feedback').css('display', 'none');
                                        $('.has-success').removeClass('has-success');*/
                                        location.reload(true);
                                    }

                                }
                            });
                        }

                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        $(".se-pre-con").fadeOut("slow");
                        var msg = "";
                        if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                            msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                        } else {
                            if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                                msg += "Exception: <strong>" + jqXHR.responseJSON
                                    .exception_message + "</strong>";
                            } else {
                                msg += "Error(s):<strong><ul>";
                                $.each(jqXHR.responseJSON, function(key, value) {
                                    msg += "<li>" + value + "</li>";
                                });
                                msg += "</ul></strong>";
                            }
                        }
                        $.alert({
                            title: 'Error!!',
                            type: 'red',
                            icon: 'fa fa-warning',
                            content: msg
                        });
                    }
                });
            });
        });

        function getZoneDetails() {
            var forDist = $("#districtcd").val();
            var token = $("input[name='_token']").val();
            $(".se-pre-con").fadeIn("slow");
            $.ajax({
                url: "getZoneDetails",
                method: 'POST',
                data: {
                    forDist: forDist,
                    _token: token
                },
                success: function(data) { //alert(data.options);
                    $(".se-pre-con").fadeOut("slow");
                    $("select[name='forZone'").html('');
                    if (data.status == 1) {
                        $("select[name='forZone'").append('<option value>[Select]</option>');
                        $.each(data.options, function(k, v) {
                            $("select[name='forZone'").append('<option value=' + k + '>' + v +
                                '</option>');
                        });
                        // getMemberDetails();
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $(".se-pre-con").fadeOut("slow");
                    var msg = "";
                    if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                        msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                    } else {
                        if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                            msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                        } else {
                            msg += "Error(s):<strong><ul>";
                            $.each(jqXHR.responseJSON, function(key, value) {
                                msg += "<li>" + value + "</li>";
                            });
                            msg += "</ul></strong>";
                        }
                    }
                    $.alert({
                        title: 'Error!!',
                        type: 'red',
                        icon: 'fa fa-warning',
                        content: msg
                    });
                }
            });
        }

        function getZoneDetailsReqiured() {
            var forZone = $("#forZone").val();
            //var ExAs1 = $("#ExAs1").val();
            var phase=$('#phase').val();
            var token = $("input[name='_token']").val();
            $(".se-pre-con").fadeIn("slow");
            $.ajax({
                url: "getZoneDetailsReverseReqiured",
                method: 'POST',
                data: {
                    forZone: forZone,
                    phase: phase,
                    _token: token
                },
                success: function(json) { //alert(data.options);
                    $(".se-pre-con").fadeOut("slow");
                    //$("#memberDetails").html('');
                    if (json.status == 1) {
                        $('#forZoneDetailsReqiured').html(json.options);
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $(".se-pre-con").fadeOut("slow");
                    var msg = "";
                    if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                        msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                    } else {
                        if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                            msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                        } else {
                            msg += "Error(s):<strong><ul>";
                            $.each(jqXHR.responseJSON, function(key, value) {
                                msg += "<li>" + value + "</li>";
                            });
                            msg += "</ul></strong>";
                        }
                    }
                    $.alert({
                        title: 'Error!!',
                        type: 'red',
                        icon: 'fa fa-warning',
                        content: msg
                    });
                }
            });
        }

    </script>
@stop
