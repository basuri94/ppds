@extends('layouts.master')
@section('content')

 <div class="wrapper">
<!-- Sidebar Holder -->
@include('layouts.sidebar')

<!-- Page Content Holder -->
<div id="content">           
<section class="tables-section">
    <nav aria-label="breadcrumb" style="width:100%;">
        <div class="breadcrumb pagehead1">
           
                {!! Form::button('<i class="fa fa-desktop"></i> Assemblywise Zone Creation',['class' => 'btn btn-primary-header add-new-button']) !!}
            
            <div class="col-md-offset-8 pull-right">
<!--                {!! Form::button('Academic Year:&nbsp;',['id'=>'show_academic','class' => 'btn btn-primary-year']) !!}-->
                
            </div>
                <div class="clearfix"></div>
        </div>
    </nav>

    <!-- form -->
<div class="outer-w3-agile">
  {!! Form::open(['url' => 'assembly_zone', 'name' => 'assembly_zone', 'class' =>'request-info clearfix form-horizontal', 'id' => 'assembly_zone', 'method' => 'post','role'=>'','files' => true]) !!}
  {!! Form::hidden('districtcd', session()->get('districtcd_ppds'),['id'=>'districtcd']) !!}
  
  
   <div class="panel-group" id="accordion5">                                                              
    <div class="panel panel-default">
     <div class="panel-heading1">
     <h6 class="panel-title">
         <a data-toggle="#" data-parent="#accordion5" href="#collapseUV"> <span class="fa fa-minus"></span> <span class="highlight">Assemblywise Zone Creation</span></a>
     </h6>
     </div>
     <div id="collapseUV" class="panel-collapse collapse5">
      <div class="panel-body">                                   
                   
        <div class="row"> 
            <div class='col-sm-12'>
                <div id='assemblyDetails'></div>
            </div>
        </div> 
        <div class="row" id="rw" style="display: none;">                                   
           <div class='col-sm-4'>
               {!! Form::label('zone', 'Zone:', ['class'=>'highlight required']) !!}
              <div class="form-group">
                  <div class=''>
                      {!! Form::select('zone',[''=>'[Select]'],Null,['id'=>'zone','class'=>'form-control','autocomplete'=>'off']) !!}
                  </div>
              </div>
          </div>
          <div class='col-sm-4'>
            {!! Form::label('phase', 'Phase:', ['class'=>'highlight required']) !!}
           <div class="form-group">
               <div class=''>
                   @php
                      $phase=\App\Phase::get();
                   @endphp
    <select id="phase" class="form-control" name="phase">
                  
                   <option value="">[Select]</option>
                   @foreach( $phase as $phase)
                   <option value="{{$phase->code}}">{{$phase->name}}</option>
              @endforeach

    </select>
                </div>
           </div>
       </div>
           <div class='col-sm-4' > 
               {!! Form::label('', '', ['class'=>'highlight']) !!}
                <div class="form-group text-right permit" >                            	
                   {{ Form::button('Create', ['class' => 'btn btn-info', 'type' => 'button','id'=>'create']) }}
                   {{ Form::button('Delete', ['class' => 'btn btn-info', 'type' => 'button','id'=>'delete']) }}
                   {{ Form::button('Update', ['class' => 'btn btn-info', 'type' => 'button','id'=>'update']) }}
                </div>
           </div>
                
        </div>  
      
          
       </div>
      </div>
    </div>                              
  </div> 
 
 {!! Form::close() !!}             
</div>
<!--// form -->


</section>

</div>
</div>
<!-- Copyright -->
<script type="text/javascript">
$(document).ready(function () {
 
 getAssemblyDetails();
  $("#create").click(function(){
        var formData = new FormData($("#assembly_zone")[0]);
        var zone=$('#zone').val();
        var phase=$('#phase').val();
       
        if(zone=="" || phase==""){
            $.confirm({
                     title: 'Error!',
                     type: 'red',
                     icon: 'fa fa-warning',
                     content: 'Please Select Zone And Phase',
                     buttons: {
                         ok: function () {
                            
                            
                         }

                     }
                 });
                 return false;
        }
        $(".se-pre-con").fadeIn("slow");
        $.ajax({
        type: "post",
        url: "createAssemblyZone",
        data: formData,
        processData: false,
        contentType: false,
        dataType: 'json',
        success: function (data) {
           if(data.status==1)
           {
                $(".se-pre-con").fadeOut("slow"); 
                var msg=data.options+" Assembly(s) created successfully for this zone";
               
                $.confirm({
                     title: 'Success!',
                     type: 'green',
                     icon: 'fa fa-check',
                     content: msg,
                     buttons: {
                         ok: function () {
                             //location.reload(true);
                             getAssemblyDetails();
                             $("#rw").hide();
                         }

                     }
                 });
           }
       },
        error: function (jqXHR, textStatus, errorThrown) {
            $(".se-pre-con").fadeOut("slow");
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-exclamation-triangle',
                  content: msg
              });
         }
     });
  });
  $("#delete").click(function(){
        var formData = new FormData($("#assembly_zone")[0]);
        $(".se-pre-con").fadeIn("slow");
        $.ajax({
        type: "post",
        url: "deleteAssemblyZone",
        data: formData,
        processData: false,
        contentType: false,
        dataType: 'json',
        success: function (data) {
           if(data.status==1)
           {
                $(".se-pre-con").fadeOut("slow"); 
                var msg=data.options+" Assembly(s) deleted successfully for this zone</br>";
                if(data.options1>0){
                   msg+=data.options1+" Assembly(s) exist in personnela table";
                }
                $.confirm({
                     title: 'Success!',
                     type: 'green',
                     icon: 'fa fa-check',
                     content: msg,
                     buttons: {
                         ok: function () {
                             //location.reload(true);
                             getAssemblyDetails();
                             $("#rw").hide();
                         }

                     }
                 });
           }
       },
        error: function (jqXHR, textStatus, errorThrown) {
            $(".se-pre-con").fadeOut("slow");
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-exclamation-triangle',
                  content: msg
              });
         }
     });
  });

  $("#update").click(function(){
    var zone=$('#zone').val();
        var phase=$('#phase').val();
       
        if(zone=="" || phase==""){
            $.confirm({
                     title: 'Error!',
                     type: 'red',
                     icon: 'fa fa-warning',
                     content: 'Please Select Zone And Phase',
                     buttons: {
                         ok: function () {
                            
                            
                         }

                     }
                 });
                 return false;
        }
        var formData = new FormData($("#assembly_zone")[0]);
        $(".se-pre-con").fadeIn("slow");
        $.ajax({
        type: "post",
        url: "updateAssemblyZone",
        data: formData,
        processData: false,
        contentType: false,
        dataType: 'json',
        success: function (data) {
           if(data.status==1)
           {
                $(".se-pre-con").fadeOut("slow"); 
                var msg=data.options+" Assembly(s) updated successfully for this zone</br>";
                if(data.options1>0){
                   msg+=data.options1+" Assembly(s) exist in personnela table";
                }
                $.confirm({
                     title: 'Success!',
                     type: 'green',
                     icon: 'fa fa-check',
                     content: msg,
                     buttons: {
                         ok: function () {
                             //location.reload(true);
                             getAssemblyDetails();
                             $("#rw").hide();
                         }

                     }
                 });
           }
       },
        error: function (jqXHR, textStatus, errorThrown) {
            $(".se-pre-con").fadeOut("slow");
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-exclamation-triangle',
                  content: msg
              });
         }
     });
  });
  
});
function getAssemblyDetails(){
    var forDist = $("#districtcd").val();
    var token = $("input[name='_token']").val();
    $(".se-pre-con").fadeIn("slow");
    $.ajax({
        type: 'POST',
        url: 'getAssemblyZone',
        data: {forDist:forDist, _token:token},
        dataType: 'json',
        success: function (response) {
        $(".se-pre-con").fadeOut("slow");
        $("#assemblyDetails").html("");
        var str='<input type="hidden" id="row_count" name="row_count" value="'+response.options.length+'"> '
        str+= "<table class='table table-bordered table-striped' width='100%' style='border-top: 2px solid #4cae4c;'>";
        str += ' <tr style="background-color: #f5f8fa"><th align="left">&nbsp;<input type="checkbox" id="myCheckall" name="myCheckall"  value="1"  class="myCheckall">&nbsp;</th><th align="left">&nbsp;Assembly Code</th><th>&nbsp;Assembly Name </th><th>&nbsp;Subdivision </th><th>&nbsp;Zone </th><th>&nbsp;Phase </th></tr>';
//         str += '<tr style=" background-color: #1ABB9C; color: #FFFFFF; font-weight: bold;"><td align="left"><input type="checkbox" id="myCheckall" name="myCheckall"  value="1"  class="submenu">&nbsp; Check All</td><td align="center" width="30%"><input type="checkbox" id="view_all" class="flat"></td></tr>';
         var sub_length = response.options.length;
        // alert(sub_length);
         for (var i = 0; i < sub_length; i++)
         {
               var c = parseInt(i) + 1;
                str += '<tr><td align="left" width="5%">&nbsp;';
                
                str+='<input type="checkbox" id="myCheck' + c + '" name="myCheck' + c + '"  value="'+ response.options[i].assemblycd+'"  class="submenu" >';                             
                str+='</td><td align="left" width="20%">&nbsp;' + response.options[i].assemblycd + '</td>';
                str+='</td><td align="left" width="40%">&nbsp;' + response.options[i].assemblyname + '</td>';
                str+='<td align="left" width="20%">';
                str+='&nbsp;' + response.options[i].subdivision + '</td>';
                
                str+='<td align="left" width="15%">';
                if(response.options[i].zonename==null)
                {
                  str+='&nbsp;</td>';
                }
                else{
                  str+='&nbsp;' + response.options[i].zonename + '</td>';
                }
                str+='</td>';
                str+='<td align="left" width="15%">';
                if(response.options[i].name==null)
                {
                  str+='&nbsp;</td>';
                }
                else{
                  str+='&nbsp;' + response.options[i].name + '</td>';
                }
                str+='</td></tr>'; // return empty
        }
        str += "</table>";
        $("#assemblyDetails").append(str);
        
          $("#myCheckall").change(function(){ 
            var status = this.checked;
            if(status==true){
                $("#rw").show();
                scrollToElement($('#rw'));
            }else if(status==false){
                $("#rw").hide();
            }
            
            $('.submenu').each(function(){
                this.checked = status;     
            });
        });
        $('.submenu').change(function(){
            if($('.submenu:checked').length=='0'){
                $("#rw").hide();
            }else{
                $("#rw").show();
            }
            if(this.checked == false){
                $("#myCheckall")[0].checked = false; 
            }
            if ($('.submenu:checked').length == $('.submenu').length ){ 
                $("#myCheckall")[0].checked = true;
            }
        });
        getZoneDetails();
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $(".se-pre-con").fadeOut("slow");
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-exclamation-triangle',
                  content: msg
              });
         }
    });
}
 function getZoneDetails(){
    var forDist = $("#districtcd").val();
    var token = $("input[name='_token']").val();
    //$(".se-pre-con").fadeIn("slow");
    $.ajax({
       url: "getZoneDetails",
       method: 'POST',
       data: {forDist: forDist, _token: token},
       success: function (data) {//alert(data.options);
         $(".se-pre-con").fadeOut("slow");
          $("select[name='zone'").html('');
          if(data.status==1)
          {
             $("select[name='zone'").append('<option value>[Select]</option>');
            $.each(data.options,function (k, v)
            {
                $("select[name='zone'").append('<option value='+k+'>'+v+'</option>');
            });
           // getMemberDetails();
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
            $(".se-pre-con").fadeOut("slow");
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-exclamation-triangle',
                  content: msg
              });
         }
   });
 }
 function scrollToElement(ele) {
    $('html, body').animate({
        scrollTop: ele.offset().top - 60
    }, 1000);
}
</script>
@stop
