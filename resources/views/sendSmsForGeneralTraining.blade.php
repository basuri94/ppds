
@extends('layouts.master')
@section('content')
<style>
    .dropdown-menu{
        /* right: 67%;*/
        /* left: auto; */
        min-width: 0px !important; 
        width:230px !important;
        padding: 1em !important;
    }

</style>
<div class="wrapper">
    <!-- Sidebar Holder -->
    @include('layouts.sidebar')

    <!-- Page Content Holder -->
    <div id="content">           
        <section class="tables-section">
            <nav aria-label="breadcrumb" style="width:100%;">
                
                
                 <div class="breadcrumb pagehead1">
                      <span class="headbrand"><i class="fa fa-desktop"></i>Send SMS For General Training
            <span class="scoop-mcaret1"></span>
           </span>&nbsp;&nbsp;&nbsp;         
                {!! Form::button(' Save SMS For General Training &nbsp;<i class="fa fa-plus-circle"></i>',['id'=>'show_save_sms','class' => 'btn btn-primary-year add-new-button']) !!}  
                
               
                <div class="clearfix"></div>
        </div>
            </nav>

            <!-- form -->
            <div class="outer-w3-agile">
                {!! Form::open(['url' => 'send_sms_for_general', 'name' => 'send_sms_for_general', 'class' =>'request-info clearfix form-horizontal', 'id' =>'send_sms_for_general', 'method' => 'post','role'=>'','files' => true]) !!}
                {!! Form::hidden('districtcd', session()->get('districtcd_ppds'),['id'=>'districtcd']) !!}
<div class="panel-group" id="accordion5">                                                              
   <div class="panel panel-default">
    <div class="panel-heading1">
     <h6 class="panel-title">
         <a data-toggle="#" data-parent="#accordion5" href="#collapsePV"> <span class="fa fa-minus"></span> <span class="highlight">Training Records Available</span> </a>
     </h6>
    </div>
    <div id="collapsePV" class="panel-collapse collapse5">
      <div class="panel-body">  
        <div class="row"> 
            <div class='col-sm-12'>
                <div id='smsDetaails'></div>
            </div>
        </div>                                 
       {{-- <div class="row">  
         
           
         <div class='col-sm-3'>
            {!! Form::label('first_training_records', 'First Training Records:', ['class'=>'highlight']) !!}
           <div class="form-group">
               <div class=''>
                   {!! Form::text('first_training_records',null,['id'=>'first_training_records','class'=>'form-control','autocomplete'=>'off','readonly']) !!}
               </div>
           </div>
        </div> 
        <div class='col-sm-3'>
            {!! Form::label('first_training_venue', 'First Training Venue:', ['class'=>'highlight']) !!}
           <div class="form-group">
               <div class=''>
                   {!! Form::text('first_training_venue',null,['id'=>'first_training_venue','class'=>'form-control','autocomplete'=>'off','readonly']) !!}
               </div>
           </div>
        </div> 
           <div class='col-sm-3'>
            {!! Form::label('second_training_records', 'Second Training Records:', ['class'=>'highlight']) !!}
           <div class="form-group">
               <div class=''>
                    {!! Form::text('second_training_records',null,['id'=>'second_training_records','class'=>'form-control','autocomplete'=>'off','readonly']) !!}
               </div>
           </div>
        </div> 
        <div class='col-sm-3'>
            {!! Form::label('second_training_venue', 'Second Training Venue:', ['class'=>'highlight']) !!}
           <div class="form-group">
               <div class=''>
                   {!! Form::text('second_training_venue',null,['id'=>'second_training_venue','class'=>'form-control','autocomplete'=>'off','readonly']) !!}
               </div>
           </div>
        </div> 
        <div class='col-sm-3'>
            {!! Form::label('special_training1', 'First Training (Special 1) Records:', ['class'=>'highlight']) !!}
           <div class="form-group">
               <div class=''>
                    {!! Form::text('special_training1',null,['id'=>'special_training1','class'=>'form-control','autocomplete'=>'off','readonly']) !!}
               </div>
           </div>
        </div> 
        <div class='col-sm-3'>
            {!! Form::label('special_training1_venue', ' First Training (Special 1) Venue:', ['class'=>'highlight']) !!}
           <div class="form-group">
               <div class=''>
                   {!! Form::text('special_training1_venue',null,['id'=>'special_training1_venue','class'=>'form-control','autocomplete'=>'off','readonly']) !!}
               </div>
           </div>
        </div> 

        <div class='col-sm-3'>
            {!! Form::label('special_training2', 'Second Training (Special 1) Records:', ['class'=>'highlight']) !!}
           <div class="form-group">
               <div class=''>
                    {!! Form::text('special_training2',null,['id'=>'special_training2','class'=>'form-control','autocomplete'=>'off','readonly']) !!}
               </div>
           </div>
        </div> 
        <div class='col-sm-3'>
            {!! Form::label('special_training2_venue', ' Second Training (Special 1) Venue:', ['class'=>'highlight']) !!}
           <div class="form-group">
               <div class=''>
                   {!! Form::text('special_training2_venue',null,['id'=>'special_training2_venue','class'=>'form-control','autocomplete'=>'off','readonly']) !!}
               </div>
           </div>
        </div> 
      </div> --}}
      
           
       
    </div>                              
   </div>
  </div>  
  </div>

                <div class="panel-group" id="accordion5" style="padding-top: 0px;">                                                              
                    <div class="panel panel-default">
                        <div class="panel-heading1">
                            <h6 class="panel-title">
                                <a data-toggle="#" data-parent="#accordion5" href="#collapseUV"> <span class="fa fa-minus"></span> <span class="highlight">Send SMS </span></a>
                            </h6>
                        </div>
                        <div id="collapseUV" class="panel-collapse collapse5">
                            <div class="panel-body">                                   
                                <div class="row">    
                                    
                                    <div class='col-sm-3'>
                                        {!! Form::label('training_type', 'Type of Training:', ['class'=>'highlight required']) !!}
                                        <div class="form-group">
                                            <div class=''>
                                                {{-- {!! Form::select('training_type',['01'=>'First Training (General)','02'=>'Second Training (General)'],Null,['id'=>'training_type','class'=>'form-control','placeholder'=>'[Select]','autocomplete'=>'off']) !!} --}}
                                            
                                                 {!! Form::select('training_type',$dataAllforSMS['desc'],Null,['id'=>'training_type','class'=>'form-control','placeholder'=>'[Select]','autocomplete'=>'off']) !!}                   
                                                
                                            
                                            </div>
                                        </div>
                                    </div> 
                                    <div class='col-sm-3'>
                                        {!! Form::label('phase', 'Phase:', ['class'=>'highlight required']) !!}
                                        <div class="form-group">
                                            <div class=''>
                                              @php
                                                  $phase=App\tbl_tblsms1::join('phase','phase.code','=','tblsms1.phase_id')->select('phase_id','phase.name')->groupBy('phase_id')->get();
                                              @endphp
                                              <select class="form-control" id='phase' name='phase'><option value=''>Select</option>
                                              @foreach ($phase as $item)
                                              <option value='{{$item->phase_id}}'>{{$item->name}}</option>
                                              @endforeach
                                            </select>
                                            </div>
                                        </div>
                                    </div> 
                                     <div class='col-sm-3'>
            {!! Form::label('from', 'From Venue:', ['class'=>'highlight required']) !!}
            <div class="form-group">
               <div class=''>
                     {!! Form::text('from',null,['id'=>'from','class'=>'form-control','autocomplete'=>'off','readonly','maxlength'=>'4' ]) !!}
               </div>
            </div>
          </div>
                            
          <div class='col-sm-3'>
            {!! Form::label('to', 'To Venue:', ['class'=>'highlight required']) !!}
             {{-- <span style="color:red;font-size: 12px;">[Count should not be greater than 999]</span> --}}
            <div class="form-group">
               <div class=''>
                     {!! Form::text('to',null,['id'=>'to','class'=>'form-control','autocomplete'=>'off','readonly','maxlength'=>'4']) !!}
               </div>
            </div>
          </div>
              </div>
            
<div class="row"> 
            <div class='col-sm-12'>                    
            <div class="form-group text-right permit">                            	
               {{ Form::button('Send', ['class' => 'btn btn-info', 'type' => 'submit','id'=>'Send']) }}
               {{ Form::button('Reset', ['class' => 'btn btn-success' ,'type' => 'reset','id'=>'reset']) }}
            </div>
            </div>
      </div> 
                                {!! Form::close() !!}   
                              


                            </div>
                        </div>                              
                    </div> 


                </div>



            </div>
            <!--// form -->

        </section>
    </div>
</div>
<!-- Copyright -->
<script type="text/javascript">

    $(document).ready(function () {
getTrainingRecordAvailable('');

        $("#reset").click(function () {
            location.reload(true);
        });
char_num("#from");
char_num("#to");
       $('#training_type').on('change', function () {
        var phase=$('#phase').val();
        var training_type=$('#training_type').val();
        if(phase!="" && training_type!=""){
            SMSRecordssend();
        }
        
        });

        $('#phase').on('change', function () {
        var training_type=$('#training_type').val();
        var phase=$('#phase').val();
        if(phase!="" && training_type!=""){
            SMSRecordssend();
        }
        
        });
$("#show_save_sms").click(function(){
       window.location.href = "save_sms"; 
  });
        $('#send_sms_for_general')
                .bootstrapValidator({
                    message: 'This value is not valid',
                    feedbackIcons: {
                        valid: 'fa fa-check',
                        invalid: 'fa fa-times',
                        validating: 'fa fa-refresh'
                    },
                    fields: {
                        from: {
                            validators: {
                                notEmpty: {
                                    message: 'From PP is required'
                                }
                            }
                        },
                        to: {
                            validators: {
                                notEmpty: {
                                    message: 'PP count is required'
                                }
                            }
                        },
                        training_type: {
                            validators: {
                                notEmpty: {
                                    message: 'Training type is required'
                                }
                            }
                        },
                        phase: {
                            validators: {
                                notEmpty: {
                                    message: 'Phase is required'
                                }
                            }
                        }
                       
                    }
                }).on('success.form.bv', function (e) {
            // alert('HI');
            // Prevent form submission
            e.preventDefault();
            var action = $(this).attr('action');

            var from = $("#from").val();
           var to = $("#to").val();
           var phase=$('#phase').val();
            var type = $(this).attr('method');
            var token = $("input[name='_token']").val();
            var training_type=$('#training_type').val();
            var fd = new FormData();
           fd.append('from', from);
           fd.append('to', to);
            fd.append('training_type', training_type);
            fd.append('phase', phase);

            fd.append('_token', token);

              $(".se-pre-con1").show();
            $.ajax({
                type: type,
                url: action,
                data: fd,
                processData: false,
                contentType: false,
                dataType: "json",
                success: function (data) {
                    $(".se-pre-con1").fadeOut("slow");
                    if (data.status == 1)
                    {

                          $(".se-pre-con1").hide();

                        var msg = data.sendSms + " Venue Record(s) send successfully.<br> ";
                      

                        msg += data.mobileUser + " user message send successfully";
                        $.confirm({
                            title: 'Success!',
                            type: 'green',
                            icon: 'fa fa-check',
                            content: msg,
                            buttons: {
                                ok: function () {
                                   getTrainingRecordAvailable(phase);
                                     SMSRecordssend();
                                }

                            }
                        });
                    } 
$("#Send").removeAttr("disabled");
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $(".se-pre-con1").fadeOut("slow");

                    $("#Send").removeAttr("disabled");
                    var msg = "";
                    if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                        msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                    } else {
                        if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                            msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                        } else {
                            msg += "Error(s):<strong><ul>";
                            $.each(jqXHR.responseJSON, function (key, value) {
                                msg += "<li>" + value + "</li>";
                            });
                            msg += "</ul></strong>";
                        }
                    }
                    $.alert({
                        title: 'Error!!',
                        type: 'red',
                        icon: 'fa fa-warning',
                        content: msg
                    });
                }
            });


        });


    });

    

    function getTrainingRecordAvailable(phase){
  
   
    var token = $("input[name='_token']").val();
    // $(".se-pre-con").fadeIn("slow");
     $.ajax({
       url: "getTrainingRecordAvailable",
       method: 'POST',
       data: { _token: token,phase:phase},
       success: function (data) {
        //   $("#first_training_records").val("");
        //        $("#second_training_records").val("");
        //        $("#first_training_venue").val("");
        //        $("#second_training_venue").val("");

        //        $("#special_training1").val("");
        //        $("#special_training1_venue").val("");
        //        $("#special_training2").val("");
        //        $("#special_training2_venue").val("");
               
               
               
               
          if(data.status==1)
          {
            $("#smsDetaails").html("");
        var str = "";
        str+= "<table class='table table-bordered table-striped' width='100%' style='border-top: 2px solid #4cae4c;' id='dvData'>";
       
        str += ' <tr style="background-color: #f5f8fa"><th >&nbsp;Training Type </th><th >&nbsp;Total Venue</th><th>&nbsp;Remaining Venue </th>';
            str += '<th>&nbsp;Total Users</th><th>&nbsp;Remaining Users </th>';
            str += ' </tr>';
       
        str +=data.requirement
      
        $("#smsDetaails").append(str);
            // $("#second_training_venue").val(data.secondTrainingVenue);
            // $("#first_training_venue").val(data.firstTrainingVenue);
            //   $("#first_training_records").val(data.firstTraining);
            //    $("#second_training_records").val(data.secondTraining);

            //    $("#special_training1").val(data.secondTrainingVenue);
            //    $("#special_training1_venue").val(data.secondTrainingVenue);
            //    $("#special_training2").val(data.secondTrainingVenue);
            //    $("#special_training2_venue").val(data.secondTrainingVenue);
          }
       
       },
        error: function (jqXHR, textStatus, errorThrown) {
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-exclamation-triangle',
                  content: msg
              });
         }
   });
}
function SMSRecordssend(){
var training_type=$('#training_type').val();
var phase= $('#phase').val();
  var token = $("input[name='_token']").val();
    // $(".se-pre-con").fadeIn("slow");
     $.ajax({
       url: "SMSRecordssend",
       method: 'POST',
       data: { _token: token,training_type:training_type,phase:phase},
       success: function (data) {
          $("#from").val("");
             
          if(data.status==1)
          {
              $("#from").val(data.SmsSendAlready);
              $("#to").val(data.nextSmsLot);
          }
         
       },
        error: function (jqXHR, textStatus, errorThrown) {
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-exclamation-triangle',
                  content: msg
              });
         }
   });
}

</script>
@stop

