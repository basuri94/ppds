
@extends('layouts.master')
@section('content')

 <!--// top-bar -->
 <div class="wrapper">
<!-- Sidebar Holder -->
@include('layouts.sidebar')

<!-- Page Content Holder -->
<div id="content">           
<section class="tables-section">
    <nav aria-label="breadcrumb" style="width:100%;">
        <div class="breadcrumb pagehead1">
           
                {!! Form::button('<i class="fa fa-desktop"></i> Subdivision Wise PP Report',['class' => 'btn btn-primary-header add-new-button']) !!}
            
            <div class="col-md-offset-8 pull-right">
<!--                {!! Form::button('Academic Year:&nbsp;',['id'=>'show_academic','class' => 'btn btn-primary-year']) !!}-->
                
            </div>
                <div class="clearfix"></div>
        </div>
    </nav>

    <!-- form -->
<div class="outer-w3-agile">
  {!! Form::open(['url' => 'getPPdataSubdivisionWise', 'name' => 'getPPdataSubdivisionWise', 'class' =>'request-info clearfix form-horizontal', 'id' => 'getPPdataSubdivisionWise', 'method' => 'post','role'=>'','files' => true]) !!}
  {!! Form::hidden('districtcd', session()->get('districtcd_ppds'),['id'=>'districtcd']) !!}
  {!! Form::hidden('subdivcd', session()->get('subdivisioncd_ppds'),['id'=>'subdivcd']) !!}
  {!! Form::hidden('blockcd', session()->get('blockmunicd_ppds'),['id'=>'blockcd']) !!}
  
<!--  <span class="highlight">[Note: Record(s) transfered from Personnel to Zonewise]</span>-->
   <div class="panel-group" id="accordion5" style="width: 100%;float: left;">                                                              
    <div class="panel panel-default">
     <div class="panel-heading1">
     <h6 class="panel-title">
         <a data-toggle="#" data-parent="#accordion5" href="#collapseUV"> <span class="fa fa-minus"></span><span class="highlight">Subdivision-wise PP data </span></a>
     </h6>
     </div>
     <div id="collapseUV" class="panel-collapse collapse5">
      <div class="panel-body">                                   
        <div class="row"> 
          <div class='col-sm-3'>
               {!! Form::label('zone', 'Zone:', ['class'=>'highlight required']) !!}
              <div class="form-group">
                  <div class=''>
                      {!! Form::select('zone',[''=>'[Select]'],Null,['id'=>'zone','class'=>'form-control','autocomplete'=>'off']) !!}
                  </div>
              </div>
          </div>
                   
            <div class='col-sm-3'>
               {!! Form::label('subdivision', 'Sub Division:', ['class'=>'highlight']) !!}
              <div class="form-group">
                  <div class=''>
                      {!! Form::select('subdivision',[''=>'[Select]'],null,['id'=>'subdivision','class'=>'form-control','autocomplete'=>'off']) !!}
                  </div>
              </div>
          </div> 
         </div> 
          
           <div class="row"> 
            <div class='col-sm-12'>
                <div id='forZoneDetailsReqiured'></div>
            </div>
         
      </div> 
        
        <div class="row"> 
            <div class='col-sm-12'>                    
            <div class="form-group text-right permit">                            	
               {{ Form::button('Submit', ['class' => 'btn btn-info', 'type' => 'submit','id'=>'swapping']) }}
                {{ Form::button('EXCEL', ['class' => 'btn btn-info', 'type' => 'button','id'=>'EXCEL']) }}
               {{ Form::button('Reset', ['class' => 'btn btn-success' ,'type' => 'reset','id'=>'reset']) }}
            </div>
            </div>
      </div> 
             
       </div>
      </div>
        
        
    </div>                              
  </div> 
  
  
 
  

 {!! Form::close() !!}             
</div>
<!--// form -->


</section>

</div>
</div>
<!-- Copyright -->
<script type="text/javascript">
$(document).ready(function () {
   $("select[name='type'").append('<option value="B">Selected (Without Individual Training)</option>','<option value="D">Selected (With Individual Training)</option>','<option value="F">Selected (Without Group Training)</option>','<option value="G">Selected (With Group Training)</option>','<option value="E">Not Selected</option>','<option value="C">Cancelled</option>');
   $("#reset").click(function () {
    location.reload(true);
  });
  getZoneDetails(); 

  
 $('select[name="zone"]').on('change', function () {
     change_subdivision(); 
   
 });
   $('select[name="subdivision"]').on('change', function () {
     getSubDivisionPoststatusReport(); 
   
 });
 
  
  $('#EXCEL').click(function () {
   
     var zone = $("#zone").val();
    var subdivision = $("#subdivision").val();
  
     if(zone==""){
         $.alert({
            title: 'Error!!',
            type: 'red',
            icon: 'fa fa-exclamation-triangle',
            content: 'Zone is required'
        });
        return false;
     }
//     if(subdivision==""){
//         $.alert({
//            title: 'Error!!',
//            type: 'red',
//            icon: 'fa fa-exclamation-triangle',
//            content: 'Subdivision is required'
//        });
//        return false;
//     }
    // $(".se-pre-con").fadeIn("slow");
     var datas = {'zone': zone,'subdivision': subdivision, '_token': $('input[name="_token"]').val()};
     redirectPost('{{url("getSubDivisionWisePPExcel")}}', datas);
     // $(".se-pre-con").fadeOut("slow");
 });
   
});

function getZoneDetails(){
     var forDist = $("#districtcd").val();
     var token = $("input[name='_token']").val();
     $(".se-pre-con").fadeIn("slow");
     $.ajax({
       url: "getZoneDetails",
       method: 'POST',
       data: {forDist: forDist, _token: token},
       success: function (data) {//alert(data.options);
         $(".se-pre-con").fadeOut("slow");
          $("select[name='zone'").html('');
          if(data.status==1)
          {
             $("select[name='zone'").append('<option value>[Select]</option>');
            $.each(data.options,function (k, v)
            {
                $("select[name='zone'").append('<option value='+k+'>'+v+'</option>');
            });
           // getMemberDetails();
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
            $(".se-pre-con").fadeOut("slow");
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-warning',
                  content: msg
              });
         }
   });
 }
function getSubDivisionPoststatusReport(){
    var zone = $("#zone").val();
     var subdivision = $("#subdivision").val();
    var token = $("input[name='_token']").val();
  //  $(".se-pre-con").fadeIn("slow");
    $.ajax({
       url: "getSubDivisionPoststatusReport",
       method: 'POST',
       data: {zone: zone,subdivision:subdivision, _token: token},
       success: function (json) {//alert(data.options);
         // $(".se-pre-con").fadeOut("slow");
          //$("#memberDetails").html('');
          if(json.status==1)
          {
              //alert(json.options.length);
    
              $('#forZoneDetailsReqiured').html(json.options);
          }
          else{
              $.alert({
               title: 'Error!!',
               type: 'red',
               icon: 'fa fa-warning',
               content: '<strong>Please complete 1st Randomisation</strong>'
           });
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
         $(".se-pre-con").fadeOut("slow");
           var msg = "";
           if (jqXHR.status !== 422 && jqXHR.status !== 400) {
               msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
           } else {
               if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                   msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
               } else {
                   msg += "Error(s):<strong><ul>";
                   $.each(jqXHR.responseJSON, function (key, value) {
                       msg += "<li>" + value + "</li>";
                   });
                   msg += "</ul></strong>";
               }
           }
           $.alert({
               title: 'Error!!',
               type: 'red',
               icon: 'fa fa-warning',
               content: msg
           });
        }
   });
 }

function change_subdivision(){
  var forDist = $("#districtcd").val();
  var zone = $("#zone").val();
   var token = $("input[name='_token']").val();
   $(".se-pre-con").fadeIn("slow");
   $.ajax({
       url: "getZonewiseSubBlockDataQuery",
       method: 'POST',
       data: {forDist: forDist, zone: zone, _token: token},
       success: function (data) {//alert(data.options);
          $(".se-pre-con").fadeOut("slow");
          $("select[name='subdivision'").html('');
          
          if(data.status==1)
          {
            $("select[name='subdivision'").append('<option value>[Select]</option>');
            $.each(data.options,function (k, v)
            {
                $("select[name='subdivision'").append('<option value='+k+'>'+v+'</option>');
            });
             getSubDivisionPoststatusReport();
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
         $(".se-pre-con").fadeOut("slow");
           var msg = "";
           if (jqXHR.status !== 422 && jqXHR.status !== 400) {
               msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
           } else {
               if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                   msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
               } else {
                   msg += "Error(s):<strong><ul>";
                   $.each(jqXHR.responseJSON, function (key, value) {
                       msg += "<li>" + value + "</li>";
                   });
                   msg += "</ul></strong>";
               }
           }
           $.alert({
               title: 'Error!!',
               type: 'red',
               icon: 'fa fa-warning',
               content: msg
           });
         }
   });
}
</script>
@stop
