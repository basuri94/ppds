
@extends('layouts.master')
@section('content')
<style>
.p_note{
 padding-left: 10px;
 padding-right: 10px;
 padding-top: 10px;
  font-size: .9rem;
  line-height: 1.5;
  color: #f85a40;
  background-color: #fff;
}
</style>
<div class="wrapper">
    <!-- Sidebar Holder -->
    @include('layouts.sidebar')

    <!-- Page Content Holder -->
    <div id="content">           
        <section class="tables-section">
            <nav aria-label="breadcrumb" style="width:100%;">
                <div class="breadcrumb pagehead1">

                    {!! Form::button('<i class="fa fa-desktop"></i> ED & PD Excel Genereate',['class' => 'btn btn-primary-header add-new-button']) !!}

                    <div class="col-md-offset-8 pull-right">
                        <!--                {!! Form::button('Academic Year:&nbsp;',['id'=>'show_academic','class' => 'btn btn-primary-year']) !!}-->

                    </div>
                    <div class="clearfix"></div>
                </div>
            </nav>

            <!-- form -->
            <div class="outer-w3-agile">
                {!! Form::open(['url' => 'ed_pd_excel_generate', 'name' => 'ed_pd_excel_generate', 'class' =>'request-info clearfix form-horizontal', 'id' => 'ed_pd_excel_generate', 'method' => 'post','role'=>'','files' => true]) !!}
                {!! Form::hidden('districtcd', session()->get('districtcd_ppds'),['id'=>'districtcd']) !!}

                <div class="panel-group" id="accordion5" style="padding-top: 0px;">                                                              
                    <div class="panel panel-default">
                        <div class="panel-heading1">
                            <h6 class="panel-title">
                                <a data-toggle="#" data-parent="#accordion5" href="#collapseUV"> <span class="fa fa-minus"></span> <span class="highlight">ED & PD Excel Genereate</span> </a>
                            </h6>
                        </div>
                        <div class="row"> 
        <div class='col-sm-12'>
            <div class="form-group">
                <div class="p_note"><p style='font-weight:bold'>Note:Follow The Steps Mention Below</p><br> 1. Update Home PC. [Before 2nd Randomisation]<br>
                           2. Next ED(Election Duty) And PB(Post Ballot) Generate [After 2nd Randomisation]<br>
                           3. <span><a id="excel-download"><i class="fa fa-download"></i> Download</a></span> Excel After Complete The First Two Steps .
                </div>
              </div>
        </div>        
    </div>
                        <div id="collapseUV" class="panel-collapse collapse5">
                            <div class="panel-body">                                   
                                <div class="row"> 
                                    <div class='col-sm-3'>
                                        {!! Form::label('', '', ['class'=>'highlight']) !!}
                                        <div class="form-group">
                                            <div class=''>
                                                {!! Form::button('<i class="fas fa-sync" aria-hidden="true"></i> Update Home PC',['class' => 'btn btn-primary-header add-new-button','id'=>'updateHPC']) !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class='col-sm-3'>
                                        {!! Form::label('', '', ['class'=>'highlight']) !!}
                                        <div class="form-group">
                                            <div class=''>
                                                {!! Form::button('<i class="fas fa-sync" aria-hidden="true"></i> Ed & PD Generate',['class' => 'btn btn-primary-header add-new-button','id'=>'edpdGen']) !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class='col-sm-3'>
                                        {!! Form::label('typeOfEdPd', 'Type Of ED & PD:', ['class'=>'highlight required']) !!}
                                        <div class="form-group">
                                            <div class=''>
                                                {!! Form::select('typeOfEdPd',[''=>'[Select]','E'=>'Election Duty','P'=>'Postal Ballot'],Null,['id'=>'typeOfEdPd','class'=>'form-control','autocomplete'=>'off']) !!}
                                            </div>
                                        </div>
                                    </div>  

                                
                                <div class='col-sm-3'>
                                        {!! Form::label('', '', ['class'=>'highlight']) !!}
                                        <div class="form-group">
                                            <div class=''>
                                                {!! Form::button('<i class="fa fa-download" aria-hidden="true"></i> Excel Download',['class' => 'btn btn-primary-header add-new-button','id'=>'excelDownload']) !!}
                                            </div>
                                        </div>
                                    </div> 
                               
                                </div> 

                                <!--         <div class="row" > 
                                          <div class='col-sm-12'>                    
                                            <div class="form-group text-right permit">                            	
                                                   {{ Form::button('Populate', ['class' => 'btn btn-info', 'type' => 'submit','id'=>'submit']) }}
                                                   {{ Form::button('Reset', ['class' => 'btn btn-success' ,'type' => 'reset','id'=>'reset']) }}
                                            </div>
                                          </div>
                                          
                                       </div>-->
                            </div>
                        </div>                              
                    </div> 


                </div>


                {!! Form::close() !!}             
            </div>
            <!--// form -->


        </section>

    </div>
</div>
<!-- Copyright -->
<script type="text/javascript">
    $(document).ready(function () {

        $('select[name="zone"]').on('change', function () {
            change_subdivision();
        });
        $("#reset").click(function () {
            location.reload(true);
        });


       
//        $("#edpdGen").click(function () {
//            edpdGen();
//        });

        $("#excelDownload").click(function () {

            var typeOfEdPd = $('#typeOfEdPd').val();
            if (typeOfEdPd == "") {
                $.alert({
                    title: 'Error!!',
                    type: 'red',
                    icon: 'fa fa-exclamation-triangle',
                    content: 'Type Of PD & ED is required is required'
                });
                return false;
            }
            var datas = {'typeOfEdPd': typeOfEdPd, '_token': $('input[name="_token"]').val()};
            redirectPost('{{url("getExcelOfPDED")}}', datas);
        });

 $('#updateHPC').click(function () { 
      $.confirm({
                    title: 'Attention!',
                    content: 'Do you want to update Home PC?',
                    type: 'orange',
                    icon: 'fa fa-exclamation-triangle',
                    buttons: {
                        confirm: function () {
                            var token = $("input[name='_token']").val();
        $(".se-pre-con1").fadeIn("slow");
        $.ajax({
            url: "updateHomePC",
            method: 'POST',
            data: {_token: token},
            success: function (data) {
                $(".se-pre-con1").fadeOut("slow");

                if (data.status == 1) {
                    var msg = "<span style='font-size:13.5px;'>Record(s) Updated successfully<br>";
                    msg += "Total No of updated records => " + data.filtered;
                    msg += "<br>Other updated records => " + data.filteredLa;
                    msg += "</span>";
                    $.confirm({
                        title: 'Success!',
                        type: 'green',
                        icon: 'fa fa-check',
                        content: msg,
                        buttons: {
                            ok: function () {
                                location.reload(true);
                            }

                        }
                    });
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $(".se-pre-con1").fadeOut("slow");
                var msg = "";
                if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                    msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                } else {
                    if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                        msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                    } else {
                        msg += "Error(s):<strong><ul>";
                        $.each(jqXHR.responseJSON, function (key, value) {
                            msg += "<li>" + value + "</li>";
                        });
                        msg += "</ul></strong>";
                    }
                }
                $.alert({
                    title: 'Error!!',
                    type: 'red',
                    icon: 'fa fa-warning',
                    content: msg
                });
            }
        });
                        },
                         cancel: function () {

                        }
                    }
          });
    });
    
    $('#edpdGen').click(function () {  
$.confirm({
                    title: 'Attention!',
                    content: 'Do you want to generate ED & PD?',
                    type: 'orange',
                    icon: 'fa fa-exclamation-triangle',
                    buttons: {
                        confirm: function () {
        var token = $("input[name='_token']").val();
        $(".se-pre-con1").fadeIn("slow");
        $.ajax({
            url: "edpdGen",
            method: 'POST',
            data: {_token: token},
            success: function (data) {
                $(".se-pre-con1").fadeOut("slow");

                if (data.status == 1) {
                    var msg = "";
                    msg += "<span>";
                    $.each(data.totalDataPc, function (index, value) {
                        msg += value.dataPccd + "-" + value.pcname + " ED:- " + value.count + "<br>";

                    });
                    msg += "</br>Total No of Post Ballot(PB):- " + data.pb_count;
                    msg += "</span>";

                    $.confirm({
                        title: 'Success!',
                        type: 'green',
                        icon: 'fa fa-check',
                        content: msg,
                        buttons: {
                            ok: function () {
                                location.reload(true);
                            }

                        }
                    });
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $(".se-pre-con1").fadeOut("slow");
                var msg = "";
                if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                    msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                } else {
                    if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                        msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                    } else {
                        msg += "Error(s):<strong><ul>";
                        $.each(jqXHR.responseJSON, function (key, value) {
                            msg += "<li>" + value + "</li>";
                        });
                        msg += "</ul></strong>";
                    }
                }
                $.alert({
                    title: 'Error!!',
                    type: 'red',
                    icon: 'fa fa-warning',
                    content: msg
                });
                 }
        });
                        },
                         cancel: function () {

                        }
                    }
          });
    });

    });

   

    
                
</script>
@stop

