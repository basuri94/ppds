@extends('layouts.master')
@section('content')
<style>
    .jconfirm-box{
        width: 650px;
        margin-left: -250px !important; 
    }
</style>
<!--// top-bar -->
<div class="wrapper">
    <!-- Sidebar Holder -->
    @include('layouts.sidebar')
    <!-- Page Content Holder -->
    <div id="content">           
        <section class="tables-section">

            <div class="breadcrumb pagehead1">
                <span class="headbrand"><i class="fa fa-desktop"></i> List of Assembly Reserve
                    <span class="scoop-mcaret1"></span>
                </span>&nbsp;&nbsp;
                &nbsp;{!! Form::button(' Add/Edit Assembly Reserve&nbsp;<i class="fa fa-plus-circle"></i>',['id'=>'add_asm_party','class' => 'btn btn-primary-year add-new-button']) !!}  


                <div class="clearfix"></div>
            </div>
            </nav>
            <div class="outer-w3-agile">
                <div class="panel-group" id="accordion5">                                                              
                    <div class="panel panel-default">
                        <div class="panel-heading1">
                            <h6 class="panel-title">
                                <a data-toggle="#" data-parent="#accordion5" href="#collapsePV"> <span class="fa fa-minus"></span> <span class="highlight">Zonewise Assembly Reserve List</span></a>
                            </h6>
                        </div>
                        <div id="collapsePV" class="panel-collapse collapse5">
                            <div class="panel-body">                                   
                                <div class="row">

                                    <div class='col-sm-4'>
                                        {!! Form::label('forZone', 'For Zone:', ['class'=>'highlight ']) !!}
                                        <div class="form-group">
                                            <div class=''>
                                                {!! Form::select('forZone',[''=>'[ALL]'],Null,['id'=>'forZone','class'=>'form-control','autocomplete'=>'off']) !!}
                                            </div>
                                        </div>
                                    </div> 
                                    <div class='col-sm-4'>
                                        {!! Form::label('assembly', 'Assembly:', ['class'=>'highlight ']) !!}
                                        <div class="form-group">
                                            <div class=''>
                                                {!! Form::select('assembly',[''=>'[ALL]'],Null,['id'=>'assembly','class'=>'form-control','autocomplete'=>'off']) !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="form-horizontal">
                            <div class="form-group row">
                                {{csrf_field()}}
                                <div class="datatbl table-responsive" style="width: 96%;margin-left: 20px;">
                                    {!! Form::hidden('districtcd', session()->get('districtcd_ppds'),['id'=>'districtcd']) !!}
                                    <table class="table table-striped table-bordered table-hover notice-types-table" id="datatable-table" style="">
                                        <thead>
                                            <tr>
                                                <th>SL#</th>
                                                <th>Assembly Code</th>
                                                <th>Assembly Name</th>
                                                <th>Member</th>
                                                <th>Gender</th>
                                                <th>Party</th>
                                                <th>Type(P/N)</th>
                                                <th>Number</th>
                                                <th>Post status</th>
<!--                                                <th>PR</th>
                                                <th>P1</th>
                                                <th>P2</th>
                                                <th>P3</th>
                                                <th>PA</th>
                                                <th>PB</th>-->
                                                <th>Action</th>
                                            </tr>

                                        </thead>
                                        <tbody></tbody>
                                        <!-- Table Footer -->

                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </section>
    </div>
</div>
<script>
    $(document).ready(function () {
        $("#add_asm_party").click(function () {
            window.location.href = "assemblyreserve_formation";
        });
        getZoneDetails();
        $('select[name="forZone"]').on('change', function () {
            var zone = $("#forZone").val();
            if(zone == ""){
                create_table();
            }else{
              getAssemblyDetails();
            }
        });
        $('select[name="assembly"]').on('change', function () {

            create_table();
        });


        var table = $('#datatable-table').DataTable();
        table.on('draw.dt', function () {
//table.fnSetColumnVis( 1, false, false ); 
            $('.edit-button').click(function () {
                var value_all = this.id;
                //     alert(value_all);
                $.ajax({
                    type: 'post',
                    url: 'check_for_edit_reserve',
                    data: {'data': value_all, '_token': $('input[name="_token"]').val()},
                    dataType: 'json',
                    success: function (datam) {
                        $(".se-pre-con").fadeOut("slow");
                        //alert(datam.count);
                        if (datam.count > 0) {
                            $.alert({
                                title: 'Error!!',
                                type: 'red',
                                icon: 'fa fa-exclamation-triangle',
                                content: "Record exist in another table.You can't change this record."
                            });
                            return false;
                        }
                        var edit_val = value_all.split("/");
                        var table_edit = '<form id="edit_first" action="" method="post" class="form-horizontal">';
                        table_edit += '<input type="hidden" value="' + edit_val[4] + '" id="chk_num_per">';

                        table_edit += '<div class="form-group">';
                        table_edit += '<label class="control-label col-md-6 required">Assembly Code:</label>';
                        table_edit += '<div class="col-md-6"><input type="text" class="form-control" value="' + edit_val[0] + '" id="assem_code" name="assem_code" style="margin-bottom: 2px;" readonly></div>';
                        table_edit += '</div>';
                        table_edit += '<div class="form-group">';
                        table_edit += '<label class="control-label col-md-6 required">Member:</label>';
                        table_edit += '<div class="col-md-6"><input type="text" class="form-control" value="' + edit_val[1] + '" id="mem_ber" name="mem_ber" style="margin-bottom: 2px;" readonly></div>';
                        table_edit += '</div>';
                        table_edit += '<div class="form-group">';
                        table_edit += '<label class="control-label col-md-6 required">Post Status :</label>';
                        table_edit += '<div class="col-md-6"><input type="text" class="form-control" value="' + edit_val[6] + '" id="post_status" name="post_status" style="margin-bottom: 2px;" readonly></div>';
                        table_edit += '</div>';
                        table_edit += '<div class="form-group">';
                        table_edit += '<label class="control-label col-md-6 required">Gender:</label>';
                        table_edit += '<div class="col-md-6"><input type="text" class="form-control" value="' + edit_val[2] + '" id="gen_der" name="gen_der" style="margin-bottom: 2px;" readonly></div>';
                        table_edit += '</div>';
                        table_edit += '<div class="form-group">';
                        table_edit += '<label class="control-label col-md-6 required">Type (Percentage or Number):</label>';
                        table_edit += '<div class="col-md-6"><select class="form-control" style="margin-bottom: 2px;" name="type" id="type"><option value="P">Percentage</option><option value="N">Number</option></select></div>';
                        table_edit += '</div>';
                        table_edit += '<div class="form-group">';
                        table_edit += '<label class="control-label col-md-6 required">Number:</label>';
                        table_edit += '<div class="col-md-6"><input type="text" class="form-control" value="' + edit_val[5] + '" id="num_ber" name="num_ber" style="margin-bottom: 2px;" maxlength="4" ></div>';
                        table_edit += '</div>';
                        table_edit += '</form>';
                        var jc = $.confirm({
                            title: '<span style="font-size: 17px;">Zonewise Assembly Reserve Formation</span>',
                            content: table_edit,
                            type: 'green',
                            icon: 'fa fa-edit',
                            typeAnimated: true,
                            buttons: {

                                edit: {
                                    btnClass: 'btn-primary',
                                    action: function () {
                                         var arr = ['assem_code', 'mem_ber', 'post_status', 'gen_der', 'type', 'num_ber'];
                                        var flag = 0;
                                        jQuery.each(arr, function (index, item) {
                                            var check = $("#" + item + "").val();
                                            if (check == '') {
                                                $.alert({
                                                    title: 'Error!!',
                                                    type: 'red',
                                                    icon: 'fa fa-exclamation-triangle',
                                                    content: "All fields are mandatory"
                                                });
//                                                $.alert('All fields are mandatory');
                                                flag = 1;
                                                return false;
                                            }
                                        });
                                        if (flag > 0) {
                                            return false;
                                        }
                                        jc.showLoading(true);
                                        var assem_code = $("#assem_code").val();
                                        var mem_ber = $("#mem_ber").val();
                                        var post_status = $("#post_status").val();
                                        var gen_der = $("#gen_der").val();
                                        var type = $("#type").val();
                                        var num_ber = $("#num_ber").val();
                                        return $.ajax({
                                            url: 'edit_reserve_party_form',
                                            dataType: 'json',
                                            data: {'assem_code': assem_code, 'mem_ber': mem_ber, 'post_status': post_status, 'gen_der': gen_der, 'type': type, 'num_ber': num_ber, '_token': $("input[name='_token']").val()},
                                            method: 'post'
                                        }).done(function (response) {
                                            //alert('hi');
                                            jc.hideLoading(true);
                                            if (response.status == 1) {
                                                var msg_suc = "<strong>SUCCESS: Record edited successfuly</strong>";
                                                $.confirm({
                                                    title: 'Success!',
                                                    type: 'green',
                                                    icon: 'fa fa-check',
                                                    content: msg_suc,
                                                    buttons: {
                                                        ok: function () {  
                                                            //table.ajax.reload();
                                                            create_table();
                                                        }

                                                    }
                                                });
                                            }
                                        }).fail(function (jqXHR, textStatus, errorThrown) {
                                            var msg = "";
                                            if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                                                msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                                            } else {
                                                if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                                                    if (jqXHR.responseJSON.exception_code == 23000) {
                                                        msg += "Record exist in another table.You can't change this record.";
                                                    } else {
                                                        msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                                                    }
                                                } else {
                                                    msg += "Error(s):<strong><ul>";
                                                    $.each(jqXHR.responseJSON, function (key, value) {
                                                        msg += "<li>" + value + "</li>";
                                                    });
                                                    msg += "</ul></strong>";
                                                }
                                            }
                                            $.alert({
                                                title: 'Error!!',
                                                type: 'red',
                                                icon: 'fa fa-warning',
                                                content: msg,
                                            });
                                        });
                                    }
                                },
                                close: function () {
                                }
                            },
                            onContentReady: function () {
// bind to events
                                var chk_num_per = $("#chk_num_per").val();

                                $('#type').val(chk_num_per);

                            },

                            onOpen: function () {
                                // after the modal is displayed.
                                //startTimer();
                            }
                        });
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        $(".se-pre-con").fadeOut("slow");
                        var msg = "";
                        if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                            msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                        } else {
                            if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                                if (jqXHR.responseJSON.exception_code == 23000) {
                                    msg += "Record exist in another table.You can't change this record.";
                                } else {
                                    msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                                }
                            } else {
                                msg += "Error(s):<strong><ul>";
                                $.each(jqXHR.responseJSON, function (key, value) {
                                    msg += "<li>" + value + "</li>";
                                });
                                msg += "</ul></strong>";
                            }
                        }
                        $.alert({
                            title: 'Error!!',
                            type: 'red',
                            icon: 'fa fa-warning',
                            content: msg,
                        });
                    }
                });

            });
            $('.delete-button').click(function () {
//                var reply = $.confirm('Are you sure to delete the record?');
//                if (!reply) {
//                    return false;
//   
//                             }
                var value_all = this.id;
                $.ajax({
                    type: 'post',
                    url: 'check_for_edit_delete',
                    data: {'data': value_all, '_token': $('input[name="_token"]').val()},
                    dataType: 'json',
                    success: function (datam) {
                        $(".se-pre-con").fadeOut("slow");
                        //alert(datam.count);
                        if (datam.count > 0) {
                            $.alert({
                                title: 'Error!!',
                                type: 'red',
                                icon: 'fa fa-exclamation-triangle',
                                content: "Record exist in another table.You can't delete this record."
                            });
                            return false;
                        }
                        $.confirm({
                            title: 'Attention!',
                            content: 'Do you want to delete this record?',
                            type: 'orange',
                            icon: 'fa fa-exclamation-triangle',
                            buttons: {
                                confirm: function () {
                                    var data_new = value_all;
                                    //alert(data_new);
                                    $.ajax({
                                        type: 'post',
                                        url: 'delete_assembly_party_form',
                                        data: {'data': data_new, '_token': $('input[name="_token"]').val()},
                                        dataType: 'json',
                                        success: function (datam) {
                                             var msg = "<strong>SUCCESS: Record deleted successfully</strong>";
                                            $.confirm({
                                                title: 'Success!',
                                                type: 'green',
                                                icon: 'fa fa-check',
                                                content: msg,
                                                buttons: {
                                                    ok: function () {  
                                                        //table.ajax.reload();
                                                        create_table();
                                                    }
                                                }
                                            });
                                        },
                                        error: function (jqXHR, textStatus, errorThrown) {
                                            $(".se-pre-con").fadeOut("slow");
                                            var msg = "";
                                            if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                                                msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                                            } else {
                                                if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                                                    if (jqXHR.responseJSON.exception_code == 23000) {
                                                        msg += "Record exist in another table.You can't delete this record.";
                                                    } else {
                                                        msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                                                    }
                                                } else {
                                                    msg += "Error(s):<strong><ul>";
                                                    $.each(jqXHR.responseJSON, function (key, value) {
                                                        msg += "<li>" + value + "</li>";
                                                    });
                                                    msg += "</ul></strong>";
                                                }
                                            }
                                            $.alert({
                                                title: 'Error!!',
                                                type: 'red',
                                                icon: 'fa fa-warning',
                                                content: msg,
                                            });
                                        }
                                    });
                                },
                                cancel: function () {

                                }
                            }
                        });
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        $(".se-pre-con").fadeOut("slow");
                        var msg = "";
                        if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                            msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                        } else {
                            if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                                if (jqXHR.responseJSON.exception_code == 23000) {
                                    msg += "Record exist in another table.You can't delete this record.";
                                } else {
                                    msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                                }
                            } else {
                                msg += "Error(s):<strong><ul>";
                                $.each(jqXHR.responseJSON, function (key, value) {
                                    msg += "<li>" + value + "</li>";
                                });
                                msg += "</ul></strong>";
                            }
                        }
                        $.alert({
                            title: 'Error!!',
                            type: 'red',
                            icon: 'fa fa-warning',
                            content: msg,
                        });
                    }
                });


            });


        });
        // table.columns( [-4,-3,-2,-1] ).visible( false );

    });
    function getZoneDetails() {
        var forDist = $("#districtcd").val();
        var token = $("input[name='_token']").val();
        $(".se-pre-con").fadeIn("slow");
        $.ajax({
            url: "getZoneDetails",
            method: 'POST',
            data: {forDist: forDist, _token: token},
            success: function (data) {//alert(data.options);
                $(".se-pre-con").fadeOut("slow");
                $("select[name='forZone'").html('');
                if (data.status == 1)
                {
                    $("select[name='forZone'").append('<option value="">[ALL]</option>');
                    $.each(data.options, function (k, v)
                    {
                        $("select[name='forZone'").append('<option value=' + k + '>' + v + '</option>');
                    });
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $(".se-pre-con").fadeOut("slow");
                var msg = "";
                if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                    msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                } else {
                    if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                        msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                    } else {
                        msg += "Error(s):<strong><ul>";
                        $.each(jqXHR.responseJSON, function (key, value) {
                            msg += "<li>" + value + "</li>";
                        });
                        msg += "</ul></strong>";
                    }
                }
                $.alert({
                    title: 'Error!!',
                    type: 'red',
                    icon: 'fa fa-exclamation-triangle',
                    content: msg
                });
            },
            complete: function () {
                create_table();
            }
        });
    }
    function getAssemblyDetails() {
        var forZone = $("#forZone").val();
        var token = $("input[name='_token']").val();
        $(".se-pre-con").fadeIn("slow");
        $.ajax({
            url: "getAssemblyDetails",
            method: 'POST',
            data: {forZone: forZone, _token: token},
            success: function (data) {//alert(data.options);
                $(".se-pre-con").fadeOut("slow");
                $("select[name='assembly'").html('');
                if (data.status == 1)
                {
                    $("select[name='assembly'").html(data.options);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $(".se-pre-con").fadeOut("slow");
                var msg = "";
                if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                    msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                } else {
                    if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                        msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                    } else {
                        msg += "Error(s):<strong><ul>";
                        $.each(jqXHR.responseJSON, function (key, value) {
                            msg += "<li>" + value + "</li>";
                        });
                        msg += "</ul></strong>";
                    }
                }
                $.alert({
                    title: 'Error!!',
                    type: 'red',
                    icon: 'fa fa-exclamation-triangle',
                    content: msg
                });
            },
            complete: function () {
                create_table();
            }
        });
    }
    function create_table() {
        var table = "";
        var token = $('input[name="_token"]').val();

        $("#datatable-table").dataTable().fnDestroy();
        table = $('#datatable-table').DataTable({
            "processing": true,
            "serverSide": true,
            "dom": 'lBfrtip',
            "buttons": [
                //'copy', 'csv', 'excel', 'pdf'
                {
                    extend: 'collection',
                    text: 'Export',
                    buttons: [
                        {
                            extend: 'copyHtml5',
                            exportOptions: {
                                columns: ':visible'
                            }
                        },
                        {
                            extend: 'excelHtml5',
                            exportOptions: {
                                columns: ':visible'
                            }
                        },
                        {
                            extend: 'pdfHtml5',
                            exportOptions: {
                                columns: ':visible'
                            }
                        },
                        {
                            extend: 'print',
                            exportOptions: {
                                columns: ':visible'
                            }
                        },
                        {
                            extend: 'csvHtml5',
                            exportOptions: {
                                columns: ':visible'
                            }
                        }

                    ]

                },
                'colvis'
            ],
            "ajax": {
                url: "assem_party_reserve_list_datatable",
                type: "post",
                data: {'_token': $('input[name="_token"]').val(), 'zone': $("#forZone").val(), 'assembly': $("#assembly").val()},
                dataSrc: "reserve_part_form",
                error: function (jqXHR, textStatus, errorThrown) {
                    var msg = "";
                    if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                        msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                    } else {
                        if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                            if (jqXHR.responseJSON.exception_code == 23000) {
                                msg += "Some Sql Exception Occured";
                            } else {
                                msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                            }
                        } else {
                            msg += "Error(s):<strong><ul>";
                            $.each(jqXHR.responseJSON, function (key, value) {
                                msg += "<li>" + value + "</li>";
                            });
                            msg += "</ul></strong>";
                        }
                    }
                    $.alert({
                        title: 'Error!!',
                        type: 'red',
                        icon: 'fa fa-warning',
                        content: msg,
                    });
                }
            },
            "dataType": 'json',
            "columnDefs":
                    [
                        {className: "table-text", "targets": "_all"},
                        {
                            "targets": 0,
                            "data": "code",
                            "defaultContent": "",
                            "searchable": false,
                            "sortable": false
                        },

                        {
                            "targets": 1,
                            "data": "forassembly"
                        },
                        {
                            "targets": 2,
                            "data": "assemblyname"
                        },
                        {
                            "targets": 3,
                            "data": "number_of_member"
                        },
                        {
                            "targets": 4,
                            "data": "gender"
                        },
                        {
                            "targets": 5,
                            "data": "no_party"
                        },
                        {
                            "targets": 6,
                            "data": "no_or_pc"
                        },
                        {
                            "targets": 7,
                            "data": "numb"
                        },
                        {
                            "targets": 8,
                            "data": "poststat"
                        },
                        {
                            "targets": -1,
                            "data": 'action',
                            "searchable": false,
                            "sortable": false,
                            "render": function (data, type, full, meta) {

                                var str_btns = "";

                                str_btns += '<button type="button"  class="btn btn-success  edit-button btn_new1" id="' + data.e + '" title="Edit"><i class="fa fa-edit"></i></button>&nbsp&nbsp';



                                //  str_btns += '<button type="button"  class="btn btn-danger  delete-button btn_new1" id="' + data.d + '" title="Delete"><i class="fa fa-trash"></i></button>';

                                return str_btns;
                            }
                        }
                    ],

            "order": [[1, 'asc']]
        });
        table.on('order.dt search.dt draw.dt', function () {
            $('[data-toggle="tooltip"]').tooltip();
            table.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
                cell.innerHTML = table.page() * table.page.len() + (i + 1);
            });
        });
    }
</script>
<!-- Copyright -->
@stop