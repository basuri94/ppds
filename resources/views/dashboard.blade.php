@extends('layouts.master')

@section('content')
<?php 
use App\Http\Controllers\DashboardController;
if (session()->has('code_ppds') == 1) {
$dist= \Session::get('districtcd_ppds');
if($dist>0){

$data_block=DashboardController::Get_block_all();
$block_data_rep=DashboardController::get_block_wise_report($data_block);
$block_data_report=(json_encode($block_data_rep));
//array_push($data_block,"Total");
$blocks=(json_encode($data_block));
$block_count=count($data_block)-1;

$data_subdivision_first=DashboardController::Get_subdivision_all();
$data_subdivision=$data_subdivision_first[0];
$subdivision_data_rep=DashboardController::get_subdivision_wise_report($data_subdivision);
$subdivision_data_report=(json_encode($subdivision_data_rep));

//array_push($data_block,"Total");
$subdivisions=(json_encode($data_subdivision));
if($data_subdivision!=""){
  $subdivision_count=count($data_subdivision)-1;
}else{
  $subdivision_count = 0;  
}

$data_institute=DashboardController::get_all_institute();
$institution=DashboardController::get_institution_wise_report($data_institute);
$institution_pie=DashboardController::get_institution_wise_report_pie($data_institute);
$tot_arr=array();
for($ins=0;$ins<count($institution_pie);$ins++){
  $ins_arr=array();
  array_push($ins_arr,$data_institute[$ins]);
  array_push($ins_arr,$institution_pie[$ins]);
   
   array_push($tot_arr,$ins_arr);
}
$ins_pie_data= json_encode($tot_arr);
//echo $ins_pie_data;die;
$institution_data=(json_encode($institution));
 //array_push($data_institute,"Total");
$institute=(json_encode($data_institute));
//echo $institution_data;die;

$institute_count=count($data_institute)-1;

//print_r($institution_data);die;
$data_govt=DashboardController::get_all_govt();
$govt_data=DashboardController::get_govt_wise_report($data_govt);
$gov_pie=DashboardController::get_govt_wise_report_pie($data_govt);
$tot_arr_gov=array();
for($gov=0;$gov<count($gov_pie);$gov++){
  $gov_arr=array();
  array_push($gov_arr,$data_govt[$gov]);
  array_push($gov_arr,$gov_pie[$gov]);
   
   array_push($tot_arr_gov,$gov_arr);
}
$gov_pie_data= json_encode($tot_arr_gov);
$govt_report=(json_encode($govt_data));
 //array_push($data_govt,"Total");
$govt=(json_encode($data_govt));
$govt_count=count($data_govt)-1;

$data_postat=DashboardController::get_all_postat();
//array_push($data_postat[0],"Total");
//array_push($data_postat[1],"Total");
$postat_data=(json_encode($data_postat[0]));
$postat_count=count($data_postat[0])-1;
$district_wise_postat=DashboardController::district_wise_postat_required($data_postat[1]);
//echo json_encode($district_wise_postat);die;
$data_req_p=($district_wise_postat['P']);
$res_p = "[" . implode ( ",", $data_req_p ) . "]";
$data_req_r=($district_wise_postat['R']);
$res_r = "[" . implode ( ",", $data_req_r ) . "]";



$district_wise_postat_ass=DashboardController::district_wise_postat_assigned($data_postat[1]);
//echo json_encode($district_wise_postat_ass);die;
$data_ass_p=json_encode($district_wise_postat_ass['P']);
$data_ass_r=json_encode($district_wise_postat_ass['R']);
//echo ($district_wise_postat_ass['P'][1]);die;
$sf_p=array();
for($i=0;$i<count($data_req_p);$i++){
    $sf=$data_req_p[$i]-$district_wise_postat_ass['P'][$i];
    array_push($sf_p,$sf);
}
$short_p= json_encode($sf_p);
$sf_r=array();
for($i=0;$i<count($data_req_r);$i++){
    $sf__new=$data_req_r[$i]-$district_wise_postat_ass['R'][$i];
    array_push($sf_r,$sf__new);
}
$short_r= json_encode($sf_r);


$pstat_pie=DashboardController::get_pstat_wise_report_pie($data_postat[1]);
//print_r($pstat_pie);die;
$tot_arr_pstat=array();
for($pstat=0;$pstat<count($pstat_pie);$pstat++){
  $pstat_arr=array();
  array_push($pstat_arr,$data_postat[0][$pstat]."(".$data_postat[1][$pstat].")");
  
  array_push($pstat_arr,$pstat_pie[$pstat]);
 
   array_push($tot_arr_pstat,$pstat_arr);
    // print_r($tot_arr_pstat);die;
}
$pstat_pie_data= json_encode($tot_arr_pstat);
//echo $pstat_pie_data;die;
}
?>

<style>
    .highcharts-container {
        width:auto !important;
    }
    
</style>

<!--<div class="container-fluid clearfix">
        <div class="row">
        	<nav aria-label="breadcrumb" style="width:100%;">
                <ol class="breadcrumb" style="width:100%; margin-bottom:0px; background-color: #bad3e8; border-radius:0px;padding-top: 5px;padding-bottom: 5px;font-size: 14px;">
                    <li class="breadcrumb-item"><a href="#">Home/</a></li>
                </ol>
            </nav>
        </div>
    </div>-->
    <!--// top-bar -->
    <div class="wrapper">
        <!-- Sidebar Holder -->
		@include('layouts.sidebar')
        <!-- Page Content Holder -->
        <div id="content"> 
            <div class="outer-w3-agile1">
            <div class="">
                <div id="pie1" class="outer-w3-agile1" style="width:49%;float:left;margin: 5px;height: 300px;/*min-width: 310px; height: 400px; max-width: 600px;*/"></div>
                <div id="pie2"  class="outer-w3-agile1" style="width:49%;float:left;margin: 5px;height: 300px;/*min-width: 310px;height: 400px; max-width: 600px; */"></div>
            </div>  
            </div>
            <div class="outer-w3-agile1">
            <div class="">
                <div id="pie3" class="outer-w3-agile1" style="width:49%;float:left;margin: 5px;height: 300px;/*min-width: 310px; height: 400px; max-width: 600px;*/"></div>
                <div id="bar0"  class="outer-w3-agile1" style="width:49%;float:left;margin: 5px;height: 300px;/*min-width: 310px;height: 400px; max-width: 600px; */"></div>
            </div>  
            </div>
            <div class="outer-w3-agile1">
                <div class="">
                  <div id="bar1" class="outer-w3-agile1 mt-3" style="margin-right:5px;margin-left:5px;float:left;width:99%;/*height: 400px;*/ "></div>
                  <div id="bar2" class="outer-w3-agile1 mt-3" style="margin-right:5px;margin-left:5px;width:99%;float:left; /*height: 400px;*/"></div>
                  <div id="bar3" class="outer-w3-agile1 mt-3" style="margin-right:5px;margin-left:5px;width:99%; height: 1200px; float:left; "></div>
                  <div id="bar4" class="outer-w3-agile1 mt-3" style="margin-right:5px;margin-left:5px;width:99%;float:left;  /*height: 1200px;*/"></div>
                </div>
            </div>
   
          </div>
    </div>
    <script>
       
<?php 
if($dist>0){
?>
 Highcharts.createElement('link', {
    href: 'https://fonts.googleapis.com/css?family=Signika:400,700',
    rel: 'stylesheet',
    type: 'text/css'
}, null, document.getElementsByTagName('head')[0]);

// Add the background image to the container
Highcharts.wrap(Highcharts.Chart.prototype, 'getContainer', function (proceed) {
    proceed.call(this);
    this.container.style.background =
        'url({{config("constants.APP_ROOT")}}sand.png)';
});


Highcharts.theme = {
    colors: ['#2b908f', '#90ee7e', '#f45b5b', '#7798BF', '#aaeeee', '#ff0066',
        '#eeaaee', '#55BF3B', '#DF5353', '#7798BF', '#aaeeee'],
    chart: {
        backgroundColor: null,
        style: {
            fontFamily: 'Signika, serif'
        }
    },
    title: {
        style: {
            color: 'black',
            fontSize: '16px',
            fontWeight: 'bold'
        }
    },
    subtitle: {
        style: {
            color: 'black'
        }
    },
    tooltip: {
        borderWidth: 0
    },
    legend: {
        itemStyle: {
            fontWeight: 'bold',
            fontSize: '13px'
        }
    },
    xAxis: {
        labels: {
            style: {
                color: '#6e6e70'
            }
        }
    },
    yAxis: {
        labels: {
            style: {
                color: '#6e6e70'
            }
        }
    },
    plotOptions: {
        series: {
            shadow: true
        },
        candlestick: {
            lineColor: '#404048'
        },
        map: {
            shadow: false
        }
    },

    // Highstock specific
    navigator: {
        xAxis: {
            gridLineColor: '#D0D0D8'
        }
    },
    rangeSelector: {
        buttonTheme: {
            fill: 'white',
            stroke: '#C0C0C8',
            'stroke-width': 1,
            states: {
                select: {
                    fill: '#D0D0D8'
                }
            }
        }
    },
    scrollbar: {
        trackBorderColor: '#C0C0C8'
    },

    // General
    background2: '#E0E0E8'

};

// Apply the theme
Highcharts.setOptions(Highcharts.theme); 

Highcharts.chart('pie1', {
   
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: 0,
        plotShadow: false,
        marginBottom:-50
       
    },
    title: {
        text: 'Institute<br>Wise<br>Polling Personnel',
        align: 'center',
        verticalAlign: 'middle',
        y: 60
    },
    tooltip: {
         headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y:1f} </b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        pie: {
            dataLabels: {
                enabled: true,
                distance: 20,
                style: {
                    fontWeight: 'bold',
                    color: 'black'
                }
            },
            startAngle: -90,
            endAngle: 90,
            center: ['50%', '75%'],
            size: '100%'
        }
    },
    series: [{
        type: 'pie',
        name: 'Polling Personnel',
        innerSize: '50%',
        
        data: <?php echo $ins_pie_data; ?>
    }]
});
Highcharts.chart('pie2', {
    chart: {
        plotBackgroundColor: null,
        plotBorderWidth: 0,
        plotShadow: false,
        marginBottom:-50
    },
    title: {
        text: 'Category<br>Wise<br>Polling Personnel',
        align: 'center',
        verticalAlign: 'middle',
        y: 60
    },
    tooltip: {
         headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y:1f} </b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        pie: {
            dataLabels: {
                enabled: true,
                distance: 20,
                style: {
                    fontWeight: 'bold',
                    color: 'black'
                }
            },
            startAngle: -90,
            endAngle: 90,
            center: ['50%', '75%'],
            size: '100%'
        }
    },
    series: [{
        type: 'pie',
        name: 'Polling Personnel',
        innerSize: '50%',
        
        data: <?php echo $gov_pie_data; ?>
    }]
});
Highcharts.chart('pie3', {
    chart: {
        type: 'pie',
        options3d: {
            enabled: true,
            alpha: 45
        }
    },
    title: {
        text: 'Posting Status Wise Polling Personnel'
    },
    
    plotOptions: {
        pie: {
            innerSize: 100,
            depth: 45
        }
    },
    series: [{
        name: 'Polling Personnel',
        data: <?php echo $pstat_pie_data; ?>
    }]
});
Highcharts.chart('bar0', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Subdivision Wise Polling Personnel'
    },
//    subtitle: {
//        text: 'Source: WorldClimate.com'
//    },
    xAxis: {
          min: 0,
            max: <?php echo $subdivision_count; ?>,
        categories: <?php echo $subdivisions; ?>,
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Polling Personnel'
        }
    },
    tooltip: {
        
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y:1f} </b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0
        }
        /* series: {
            stacking: 'normal'
        }*/
    },
    series: <?php echo $subdivision_data_report; ?>
});
Highcharts.chart('bar1', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Institute Wise Polling Personnel'
    },
//    subtitle: {
//        text: 'Source: WorldClimate.com'
//    },
    xAxis: {
          min: 0,
            max: <?php echo $institute_count; ?>,
        categories: <?php echo $institute; ?>,
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Polling Personnel'
        }
    },
    tooltip: {
        
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y:1f} </b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0
        }
        /* series: {
            stacking: 'normal'
        }*/
    },
    series: <?php echo $institution_data; ?>
});
Highcharts.chart('bar2', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Category Wise Polling Personnel'
    },
    
    xAxis: {
          min: 0,
            max: <?php echo $govt_count; ?>,
        categories: <?php echo $govt; ?>,
        crosshair: true
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Polling Personnel'
        }
    },
            
    tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            '<td style="padding:0"><b>{point.y:1f}</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
    },
    plotOptions: {
        column: {
            pointPadding: 0.2,
            borderWidth: 0
        }
    },
    series: <?php echo $govt_report; ?>
});

Highcharts.chart('bar3', {
    chart: {
        type: 'bar'
    },
    title: {
        text: 'Block/Municipality Wise Polling Personnel'
    },
    
    xAxis: {
        min: 0,
            max: <?php echo $block_count; ?>,
        categories: <?php echo $blocks; ?>,
        title: {
            text: 'Block/Municipality'
        }
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Polling Personnel',
            align: 'high'
        },
        labels: {
            overflow: 'justify'
        }
    },
    tooltip: {
        valueSuffix: ' '
    },
    plotOptions: {
       bar: {
            dataLabels: {
                enabled: true,
            }
        },
        
       series: {
            stacking: 'normal'
        }
    },
    legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'top',
        x: -40,
        y: 80,
        floating: true,
        borderWidth: 1,
        backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
        shadow: true
    },
    credits: {
        enabled: false
    },
    series: <?php echo $block_data_report; ?>
});
Highcharts.chart('bar4', {

    chart: {
        type: 'column'
    },

    title: {
        text: 'Polling Personnel After Randomisation'
    },

    xAxis: {
        min: 0,
            max: <?php echo $postat_count; ?>,
        categories: <?php echo $postat_data; ?>
    },

    yAxis: {
        allowDecimals: false,
        min: 0,
        title: {
            text: 'Polling Personnel'
        }
    },

    tooltip: {
        formatter: function () {
            return '<b>' + this.x + '</b><br/>' +
                this.series.name + ': ' + this.y + '<br/>' +
                'Total: ' + this.point.stackTotal;
        }
    },

    plotOptions: {
        column: {
            stacking: 'normal'
        }
    },

    series: [{
        name: 'Required Reserved',
        data: <?php echo $res_r; ?>,
        stack: 'Required'
    },
    {
        name: 'Required Party',
        data: <?php echo $res_p; ?>,
        stack: 'Required'
    },
    {
        name: 'Assigned Reserved',
        data: <?php echo $data_ass_r; ?>,
        stack: 'Assigned'
    }, 
    {
        name: 'Assigned Party',
        data: <?php echo $data_ass_p; ?>,
        stack: 'Assigned'
    }, {
        name: 'Shortfall Reserved',
        data: <?php echo $short_r; ?>,
        stack: 'Shortfall'
    },{
        name: 'Shortfall Party',
        data: <?php echo $short_p; ?>,
        stack: 'Shortfall'
    }]
});
  //  });
<?php }?>
</script>
<?php     
}else{
    ?>
    <script type="text/javascript">
           window.location = "login";
     </script>
 <?php
}
?>

<!-- Copyright -->
@stop
