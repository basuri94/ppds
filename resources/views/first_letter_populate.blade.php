
@extends('layouts.master')
@section('content')

 <div class="wrapper">
<!-- Sidebar Holder -->
@include('layouts.sidebar')

<!-- Page Content Holder -->
<div id="content">           
<section class="tables-section">
    <nav aria-label="breadcrumb" style="width:100%;">
        <div class="breadcrumb pagehead1">
           
                {!! Form::button('<i class="fa fa-desktop"></i> First Appointment Letter Populate',['class' => 'btn btn-primary-header add-new-button']) !!}
            
            <div class="col-md-offset-8 pull-right">
<!--                {!! Form::button('Academic Year:&nbsp;',['id'=>'show_academic','class' => 'btn btn-primary-year']) !!}-->
                
            </div>
            <div class="clearfix"></div>
        </div>
    </nav>

    <!-- form -->
<div class="outer-w3-agile">
  {!! Form::open(['url' => 'first_appt_letter_populate', 'name' => 'first_appt_letter_populate', 'class' =>'request-info clearfix form-horizontal', 'id' => 'first_appt_letter_populate', 'method' => 'post','role'=>'','files' => true]) !!}
  {!! Form::hidden('districtcd', session()->get('districtcd_ppds'),['id'=>'districtcd']) !!}
  
  <div class="panel-group" id="accordion5" style="padding-top: 0px;">                                                              
    <div class="panel panel-default">
     <div class="panel-heading1">
     <h6 class="panel-title">
         <a data-toggle="#" data-parent="#accordion5" href="#collapseUV"> <span class="fa fa-minus"></span> <span class="highlight">Zone-wise Populate</span> </a>
     </h6>
     </div>
     <div id="collapseUV" class="panel-collapse collapse5">
      <div class="panel-body">                                   
        <div class="row"> 
         <div class='col-sm-4'>
               {!! Form::label('zone', 'Zone:', ['class'=>'highlight required']) !!}
              <div class="form-group">
                  <div class=''>
                      {!! Form::select('zone',[''=>'[Select]'],Null,['id'=>'zone','class'=>'form-control','autocomplete'=>'off']) !!}
                  </div>
              </div>
          </div>  
       


           <div class='col-sm-4'>
              {!! Form::label('trainingtype', 'Training Head:', ['class'=>'highlight required']) !!}
              <div class="form-group">
                  <div class=''>
                       {!! Form::select('trainingtype',$training_type['desc'],null,['id'=>'trainingtype','class'=>'form-control','placeholder'=>'[Select]','autocomplete'=>'off']) !!}
                  </div>
              </div>
          </div>
          <div class='col-sm-4'>
            {!! Form::label('phase', 'Phase:', ['class'=>'highlight required']) !!}
           <div class="form-group">
               <div class=''>
                
    <select id="phase" class="form-control" name="phase">
                  
                   <option value="">[Select]</option>
                  

    </select>
                </div>
           </div>
       </div>
            <div class='col-sm-4'>
               {!! Form::label('orderno', 'Order Date & No:', ['class'=>'highlight required']) !!}
              <div class="form-group">
                  <div class=''>
                      {!! Form::select('orderno',[''=>'[Select]'],Null,['id'=>'orderno','class'=>'form-control','autocomplete'=>'off']) !!}
                  </div>
              </div>
          </div>
      </div>      
        
         <div class="row" > 
          <div class='col-sm-12'>                    
            <div class="form-group text-right permit">                            	
                   {{ Form::button('Populate', ['class' => 'btn btn-info', 'type' => 'submit','id'=>'submit']) }}
                   {{ Form::button('Reset', ['class' => 'btn btn-success' ,'type' => 'reset','id'=>'reset']) }}
            </div>
          </div>
          
       </div>
      </div>
    </div>                              
  </div> 
 
  
  </div>

 
 {!! Form::close() !!}             
</div>
<!--// form -->


</section>

</div>
</div>
<!-- Copyright -->
<script type="text/javascript">
$(document).ready(function () {
 getZoneDetails();
$('select[name="zone"]').on('change', function () {
    getZonePhasedata(); 
});
$("#reset").click(function () {
  location.reload(true);  
});
  $('#first_appt_letter_populate')
            .bootstrapValidator({
            message: 'This value is not valid',
            feedbackIcons: {
                    valid: 'fa fa-check',
                    invalid: 'fa fa-times',
                    validating: 'fa fa-refresh'
            },
                fields: {
                    zone: {
                        validators: {
                            notEmpty: {
                                message: 'Zone is required'
                            }
                        }
                    },
                    orderno: {
                        validators: {
                            notEmpty: {
                                message: 'Order Date & No is required'
                            }
                        }
                    },

                    trainingtype: {
                        validators: {
                            notEmpty: {
                                message: 'Training Head is required'
                            }
                        }
                    },
                    phase: {
                        validators: {
                            notEmpty: {
                                message: 'Phase is required'
                            }
                        }
                    }             
                }
            }).on('success.form.bv', function (e) {
           // Prevent form submission
            e.preventDefault();
            var action = $(this).attr('action');
            var type = $(this).attr('method');
            var formData = new FormData($(this)[0]);
            // var operation = "save";
            //  var operated = "saved";
            $(".se-pre-con1").fadeIn("slow");
            $.ajax({
            type: type,
                    url: action,
                    data: formData,
                    processData: false,
                    contentType: false,
                    dataType: "json",
                    success: function (data) {

                    if(data.status==1)
                    {
                      $(".se-pre-con1").fadeOut("slow");            
                      var msg=data.options+" Record(s) populated successfully";
                       $.confirm({
                            title: 'Success!',
                            type: 'green',
                            icon: 'fa fa-check',
                            content: msg,
                            buttons: {
                                ok: function () {
                                   // location.reload(true);
                                  // getZoneWiseAssembly();
                                  location.reload(true);
                                  
                                }

                            }
                        });
                      }              

                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        $(".se-pre-con1").fadeOut("slow");
                        var msg = "";
                        if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                            msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                        } else {
                            if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                                msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                            } else {
                                msg += "Error(s):<strong><ul>";
                                $.each(jqXHR.responseJSON, function (key, value) {
                                    msg += "<li>" + value + "</li>";
                                });
                                msg += "</ul></strong>";
                            }
                        }
                        $.alert({
                            title: 'Error!!',
                            type: 'red',
                            icon: 'fa fa-warning',
                            content: msg
                        });
                    }
            });
 
     });
 
});
function getZoneDetails(){
    var forDist = $("#districtcd").val();
    var token = $("input[name='_token']").val();
    $(".se-pre-con").fadeIn("slow");
    $.ajax({
       url: "getZoneDetails",
       method: 'POST',
       data: {forDist: forDist, _token: token},
       success: function (data) {//alert(data.options);
         $(".se-pre-con").fadeOut("slow");
          $("select[name='zone'").html('');
          if(data.status==1)
          {
             $("select[name='zone'").append('<option value>[Select]</option>');
            $.each(data.options,function (k, v)
            {
                $("select[name='zone'").append('<option value='+k+'>'+v+'</option>');
            });
           // getMemberDetails();
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
            $(".se-pre-con").fadeOut("slow");
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-warning',
                  content: msg
              });
         },complete: function(){
          getOrderDetails();
         
         }
   });
 }
 function getOrderDetails(){
    var forDist = $("#districtcd").val();
    var token = $("input[name='_token']").val();
    //$(".se-pre-con").fadeIn("slow");
    $.ajax({
       url: "getOrderDetails",
       method: 'POST',
       data: {forDist: forDist, _token: token},
       success: function (data) {//alert(data.options);
        // $(".se-pre-con").fadeOut("slow");
          $("select[name='orderno'").html('');
          if(data.status==1)
          {
              $("select[name='orderno'").html(data.options);
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
            $(".se-pre-con").fadeOut("slow");
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-warning',
                  content: msg
              });
         }
   });
 }
 function change_subdivision(){
  var zone = $("#zone").val();
   var token = $("input[name='_token']").val();
  // $(".se-pre-con").fadeIn("slow");
   $.ajax({
       url: "getZonewiseSubdivisionData",
       method: 'POST',
       data: {zone: zone, _token: token},
       success: function (data) {//alert(data.options);
       //   $(".se-pre-con").fadeOut("slow");
          $("select[name='subdivision'").html('');
          
          if(data.status==1)
          {
            $("select[name='subdivision'").append('<option value>[Select]</option>');
            $.each(data.options,function (k, v)
            {
                $("select[name='subdivision'").append('<option value='+k+'>'+v+'</option>');
            });
            //$("select[name='subdivision'").html(data.options);
             // change_officeName();
            // getMemberDetails();
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
         $(".se-pre-con").fadeOut("slow");
           var msg = "";
           if (jqXHR.status !== 422 && jqXHR.status !== 400) {
               msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
           } else {
               if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                   msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
               } else {
                   msg += "Error(s):<strong><ul>";
                   $.each(jqXHR.responseJSON, function (key, value) {
                       msg += "<li>" + value + "</li>";
                   });
                   msg += "</ul></strong>";
               }
           }
           $.alert({
               title: 'Error!!',
               type: 'red',
               icon: 'fa fa-exclamation-triangle',
               content: msg
           });
         }
   });
}
function getZonePhasedata(){
  var zone = $("#zone").val();
  $("#rw").hide();
   var token = $("input[name='_token']").val();
  // $(".se-pre-con").fadeIn("slow");
   $.ajax({
       url: "getZonePhasedata",
       method: 'POST',
       data: {zone: zone, _token: token},
       success: function (data) {//alert(data.options);
       //   $(".se-pre-con").fadeOut("slow");
          $("select[name='phase'").html('');
          
          if(data.status==1)
          {
            $("select[name='phase'").append('<option value>[Select]</option>');
            $.each(data.options,function (k, v)
            {
                $("select[name='phase'").append('<option value='+k+'>'+v+'</option>');
            });
            //$("select[name='subdivision'").html(data.options);
             // change_officeName();
             
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
       //  $(".se-pre-con").fadeOut("slow");
           var msg = "";
           if (jqXHR.status !== 422 && jqXHR.status !== 400) {
               msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
           } else {
               if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                   msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
               } else {
                   msg += "Error(s):<strong><ul>";
                   $.each(jqXHR.responseJSON, function (key, value) {
                       msg += "<li>" + value + "</li>";
                   });
                   msg += "</ul></strong>";
               }
           }
           $.alert({
               title: 'Error!!',
               type: 'red',
               icon: 'fa fa-warning',
               content: msg
           });
         },complete: function(){
          getZonePhaseWiseAssembly();
         }
   });
}
</script>
@stop

