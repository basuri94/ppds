@extends('layouts.master')
@section('content')
<style>
.p_note{
 padding-left: 10px;
 padding-right: 10px;
 padding-top: 10px;
  font-size: .9rem;
  line-height: 1.5;
  color: #f85a40;
  background-color: #fff;
}
</style>
<!--// top-bar -->
 <div class="wrapper">
<!-- Sidebar Holder -->
@include('layouts.sidebar')

<!-- Page Content Holder -->
<div id="content">           
<section class="tables-section">
    <nav aria-label="breadcrumb" style="width:100%;">
         <div class="breadcrumb pagehead1">
           <span class="headbrand"><i class="fa fa-desktop"></i> Pre Group Cancellation (Excel Upload)
            <span class="scoop-mcaret1"></span>
           </span>
           <div class="clearfix"></div>
        </div>
    </nav>

    <!-- form -->
<div class="outer-w3-agile">
  {!! Form::open(['url' => 'preGroupCancellationExcel', 'name' => 'preGroupCancellationExcel', 'class' =>'request-info clearfix form-horizontal', 'id' => 'preGroupCancellationExcel', 'method' => 'post','role'=>'','files' => true]) !!}
  {!! Form::hidden('districtcd', session()->get('districtcd_ppds'),['id'=>'districtcd']) !!}
  
  <div class="panel-group" id="accordion5">                                                              
   <div class="panel panel-default">
    <div class="panel-heading1">
     <h6 class="panel-title">
         <a data-toggle="#" data-parent="#accordion5" href="#collapsePV"> <span class="fa fa-minus"></span> <span class="highlight">Pre Group Cancellation (Excel Upload)</span></a>
     </h6>
    </div>
    <div class="row"> 
        <div class='col-sm-12'>
            <div class="form-group">
                <div class="p_note">Note:<br> 1. File should be an excel format(.xlsx).<br>
                           2. File column value should follow the following format:<br> &nbsp;&nbsp;&nbsp;a)Personnel ID(mandatory), b)Reason (mandatory)<br>
                           3. File column header name should be same.<br>
                           4. <span style="font-size:14px;cursor: pointer;color: blue;"><a id="excel-download"><i class="fa fa-download"></i> Download</a></span> excel format to upload the file .
                </div>
              </div>
        </div>        
    </div>
    <div id="collapsePV" class="panel-collapse collapse5">
      <div class="panel-body"> 
          <div class="row"> 
         <div class='col-sm-4'>
               {!! Form::label('zone', 'Zone:', ['class'=>'highlight required']) !!}
              <div class="form-group">
                  <div class=''>
                      {!! Form::select('zone',[''=>'[Select]'],Null,['id'=>'zone','class'=>'form-control','autocomplete'=>'off']) !!}
                  </div>
              </div>
          </div>
          <div class='col-sm-4'>
            {!! Form::label('phase', 'Phase:', ['class'=>'highlight required']) !!}
           <div class="form-group">
               <div class=''>
                
    <select id="phase" class="form-control" name="phase">
                  
                   <option value="">[Select]</option>
                  

    </select>
                </div>
           </div>
       </div>
        <div class='col-sm-4'>
               {!! Form::label('exceldata', 'Upload file:', ['class'=>'highlight required']) !!}
              <div class="form-group">
                  <div class=''>
                      {!! Form::file('excelup',['id'=>'excelup','class'=>'form-control','autocomplete'=>'off']) !!}
                  </div>
              </div>
          </div>     
        </div>
       
          
 
          
<!--      <div class="row"> 
            <div class='col-sm-12'>
                <div class="form-group">
                     <div id='subVenueDetails'></div>
                  </div>
            </div>
         
      </div> -->
      <div class="row">                  
         <div class='col-sm-12'>
            <div class="form-group text-right permit">                            	
               {{ Form::button('Submit', ['class' => 'btn btn-info', 'type' => 'submit','id'=>'Submit']) }}
               {{ Form::button('Reset', ['class' => 'btn btn-success' ,'type' => 'reset','id'=>'reset']) }}
            </div>
          </div>
      </div>     
       
    </div>                              
   </div>
  </div>  
  </div>
 {!! Form::close() !!}             
</div>
<!--// form -->

</section>

</div>
</div>
<!-- Copyright -->
<script type="text/javascript">
$(document).ready(function () {
     getZoneDetails(); 
     $("#zone").change(function(){
  
    getZonePhasedata(); 
 });

$("#reset").click(function () {
  location.reload(true);  
});
$("#excel-download").click(function(){
    var datas = {'_token': $('input[name="_token"]').val()};
    redirectPost('{{url("excelFormatForCancellation")}}', datas);

});

$('#preGroupCancellationExcel').bootstrapValidator({

        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            excelup: {
                validators: {
                    notEmpty: {
                        message: 'Please Select File'
                    },
                    file: {
                        extension: 'xlsx',
                        message: 'Excel file should be in EXCEL format'
                    }
                }
            },
             zone: {
                validators: {
                    notEmpty: {
                        message: 'Please Select Zone'
                    }
                   
                }
            },
            phase: {
                validators: {
                    notEmpty: {
                        message: 'Please Select Phase'
                    }
                   
                }
            }
        }

    }).on('success.form.bv', function (e) {
        e.preventDefault();
        var action = $(this).attr('action');
        var type = $(this).attr('method');
        var formData = new FormData($(this)[0]);
        $(".se-pre-con").fadeIn("slow");
        $.ajax({
            type: type,
            url: action,
            data: formData,
            processData: false,
            contentType: false,
            dataType: "json",
            success: function (data) {
                $(".se-pre-con").fadeOut("slow");

                if (data.status == 1) {        
                     //var msg=data.options+" Record(s) saved successfully";
                     $.confirm({
                        title: 'Success!',
                        type: 'green',
                        icon: 'fa fa-check',
                        content: data.options,
                        buttons: {
                            ok: function () {     
                                location.reload(true);
                            }

                        }
                    });

                } else {
                   $.alert({
                        title: 'Error!!',
                        type: 'red',
                        icon: 'fa fa-exclamation-triangle',
                        content: data.options
                    });
                    $("#Submit").attr('disabled',false);  
                }
                $("#Submit").attr('disabled',false);
            },
            error: function (jqXHR, textStatus, errorThrown) {
            $(".se-pre-con").fadeOut("slow");
           // var arr = $.map(jqXHR.responseJSON, function(el) { return el });
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-exclamation-triangle',
                  content: msg
              });
              $("#Submit").attr('disabled',false);
           }
        });
    });
});

function getZoneDetails(){
    var forDist = $("#districtcd").val();
    var token = $("input[name='_token']").val();
    $(".se-pre-con").fadeIn("slow");
    $.ajax({
       url: "getZoneDetails",
       method: 'POST',
       data: {forDist: forDist, _token: token},
       success: function (data) {//alert(data.options);
         $(".se-pre-con").fadeOut("slow");
          $("select[name='zone'").html('');
          if(data.status==1)
          {
             $("select[name='zone'").append('<option value>[Select]</option>');
            $.each(data.options,function (k, v)
            {
                $("select[name='zone'").append('<option value='+k+'>'+v+'</option>');
            });
           // getMemberDetails();
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
            $(".se-pre-con").fadeOut("slow");
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-warning',
                  content: msg
              });
         }
   });
 }
 function getZonePhasedata(){
  var zone = $("#zone").val();
  $("#rw").hide();
   var token = $("input[name='_token']").val();
  // $(".se-pre-con").fadeIn("slow");
   $.ajax({
       url: "getZonePhasedata",
       method: 'POST',
       data: {zone: zone, _token: token},
       success: function (data) {//alert(data.options);
       //   $(".se-pre-con").fadeOut("slow");
          $("select[name='phase'").html('');
          
          if(data.status==1)
          {
            $("select[name='phase'").append('<option value>[Select]</option>');
            $.each(data.options,function (k, v)
            {
                $("select[name='phase'").append('<option value='+k+'>'+v+'</option>');
            });
            //$("select[name='subdivision'").html(data.options);
             // change_officeName();
             
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
       //  $(".se-pre-con").fadeOut("slow");
           var msg = "";
           if (jqXHR.status !== 422 && jqXHR.status !== 400) {
               msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
           } else {
               if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                   msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
               } else {
                   msg += "Error(s):<strong><ul>";
                   $.each(jqXHR.responseJSON, function (key, value) {
                       msg += "<li>" + value + "</li>";
                   });
                   msg += "</ul></strong>";
               }
           }
           $.alert({
               title: 'Error!!',
               type: 'red',
               icon: 'fa fa-warning',
               content: msg
           });
         },complete: function(){
          getZonePhaseWiseAssembly();
         }
   });
}
</script>
@stop