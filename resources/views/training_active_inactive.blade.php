@extends('layouts.master')
@section('content')

 <div class="wrapper">
<!-- Sidebar Holder -->
@include('layouts.sidebar')

<!-- Page Content Holder -->
<div id="content">           
<section class="tables-section">
    <nav aria-label="breadcrumb" style="width:100%;">
        <div class="breadcrumb pagehead1">
           
                {!! Form::button('<i class="fa fa-desktop"></i> First Training Active/Inactive',['class' => 'btn btn-primary-header add-new-button']) !!}
            
          
                <div class="clearfix"></div>
        </div>
    </nav>

    <!-- form -->
<div class="outer-w3-agile">
  {!! Form::open(['url' => '', 'name' => 'trainingTypeActiveInactive', 'class' =>'request-info clearfix form-horizontal', 'id' => 'trainingTypeActiveInactive', 'method' => 'post','role'=>'','files' => true]) !!}
  {!! Form::hidden('districtcd', session()->get('districtcd_ppds'),['id'=>'districtcd']) !!}
  
  
   <div class="panel-group" id="accordion5">                                                              
    <div class="panel panel-default">
     <div class="panel-heading1">
     <h6 class="panel-title">
         <a data-toggle="#" data-parent="#accordion5" href="#collapseUV"> <span class="fa fa-minus"></span> <span class="highlight">First Training Active/Inactive</span></a>
     </h6>
     </div>
     <div id="collapseUV" class="panel-collapse collapse5">
      <div class="panel-body">                                   
     
        <div class="row"> 
            <div class='col-sm-12'>
                <div id='trainingType'></div>
            </div>
        </div> 
        <div class="row" id="rw" style="display: none;">                                   
           <div class='col-sm-4'>
               {!! Form::label('status', 'Status:', ['class'=>'highlight required']) !!}
              <div class="form-group">
                  <div class=''>
                      {!! Form::select('status',[''=>'[Select]','1'=>'Active','0'=>'In Active'],Null,['id'=>'status','class'=>'form-control','autocomplete'=>'off']) !!}
                  </div>
              </div>
          </div>
       
           <div class='col-sm-4' > 
               {!! Form::label('', '', ['class'=>'highlight']) !!}
                <div class="form-group text-right permit" >                            	
                   {{ Form::button('Create', ['class' => 'btn btn-info', 'type' => 'button','id'=>'create']) }}
                   {{-- {{ Form::button('Delete', ['class' => 'btn btn-info', 'type' => 'button','id'=>'delete']) }} --}}
                </div>
           </div>
                
        </div>  
      
          
       </div>
      </div>
    </div>                              
  </div> 
 
 {!! Form::close() !!}             
</div>
<!--// form -->


</section>

</div>
</div>
<!-- Copyright -->
<script type="text/javascript">
$(document).ready(function () {
    getTrainingTypeActiveInactiveDetails();
  $("#create").click(function(){
        var formData = new FormData($("#trainingTypeActiveInactive")[0]);
      
       
   
        $(".se-pre-con").fadeIn("slow");
        $.ajax({
        type: "post",
        url: "changeTrainingTypeStatus",
        data: formData,
        processData: false,
        contentType: false,
        dataType: 'json',
        success: function (data) { console.log(data)
           if(data.status==1)
           {
                $(".se-pre-con").fadeOut("slow"); 
               
               
                $.confirm({
                     title: 'Success!',
                     type: 'green',
                     icon: 'fa fa-check',
                     content: data.msg,
                     buttons: {
                         ok: function () {
                             //location.reload(true);
                             getTrainingTypeActiveInactiveDetails();
                             $("#rw").hide();
                         }

                     }
                 });
           }
       },
        error: function (jqXHR, textStatus, errorThrown) {
            $(".se-pre-con").fadeOut("slow");
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-exclamation-triangle',
                  content: msg
              });
         }
     });
  });

  
  
});
function getTrainingTypeActiveInactiveDetails(){
   
    var token = $("input[name='_token']").val();
   // $(".se-pre-con").fadeIn("slow");
    $.ajax({
        type: 'POST',
        url: 'getTrainingTypeActiveInactive',
        data: { _token:token},
        dataType: 'json',
        success: function (response) {
       // $(".se-pre-con").fadeOut("slow");
        $("#trainingType").html("");

        $("#trainingType").append(response.options);
        
          $("#myCheckall").change(function(){ 
            var status = this.checked;
            if(status==true){
                $("#rw").show();
                scrollToElement($('#rw'));
            }else if(status==false){
                $("#rw").hide();
            }
            
            $('.submenu').each(function(){
                this.checked = status;     
            });
        });
        $('.submenu').change(function(){
            if($('.submenu:checked').length=='0'){
                $("#rw").hide();
            }else{
                $("#rw").show();
            }
            if(this.checked == false){
                $("#myCheckall")[0].checked = false; 
            }
            if ($('.submenu:checked').length == $('.submenu').length ){ 
                $("#myCheckall")[0].checked = true;
            }
        });
     
        },
        error: function (jqXHR, textStatus, errorThrown) {
           // $(".se-pre-con").fadeOut("slow");
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-exclamation-triangle',
                  content: msg
              });
         }
    });
}

 
 function scrollToElement(ele) {
    $('html, body').animate({
        scrollTop: ele.offset().top - 60
    }, 1000);
}
</script>
@stop
