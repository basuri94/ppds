@extends('layouts.master')
@section('content')
<div class="wrapper">
    @include('layouts.sidebar')
    <div id="content">           
        <section class="tables-section">
            <nav aria-label="breadcrumb" style="width:100%;">
                <div class="breadcrumb pagehead1">
                        {!! Form::button('<i class="fa fa-desktop"></i> Zone And Phase Wise After Group Training Populate Checking',['class' => 'btn btn-primary-header add-new-button']) !!}
                        <div class="clearfix"></div>
                </div>
            </nav>
            <div class="outer-w3-agile">
                {!! Form::open(['url' => 'group_tr_allocation_checking', 'name' => 'group_tr_allocation_checking', 'class' =>'request-info clearfix form-horizontal', 'id' => 'group_tr_allocation_checking', 'method' => 'post','role'=>'','files' => true]) !!}
                {!! Form::hidden('districtcd', session()->get('districtcd_ppds'),['id'=>'districtcd']) !!}
                <div class="panel-group" id="accordion5">                                                              
                    <div class="panel panel-default">
                     <div class="panel-heading1">
                     <h6 class="panel-title">
                         <a data-toggle="#" data-parent="#accordion5" href="#collapseUV"> <span class="fa fa-minus"></span> <span class="highlight">Zone And Phase Wise After Group Training Populate Checking</span> </a>
                     </h6>
                     </div>
                     <div id="collapseUV" class="panel-collapse collapse5">
                      <div class="panel-body">                                   
                        <div class="row">                                   
                           {{-- <div class='col-sm-4'>  --}}
                            <div class='col-sm-3'>
                               {!! Form::label('zone', 'Zone:', ['class'=>'highlight required']) !!}
                              <div class="form-group">
                                  <div class=''>
                                      {!! Form::select('zone',[''=>'[Select]'],Null,['id'=>'zone','class'=>'form-control','autocomplete'=>'off']) !!}
                                  </div>
                              </div>
                         </div> 

                          {{-- <div class='col-sm-3'>
                            {!! Form::label('subdivision', 'Sub Division:', ['class'=>'highlight required']) !!}
                           <!--<span class="highlight">Sub Division</span> -->
                           <div class="form-group">
                               <div class=''>
                                   {!! Form::select('subdivision',[''=>'[Select]'],null,['id'=>'subdivision','class'=>'form-control','autocomplete'=>'off']) !!}
                               </div>
                           </div>
                       </div> --}}
                       
                          <div class='col-sm-3'>
                            {!! Form::label('phase', 'Phase:', ['class'=>'highlight required']) !!}
                           <div class="form-group">
                               <div class=''>
                                    <select id="phase" class="form-control" name="phase">
                                                <option value="">[Select]</option>
                                    </select>
                                </div>
                           </div>
                       </div>

                       {{-- <div class='col-sm-3'>
                        {!! Form::label('assembly', 'Assembly:', ['class'=>'highlight required']) !!}

                        <div class="form-group">
                       @php
                       $assemblydata=App\tbl_assembly::select('assemblyname','assemblycd')->where('districtcd',session()->get('districtcd_ppds'))->get();
                    @endphp
                       <select class="form-control" id="assembly" name="assembly">  
                        <option value="">[Select]</option>
                    @foreach ($assemblydata as $item)
                        <option value="{{$item->assemblycd}}">{{$item->assemblycd}} - {{$item->assemblyname}}</option>
                    @endforeach

                    </select>
                </div>
                    </div>  --}}
                    </div> 
                     
                       <div class="row"> 
                        <div class='col-sm-12'>
                            <div id='assemblyDetails'></div>
                        </div>
                    </div>
 
                    {{-- <div class="row" id="rw" style="display: none;"> 
                        <div class='col-sm-12'>                    
                         <div class="form-group text-right permit">  
                            {{ Form::button('EXCEL', ['class' => 'btn btn-info', 'type' => 'button','id'=>'EXCEL']) }}
                            {{ Form::button('Reset', ['class' => 'btn btn-success' ,'type' => 'reset','id'=>'reset']) }}
                         </div>
                       </div>
                     </div> --}}
                    {!! Form::close() !!}         
                </div>
            </div>
        </div>                              
    </div> 
                           
                </div>
            </section>
</div>
</div>
<script>
    $(document).ready(function () {
        getZoneDetails();
        // $("#zone").change(function(){
        //     getZonePhasedata();
        // //    change_subdivision();
        // });
        // change_subdivision(); 
        $('select[name="zone"]').on('change', function () {    
            getZonePhasedata();
        });


        // $("select[name='party_reserve'").append('<option value="P">Party</option>','<option value="R">Reserve</option>');
        
        $("#phase").change(function(){
            var zone = $("#zone").val();
            var phase = $("#phase").val();
            // var subdivison = $("#subdivision").val();
          
            if(zone!='' && phase!=''){
                subdivision_wise_traning_checking();
                $("#rw").show();
            } else{
                $("#assemblyDetails").html("");
          
            }
        });

        $('#EXCEL').click(function () {
            var zone = $("#zone").val();
            var phase = $("#phase").val();
            if(zone==""){
                    $.alert({
                            title: 'Error!!',
                            type: 'red',
                            icon: 'fa fa-exclamation-triangle',
                            content: 'For Zone is required'
                        });
                        return false;
                 }
                 if(phase==""){
                    $.alert({
                            title: 'Error!!',
                            type: 'red',
                            icon: 'fa fa-exclamation-triangle',
                            content: 'Phase is required'
                        });
                        return false;
                 }

                 var datas = {zone: zone,phase:phase,'_token': $('input[name="_token"]').val()};
              
                 redirectPost('{{url("getAfterPopulateRecordExcel")}}', datas);
        });
       

    });

    function change_subdivision(){
  var forDist = $("#districtcd").val();
   var token = $("input[name='_token']").val();
  // $(".se-pre-con").fadeIn("slow");
   $.ajax({
       url: "getSubdivisionData",
       method: 'POST',
       data: {forDist: forDist, _token: token},
       success: function (data) {//alert(data.options);
       //   $(".se-pre-con").fadeOut("slow");
          $("select[name='subdivision'").html('');
          
          if(data.status==1)
          {
            $("select[name='subdivision'").append('<option value>[Select]</option>');
            $.each(data.options,function (k, v)
            {
                $("select[name='subdivision'").append('<option value='+k+'>'+v+'</option>');
            });
            //$("select[name='subdivision'").html(data.options);
             // change_officeName();
            // getMemberDetails();
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
         $(".se-pre-con").fadeOut("slow");
           var msg = "";
           if (jqXHR.status !== 422 && jqXHR.status !== 400) {
               msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
           } else {
               if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                   msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
               } else {
                   msg += "Error(s):<strong><ul>";
                   $.each(jqXHR.responseJSON, function (key, value) {
                       msg += "<li>" + value + "</li>";
                   });
                   msg += "</ul></strong>";
               }
           }
           $.alert({
               title: 'Error!!',
               type: 'red',
               icon: 'fa fa-exclamation-triangle',
               content: msg
           });
         }
        //  complete: function () {
        //     getTrainingDateTime();
            
        // }
   });
}


    function getZoneDetails(){
    var forDist = $("#districtcd").val();
    var token = $("input[name='_token']").val();
    $(".se-pre-con").fadeIn("slow");
    $.ajax({
       url: "getZoneDetails",
       method: 'POST',
       data: {forDist: forDist, _token: token},
       success: function (data) {//alert(data.options);
         $(".se-pre-con").fadeOut("slow");
          $("select[name='zone'").html('');
          if(data.status==1)
          {
             $("select[name='zone'").append('<option value>[Select]</option>');
            $.each(data.options,function (k, v)
            {
                $("select[name='zone'").append('<option value='+k+'>'+v+'</option>');
            });
           // getMemberDetails();
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
            $(".se-pre-con").fadeOut("slow");
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-warning',
                  content: msg
              });
         },
   });
 }
 function getZonePhasedata(){
  var zone = $("#zone").val();
//   $("#rw").hide();
   var token = $("input[name='_token']").val();
  // $(".se-pre-con").fadeIn("slow");
   $.ajax({
       url: "getZonePhasedata",
       method: 'POST',
       data: {zone: zone, _token: token},
       success: function (data) {//alert(data.options);
       //   $(".se-pre-con").fadeOut("slow");
          $("select[name='phase'").html('');
          
          if(data.status==1)
          {
            $("select[name='phase'").append('<option value>[Select]</option>');
            $.each(data.options,function (k, v)
            {
                $("select[name='phase'").append('<option value='+k+'>'+v+'</option>');
            });
            //$("select[name='subdivision'").html(data.options);
             // change_officeName();
             
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
       //  $(".se-pre-con").fadeOut("slow");
           var msg = "";
           if (jqXHR.status !== 422 && jqXHR.status !== 400) {
               msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
           } else {
               if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                   msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
               } else {
                   msg += "Error(s):<strong><ul>";
                   $.each(jqXHR.responseJSON, function (key, value) {
                       msg += "<li>" + value + "</li>";
                   });
                   msg += "</ul></strong>";
               }
           }
           $.alert({
               title: 'Error!!',
               type: 'red',
               icon: 'fa fa-warning',
               content: msg
           });
         },
   });
}

function getSubdivisionPhasedata(){
 // var zone = $("#zone").val();
  var subdivision = $("#subdivision").val();
   var token = $("input[name='_token']").val();
  // $(".se-pre-con").fadeIn("slow");
   $.ajax({
       url: "getSubdivisionPhasedata",
       method: 'POST',
       data: {subdivision:subdivision, _token: token},
       success: function (data) {//alert(data.options);
       //   $(".se-pre-con").fadeOut("slow");
          $("select[name='phase'").html('');
          
          if(data.status==1)
          {
            $("select[name='phase'").append('<option value>[Select]</option>');
            $.each(data.options,function (k, v)
            {
                $("select[name='phase'").append('<option value='+k+'>'+v+'</option>');
            });
            //$("select[name='subdivision'").html(data.options);
             // change_officeName();
             
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
       //  $(".se-pre-con").fadeOut("slow");
           var msg = "";
           if (jqXHR.status !== 422 && jqXHR.status !== 400) {
               msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
           } else {
               if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                   msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
               } else {
                   msg += "Error(s):<strong><ul>";
                   $.each(jqXHR.responseJSON, function (key, value) {
                       msg += "<li>" + value + "</li>";
                   });
                   msg += "</ul></strong>";
               }
           }
           $.alert({
               title: 'Error!!',
               type: 'red',
               icon: 'fa fa-warning',
               content: msg
           });
           
         },
       
   });
}






// function change_subdivision(){
//   var zone = $("#zone").val();
//   var forDist = $("#districtcd").val();
//    var token = $("input[name='_token']").val();
//   $(".se-pre-con").fadeIn("slow");
//    $.ajax({
//        url: "getZonewiseSubdivisionall_Data",
//        method: 'POST',
//        data: {zone: zone,forDist: forDist, _token: token},
//        success: function (data) {//alert(data.options);
//           $(".se-pre-con").fadeOut("slow");
//           $("select[name='subdivision'").html('');
          
//           if(data.status==1)
//           {
//             $("select[name='subdivision'").append('<option value>[Select]</option>');
//             $.each(data.options,function (k, v)
//             {
//                 $("select[name='subdivision'").append('<option value='+k+'>'+v+'</option>');
//             });
//             //$("select[name='subdivision'").html(data.options);
//              // change_officeName();
//             // getMemberDetails();
//           }
//        },
//         error: function (jqXHR, textStatus, errorThrown) {
//          $(".se-pre-con").fadeOut("slow");
//            var msg = "";
//            if (jqXHR.status !== 422 && jqXHR.status !== 400) {
//                msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
//            } else {
//                if (jqXHR.responseJSON.hasOwnProperty('exception')) {
//                    msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
//                } else {
//                    msg += "Error(s):<strong><ul>";
//                    $.each(jqXHR.responseJSON, function (key, value) {
//                        msg += "<li>" + value + "</li>";
//                    });
//                    msg += "</ul></strong>";
//                }
//            }
//            $.alert({
//                title: 'Error!!',
//                type: 'red',
//                icon: 'fa fa-exclamation-triangle',
//                content: msg
//            });
//          },
//         complete: function () {
//             other_subdivision();
//             getZonePhasedata(); 
//         }
//    });
// }

function subdivision_wise_traning_checking() {
   
    var zone = $("#zone").val();
    var phase = $("#phase").val();
    // var assembly = $("#assembly").val();
   
    
    var token = $("input[name='_token']").val();
    $(".se-pre-con").fadeIn("slow");
    $.ajax({
        type: 'POST',
        url: 'group_tr_after_populate',
      
        data: {zone:zone,phase:phase,_token:token},
        dataType: 'json',
        success: function (response) {
        $(".se-pre-con").fadeOut("slow");
        $("#assemblyDetails").html("");
        var str = "";
        str+= "<table class='table table-bordered table-striped' width='100%' style='border-top: 2px solid #4cae4c;' id='dvData'>";
       
        str += ' <tr style="background-color: #f5f8fa"><th rowspan="2" style="width: 15%;">&nbsp;Assembly </th><th colspan="5" scope="colgroup">&nbsp;After Group Training Populate</th></tr>';
        str += '<tr style="background-color: #f5f8fa"><th>P1[P + R]</th><th>P2[P + R]</th><th>P3[P + R]</th><th>PR[P + R]</th><th>Total[P + R]</th></tr>';
        str +=response.requirement
      
        $("#assemblyDetails").append(str);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $(".se-pre-con").fadeOut("slow");
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-warning',
                  content: msg
              });
         }
   
    });
}

</script>

@stop
