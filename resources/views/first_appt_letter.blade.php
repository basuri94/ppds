
@extends('layouts.master')
@section('content')

 <!--// top-bar -->
 <div class="wrapper">
<!-- Sidebar Holder -->
@include('layouts.sidebar')

<!-- Page Content Holder -->
<div id="content">           
<section class="tables-section">
    <nav aria-label="breadcrumb" style="width:100%;">
        <div class="breadcrumb pagehead1">
           
                {!! Form::button('<i class="fa fa-desktop"></i> First Appointment Letter',['class' => 'btn btn-primary-header add-new-button']) !!}
            
            <div class="col-md-offset-8 pull-right">
<!--                {!! Form::button('Academic Year:&nbsp;',['id'=>'show_academic','class' => 'btn btn-primary-year']) !!}-->
                
            </div>
                <div class="clearfix"></div>
        </div>
    </nav>

    <!-- form -->
<div class="outer-w3-agile">
  {!! Form::open(['url' => 'getFirstAppointmentLetterPDF', 'name' => 'getFirstAppointmentLetterPDF', 'class' =>'request-info clearfix form-horizontal', 'id' => 'getFirstAppointmentLetterPDF', 'method' => 'post','role'=>'','files' => true]) !!}
  {!! Form::hidden('districtcd', session()->get('districtcd_ppds'),['id'=>'districtcd']) !!}
  {!! Form::hidden('subdivcd', session()->get('subdivisioncd_ppds'),['id'=>'subdivcd']) !!}
  {!! Form::hidden('blockcd', session()->get('blockmunicd_ppds'),['id'=>'blockcd']) !!}
  
<!--  <span class="highlight">[Note: Record(s) transfered from Personnel to Zonewise]</span>-->
   <div class="panel-group" id="accordion5">                                                              
    <div class="panel panel-default">
     <div class="panel-heading1">
     <h6 class="panel-title">
         <a data-toggle="#" data-parent="#accordion5" href="#collapseUV"> <span class="fa fa-minus"></span><span class="highlight"> District-wise,Subdivision-wise, Block/Municipality-wise, Office-wise,Personnel Id-wise </span></a>
     </h6>
     </div>
     <div id="collapseUV" class="panel-collapse collapse5">
      <div class="panel-body">                                   
        <div class="row"> 
          
           <div class='col-sm-4'>
               {!! Form::label('subdivision', 'Sub Division:', ['class'=>'highlight']) !!}
              <div class="form-group">
                  <div class=''>
                      {!! Form::select('subdivision',[''=>'[Select]'],null,['id'=>'subdivision','class'=>'form-control','autocomplete'=>'off']) !!}
                  </div>
              </div>
          </div>
            <div class='col-sm-4'>
              {!! Form::label('trainingtype', 'Training Head:', ['class'=>'highlight']) !!}
              <div class="form-group">
                  <div class=''>
                       {!! Form::select('trainingtype',$training_type['desc'],null,['id'=>'trainingtype','class'=>'form-control','placeholder'=>'[Select]','autocomplete'=>'off']) !!}
                  </div>
              </div>
           </div>
           <div class='col-sm-4'>
            {!! Form::label('phase', 'Phase:', ['class'=>'highlight ']) !!}
           <div class="form-group">
               <div class=''>
                
    <select id="phase" class="form-control" name="phase">
                  @php
                      $phase_data=App\Phase::get();
                  @endphp
                  <option value="">[Select]</option>
                  @foreach ($phase_data as $item)
                  <option value="{{ $item->code}}">{{$item->name}}</option>
                  @endforeach
                   
                  

    </select>
                </div>
           </div>
       </div>
          
          
            
          
            
         </div>
          
          <div class="row"> 
            <div class='col-sm-4'>
                {!! Form::label('blockMunicipality', 'Block/Municipality:', ['class'=>'highlight']) !!}
                <div class="form-group">
                    <div class=''>
                        {!! Form::select('blockMunicipality',[''=>'[Select]'],null,['id'=>'blockMunicipality','class'=>'form-control','autocomplete'=>'off']) !!}
                    </div>
                </div>
            </div>
            <div class='col-sm-4'>
              {!! Form::label('officeName', 'Office Name:', ['class'=>'highlight']) !!}
              <div class="form-group">
                  <div class=''>
                      {!! Form::select('officeName',[''=>'[Select]'],null,['id'=>'officeName','class'=>'form-control','autocomplete'=>'off']) !!}
                  </div>
              </div>
          </div>
           <div class='col-sm-4'>
            {!! Form::label('PersonnelId', 'Personnel Id:', ['class'=>'highlight']) !!}
           <div class="form-group">
               <div class=''>
                     {!! Form::text('PersonnelId',null,['id'=>'PersonnelId','class'=>'form-control','autocomplete'=>'off']) !!}
               </div>
           </div>
         </div> 
<!--            <div class='col-sm-4'> 
              {!! Form::label('', '', ['class'=>'highlight']) !!}
            <div class="form-group text-right permit">                            	
               {{ Form::button('PDF', ['class' => 'btn btn-info', 'type' => 'button','id'=>'PDF']) }}
               {{ Form::button('Reset', ['class' => 'btn btn-success' ,'type' => 'reset','id'=>'reset']) }}
            </div>
            </div>-->
          </div>
        
       </div>
      </div>
    </div>                              
  </div> 
  
  <div class="panel-group" id="accordion6">                                                              
    <div class="panel panel-default">
     <div class="panel-heading1">
     <h6 class="panel-title">
         <a data-toggle="#" data-parent="#accordion6" href="#collapsePDF"> <span class="fa fa-minus"></span> <span class="highlight"> PDF / EXCEL </span> </a>
     </h6>
     </div>
     <div id="collapsePDF" class="panel-collapse collapse5">
      <div class="panel-body">                                   
        <div class="row">                     
          <div class='col-sm-4'>
            {!! Form::label('recordsav', 'Records Available:', ['class'=>'highlight']) !!}
            <div class="form-group">
               <div class=''>
                     {!! Form::text('recordsav',null,['id'=>'recordsav','class'=>'form-control','autocomplete'=>'off', 'readonly'=>'true']) !!}
               </div>
            </div>
          </div>
         <div class='col-sm-4'>
            {!! Form::label('from', 'Records From:', ['class'=>'highlight required']) !!}
            <div class="form-group">
               <div class=''>
                     {!! Form::text('from',null,['id'=>'from','class'=>'form-control','autocomplete'=>'off']) !!}
               </div>
            </div>
          </div>
          <div class='col-sm-4'>
            {!! Form::label('to', 'Records To:', ['class'=>'highlight required']) !!}
            <div class="form-group">
               <div class=''>
                     {!! Form::text('to',null,['id'=>'to','class'=>'form-control','autocomplete'=>'off']) !!}
               </div>
            </div>
          </div>            
        </div>
          
          
          
        <div class="row"> 
           <div class='col-sm-12'>                    
            <div class="form-group text-right permit">    
               {{ Form::button('EXCEL', ['class' => 'btn btn-info', 'type' => 'button','id'=>'EXCEL']) }}
               {{ Form::button('PDF', ['class' => 'btn btn-info', 'type' => 'button','id'=>'PDF']) }}
               {{ Form::button('Reset', ['class' => 'btn btn-success' ,'type' => 'reset','id'=>'reset']) }}
            </div>
          </div>
        </div>
        
       </div>
      </div>
    </div>                              
  </div> 

 {!! Form::close() !!}             
</div>
<!--// form -->


</section>

</div>
</div>
<!-- Copyright -->
<script type="text/javascript">
$(document).ready(function () {
   $("#reset").click(function () {
    location.reload(true);
  });
  change_subdivision();
  $('select[name="subdivision"]').on('change', function () {
    change_blockMuni();    
 });
  $('select[name="blockMunicipality"]').on('change', function () {
    change_officeName();
 }); 
 $('select[name="phase"]').on('change', function () {
    getFirstRecordAvailable();
 }); 
  $('select[name="officeName"]').on('change', function () {
    getFirstRecordAvailable();
 }); 
 $('select[name="trainingtype"]').on('change', function () {
//    getFirstRecordAvailable();
    change_blockMuni();
//    change_officeName();
 });
  $('#PersonnelId').on('change', function () {
    getFirstRecordAvailable();
 }); 
  $('#PDF').click(function () {
     var districtcd = $("#districtcd").val();
     var subdivision = $("#subdivision").val();
     var blockMunicipality = $("#blockMunicipality").val();
     var officeName = $("#officeName").val();
     var PersonnelId = $("#PersonnelId").val();
     var trainingtype = $("#trainingtype").val();
     var phase =$("#phase").val();
     var from = $("#from").val();
    var to = $("#to").val();
     if(from== ""){
           $.alert({
                title: 'Error!!',
                type: 'red',
                icon: 'fa fa-exclamation-triangle',
                content: 'Records From is required'
            });
            return false;
        }
        if(to==""){
           $.alert({
                title: 'Error!!',
                type: 'red',
                icon: 'fa fa-exclamation-triangle',
                content: 'Records To is required'
            });
            return false;
        }
     var datas = {'districtcd': districtcd,'subdivision': subdivision,'blockMunicipality': blockMunicipality,'officeName': officeName,'PersonnelId': PersonnelId, 'trainingtype': trainingtype,from: from,to: to,phase:phase, '_token': $('input[name="_token"]').val()};
     //$(".se-pre-con").fadeIn("slow");
     redirectPost_newTab('{{url("getFirstAppointmentLetterPDF2019")}}', datas);


 });
    $('#EXCEL').click(function () {
     var districtcd = $("#districtcd").val();
     var subdivision = $("#subdivision").val();
     var blockMunicipality = $("#blockMunicipality").val();
     var officeName = $("#officeName").val();
     var PersonnelId = $("#PersonnelId").val();
     var trainingtype = $("#trainingtype").val();
     var phase =$("#phase").val();
     var from = $("#from").val();
    var to = $("#to").val();
     if(from== ""){
           $.alert({
                title: 'Error!!',
                type: 'red',
                icon: 'fa fa-exclamation-triangle',
                content: 'Records From is required'
            });
            return false;
        }
        if(to==""){
           $.alert({
                title: 'Error!!',
                type: 'red',
                icon: 'fa fa-exclamation-triangle',
                content: 'Records To is required'
            });
            return false;
        }
     var datas = {'districtcd': districtcd,'subdivision': subdivision,'blockMunicipality': blockMunicipality,'officeName': officeName,'PersonnelId': PersonnelId, 'trainingtype': trainingtype,from: from,to: to,phase:phase, '_token': $('input[name="_token"]').val()};
     $(".se-pre-con").fadeIn("slow");
        redirectPost('{{url("getFirstApptExcel")}}', datas);
        $(".se-pre-con").fadeOut("slow");


 }); 
});
function change_subdivision(){
  var forDist = $("#districtcd").val();
   var token = $("input[name='_token']").val();
  // $(".se-pre-con").fadeIn("slow");
   $.ajax({
       url: "getSubdivisionDataUserwise",
       method: 'POST',
       data: {forDist: forDist, _token: token},
       success: function (data) {//alert(data.options);
       //   $(".se-pre-con").fadeOut("slow");
          $("select[name='subdivision'").html('');
          
          if(data.status==1)
          {
            $("select[name='subdivision'").append('<option value>[Select]</option>');
            $.each(data.options,function (k, v)
            {
                $("select[name='subdivision'").append('<option value='+k+'>'+v+'</option>');
            });
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
         $(".se-pre-con").fadeOut("slow");
           var msg = "";
           if (jqXHR.status !== 422 && jqXHR.status !== 400) {
               msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
           } else {
               if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                   msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
               } else {
                   msg += "Error(s):<strong><ul>";
                   $.each(jqXHR.responseJSON, function (key, value) {
                       msg += "<li>" + value + "</li>";
                   });
                   msg += "</ul></strong>";
               }
           }
           $.alert({
               title: 'Error!!',
               type: 'red',
               icon: 'fa fa-warning',
               content: msg
           });
         },
            complete: function() {
            getFirstRecordAvailable();
        }
   });
}
function change_blockMuni() {
   var subdivision = $("#subdivision").val();
   var forDist = $("#districtcd").val();
     var trainingtype = $("#trainingtype").val();
   var token = $("input[name='_token']").val();
   if(subdivision == "")
   {
       return false;
   }
  // $(".se-pre-con").fadeIn("slow");
   $.ajax({
       url: "getBlockMuniUserwise",
       method: 'POST',
       data: {subdivision: subdivision,forDist:forDist,trainingtype:trainingtype, _token: token},
       success: function (data) {//alert(data.options);
       //   $(".se-pre-con").fadeOut("slow");
          $("select[name='blockMunicipality'").html('');
          if(data.status==1)
          {
            $("select[name='blockMunicipality'").html(data.options);
            change_officeName();
             //getMemberDetails();
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
            $(".se-pre-con").fadeOut("slow");
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-warning',
                  content: msg
              });
         },
            complete: function() {
            getFirstRecordAvailable();
        }
   });
 }
function change_officeName(){
     var forDist = $("#districtcd").val();
     var blockmuni = $("#blockMunicipality").val();
     var subdivision = $("#subdivision").val();
      var trainingtype = $("#trainingtype").val();
     var token = $("input[name='_token']").val();
    $(".se-pre-con").fadeIn("slow");
     $.ajax({
       url: "getOfficeNameUserwise",
       method: 'POST',
       data: {subdivision: subdivision,blockmuni: blockmuni,trainingtype:trainingtype, forDist:forDist, _token: token},
       success: function (data) {//alert(data.options);
         $(".se-pre-con").fadeOut("slow");
          $("select[name='officeName'").html('');
          if(data.status==1)
          {
            //$("select[name='officeName'").html(data.options);
             $("select[name='officeName'").append('<option value>[Select]</option>');
            $.each(data.options,function (k, v)
            {
                $("select[name='officeName'").append('<option value='+k+'>'+v+'</option>');
            });
           // getMemberDetails();
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
            $(".se-pre-con").fadeOut("slow");
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-warning',
                  content: msg
              });
         },
            complete: function() {
            getFirstRecordAvailable();
        }
     });
 }
function getFirstRecordAvailable(){
    var districtcd = $("#districtcd").val();
     var subdivision = $("#subdivision").val();
     var blockMunicipality = $("#blockMunicipality").val();
     var officeName = $("#officeName").val();
     var PersonnelId = $("#PersonnelId").val();
     var trainingtype = $("#trainingtype").val();
    var phase =$("#phase").val();
    var token = $("input[name='_token']").val();
    // $(".se-pre-con").fadeIn("slow");
     $.ajax({
       url: "getFirstRecordAvailable",
       method: 'POST',
       data: {'districtcd': districtcd,'subdivision': subdivision,'blockMunicipality': blockMunicipality,'officeName': officeName,'PersonnelId': PersonnelId, 'trainingtype': trainingtype,phase:phase, _token: token},
       success: function (data) {//alert(data.options);
        // $(".se-pre-con").fadeOut("slow");
         $("#recordsav").val("");
          if(data.status==1)
          {
              $("#recordsav").val(data.options);
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-exclamation-triangle',
                  content: msg
              });
         }
   });
}
</script>
@stop
