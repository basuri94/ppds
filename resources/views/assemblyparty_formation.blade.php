
@extends('layouts.master')
@section('content')

<!--// top-bar -->
 <div class="wrapper">
<!-- Sidebar Holder -->
@include('layouts.sidebar')

<!-- Page Content Holder -->
<div id="content">           
<section class="tables-section">
    <nav aria-label="breadcrumb" style="width:100%;">
        <div class="breadcrumb pagehead1">
           <span class="headbrand"><i class="fa fa-desktop"></i> Add Assembly Party
                        <span class="scoop-mcaret1"></span>
                    </span>&nbsp;&nbsp;
                    &nbsp;{!! Form::button(' List of Assembly Party&nbsp;<i class="fa fa-arrow-circle-right"></i>',['id'=>'show_asm_party_list','class' => 'btn btn-primary-year add-new-button']) !!}  

<!--                {!! Form::button('<i class="fa fa-desktop"></i> Add Assembly Party',['class' => 'btn btn-primary-header add-new-button']) !!}
            
            <div class="col-md-offset-8 pull-right">
                {!! Form::button('Academic Year:&nbsp;',['id'=>'show_academic','class' => 'btn btn-primary-year']) !!} 
{!! Form::button('List of Assembly Party&nbsp;<i class="fa fa-arrow-circle-right"></i>',['id'=>'show_asm_party_list','class' => 'btn btn-primary-year add-new-button']) !!}  
            </div>-->
                <div class="clearfix"></div>
        </div>
    </nav>

    <!-- form -->
<div class="outer-w3-agile">
  {!! Form::open(['url' => 'assemblyPartyForm', 'name' => 'assemblyPartyForm', 'class' =>'request-info clearfix form-horizontal', 'id' => 'assemblyPartyForm', 'method' => 'post','role'=>'','files' => true]) !!}
  {!! Form::hidden('districtcd', session()->get('districtcd_ppds'),['id'=>'districtcd']) !!}
  {!! Form::hidden('assembly_array', '',['class'=>'form-control','id'=>'assembly_array']) !!}
  {!! Form::hidden('no_of_member_array', '',['class'=>'form-control','id'=>'no_of_member_array']) !!}
  {!! Form::hidden('gender_array', '',['class'=>'form-control','id'=>'gender_array']) !!}
  {!! Form::hidden('no_of_party_array', '',['class'=>'form-control','id'=>'no_of_party_array']) !!}
<!--  {!! Form::hidden('tysfs_code', '',['id'=>'tysfs_code']) !!}
  {!! Form::hidden('tysfm_code', '',['id'=>'tysfm_code']) !!}-->
  
  <div class="panel-group" id="accordion5">                                                              
   <div class="panel panel-default">
    <div class="panel-heading1">
     <h6 class="panel-title">
         <a data-toggle="#" data-parent="#accordion5" href="#collapsePV"> <span class="fa fa-minus"></span> <span class="highlight">Zone-wise Assembly Party Formation </span></a>
     </h6>
    </div>
    <div id="collapsePV" class="panel-collapse collapse5">
      <div class="panel-body">                                   
       <div class="row">  
<!--    <div class='col-sm-4'>
            {!! Form::label('subdivision', 'Sub Division:', ['class'=>'highlight required']) !!}
              <span class="highlight">Sub Division</span> 
              <div class="form-group">
                  <div class=''>
                      {!! Form::select('subdivision',[''=>'[Select]'],null,['id'=>'subdivision','class'=>'form-control','autocomplete'=>'off']) !!}
                  </div>
              </div>
        </div> -->
           
           <div class='col-sm-4'>
            {!! Form::label('forZone', 'For Zone:', ['class'=>'highlight required']) !!}
           <div class="form-group">
               <div class=''>
                   {!! Form::select('forZone',[''=>'[Select]'],Null,['id'=>'forZone','class'=>'form-control','autocomplete'=>'off']) !!}
               </div>
           </div>
        </div> 
          
      </div>
       
          
      <div class="row"> 
            <div class='col-sm-12'>
                <div class="form-group">
                    <table class="table table-bordered table-striped" style='border-top: 2px solid #4cae4c;' id="assembly_party">
                          <thead>
                              <tr style="background-color: #f5f8fa" >
                                  <th width='37%' class="highlight">Assembly Name <span style="color: red;">*</span></th>
                                  <th width='18%' class="highlight">No of Member <span style="color: red;">*</span></th>
                                  <th width='18%' class="highlight">Gender <span style="color: red;">*</span></th>
                                  <th width='17%' class="highlight">No of Party <span style="color: red;">*</span></th>
                                  <th width='10%' class="highlight">Action</th>
                              </tr>
                          </thead>
                           <tbody>
                              <tr style="background-color: #FDF5E6">
                                  <td>
                                       {!! Form::select('assembly',[''=>'[Select]'],Null,['id'=>'assembly','class'=>'form-control','autocomplete'=>'off']) !!}
                                  </td>
                                  <td>   {!! Form::select('no_of_member',['4'=>'4'],Null,['id'=>'no_of_member','class'=>'form-control','autocomplete'=>'off']) !!}</td>
                                  <td>
                                       {!! Form::select('gender',['M'=>'Male'],Null,['id'=>'gender','class'=>'form-control','autocomplete'=>'off']) !!}
                                  </td>
                                  
                                  <td>   {!! Form::text('no_of_party', null, ['id'=>'no_of_party','class'=>'form-control','maxlength'=>'4'])!!}</td>
                                  <td>   <a href='javascript:' class='add-row'><i class='fa fa-plus-circle' style='color: #17a2b8;cursor:pointer;font-size: 16.5px;padding-top: 10px;'></i>&nbsp;</a>  </td>

                              </tr> 
                          </tbody>
                                               
                 </table>

                  </div>
            </div>
         
      </div> 
      <div class="row">  
         <div class='col-sm-12'>
            <div class="form-group text-right permit">                            	
               {{ Form::button('Submit', ['class' => 'btn btn-info', 'type' => 'submit','id'=>'Submit']) }}
               {{ Form::button('Reset', ['class' => 'btn btn-success' ,'type' => 'reset','id'=>'reset']) }}
            </div>
          </div>
      </div>     
       
    </div>                              
   </div>
  </div>  
  </div>
 {!! Form::close() !!}             
</div>
<!--// form -->

</section>

</div>
</div>
<!-- Copyright -->
<script type="text/javascript">
$(document).ready(function () {
 $("#show_asm_party_list").click(function(){
       window.location.href = "assembly_party_form_list"; 
    });
 $("select[name='gender'").append('<option value="F">Female</option>');
 $("select[name='no_of_member'").append('<option value="5">5</option>','<option value="6">6</option>');
getZoneDetails(); 
//change_subdivision();
$('select[name="forZone"]').on('change', function () {   
getAssemblyDetails(); 
$('.table_tr').remove();
//getAssemblyPartyDetails();
array_assembly=[];
array_no_of_member=[];
array_gender=[];
array_no_of_party=[];
array_duplicate=[];
$("#assembly_array").val("");
    $("#no_of_member_array").val("");
    $("#gender_array").val("");
    $("#no_of_party_array").val("");
});
$("#reset").click(function () {
  location.reload(true);  
});
char_num("#no_of_party");
array_assembly=[];
array_no_of_member=[];
array_gender=[];
array_no_of_party=[];
array_duplicate=[];
    $(".add-row").click(function () {
       var assembly = $("#assembly").val();
       var no_of_member = $("#no_of_member").val();
       var gender = $("#gender").val();
       var no_of_party = $("#no_of_party").val();
       
       if(assembly==""||no_of_member==""||gender==""||no_of_party==""){
           $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-exclamation-triangle',
                  content: "Enter required field"
              });
          return false;
       }
       var asm_member=assembly+"-"+no_of_member+"-"+gender;
       var duplicate_asm=jQuery.inArray(asm_member,array_duplicate);
       if(duplicate_asm>=0){
            $.alert({
                    title: 'Error!!',
                    type: 'red',
                    icon: 'fa fa-exclamation-triangle',
                    content: 'Duplicate record not allow'
                });
            return false;
       }
       array_duplicate.push(assembly+"-"+no_of_member+"-"+gender);
       var assembly_text = $("#assembly option:selected").text();
       var gender_text = $("#gender option:selected").text();
       var markup = "<tr class='table_tr'><td>&nbsp;" + assembly_text + "</td><td>&nbsp;" + no_of_member + "</td><td>&nbsp;" + gender_text + "</td><td>&nbsp;" + no_of_party + "&nbsp;</td><td><a href='javascript:' onclick='deleteRow(this);'><i class='fa fa-trash-alt' style='color:#f10418;cursor:pointer;font-size:16px;'></i></a></td></tr>";
       $("table tbody").append(markup);
        array_assembly.push(assembly);
        array_no_of_member.push(no_of_member);
        array_gender.push(gender);
        array_no_of_party.push(no_of_party);
        $("#assembly_array").val(array_assembly);
        $("#no_of_member_array").val(array_no_of_member);
        $("#gender_array").val(array_gender);
        $("#no_of_party_array").val(array_no_of_party);
        $("#no_of_party").val("");
        document.getElementById('Submit').disabled=false;
    });
    $('#assemblyPartyForm').bootstrapValidator({

        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {

            forZone: {
                validators: {
                    notEmpty: {
                        message: 'Zone is required'
                    }
                }
            }

        }

    }).on('success.form.bv', function (e) {
        e.preventDefault();
        var norowCount = $('#assembly_party tr').length;
        if(norowCount==2){
            $.alert({
                    title: 'Error!!',
                    type: 'red',
                    icon: 'fa fa-exclamation-triangle',
                    content: 'Add atleast one assembly'
                });
            return false;
       }
        var action = $(this).attr('action');
        //alert(action);
        var type = $(this).attr('method');
        var formData = new FormData($(this)[0]);
        
//        var tysfm_code = $('#tysfm_code').val();
//        if (tysfs_code !== "" && tysfm_code != "") {
//            action = 'venue_allocation_update';
//            var operation = "update";
//            var operated = "updated";
//        }
       $(".se-pre-con").fadeIn("slow");
        $.ajax({
            type: type,
            url: action,
            data: formData,
            processData: false,
            contentType: false,
            dataType: "json",
            success: function (data) {
                $(".se-pre-con").fadeOut("slow");

                if (data.status == 1) { 
                        var msg=data.options+" Record(s) saved successfully";
                        if (data.duplicate >0){
                            msg+="</br>Duplicate entry not allow ["+data.duplicate+"]";
                        }
                        $.confirm({
                           title: 'Success!',
                           type: 'green',
                           icon: 'fa fa-check',
                           content: msg,
                           buttons: {
                               ok: function () {     
                                   location.reload(true);
                               }
                           }
                       });

                } else {
                    var msg="Record(s) updated successfully";
                     $.confirm({
                        title: 'Success!',
                        type: 'green',
                        icon: 'fa fa-check',
                        content: msg,
                        buttons: {
                            ok: function () {     
                                location.reload(true);
                            }

                        }
                    });

                }
                //$("#firstTrVenue")[0].reset();
               // $('.table_tr').html("");
               

            },
            error: function (jqXHR, textStatus, errorThrown) {
            $(".se-pre-con").fadeOut("slow");
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-exclamation-triangle',
                  content: msg
              });
           }
        });
    });
});
//function  getAssemblyPartyDetails(){
//    var forZone = $("#forZone").val();
//     var token = $("input[name='_token']").val();
//     $(".se-pre-con").fadeIn("slow");
//     $.ajax({
//       url: "getAssemblyPartyDetails",
//       method: 'POST',
//       data: {forZone: forZone, _token: token},
//       success: function (data) {//alert(data.options);
//         $(".se-pre-con").fadeOut("slow");
//         // $("select[name='assembly'").html('');
//          if(data.status==1)
//          {
//            // $("select[name='assembly'").html(data.options);
//            var tr;
//            for (var i = 0; i < data.options.length; i++) {
//                 tr+="<tr>"; 
//                    tr+="<td>" + data.options[i].assemblycd + "</td>";
//                    tr+="<td>" + data.options[i].no_of_member + "</td>";
//                    tr+="<td>" + data.options[i].assemblycd + "</td>";
//                    tr+="<td>" + data.options[i].no_of_member + "</td>";
//                    tr+="<td>" + data.options[i].no_of_member + "</td>";
//                  tr+="</tr>"; 
//                }
//              $("table tbody").append(tr);
//          }
//       },
//        error: function (jqXHR, textStatus, errorThrown) {
//            $(".se-pre-con").fadeOut("slow");
//              var msg = "";
//              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
//                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
//              } else {
//                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
//                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
//                  } else {
//                      msg += "Error(s):<strong><ul>";
//                      $.each(jqXHR.responseJSON, function (key, value) {
//                          msg += "<li>" + value + "</li>";
//                      });
//                      msg += "</ul></strong>";
//                  }
//              }
//              $.alert({
//                  title: 'Error!!',
//                  type: 'red',
//                  icon: 'fa fa-exclamation-triangle',
//                  content: msg
//              });
//         }
//   });
//}
function deleteRow(row){
    var i = row.parentNode.parentNode.rowIndex;

    document.getElementById('assembly_party').deleteRow(i);
    var index_arr = parseInt(i) - parseInt(2);
    array_assembly.splice(index_arr, 1);
    array_no_of_member.splice(index_arr, 1);
    array_gender.splice(index_arr, 1);
    array_no_of_party.splice(index_arr, 1);
    array_duplicate.splice(index_arr, 1);
    $("#assembly_array").val(array_assembly);
    $("#no_of_member_array").val(array_no_of_member);
    $("#gender_array").val(array_gender);
    $("#no_of_party_array").val(array_no_of_party);

}
function change_subdivision(){
  var forDist = $("#districtcd").val();
   var token = $("input[name='_token']").val();
  // $(".se-pre-con").fadeIn("slow");
   $.ajax({
       url: "getSubdivisionData",
       method: 'POST',
       data: {forDist: forDist, _token: token},
       success: function (data) {//alert(data.options);
       //   $(".se-pre-con").fadeOut("slow");
          $("select[name='subdivision'").html('');
          
          if(data.status==1)
          {
            $("select[name='subdivision'").append('<option value>[Select]</option>');
            $.each(data.options,function (k, v)
            {
                $("select[name='subdivision'").append('<option value='+k+'>'+v+'</option>');
            })
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
         $(".se-pre-con").fadeOut("slow");
           var msg = "";
           if (jqXHR.status !== 422 && jqXHR.status !== 400) {
               msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
           } else {
               if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                   msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
               } else {
                   msg += "Error(s):<strong><ul>";
                   $.each(jqXHR.responseJSON, function (key, value) {
                       msg += "<li>" + value + "</li>";
                   });
                   msg += "</ul></strong>";
               }
           }
           $.alert({
               title: 'Error!!',
               type: 'red',
               icon: 'fa fa-exclamation-triangle',
               content: msg
           });
         }
   });
}
function getZoneDetails(){
     var forDist = $("#districtcd").val();
     var token = $("input[name='_token']").val();
     $(".se-pre-con").fadeIn("slow");
     $.ajax({
       url: "getZoneDetails",
       method: 'POST',
       data: {forDist: forDist, _token: token},
       success: function (data) {//alert(data.options);
         $(".se-pre-con").fadeOut("slow");
          $("select[name='forZone'").html('');
          if(data.status==1)
          {
             $("select[name='forZone'").append('<option value>[Select]</option>');
            $.each(data.options,function (k, v)
            {
                $("select[name='forZone'").append('<option value='+k+'>'+v+'</option>');
            });
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
            $(".se-pre-con").fadeOut("slow");
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-exclamation-triangle',
                  content: msg
              });
         }
   });
}
function getAssemblyDetails(){
    var forZone = $("#forZone").val();
     var token = $("input[name='_token']").val();
     $(".se-pre-con").fadeIn("slow");
     $.ajax({
       url: "getAssemblyDetails",
       method: 'POST',
       data: {forZone: forZone, _token: token},
       success: function (data) {//alert(data.options);
         $(".se-pre-con").fadeOut("slow");
          $("select[name='assembly'").html('');
          if(data.status==1)
          {
              $("select[name='assembly'").html(data.options);
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
            $(".se-pre-con").fadeOut("slow");
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-exclamation-triangle',
                  content: msg
              });
         }
   });
}
</script>
@stop
