@extends('layouts.master')
@section('content')
<style>
   .jconfirm-box{
        width: 700px;
        margin-left: -250px !important; 
    }

    table.dataTable thead th{
        /* right: 67%;*/
        /* left: auto; */

        /*     padding: .92857143em .78571429em !important;*/
    }


</style>
<!--// top-bar -->
<div class="wrapper">
    <!-- Sidebar Holder -->
    @include('layouts.sidebar')
    <!-- Page Content Holder -->
    <div id="content">           
        <section class="tables-section">

            <div class="breadcrumb pagehead1">

                {!! Form::button('<i class="fa fa-desktop"></i> List of Polling Station',['class' => 'btn btn-primary-header add-new-button']) !!}


<!--                {!! Form::button('<i class="fa fa-plus-circle"></i>&nbsp;Add DCRC',['id'=>'add_dcrc_master','class' => 'btn btn-primary-header pull-right', 'style'=>'margin-left: 710px;']) !!}-->


                <div class="clearfix"></div>
            </div>
            </nav>
            <div class="outer-w3-agile">
                <div class="panel-group" id="accordion5">                                                              
                    <div class="panel panel-default">
                        <div class="panel-heading1">
                            <h6 class="panel-title">
                                <a data-toggle="#" data-parent="#accordion5" href="#collapsePV"> <span class="fa fa-minus"></span> <span class="highlight">Polling Station List</span></a>
                            </h6>
                        </div>
                        <div id="collapsePV" class="panel-collapse collapse5">
                            <div class="panel-body">                                   
                                <div class="row">

                                    <div class='col-sm-3'>
                                        {!! Form::label('forZone', 'For Zone:', ['class'=>'highlight']) !!}
                                        <div class="form-group">
                                            <div class=''>
                                                {!! Form::select('forZone',[''=>'[ALL]'],null,['id'=>'forZone','class'=>'form-control','autocomplete'=>'off']) !!}
                                            </div>
                                        </div>
                                    </div> 

                                    <div class='col-sm-3'>
                                        {!! Form::label('phase', 'Phase:', ['class'=>'highlight']) !!}
                                       <div class="form-group">
                                           <div class=''>
                                                <select id="phase" class="form-control" name="phase">
                                                    <option value="">[Select]</option>
                                                </select>
                                            </div>
                                       </div>
                                    </div> 



                                    <div class='col-sm-3'>
                                        {!! Form::label('assembly', 'Assembly:', ['class'=>'highlight']) !!}
                                        <div class="form-group">
                                            <div class=''>
                                                {!! Form::select('assembly',[''=>'[Select]'],null,['id'=>'assembly','class'=>'form-control','autocomplete'=>'off']) !!}
                                            </div>
                                        </div>
                                    </div> 
                                    <div class='col-sm-3'>
                                        {!! Form::label('dcvenue', 'DC Venue,Date & Time:', ['class'=>'highlight']) !!}
                                        <div class="form-group">
                                            <div class=''>
                                                {!! Form::select('dcvenue',[''=>'[Select]'],Null,['id'=>'dcvenue','class'=>'form-control','autocomplete'=>'off']) !!}
                                            </div>
                                        </div>
                                    </div> 

                                </div>
                            </div>

                        </div>
                        <div class="form-horizontal">
                            <div class="form-group row">
                                {{csrf_field()}}
                                <div class="datatbl table-responsive" style="width: 96%;margin-left: 20px;">
                                    {!! Form::hidden('districtcd', session()->get('districtcd_ppds'),['id'=>'districtcd']) !!}
                                    <table class="table table-striped table-bordered table-hover notice-types-table" id="datatable-table" style="">
                                        <thead>
                                            <tr>
                                                <th>SL#</th>
                                                <th>DC Venue</th>
                                                <th>Assembly</th>
                                                <th>Member</th>
                                                <th>Gender</th>
                                                <th>PS No</th>
                                                <th>Polling Name</th>
                                                <th>Polling Party</th>
                                                <th>Action</th>
                                            </tr>

                                        </thead>
                                        <tbody></tbody>
                                        <!-- Table Footer -->

                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </section>
    </div>
</div>
<script>
    $(document).ready(function () {

        getZoneDetails();
        $('select[name="forZone"]').on('change', function () {
            var forZone=$('#forZone').val();
            if(forZone==''){
                  create_table();
            }
         else{
              //getDCVenueDetails();
              getZonePhasedata();
              getAssemblyDetails();
//              create_table();
         }
           
            
        });
        $('select[name="phase"]').on('change', function () {
            var forZone=$('#forZone').val();
            var phase=$('#phase').val();
            if(phase!='' || forZone!=''){
                getAssemblyDetails();
            } else {
                create_table();
            }
//          else{
//               //getDCVenueDetails();
//               getAssemblyDetails();
// //              create_table();
//          }
        });


        $('select[name="assembly"]').on('change', function () {
           getDCVenueDetails();
           // create_table();
        });

        $('select[name="dcvenue"]').on('change', function () {

            create_table();

        });

        var table = $('#datatable-table').DataTable();
        table.on('draw.dt', function () {

            $('.edit-button').click(function () {
                var value_all = this.id;

                var explode_data = value_all.split("/");

                $.ajax({
                    type: 'post',
                    url: 'polling_modify',
                    data: {'data': value_all, '_token': $('input[name="_token"]').val()},
                    dataType: 'json',
                    success: function (datam) {
                        $(".se-pre-con").fadeOut("slow");
                        //alert(datam.count);
//                            if (datam.count > 0) {
//                                $.alert("You Cannot Edit this record becasue this is already exist in pesonal Table");
//                                return false;
//                            }
                        var edit_val = datam.rec;
                        var spli_val = value_all.split("/");
                        var table_edit = '<form id="edit_first" action="" method="post" class="form-horizontal">';
                        table_edit += '<input type="hidden" value="' + edit_val[0].forzone + '" id="forzone_code">';
                        table_edit += '<input type="hidden" value="' + edit_val[0].psfix + '" id="ps_code">';

                        table_edit += '<div class="form-group">';
                        table_edit += '<label class="control-label col-md-6 required">PS No:</label>';
                        table_edit += '<div class="col-md-6"><input type="text" class="form-control" value="' + edit_val[0].psno + '" id="ps_no" name="ps_no" style="margin-bottom: 5px;" readonly></div>';
                        table_edit += '</div>';
                        table_edit += '<div class="form-group">';
                        table_edit += '<label class="control-label col-md-6 required">Zone Name:</label>';
                        table_edit += '<div class="col-md-6"><input type="text" class="form-control" value="' + edit_val[0].zonename + '" id="zone_name" name="zone_name" style="margin-bottom: 5px;" readonly></div>';
                        table_edit += '</div>';
                        table_edit += '<div class="form-group">';
                        table_edit += '<label class="control-label col-md-6 required">Assembly Code:</label>';
                        table_edit += '<div class="col-md-6"><input type="text" class="form-control" value="' + edit_val[0].forassembly + '" id="assem_code" name="assem_code" style="margin-bottom: 5px;" readonly></div>';
                        table_edit += '</div>';
                        table_edit += '<div class="form-group">';
                        table_edit += '<label class="control-label col-md-6 required">Polling Name:</label>';
                        table_edit += '<div class="col-md-6"><input type="text" class="form-control" value="' + edit_val[0].psname + '" id="ps_name" name="ps_name" style="margin-bottom: 5px;"></div>';
                        table_edit += '</div>';
                        table_edit += '<div class="form-group">';
                        table_edit += '<label class="control-label col-md-6 required">Member:</label>';
                        table_edit += '<div class="col-md-6"><input type="text" class="form-control dc_dt" value="' + edit_val[0].member + '" id="mem_ber" name="mem_ber" style="margin-bottom: 5px;" readonly></div>';
                        table_edit += '</div>';
                        table_edit += '<div class="form-group">';
                        table_edit += '<label class="control-label col-md-6 required">Gender:</label>';
                        table_edit += '<div class="col-md-6"><input type="text" class="form-control" value="' + spli_val[4] + '" id="gen_der" name="gen_der" style="margin-bottom: 5px;" readonly></div>';
                        table_edit += '</div>';
                        table_edit += '<div class="form-group">';
                        table_edit += '<label class="control-label col-md-6 ">Polling Party:</label>';
                        if(edit_val[0].groupid ==null){
                           var groupid=0; 
                        } else{
                           var groupid = edit_val[0].groupid;
                        }
                        table_edit += '<div class="col-md-6"><input type="text" class="form-control" value="' + groupid + '" id="grp_id" name="grp_id" style="margin-bottom: 5px;" readonly></div>';
                        table_edit += '</div>';
                       
                        table_edit += '</form>';

                        var jc = $.confirm({
                            title: '<span style="font-size: 17px;">Edit Polling Station</span>',
                            content: table_edit,
                            type: 'green',
                            icon: 'fa fa-edit',
                            typeAnimated: true,
                            buttons: {

                                edit: {
                                    btnClass: 'btn-primary',
                                    action: function () {
                                        var arr = ['ps_no', 'zone_name', 'assem_code', 'ps_name', 'mem_ber', 'gen_der'];
                                        var flag = 0;
                                        jQuery.each(arr, function (index, item) {
                                            var check = $("#" + item + "").val();
                                            if (check == '') {
                                                 $.alert({
                                                    title: 'Error!!',
                                                    type: 'red',
                                                    icon: 'fa fa-exclamation-triangle',
                                                    content: "All fields are mandatory"
                                                });
                                                flag = 1;
                                                return false;
                                            }
                                        });
                                        if (flag > 0) {
                                            return false;
                                        }
                                        jc.showLoading(true);
                                        var ps_no = $("#ps_no").val();
                                        //   var zone_name = $("#zone_name").val();
                                        var assem_code = $("#assem_code").val();
                                        var ps_name = $("#ps_name").val();
                                        // alert(ps_name);
                                        var mem_ber = $("#mem_ber").val();
                                        var gen_der = $("#gen_der").val();
                                        //var grp_id = $("#grp_id").val();
                                        var forzone_code = $('#forzone_code').val();
                                        var ps_code = $('#ps_code').val();
                                        return $.ajax({
                                            url: 'update_polling',
                                            dataType: 'json',
                                            data: {'ps_no': ps_no, 'assem_code': assem_code, 'ps_name': ps_name, 'mem_ber': mem_ber, 'gen_der': gen_der, 'forzone_code': forzone_code, 'ps_code': ps_code, '_token': $("input[name='_token']").val()},
                                            method: 'post'
                                        }).done(function (response) {
                                            //alert('hi');
                                            jc.hideLoading(true);
                                            if (response.status == 1) {
                                                var msg_suc = "<strong>SUCCESS: Record edited successfuly</strong>";
                                                $.confirm({
                                                    title: 'Success!',
                                                    type: 'green',
                                                    icon: 'fa fa-check',
                                                    content: msg_suc,
                                                    buttons: {
                                                        ok: function () {  
                                                            //table.ajax.reload();
                                                            create_table();
                                                        }

                                                    }
                                                });
//                                                $.alert({
//                                                    title: 'Success!!',
//                                                    type: 'green',
//                                                    icon: 'fa fa-check',
//                                                    content: msg_suc,
//                                                });
                                               // table.ajax.reload();
                                            }
                                        }).fail(function (jqXHR, textStatus, errorThrown) {
                                            var msg = "";
                                            if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                                                msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                                            } else {
                                                if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                                                    if (jqXHR.responseJSON.exception_code == 23000) {
                                                        msg += "Record exist in another table.You can't change this record";
                                                    } else {
                                                        msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                                                    }
                                                } else {
                                                    msg += "Error(s):<strong><ul>";
                                                    $.each(jqXHR.responseJSON, function (key, value) {
                                                        msg += "<li>" + value + "</li>";
                                                    });
                                                    msg += "</ul></strong>";
                                                }
                                            }
                                            $.alert({
                                                title: 'Error!!',
                                                type: 'red',
                                                icon: 'fa fa-warning',
                                                content: msg,
                                            });
                                        });
                                    }

                                },
                                close: function () {
                                }
                            },

                            onOpen: function () {
                                // after the modal is displayed.
                                //startTimer();
                            }
                        });
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        $(".se-pre-con").fadeOut("slow");
                        var msg = "";
                        if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                            msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                        } else {
                            if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                                if (jqXHR.responseJSON.exception_code == 23000) {
                                    msg += "Record exist in another table.You can't change this record.";
                                } else {
                                    msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                                }
                            } else {
                                msg += "Error(s):<strong><ul>";
                                $.each(jqXHR.responseJSON, function (key, value) {
                                    msg += "<li>" + value + "</li>";
                                });
                                msg += "</ul></strong>";
                            }
                        }
                        $.alert({
                            title: 'Error!!',
                            type: 'red',
                            icon: 'fa fa-warning',
                            content: msg,
                        });
                    }
                });

            });
            $('.delete-button').click(function () {
//                var reply = $.confirm('Are you sure to delete the record?');
//                if (!reply) {
//                    return false;
//   
//                             }
                var value_all = this.id;

                $.ajax({
                    type: 'post',
                    url: 'delete_polling_chking',
                    data: {'data': value_all, '_token': $('input[name="_token"]').val()},
                    dataType: 'json',
                    success: function (datam) {
                        $(".se-pre-con").fadeOut("slow");
                        //alert(datam.count);
                        if (datam.count > 0) {
                             $.alert({
                                title: 'Error!!',
                                type: 'red',
                                icon: 'fa fa-exclamation-triangle',
                                content: "Polling Party exist in polling_station table.You can't delete this record."
                            });
                            return false;
                        }
                        $.confirm({
                            title: 'Attention!',
                            content: 'Do you want to delete this record?',
                            type: 'orange',
                            icon: 'fa fa-exclamation-triangle',
                            buttons: {
                                confirm: function () {
                                    // var data_new = value_all;
                                    // alert(data_new);
                                    $.ajax({
                                        type: 'post',
                                        url: 'delete_polling',
                                        data: {'data': value_all, '_token': $('input[name="_token"]').val()},
                                        dataType: 'json',
                                        success: function (datam) {

                                            var msg = "<strong>SUCCESS: Record deleted successfully</strong>";
                                            $.confirm({
                                                title: 'Success!',
                                                type: 'green',
                                                icon: 'fa fa-check',
                                                content: msg,
                                                buttons: {
                                                    ok: function () {  
                                                        //table.ajax.reload();
                                                        create_table();
                                                    }
                                                }
                                            });

                                        },
                                        error: function (jqXHR, textStatus, errorThrown) {
                                            $(".se-pre-con").fadeOut("slow");
                                            var msg = "";
                                            if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                                                msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                                            } else {
                                                if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                                                    if (jqXHR.responseJSON.exception_code == 23000) {
                                                        msg += "Record exist in another table.You can't delete this record.";
                                                    } else {
                                                        msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                                                    }
                                                } else {
                                                    msg += "Error(s):<strong><ul>";
                                                    $.each(jqXHR.responseJSON, function (key, value) {
                                                        msg += "<li>" + value + "</li>";
                                                    });
                                                    msg += "</ul></strong>";
                                                }
                                            }
                                            $.alert({
                                                title: 'Error!!',
                                                type: 'red',
                                                icon: 'fa fa-warning',
                                                content: msg,
                                            });
                                        }
                                    });
                                },
                                cancel: function () {

                                }
                            }
                        });
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        $(".se-pre-con").fadeOut("slow");
                        var msg = "";
                        if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                            msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                        } else {
                            if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                                if (jqXHR.responseJSON.exception_code == 23000) {
                                    msg += "Record exist in another table.You can't delete this record.";
                                } else {
                                    msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                                }
                            } else {
                                msg += "Error(s):<strong><ul>";
                                $.each(jqXHR.responseJSON, function (key, value) {
                                    msg += "<li>" + value + "</li>";
                                });
                                msg += "</ul></strong>";
                            }
                        }
                        $.alert({
                            title: 'Error!!',
                            type: 'red',
                            icon: 'fa fa-warning',
                            content: msg,
                        });
                    }
                });


            });


        });
        // table.columns( [-4,-3,-2,-1] ).visible( false );

    });
    function getZoneDetails() {
        var forDist = $("#districtcd").val();
        var token = $("input[name='_token']").val();
        $(".se-pre-con").fadeIn("slow");
        $.ajax({
            url: "getZoneDetails",
            method: 'POST',
            data: {forDist: forDist, _token: token},
            success: function (data) {//alert(data.options);
                $(".se-pre-con").fadeOut("slow");
                $("select[name='forZone'").html('');
                if (data.status == 1)
                {
                    $("select[name='forZone']").append('<option value="">[ALL]</option>');
                    $.each(data.options, function (k, v)
                    {
                        $("select[name='forZone']").append('<option value=' + k + '>' + v + '</option>');
                    });

                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $(".se-pre-con").fadeOut("slow");
                var msg = "";
                if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                    msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                } else {
                    if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                        msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                    } else {
                        msg += "Error(s):<strong><ul>";
                        $.each(jqXHR.responseJSON, function (key, value) {
                            msg += "<li>" + value + "</li>";
                        });
                        msg += "</ul></strong>";
                    }
                }
                $.alert({
                    title: 'Error!!',
                    type: 'red',
                    icon: 'fa fa-exclamation-triangle',
                    content: msg
                });
            },
            complete: function() {
                 create_table();
            }
        });
    }

    function getZonePhasedata() {
            var zone = $("#forZone").val();
            $("#rw").hide();
            var token = $("input[name='_token']").val();
            // $(".se-pre-con").fadeIn("slow");
            $.ajax({
                url: "getZonePhasedata",
                method: 'POST',
                data: {
                    zone: zone,
                    _token: token
                },
                success: function(data) { //alert(data.options);
                    //   $(".se-pre-con").fadeOut("slow");
                    $("select[name='phase'").html('');

                    if (data.status == 1) {
                        $("select[name='phase'").append('<option value>[Select]</option>');
                        $.each(data.options, function(k, v) {
                            $("select[name='phase'").append('<option value=' + k + '>' + v +
                                '</option>');
                        });
                        //$("select[name='subdivision'").html(data.options);
                        // change_officeName();

                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    //  $(".se-pre-con").fadeOut("slow");
                    var msg = "";
                    if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                        msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                    } else {
                        if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                            msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                        } else {
                            msg += "Error(s):<strong><ul>";
                            $.each(jqXHR.responseJSON, function(key, value) {
                                msg += "<li>" + value + "</li>";
                            });
                            msg += "</ul></strong>";
                        }
                    }
                    $.alert({
                        title: 'Error!!',
                        type: 'red',
                        icon: 'fa fa-warning',
                        content: msg
                    });
                }
                // complete: function() {
                //     getZonePhaseWiseAssembly();
                // }
            });
        }





    function getAssemblyDetails() {
        var forZone = $("#forZone").val();
        var phase = $("#phase").val();
        var token = $("input[name='_token']").val();
       // $(".se-pre-con").fadeIn("slow");
        $.ajax({
            url: "get_boothtaging_AssemblyDetails",
            method: 'POST',
            data: {forZone: forZone,phase:phase, _token: token},
            success: function (data) {//alert(data.options);
                $(".se-pre-con").fadeOut("slow");
                $("select[name='assembly'").html('');
                if (data.status == 1)
                {
                    $("select[name='assembly'").html(data.options);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $(".se-pre-con").fadeOut("slow");
                var msg = "";
                if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                    msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                } else {
                    if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                        msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                    } else {
                        msg += "Error(s):<strong><ul>";
                        $.each(jqXHR.responseJSON, function (key, value) {
                            msg += "<li>" + value + "</li>";
                        });
                        msg += "</ul></strong>";
                    }
                }
                $.alert({
                    title: 'Error!!',
                    type: 'red',
                    icon: 'fa fa-exclamation-triangle',
                    content: msg
                });
            },
            complete: function() {
                 create_table();
            }
        });
    }

    function getDCVenueDetails() {
        var forZone = $("#forZone").val();
        var assembly = $("#assembly").val();
        var token = $("input[name='_token']").val();
       // $(".se-pre-con").fadeIn("slow");
        $.ajax({
            url: "getDCAssemblyVenueDetails",
            method: 'POST',
            data: {forZone: forZone, assembly:assembly, _token: token},
            success: function (data) {//alert(data.options);
                $(".se-pre-con").fadeOut("slow");
                $("select[name='dcvenue'").html('');
                if (data.status == 1)
                {
                    $("select[name='dcvenue']").append('<option value>[Select]</option>');
                    $.each(data.options, function (k, v)
                    {
                        $("select[name='dcvenue']").append('<option value=' + k + '>' + v + '</option>');
                    });
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $(".se-pre-con").fadeOut("slow");
                var msg = "";
                if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                    msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                } else {
                    if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                        msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                    } else {
                        msg += "Error(s):<strong><ul>";
                        $.each(jqXHR.responseJSON, function (key, value) {
                            msg += "<li>" + value + "</li>";
                        });
                        msg += "</ul></strong>";
                    }
                }
                $.alert({
                    title: 'Error!!',
                    type: 'red',
                    icon: 'fa fa-exclamation-triangle',
                    content: msg
                });
            },
            complete: function() {
                create_table();
                
            }
        });
    }
    function create_table() {
        var table = "";
        var token = $('input[name="_token"]').val();
        $("#datatable-table").dataTable().fnDestroy();
        table = $('#datatable-table').DataTable({
            "processing": true,
            "serverSide": true,
            "dom": 'lBfrtip',
            "buttons": [
                //'copy', 'csv', 'excel', 'pdf'
                {
                    extend: 'collection',
                    text: 'Export',
                    buttons: [
                        {
                            extend: 'copyHtml5',
                            exportOptions: {
                                columns: ':visible'
                            }
                        },
                        {
                            extend: 'excelHtml5',
                            exportOptions: {
                                columns: ':visible'
                            }
                        },
                        {
                            extend: 'pdfHtml5',
                            exportOptions: {
                                columns: ':visible'
                            }
                        },
                        {
                            extend: 'print',
                            exportOptions: {
                                columns: ':visible'
                            }
                        },
                        {
                            extend: 'csvHtml5',
                            exportOptions: {
                                columns: ':visible'
                            }
                        }

                    ]

                },
                'colvis'
            ],
            "ajax": {
                url: "polling_list_datatable",
                type: "post",
                data: {'_token': $('input[name="_token"]').val(), 'zone': $("#forZone").val(), 'phase': $("#phase").val(),'dcvenue': $("#dcvenue").val(), 'assembly': $("#assembly").val()},
                dataSrc: "polling_masters",
                error: function (jqXHR, textStatus, errorThrown) {
                    var msg = "";
                    if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                        msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                    } else {
                        if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                            if (jqXHR.responseJSON.exception_code == 23000) {
                                msg += "Some Sql Exception Occured";
                            } else {
                                msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                            }
                        } else {
                            msg += "Error(s):<strong><ul>";
                            $.each(jqXHR.responseJSON, function (key, value) {
                                msg += "<li>" + value + "</li>";
                            });
                            msg += "</ul></strong>";
                        }
                    }
                    $.alert({
                        title: 'Error!!',
                        type: 'red',
                        icon: 'fa fa-warning',
                        content: msg
                    });
                }
            },
            "dataType": 'json',
            "columnDefs":
                    [
                        {className: "table-text", "targets": "_all"},
                        {
                            "targets": 0,
                            "width": "2%",
                            "data": "code",
                            "defaultContent": "",
                            "searchable": false,
                            "sortable": false
                        },
                        {
                            "targets": 1,
                            "data": "dc_venue",
                            "width": "30%"
                        },

                        {
                            "targets": 2,
                            "data": "assemblyname",
                            "width": "18%"
                        },
                        {
                            "targets": 3,
                            "width": "2%",
                            "data": "member",
                        },
                        {
                            "targets": 4,
                            "data": "gender"
                        },
                        {
                            "targets": 5,
                            "data": "psno_prefix",
                            "sortable": false
                        },

                        {
                            "targets": 6,
                            "width": "30%",
                            "data": "psname"
                        },
                        {
                            "targets": 7,
                            "data": "groupid",
                        },
                        {
                            "targets": -1,
                            "data": 'action',
                            "searchable": false,
                            "sortable": false,
                            "render": function (data, type, full, meta) {
                                var str_btns = "";

                                str_btns += '<button type="button"  class="btn btn-success  edit-button btn_new1" id="' + data.e + '" title="Edit"><i class="fa fa-edit"></i></button>&nbsp&nbsp';



                                str_btns += '<button type="button"  class="btn btn-danger  delete-button btn_new1" id="' + data.d + '" title="Delete"><i class="fa fa-trash"></i></button>';

                                return str_btns;
                            }
                        }
                    ],

            "order": [[1, 'asc']]
        });
        table.on('order.dt search.dt draw.dt', function () {
            $('[data-toggle="tooltip"]').tooltip();
            table.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
                cell.innerHTML = table.page() * table.page.len() + (i + 1);
            });
        });
    }
</script>
<!-- Copyright -->
@stop