@extends('layouts.master')
@section('content')
<!--// top-bar -->
 <div class="wrapper">
<!-- Sidebar Holder -->
@include('layouts.sidebar')

<!-- Page Content Holder -->
<div id="content">           
<section class="tables-section">
    <nav aria-label="breadcrumb" style="width:100%;">
        <div class="breadcrumb pagehead1">
            <span class="headbrand"><i class="fa fa-desktop"></i> Add/Edit DCRC Party
            <span class="scoop-mcaret1"></span>
           </span>&nbsp;&nbsp;&nbsp;         
                {!! Form::button(' Add Party Separation DCRC&nbsp;<i class="fa fa-plus-circle"></i>',['id'=>'show_dcrc_party','class' => 'btn btn-primary-year add-new-button']) !!}  
                &nbsp;{!! Form::button(' List of DCRC party&nbsp;<i class="fa fa-arrow-circle-right"></i>',['id'=>'show_dcrc_party_list','class' => 'btn btn-primary-year add-new-button']) !!}  
                          
                <div class="clearfix"></div>
        </div>
    </nav>

    <!-- form -->
<div class="outer-w3-agile">
  {!! Form::open(['url' => 'DCRCParty', 'name' => 'DCRCParty', 'class' =>'request-info clearfix form-horizontal', 'id' => 'DCRCParty', 'method' => 'post','role'=>'','files' => true]) !!}
  {!! Form::hidden('districtcd', session()->get('districtcd_ppds'),['id'=>'districtcd']) !!}
  
  <div class="panel-group" id="accordion5">                                                              
   <div class="panel panel-default">
    <div class="panel-heading1">
     <h6 class="panel-title">
         <a data-toggle="#" data-parent="#accordion5" href="#collapsePV"> <span class="fa fa-minus"></span> <span class="highlight">Assemblywise DCRC Party Formation </span></a>
     </h6>
    </div>
    <div id="collapsePV" class="panel-collapse collapse5">
      <div class="panel-body">                                   
       <div class="row">  
         <div class='col-sm-4'>
            {!! Form::label('forZone', 'For Zone:', ['class'=>'highlight required']) !!}
           <div class="form-group">
               <div class=''>
                   {!! Form::select('forZone',[''=>'[Select]'],Null,['id'=>'forZone','class'=>'form-control','autocomplete'=>'off']) !!}
               </div>
           </div>          
        </div> 
        <div class='col-sm-4'>
               {!! Form::label('dcvenue', 'DC Venue,Date & Time:', ['class'=>'highlight required']) !!}
              <div class="form-group">
                  <div class=''>
                      {!! Form::select('dcvenue',[''=>'[Select]'],Null,['id'=>'dcvenue','class'=>'form-control','autocomplete'=>'off']) !!}
                  </div>
              </div>
          </div>
<!--          <div class='col-sm-3'>
                {!! Form::label('dcdate', 'DC Date:', ['class'=>'highlight required']) !!}
               <div class="form-group">
                   <div class=''>
                        {!! Form::text('dcdate',null,['id'=>'dcdate','class'=>'form-control','autocomplete'=>'off','maxlength'=>'10','placeholder'=>'DD/MM/YYYY']) !!}
                   </div>
               </div>
           </div>
          <div class='col-sm-3'>
                {!! Form::label('dctime', 'DC Time:', ['class'=>'highlight required']) !!}
               <div class="form-group">
                   <div class=''>
                        {!! Form::text('dctime',null,['id'=>'dctime','class'=>'form-control','autocomplete'=>'off','maxlength'=>'10']) !!}
                   </div>
               </div>
           </div>-->
        </div>
        
          
          
      <div class="row"> 
            <div class='col-sm-12'>
                <div id='assemblyDetails'></div>
            </div>
         
      </div> 
      <div class="row" id="rw" style="display: none;">                                   
           
           <div class='col-sm-12' > 
<!--               {!! Form::label('', '', ['class'=>'highlight']) !!}-->
                <div class="form-group text-right permit" >                            	
                    {{ Form::button('Submit', ['class' => 'btn btn-info', 'type' => 'submit','id'=>'Submit']) }}
                    {{ Form::button('Reset', ['class' => 'btn btn-success' ,'type' => 'reset','id'=>'reset']) }}
                </div>
           </div>
                
       </div>     
    </div>                              
   </div>
  </div>  
  </div>
 {!! Form::close() !!}             
</div>
<!--// form -->

</section>
</div>
</div>
<!-- Copyright -->
<script type="text/javascript">
$(document).ready(function () {
 $("select[name='type'").append('<option value="N">Number</option>');
 $("#reset").click(function () {
  location.reload(true);  
});
 $("#dcdate").on('change', function () {
    var str= $("#dcdate").val();
    if(!str.match(/^(?:(0[1-9]|[12][0-9]|3[01])[\- \/.](0[1-9]|1[012])[\- \/.](19|20)[0-9]{2})$/))
    {
       $.alert({
            title: 'Error!!',
            type: 'red',
            icon: 'fa fa-exclamation-triangle',
            content: 'Invalid Date format'
        });
        $("#dcdate").val("");
        return false;
    }  
   
 });
$("#show_dcrc_party_list").click(function(){
    window.location.href = "dcrc_party_list"; 
});
 $("#show_dcrc_party").click(function(){
    window.location.href = "dcrc_party_separate"; 
});
getZoneDetails(); 
//char_num("#pr");
//char_num("#p1");
//char_num("#p2");
//char_num("#p3");
//char_num("#pa");
//char_num("#pb");
 $('select[name="forZone"]').on('change', function () {
    getDCVenueDetails();
       
 });
 $('#DCRCParty').bootstrapValidator({

        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {

            forZone: {
                validators: {
                    notEmpty: {
                        message: 'Zone is required'
                    }
                }
            },
            dcvenue: {
                validators: {
                    notEmpty: {
                        message: 'DC Venue is required'
                    }
                }
            }
            
//            dcdate: {
//                validators: {
//                    notEmpty: {
//                        message: 'DC Date is required'
//                    }
//                }
//            },
//            dctime: {
//                validators: {
//                    notEmpty: {
//                        message: 'DC Time is required'
//                    }
//                }
//            }
            
        }

    }).on('success.form.bv', function (e) {
        e.preventDefault();
        var action = $(this).attr('action');
        var type = $(this).attr('method');
        
        var formData = new FormData($(this)[0]);
       $(".se-pre-con").fadeIn("slow");
        $.ajax({
            type: type,
            url: action,
            data: formData,
            processData: false,
            contentType: false,
            dataType: "json",
            success: function (data) {
                $(".se-pre-con").fadeOut("slow");

                if (data.status == 1) { 
                    var msg=data.options+" Record(s) saved successfully";
                    $.confirm({
                       title: 'Success!',
                       type: 'green',
                       icon: 'fa fa-check',
                       content: msg,
                       buttons: {
                           ok: function () {     
                               location.reload(true);
                           }
                       }
                   });
                } 
            },
            error: function (jqXHR, textStatus, errorThrown) {
            $(".se-pre-con").fadeOut("slow");
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-exclamation-triangle',
                  content: msg
              });
              $("#Submit").attr('disabled',false);
           }
        });
    });
});

function get_AssemblyPartyDetails(){
   var forDist = $("#districtcd").val();
   var forZone = $("#forZone").val();
   $("#rw").hide();
    var token = $("input[name='_token']").val();
    $(".se-pre-con").fadeIn("slow");
    $.ajax({
        type: 'POST',
        url: 'getAssemblyPartyDetailsForDCRC',
        data: {forDist:forDist,forZone:forZone, _token:token},
        dataType: 'json',
        success: function (response) {
        $(".se-pre-con").fadeOut("slow");
        $("#assemblyDetails").html("");
        var str='<input type="hidden" id="row_count" name="row_count" value="'+response.options.length+'"> '
        str+= "<table class='table table-bordered table-striped' width='100%' style='border-top: 2px solid #4cae4c;'>";
        str += ' <tr style="background-color: #f5f8fa"><th align="left">&nbsp;<input type="checkbox" id="myCheckall" name="myCheckall"  value="1"  class="myCheckall">&nbsp;</th><th align="left">&nbsp;Assembly Name </th><th>&nbsp;No of Member </th><th>&nbsp;Gender </th><th>&nbsp;No of Party </th><th>&nbsp;DCRC </th></tr>';
//         str += '<tr style=" background-color: #1ABB9C; color: #FFFFFF; font-weight: bold;"><td align="left"><input type="checkbox" id="myCheckall" name="myCheckall"  value="1"  class="submenu">&nbsp; Check All</td><td align="center" width="30%"><input type="checkbox" id="view_all" class="flat"></td></tr>';
         var sub_length = response.options.length;
        // alert(sub_length);
         for (var i = 0; i < sub_length; i++)
         {
               var c = parseInt(i) + 1;
               var gender;
               if(response.options[i].gender=='M'){
                   gender="Male";
               }else{
                   gender="Female";
               }
                str += '<tr><td align="left" width="5%">&nbsp;';
                
                str+='<input type="checkbox" id="myCheck' + c + '" name="myCheck' + c + '"  value="'+ response.options[i].assemblycd+''+response.options[i].no_of_member+''+response.options[i].gender+'"  class="submenu" >';                             
                str+='</td><td align="left" width="30%">&nbsp;' + response.options[i].assemblycd +' - '+ response.options[i].assemblyname + '</td>';
                str+='</td><td align="left" width="10%">&nbsp;' + response.options[i].no_of_member + '</td>';
                str+='</td><td align="left" width="10%">&nbsp;' + gender + '</td>';
                str+='</td><td align="left" width="19%">&nbsp;' + response.options[i].no_party + '</td>';
                
                str+='<td align="left" width="26%">';
                if(response.options[i].reserveStatus=='')
                {
                   str+='&nbsp;<span class="fa fa-exclamation-triangle" style="color: red;"></span></td>';
                }
                else{
                  str+='&nbsp;<span style="color: green;">'+response.options[i].reserveStatus+'</span></td>';  
                 
                }
                str+='</td></tr>'; // return empty
        }
        str += "</table>";
        $("#assemblyDetails").append(str);
        
          $("#myCheckall").change(function(){ 
            var status = this.checked;
            if(status==true){
                $("#rw").show();
                scrollToElement($('#rw'));
            }else if(status==false){
                $("#rw").hide();
            }
            
            $('.submenu').each(function(){
                this.checked = status;     
            });
        });
        $('.submenu').change(function(){
            if($('.submenu:checked').length=='0'){
                $("#rw").hide();
            }else{
                $("#rw").show();
            }
            if(this.checked == false){
                $("#myCheckall")[0].checked = false; 
            }
            if ($('.submenu:checked').length == $('.submenu').length ){ 
                $("#myCheckall")[0].checked = true;
            }
        });
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $(".se-pre-con").fadeOut("slow");
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-exclamation-triangle',
                  content: msg
              });
         }
    });
}

function getZoneDetails(){
     var forDist = $("#districtcd").val();
     var token = $("input[name='_token']").val();
     $(".se-pre-con").fadeIn("slow");
     $.ajax({
       url: "getZoneDetails",
       method: 'POST',
       data: {forDist: forDist, _token: token},
       success: function (data) {//alert(data.options);
         $(".se-pre-con").fadeOut("slow");
          $("select[name='forZone'").html('');
          if(data.status==1)
          {
             $("select[name='forZone'").append('<option value>[Select]</option>');
            $.each(data.options,function (k, v)
            {
                $("select[name='forZone'").append('<option value='+k+'>'+v+'</option>');
            });
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
            $(".se-pre-con").fadeOut("slow");
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-exclamation-triangle',
                  content: msg
              });
         }
   });
}
function getDCVenueDetails(){
     var forZone = $("#forZone").val();
     var token = $("input[name='_token']").val();
    // $(".se-pre-con").fadeIn("slow");
     $.ajax({
       url: "getDCVenueDetails",
       method: 'POST',
       data: {forZone: forZone, _token: token},
       success: function (data) {//alert(data.options);
        // $(".se-pre-con").fadeOut("slow");
          $("select[name='dcvenue'").html('');
          if(data.status==1)
          {
             $("select[name='dcvenue'").append('<option value>[Select]</option>');
            $.each(data.options,function (k, v)
            {
                $("select[name='dcvenue'").append('<option value='+k+'>'+v+'</option>');
            });
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
            $(".se-pre-con").fadeOut("slow");
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-exclamation-triangle',
                  content: msg
              });
         },complete: function(){
          get_AssemblyPartyDetails(); 
         }
   });
}
function scrollToElement(ele) {
    $('html, body').animate({
        scrollTop: ele.offset().top - 60
    }, 1000);
}
</script>
@stop