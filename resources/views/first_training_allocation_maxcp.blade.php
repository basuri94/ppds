@extends('layouts.master')
@section('content')
<style>
    .p_small_box{
        width: 40px;
        padding: 2px;
        font-size: .9rem;
        line-height: 1.5;
        color: #495057;
        background-color: #fff;
        background-clip: padding-box;
        border: 1px solid #ccc;
        border-radius: 0.15rem;
        transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
        height: calc(1.40rem + 1px);
    }
    .p_big_box{
        width: 55px;
        padding: 2px;
        font-size: .95rem;
        line-height: 1.5;
        color: #495057;
        background-color: #fff;
        background-clip: padding-box;
        border: 1px solid #ccc;
        border-radius: 0.15rem;
        transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
        height: calc(1.40rem + 1px);
    }
</style>
<!--// top-bar -->
<div class="wrapper">
    <!-- Sidebar Holder -->
    @include('layouts.sidebar')

    <!-- Page Content Holder -->
    <div id="content">           
        <section class="tables-section">
            <nav aria-label="breadcrumb" style="width:100%;">
                <div class="breadcrumb pagehead1">
                    <span class="headbrand"><i class="fa fa-desktop"></i> Add/Edit First Training Allocation (Max Capacitywise)
                        <span class="scoop-mcaret1"></span>
                    </span>&nbsp;&nbsp;
                    &nbsp;{!! Form::button(' List of First Training Allocation&nbsp;<i class="fa fa-arrow-circle-right"></i>',['id'=>'first_training_venue_allocation_list','class' => 'btn btn-primary-year add-new-button']) !!}  
                
                    <div class="clearfix"></div>
                </div>
            </nav>

            <!-- form -->
            <div class="outer-w3-agile">
                {!! Form::open(['url' => 'firstTrAllocation', 'name' => 'firstTrAllocation', 'class' =>'request-info clearfix form-horizontal', 'id' => 'firstTrAllocation', 'method' => 'post','role'=>'','files' => true]) !!}
                {!! Form::hidden('districtcd', session()->get('districtcd_ppds'),['id'=>'districtcd']) !!}

                <div class="panel-group" id="accordion5">                                                              
                    <div class="panel panel-default">
                        <div class="panel-heading1">
                            <h6 class="panel-title">
                                <a data-toggle="#" data-parent="#accordion5" href="#collapsePV"> <span class="fa fa-minus"></span> <span class="highlight">Add/Edit First Training Allocation (Max Capacitywise)</span></a>
                            </h6>
                        </div>
                        <div id="collapsePV" class="panel-collapse collapse5">
                            <div class="panel-body">                                   
                                <div class="row"> 
                                    <div class='col-sm-3'>
                                        {!! Form::label('zone', 'Zone:', ['class'=>'highlight required']) !!}
                                        <div class="form-group">
                                            <div class=''>
                                                {!! Form::select('zone',[''=>'[Select]'],Null,['id'=>'zone','class'=>'form-control','autocomplete'=>'off']) !!}
                                            </div>
                                        </div>
                                    </div> 
                                    <div class='col-sm-3'>
                                        {!! Form::label('phase', 'Phase:', ['class'=>'highlight required']) !!}
                                       <div class="form-group">
                                           <div class=''>
                                            
                                <select id="phase" class="form-control" name="phase">
                                              
                                               <option value="">[Select]</option>
                                              
                            
                                </select>
                                            </div>
                                       </div>
                                   </div>  
                                    <div class='col-sm-3'>
                                        {!! Form::label('subdivision', 'Sub Division:', ['class'=>'highlight required']) !!}
                                          <!--<span class="highlight">Sub Division</span> -->
                                        <div class="form-group">
                                            <div class=''>
                                                {!! Form::select('subdivision',[''=>'[Select]'],null,['id'=>'subdivision','class'=>'form-control','autocomplete'=>'off']) !!}
                                            </div>
                                        </div>
                                    </div> 

                                    <div class='col-sm-3'>
                                        {!! Form::label('venuename', 'Venue Name:', ['class'=>'highlight required']) !!}
                                        <div class="form-group">
                                            <div class=''>
                                                {!! Form::select('venuename',[''=>'[Select]'],null,['id'=>'venuename','class'=>'form-control','autocomplete'=>'off']) !!}
                                            </div>
                                        </div>
                                    </div>
                                   
                                </div>

                                <div class="row">  
                                    <div class='col-sm-3'>
                                        {!! Form::label('trainingtype', 'Training Type:', ['class'=>'highlight required']) !!}
                                        <div class="form-group">
                                            <div class=''>
                                                {!! Form::select('trainingtype',$training_type['desc'],null,['id'=>'trainingtype','class'=>'form-control','placeholder'=>'[Select]','autocomplete'=>'off']) !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class='col-sm-4'>
                                        {!! Form::label('trainingdatetime', 'Training Date & Time:', ['class'=>'highlight required']) !!}
                                        <div class="form-group">
                                            <div class=''>
                                                {!! Form::select('trainingdatetime',[''=>'[Select]'],null,['id'=>'trainingdatetime','class'=>'form-control','placeholder'=>'[Select]','autocomplete'=>'off']) !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class='col-sm-4'>
                                        {!! Form::label('othersubdiv', 'Assign for other Subdivision:', ['class'=>'highlight']) !!}
                                        <div class="form-group">
                                            <div class=''>
                                                {!! Form::select('othersubdiv',['S'=>'NO'],null,['id'=>'othersubdiv','class'=>'form-control','autocomplete'=>'off']) !!}
                                            </div>
                                        </div>
                                    </div> 
                                    <div class='col-sm-4' id="choiceSub" style="display:none;">
                                        {!! Form::label('choicesubdiv', 'Other Subdivision:', ['class'=>'highlight required']) !!}
                                        <div class="form-group">
                                            <div class=''>
                                                {!! Form::select('choicesubdiv',[''=>'[Select]'],null,['id'=>'choicesubdiv','class'=>'form-control','placeholder'=>'[Select]','autocomplete'=>'off']) !!}
                                            </div>
                                        </div>
                                    </div>
                                </div> 

                                <div class="row"> 
                                    <div class='col-sm-12'>
                                        <div class="form-group">
                                            <div id='subVenueDetails'></div>
                                        </div>
                                    </div>

                                </div> 
                                <div class="row">                  
                                    <div class='col-sm-12'>
                                        <div class="form-group text-right permit">                            	
                                            {{ Form::button('Submit', ['class' => 'btn btn-info', 'type' => 'submit','id'=>'Submit']) }}
                                            {{ Form::button('Reset', ['class' => 'btn btn-success' ,'type' => 'reset','id'=>'reset']) }}
                                        </div>
                                    </div>
                                </div>     

                            </div>                              
                        </div>
                    </div>  
                </div>
                {!! Form::close() !!}             
            </div>
            <!--// form -->

        </section>

    </div>
</div>
<!-- Copyright -->
<script type="text/javascript">
    $(document).ready(function () {
         $("#first_training_venue_allocation_list").click(function () {
            window.location.href = "first_training_venue_allocation_list";
        });
        getZoneDetails();
        $("select[name='othersubdiv'").append('<option value="D">YES</option>');
        $("select[name='othersubdiv'").append('<option value="O">Office Assembly</option>');
 $("select[name='othersubdiv'").append('<option value="H">Home Assembly</option>');
        $("#othersubdiv").on('change', function () {
            var othersubdiv = $("#othersubdiv").val();
            if (othersubdiv == "D") {
                $("#choiceSub").show();
                other_subdivision();
            } else {
                $("#choiceSub").hide();
            }
        });
        $('select[name="zone"]').on('change', function () {
            change_subdivision();
           // other_subdivision();
        });

        //getTrainingDateTime();
        $("#reset").click(function () {
            location.reload(true);
        });
        $('select[name="subdivision"]').on('change', function () {
            change_venueName();
        });
        $('select[name="venuename"]').on('change', function () {
            get_SubvenueName();
        });
        $('select[name="trainingdatetime"]').on('change', function () {
            get_SubvenueName();
        });

        $('#firstTrAllocation').bootstrapValidator({

            message: 'This value is not valid',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                zone: {
                    validators: {
                        notEmpty: {
                            message: 'Zone is required'
                        }
                    }
                },
                phase: {
                validators: {
                    notEmpty: {
                        message: 'Phase is required'
                    }
                }
            },
                subdivision: {
                    validators: {
                        notEmpty: {
                            message: 'Subdivision is required'
                        }
                    }
                },
                venuename: {
                    validators: {
                        notEmpty: {
                            message: 'Venue Name is required'
                        }
                    }
                },
                trainingtype: {
                    validators: {
                        notEmpty: {
                            message: 'Training Type is required'
                        }
                    }
                },
                trainingdatetime: {
                    validators: {
                        notEmpty: {
                            message: 'Training Date & Time is required'
                        }
                    }
                },
                choicesubdiv: {
                    validators: {
                        notEmpty: {
                            message: 'Other Subdivision is required'
                        }
                    }
                }

            }

        }).on('success.form.bv', function (e) {
            e.preventDefault();
            var action = $(this).attr('action');
            var type = $(this).attr('method');
            var formData = new FormData($(this)[0]);
            $(".se-pre-con").fadeIn("slow");
            $.ajax({
                type: type,
                url: action,
                data: formData,
                processData: false,
                contentType: false,
                dataType: "json",
                success: function (data) {
                    $(".se-pre-con").fadeOut("slow");

                    if (data.status == 1) {
                        var msg = "Record(s) saved successfully";
                        $.confirm({
                            title: 'Success!',
                            type: 'green',
                            icon: 'fa fa-check',
                            content: msg,
                            buttons: {
                                ok: function () {
                                    location.reload(true);
                                }

                            }
                        });

                    } else if (data.status == 0) {
                        $.alert({
                            title: 'Error!!',
                            type: 'red',
                            icon: 'fa fa-exclamation-triangle',
                            content: 'Assign value does not match'
                        });
                        $("#Submit").attr('disabled', false);
                    } else {
                        //var msg="Record(s) updated successfully";
                        var msg = "Venue can not be same in same Training Date & Time";
                        $.confirm({
                            title: 'Error!!',
                            type: 'red',
                            icon: 'fa fa-exclamation-triangle',
                            content: msg,
                            buttons: {
                                ok: function () {
                                    location.reload(true);
                                }

                            }
                        });

                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $(".se-pre-con").fadeOut("slow");
                    var msg = "";
                    if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                        msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                    } else {
                        if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                            msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                        } else {
                            msg += "Error(s):<strong><ul>";
                            $.each(jqXHR.responseJSON, function (key, value) {
                                msg += "<li>" + value + "</li>";
                            });
                            msg += "</ul></strong>";
                        }
                    }

                    $.alert({
                        title: 'Error!!',
                        type: 'red',
                        icon: 'fa fa-exclamation-triangle',
                        content: msg
                    });
                    $("#Submit").attr('disabled', false);
                }
            });
        });
    });

    function get_SubvenueName() {
        var venuename = $("#venuename").val();
        var trainingdatetime = $("#trainingdatetime").val();
        if (venuename == "" || trainingdatetime == "") {
            return false;
        }
        var token = $("input[name='_token']").val();
        $(".se-pre-con").fadeIn("slow");
        $.ajax({
            type: 'POST',
            url: 'getSubVenueRoomWise',
            data: {venuename: venuename, trainingdatetime: trainingdatetime, _token: token},
            dataType: 'json',
            success: function (data) {
                $(".se-pre-con").fadeOut("slow");
                $("#subVenueDetails").html("");
                if (data.status == 1)
                {
                    if (data.options.length == 1) {
                        var str = '<input type="hidden" id="row_count" name="row_count" value="' + data.options[0].sub_venue_detail_brief.length + '"> '
                        str += "<table class='table table-bordered table-striped' width='100%' style='border-top: 2px solid #4cae4c;'>";
                        str += ' <tr style="background-color: #f5f8fa"><th align="left" width="22%">Sub venue</th><th align="left" width="8%">&nbsp;PR</th><th align="left" width="8%">&nbsp;P1</th><th align="left" width="8%">&nbsp;P2</th><th align="left" width="8%">&nbsp;P3</th><th align="left" width="8%">&nbsp;PA</th><th align="left" width="8%">&nbsp;PB</th><th align="left" width="10%">&nbsp;Max CP</th><th align="left" width="10%">&nbsp;Assigned</th><th align="left" width="10%">&nbsp;Left</th></tr>';
                        str += ' <tr style="background-color: #FDF5E6"><td align="left">&nbsp;Total</td><td align="left">&nbsp;<input type="text" id="pr" class="p_small_box" maxlength="4"/></td><th align="left">&nbsp;<input type="text" id="p1" class="p_small_box" maxlength="4"/></th><th align="left">&nbsp;<input type="text" id="p2" class="p_small_box" maxlength="4"/></th><th align="left">&nbsp;<input type="text" id="p3" class="p_small_box" maxlength="4"/></th><th align="left">&nbsp;<input type="text" id="pa" class="p_small_box" maxlength="4"/></th><th align="left">&nbsp;<input type="text" id="pb" class="p_small_box" maxlength="4"/></th><th align="left">&nbsp;<input type="text" id="maxcp" name="maxcp" class="p_big_box" readonly="true" value="' + data.options[0].maximumcapacity + '"/></th><th align="left">&nbsp;<input type="text" id="assign" name="assign" class="p_big_box" readonly="true"/></th><th align="left">&nbsp;<input type="text" id="left" class="p_big_box" readonly="true"  value="' + data.options[0].maximumcapacity + '"/></th></tr>';
                        for (var i = 0; i < data.options[0].sub_venue_detail_brief.length; i++) {
                            var c = parseInt(i) + 1;
                            var fetch_p1_val = "";
                            var fetch_p2_val = "";
                            var fetch_p3_val = "";
                            var fetch_pr_val = "";
                            var fetch_pa_val = "";
                            var fetch_pb_val = "";
                            if (data.options[0].sub_venue_detail_brief[i].pr == null || data.options[0].sub_venue_detail_brief[i].pr == "") {
                                fetch_pr_val = "";
                            } else {
                                fetch_pr_val = data.options[0].sub_venue_detail_brief[i].pr;
                            }
                            if (data.options[0].sub_venue_detail_brief[i].p1 == null || data.options[0].sub_venue_detail_brief[i].p1 == "") {
                                fetch_p1_val = "";
                            } else {
                                fetch_p1_val = data.options[0].sub_venue_detail_brief[i].p1;
                            }
                            if (data.options[0].sub_venue_detail_brief[i].p2 == null || data.options[0].sub_venue_detail_brief[i].p2 == "") {
                                fetch_p2_val = "";
                            } else {
                                fetch_p2_val = data.options[0].sub_venue_detail_brief[i].p2;
                            }
                            if (data.options[0].sub_venue_detail_brief[i].p3 == null || data.options[0].sub_venue_detail_brief[i].p3 == "") {
                                fetch_p3_val = "";
                            } else {
                                fetch_p3_val = data.options[0].sub_venue_detail_brief[i].p3;
                            }
                            if (data.options[0].sub_venue_detail_brief[i].pa == null || data.options[0].sub_venue_detail_brief[i].pa == "") {
                                fetch_pa_val = "";
                            } else {
                                fetch_pa_val = data.options[0].sub_venue_detail_brief[i].pa;
                            }
                            if (data.options[0].sub_venue_detail_brief[i].pb == null || data.options[0].sub_venue_detail_brief[i].pb == "") {
                                fetch_pb_val = "";
                            } else {
                                fetch_pb_val = data.options[0].sub_venue_detail_brief[i].pb;
                            }
                            str += '<tr>';
                            str += '<input type="hidden" id="subVenue' + c + '" name="subVenue' + c + '"  value="' + data.options[0].sub_venue_detail_brief[i].subvenue_cd + '" >';
                            str += '<td align="left">&nbsp;' + data.options[0].sub_venue_detail_brief[i].subvenue + '</td>';
                            str += '<td align="left">&nbsp;<input type="text" id="s_pr' + c + '" name="s_pr' + c + '" class="p_small_box" value="' + fetch_pr_val + '" maxlength="4" onChange="SubPR(' + c + ');"/></td>';
                            str += '<td align="left">&nbsp;<input type="text" id="s_p1' + c + '" name="s_p1' + c + '" class="p_small_box" value="' + fetch_p1_val + '" maxlength="4" onChange="SubP1(' + c + ');"/></td>';
                            str += '<td align="left">&nbsp;<input type="text" id="s_p2' + c + '" name="s_p2' + c + '" class="p_small_box" value="' + fetch_p2_val + '" maxlength="4" onChange="SubP2(' + c + ');"/></td>';
                            str += '<td align="left">&nbsp;<input type="text" id="s_p3' + c + '" name="s_p3' + c + '" class="p_small_box" value="' + fetch_p3_val + '" maxlength="4" onChange="SubP3(' + c + ');"/></td>';
                            str += '<td align="left">&nbsp;<input type="text" id="s_pa' + c + '" name="s_pa' + c + '" class="p_small_box" value="' + fetch_pa_val + '" maxlength="4" onChange="SubPA(' + c + ');"/></td>';
                            str += '<td align="left">&nbsp;<input type="text" id="s_pb' + c + '" name="s_pb' + c + '" class="p_small_box" value="' + fetch_pb_val + '" maxlength="4" onChange="SubPB(' + c + ');"/></td>';

                            str += '<td align="left">&nbsp;<input type="text" id="s_maxcp' + c + '" name="s_maxcp' + c + '" class="p_big_box" value="' + data.options[0].sub_venue_detail_brief[i].maxcapacity + '" readOnly="true"/></td>';
                            str += '<td align="left">&nbsp;<input type="text" id="s_assign' + c + '" name="s_assign' + c + '" class="p_big_box" value="" readOnly="true"/></td>';
                            str += '<td align="left">&nbsp;<input type="text" id="s_left' + c + '" name="s_left' + c + '" class="p_big_box" value="' + data.options[0].sub_venue_detail_brief[i].maxcapacity + '" readOnly="true"/></td>';
                            str += '</tr>';

                        }
                        str += "</table>";
                    } else {
                        var str = "<span style='color: red;'>Sub Venue is required</span>";
                    }
                } else {
                    var str = "<span style='color: red;'>Sub Venue is required</span>";
                }
                $("#subVenueDetails").append(str);

                PP_Recordmanage();
                //::::::::::::::For PR:::::::::::://
                $("#pr").on('change', function () {
                    var pr = $("#pr").val();
                    var p1 = $("#p1").val();
                    var p2 = $("#p2").val();
                    var p3 = $("#p3").val();
                    var pa = $("#pa").val();
                    var pb = $("#pb").val();
                    var maxcp = $("#maxcp").val();
                    var totalpp = Math.round((+pr) + (+p1) + (+p2) + (+p3) + (+pa) + (+pb));

                    var Countpr = pr;
                    var rwCnt = $("#row_count").val();
                    for (var p = 0; p < rwCnt; p++) {
                        var q = parseInt(p) + 1;
                        $("#s_pr" + q).val("");
                    }
                    
                    PP_Recordmanage();
                    for (var j = 0; j < rwCnt; j++) {
                        var k = parseInt(j) + 1;
                        var count = 0;
                        var s_maxcp = $("#s_maxcp" + k).val();
                        var s_assign = $("#s_assign" + k).val();
                        if (totalpp > maxcp) {
                            count++;
                            var new_cap = "";
                        } else {
                            var sa_left = Math.round((+s_maxcp) - (+s_assign));
                            if (sa_left < Countpr)
                            {
                                var new_cap = sa_left;
                                Countpr = Countpr - new_cap;
                            } else
                            {
                                var new_cap = Countpr;
                                Countpr = Countpr - new_cap;
                                if (new_cap == 0)
                                {
                                    break;
                                }
                            }

                        }
                        $("#s_pr" + k).val(new_cap);
                    }
                    if (count > 0) {
                        $.alert({
                            title: 'Error!!',
                            type: 'red',
                            icon: 'fa fa-exclamation-triangle',
                            content: 'Total No [' + totalpp + '] must not be greater than Max CP [' + maxcp + ']'
                        });
                        $("#pr").val("");
//                totalAssignLeft();

                    }
//            else{
//               $("#assign").val(totalpp);
//               var to_left=Math.round((+maxcp)-(+totalpp));
//               $("#left").val(to_left);
//            }
                    PP_Recordmanage();
                });
                //::::::::::::::For P1:::::::::::://
                $("#p1").on('change', function () {
                    var pr = $("#pr").val();
                    var p1 = $("#p1").val();
                    var p2 = $("#p2").val();
                    var p3 = $("#p3").val();
                    var pa = $("#pa").val();
                    var pb = $("#pb").val();
                    var maxcp = $("#maxcp").val();
                    var totalpp = Math.round((+pr) + (+p1) + (+p2) + (+p3) + (+pa) + (+pb));

                    var Countpr = p1;
                    var rwCnt = $("#row_count").val();
                    for (var p = 0; p < rwCnt; p++) {
                        var q = parseInt(p) + 1;
                        $("#s_p1" + q).val("");
                    }
                    PP_Recordmanage();
                    for (var j = 0; j < rwCnt; j++) {
                        var k = parseInt(j) + 1;
                        var count = 0;
                        var s_maxcp = $("#s_maxcp" + k).val();
                        var s_assign = $("#s_assign" + k).val();
                        if (totalpp > maxcp) {
                            count++;
                            var new_cap = "";
                        } else {
                            var sa_left = Math.round((+s_maxcp) - (+s_assign));
                            if (sa_left < Countpr)
                            {
                                var new_cap = sa_left;
                                Countpr = Countpr - new_cap;
                            } else
                            {
                                var new_cap = Countpr;
                                Countpr = Countpr - new_cap;
                                if (new_cap == 0)
                                {
                                    break;
                                }
                            }

                        }
                        $("#s_p1" + k).val(new_cap);
                    }
                    if (count > 0) {
                        $.alert({
                            title: 'Error!!',
                            type: 'red',
                            icon: 'fa fa-exclamation-triangle',
                            content: 'Total No [' + totalpp + '] must not be greater than Max CP [' + maxcp + ']'
                        });
                        $("#p1").val("");
//                totalAssignLeft();

                    }
//            else{
//               $("#assign").val(totalpp);
//               var to_left=Math.round((+maxcp)-(+totalpp));
//               $("#left").val(to_left);
//            }
                    PP_Recordmanage();
                });
                //::::::::::::::For P2:::::::::::://
                $("#p2").on('change', function () {
                    var pr = $("#pr").val();
                    var p1 = $("#p1").val();
                    var p2 = $("#p2").val();
                    var p3 = $("#p3").val();
                    var pa = $("#pa").val();
                    var pb = $("#pb").val();
                    var maxcp = $("#maxcp").val();
                    var totalpp = Math.round((+pr) + (+p1) + (+p2) + (+p3) + (+pa) + (+pb));

                    var Countpr = p2;
                    var rwCnt = $("#row_count").val();
                    for (var p = 0; p < rwCnt; p++) {
                        var q = parseInt(p) + 1;
                        $("#s_p2" + q).val("");
                    }
                    PP_Recordmanage();
                    for (var j = 0; j < rwCnt; j++) {
                        var k = parseInt(j) + 1;
                        var count = 0;
                        var s_maxcp = $("#s_maxcp" + k).val();
                        var s_assign = $("#s_assign" + k).val();
                        if (totalpp > maxcp) {
                            count++;
                            var new_cap = "";
                        } else {
                            var sa_left = Math.round((+s_maxcp) - (+s_assign));
                            if (sa_left < Countpr)
                            {
                                var new_cap = sa_left;
                                Countpr = Countpr - new_cap;
                            } else
                            {
                                var new_cap = Countpr;
                                Countpr = Countpr - new_cap;
                                if (new_cap == 0)
                                {
                                    break;
                                }
                            }

                        }
                        $("#s_p2" + k).val(new_cap);
                    }
                    if (count > 0) {
                        $.alert({
                            title: 'Error!!',
                            type: 'red',
                            icon: 'fa fa-exclamation-triangle',
                            content: 'Total No [' + totalpp + '] must not be greater than Max CP [' + maxcp + ']'
                        });
                        $("#p2").val("");
//                totalAssignLeft();

                    }
//            else{
//               $("#assign").val(totalpp);
//               var to_left=Math.round((+maxcp)-(+totalpp));
//               $("#left").val(to_left);
//            }
                    PP_Recordmanage();
                });
                //::::::::::::::For P3:::::::::::://
                $("#p3").on('change', function () {
                    var pr = $("#pr").val();
                    var p1 = $("#p1").val();
                    var p2 = $("#p2").val();
                    var p3 = $("#p3").val();
                    var pa = $("#pa").val();
                    var pb = $("#pb").val();
                    var maxcp = $("#maxcp").val();
                    var totalpp = Math.round((+pr) + (+p1) + (+p2) + (+p3) + (+pa) + (+pb));

                    var Countpr = p3;
                    var rwCnt = $("#row_count").val();
                    for (var p = 0; p < rwCnt; p++) {
                        var q = parseInt(p) + 1;
                        $("#s_p3" + q).val("");
                    }
                    PP_Recordmanage();
                    for (var j = 0; j < rwCnt; j++) {
                        var k = parseInt(j) + 1;
                        var count = 0;
                        var s_maxcp = $("#s_maxcp" + k).val();
                        var s_assign = $("#s_assign" + k).val();
                        if (totalpp > maxcp) {
                            count++;
                            var new_cap = "";
                        } else {
                            var sa_left = Math.round((+s_maxcp) - (+s_assign));
                            if (sa_left < Countpr)
                            {
                                var new_cap = sa_left;
                                Countpr = Countpr - new_cap;
                            } else
                            {
                                var new_cap = Countpr;
                                Countpr = Countpr - new_cap;
                                if (new_cap == 0)
                                {
                                    break;
                                }
                            }

                        }
                        $("#s_p3" + k).val(new_cap);
                    }
                    if (count > 0) {
                        $.alert({
                            title: 'Error!!',
                            type: 'red',
                            icon: 'fa fa-exclamation-triangle',
                            content: 'Total No [' + totalpp + '] must not be greater than Max CP [' + maxcp + ']'
                        });
                        $("#p3").val("");
//                totalAssignLeft();

                    }
//            else{
//               $("#assign").val(totalpp);
//               var to_left=Math.round((+maxcp)-(+totalpp));
//               $("#left").val(to_left);
//            }
                    PP_Recordmanage();
                });
                //::::::::::::::For PA:::::::::::://
                $("#pa").on('change', function () {
                    var pr = $("#pr").val();
                    var p1 = $("#p1").val();
                    var p2 = $("#p2").val();
                    var p3 = $("#p3").val();
                    var pa = $("#pa").val();
                    var pb = $("#pb").val();
                    var maxcp = $("#maxcp").val();
                    var totalpp = Math.round((+pr) + (+p1) + (+p2) + (+p3) + (+pa) + (+pb));

                    var Countpr = pa;
                    var rwCnt = $("#row_count").val();
                    for (var p = 0; p < rwCnt; p++) {
                        var q = parseInt(p) + 1;
                        $("#s_pa" + q).val("");
                    }
                    PP_Recordmanage();
                    for (var j = 0; j < rwCnt; j++) {
                        var k = parseInt(j) + 1;
                        var count = 0;
                        var s_maxcp = $("#s_maxcp" + k).val();
                        var s_assign = $("#s_assign" + k).val();
                        if (totalpp > maxcp) {
                            count++;
                            var new_cap = "";
                        } else {
                            var sa_left = Math.round((+s_maxcp) - (+s_assign));
                            if (sa_left < Countpr)
                            {
                                var new_cap = sa_left;
                                Countpr = Countpr - new_cap;
                            } else
                            {
                                var new_cap = Countpr;
                                Countpr = Countpr - new_cap;
                                if (new_cap == 0)
                                {
                                    break;
                                }
                            }

                        }
                        $("#s_pa" + k).val(new_cap);
                    }
                    if (count > 0) {
                        $.alert({
                            title: 'Error!!',
                            type: 'red',
                            icon: 'fa fa-exclamation-triangle',
                            content: 'Total No [' + totalpp + '] must not be greater than Max CP [' + maxcp + ']'
                        });
                        $("#pa").val("");
//                totalAssignLeft();

                    }
//            else{
//               $("#assign").val(totalpp);
//               var to_left=Math.round((+maxcp)-(+totalpp));
//               $("#left").val(to_left);
//            }
                    PP_Recordmanage();
                });
                //::::::::::::::For PB:::::::::::://
                $("#pb").on('change', function () {
                    var pr = $("#pr").val();
                    var p1 = $("#p1").val();
                    var p2 = $("#p2").val();
                    var p3 = $("#p3").val();
                    var pa = $("#pa").val();
                    var pb = $("#pb").val();
                    var maxcp = $("#maxcp").val();
                    var totalpp = Math.round((+pr) + (+p1) + (+p2) + (+p3) + (+pa) + (+pb));

                    var Countpr = pb;
                    var rwCnt = $("#row_count").val();
                    for (var p = 0; p < rwCnt; p++) {
                        var q = parseInt(p) + 1;
                        $("#s_pb" + q).val("");
                    }
                    PP_Recordmanage();
                    for (var j = 0; j < rwCnt; j++) {
                        var k = parseInt(j) + 1;
                        var count = 0;
                        var s_maxcp = $("#s_maxcp" + k).val();
                        var s_assign = $("#s_assign" + k).val();
                        if (totalpp > maxcp) {
                            count++;
                            var new_cap = "";
                        } else {
                            var sa_left = Math.round((+s_maxcp) - (+s_assign));
                            if (sa_left < Countpr)
                            {
                                var new_cap = sa_left;
                                Countpr = Countpr - new_cap;
                            } else
                            {
                                var new_cap = Countpr;
                                Countpr = Countpr - new_cap;
                                if (new_cap == 0)
                                {
                                    break;
                                }
                            }

                        }
                        $("#s_pb" + k).val(new_cap);
                    }
                    if (count > 0) {
                        $.alert({
                            title: 'Error!!',
                            type: 'red',
                            icon: 'fa fa-exclamation-triangle',
                            content: 'Total No [' + totalpp + '] must not be greater than Max CP [' + maxcp + ']'
                        });
                        $("#pb").val("");
//                totalAssignLeft();

                    }
//            else{
//               $("#assign").val(totalpp);
//               var to_left=Math.round((+maxcp)-(+totalpp));
//               $("#left").val(to_left);
//            }
                    PP_Recordmanage();
                });
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $(".se-pre-con").fadeOut("slow");
                var msg = "";
                if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                    msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                } else {
                    if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                        msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                    } else {
                        msg += "Error(s):<strong><ul>";
                        $.each(jqXHR.responseJSON, function (key, value) {
                            msg += "<li>" + value + "</li>";
                        });
                        msg += "</ul></strong>";
                    }
                }
                $.alert({
                    title: 'Error!!',
                    type: 'red',
                    icon: 'fa fa-exclamation-triangle',
                    content: msg
                });
            }
        });
    }
    function getZoneDetails() {
        var forDist = $("#districtcd").val();
        var token = $("input[name='_token']").val();
        $(".se-pre-con").fadeIn("slow");
        $.ajax({
            url: "getZoneDetails",
            method: 'POST',
            data: {forDist: forDist, _token: token},
            success: function (data) {//alert(data.options);
                $(".se-pre-con").fadeOut("slow");
                $("select[name='zone'").html('');
                if (data.status == 1)
                {
                    $("select[name='zone'").append('<option value>[Select]</option>');
                    $.each(data.options, function (k, v)
                    {
                        $("select[name='zone'").append('<option value=' + k + '>' + v + '</option>');
                    });
                    // getMemberDetails();
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $(".se-pre-con").fadeOut("slow");
                var msg = "";
                if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                    msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                } else {
                    if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                        msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                    } else {
                        msg += "Error(s):<strong><ul>";
                        $.each(jqXHR.responseJSON, function (key, value) {
                            msg += "<li>" + value + "</li>";
                        });
                        msg += "</ul></strong>";
                    }
                }
                $.alert({
                    title: 'Error!!',
                    type: 'red',
                    icon: 'fa fa-warning',
                    content: msg
                });
            },
            complete: function () {
                getTrainingDateTime();
            }
        });
    }
    function other_subdivision() {
        var zone = $("#zone").val();
        var token = $("input[name='_token']").val();
        // $(".se-pre-con").fadeIn("slow");
        $.ajax({
            url: "getOtherSubdivisionData",
            method: 'POST',
            data: {zone: zone, _token: token},
            success: function (data) {//alert(data.options);
                //   $(".se-pre-con").fadeOut("slow");
                $("select[name='choicesubdiv'").html('');

                if (data.status == 1)
                {
                    $("select[name='choicesubdiv'").append('<option value>[Select]</option>');
                    $.each(data.options, function (k, v)
                    {
                        $("select[name='choicesubdiv'").append('<option value=' + k + '>' + v + '</option>');
                    });
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $(".se-pre-con").fadeOut("slow");
                var msg = "";
                if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                    msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                } else {
                    if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                        msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                    } else {
                        msg += "Error(s):<strong><ul>";
                        $.each(jqXHR.responseJSON, function (key, value) {
                            msg += "<li>" + value + "</li>";
                        });
                        msg += "</ul></strong>";
                    }
                }
                $.alert({
                    title: 'Error!!',
                    type: 'red',
                    icon: 'fa fa-exclamation-triangle',
                    content: msg
                });
            }
        });
    }
    function change_subdivision() {
        var zone = $("#zone").val();
        var forDist = $("#districtcd").val();
        var token = $("input[name='_token']").val();
        // $(".se-pre-con").fadeIn("slow");
        $.ajax({
            url: "getZonewiseSubdivisionall_Data",
            method: 'POST',
            data: {zone: zone,forDist: forDist, _token: token},
            success: function (data) {//alert(data.options);
                //   $(".se-pre-con").fadeOut("slow");
                $("select[name='subdivision'").html('');

                if (data.status == 1)
                {
                    $("select[name='subdivision'").append('<option value>[Select]</option>');
                    $.each(data.options, function (k, v)
                    {
                        $("select[name='subdivision'").append('<option value=' + k + '>' + v + '</option>');
                    });
                    //$("select[name='subdivision'").html(data.options);
                    // change_officeName();
                    // getMemberDetails();
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $(".se-pre-con").fadeOut("slow");
                var msg = "";
                if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                    msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                } else {
                    if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                        msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                    } else {
                        msg += "Error(s):<strong><ul>";
                        $.each(jqXHR.responseJSON, function (key, value) {
                            msg += "<li>" + value + "</li>";
                        });
                        msg += "</ul></strong>";
                    }
                }
                $.alert({
                    title: 'Error!!',
                    type: 'red',
                    icon: 'fa fa-exclamation-triangle',
                    content: msg
                });
            },
            complete: function () {
                other_subdivision();
                getZonePhasedata(); 
            }
        });
    }
    function getTrainingDateTime() {
        var forDist = $("#districtcd").val();
        var token = $("input[name='_token']").val();
       // $(".se-pre-con").fadeIn("slow");
        $.ajax({
            url: "getTrainingDateTime",
            method: 'POST',
            data: {forDist: forDist, _token: token},
            success: function (data) {//alert(data.options);
             //   $(".se-pre-con").fadeOut("slow");
                $("select[name='trainingdatetime'").html('');
                if (data.status == 1)
                {
                    $("select[name='trainingdatetime'").html(data.options);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
            //    $(".se-pre-con").fadeOut("slow");
                var msg = "";
                if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                    msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                } else {
                    if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                        msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                    } else {
                        msg += "Error(s):<strong><ul>";
                        $.each(jqXHR.responseJSON, function (key, value) {
                            msg += "<li>" + value + "</li>";
                        });
                        msg += "</ul></strong>";
                    }
                }
                $.alert({
                    title: 'Error!!',
                    type: 'red',
                    icon: 'fa fa-warning',
                    content: msg
                });
            }
        });
    }
    function change_venueName() {
        var subdivision = $("#subdivision").val();
        var token = $("input[name='_token']").val();
        var flag=1;
        $(".se-pre-con").fadeIn("slow");
        $.ajax({
            url: "getVenueName",
            method: 'POST',
            data: {subdivision: subdivision,flag:flag, _token: token},
            success: function (data) {//alert(data.options);
                $(".se-pre-con").fadeOut("slow");
                $("select[name='venuename'").html('');

                if (data.status == 1)
                {
                    $("select[name='venuename'").append('<option value>[Select]</option>');
                    $.each(data.options, function (k, v)
                    {
                        $("select[name='venuename'").append('<option value=' + k + '>' + v + '</option>');
                    });
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $(".se-pre-con").fadeOut("slow");
                var msg = "";
                if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                    msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                } else {
                    if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                        msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                    } else {
                        msg += "Error(s):<strong><ul>";
                        $.each(jqXHR.responseJSON, function (key, value) {
                            msg += "<li>" + value + "</li>";
                        });
                        msg += "</ul></strong>";
                    }
                }
                $.alert({
                    title: 'Error!!',
                    type: 'red',
                    icon: 'fa fa-exclamation-triangle',
                    content: msg
                });
            }
        });
    }

    function SubPR(m) {
        var s_maxcp = $("#s_maxcp" + m).val();
        var s_pr = $("#s_pr" + m).val();
        var s_p1 = $("#s_p1" + m).val();
        var s_p2 = $("#s_p2" + m).val();
        var s_p3 = $("#s_p3" + m).val();
        var s_pa = $("#s_pa" + m).val();
        var s_pb = $("#s_pb" + m).val();
        var new_assign = Math.round((+s_p1) + (+s_p2) + (+s_p3) + (+s_pa) + (+s_pb) + (+s_pr));
        if (new_assign > s_maxcp) {
            $.alert({
                title: 'Error!!',
                type: 'red',
                icon: 'fa fa-exclamation-triangle',
                content: 'Total no [' + new_assign + '] must not be greater than Max CP [' + s_maxcp + ']'
            });
            $("#s_pr" + m).val("");
            var new_assign = Math.round((+s_p1) + (+s_p2) + (+s_p3) + (+s_pa) + (+s_pb));
            $("#s_assign" + m).val(new_assign);
            var new_left = Math.round((+s_maxcp) - (+new_assign));
            $("#s_left" + m).val(new_left);

        } else {
            $("#s_assign" + m).val(new_assign);
            var new_left = Math.round((+s_maxcp) - (+new_assign));
            $("#s_left" + m).val(new_left);
        }
        var TotalrwCnt = $("#row_count").val();
        var cntPR = 0;
        for (var l = 0; l < TotalrwCnt; l++) {
            var n = parseInt(l) + 1;
            var s_pr_n = $("#s_pr" + n).val();
            cntPR = Math.round((+cntPR) + (+s_pr_n));
        }
        $("#pr").val(cntPR);
        totalAssignLeft();
    }
    function SubP1(m) {
        var s_maxcp = $("#s_maxcp" + m).val();
        var s_pr = $("#s_pr" + m).val();
        var s_p1 = $("#s_p1" + m).val();
        var s_p2 = $("#s_p2" + m).val();
        var s_p3 = $("#s_p3" + m).val();
        var s_pa = $("#s_pa" + m).val();
        var s_pb = $("#s_pb" + m).val();
        var new_assign = Math.round((+s_p1) + (+s_p2) + (+s_p3) + (+s_pa) + (+s_pb) + (+s_pr));
        if (new_assign > s_maxcp) {
            $.alert({
                title: 'Error!!',
                type: 'red',
                icon: 'fa fa-exclamation-triangle',
                content: 'Total no [' + new_assign + '] must not be greater than Max CP [' + s_maxcp + ']'
            });
            $("#s_p1" + m).val("");
            var new_assign = Math.round((+s_pr) + (+s_p2) + (+s_p3) + (+s_pa) + (+s_pb));
            $("#s_assign" + m).val(new_assign);
            var new_left = Math.round((+s_maxcp) - (+new_assign));
            $("#s_left" + m).val(new_left);

        } else {
            $("#s_assign" + m).val(new_assign);
            var new_left = Math.round((+s_maxcp) - (+new_assign));
            $("#s_left" + m).val(new_left);
        }
        var TotalrwCnt = $("#row_count").val();
        var cntPR = 0;
        for (var l = 0; l < TotalrwCnt; l++) {
            var n = parseInt(l) + 1;
            var s_pr_n = $("#s_p1" + n).val();
            cntPR = Math.round((+cntPR) + (+s_pr_n));
        }
        $("#p1").val(cntPR);
        totalAssignLeft();
    }
    function SubP2(m) {
        var s_maxcp = $("#s_maxcp" + m).val();
        var s_pr = $("#s_pr" + m).val();
        var s_p1 = $("#s_p1" + m).val();
        var s_p2 = $("#s_p2" + m).val();
        var s_p3 = $("#s_p3" + m).val();
        var s_pa = $("#s_pa" + m).val();
        var s_pb = $("#s_pb" + m).val();
        var new_assign = Math.round((+s_p1) + (+s_p2) + (+s_p3) + (+s_pa) + (+s_pb) + (+s_pr));
        if (new_assign > s_maxcp) {
            $.alert({
                title: 'Error!!',
                type: 'red',
                icon: 'fa fa-exclamation-triangle',
                content: 'Total no [' + new_assign + '] must not be greater than Max CP [' + s_maxcp + ']'
            });
            $("#s_p2" + m).val("");
            var new_assign = Math.round((+s_pr) + (+s_p1) + (+s_p3) + (+s_pa) + (+s_pb));
            $("#s_assign" + m).val(new_assign);
            var new_left = Math.round((+s_maxcp) - (+new_assign));
            $("#s_left" + m).val(new_left);

        } else {
            $("#s_assign" + m).val(new_assign);
            var new_left = Math.round((+s_maxcp) - (+new_assign));
            $("#s_left" + m).val(new_left);
        }
        var TotalrwCnt = $("#row_count").val();
        var cntPR = 0;
        for (var l = 0; l < TotalrwCnt; l++) {
            var n = parseInt(l) + 1;
            var s_pr_n = $("#s_p2" + n).val();
            cntPR = Math.round((+cntPR) + (+s_pr_n));
        }
        $("#p2").val(cntPR);
        totalAssignLeft();
    }
    function SubP3(m) {
        var s_maxcp = $("#s_maxcp" + m).val();
        var s_pr = $("#s_pr" + m).val();
        var s_p1 = $("#s_p1" + m).val();
        var s_p2 = $("#s_p2" + m).val();
        var s_p3 = $("#s_p3" + m).val();
        var s_pa = $("#s_pa" + m).val();
        var s_pb = $("#s_pb" + m).val();
        var new_assign = Math.round((+s_p1) + (+s_p2) + (+s_p3) + (+s_pa) + (+s_pb) + (+s_pr));
        if (new_assign > s_maxcp) {
            $.alert({
                title: 'Error!!',
                type: 'red',
                icon: 'fa fa-exclamation-triangle',
                content: 'Total no [' + new_assign + '] must not be greater than Max CP [' + s_maxcp + ']'
            });
            $("#s_p3" + m).val("");
            var new_assign = Math.round((+s_pr) + (+s_p1) + (+s_p2) + (+s_pa) + (+s_pb));
            $("#s_assign" + m).val(new_assign);
            var new_left = Math.round((+s_maxcp) - (+new_assign));
            $("#s_left" + m).val(new_left);

        } else {
            $("#s_assign" + m).val(new_assign);
            var new_left = Math.round((+s_maxcp) - (+new_assign));
            $("#s_left" + m).val(new_left);
        }
        var TotalrwCnt = $("#row_count").val();
        var cntPR = 0;
        for (var l = 0; l < TotalrwCnt; l++) {
            var n = parseInt(l) + 1;
            var s_pr_n = $("#s_p3" + n).val();
            cntPR = Math.round((+cntPR) + (+s_pr_n));
        }
        $("#p3").val(cntPR);
        totalAssignLeft();
    }
    function SubPA(m) {
        var s_maxcp = $("#s_maxcp" + m).val();
        var s_pr = $("#s_pr" + m).val();
        var s_p1 = $("#s_p1" + m).val();
        var s_p2 = $("#s_p2" + m).val();
        var s_p3 = $("#s_p3" + m).val();
        var s_pa = $("#s_pa" + m).val();
        var s_pb = $("#s_pb" + m).val();
        var new_assign = Math.round((+s_p1) + (+s_p2) + (+s_p3) + (+s_pa) + (+s_pb) + (+s_pr));
        if (new_assign > s_maxcp) {
            $.alert({
                title: 'Error!!',
                type: 'red',
                icon: 'fa fa-exclamation-triangle',
                content: 'Total no [' + new_assign + '] must not be greater than Max CP [' + s_maxcp + ']'
            });
            $("#s_pa" + m).val("");
            var new_assign = Math.round((+s_pr) + (+s_p1) + (+s_p2) + (+s_p3) + (+s_pb));
            $("#s_assign" + m).val(new_assign);
            var new_left = Math.round((+s_maxcp) - (+new_assign));
            $("#s_left" + m).val(new_left);

        } else {
            $("#s_assign" + m).val(new_assign);
            var new_left = Math.round((+s_maxcp) - (+new_assign));
            $("#s_left" + m).val(new_left);
        }
        var TotalrwCnt = $("#row_count").val();
        var cntPR = 0;
        for (var l = 0; l < TotalrwCnt; l++) {
            var n = parseInt(l) + 1;
            var s_pr_n = $("#s_pa" + n).val();
            cntPR = Math.round((+cntPR) + (+s_pr_n));
        }
        $("#pa").val(cntPR);
        totalAssignLeft();
    }
    function SubPB(m) {
        var s_maxcp = $("#s_maxcp" + m).val();
        var s_pr = $("#s_pr" + m).val();
        var s_p1 = $("#s_p1" + m).val();
        var s_p2 = $("#s_p2" + m).val();
        var s_p3 = $("#s_p3" + m).val();
        var s_pa = $("#s_pa" + m).val();
        var s_pb = $("#s_pb" + m).val();
        var new_assign = Math.round((+s_p1) + (+s_p2) + (+s_p3) + (+s_pa) + (+s_pb) + (+s_pr));
        if (new_assign > s_maxcp) {
            $.alert({
                title: 'Error!!',
                type: 'red',
                icon: 'fa fa-exclamation-triangle',
                content: 'Total no [' + new_assign + '] must not be greater than Max CP [' + s_maxcp + ']'
            });
            $("#s_pb" + m).val("");
            var new_assign = Math.round((+s_pr) + (+s_p1) + (+s_p2) + (+s_p3) + (+s_pa));
            $("#s_assign" + m).val(new_assign);
            var new_left = Math.round((+s_maxcp) - (+new_assign));
            $("#s_left" + m).val(new_left);

        } else {
            $("#s_assign" + m).val(new_assign);
            var new_left = Math.round((+s_maxcp) - (+new_assign));
            $("#s_left" + m).val(new_left);
        }
        var TotalrwCnt = $("#row_count").val();
        var cntPR = 0;
        for (var l = 0; l < TotalrwCnt; l++) {
            var n = parseInt(l) + 1;
            var s_pr_n = $("#s_pb" + n).val();
            cntPR = Math.round((+cntPR) + (+s_pr_n));
        }
        $("#pb").val(cntPR);
        totalAssignLeft();
    }
    function totalAssignLeft() {
        var maxcp = $("#maxcp").val();
        var pr = $("#pr").val();
        var p1 = $("#p1").val();
        var p2 = $("#p2").val();
        var p3 = $("#p3").val();
        var pa = $("#pa").val();
        var pb = $("#pb").val();
        var totalpp1 = Math.round((+pr) + (+p1) + (+p2) + (+p3) + (+pa) + (+pb));
        $("#assign").val(totalpp1);
        var to_left = Math.round((+maxcp) - (+totalpp1));
        $("#left").val(to_left);
    }
    function PP_Recordmanage() {
        char_num("#pr");
        char_num("#p1");
        char_num("#p2");
        char_num("#p3");
        char_num("#pa");
        char_num("#pb");
        var TotalrwCnt = $("#row_count").val();
        var totalpp = 0;
        var totalp1 = 0;
        var totalp2 = 0;
        var totalpr = 0;
        var totalp3 = 0;
        var totalpa = 0;
        var totalpb = 0;
        for (var l = 0; l < TotalrwCnt; l++) {
            var m = parseInt(l) + 1;
            char_num("#s_pr" + m);
            char_num("#s_p1" + m);
            char_num("#s_p2" + m);
            char_num("#s_p3" + m);
            char_num("#s_pa" + m);
            char_num("#s_pb" + m);

            var s_maxcp = $("#s_maxcp" + m).val();
            var s_pr = $("#s_pr" + m).val();
            var s_p1 = $("#s_p1" + m).val();
            var s_p2 = $("#s_p2" + m).val();
            var s_p3 = $("#s_p3" + m).val();
            var s_pa = $("#s_pa" + m).val();
            var s_pb = $("#s_pb" + m).val();
            var new_assign = Math.round((+s_p1) + (+s_p2) + (+s_p3) + (+s_pa) + (+s_pb) + (+s_pr));
            $("#s_assign" + m).val(new_assign);
            var new_left = Math.round((+s_maxcp) - (+new_assign));
            $("#s_left" + m).val(new_left);
            totalpp = totalpp + new_assign;

            totalpr = Math.round((+totalpr) + (+s_pr));
            totalp1 = Math.round((+totalp1) + (+s_p1));
            totalp2 = Math.round((+totalp2) + (+s_p2));
            totalp3 = Math.round((+totalp3) + (+s_p3));
            totalpa = Math.round((+totalpa) + (+s_pa));
            totalpb = Math.round((+totalpb) + (+s_pb));

        }
      
        if (totalpp > 0) {
            $("#pr").val(totalpr);
            $("#p1").val(totalp1);
            $("#p2").val(totalp2);
            $("#p3").val(totalp3);
            $("#pa").val(totalpa);
            $("#pb").val(totalpb);
            var maxcp = $("#maxcp").val();
            $("#assign").val(totalpp);
            var to_left = Math.round((+maxcp) - (+totalpp));
            $("#left").val(to_left);
        }else{
            var maxcp = $("#maxcp").val();
            $("#assign").val(totalpp);
            var to_left = Math.round((+maxcp) - (+totalpp));
            $("#left").val(to_left);
        }
    }
    function getZonePhasedata(){
  var zone = $("#zone").val();

   var token = $("input[name='_token']").val();
  // $(".se-pre-con").fadeIn("slow");
   $.ajax({
       url: "getZonePhasedata",
       method: 'POST',
       data: {zone: zone, _token: token},
       success: function (data) {//alert(data.options);
       //   $(".se-pre-con").fadeOut("slow");
          $("select[name='phase'").html('');
          
          if(data.status==1)
          {
            $("select[name='phase'").append('<option value>[Select]</option>');
            $.each(data.options,function (k, v)
            {
                $("select[name='phase'").append('<option value='+k+'>'+v+'</option>');
            });
            //$("select[name='subdivision'").html(data.options);
             // change_officeName();
             
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
       //  $(".se-pre-con").fadeOut("slow");
           var msg = "";
           if (jqXHR.status !== 422 && jqXHR.status !== 400) {
               msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
           } else {
               if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                   msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
               } else {
                   msg += "Error(s):<strong><ul>";
                   $.each(jqXHR.responseJSON, function (key, value) {
                       msg += "<li>" + value + "</li>";
                   });
                   msg += "</ul></strong>";
               }
           }
           $.alert({
               title: 'Error!!',
               type: 'red',
               icon: 'fa fa-warning',
               content: msg
           });
         }
   });
}
</script>
@stop