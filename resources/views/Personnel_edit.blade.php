
@extends('layouts.master')
@section('content')

 <div class="wrapper">
<!-- Sidebar Holder -->
@include('layouts.sidebar')

<!-- Page Content Holder -->
<div id="content">           
<section class="tables-section">
    <nav aria-label="breadcrumb" style="width:100%;">
        <div class="breadcrumb pagehead1">
           
                {!! Form::button('<i class="fa fa-desktop"></i> Personnel Edit',['class' => 'btn btn-primary-header add-new-button']) !!}
               &nbsp; {!! Form::button(' Back To Personnel List &nbsp;<i class="fa fa-arrow-circle-left"></i>',['id'=>'back_personnel_list','class' => 'btn btn-primary-header add-new-button']) !!}   
            <div class="col-md-offset-8 pull-right">
<!--                {!! Form::button('Academic Year:&nbsp;',['id'=>'show_academic','class' => 'btn btn-primary-year']) !!}-->
                
            </div>
            <div class="clearfix"></div>
        </div>
    </nav>

    <!-- form -->
<div class="outer-w3-agile">
{!! Form::open(['url' => 'save_personnel', 'name' => 'personnel_save', 'class' =>'request-info clearfix form-horizontal', 'id' => 'personnel_save', 'method' => 'post','role'=>'','files' => true]) !!}
  {!! Form::hidden('districtcd', session()->get('districtcd_ppds'),['id'=>'districtcd']) !!}
  
  
  <div class="panel-group" id="accordion5" style="padding-top: 0px;">                                                              
    <div class="panel panel-default">
     <div class="panel-heading1">
     <h6 class="panel-title">
         <a data-toggle="#" data-parent="#accordion5" href="#collapseUV"> 
             <span class="fa fa-minus"></span> <span class="highlight">Personnel Details</span> </a>
            
     </h6>
     </div>
     <div id="collapseUV" class="panel-collapse collapse5">
      <div class="panel-body">                                   
        
      
         <div class="row"> 
            <div class='col-sm-12'>
            @if ($errors->any())
    <div class="alert alert-danger" style="color:red;">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
                <div >
                <table class="table table-striped table-bordered table-hover " width="100%">
                <thead>
                <tr>
                <td ><b>Field</b></td>
                <td ><b>Details</b></td>

                </tr></thead>
                
                <tbody>
                
                <?php foreach($person as $pp){ 
                    // dd($pp);

                    $dist = \Session::get('districtcd_ppds');
                    
                    if ($dist < 10) {  
                        $value = str_pad($pp->personcd, 11, "0", STR_PAD_LEFT);
                    }
                    else{
                        $value =$pp->personcd;
                    }
                    $personCd= $value;

                    ?>
                    {!! Form::text('personcd',$personCd,['class'=>'form-control','name'=>'personnel_id','id'=>'personnel_id']) !!}
                <tr><td>Name</td><td><div class="form-group"><input type="text" class="form-control" name="officer_name" value="<?php echo $pp->officer_name; ?>"></div></td></tr>
                <tr><td>Designation</td><td><div class="form-group"><input type="text" class="form-control" name="off_desg" value="<?php echo $pp->off_desg; ?>"></div></td></tr>
                <tr><td>Present Address1</td><td><div class="form-group"><textarea class="form-control" name="present_addr1"><?php echo $pp->present_addr1; ?></textarea></div></td></tr>
                <tr><td>Present Address2</td><td><div class="form-group"><textarea class="form-control" name="present_addr2"><?php echo $pp->present_addr2; ?></textarea></div></td></tr>
                <tr><td>Permanent Address1</td><td><div class="form-group"><textarea class="form-control" name="perm_addr1"><?php echo $pp->perm_addr1; ?></textarea></div></td></tr>
                <tr><td>Permanent Address2</td><td><div class="form-group"><textarea class="form-control" name="perm_addr2"><?php echo $pp->perm_addr2; ?></textarea></div></td></tr>
                <tr><td>Birth Date</td><td><div class="form-group"><input type="text" class="form-control" name="dateofbirth" id="dob" value="<?php echo $pp->dateofbirth; ?>" placeholder="YYYY-MM-DD 00:00:00"></div></td></tr>
                <tr><td>Gender</td><td><div class="form-group"><select name="gender" class="form-control"><option <?php if($pp->gender=='M') echo 'selected';?> value="M">Male</option><option <?php if($pp->gender=='F') echo 'selected';?> value="F">Female</option></select></div></td></tr>
                <tr><td>Pay Scale</td><td><div class="form-group"><input type="text" class="form-control" name="scale" value="<?php echo $pp->scale; ?>"></div></td></tr>
                <tr><td>Basic Pay</td><td><div class="form-group"><input type="text" class="form-control" name="basic_pay" value="<?php echo $pp->basic_pay; ?>"></div></td></tr>
                <tr><td>Group</td><td><div class="form-group"><select name="pgroup" class="form-control"><option <?php if($pp->pgroup=='A') echo 'selected';?> value="A">A</option><option <?php if($pp->pgroup=='B') echo 'selected';?> value="B">B</option><option <?php if($pp->pgroup=='C') echo 'selected';?> value="C">C</option><option <?php if($pp->pgroup=='D') echo 'selected';?> value="D">D</option></select></div></td></tr>
                <tr><td>Garde Pay</td><td><div class="form-group"><input type="text" class="form-control" name="grade_pay" value="<?php echo $pp->grade_pay; ?>"></div></td></tr>
                <tr><td>Working Status</td><td><div class="form-group"><select name="workingstatus" class="form-control"><option <?php if($pp->workingstatus=='Y') echo 'selected';?> value="Y">Yes</option><option <?php if($pp->workingstatus=='N') echo 'selected';?> value="N">No</option></select></div></td></tr>
                <tr><td>Email</td><td><div class="form-group"><input type="text" class="form-control" name="email" value="<?php echo $pp->email; ?>"></div></td></tr>
                <tr><td>Phone (Residencial)</td><td><div class="form-group"><input type="text" class="form-control" name="resi_no" value="<?php echo $pp->resi_no; ?>"></div></td></tr>
                <tr><td>Mobile</td><td><div class="form-group"><input type="text" name="mob_no" class="form-control" value="<?php echo $pp->mob_no; ?>"></div></td></tr>
                <tr><td>Qualification</td><td><div class="form-group"><select class="form-control" name="qualificationcd"><?php foreach($qualification as $q){if($pp->qualificationcd==$q->qualificationcd){echo '<option selected  value="'.$q->qualificationcd.'">'.$q->qualification.'</option>';}else{echo '<option  value="'.$q->qualificationcd.'">'.$q->qualification.'</option>';}} ?></select></div></td></tr>
                <tr><td>Language</td><td><div class="form-group"><select name="languagecd" class="form-control"><?php foreach($language as $l){if($pp->languagecd==$l->languagecd){echo '<option selected  value="'.$l->languagecd.'">'.$l->language.'</option>';}else{echo '<option  value="'.$l->languagecd.'">'.$l->language.'</option>';}}?></select></div></td></tr>
                <tr><td>EPIC</td><td><div class="form-group"><input type="text" class="form-control" name="epic" value="<?php echo $pp->epic; ?>"></div></td></tr>
                
                
                
                <tr><td>Assembly (Permanent)</td><td><div class="form-group"> <select class="assembly_perm form-control"  name="assembly_perm" id='assembly_perm' ><?php foreach($ac as $a){if($pp->assembly_perm==$a->assemblycd){echo '<option selected value="'.$a->assemblycd.'">'.$a->assemblycd.'-'.$a->assemblyname.'</option>';}else{echo '<option value="'.$a->assemblycd.'">'.$a->assemblycd.'-'.$a->assemblyname.'</option>';} }?></select></div></td></tr>
                <tr><td>Assembly (Temporary)</td><td><div class="form-group">  <select class="assembly_temp form-control"  name="assembly_temp" id='assembly_temp' ><?php foreach($ac as $a){if($pp->assembly_temp==$a->assemblycd){echo '<option selected value="'.$a->assemblycd.'">'.$a->assemblycd.'-'.$a->assemblyname.'</option>';}else{echo '<option value="'.$a->assemblycd.'">'.$a->assemblycd.'-'.$a->assemblyname.'</option>';} }?></select></td></div></tr>
                <tr><td>Assembly (Office)</td><td> <div class="form-group"> <select class="assembly_off form-control"  name="assembly_off" id='assembly_off' ><?php foreach($ac as $a){if($pp->assembly_off==$a->assemblycd){echo '<option selected value="'.$a->assemblycd.'">'.$a->assemblycd.'-'.$a->assemblyname.'</option>';}else{echo '<option value="'.$a->assemblycd.'">'.$a->assemblycd.'-'.$a->assemblyname.'</option>';} }?></select></td></div></tr>
        
                
                
                
                <tr><td>Serial No</td><td><div class="form-group"><input type="text" class="form-control" name="slno" value="<?php echo $pp->slno; ?>"></div></td></tr>
                <tr><td>Part No</td><td><div class="form-group"><input type="text" class="form-control" name="partno" value="<?php echo $pp->partno; ?>"></div></td></tr>
                <tr><td>Post Status</td><td><div class="form-group"><select class="form-control" name="poststat" id="poststat"><?php foreach($poststatus as $ps){if($pp->poststat==$ps->post_stat){echo '<option selected value="'.$ps->post_stat.'">'.$ps->poststatus.'</option>';}else{echo '<option  value="'.$ps->post_stat.'">'.$ps->poststatus.'</option>';}} ?></select></div></td></tr>
                <tr><td>Branch IFSC</td><td><div class="form-group"><input class="form-control" type="text" name="branch_ifsc" value="<?php echo $pp->branch_ifsc; ?>"></div></td></tr>
                <tr><td>Bank Account Number</td><td><div class="form-group"><input class="form-control" type="text" name="bank_acc_no" value="<?php echo $pp->bank_acc_no; ?>"></div></td></tr>
                <tr><td>Remark</td><td><div class="form-group"><select class="form-control" name="remarks"><?php foreach($remark as $r){if($pp->remarks==$r->remarks_cd){echo '<option selected value="'.$r->remarks_cd.'">'.$r->remarks.'</option>';}else{echo '<option  value="'.$r->remarks_cd.'">'.$r->remarks.'</option>';}} ?></select></div></td></tr>
                
                <?php } ?>
                
                <tr>
                    <td><?php if(!empty($_GET['save']) && $_GET['save']==1) echo '<p style="color:green;">Personnel updated</p>';?></td>
                    <td>
                        <div class="form-group">
                            <input type="submit" name="save" id="submit" value="Update" class="btn btn-info">
                        
                            {!! Form::close() !!}
                       


                            {{-- {!! Form::open(['name' => 'revoke_exemption_form', 'class' =>'request-info clearfix form-horizontal', 'id' => 'revoke_exemption_form']) !!} --}}

                            <?php
                              //  foreach($person as $pp1){ 
                             // dd($pp);

                            // $dist1 = \Session::get('districtcd_ppds');
                            
                            // if ($dist1 < 10) {  
                            //     $value = str_pad($pp1->personcd, 11, "0", STR_PAD_LEFT);
                            // }
                            // else{
                            //     $value1 =$pp1->personcd;
                            // }
                            // $personCd1= $value1;

                                if ($pp->booked=='C') {
                                    
                              
                            ?>

                            <button type="button" name="revoke_exemption" id="revoke_exemption" class="btn btn-primary">Revoke Exemption</button>
                            <?php } ?>

                            {{-- {!! Form::close() !!} --}}
                        
                   
                        </div>
                    </td>
                </tr>



                </tbody>
                </table>
                </div>
            </div>
        </div>  
        
         
      </div>
    </div>                              
  </div> 
 
  
  </div>

 
 {{-- {!! Form::close() !!}              --}}
</div>
<!--// form -->

</section>
</div>
</div>
<!-- Copyright -->
<script type="text/javascript">
$(document).ready(function () {
    $("#back_personnel_list").click(function () {
            window.location.href = "getofficebydistrict";
        });
//  getoffice();
// var table = "";
//         table = $('#datatable-table').DataTable();
$("#reset").click(function () {
  location.reload(true);  
});


var token = $("input[name='_token']").val();
$('.assembly_perm,.assembly_temp,.assembly_off').select2({
            placeholder: "Enter Assembly Name Or Code",
            minimumInputLength: 3,
            maximumInputLength: 11,
             
            ajax: {
                url: 'assemblysearch',
                dataType: 'json',
                method: 'POST',
                data: function (params) {
                    return {
                        q: $.trim(params.term),
                         _token: token
                    };
                },
                processResults: function (data) {
                    
                    return {
                        results:  $.map(data.options, function (item) {
                    return {
                        text: item.assemblycd + '-' + item.assemblyname,
                        id: item.assemblycd
                    }
                })
                    };

                },
                cache: true
            }
        }).on("select2:select", function(event) {

}); 




  $('#personnel_save')
            .bootstrapValidator({
            message: 'This value is not valid',
            feedbackIcons: {
            valid: 'fa fa-check',
                    invalid: 'fa fa-times',
                    validating: 'fa fa-refresh'
            },
            fields: {
                officer_name: {
                    validators: {
                        notEmpty: {
                            message: 'Officer name is required'
                        }
                    }
                }
              }
            }).on('success.form.bv', function (e) {
           // Prevent form submission
            e.preventDefault();
            var action = $(this).attr('action');
            var type = $(this).attr('method');
            var token = $("input[name='_token']").val();
            var formData = new FormData($(this)[0]);
         
            var districtcd = $("#districtcd").val();
           
          
          $(".se-pre-con").fadeIn("slow");
            $.ajax({
            type: type,
                    url: action,
                    data: formData,
                    processData: false,
                    contentType: false,
                    dataType: "json",
                    success: function (data) {
                        // console.log(data.person_first_rand_tbl);
                    if(data.status==1)
                    {
                      $(".se-pre-con").fadeOut("slow");            
                      var msg="Record(s) saved successfully";
                       $.confirm({
                            title: 'Success!',
                            type: 'green',
                            icon: 'fa fa-check',
                            content: msg,
                            buttons: {
                                ok: function () {
                                   // location.reload(true);
                                 //  getZoneWiseAssembly();
                                //  alert(districtcd);
                                if(data.person_first_rand_tbl==''){
                                    window.location.assign("getofficebydistrict")
                                   
                                  
                                }else{
                                      print_appt();
                                }

                                }

                            }
                        });
                      }
                     

                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        $(".se-pre-con").fadeOut("slow");
                           $("#submit").removeAttr("disabled");
                        var msg = "";
                        if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                            msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                        } else {
                            if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                                msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                            } else {
                                msg += "Error(s):<strong><ul>";
                                $.each(jqXHR.responseJSON, function (key, value) {
                                    msg += "<li>" + value + "</li>";
                                });
                                msg += "</ul></strong>";
                            }
                        }
                        $.alert({
                            title: 'Error!!',
                            type: 'red',
                            icon: 'fa fa-warning',
                            content: msg
                        });
                    }
            });
 
     });




$("#revoke_exemption").click(function() {
    exemption();
});
  

});

function exemption() {
    // alert("hi");
    var token = $('input[name="_token"]').val();
    var personeel_id = $("#personnel_id").val();
    $(".se-pre-con").fadeIn("slow");
    $.ajax({
            type: "POST",
                    url: "update_revoke_exemption",
                    data: {'_token':'{{csrf_token()}}','personeel_id':personeel_id},
                    
                    dataType: "json",
                    success: function (data) {
                        if(data.status==2)
                            {
                            $(".se-pre-con").fadeOut("slow");            
                            var msg="Booked Updated Successfully";
                            $.confirm({
                                    title: 'Success!',
                                    type: 'green',
                                    icon: 'fa fa-check',
                                    content: msg,
                                    buttons: {
                                        ok: function () {
                                        // location.reload(true);
                                        //  getZoneWiseAssembly();
                                        //  alert(districtcd);
                                        window.location.assign("getofficebydistrict");

                                        }

                                    }
                                });
                            }
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        $(".se-pre-con").fadeOut("slow");
                           $("#submit").removeAttr("disabled");
                        var msg = "";
                        if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                            msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                        } else {
                            if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                                msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                            } else {
                                msg += "Error(s):<strong><ul>";
                                $.each(jqXHR.responseJSON, function (key, value) {
                                    msg += "<li>" + value + "</li>";
                                });
                                msg += "</ul></strong>";
                            }
                        }
                        $.alert({
                            title: 'Error!!',
                            type: 'red',
                            icon: 'fa fa-warning',
                            content: msg
                        });
                    }
    });
}


function print_appt() {
    var districtcd = $("#districtcd").val();
    //  var subdivision = "";
    //  var blockMunicipality = "";
    //  var officeName = "";
     var PersonnelId = $("#personnel_id").val();
    //  var trainingtype = "";
     var from = "1";
     var to = "1";
    //  var datas = {'districtcd': districtcd,'subdivision': subdivision,'blockMunicipality': blockMunicipality,'officeName': officeName,'PersonnelId': PersonnelId,'trainingtype': trainingtype,from: from,to: to, '_token': $('input[name="_token"]').val()};
    var datas = {'districtcd': districtcd,'PersonnelId': PersonnelId,from: from,to: to, '_token': $('input[name="_token"]').val()};
     
     redirectPost_newTab('{{url("getFirstAppointmentLetterPDF2019")}}', datas);
}




function delete_zone(code) {

//    var reply=confirm('Are you sure to delete this record?');
//                 if(!reply){
//                     return false;
//                 }
    var msg="Are you sure to delete this record?";
    $.confirm({
         title: 'Confirm!',
         type: 'red',
         icon: 'fa fa-exclamation-triangle',
         content: msg,
         buttons: {
             ok: function () {
                 $.ajax({
                  type: 'post',
                  url: 'zone_delete',
                  data: {'zone': code, '_token': $('input[name="_token"]').val()},
                  dataType: 'json',
                  success: function (data) {
                    if(data.status==1)
                    {
                     location.reload(true);
                    }
                    else if (data.status==2){
                       $.alert({
                            title: 'Error!!',
                            type: 'red',
                            icon: 'fa fa-warning',
                            content: data.options
                        }); 
                    }
                  }
                });
              

             },
             cancel:function () {
                 
             }

         }
     }); 
    

}

function edit_zone(code) {
    $("#reset").show();
    $.ajax({
        type: 'post',
        url: 'zone_edit',
        data: {'zone': code, '_token': $('input[name="_token"]').val()},
        dataType: 'json',
        success: function (data) {
            $('#zone').val(data.options[0].zonename);
            $('#zone_code').val(data.options[0].zone);
        }
    });
 }
 

function getpersonnel(){
    var token = $('input[name="_token"]').val();
        var districtcd = $("#districtcd").val();   
        var office_id=$('#ofice_list').children("option:selected"). val();
   
   $(".se-pre-con").fadeIn("slow");
   $.ajax({
       url: "su_personnel_list/"+office_id,
       method: 'GET',
       data: {district_id:districtcd, _token: token},
       success: function (json) {//alert(json);
          $(".se-pre-con").fadeOut("slow");
          $("#memberDetails").html('');
          $('#memberDetails').html(json);
          
       },
        error: function (jqXHR, textStatus, errorThrown) {
         $(".se-pre-con").fadeOut("slow");
           var msg = "";
           if (jqXHR.status !== 422 && jqXHR.status !== 400) {
               msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
           } else {
               if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                   msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
               } else {
                   msg += "Error(s):<strong><ul>";
                   $.each(jqXHR.responseJSON, function (key, value) {
                       msg += "<li>" + value + "</li>";
                   });
                   msg += "</ul></strong>";
               }
           }
           $.alert({
               title: 'Error!!',
               type: 'red',
               icon: 'fa fa-warning',
               content: msg
           });
        }
   });
    
 }
        

        
</script>
@stop

