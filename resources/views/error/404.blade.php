<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/jquery-confirm.min.css" rel="stylesheet" type="text/css" media="all" />
<script src="{{ asset('js/jquery-2.2.3.min.js') }}"></script>
<script src="js/jquery-confirm.min.js"></script>

       <script>
           $(document).ready(function(){
               $.confirm({
    title: '<span style="color:red;">ERROR:404</span>',
    content: '<span style="color:green;">Page Not Found. Please Go back To Previsous Page!</span>',
    buttons: {
        Back: {
            btnClass: 'btn-red',
            action: function(){
                window.history.back();
            }
        }
    }
});
           });
           </script>
           