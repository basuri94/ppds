@extends('layouts.master')

@section('content')
<?php
//print_r($pc['pc']);
// print_r(config('constants.USER_CATEGORY'));
//die; 
?>
<style>
    .sub_permission{
        margin-left: 10px;
        background-color: #2a3f54;
        padding: 4px 4px 4px 4px;
        border-radius: 30%;
        color: #ade0d6;
    }
</style>
<!--// top-bar -->
<div class="wrapper">
    <!-- Sidebar Holder -->
    @include('layouts.sidebar')
    <!-- Page Content Holder -->
    <div id="content">           
        <section class="tables-section">
            <nav aria-label="breadcrumb" style="width:100%;">
                <ol class="breadcrumb pagehead1">
                    <span class="headbrand"><i class="fa fa-desktop"></i> User Permission
                        <span class="scoop-mcaret1"></span>
                    </span>&nbsp;&nbsp;
                    &nbsp;{!! Form::button(' List of User&nbsp;<i class="fa fa-arrow-circle-right"></i>',['id'=>'add-new-button','class' => 'btn btn-primary-year add-new-button']) !!}    
                    &nbsp;{!! Form::button(' Create User&nbsp;<i class="fa fa-plus-circle"></i>',['id'=>'add-new-button1','class' => 'btn btn-primary-year add-new-button']) !!} 
                </ol>
            </nav>
            <div class="outer-w3-agile">

                <div style="padding-left:50px;padding-top:5px;">
                    {!! Form::open(['url' => 'create_user_permission', 'name' => 'create_user_permission', 'id' => 'create_user_permission', 'method' => 'post' ,'class'=>'animate-form form-horizontal','role'=>'form']) !!}
                    {!! Form::hidden('user_code', $user_data[0]->code,['id'=>'user_code']) !!}
                    <div class="form-group row" style="text-align:left;">
                        {!! Form::label('username', 'Username:',['class'=>'col-lg-1 col-form-label required']) !!}
                        <div class="col-lg-4">
                            {!! Form::text('username', $user_data[0]->user_id, ['id'=>'username','class'=>'form-control','readonly'=>'readonly','autocomplete'=>'off']) !!}
                        </div>

                        {!! Form::label('menucd', 'Menu Name:',['class'=>'col-lg-2 col-form-label required']) !!}
                        <div class="col-lg-4">
                            <?php //print_r(json_encode($data));  ?>
                            <select id="menucd" name="menucd" class="form-control">
                                <option value=''>[Select]</option>
                                <?php foreach ($data[0]['user_permission_code'] as $menu) { ?>
                                    <option value="<?php echo $menu->menu_cd ?>"><?php echo $menu->menu->menu ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <table class="table table-bordered table-striped" style='border-top: 2px solid #4cae4c;'>

                       
                            <tr>

                                <th width="50%" class="column-title">Permission Assign</th>
                                <th width="50%" class="column-title">Permission Alloted</th>

                            </tr>
                        

                        <tbody>
                            <tr style="background-color: #fff">
                                <td  width="50%" style="padding: 0px;">
                                    <div id='give_permission' style="height:350px;overflow:scroll;"></div> 
                                </td>
                                <td  width="50%" style="padding: 0px;">

<!--                                    <table width='100%'  class="table table-striped jambo_table bulk_action">
                                        <tr style=" background-color: #2A3F54; color: #FFFFFF; font-weight: bold;   "><td align="left">Permission Assigned</td></tr>
                                        <tr style=" background-color: #1ABB9C; color: #FFFFFF; font-weight: bold;   "><td align="left">Menu & Submenu List</td></tr>
                                        <tr>-->
<!--                                            <td style="padding: 0px;">-->
                                                <div id='alloted_permission' style="height:350px;overflow:scroll;"></div>
<!--                                            </td>-->
<!--                                        </tr>
                                    </table>-->



                                </td>
                            </tr>
                        </tbody>

                    </table>


                    <div class="form-group row">
                        <div class='col-sm-12'> 
                            <div class="form-group text-right permit" >                            	
                                {{ Form::button('Submit', ['class' => 'btn btn-info', 'type' => 'submit','id'=>'save_id']) }}
                                {{ Form::button('Reset', ['class' => 'btn btn-success' ,'type' => 'reset','id'=>'reset']) }}
                            </div>
<!--                        <div class="col-lg-4">
                            <button type="submit" class="btn btn-primary btn-sm" id="save_id">Save</button>
                        </div>-->
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
            <!--// form -->

        </section>

    </div>
</div>
</div>
<script>
    $(document).ready(function () {
         $("#reset").click(function () {
            location.reload(true);  
          });
        $('#add-new-button').click(function () {
            //$(".se-pre-con").fadeIn("slow"); 
            window.location.href = "user_list";
        });
         $('#add-new-button1').click(function () {
            //$(".se-pre-con").fadeIn("slow"); 
            var datas = {'user_code': 0,'_token': $('input[name="_token"]').val()};
            redirectPost('{{url("user_create")}}',datas); 
        });
//        $('#add-new-button1').click(function () {
//            //$(".se-pre-con").fadeIn("slow"); 
//            window.location.href = "user_create";
//        });
//        $("#menucd").change(function () {
//            var user_code = $('#user_code').val();
//            var menu_code = $('#menucd').val();
//            var token = $("input[name='_token']").val();
//            $(".se-pre-con").fadeIn("slow");
//            //alert(menu_code);
//            $.ajax({
//                type: 'POST',
//                url: 'user_permission_sub_menu_list',
//                data: {user_code: user_code, menu_code: menu_code, _token: token},
//                dataType: 'json',
//                success: function (response) {
//                    $(".se-pre-con").fadeOut("slow");
//                    var str = '<input type="hidden" id="row_count" name="row_count" value="' + response[0].menu_wise_submenu.length + '"> '
//                    str += "<table class='table table-borderd'>";
//                    str += ' <tr style=" background-color: #2A3F54; color: #FFFFFF; font-weight: bold;   "><td align="left" width="70%">&nbsp; Sub Menu</td><td align="center" width="30%">View </td></tr>';
//                    str += '<tr style=" background-color: #1ABB9C; color: #FFFFFF; font-weight: bold;"><td align="left" width="70%"><input type="checkbox" id="myCheckall" name="myCheckall"  value="1" >&nbsp; Check All</td><td align="center" width="30%"><input type="checkbox" id="view_all" class="flat"></td></tr>';
//                    var sub_length = response[0].menu_wise_submenu.length;
//                    var submenu_len = 0;
//                    var view_len = 0
//                    //alert(sub_length);
//                    for (var i = 0; i < sub_length; i++)
//                    {
//                        var c = parseInt(i) + 1;
//                        str += '<tr><td align="left" width="70%">';
//                        if (response[0].menu_wise_submenu[i].user_permission_submenu_cd.length > 0) {
//                            str += '<input type="checkbox" id="myCheck_' + c + '" name="myCheck_' + c + '"  value="' + response[0].menu_wise_submenu[i].submenu_cd + '"  class="submenu" checked="checked">';
//                            submenu_len++
//                        } else {
//                            str += '<input type="checkbox" id="myCheck_' + c + '" name="myCheck_' + c + '"  value="' + response[0].menu_wise_submenu[i].submenu_cd + '"  class="submenu" >';
//                        }
//                        str += '&nbsp;&nbsp;' + response[0].menu_wise_submenu[i].submenu + '</td><td align="center" width="30%">';
//                        if (response[0].menu_wise_submenu[i].user_permission_submenu_cd.length > 0 && response[0].menu_wise_submenu[i].user_permission_submenu_cd[0].show_status == 1) {
//                            str += '<input type="checkbox" id="view_' + c + '" name="view_' + c + '"  value="1"  class="view" checked="checked">';
//                            view_len++
//                        } else {
//                            str += '<input type="checkbox" id="view_' + c + '" name="view_' + c + '"  value="1"  class="view">';
//                        }
//                        str += '</td></tr>'; // return empty
//                    }
//                    str += "</table>";
//
//                    //alert(str);
//                    $("#give_permission").html(str);
//                    // alert($('.submenu').length);
//                    if (submenu_len === $('.submenu').length) {
//
//                        $("#myCheckall").attr('checked', true);
//                    }
//                    if (view_len == $('.view').length) {
//                        $("#view_all").attr('checked', true);
//                    }
//                    $("#myCheckall").change(function () {
//                        var status = this.checked;
//                        $('.submenu').each(function () {
//                            this.checked = status;
//                        });
//                        if (status == false) {
//                            $("#view_all")[0].checked = status;
//                            $('.view').each(function () {
//                                this.checked = status;
//                            });
//                        }
//                    });
//                    $('.submenu').change(function () {
//                        var viewid = this.id.split('_');
//
//                        if (this.checked == false) {
//                            $("#myCheckall")[0].checked = false;
//                            //alert(viewid[1]);
//                            $("#view_" + viewid[1]).attr('checked', false);
//                        }
//                        if ($('.view:checked').length == $('.view').length) {
//                            $("#view_all")[0].checked = true;
//                        }else{
//                           $("#view_all")[0].checked = false;  
//                        }
//                        if ($('.submenu:checked').length == $('.submenu').length) {
//                            $("#myCheckall")[0].checked = true;
//                        }
//                        else{
//                           $("#myCheckall")[0].checked = false;  
//                        }
//                    });
//                    $("#view_all").change(function () {
//                        var status = this.checked;
//                        $('.view').each(function () {
//                            this.checked = status;
//                        });
//                        if (status == true) {
//                            $("#myCheckall")[0].checked = status;
//                            $('.submenu').each(function () {
//                                this.checked = status;
//                            });
//                        }
//                    });
//                    $('.view').change(function () {
//                        var myCheckallid = this.id.split('_');
//                        if (this.checked == false) {
//                            $("#view_all")[0].checked = false;
//                        }
//                        if (this.checked == true) {
//                            $("#myCheck_" + myCheckallid[1])[0].checked = true;
//                        }
//                        if ($('.view:checked').length == $('.view').length) {
//                            $("#view_all")[0].checked = true;
//                        }else{
//                           $("#view_all")[0].checked = false;  
//                        }
//                        if ($('.submenu:checked').length == $('.submenu').length) {
//                            $("#myCheckall")[0].checked = true;
//                        }
//                        else{
//                           $("#myCheckall")[0].checked = false;  
//                        }
//                    });
//
//                },
//                error: function (jqXHR, textStatus, errorThrown) {
//                    $(".se-pre-con").fadeOut("slow");
//                    $("#loadingDiv").hide();
//                    messages_hide();
//                    var msg = "<strong>Failed to Fetch data.</strong><br/>";
//                    if (jqXHR.status !== 422 && jqXHR.status !== 400) {
//                        msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
//                    } else {
//                        if (jqXHR.responseJSON.hasOwnProperty('exception')) {
//                            msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
//                        } else {
//                            msg += "Error(s):<strong><ul>";
//                            $.each(jqXHR.responseJSON, function (key, value) {
//                                msg += "<li>" + value + "</li>";
//                            });
//                            msg += "</ul></strong>";
//                        }
//                    }
//                    $.alert({
//                        title: 'Error!!',
//                        icon: 'fa fa-times',
//                        type: 'red',
//                        content: msg,
//                    });
//                }
//            });
//        });
        $("#menucd").change(function () {
            var user_code = $('#user_code').val();
            var menu_code = $('#menucd').val();
            var token = $("input[name='_token']").val();
            $(".se-pre-con").fadeIn("slow");
            //alert(menu_code);
            $.ajax({
                type: 'POST',
                url: 'user_permission_sub_menu_list_create',
                data: {user_code: user_code, menu_code: menu_code, _token: token},
                dataType: 'json',
                success: function (response) {
                    $(".se-pre-con").fadeOut("slow");

                    // alert(sub_length);
                    var str = '<input type="hidden" id="row_count" name="row_count" value="' + response[0].user_permission_code[0]['menu'].menu_wise_submenu.length + '"> '
                    str += "<table class='table table-borderd'>";
                    str += ' <tr style=" background-color: #0086b3; color: #FFFFFF; font-weight: bold;   "><td align="left" width="70%">&nbsp; Sub Menu</td><td align="center" width="30%">Show in Menubar</td></tr>';
                    str += '<tr style=" background-color: #1ABB9C; color: #FFFFFF; font-weight: bold;"><td align="left" width="70%"><input type="checkbox" id="myCheckall" name="myCheckall"  value="1" >&nbsp; Check All</td><td align="center" width="30%"><input type="checkbox" id="view_all" class="flat"></td></tr>';
                    var sub_length = response[0].user_permission_code[0]['menu'].menu_wise_submenu.length;
                    var submenu_len = 0;
                    var view_len = 0
                    //alert(sub_length);
                    for (var i = 0; i < sub_length; i++)
                    {
                        var c = parseInt(i) + 1;
                        str += '<tr><td align="left" width="70%">';

                        var per_str = response[0].user_permission_code[0]['menu'].menu_wise_submenu[i].user_permission_submenu_cd.length;
                        for (var p = 0; p <= per_str; p++) {
                            var per_u_code = response[0].user_permission_code[0]['menu'].menu_wise_submenu[i].user_permission_submenu_cd[p].user_cd;
                            var per_show_status = response[0].user_permission_code[0]['menu'].menu_wise_submenu[i].user_permission_submenu_cd[p].show_status;
                            if (per_u_code ==<?php echo \Session::get('code_ppds'); ?>) {
                                var arr = response[0].user_permission_code[0]['menu'].menu_wise_submenu[i].user_permission_submenu_cd;
                                var check_arr = arr.filter(function (sub_per) {
                                    return sub_per.user_cd == user_code;
                                });
                                if (check_arr.length == 1) {
                                    str += '<input type="checkbox" id="myCheck_' + c + '" name="myCheck_' + c + '"  value="' + response[0].user_permission_code[0]['menu'].menu_wise_submenu[i].submenu_cd + '"  class="submenu" checked="checked">';
                                    submenu_len++
                                } else {
                                    str += '<input type="checkbox" id="myCheck_' + c + '" name="myCheck_' + c + '"  value="' + response[0].user_permission_code[0]['menu'].menu_wise_submenu[i].submenu_cd + '"  class="submenu" >';
                                }
                                str += '&nbsp;&nbsp;' + response[0].user_permission_code[0]['menu'].menu_wise_submenu[i].submenu + '</td><td align="center" width="30%">';
                                if (per_show_status == 1) {
                                    var check_arr1 = arr.filter(function (sub_per) {
                                        return sub_per.user_cd == user_code && sub_per.show_status == 1;
                                    });
                                    if (check_arr1.length == 1) {
                                        str += '<input type="checkbox" id="view_' + c + '" name="view_' + c + '"  value="1"  class="view" checked="checked">';
                                        view_len++
                                    } else {
                                        str += '<input type="checkbox" id="view_' + c + '" name="view_' + c + '"  value="1"  class="view">';
                                    }
                                } else {
                                    str += '';
                                }
                                break;
                            }
                        }

                        str += '</td></tr>'; // return empty
                    }
                    str += "</table>";

                    //alert(str);
                    $("#give_permission").html(str);
                    if (submenu_len === $('.submenu').length) {

                        $("#myCheckall").attr('checked', true);
                    }
                    if (view_len == $('.view').length) {
                        $("#view_all").attr('checked', true);
                    }
                    $("#myCheckall").change(function () {
                        var status = this.checked;
                        $('.submenu').each(function () {
                            this.checked = status;
                        });
                        if (status == false) {
                            $("#view_all")[0].checked = status;
                            $('.view').each(function () {
                                this.checked = status;
                            });
                        }
                    });
                    $('.submenu').change(function () {
                        var viewid = this.id.split('_');

                        if (this.checked == false) {
                            $("#myCheckall")[0].checked = false;
                            //alert(viewid[1]);
                            $("#view_" + viewid[1]).attr('checked', false);
                        }
                        if ($('.view:checked').length == $('.view').length) {
                            $("#view_all")[0].checked = true;
                        } else {
                            $("#view_all")[0].checked = false;
                        }
                        if ($('.submenu:checked').length == $('.submenu').length) {
                            $("#myCheckall")[0].checked = true;
                        } else {
                            $("#myCheckall")[0].checked = false;
                        }
                    });
                    $("#view_all").change(function () {
                        var status = this.checked;
                        $('.view').each(function () {
                            this.checked = status;
                        });
                        if (status == true) {
                            $("#myCheckall")[0].checked = status;
                            $('.submenu').each(function () {
                                this.checked = status;
                            });
                        }
                    });
                    $('.view').change(function () {
                        var myCheckallid = this.id.split('_');
                        if (this.checked == false) {
                            $("#view_all")[0].checked = false;
                        }
                        if (this.checked == true) {
                            $("#myCheck_" + myCheckallid[1])[0].checked = true;
                        }
                        if ($('.view:checked').length == $('.view').length) {
                            $("#view_all")[0].checked = true;
                        } else {
                            $("#view_all")[0].checked = false;
                        }
                        if ($('.submenu:checked').length == $('.submenu').length) {
                            $("#myCheckall")[0].checked = true;
                        } else {
                            $("#myCheckall")[0].checked = false;
                        }
                    });
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $(".se-pre-con").fadeOut("slow");
                    $("#loadingDiv").hide();
                    messages_hide();
                    var msg = "<strong>Failed to Fetch data.</strong><br/>";
                    if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                        msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                    } else {
                        if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                            msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                        } else {
                            msg += "Error(s):<strong><ul>";
                            $.each(jqXHR.responseJSON, function (key, value) {
                                msg += "<li>" + value + "</li>";
                            });
                            msg += "</ul></strong>";
                        }
                    }
                    $.alert({
                        title: 'Error!!',
                        icon: 'fa fa-times',
                        type: 'red',
                        content: msg,
                    });
                }
            });
        });
        $('#create_user_permission')
                .bootstrapValidator({
                    message: 'This value is not valid',
                    feedbackIcons: {
                        valid: 'fa fa-check',
                        invalid: 'fa fa-times',
                        validating: 'fa fa-refresh'
                    },
                    fields: {

                        menucd: {
                            validators: {
                                notEmpty: {
                                    message: 'Menu Name is required'
                                }
                            }
                        }

                    }
                }).on('success.form.bv', function (e) {
            // Prevent form submission
            e.preventDefault();
            var action = $(this).attr('action');
            var type = $(this).attr('method');
            var formData = new FormData($(this)[0]);
            var operation = "save";
            var operated = "saved";
            /*if (formData.get('smenu_edit_code') !== "") {
             action = 'submenu_data_update';
             operation = "update";
             operated = "updated";
             
             
             }*/


            $(".se-pre-con").fadeIn("slow");
            $.ajax({
                type: type,
                url: action,
                data: formData,
                processData: false,
                contentType: false,
                dataType: "json",
                success: function (data) {
                    $(".se-pre-con").fadeOut("slow");
                    $("#save_id").attr('disabled', false);                   
                    $.confirm({
                       title: 'Success!',
                       type: 'green',
                       icon: 'fa fa-check',
                       content: "Saved successfully",
                       buttons: {
                           ok: function () {     
                               alloted_permission();
                           }
                       }
                   });
//                 

                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $(".se-pre-con").fadeOut("slow");
                    $("#loadingDiv").hide();
                    messages_hide();
                    var msg = "<strong>Failed to " + operation + " data.</strong><br/>";
                    if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                        msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                    } else {
                        if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                            msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                        } else {
                            msg += "Error(s):<strong><ul>";
                            $.each(jqXHR.responseJSON, function (key, value) {
                                msg += "<li>" + value + "</li>";
                            });
                            msg += "</ul></strong>";
                        }
                    }
                    $.alert({
                        title: 'Error!!',
                        icon: 'fa fa-times',
                        type: 'red',
                        content: msg,
                    });
                }
            });

        });
        alloted_permission();
    });
    function alloted_permission() {
        //alert('hi');
        var user_code = $("#user_code").val();
        var token = $("input[name='_token']").val();
        $(".se-pre-con").fadeIn("slow");
        $.ajax({
            type: 'POST',
            url: 'user_permission_alloted',
            data: {user_code: user_code, _token: token},
            dataType: 'json',
            success: function (response) {
                $(".se-pre-con").fadeOut("slow");
                var len1 = response[0].user_permission_code.length;
                //alert(len1);
                var jsona = '[';
                for (var l1 = 0; l1 < len1; l1++) {
                    if (l1 > 0) {
                        jsona += ','
                    }
                    jsona += '{';

                    jsona += '"text": "' + response[0].user_permission_code[l1].menu.menu + '",' +
                            '"nodes": [';
                    var len2 = response[0].user_permission_code[l1].menu.menu_wise_submenu.length;
                    for (var l2 = 0; l2 < len2; l2++) {
                        var status = response[0].user_permission_code[l1].menu.menu_wise_submenu[l2].user_permission_submenu_cd[0].show_status;
                        if (status == 1) {
                            var Show = "&nbsp;<span class='fa fa-check' style='color: green;'></span>";
                        } else {
                            var Show = "";
                        }
                        if (l2 > 0) {
                            jsona += ',';
                        }
                        jsona += '{';
                        jsona += '"text": ">>&nbsp; ' + response[0].user_permission_code[l1].menu.menu_wise_submenu[l2].submenu + Show + '"' +
                                '}';
                    }
                    jsona += ']';

                    jsona += '}';
                }
                jsona += ']';
                //alert(jsona);
                $('#alloted_permission').treeview({
                    color: "#428bca",
                    expandIcon: 'fa fa-plus-square',
                    collapseIcon: 'fa fa-minus-square',
                    data: jsona
                });
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $(".se-pre-con").fadeOut("slow");
                $("#loadingDiv").hide();
                messages_hide();
                var msg = "<strong>Failed to Fetch data.</strong><br/>";
                if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                    msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                } else {
                    if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                        msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                    } else {
                        msg += "Error(s):<strong><ul>";
                        $.each(jqXHR.responseJSON, function (key, value) {
                            msg += "<li>" + value + "</li>";
                        });
                        msg += "</ul></strong>";
                    }
                }
                $.alert({
                    title: 'Error!!',
                    icon: 'fa fa-times',
                    type: 'red',
                    content: msg
                });
            }
        });
    }

</script>
<!-- Copyright -->
@stop