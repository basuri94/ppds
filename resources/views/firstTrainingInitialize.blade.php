@extends('layouts.master')
@section('content')
<!--// top-bar -->
 <div class="wrapper">
<!-- Sidebar Holder -->
@include('layouts.sidebar')

<!-- Page Content Holder -->
<div id="content">           
<section class="tables-section">
    <nav aria-label="breadcrumb" style="width:100%;">
        <div class="breadcrumb pagehead1">
           
                {!! Form::button('<i class="fa fa-desktop"></i>First Training Initialize',['class' => 'btn btn-primary-header add-new-button']) !!}
            
            <div class="col-md-offset-8 pull-right">
<!--                {!! Form::button('Academic Year:&nbsp;',['id'=>'show_academic','class' => 'btn btn-primary-year']) !!}-->
                
            </div>
                <div class="clearfix"></div>
        </div>
    </nav>

    <!-- form -->
<div class="outer-w3-agile">
  {!! Form::open(['url' => 'firstTrInitialize', 'name' => 'firstTrInitialize', 'class' =>'request-info clearfix form-horizontal', 'id' => 'firstTrInitialize', 'method' => 'post','role'=>'','files' => true]) !!}
  {!! Form::hidden('districtcd', session()->get('districtcd_ppds'),['id'=>'districtcd']) !!}
  
  <div class="panel-group" id="accordion5">                                                              
   <div class="panel panel-default">
    <div class="panel-heading1">
     <h6 class="panel-title">
         <a data-toggle="#" data-parent="#accordion5" href="#collapsePV"> <span class="fa fa-minus"></span> <span class="highlight">First Training Initialize</span></a>
     </h6>
    </div>
    <div id="collapsePV" class="panel-collapse collapse5">
      <div class="panel-body">                                   
       <div class="row"> 
         <div class='col-sm-4'>
               {!! Form::label('zone', 'Zone:', ['class'=>'highlight required']) !!}
              <div class="form-group">
                  <div class=''>
                      {!! Form::select('zone',[''=>'[Select]'],Null,['id'=>'zone','class'=>'form-control','autocomplete'=>'off']) !!}
                  </div>
              </div>
          </div>  

        <div class='col-sm-4'>
            {!! Form::label('phase', 'Phase:', ['class'=>'highlight required']) !!}
           <div class="form-group">
               <div class=''>
                
    <select id="phase" class="form-control" name="phase">
                  
                   <option value="">[Select]</option>
                  

    </select>
                </div>
           </div>
       </div>

           <div class='col-sm-4'>
              {!! Form::label('trainingtype', 'Training Head:', ['class'=>'highlight required']) !!}
              <div class="form-group">
                  <div class=''>
                       {!! Form::select('trainingtype',$training_type['desc'],null,['id'=>'trainingtype','class'=>'form-control','placeholder'=>'[Select]','autocomplete'=>'off']) !!}
                  </div>
              </div>
          </div>
      </div>
          
 
          
<!--      <div class="row"> 
            <div class='col-sm-12'>
                <div class="form-group">
                     <div id='subVenueDetails'></div>
                  </div>
            </div>
         
      </div> -->
      <div class="row">                  
         <div class='col-sm-12'>
            <div class="form-group text-right permit">                            	
               {{ Form::button('Submit', ['class' => 'btn btn-info', 'type' => 'submit','id'=>'Submit']) }}
               {{ Form::button('Reset', ['class' => 'btn btn-success' ,'type' => 'reset','id'=>'reset']) }}
            </div>
          </div>
      </div>     
       
    </div>                              
   </div>
  </div>  
  </div>
 {!! Form::close() !!}             
</div>
<!--// form -->

</section>

</div>
</div>
<!-- Copyright -->
<script type="text/javascript">
$(document).ready(function () {
  getZoneDetails();

$('select[name="zone"]').on('change', function () {
    getZonePhasedata(); 
 });

$("#reset").click(function () {
  location.reload(true);  
});


$('#firstTrInitialize').bootstrapValidator({

        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            zone: {
                validators: {
                    notEmpty: {
                        message: 'Zone is required'
                    }
                }
            },
           phase: {
               validators: {
                   notEmpty: {
                       message: 'Phase is required'
                   }
               }
           },
            
            trainingtype: {
                validators: {
                    notEmpty: {
                        message: 'Training Head is required'
                    }
                }
            }           
        }

    }).on('success.form.bv', function (e) {
        e.preventDefault();
        var action = $(this).attr('action');
        var type = $(this).attr('method');
        var formData = new FormData($(this)[0]);
       $("#Submit").attr('disabled',true);
       
       
       
        var table_edit = '<form id="traniningInitialisePass" action="traniningInitialisePass" method="post" class="form-horizontal">';
        table_edit += '<div class="form-group">';
       // table_edit += '<label class="control-label col-md-4 required" style="font-size:12px;">Enter Password:</label>';
        table_edit += '<div class="col-md-11"><input type="password" class="form-control" id="password" name="password" style="margin-bottom: 2px;" /></div>';
        table_edit += '</div>';
        table_edit += '</form>';
        var jc = $.confirm({
            title: '<span style="font-size: 17px;">Enter Training Initialise Password</span>',
            content: table_edit,
            type: 'green',
            icon: 'fa fa-edit',
            typeAnimated: true,
            buttons: {

                proceed: {
                    btnClass: 'btn-primary',
                    action: function () {
                        var flag = 0;
                        var check = $("#password").val();
                        if (check == '') {
                            $.alert({
                                title: 'Error!!',
                                type: 'red',
                                icon: 'fa fa-exclamation-triangle',
                                content: "Please enter password"
                            });
                            flag = 1;
                            return false;
                        }
                        if (flag > 0) {
                            return false;
                        }
                        jc.showLoading(true);
                        var password = $("#password").val();
                        
                         $.ajax({
                            type: 'post',
                            url: 'check_for_training_initialise_password',
                            data: {'password': password, '_token': $('input[name="_token"]').val()},
                            dataType: 'json',
                            success: function (datam) {
                              if(datam.options>0){
                                  $(".se-pre-con1").show();
                                    return $.ajax({
                                            type: "post",
                                            url: "firstTrInitialize",
                                            data: formData,
                                            processData: false,
                                            contentType: false

                                        }).done(function (data) {
                                            $(".se-pre-con1").hide();
                                          if (data.status == 1) {  
                                              jc.hideLoading(true);
                                                
                     var msg="<span style='font-size:13.5px;'>Record(s) initialised successfully</br>";
                     //msg+="Total Number of PP => "+data.totalPP;
                     msg+="</br>Total No of initialized PP => "+data.totalInitializePP;
                    
                     msg+="</span>";
                     $.confirm({
                        title: 'Success!',
                        type: 'green',
                        icon: 'fa fa-check',
                        content: msg,
                        buttons: {
                            ok: function () {     
                                location.reload(true);
                            }

                        }
                    });

                } else if (data.status == 0){
                   $.alert({
                        title: 'Error!!',
                        type: 'red',
                        icon: 'fa fa-exclamation-triangle',
                        content: 'First Training allocation is empty'
                    });
                    $("#Submit").attr('disabled',false);  
                }
                                            
                                            
                                            
                                        }).fail(function (jqXHR, textStatus, errorThrown) {
                                            $(".se-pre-con1").hide();
                                            var msg = "";
                                            if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                                                msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                                            } else {
                                                if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                                                    if (jqXHR.responseJSON.exception_code == 23000) {
                                                        msg += "Check Password.";
                                                    } else {
                                                        msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                                                    }
                                                } else {
                                                    msg += "Error(s):<strong><ul>";
                                                    $.each(jqXHR.responseJSON, function (key, value) {
                                                        msg += "<li>" + value + "</li>";
                                                    });
                                                    msg += "</ul></strong>";
                                                }
                                            }
                                            $.alert({
                                                title: 'Error!!',
                                                type: 'red',
                                                icon: 'fa fa-warning',
                                                content: msg,
                                            });
                                        });
                              }else{
                                 $("#Submit").attr('disabled',false);
                                    $.alert({
                                        title: 'Error!!',
                                        type: 'red',
                                        icon: 'fa fa-exclamation-triangle',
                                        content: "Please enter correct password"
                                    });
                                    
                                }
                            },
                            error: function (jqXHR, textStatus, errorThrown) {
                                $(".se-pre-con1").hide();
                                var msg = "";
                                if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                                    msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                                } else {
                                    if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                                        msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                                    } else {
                                        msg += "Error(s):<strong><ul>";
                                        $.each(jqXHR.responseJSON, function (key, value) {
                                            msg += "<li>" + value + "</li>";
                                        });
                                        msg += "</ul></strong>";
                                    }
                                }
                                $.alert({
                                    title: 'Error!!',
                                    type: 'red',
                                    icon: 'fa fa-warning',
                                    content: msg
                                });
                                $("#reserve").attr('disabled',false);
                            }
                        });
                         
                        
                       
                    }
                },
                close: function () {
//                   $("#submit").attr('disabled',false);
                }
            },
            onContentReady: function () {
        // bind to events

            },

            onOpen: function () {
                // after the modal is displayed.
                //startTimer();
            }
           });
       
       
       
       
       
       
        
    });
});
function getZoneDetails(){
    var forDist = $("#districtcd").val();
    var token = $("input[name='_token']").val();
    $(".se-pre-con").fadeIn("slow");
    $.ajax({
       url: "getZoneDetails",
       method: 'POST',
       data: {forDist: forDist, _token: token},
       success: function (data) {//alert(data.options);
         $(".se-pre-con").fadeOut("slow");
          $("select[name='zone'").html('');
          if(data.status==1)
          {
             $("select[name='zone'").append('<option value>[Select]</option>');
            $.each(data.options,function (k, v)
            {
                $("select[name='zone'").append('<option value='+k+'>'+v+'</option>');
            });
           // getMemberDetails();
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
            $(".se-pre-con").fadeOut("slow");
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-warning',
                  content: msg
              });
         }
   });
 }
 

function getZonePhasedata(){
  var zone = $("#zone").val();

   var token = $("input[name='_token']").val();
  // $(".se-pre-con").fadeIn("slow");
   $.ajax({
       url: "getZonePhasedata",
       method: 'POST',
       data: {zone: zone, _token: token},
       success: function (data) {//alert(data.options);
       //   $(".se-pre-con").fadeOut("slow");
          $("select[name='phase'").html('');
          
          if(data.status==1)
          {
            $("select[name='phase'").append('<option value>[Select]</option>');
            $.each(data.options,function (k, v)
            {
                $("select[name='phase'").append('<option value='+k+'>'+v+'</option>');
            });
            //$("select[name='subdivision'").html(data.options);
             // change_officeName();
             
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
       //  $(".se-pre-con").fadeOut("slow");
           var msg = "";
           if (jqXHR.status !== 422 && jqXHR.status !== 400) {
               msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
           } else {
               if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                   msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
               } else {
                   msg += "Error(s):<strong><ul>";
                   $.each(jqXHR.responseJSON, function (key, value) {
                       msg += "<li>" + value + "</li>";
                   });
                   msg += "</ul></strong>";
               }
           }
           $.alert({
               title: 'Error!!',
               type: 'red',
               icon: 'fa fa-warning',
               content: msg
           });
         }
   });
}
</script>
@stop