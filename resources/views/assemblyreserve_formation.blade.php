@extends('layouts.master')
@section('content')
<!--// top-bar -->
 <div class="wrapper">
<!-- Sidebar Holder -->
@include('layouts.sidebar')

<!-- Page Content Holder -->
<div id="content">           
<section class="tables-section">
    <nav aria-label="breadcrumb" style="width:100%;">
        <div class="breadcrumb pagehead1">
           <span class="headbrand"><i class="fa fa-desktop"></i> Add/Edit Assembly Reserve
                        <span class="scoop-mcaret1"></span>
                    </span>&nbsp;&nbsp;
                    &nbsp;{!! Form::button(' List of Assembly Reserve&nbsp;<i class="fa fa-arrow-circle-right"></i>',['id'=>'show_asm_res_list','class' => 'btn btn-primary-year add-new-button']) !!}  

<!--                {!! Form::button('<i class="fa fa-desktop"></i> Add Assembly Reserve',['class' => 'btn btn-primary-header add-new-button']) !!}
            
            <div class="col-md-offset-7 pull-right">
                {!! Form::button('Academic Year:&nbsp;',['id'=>'show_academic','class' => 'btn btn-primary-year']) !!}
   {!! Form::button('List of Assembly Reserve&nbsp;<i class="fa fa-arrow-circle-right"></i>',['id'=>'show_asm_res_list','class' => 'btn btn-primary-year add-new-button']) !!}               
            </div>-->
                <div class="clearfix"></div>
        </div>
    </nav>

    <!-- form -->
<div class="outer-w3-agile">
  {!! Form::open(['url' => 'assemblyReserveForm', 'name' => 'assemblyReserveForm', 'class' =>'request-info clearfix form-horizontal', 'id' => 'assemblyReserveForm', 'method' => 'post','role'=>'','files' => true]) !!}
  {!! Form::hidden('districtcd', session()->get('districtcd_ppds'),['id'=>'districtcd']) !!}
  
  <div class="panel-group" id="accordion5">                                                              
   <div class="panel panel-default">
    <div class="panel-heading1">
     <h6 class="panel-title">
         <a data-toggle="#" data-parent="#accordion5" href="#collapsePV"> <span class="fa fa-minus"></span> <span class="highlight">Assemblywise Reserve Formation </span></a>
     </h6>
    </div>
    <div id="collapsePV" class="panel-collapse collapse5">
      <div class="panel-body">                                   
       <div class="row">  
         <div class='col-sm-3'>
            {!! Form::label('forZone', 'For Zone:', ['class'=>'highlight required']) !!}
           <div class="form-group">
               <div class=''>
                   {!! Form::select('forZone',[''=>'[Select]'],Null,['id'=>'forZone','class'=>'form-control','autocomplete'=>'off']) !!}
               </div>
           </div>          
        </div> 
        <div class='col-sm-3'>
               {!! Form::label('type', 'Type (Percentage or Number):', ['class'=>'highlight required']) !!}
              <div class="form-group">
                  <div class=''>
                      {!! Form::select('type',['P'=>'Percentage'],Null,['id'=>'type','class'=>'form-control','autocomplete'=>'off']) !!}
                  </div>
              </div>
          </div>
          <div class='col-sm-3'>
                {!! Form::label('pr', 'PR:', ['class'=>'highlight required']) !!}
               <div class="form-group">
                   <div class=''>
                        {!! Form::text('pr',null,['id'=>'pr','class'=>'form-control','autocomplete'=>'off','maxlength'=>'4']) !!}
                   </div>
               </div>
           </div>
          <div class='col-sm-3'>
                {!! Form::label('p1', 'P1:', ['class'=>'highlight required']) !!}
               <div class="form-group">
                   <div class=''>
                        {!! Form::text('p1',null,['id'=>'p1','class'=>'form-control','autocomplete'=>'off','maxlength'=>'4']) !!}
                   </div>
               </div>
           </div>
        </div>
        <div class="row"> 
          <div class='col-sm-3'>
                {!! Form::label('p2', 'P2:', ['class'=>'highlight required']) !!}
               <div class="form-group">
                   <div class=''>
                        {!! Form::text('p2',null,['id'=>'p2','class'=>'form-control','autocomplete'=>'off','maxlength'=>'4']) !!}
                   </div>
               </div>
           </div>
          <div class='col-sm-3'>
                {!! Form::label('p3', 'P3:', ['class'=>'highlight required']) !!}
               <div class="form-group">
                   <div class=''>
                        {!! Form::text('p3',null,['id'=>'p3','class'=>'form-control','autocomplete'=>'off','maxlength'=>'4']) !!}
                   </div>
               </div>
           </div>
          <div class='col-sm-3'>
                {!! Form::label('pa', 'PA:', ['class'=>'highlight']) !!}
               <div class="form-group">
                   <div class=''>
                        {!! Form::text('pa',null,['id'=>'pa','class'=>'form-control','autocomplete'=>'off','maxlength'=>'4']) !!}
                   </div>
               </div>
           </div>
          <div class='col-sm-3'>
                {!! Form::label('pb', 'PB:', ['class'=>'highlight']) !!}
               <div class="form-group">
                   <div class=''>
                        {!! Form::text('pb',null,['id'=>'pb','class'=>'form-control','autocomplete'=>'off','maxlength'=>'4']) !!}
                   </div>
               </div>
          </div>   
      </div>
          
          
      <div class="row"> 
            <div class='col-sm-12'>
                <div id='assemblyDetails'></div>
            </div>
         
      </div> 
      <div class="row" id="rw" style="display: none;">                                   
           
           <div class='col-sm-12' > 
<!--               {!! Form::label('', '', ['class'=>'highlight']) !!}-->
                <div class="form-group text-right permit" >                            	
                    {{ Form::button('Submit', ['class' => 'btn btn-info', 'type' => 'submit','id'=>'Submit']) }}
                    {{ Form::button('Reset', ['class' => 'btn btn-success' ,'type' => 'reset','id'=>'reset']) }}
                </div>
           </div>
                
       </div>     
       
    </div>                              
   </div>
  </div>  
  </div>
 {!! Form::close() !!}             
</div>
<!--// form -->

</section>
</div>
</div>
<!-- Copyright -->
<script type="text/javascript">
$(document).ready(function () {
  $("#show_asm_res_list").click(function(){
       window.location.href = "assembly_reserve_list"; 
 });
 $("select[name='type'").append('<option value="N">Number</option>');
$("#reset").click(function () {
  location.reload(true);  
});
getZoneDetails(); 
char_num("#pr");
char_num("#p1");
char_num("#p2");
char_num("#p3");
char_num("#pa");
char_num("#pb");
 $('select[name="forZone"]').on('change', function () {
    get_AssemblyPartyDetails();    
 });
 $('#assemblyReserveForm').bootstrapValidator({

        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {

            forZone: {
                validators: {
                    notEmpty: {
                        message: 'Zone is required'
                    }
                }
            },
            type: {
                validators: {
                    notEmpty: {
                        message: 'Type is required'
                    }
                }
            },
            p1: {
                validators: {
                    notEmpty: {
                        message: 'P1 is required'
                    }
                }
            },
            pr: {
                validators: {
                    notEmpty: {
                        message: 'PR is required'
                    }
                }
            },
            p2: {
                validators: {
                    notEmpty: {
                        message: 'P2 is required'
                    }
                }
            },
            p3: {
                validators: {
                    notEmpty: {
                        message: 'P3 is required'
                    }
                }
            }
            
        }

    }).on('success.form.bv', function (e) {
        e.preventDefault();
        var action = $(this).attr('action');
        var type = $(this).attr('method');
        var formData = new FormData($(this)[0]);
       $(".se-pre-con").fadeIn("slow");
        $.ajax({
            type: type,
            url: action,
            data: formData,
            processData: false,
            contentType: false,
            dataType: "json",
            success: function (data) {
                $(".se-pre-con").fadeOut("slow");

                if (data.status == 1) { 
                    if(data.options == 0){
                        var msg="Record(s) saved successfully";
                        $.confirm({
                           title: 'Success!',
                           type: 'green',
                           icon: 'fa fa-check',
                           content: msg,
                           buttons: {
                               ok: function () {     
                                   location.reload(true);
                               }

                           }
                       });
                   }else if (data.options == 1){
                       $.alert({
                        title: 'Error!!',
                        type: 'red',
                        icon: 'fa fa-exclamation-triangle',
                        content: 'PA and PB are required due to existence of greater than 4 members'
                     });
                    $("#Submit").attr('disabled',false);  
                   }else if (data.options == 2){
                       $.alert({
                        title: 'Error!!',
                        type: 'red',
                        icon: 'fa fa-exclamation-triangle',
                        content: 'PB is required due to existence of 6 no of members'
                     });
                    $("#Submit").attr('disabled',false);  
                   }else if (data.options == 3){
                       $.alert({
                        title: 'Error!!',
                        type: 'red',
                        icon: 'fa fa-exclamation-triangle',
                        content: 'PA is required due to existence of greater than 4 members'
                     });
                    $("#Submit").attr('disabled',false);  
                   }

                } 
            },
            error: function (jqXHR, textStatus, errorThrown) {
            $(".se-pre-con").fadeOut("slow");
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-exclamation-triangle',
                  content: msg
              });
              $("#Submit").attr('disabled',false);
           }
        });
    });
});

function get_AssemblyPartyDetails(){
   var forDist = $("#districtcd").val();
   var forZone = $("#forZone").val();
    var token = $("input[name='_token']").val();
    $("#rw").hide();
    $(".se-pre-con").fadeIn("slow");
    $.ajax({
        type: 'POST',
        url: 'getAssemblyPartyDetails',
        data: {forDist:forDist,forZone:forZone, _token:token},
        dataType: 'json',
        success: function (response) {
        $(".se-pre-con").fadeOut("slow");
        $("#assemblyDetails").html("");
        var str='<input type="hidden" id="row_count" name="row_count" value="'+response.options.length+'"> '
        str+= "<table class='table table-bordered table-striped' width='100%' style='border-top: 2px solid #4cae4c;'>";
        str += ' <tr style="background-color: #f5f8fa"><th align="left">&nbsp;<input type="checkbox" id="myCheckall" name="myCheckall"  value="1"  class="myCheckall">&nbsp;</th><th align="left">&nbsp;Assembly Name </th><th>&nbsp;No of Member </th><th>&nbsp;Gender </th><th>&nbsp;No of Party </th><th>&nbsp;Reserve Status </th></tr>';
//         str += '<tr style=" background-color: #1ABB9C; color: #FFFFFF; font-weight: bold;"><td align="left"><input type="checkbox" id="myCheckall" name="myCheckall"  value="1"  class="submenu">&nbsp; Check All</td><td align="center" width="30%"><input type="checkbox" id="view_all" class="flat"></td></tr>';
         var sub_length = response.options.length;
        // alert(sub_length);
         for (var i = 0; i < sub_length; i++)
         {
               var c = parseInt(i) + 1;
               var gender;
               if(response.options[i].gender=='M'){
                   gender="Male";
               }else{
                   gender="Female";
               }
                str += '<tr><td align="left" width="5%">&nbsp;';
                
                str+='<input type="checkbox" id="myCheck' + c + '" name="myCheck' + c + '"  value="'+ response.options[i].assemblycd+''+response.options[i].no_of_member+''+response.options[i].gender+'"  class="submenu" >';                             
                str+='</td><td align="left" width="30%">&nbsp;' + response.options[i].assemblycd +' - '+ response.options[i].assemblyname + '</td>';
                str+='</td><td align="left" width="19%">&nbsp;' + response.options[i].no_of_member + '</td>';
                str+='</td><td align="left" width="10%">&nbsp;' + gender + '</td>';
                str+='</td><td align="left" width="19%">&nbsp;' + response.options[i].no_party + '</td>';
                
                str+='<td align="left" width="17%">';
                if(response.options[i].reserveStatus==0)
                {
                   str+='&nbsp;<span class="fa fa-exclamation-triangle" style="color: red;"></span></td>';
                }
                else{
                  str+='&nbsp;<span class="fa fa-check" style="color: green;"></span></td>';  
                 
                }
                str+='</td></tr>'; // return empty
        }
        str += "</table>";
        $("#assemblyDetails").append(str);
        
          $("#myCheckall").change(function(){ 
            var status = this.checked;
            if(status==true){
                $("#rw").show();
                scrollToElement($('#rw'));
            }else if(status==false){
                $("#rw").hide();
            }
            
            $('.submenu').each(function(){
                this.checked = status;     
            });
        });
        $('.submenu').change(function(){
            if($('.submenu:checked').length=='0'){
                $("#rw").hide();
            }else{
                $("#rw").show();
            }
            if(this.checked == false){
                $("#myCheckall")[0].checked = false; 
            }
            if ($('.submenu:checked').length == $('.submenu').length ){ 
                $("#myCheckall")[0].checked = true;
            }
        });
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $(".se-pre-con").fadeOut("slow");
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-exclamation-triangle',
                  content: msg
              });
         }
    });
}

function getZoneDetails(){
     var forDist = $("#districtcd").val();
     var token = $("input[name='_token']").val();
     $(".se-pre-con").fadeIn("slow");
     $.ajax({
       url: "getZoneDetails",
       method: 'POST',
       data: {forDist: forDist, _token: token},
       success: function (data) {//alert(data.options);
         $(".se-pre-con").fadeOut("slow");
          $("select[name='forZone'").html('');
          if(data.status==1)
          {
             $("select[name='forZone'").append('<option value>[Select]</option>');
            $.each(data.options,function (k, v)
            {
                $("select[name='forZone'").append('<option value='+k+'>'+v+'</option>');
            });
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
            $(".se-pre-con").fadeOut("slow");
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-exclamation-triangle',
                  content: msg
              });
         }
   });
}
function scrollToElement(ele) {
    $('html, body').animate({
        scrollTop: ele.offset().top - 60
    }, 1000);
}
</script>
@stop