
@extends('layouts.master')
@section('content')

 <!--// top-bar -->
 <div class="wrapper">
<!-- Sidebar Holder -->
@include('layouts.sidebar')

<!-- Page Content Holder -->
<div id="content">           
<section class="tables-section">
    <nav aria-label="breadcrumb" style="width:100%;">
        <div class="breadcrumb pagehead1">
           
                {!! Form::button('<i class="fa fa-desktop"></i> Second Subdivision Office-wise PP List',['class' => 'btn btn-primary-header add-new-button']) !!}
            
            <div class="col-md-offset-8 pull-right">
<!--                {!! Form::button('Academic Year:&nbsp;',['id'=>'show_academic','class' => 'btn btn-primary-year']) !!}-->
                
            </div>
                <div class="clearfix"></div>
        </div>
    </nav>

    <!-- form -->
<div class="outer-w3-agile">
  {!! Form::open(['url' => 'secondFirstAppointmentLetterPDF', 'name' => 'secondFirstAppointmentLetterPDF', 'class' =>'request-info clearfix form-horizontal', 'id' => 'secondFirstAppointmentLetterPDF', 'method' => 'post','role'=>'','files' => true]) !!}
  {!! Form::hidden('districtcd', session()->get('districtcd_ppds'),['id'=>'districtcd']) !!}
  {!! Form::hidden('subdivcd', session()->get('subdivisioncd_ppds'),['id'=>'subdivcd']) !!}
  {!! Form::hidden('blockcd', session()->get('blockmunicd_ppds'),['id'=>'blockcd']) !!}
  
<!--  <span class="highlight">[Note: Record(s) transfered from Personnel to Zonewise]</span>-->
   <div class="panel-group" id="accordion5">                                                              
    <div class="panel panel-default">
     <div class="panel-heading1">
     <h6 class="panel-title">
         <a data-toggle="#" data-parent="#accordion5" href="#collapseUV"> <span class="fa fa-minus"></span> <span class="highlight">Second Subdivision Office-wise PP List</span> </a>
     </h6>
     </div>
     <div id="collapseUV" class="panel-collapse collapse5">
      <div class="panel-body">                                   
        <div class="row"> 
           <div class='col-sm-4'>
               {!! Form::label('subdivision', 'Sub Division:', ['class'=>'highlight required']) !!}
              <div class="form-group">
                  <div class=''>
                      {!! Form::select('subdivision',[''=>'[Select]'],null,['id'=>'subdivision','class'=>'form-control','autocomplete'=>'off']) !!}
                  </div>
              </div>
          </div>
             
           <div class='col-sm-4'>
              {!! Form::label('officeName', 'Office Name:', ['class'=>'highlight']) !!}
              <div class="form-group">
                  <div class=''>
                      {!! Form::select('officeName',[''=>'[Select]'],null,['id'=>'officeName','class'=>'form-control','autocomplete'=>'off']) !!}
                  </div>
              </div>
          </div>
            
            <div class='col-sm-4'>
               {!! Form::label('orderno', 'Order Date & No:', ['class'=>'highlight']) !!}
              <div class="form-group">
                  <div class=''>
                      {!! Form::select('orderno',[''=>'[Select]'],Null,['id'=>'orderno','class'=>'form-control','autocomplete'=>'off']) !!}
                  </div>
              </div>
          </div>
          
                             
       </div>
          

                       
       </div>
      </div>
    </div>                              
  </div> 
  
   <div class="panel-group" id="accordion6" style="display:none;">                                                              
    <div class="panel panel-default">
     <div class="panel-heading1">
     <h6 class="panel-title">
         <a data-toggle="#" data-parent="#accordion6" href="#collapsePDF"> <span class="fa fa-minus"></span> <span class="highlight">(PDF)</span> </a>
     </h6>
     </div>
     <div id="collapsePDF" class="panel-collapse collapse5">
      <div class="panel-body">                                   
        <div class="row">                     
          <div class='col-sm-4'>
            {!! Form::label('recordsav', 'Office Records Available:', ['class'=>'highlight']) !!}
            <div class="form-group">
               <div class=''>
                     {!! Form::text('recordsav',null,['id'=>'recordsav','class'=>'form-control','autocomplete'=>'off', 'readonly'=>'true']) !!}
               </div>
            </div>
          </div>
         <div class='col-sm-4'>
            {!! Form::label('from', 'Records From:', ['class'=>'highlight required']) !!}
            <div class="form-group">
               <div class=''>
                     {!! Form::text('from',null,['id'=>'from','class'=>'form-control','autocomplete'=>'off']) !!}
               </div>
            </div>
          </div>
          <div class='col-sm-4'>
            {!! Form::label('to', 'Records To:', ['class'=>'highlight required']) !!}
            <div class="form-group">
               <div class=''>
                     {!! Form::text('to',null,['id'=>'to','class'=>'form-control','autocomplete'=>'off']) !!}
               </div>
            </div>
          </div>            
        </div>
                
       </div>
      </div>
    </div>                              
  </div> 
  <div class="row"> 
    <div class='col-sm-12'>                    
     <div class="form-group text-right permit">                            	
        {{ Form::button('PDF', ['class' => 'btn btn-info', 'type' => 'button','id'=>'PDF']) }}
        {{ Form::button('Reset', ['class' => 'btn btn-success' ,'type' => 'reset','id'=>'reset']) }}
     </div>
   </div>
 </div>
   
  

 {!! Form::close() !!}             
</div>
<!--// form -->


</section>

</div>
</div>
<!-- Copyright -->
<script type="text/javascript">
$(document).ready(function () {
 $("#reset").click(function () {
  location.reload(true);  
});
change_subdivision();

 $("#subdivision").on('change', function () {
   $("select[name='officeName'").html('<option value>[Select]</option>');
   PDFShowHide();
 });
 $("#officeName").on('change', function () {
    
   PDFShowHide();
 });
 $("#orderno").on('change', function () {
    
   PDFShowHide();
 });
  $('#PDF').click(function () {
    var subdivision = $("#subdivision").val();
    var officeName = $("#officeName").val();
    var orderno=$('#orderno').val();
    if(officeName!="" && subdivision!=''){
         var from = 1;
        var to = 1;
    }else{
        var from = $("#from").val();
        var to = $("#to").val();
    }
    
    if(subdivision== ""){
        $.alert({
             title: 'Error!!',
             type: 'red',
             icon: 'fa fa-exclamation-triangle',
             content: 'Subdivision is required'
         });
         return false;
     }
   
    if(subdivision!="" && officeName==""){
        if(from== ""){
           $.alert({
                title: 'Error!!',
                type: 'red',
                icon: 'fa fa-exclamation-triangle',
                content: 'Records From is required'
            });
            return false;
        }
        if(to==""){
           $.alert({
                title: 'Error!!',
                type: 'red',
                icon: 'fa fa-exclamation-triangle',
                content: 'Records To is required'
            });
            return false;
        }
    }
    var datas = {subdivision: subdivision,officeName: officeName,from: from,to: to,orderno:orderno,'_token': $('input[name="_token"]').val()};
    
    //$(".se-pre-con").fadeIn("slow");
      redirectPost_newTab('{{url("getSecondSubOffPPReport")}}', datas);   


 });
   
});
function PDFShowHide(){
//alert('hi');
   var subdivision = $("#subdivision").val(); 
   var officeName = $("#officeName").val(); 
  // alert(officeName);
   
   if(subdivision!=""  && officeName==''){
      //getOfficeRecordAvailable();
      change_officeName();
      $("#accordion6").show();
   }else if(subdivision==""){
      $("#accordion6").hide();
      $("#recordsav").val("");
   }else {
     $("#accordion6").hide();  
     $("#recordsav").val("");
   }
//    if(officeName==""){
//     //  getOfficeRecordAvailable();
//   }else{
//       $("#recordsav").val("");
//   }
}
function change_subdivision(){
  var forDist = $("#districtcd").val();
   var token = $("input[name='_token']").val();
  // $(".se-pre-con").fadeIn("slow");
   $.ajax({
       url: "getSubdivisionDataUserwise",
       method: 'POST',
       data: {forDist: forDist, _token: token},
       success: function (data) {//alert(data.options);
       //   $(".se-pre-con").fadeOut("slow");
          $("select[name='subdivision'").html('');
          
          if(data.status==1)
          {
            $("select[name='subdivision'").append('<option value>[Select]</option>');
            $.each(data.options,function (k, v)
            {
                $("select[name='subdivision'").append('<option value='+k+'>'+v+'</option>');
            });
          }
          getOrderDetails();
       },
        error: function (jqXHR, textStatus, errorThrown) {
         $(".se-pre-con").fadeOut("slow");
           var msg = "";
           if (jqXHR.status !== 422 && jqXHR.status !== 400) {
               msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
           } else {
               if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                   msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
               } else {
                   msg += "Error(s):<strong><ul>";
                   $.each(jqXHR.responseJSON, function (key, value) {
                       msg += "<li>" + value + "</li>";
                   });
                   msg += "</ul></strong>";
               }
           }
           $.alert({
               title: 'Error!!',
               type: 'red',
               icon: 'fa fa-warning',
               content: msg
           });
         }
   });
}
function change_officeName(){
     var forDist = $("#districtcd").val();
     var blockmuni = "";
     var subdivision = $("#subdivision").val();
     var orderno=$('#orderno').val();
     var token = $("input[name='_token']").val();
    $(".se-pre-con").fadeIn("slow");
     $.ajax({
       url: "getOfficeNameUserwiseForSecondRandomization",
       method: 'POST',
       data: {subdivision: subdivision,blockmuni: blockmuni, forDist:forDist,orderno:orderno, _token: token},
       success: function (data) {//alert(data.options);
         $(".se-pre-con").fadeOut("slow");
          $("select[name='officeName'").html('');
          if(data.status==1)
          {
            //$("select[name='officeName'").html(data.options);
            var count=0;
             $("select[name='officeName'").append('<option value>[Select]</option>');
            $.each(data.options,function (k, v)
            {
                $("select[name='officeName'").append('<option value='+k+'>'+v+'</option>');
                count++;
            });
           // getMemberDetails();
           $("#recordsav").val(count);
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
            $(".se-pre-con").fadeOut("slow");
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-warning',
                  content: msg
              });
         }
     });
 }
 
 function getOrderDetails(){
    var forDist = $("#districtcd").val();
    var token = $("input[name='_token']").val();
    //$(".se-pre-con").fadeIn("slow");
    $.ajax({
       url: "getOrderNoDateForSecondAppointment",
       method: 'POST',
       data: {forDist: forDist, _token: token},
       success: function (data) {//alert(data.options);
        // $(".se-pre-con").fadeOut("slow");
          $("select[name='orderno'").html('');
          if(data.status==1)
          {
              $("select[name='orderno'").html(data.options);
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
            $(".se-pre-con").fadeOut("slow");
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-warning',
                  content: msg
              });
         }
   });
 }
</script>
@stop

