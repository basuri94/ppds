
@extends('layouts.master')
@section('content')

 <!--// top-bar -->
 <div class="wrapper">
<!-- Sidebar Holder -->
@include('layouts.sidebar')

<!-- Page Content Holder -->
<div id="content">           
<section class="tables-section">
    <nav aria-label="breadcrumb" style="width:100%;">
        <div class="breadcrumb pagehead1">
           
                {!! Form::button('<i class="fa fa-desktop"></i> MO Details Export',['class' => 'btn btn-primary-header add-new-button']) !!}
            
            <div class="col-md-offset-8 pull-right">
<!--                {!! Form::button('Academic Year:&nbsp;',['id'=>'show_academic','class' => 'btn btn-primary-year']) !!}-->
                
            </div>
                <div class="clearfix"></div>
        </div>
    </nav>

    <!-- form -->
<div class="outer-w3-agile">
  {!! Form::open(['url' => 'getFirstAppointmentLetterPDF', 'name' => 'getFirstAppointmentLetterPDF', 'class' =>'request-info clearfix form-horizontal', 'id' => 'getFirstAppointmentLetterPDF', 'method' => 'post','role'=>'','files' => true]) !!}
  {!! Form::hidden('districtcd', session()->get('districtcd_ppds'),['id'=>'districtcd']) !!}
  {!! Form::hidden('subdivcd', session()->get('subdivisioncd_ppds'),['id'=>'subdivcd']) !!}
  {!! Form::hidden('blockcd', session()->get('blockmunicd_ppds'),['id'=>'blockcd']) !!}
  
<!--  <span class="highlight">[Note: Record(s) transfered from Personnel to Zonewise]</span>-->
   <div class="panel-group" id="accordion5">                                                              
    <div class="panel panel-default">
     <div class="panel-heading1">
     <h6 class="panel-title">
         <a data-toggle="#" data-parent="#accordion5" href="#collapseUV"> <span class="fa fa-minus"></span><span class="highlight"> Zone-wise & Assembly-wise </span></a>
     </h6>
     </div>
     <div id="collapseUV" class="panel-collapse collapse5">
      <div class="panel-body">                                   
        <div class="row"> 
           
           <div class='col-sm-4'>
               {!! Form::label('forZone', 'For Zone:', ['class'=>'highlight required']) !!}
                <div class="form-group">
                    <div class=''>
                        {!! Form::select('forZone',[''=>'[Select]'],Null,['id'=>'forZone','class'=>'form-control','autocomplete'=>'off']) !!}
                    </div>
                </div>
          </div>
            
          <div class='col-sm-4'>
              {!! Form::label('assembly', 'Assembly:', ['class'=>'highlight required']) !!}
              <div class="form-group">
                  <div class=''>
                      {!! Form::select('assembly',[''=>'[Select]'],null,['id'=>'assembly','class'=>'form-control','autocomplete'=>'off']) !!}
                  </div>
              </div>
          </div>                      
       </div>
        
                       
       </div>
      </div>
    </div>                              
  </div> 
  
   <div class="panel-group" id="accordion6">                                                              
    <div class="panel panel-default">
     <div class="panel-heading1">
     <h6 class="panel-title">
         <a data-toggle="#" data-parent="#accordion6" href="#collapsePDF"> <span class="fa fa-minus"></span> <span class="highlight"> PDF / EXCEL </span></a>
     </h6>
     </div>
     <div id="collapsePDF" class="panel-collapse collapse5">
      <div class="panel-body">                                   
        <div class="row">                     
          <div class='col-sm-4'>
            {!! Form::label('recordsav', 'Records Available:', ['class'=>'highlight']) !!}
            <div class="form-group">
               <div class=''>
                     {!! Form::text('recordsav',null,['id'=>'recordsav','class'=>'form-control','autocomplete'=>'off', 'readonly'=>'true']) !!}
               </div>
            </div>
          </div>
            <div class='col-sm-4'>           
                {!! Form::label('', '', ['class'=>'highlight']) !!}
            <div class="form-group text-right permit">  
               {{ Form::button('EXCEL', ['class' => 'btn btn-info', 'type' => 'button','id'=>'EXCEL']) }}
               {{ Form::button('PDF', ['class' => 'btn btn-info', 'type' => 'button','id'=>'PDF']) }}
               {{ Form::button('Reset', ['class' => 'btn btn-success' ,'type' => 'reset','id'=>'reset']) }}
            </div>
          </div>        
        </div>
          
          
          
        <div class="row"> 
           
        </div>
        
       </div>
      </div>
    </div>                              
  </div> 
  
   
  

 {!! Form::close() !!}             
</div>
<!--// form -->


</section>

</div>
</div>
<!-- Copyright -->
<script type="text/javascript">
$(document).ready(function () {
 $("#reset").click(function () {
  location.reload(true);  
});
 
getZoneDetails(); 
 $('select[name="forZone"]').on('change', function () {   
    $("#recordsav").val("");
    getAssemblyDetails(); 
    //getSecondRecordAvailable();
 });
  $('select[name="assembly"]').on('change', function () {
    getRecordAvailable();
 }); 
  
  
  $('#EXCEL').click(function () {
    var forZone = $("#forZone").val();
    var assembly = $("#assembly").val();
     if(forZone==""){
        $.alert({
             title: 'Error!!',
             type: 'red',
             icon: 'fa fa-exclamation-triangle',
             content: 'For Zone is required'
         });
         return false;
     }
    if(assembly==""){
        $.alert({
             title: 'Error!!',
             type: 'red',
             icon: 'fa fa-exclamation-triangle',
             content: 'Assembly is required'
         });
         return false;
     }
    var datas = {forZone: forZone,assembly: assembly,'_token': $('input[name="_token"]').val()};
    $(".se-pre-con").fadeIn("slow");
    
        redirectPost('{{url("moDetailsEXCEL")}}', datas);   
    
     $(".se-pre-con").fadeOut("slow");
 });
 $('#PDF').click(function () {
    var forZone = $("#forZone").val();
    var assembly = $("#assembly").val();
     if(forZone==""){
        $.alert({
             title: 'Error!!',
             type: 'red',
             icon: 'fa fa-exclamation-triangle',
             content: 'For Zone is required'
         });
         return false;
     }
    if(assembly==""){
        $.alert({
             title: 'Error!!',
             type: 'red',
             icon: 'fa fa-exclamation-triangle',
             content: 'Assembly is required'
         });
         return false;
     }
    var datas = {forZone: forZone,assembly: assembly,'_token': $('input[name="_token"]').val()};
    $(".se-pre-con").fadeIn("slow");
    
        redirectPost_newTab('{{url("mo_pdf")}}', datas);   
    
     $(".se-pre-con").fadeOut("slow");
 });
});
function getZoneDetails(){
     var forDist = $("#districtcd").val();
     var token = $("input[name='_token']").val();
     $(".se-pre-con").fadeIn("slow");
     $.ajax({
       url: "getZoneDetails",
       method: 'POST',
       data: {forDist: forDist, _token: token},
       success: function (data) {//alert(data.options);
         $(".se-pre-con").fadeOut("slow");
          $("select[name='forZone'").html('');
          if(data.status==1)
          {
             $("select[name='forZone'").append('<option value>[Select]</option>');
            $.each(data.options,function (k, v)
            {
                $("select[name='forZone'").append('<option value='+k+'>'+v+'</option>');
            });
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
            $(".se-pre-con").fadeOut("slow");
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-exclamation-triangle',
                  content: msg
              });
         }
   });
}
function getAssemblyDetails(){
    var forZone = $("#forZone").val();
     var token = $("input[name='_token']").val();
     $(".se-pre-con").fadeIn("slow");
     $.ajax({
       url: "getAssemblyDetails",
       method: 'POST',
       data: {forZone: forZone, _token: token},
       success: function (data) {//alert(data.options);
         $(".se-pre-con").fadeOut("slow");
          $("select[name='assembly'").html('');
          if(data.status==1)
          {
              $("select[name='assembly'").html(data.options);
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
            $(".se-pre-con").fadeOut("slow");
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-exclamation-triangle',
                  content: msg
              });
         }
   });
}
function getRecordAvailable(){
    var forZone = $("#forZone").val();
    var assembly = $("#assembly").val();
    
    var token = $("input[name='_token']").val();
    // $(".se-pre-con").fadeIn("slow");
     $.ajax({
       url: "mo_detailsCount",
       method: 'POST',
       data: {forZone: forZone,assembly:assembly, _token: token},
       success: function (data) {//alert(data.options);
        // $(".se-pre-con").fadeOut("slow");
         $("#recordsav").val("");
          if(data.status==1)
          {
              $("#recordsav").val(data.options);
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-exclamation-triangle',
                  content: msg
              });
         }
   });
}
</script>
@stop
