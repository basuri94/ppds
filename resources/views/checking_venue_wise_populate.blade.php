@extends('layouts.master')
@section('content')
<div class="wrapper">
    @include('layouts.sidebar')
    <div id="content">           
        <section class="tables-section">
            <nav aria-label="breadcrumb" style="width:100%;">
                <div class="breadcrumb pagehead1">
                        {!! Form::button('<i class="fa fa-desktop"></i>  Checking Venue Wise After Populate',['class' => 'btn btn-primary-header add-new-button']) !!}
                        <div class="clearfix"></div>
                </div>
            </nav>
            <div class="outer-w3-agile">
                {!! Form::hidden('districtcd', session()->get('districtcd_ppds'),['id'=>'districtcd']) !!}

                <div class="panel-group" id="accordion5">                                                              
                    <div class="panel panel-default">
                     <div class="panel-heading1">
                     <h6 class="panel-title">
                         <a data-toggle="#" data-parent="#accordion5" href="#collapseUV"> <span class="fa fa-minus"></span> <span class="highlight">Checking Venue Wise After Populate</span> </a>
                         {{-- <a data-toggle="#" data-parent="#accordion5" href="#collapseUV"> <span class="fa fa-minus"></span> <span class="highlight">Zone And Phase Wise General Traning Checking</span> </a> --}}
                     </h6>
                     </div>
                    <div id="collapseUV" class="panel-collapse collapse5">
                      <div class="panel-body"> 
                            <div class="row">                                   
                                <div class='col-md-12'>

                                {{-- <ul class="nav nav-tabs bg-dark" style="width: 541px;">
                                    <div class="col-md-6">
                                        <li class="nav-item" id="firsttraningTab">
                                            <a style="font-weight: 300;background-color: #5c615e;" class="nav-link active text-light" id="firsttraningTabid" data-toggle="tab" href="#firsttraningdetails"><b>First Training</b></a>
                                        </li>
                                    </div>
                                    <div class="col-md-6">
                                    <li class="nav-item" id="secondtraningTab">
                                    <a style="font-weight: 300;background-color: #5c615e;" class="nav-link text-light" id="secondtraningTabid" data-toggle="tab" href="#secondtraningdetails"><b>Second Training</b></a>
                                    </li>
                                </div>
                                </ul> --}}
                                <ul class="nav nav-tabs">
                                    <li class="nav-item">
                                      <a class="nav-link active" data-toggle="tab" href="#firsttraningdetails">First Training</a>
                                    </li>
                                    <li class="nav-item">
                                      <a class="nav-link"  data-toggle="tab" href="#secondtraningdetails">Second Training</a>
                                    </li>
                                   
                                  </ul>


                            </div>
                        </div>
                            {{-- <div class="tab-content">
                                <div id="firsttraningdetails" class="tab-pane fade in active show">
                                    <div class="row"> 
                                        <div class='col-sm-12'>
                                            <div id='first_training_details'></div>
                                        </div>
                                    </div>
                                </div>
                                <div id="secondtraningdetails" class="tab-pane fade in show">
                                    <div class="row"> 
                                        <div class='col-sm-12'>
                                            <div id='second_training_details'></div>
                                        </div>
                                    </div>
                                </div>
                            </div>     --}}
                            <div class="tab-content" style="margin-top: 20px;">
                                <div class="tab-pane container active" id="firsttraningdetails">
                                    <div class="row"> 
                                        <div class='col-sm-12'>
                                            <div id='first_training_details'></div>
                                        </div>
                                    </div>
                                    {{-- <div class="row"> 
                                        <div class='col-sm-12'>                    
                                        <div class="form-group text-right permit">                            	
                                           
                                            {{ Form::button('EXCEL', ['class' => 'btn btn-info', 'type' => 'button','id'=>'EXCEL']) }}
                                           {{ Form::button('Reset', ['class' => 'btn btn-success' ,'type' => 'reset','id'=>'reset']) }}
                                        </div>
                                        </div>
                                  </div>  --}}
                                </div>
                                <div class="tab-pane container fade" id="secondtraningdetails">
                                    <div class="row"> 
                                        <div class='col-sm-12'>
                                            <div id='second_training_details'></div>
                                        </div>
                                    </div>
                                    <div class="row"> 
                                        <div class='col-sm-12'>                    
                                        <div class="form-group text-right permit">                            	
{{--                                            
                                            {{ Form::button('EXCEL', ['class' => 'btn btn-info', 'type' => 'button','id'=>'EXCEL']) }}
                                           {{ Form::button('Reset', ['class' => 'btn btn-success' ,'type' => 'reset','id'=>'reset']) }} --}}
                                        </div>
                                        </div>
                                  </div> 
                                </div>
                               
                            </div>



                        </div>
                    </div>
                </div>                              
            </div> 
                           
                </div>
            </section>
</div>
</div>
<script>
    $(document).ready(function () {
        getVenueSubvenueWiseChecking();

        $('#EXCEL').click(function () {
            var fordist = $("#districtcd").val();
            var token = $("input[name='_token']").val();
            
             var datas = {'fordist': fordist,'_token': $('input[name="_token"]').val()};
             redirectPost('{{url("getFirstTraningPopulateCheckingEXcel")}}', datas);
        });

    });

    function getVenueSubvenueWiseChecking() {
    var fordist = $("#districtcd").val();
  
    var token = $("input[name='_token']").val();
    $(".se-pre-con").fadeIn("slow");
    $.ajax({
        
        method: 'POST',
        url: "getVenueSubvenueWiseChecking",
        data: {fordist:fordist,_token:token},
        dataType: 'json',
        success: function (response) {
        $(".se-pre-con").fadeOut("slow");
        $("#first_training_details").html("");
        var str = "";
        str+= "<table class='table table-bordered table-striped' width='100%' style='border-top: 2px solid #4cae4c;'>";
        str += ' <tr style="background-color: #f5f8fa"><th rowspan="2" style="width: 15%;">&nbsp;Venue Name </th><th rowspan="2" style="width: 15%;">&nbsp;Sub Venue Name </th><th rowspan="2" style="width: 15%;">&nbsp;Phase </th><th rowspan="2" style="width: 15%;">&nbsp;Traning Date Time </th><th colspan="4" scope="colgroup">&nbsp;Training Populated</th></tr>';
       
        str += '<tr style="background-color: #f5f8fa"><th>P1</th><th>P2</th><th>P3</th><th>PR</th></tr>';
  
    
        str += response.requirement;
       
       $("#first_training_details").html(str);



       $("#second_training_details").html("");
        var str1 = "";
        str1+= "<table class='table table-bordered table-striped' width='100%' style='border-top: 2px solid #4cae4c;'>";
            str1 += ' <tr style="background-color: #f5f8fa"><th rowspan="2" style="width: 15%;">&nbsp;Venue Name </th><th rowspan="2" style="width: 15%;">&nbsp;Sub Venue Name </th><th rowspan="2" style="width: 15%;">&nbsp;Phase </th><th rowspan="2" style="width: 15%;">&nbsp;Traning Date Time </th><th colspan="4" scope="colgroup">&nbsp;Training Populated</th></tr>';
            
            str1 += '<tr style="background-color: #f5f8fa"><th>P1</th><th>P2</th><th>P3</th><th>PR</th></tr>';
            str1 += response.requirement1; 
       
       $("#second_training_details").html(str1);
        }
        // error: function (jqXHR, textStatus, errorThrown) {
        //     $(".se-pre-con").fadeOut("slow");
        //       var msg = "";
        //       if (jqXHR.status !== 422 && jqXHR.status !== 400) {
        //           msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
        //       } else {
        //           if (jqXHR.responseJSON.hasOwnProperty('exception')) {
        //               msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
        //           } else {
        //               msg += "Error(s):<strong><ul>";
        //               $.each(jqXHR.responseJSON, function (key, value) {
        //                   msg += "<li>" + value + "</li>";
        //               });
        //               msg += "</ul></strong>";
        //           }
        //       }
        //       $.alert({
        //           title: 'Error!!',
        //           type: 'red',
        //           icon: 'fa fa-warning',
        //           content: msg
        //       });
        //  }
   
    });

}

</script>
@stop
