@extends('layouts.master')
@section('content')
<div class="wrapper">
    @include('layouts.sidebar')
    <div id="content">
        <section class="tables-section">
            <nav aria-label="breadcrumb" style="width:100%;">
                <div class="breadcrumb pagehead1">
                    {!! Form::button('<i class="fa fa-desktop"></i> Zone And Phase Wise Before First Randomisation Checking',['class' => 'btn
                    btn-primary-header add-new-button']) !!}
                    <div class="clearfix"></div>
                </div>
            </nav>
            <div class="outer-w3-agile">
                {{-- {!! Form::open(['url' => 'first_random_checking', 'name' => 'first_random_checking', 'class' =>'request-info clearfix form-horizontal', 'id' => 'first_random_checking', 'method' => 'post','role'=>'','files' => true]) !!} --}}
                {!! Form::open(['url' => 'first_randomisation_checking', 'name' => 'first_randomisation_checking',
                'class' =>'request-info clearfix form-horizontal', 'id' => 'first_randomisation_checking', 'method' =>
                'post','role'=>'','files' => true]) !!}
                {!! Form::hidden('districtcd', session()->get('districtcd_ppds'),['id'=>'districtcd']) !!}
                <div class="panel-group" id="accordion5">
                    <div class="panel panel-default">
                        <div class="panel-heading1">
                            <h6 class="panel-title">
                                <a data-toggle="#" data-parent="#accordion5" href="#collapseUV"> <span
                                        class="fa fa-minus"></span> <span class="highlight">Zone And Phase Wise First Before
                                        Randomisation Checking</span> </a>
                                {{-- <a data-toggle="#" data-parent="#accordion5" href="#collapseUV"> <span class="fa fa-minus"></span> <span class="highlight">Zone And Phase Wise General Traning Checking</span> </a> --}}
                            </h6>
                        </div>
                        <div id="collapseUV" class="panel-collapse collapse5">
                            <div class="panel-body">
                                <div class="row">
                                    <div class='col-sm-4'>
                                        {!! Form::label('zone', 'Zone:', ['class'=>'highlight required']) !!}
                                        <div class="form-group">
                                            <div class=''>
                                                {!!
                                                Form::select('zone',[''=>'[Select]'],Null,['id'=>'zone','class'=>'form-control','autocomplete'=>'off'])
                                                !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class='col-sm-4'>
                                        {!! Form::label('phase', 'Phase:', ['class'=>'highlight required']) !!}
                                        <div class="form-group">
                                            <div class=''>
                                                <select id="phase" class="form-control" name="phase">
                                                    <option value="">[Select]</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class='col-sm-12'>
                                        <div id='assemblyDetails'></div>
                                    </div>
                                </div>


                                <div class="row"> 
                                    <div class='col-sm-12'>                    
                                    <div class="form-group text-right permit">                            	
{{--                                        
                                        {{ Form::button('EXCEL', ['class' => 'btn btn-info', 'type' => 'button','id'=>'EXCEL']) }}
                                       {{ Form::button('Reset', ['class' => 'btn btn-success' ,'type' => 'reset','id'=>'reset']) }} --}}
                                    </div>
                                    </div>
                              </div> 
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </section>
    </div>
</div>

<script>
    $(document).ready(function () {
        getZoneDetails();
        $("#zone").change(function(){
            var zone = $("#zone").val();
            var phase = $("#phase").val();
            if (zone!=""){
                getZonePhasedata();
            }
            if(zone!="" && phase!=""){
            getphasewisecheking();
          }
          
        });
        $("#phase").change(function(){
            var zone = $("#zone").val();
    var phase = $("#phase").val();
    if(zone!="" && phase!=""){
            getphasewisecheking();
          }

           
            
        });
        
        $('#EXCEL').click(function () {
            var zone = $("#zone").val();
            var phase = $("#phase").val();
            // alert(phase);
            if(zone=="" || phase==''){
                $.alert({
                    title: 'Error!!',
                    type: 'red',
                    icon: 'fa fa-exclamation-triangle',
                    content: 'Please Select Both Zone And Phase'
                });
                return false;
            }
             var datas = {'zone': zone,'phase': phase, '_token': $('input[name="_token"]').val()};
             redirectPost('{{url("getBeforeRandomisationPPExcel")}}', datas);
        });

    });
    function getZoneDetails(){
    var forDist = $("#districtcd").val();
    var token = $("input[name='_token']").val();
    $(".se-pre-con").fadeIn("slow");
    $.ajax({
       url: "getZoneDetails",
       method: 'POST',
       data: {forDist: forDist, _token: token},
       success: function (data) {//alert(data.options);
         $(".se-pre-con").fadeOut("slow");
          $("select[name='zone'").html('');
          if(data.status==1)
          {
             $("select[name='zone'").append('<option value>[Select]</option>');
            $.each(data.options,function (k, v)
            {
                $("select[name='zone'").append('<option value='+k+'>'+v+'</option>');
            });
           // getMemberDetails();
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
            $(".se-pre-con").fadeOut("slow");
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-warning',
                  content: msg
              });
         },
   });
 }
 function getZonePhasedata(){
  var zone = $("#zone").val();
  $("#rw").hide();
   var token = $("input[name='_token']").val();
  // $(".se-pre-con").fadeIn("slow");
   $.ajax({
       url: "getZonePhasedata",
       method: 'POST',
       data: {zone: zone, _token: token},
       success: function (data) {//alert(data.options);
       //   $(".se-pre-con").fadeOut("slow");
          $("select[name='phase'").html('');
          
          if(data.status==1)
          {
            $("select[name='phase'").append('<option value>[Select]</option>');
            $.each(data.options,function (k, v)
            {
                $("select[name='phase'").append('<option value='+k+'>'+v+'</option>');
            });
            //$("select[name='subdivision'").html(data.options);
             // change_officeName();
             
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
       //  $(".se-pre-con").fadeOut("slow");
           var msg = "";
           if (jqXHR.status !== 422 && jqXHR.status !== 400) {
               msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
           } else {
               if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                   msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
               } else {
                   msg += "Error(s):<strong><ul>";
                   $.each(jqXHR.responseJSON, function (key, value) {
                       msg += "<li>" + value + "</li>";
                   });
                   msg += "</ul></strong>";
               }
           }
           $.alert({
               title: 'Error!!',
               type: 'red',
               icon: 'fa fa-warning',
               content: msg
           });
         }
   });
}

function getphasewisecheking() {
    var zone = $("#zone").val();
    var phase = $("#phase").val();
   
    //alert(subdivison);
    var token = $("input[name='_token']").val();
    $(".se-pre-con").fadeIn("slow");
    $.ajax({
        type: 'POST',
        url: 'getsubdivisionwisedata',
        data: {zone:zone,phase:phase,_token:token},
        dataType: 'json',
        success: function (response) {
        $(".se-pre-con").fadeOut("slow");
        if(response.status==1){
            $("#assemblyDetails").html("");
        var str = "";
        str+= "<table class='table table-bordered table-striped' width='100%' style='border-top: 2px solid #4cae4c; border='1'>";
        str += ' <tr style="background-color: #f5f8fa"><th rowspan="2" style="width: 15%;">&nbsp;Phase </th><th colspan="4" scope="colgroup">&nbsp;Phase Wise Requirement ( PP )</th><th colspan="4">&nbsp;Phase Wise Swapped ( PP )</th></tr>';
        str += '<tr style="background-color: #f5f8fa"><th>P1</th><th>P2</th><th>P3</th><th>PR</th><th>P1</th><th>P2</th><th>P3</th><th>PR</th></tr>';
        str += response.requirement;
       
        $("#assemblyDetails").html(str);
        }
        else{
            $("#assemblyDetails").html("");
       
        }
       
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $(".se-pre-con").fadeOut("slow");
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-warning',
                  content: msg
              });
         }
   
    });

}

</script>
@stop