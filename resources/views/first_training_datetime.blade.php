
@extends('layouts.master')
@section('content')
<style>
    .dropdown-menu{
        /* right: 67%;*/
        /* left: auto; */
        min-width: 0px !important; 
        width:230px !important;
        padding: 1em !important;
    }

</style>
<div class="wrapper">
    <!-- Sidebar Holder -->
    @include('layouts.sidebar')

    <!-- Page Content Holder -->
    <div id="content">           
        <section class="tables-section">
            <nav aria-label="breadcrumb" style="width:100%;">
                <div class="breadcrumb pagehead1">

                    {!! Form::button('<i class="fa fa-desktop"></i> First Training date & time',['class' => 'btn btn-primary-header add-new-button']) !!}

                    <div class="col-md-offset-8 pull-right">
                        <!--                {!! Form::button('Academic Year:&nbsp;',['id'=>'show_academic','class' => 'btn btn-primary-year']) !!}-->

                    </div>
                    <div class="clearfix"></div>
                </div>
            </nav>

            <!-- form -->
            <div class="outer-w3-agile">
                {!! Form::open(['url' => 'first_training_date_entry', 'name' => 'first_training_date_entry', 'class' =>'request-info clearfix form-horizontal', 'id' => 'first_training_date_entry', 'method' => 'post','role'=>'','files' => true]) !!}
                {!! Form::hidden('districtcd', session()->get('districtcd_ppds'),['id'=>'districtcd']) !!}
                {!! Form::hidden('first_training_date_time_code','',['class'=>'form-control','id'=>'first_training_date_time_code']) !!}

                <div class="panel-group" id="accordion5" style="padding-top: 0px;">                                                              
                    <div class="panel panel-default">
                        <div class="panel-heading1">
                            <h6 class="panel-title">
                                <a data-toggle="#" data-parent="#accordion5" href="#collapseUV"> <span class="fa fa-minus"></span> <span class="highlight">First Training date & time </span></a>
                            </h6>
                        </div>
                        <div id="collapseUV" class="panel-collapse collapse5">
                            <div class="panel-body">                                   
                                <div class="row">                                   
                                    <div class='col-sm-4'>
                                        {!! Form::label('first_train_dt', 'First Training Date:', ['class'=>'highlight required']) !!}
                                        <div class="form-group">
                                            <div class=''>
                                                {!! Form::text('first_train_dt',null,['id'=>'first_train_dt','class'=>'form-control','autocomplete'=>'off','placeholder'=>'DD/MM/YYYY','maxlength'=>'10']) !!}

                                            </div>
                                        </div>
                                    </div>  
                                    <div class='col-sm-4'>
                                        {!! Form::label('first_train_tm', 'First Training Time:', ['class'=>'highlight required']) !!} <span style="color:red;font-size: 11px;font-weight: bold;">[Max:15]</span>
                                        <div class="form-group">
                                            <div class=''>
                                                {!! Form::text('first_train_tm',null,['id'=>'first_train_tm','class'=>'form-control','autocomplete'=>'off','maxlength'=>'15']) !!}

                                            </div>
                                        </div>
                                    </div> 
                                </div>
                                <div class="row" > 
                                    <div class='col-sm-12'>                    
                                        <div class="form-group text-right permit">                            	
                                            {{ Form::button('Submit', ['class' => 'btn btn-info', 'type' => 'submit','id'=>'submit']) }}
                                            {{ Form::button('Reset', ['class' => 'btn btn-success', 'style'=>'display:none;' ,'type' => 'reset','id'=>'reset']) }}
                                        </div>
                                    </div>

                                </div>
                                {!! Form::close() !!}   
                                <div class="row"> 
                                    <div class='col-sm-12'>
                                        <div id='memberDetails'></div>
                                    </div>
                                </div>  


                            </div>
                        </div>                              
                    </div> 


                </div>



            </div>
            <!--// form -->

        </section>
    </div>
</div>
<!-- Copyright -->
<script type="text/javascript">
    $(document).ready(function () {
        getFirstTraningDateRecords();
        $("#reset").click(function () {
            location.reload(true);
        });
        var date = new Date();
        var today = new Date(date.getFullYear(), date.getMonth(), date.getDate());

        $('#first_train_dt').datepicker({
            format: "dd/mm/yyyy",
            todayHighlight: true,
//            endDate: new Date(),
            autoclose: true
        });
        $('#first_train_dt').datepicker('setDate', today);

//        $("#first_train_dt").datepicker({
//            format: "dd/mm/yyyy",
//            autoclose: true,
//            todayHighlight: true,
//            endDate: new Date()})
//                .on('changeDate', function (ev) {
//                    if ($('#datepicker').valid()) {
//                        $('#datepicker').removeClass('invalid').addClass('success');
//                    }
//                });
        $('#first_training_date_entry')
                .bootstrapValidator({
                    message: 'This value is not valid',
                    feedbackIcons: {
                        valid: 'fa fa-check',
                        invalid: 'fa fa-times',
                        validating: 'fa fa-refresh'
                    },
                    fields: {
                        first_train_dt: {
                            validators: {
                                notEmpty: {
                                    message: 'Date is required'
                                }
                            }
                        },
                        first_train_tm: {
                            validators: {
                                notEmpty: {
                                    message: 'Time is required'
                                },
                                regexp: {
                                    regexp: /^[A-Za-z0-9.\s-]+$/,
                                    message: 'Time can consist of alphanumerical characters'
                                }
                            }
                        }
                    }
                }).on('success.form.bv', function (e) {
            // Prevent form submission
            e.preventDefault();
            var action = $(this).attr('action');

            var districtcd = $("#districtcd").val();
            var first_train_dt = $("#first_train_dt").val();
            var first_train_tm = $("#first_train_tm").val();
            var type = $(this).attr('method');
            var token = $("input[name='_token']").val();
            var first_training_date_time_code = $('#first_training_date_time_code').val();
            var fd = new FormData();
            fd.append('districtcd', districtcd);
            fd.append('first_train_dt', first_train_dt);
            fd.append('first_train_tm', first_train_tm);
            fd.append('first_training_date_time_code', first_training_date_time_code);

            fd.append('_token', token);
            if (first_training_date_time_code != '') {
                action = "update_first_date_time_delete"
                fd.append('first_training_date_time_code', first_training_date_time_code);
            }
            $(".se-pre-con").fadeIn("slow");
            $.ajax({
                type: type,
                url: action,
                data: fd,
                processData: false,
                contentType: false,
                dataType: "json",
                success: function (data) {

                    if (data.status == 1)
                    {

                        $(".se-pre-con").fadeOut("slow");

                        var msg = "Record(s) saved successfully";
                        $.confirm({
                            title: 'Success!',
                            type: 'green',
                            icon: 'fa fa-check',
                            content: msg,
                            buttons: {
                                ok: function () {

                                    //  document.getElementById("first_training_date_entry").reset();
                                    $('#first_train_dt').val('');
                                    $('#first_train_tm').val('');
                                    getFirstTraningDateRecords();
                                }

                            }
                        });
                    } else if (data.status == 2) {
                        $(".se-pre-con").fadeOut("slow");
                        var msg = "Record(s) updated successfully";
                        $.confirm({
                            title: 'Success!',
                            type: 'green',
                            icon: 'fa fa-check',
                            content: msg,
                            buttons: {
                                ok: function () {
                                    location.reload(true);

                                }

                            }
                        });
                    }

                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $(".se-pre-con").fadeOut("slow");
                    var msg = "";
                    if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                        msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                    } else {
                        if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                            msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                        } else {
                            msg += "Error(s):<strong><ul>";
                            $.each(jqXHR.responseJSON, function (key, value) {
                                msg += "<li>" + value + "</li>";
                            });
                            msg += "</ul></strong>";
                        }
                    }
                    $.alert({
                        title: 'Error!!',
                        type: 'red',
                        icon: 'fa fa-warning',
                        content: msg
                    });
                }
            });

        });


    });
    function delete_first(date_time_cd) {

//    var reply=confirm('Are you sure to delete this record?');
//                 if(!reply){
//                     return false;
//                 }
        var msg = "Are you sure to delete this record?";
        $.confirm({
            title: 'Confirm!',
            type: 'red',
            icon: 'fa fa-exclamation-triangle',
            content: msg,
            buttons: {
                ok: function () {
                    $.ajax({
                        type: 'post',
                        url: 'first_date_time_delete',
                        data: {'date_time_cd': date_time_cd, '_token': $('input[name="_token"]').val()},
                        dataType: 'json',
                        success: function (data) {
                            if (data.status == 1)
                            {
                                var msg = "<strong>SUCCESS: Record deleted successfully</strong>";
                                $.confirm({
                                    title: 'Success!',
                                    type: 'green',
                                    icon: 'fa fa-check',
                                    content: msg,
                                    buttons: {
                                        ok: function () {
                                            $('#first_training_date_entry').trigger("reset");
                                            getFirstTraningDateRecords();
                                        }

                                    }

                                });

                            } else if (data.status == 2) {
                                $.alert({
                                    title: 'Error!!',
                                    type: 'red',
                                    icon: 'fa fa-warning',
                                    content: data.options
                                });
                            }
                        }
                    });


                },
                cancel: function () {

                }

            }
        });


    }

    function edit_first(date_time_cd) {
        $("#reset").show();
        $(".se-pre-con").fadeIn("slow");
        $.ajax({
            type: 'post',
            url: 'first_date_time_edit',
            data: {'date_time_cd': date_time_cd, '_token': $('input[name="_token"]').val()},
            dataType: 'json',
            success: function (data) {
                $(".se-pre-con").fadeOut("slow");
                $('#first_train_dt').val(data.options[0].trai_date);
                $('#first_train_tm').val(data.options[0].training_time);
                $('#first_training_date_time_code').val(data.options[0].datetime_cd);


            }
        });
    }
    function getFirstTraningDateRecords() {
        var districtcd = $("#districtcd").val();
        var token = $("input[name='_token']").val();
        $(".se-pre-con").fadeIn("slow");
        $.ajax({
            url: "getFirstTraningDateRecords",
            method: 'POST',
            data: {forDist: districtcd, _token: token},
            success: function (json) {//alert(data.options);
                $(".se-pre-con").fadeOut("slow");
                $("#memberDetails").html('');
                if (json.status == 1)
                {
                    //alert(json.options.length);
//              var tr;
//              tr="<table class='table table-bordered table-striped1' width='100%' style='border-top: 2px solid #009fe8;'>";
//              tr+="<tr style='background-color: #f5f8fa'>";
//              tr+="<th colspan='4'>List of Zone";
//              tr+="</th>";
//              tr+="</tr>";
//              tr+="<tr style='background-color: #f5f8fa'>";
//              tr+="<td width='10%'><b>SL#</b></td><td><b>Zone</b></td><td><b>Name of Zone </b></td><td width='10%'><b>Action</b></td>";
//              tr+="</tr>";
//               for (var i = 0; i < json.options.length; i++) {
//                    tr+="<tr><td>" + (i+1) + "</td>";
//                    tr+="<td>" + json.options[i].zone + "</td>";
//                    tr+="<td>" + json.options[i].zonename + "</td>";
//                    tr+="<td><a class='edit_button' title='Edit'  onclick='edit_zone("+json.options[i].zone+");'><i class='fa fa-pencil-alt' aria-hidden='true' value="+json.options[i].zone+"></i></a>";
//                    tr+="</td></tr>"; 
//                } 
//              
//              tr+="</table>";
//              $('#memberDetails').append(tr);
                    $('#memberDetails').html(json.options);


                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $(".se-pre-con").fadeOut("slow");
                var msg = "";
                if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                    msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                } else {
                    if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                        msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                    } else {
                        msg += "Error(s):<strong><ul>";
                        $.each(jqXHR.responseJSON, function (key, value) {
                            msg += "<li>" + value + "</li>";
                        });
                        msg += "</ul></strong>";
                    }
                }
                $.alert({
                    title: 'Error!!',
                    type: 'red',
                    icon: 'fa fa-warning',
                    content: msg
                });
            }
        });

    }
</script>
@stop

