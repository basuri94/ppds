@extends('layouts.master')

@section('content')
<?php
//print_r($pc['pc']);
// print_r(config('constants.USER_CATEGORY'));
//die; 
$collection = collect(config('constants.USER_CATEGORY'));
$cateory = session()->get('category_ppds');
for ($i = 0; $i <= $cateory; $i++) {
    $collection = $collection->except([$i]);
}

$new = $collection->all();
//$abc=array_slice_keys(config('constants.USER_CATEGORY'),array('1','2','3'));
//print_r($new);die;
?>

<!--// top-bar -->
<div class="wrapper">
    <!-- Sidebar Holder -->
    @include('layouts.sidebar')
    <!-- Page Content Holder -->
    <div id="content">           
        <section class="tables-section">
            <nav aria-label="breadcrumb" style="width:100%;">
                <ol class="breadcrumb pagehead1">
                    <span class="headbrand"><i class="fa fa-desktop"></i> <?php
                        if (isset($user_data[0])) {
                            echo "Edit User";
                        } else {
                            echo "Ceate User";
                        }
                        ?>
                        <span class="scoop-mcaret1"></span>
                    </span>&nbsp;&nbsp;&nbsp;{!! Form::button(' List of User&nbsp;<i class="fa fa-arrow-circle-right"></i>',['id'=>'add-new-button','class' => 'btn btn-primary-year add-new-button']) !!}  

                </ol>
            </nav>
            <div class="outer-w3-agile">

                <div style="padding-left:50px;padding-top:5px;">
                    {!! Form::open(['url' => 'add_user_details', 'name' => 'create_user', 'id' => 'create_user', 'method' => 'post' ,'class'=>'animate-form form-horizontal','role'=>'form']) !!}
                    {!! Form::hidden('code', '',['id'=>'code']) !!}
                    <div class="form-group row" id="trUd">
                        {!! Form::label('full_name', 'Full Name:',['class'=>'col-lg-3 col-form-label required']) !!} 
                        <div class="col-lg-8">
                            {!! Form::text('full_name', null, ['id'=>'full_name','class'=>'form-control','placeholder'=>'Enter Full Name','autocomplete'=>'off']) !!}
                        </div>
                    </div>
                    <div class="form-group row" id="trUd">
                        {!! Form::label('username', 'Username:',['class'=>'col-lg-3 col-form-label required']) !!}
                        <div class="col-lg-8">
                            {!! Form::text('username', null, ['id'=>'username','class'=>'form-control','placeholder'=>'Enter User Name','autocomplete'=>'off']) !!}
                        </div>
                    </div>
                    <?php //if(!isset($user_data[0])){     ?> 
                    <div class="form-group row" id="trUd1">
                        {!! Form::label('pass', 'Password:',['class'=>'col-lg-3 col-form-label required']) !!}
                        <div class="col-lg-8">
                            {!! Form::password('pass',['id'=>'pass','class' => 'form-control', 'placeholder' => 'Password', 'type' => 'password','autocomplete'=>'off']) !!}
                        </div>
                    </div>
                    <div class="form-group row" id="trUd2">
                        {!! Form::label('con_pass', 'Confirm Password:',['class'=>'col-lg-3 col-form-label required']) !!}
                        <div class="col-lg-8">
                            {!! Form::password('con_pass',['id'=>'con_pass','class' => 'form-control', 'placeholder' => 'Confirm Password', 'type' => 'password','autocomplete'=>'off']) !!}
                        </div>
                    </div>
                    <?php // }  ?>
                    <div class="form-group row" id="trUd">
                        {!! Form::label('mobileno', 'Mobile No:',['class'=>'col-lg-3 col-form-label required']) !!}
                        <div class="col-lg-8">
                            {!! Form::text('mobileno', null, ['id'=>'mobileno','class'=>'form-control','placeholder'=>'Enter Mobile No','autocomplete'=>'off','maxlength'=>'10']) !!}
                        </div>
                    </div>
                    <div class="form-group row" id="trUd">
                        {!! Form::label('email_user', 'Email Id:',['class'=>'col-lg-3 col-form-label required']) !!}
                        <div class="col-lg-8">
                            {!! Form::text('email_user', null, ['id'=>'email_user','class'=>'form-control','placeholder'=>'Enter Email Id','autocomplete'=>'off','maxlength'=>'50']) !!}
                        </div>
                    </div>
                    <div class="form-group row" id="trUd">
                        {!! Form::label('designation', 'Designation:',['class'=>'col-lg-3 col-form-label required']) !!}
                        <div class="col-lg-8">
                            {!! Form::text('designation', null, ['id'=>'designation','class'=>'form-control','placeholder'=>'Enter Designation','autocomplete'=>'off']) !!}
                        </div>
                    </div>
                    <div class="form-group row">
                        {!! Form::label('category', 'Category:',['class'=>'col-lg-3 col-form-label required']) !!}
                        <div class="col-lg-8">
                            {!! Form::select('category',$new,[''=>'[Select]'],['id'=>'category','class'=>'form-control','placeholder'=>'[Select]','autocomplete'=>'off']) !!}

                        </div>
                    </div>
                    <div class="form-group row" id="trDist">
                        {!! Form::label('districtcd', 'District:',['class'=>'col-lg-3 col-form-label required']) !!}
                        <div class="col-lg-8">
                            {!! Form::select('districtcd',$dist['district'],[''=>'[Select]'],['id'=>'districtcd','class'=>'form-control','placeholder'=>'[Select]','autocomplete'=>'off']) !!}
<!--                                 <select class="form-control" id="districtcd" name="districtcd" >
                                <option value="">[Select]</option>
                            <?php //                                     foreach($get_all as $dist){   ?>
                                  <option value="<?php // echo $dist['districtcd']    ?>"><?php // echo $dist['district']    ?></option>  
                            <?php // }
                            ?>
                            </select>-->
                        </div>
                    </div>
                    <div class="form-group row" id="trSubdiv">
                        {!! Form::label('subdivisioncd', 'Subdivision:',['class'=>'col-lg-3 col-form-label required']) !!}
                        <div class="col-lg-8">
                            <select class="form-control" id="subdivisioncd" name="subdivisioncd" >
                                <option value=''>[Select]</option> 
                            </select>

                        </div>
                    </div>
                    <div class="form-group row" id="trBlock">
                        {!! Form::label('blockcd', 'Block/Municipality:',['class'=>'col-lg-3 col-form-label required']) !!}
                        <div class="col-lg-8">
                            <select class="form-control" id="blockcd" name="blockcd" >
                                <option value=''>[Select]</option> 
                            </select>

                        </div>
                    </div>
                    <div class="form-group row" id="trAssembly">
                        {!! Form::label('assemblycd', 'Assembly:',['class'=>'col-lg-3 col-form-label required']) !!}
                        <div class="col-lg-8">
                            <select class="form-control" id="assemblycd" name="assemblycd" >
                                <option value=''>[Select]</option> 
                            </select>

                        </div>
                    </div>
                    <div class="form-group row" id="trZone">
                        {!! Form::label('zonecd', 'Zone:',['class'=>'col-lg-3 col-form-label required']) !!}
                        <div class="col-lg-8">
                            <select class="form-control" id="zonecd" name="zonecd" >
                                <option value=''>[Select]</option> 
                            </select>

                        </div>
                    </div>
                    <div class="form-group row" id="trParliament">
                        {!! Form::label('parliamentcd', 'PC:',['class'=>'col-lg-3 col-form-label required']) !!}
                        <div class="col-lg-8">
                            {!! Form::select('parliamentcd',$pc['pc'],[''=>'[Select]'],['id'=>'parliamentcd','class'=>'form-control','placeholder'=>'[Select]','autocomplete'=>'off']) !!}

                        </div>
                    </div>

                    <div class="form-group row">
                        <div class='col-sm-12'> 
                            <div class="form-group text-right permit" >                            	
                                {{ Form::button('Submit', ['class' => 'btn btn-info', 'type' => 'submit','id'=>'save_id']) }}
                                {{ Form::button('Reset', ['class' => 'btn btn-success' ,'type' => 'reset','id'=>'reset']) }}
                            </div>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
            <!--// form -->

        </section>

    </div>
</div>
</div>
<script>
    $(document).ready(function () {
     $("#reset").click(function () {
            location.reload(true);  
    });
<?php
if (isset($user_data[0])) {
    ?>
            $("#code").val('<?php echo $user_data[0]->code ?>');
            $('#full_name').val('<?php echo $user_data[0]->full_name ?>');
            $('#designation').val('<?php echo $user_data[0]->designation ?>');
            $('#username').val('<?php echo $user_data[0]->user_id ?>');
            $('#username').attr('readonly', true);
            $('#category').val('<?php echo $user_data[0]->category ?>');
            //$('#category').attr('disabled', true);
            change_category();
            $('#districtcd').val('<?php echo $user_data[0]->districtcd ?>');
            $('#districtcd').attr('disabled', true);
            var category = $('#category').val();
            if (category == 6) {
                get_zone();
            } else if (category == 3 || category == 5 || category == 4 || category == 7) {
                get_sub();
            }
            //            get_sub();
            //            get_assembly();           
            //            get_block();
            $('#subdivisioncd').attr('disabled', true);
            $('#assemblycd').attr('disabled', true);
            $('#zonecd').attr('disabled', true);
            $('#blockcd').attr('disabled', true);
            $("#parliamentcd").val('<?php echo $user_data[0]->parliamentcd ?>');
            $('#parliamentcd').attr('disabled', true);
            $('#email_user').val('<?php echo $user_data[0]->email ?>');
            $('#mobileno').val('<?php echo $user_data[0]->mobileno ?>');

<?php } ?>
        $('#add-new-button').click(function () {

            window.location.href = "user_list";
        });
        $("#districtcd").change(function () {
            var category = $('#category').val();
            if (category != "")
            {
                var districtcd = $('#districtcd').val();
                if (districtcd != "") {
                    if (category == 6) {
                        get_zone();
                    } else if (category == 3 || category == 5 || category == 4 || category == 7) {
                        get_sub();
                    }
                }
            } else {
                $('#districtcd').val("");
                $.alert({
                    title: 'Error!!',
                    type: 'red',
                    icon: 'fa fa-exclamation-triangle',
                    content: "Select Category"
                });

            }
//            get_sub();
//            get_assembly();
            // get_zone();
        });
        $("#subdivisioncd").change(function () {
            var category = $('#category').val();
            var subdivisioncd = $('#subdivisioncd').val();
            if (subdivisioncd != "") {
                if (category == 5) {
                    get_block();
                } else if (category == 7) {
                    get_assembly();
                }
            }

        });
        $("#category").change(function () {
            change_category();
        });
        $('#create_user')
                .bootstrapValidator({
                    message: 'This value is not valid',
                    feedbackIcons: {
                        valid: 'fa fa-check',
                        invalid: 'fa fa-times',
                        validating: 'fa fa-refresh',
                    },

                    fields: {
                        full_name: {
                            validators: {
                                notEmpty: {
                                    message: 'Full Name is required'
                                },
                                stringLength: {
                                    max: 100,
                                    message: 'Full name must not be greater than 100 digit'
                                }
                            }
                        },
                        username: {
                            validators: {
                                notEmpty: {
                                    message: 'User Name is required'
                                },
                                stringLength: {
                                    min: 3,
                                    max: 10,
                                    message: 'Username must be between 3-10 digit'
                                }
                            }
                        },

                        pass: {
                            validators: {
                                notEmpty: {
                                    message: 'Password is required'
                                },
                                stringLength: {
                                    min: 6,
                                    max: 10,
                                    message: 'Password must be between 6-10'
                                },
                                regexp: {
                                    regexp: /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@$#%\-\_])(?=.{6,})/,
                                    message: 'Password contain alteast one uppercase letter, one lowercase letter, one number and one special character[!@#$%^&*_-].'
                                }
                            }
                        },
                        con_pass: {
                            validators: {
                                notEmpty: {
                                    message: 'Confirm Password is required'
                                },
                                identical: {
                                    field: 'pass',
                                    message: 'Password does not match the confirm password'
                                },
                                regexp: {
                                    regexp: /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@$#%\-\_])(?=.{6,})/,
                                    message: 'Confirm Password contain alteast six characters, one uppercase letter, one lowercase letter, one number and one special character[!@#$%^&*_-].'
                                }
                            }
                        },
                        email_user: {
                            validators: {
                                notEmpty: {
                                    message: "Please Provide Email"
                                },
                                regexp: {
                                    regexp: /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i,
                                    message: 'Enter correct email Format'
                                }
                            }
                        },
                        mobileno: {
                            validators: {
                                notEmpty: {
                                    message: 'The Mobile Number is required'
                                },
                                digits: {
                                    message: 'Mobile Number is not valid'
                                },
                                stringLength: {
                                    min: 10,
                                    max: 10,
                                    message: 'Mobile Number must have 10 digit'
                                }
                            }
                        },
                        designation: {
                            validators: {
                                notEmpty: {
                                    message: 'Designation is required'
                                },
                                stringLength: {
                                    max: 50,
                                    message: 'Designation must not be greater than 50 digit'
                                }
                            }
                        },
                        category: {
                            validators: {
                                notEmpty: {
                                    message: 'Category is required'
                                }
                            }
                        },
                        districtcd: {
                            validators: {
                                notEmpty: {
                                    message: 'District is required'
                                }
                            }
                        },
                        subdivisioncd: {
                            validators: {
                                notEmpty: {
                                    message: 'Subdivision is required'
                                }
                            }
                        },
                        blockcd: {
                            validators: {
                                notEmpty: {
                                    message: 'Block/Municipality is required'
                                }
                            }
                        },
                        assemblycd: {
                            validators: {
                                notEmpty: {
                                    message: 'Assembly is required'
                                }
                            }
                        },
                        zonecd: {
                            validators: {
                                notEmpty: {
                                    message: 'Zone is required'
                                }
                            }
                        },
                        parliamentcd: {
                            validators: {
                                notEmpty: {
                                    message: 'Parliament is required'
                                }
                            }
                        }


                    }
                }).on('success.form.bv', function (e) {
            // Prevent form submission
            e.preventDefault();
            var action = $(this).attr('action');
            var type = $(this).attr('method');
            // alert(type);
            var formData = new FormData($('#create_user')[0]);
            // alert(type);
            if ($('#code').val() !== "") {
                action = 'add_user_update';
            }
            $(".se-pre-con").fadeIn("slow");
            $.ajax({
                type: type,
                url: action,
                data: formData,
                processData: false,
                contentType: false,
                dataType: "json",
                success: function (data) {
                    $(".se-pre-con").fadeOut("slow");

                    if (data.status == 1) {

                        var msg = "User created successfully";

                    } else {
                        var msg = "User details updated successfully";
                    }
                    $.confirm({
                        title: 'Success!',
                        type: 'green',
                        icon: 'fa fa-check',
                        content: msg,
                        buttons: {
                            ok: function () {

                                location.reload(true);
                                if (data.status != 1) {
                                  window.location.href = "user_list";
                                }
                            }

                        }
                    });

                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $(".se-pre-con").fadeOut("slow");
                    var msg = "";
                    if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                        msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                    } else {
                        if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                            msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                        } else {
                            msg += "Error(s):<strong><ul>";
                            $.each(jqXHR.responseJSON, function (key, value) {
                                msg += "<li>" + value + "</li>";
                            });
                            msg += "</ul></strong>";
                        }
                    }
                    $.alert({
                        title: 'Error!!',
                        type: 'red',
                        icon: 'fa fa-warning',
                        content: msg,
                    });
                }
            });

        });
    });
    function get_sub() {
        var districtcd = $("#districtcd").val();
        var token = $("input[name='_token']").val();
        $(".se-pre-con").fadeIn("slow");
        $.ajax({
            url: "select_subdivision",
            method: 'POST',
            type: 'json',
            data: {districtcd: districtcd, _token: token},
            success: function (data) {

                $(".se-pre-con").fadeOut("slow");
                $("#subdivisioncd").html('<option value="">[Select]</option>');
                $.each(data.options, function (key, value)
                {
                    $("#subdivisioncd").append('<option value=' + key + '>' + value + '</option>'); // return empty
                });
<?php if (isset($user_data[0])) { ?>
                    $('#subdivisioncd').val('<?php echo $user_data[0]->subdivisioncd; ?>');
<?php } ?>

            },
            error: function (jqXHR, textStatus, errorThrown) {
                $(".se-pre-con").fadeOut("slow");
                var msg = "";
                if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                    msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                } else {
                    if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                        msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                    } else {
                        msg += "Error(s):<strong><ul>";
                        $.each(jqXHR.responseJSON, function (key, value) {
                            msg += "<li>" + value + "</li>";
                        });
                        msg += "</ul></strong>";
                    }
                }
                $.alert({
                    title: 'Error!!',
                    type: 'red',
                    icon: 'fa fa-warning',
                    content: msg
                });
            }, complete: function () {
<?php if (isset($user_data[0])) { ?>
        var category = $('#category').val();
        if (category == 5) {
                get_block();
            } else if (category == 7) {
                get_assembly();
            }
<?php } ?>
            }
        });
    }
    function get_assembly() {
        var districtcd = $("#districtcd").val();
        var token = $("input[name='_token']").val();
        $(".se-pre-con").fadeIn("slow");
        $.ajax({
            url: "select_assebly",
            method: 'POST',
            type: 'json',
            data: {districtcd: districtcd, _token: token},
            success: function (data) {
                $(".se-pre-con").fadeOut("slow");
                $("#assemblycd").html('<option value="">[Select]</option>');
                $.each(data.options, function (key, value)
                {
                    $("#assemblycd").append('<option value=' + key + '>' + value + '</option>'); // return empty
                });
            }, complete: function () {
                <?php if (isset($user_data[0])) { ?>
                    $('#assemblycd').val('<?php echo $user_data[0]->assemblycd; ?>');
                <?php } ?>
            }
        });
    }
    function get_zone() {
        var districtcd = $("#districtcd").val();
        var token = $("input[name='_token']").val();
        $(".se-pre-con").fadeIn("slow");
        $.ajax({
            url: "select_zone",
            method: 'POST',
            type: 'json',
            data: {districtcd: districtcd, _token: token},
            success: function (data) {
                $(".se-pre-con").fadeOut("slow");
                $("#zonecd").html('<option value="">[Select]</option>');
                $.each(data.options, function (key, value)
                {
                    $("#zonecd").append('<option value=' + key + '>' + value + '</option>'); // return empty
                });
<?php if (isset($user_data[0])) { ?>
        $('#zonecd').val('<?php echo $user_data[0]->zonecd; ?>');
<?php } ?>
            }
        });
    }
    function  get_block() {
        var subdivisioncd = $("#subdivisioncd").val();
        var token = $("input[name='_token']").val();
        $(".se-pre-con").fadeIn("slow");
        $.ajax({
            url: "select_block",
            method: 'POST',
            type: 'json',
            data: {subdivisioncd: subdivisioncd, _token: token},
            success: function (data) {
                $(".se-pre-con").fadeOut("slow");
                $("#blockcd").html('<option value="">[Select]</option>');
                $.each(data.options, function (key, value)
                {
                    $("#blockcd").append('<option value=' + key + '>' + value + '</option>'); // return empty
                });

            }, complete: function () {
                <?php if (isset($user_data[0])) { ?>
                    $('#blockcd').val('<?php echo $user_data[0]->blockmunicd; ?>');
                <?php } ?>
            }
        });
    }
    function change_category()
    {
        $('#district').val('');
        $('#subdivisioncd').val('');
        $('#blockcd').val('');
        $('#assemblycd').val('');
        $('#zonecd').val('');
        $('#parliamentcd').val('');
        var cat = $("#category").val();
        if (cat == 0 || cat == 1) {
            $('#trDist').show();
            $('#trSubdiv').hide();
            $('#trZone').hide();
            $('#trParliament').hide();
            $('#trBlock').hide();
            $('#trUd').show();
            $('#trUd1').show();
            $('#trUd2').show();
            $('#trDist').show();
            $('#trAssembly').hide();


            return false;
        } else
        {
            $('#trDist').hide();

        }
        if (cat == 2) {
            $('#trDist').show();
            $('#trSubdiv').hide();
            $('#trZone').hide();
            $('#trParliament').hide();
            $('#trBlock').hide();
            $('#trUd').show();
            $('#trUd1').show();
            $('#trUd2').show();
            $('#trAssembly').hide();
            $('#district').val('');
            return false;
        } else
        {
            $('#trDist').hide();

        }

        if (cat == 3) {
            $('#district').val('');
            $('#trDist').show();
            $('#trSubdiv').show();
            $('#trZone').hide();
            $('#trParliament').hide();
            $('#trBlock').hide();
            $('#trUd').show();
            $('#trUd1').show();
            $('#trUd2').show();
            $('#trAssembly').hide();
            return false;
        } else
            $('#trSubdiv').hide();

        if (cat == 4) {
            $('#district').val('');
            $('#trDist').show();
            $('#trSubdiv').show();
            $('#trZone').hide();
            $('#trParliament').show();
            $('#trBlock').hide();
            $('#trUd').show();
            $('#trUd1').show();
            $('#trUd2').show();
            $('#trAssembly').hide();
            return false;
        } else
            $('#trParliament').hide();

        if (cat == 5) {
            $('#district').val('');
            $('#trDist').show();
            $('#trSubdiv').show();
            $('#trZone').hide();
            $('#trParliament').hide();
            $('#trBlock').show();
            $('#trUd').show();
            $('#trUd1').show();
            $('#trUd2').show();
            $('#trAssembly').hide();
            return false;
        } else
            $('#trBlock').hide();



        if (cat == 6) {
            $('#district').val('');
            $('#trDist').show();
            $('#trZone').show();
            $('#trSubdiv').hide();
            $('#trParliament').hide();
            $('#trBlock').hide();
            $('#trUd').show();
            $('#trUd1').show();
            $('#trUd2').show();
            $('#trAssembly').hide();
            return false;
        } else
        {
            $('#trZone').hide();
        }
        if (cat == 7) {
            $('#district').val('');
            $('#trDist').show();
            $('#trZone').hide();
            $('#trSubdiv').show();
            $('#trParliament').hide();
            $('#trBlock').hide();
            $('#trUd').show();
            $('#trUd1').show();
            $('#trUd2').show();
            $('#trAssembly').show();
            return false;
        } else
        {
            $('#trAssembly').hide();
        }
    }
</script>
<!-- Copyright -->
@stop