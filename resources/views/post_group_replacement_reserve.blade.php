
@extends('layouts.master')
@section('content')

<!--// top-bar -->
<div class="wrapper">
    <!-- Sidebar Holder -->
    @include('layouts.sidebar')

    <!-- Page Content Holder -->
    <div id="content">           
        <section class="tables-section">
            <nav aria-label="breadcrumb" style="width:100%;">
                <div class="breadcrumb pagehead1">

                    {!! Form::button('<i class="fa fa-desktop"></i> Post-group Replacement (Reserve PP)',['class' => 'btn btn-primary-header add-new-button']) !!}

                    <div class="col-md-offset-8 pull-right">
                        <!--                {!! Form::button('Academic Year:&nbsp;',['id'=>'show_academic','class' => 'btn btn-primary-year']) !!}-->

                    </div>
                    <div class="clearfix"></div>
                </div>
            </nav>

            <!-- form -->
            <div class="outer-w3-agile">
                {!! Form::open(['url' => 'preGroupReplacement', 'name' => 'preGroupReplacement', 'class' =>'request-info clearfix form-horizontal', 'id' => 'preGroupReplacement', 'method' => 'post','role'=>'','files' => true]) !!}
                {!! Form::hidden('districtcd', session()->get('districtcd_ppds'),['id'=>'districtcd']) !!}
                <!-- OLD PWise -->
                <div class="panel-group" id="accordion5" style="width: 49%;float: left;">                                                              
                    <div class="panel panel-default">
                        <div class="panel-heading1">
                            <h6 class="panel-title">
                                <a data-toggle="#" data-parent="#accordion5" href="#collapsePV"> <span class="fa fa-minus"></span><span class="highlight"> OLD PERSONNEL </span></a>
                            </h6>
                        </div>
                        <div id="collapsePV" class="panel-collapse collapse5">
                            <div class="panel-body" style="min-height: 515px;">                                   
                                <div class="row"> 
                                    <div class='col-sm-6'>
                                        {!! Form::label('forZone', 'For Zone:', ['class'=>'highlight required']) !!}
                                        <div class="form-group">
                                            <div class=''>
                                                {!! Form::select('forZone',[''=>'[Select]'],Null,['id'=>'forZone','class'=>'form-control','autocomplete'=>'off']) !!}
                                            </div>
                                        </div>
                                    </div>


                                    <div class='col-sm-6'>
                                        {!! Form::label('phase', 'Phase:', ['class'=>'highlight required']) !!}
                                       <div class="form-group">
                                           <div class=''>
                                                <select id="phase" class="form-control" name="phase">
                                                            @php
                                                                $phase_data=App\Phase::get();
                                                            @endphp
                                                            <option value="">[Select]</option>
                                                            @foreach ($phase_data as $item)
                                                            <option value="{{ $item->code}}">{{$item->name}}</option>
                                                            @endforeach
                                                </select>
                                            </div>
                                       </div>
                                   </div>







                                    <div class='col-sm-12'>
                                        {!! Form::label('assembly', 'Assembly:', ['class'=>'highlight required']) !!}
                                        <div class="form-group">
                                            <div class=''>
                                                {!! Form::select('assembly',[''=>'[Select]'],null,['id'=>'assembly','class'=>'form-control','autocomplete'=>'off']) !!}
                                            </div>
                                        </div>
                                    </div> 
                                </div>
                                <div class="row"> 
                                    <div class='col-sm-6'>
                                        {!! Form::label('partyno', 'Polling Party No:', ['class'=>'highlight required']) !!}
                                        <span style="color:red;font-size: 11px;">[Max:4]</span>
                                        <div class="form-group">
                                            <div class=''>
                                                {!! Form::text('partyno',null,['id'=>'partyno','class'=>'form-control','autocomplete'=>'off','maxlength' =>'4']) !!}
                                            </div>
                                        </div>
                                    </div>

                                    <div class='col-sm-6'>
                                        {!! Form::label('poststatus', 'Post Status:', ['class'=>'highlight required']) !!}
                                        <div class="form-group">
                                            <div class=''>
                                                {!! Form::select('poststatus',$poststat['poststatus'],null,['id'=>'poststatus','class'=>'form-control','placeholder'=>'[Select]','autocomplete'=>'off']) !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row"> 
                                    <div class='col-sm-12'>
                                        {!! Form::label('personnel_id', 'Personnel ID:', ['class'=>'highlight required']) !!}
                                        <div class="form-group">
                                            <div class=''>
                                                {!! Form::select('personnel_id',[''=>'[Select]'],null,['id'=>'personnel_id','class'=>'form-control','autocomplete'=>'off','maxlength'=>'11','minlength'=>'11']) !!}
                                            </div>
                                        </div>
                                    </div>     
                                </div>
                                <div class="row"> 
                                    <div class='col-sm-12'> 
                                        <div id="old_personnel_details" >                            	

                                        </div>
                                    </div>
                                </div>                
                            </div>
                        </div>
                    </div>                              
                </div> 

                <!-- New PWise -->
                <div class="panel-group" id="accordion5" style="width: 50%;float: left;margin-left: 3px;">                                                              
                    <div class="panel panel-default">
                        <div class="panel-heading1">
                            <h6 class="panel-title">
                                <a data-toggle="#" data-parent="#accordion5" href="#collapseCV"> <span class="fa fa-minus"></span><span class="highlight"> NEW PERSONNEL (RESERVE) </span></a>
                            </h6>
                        </div>
                        <div id="collapseCV" class="panel-collapse collapse5">
                            <div class="panel-body" style="min-height: 515px;">                                   
                                <div class="row">      
                                    <div class='col-sm-11'>
                                        {!! Form::label('newpersonnel_id', 'New Personnel ID:', ['class'=>'highlight']) !!}
                                        <div class="form-group">
                                            <div class=''>
                                                {!! Form::text('newpersonnel_id',null,['id'=>'newpersonnel_id','class'=>'form-control','autocomplete'=>'off','maxlength'=>'11','minlength'=>'11','readonly'=>'true']) !!}
                                            </div>
                                        </div>
                                    </div>     
                                </div>
                                <div class="row"> 
                                    <div class='col-sm-12'> 
                                        <div id="new_personnel_details">                            	

                                        </div>
                                    </div>
                                </div>  
                            </div> 
                            
                            
                        </div>
                    </div>
                </div> 
                
                
                <div class="panel-group" id="accordion5" style="width: 100%;float: left;">                                                              
                    <div class="panel panel-default">
                        <div class="panel-heading1">
                            <h6 class="panel-title">
                                <a data-toggle="#" data-parent="#accordion5" href="#collapse"> <span class="fa fa-minus"></span><span class="highlight"> Training, Reason </span></a>
                            </h6>
                        </div>
                        <div id="collapse" class="panel-collapse collapse5">
                            <div class="panel-body">                                   
                                <div class="row"> 
                                    <div class='col-sm-3'>
                                        {!! Form::label('reason', 'Reason for Replacement:', ['class'=>'highlight required']) !!}
                                        <div class="form-group">
                                            <div class=''>
                                                {!! Form::text('reason',null,['id'=>'reason','class'=>'form-control','autocomplete'=>'off','maxlength'=>'30']) !!}
                                            </div>
                                        </div>
                                    </div>  
                                </div> 
                                <div class="row" id="rw" style="display: none;"> 
                                    <div class="col-sm-12">

                                        <div class="form-group text-right permit">                            	
                                            {{ Form::button('Search', ['class' => 'btn btn-info', 'type' => 'button','id'=>'search']) }}
                                        {{ Form::button('Replace', ['class' => 'btn btn-info', 'type' => 'button','id'=>'replace','disabled'=>'true']) }}
                                        {{ Form::button('Print', ['class' => 'btn btn-info', 'type' => 'button','id'=>'print','disabled'=>'true']) }}
                                        {{ Form::button('Reset', ['class' => 'btn btn-success' ,'type' => 'reset','id'=>'reset']) }}
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>                              
                </div>

<!--                <div class="panel-group" id="accordion5" style="width: 100%;float: left;">                                                              
                    <div class="panel panel-default">
                        <div class="panel-heading1">
                            <h6 class="panel-title">
                                <a data-toggle="#" data-parent="#accordion5" href="#collapse"> <span class="fa fa-minus"></span><span class="highlight"> Training, Reason </span></a>
                            </h6>
                        </div>
                        <div id="collapse" class="panel-collapse collapse5">
                            <div class="panel-body">                                   
                                <div class="row"> 
                                    <div class='col-sm-3'>
                                        {!! Form::label('reason', 'Reason for Replacement:', ['class'=>'highlight']) !!}
                                        <div class="form-group">
                                            <div class=''>
                                                {!! Form::text('reason',null,['id'=>'reason','class'=>'form-control','autocomplete'=>'off','maxlength'=>'10']) !!}
                                            </div>
                                        </div>
                                    </div>  
                                    <div class='col-sm-4'>
                                        <span style="display:none;" id="venue_sch"><input type="checkbox" id="chkSameVenueTraining" name="chkSameVenueTraining" checked value="true" onclick="return chkSameVenueTraining_change();" />
                                            {!! Form::label('chkSameVenueTraining', 'Training at Same Venue for New Personnel', ['class'=>'highlight']) !!} </span>
                                        <div class="form-group">
                                            <div class='' style="display:none;" id="difnt_sch">
                                                {!! Form::label('training_sch', 'Training Schedule Code:', ['class'=>'highlight required']) !!}
                                                {!! Form::select('training_sch',[''=>'[Select]'],Null,['id'=>'training_sch','class'=>'form-control','autocomplete'=>'off']) !!}
                                            </div>
                                        </div>
                                    </div>


                                </div> 
                               

                            </div>
                        </div>
                    </div>                              
                </div> -->



                {!! Form::close() !!}             
            </div>
            <!--// form -->


        </section>

    </div>
</div>
<!-- Copyright -->
<script type="text/javascript">
//    function chkSameVenueTraining_change()
//    {
//        if (document.getElementById('chkSameVenueTraining').checked == true)
//        {
//            $("#difnt_sch").hide();
//            document.getElementById('training_sch').value = '';
//        } else
//        {
//            $("#difnt_sch").show();
//        }
//    }
    $(document).ready(function () {
        getZoneDetails();
        $('select[name="forZone"]').on('change', function () {
            getAssemblyDetails();
        });
        $('select[name="assembly"]').on('change', function () {
            personnel_list();
        });
        $('#partyno').on('change', function () {
            personnel_list();
        });
        $('select[name="poststatus"]').on('change', function () {
            personnel_list();
        });
        $("#reset").click(function () {
            location.reload(true);
        });
        char_num("#personnel_id");
        //getZoneDetails(); 
        $('#personnel_id').on('change', function () {
            get_personnel_details();
        });
        $("#search").click(function () {
            document.getElementById('replace').disabled = true;
            var personnel_id = $("#personnel_id").val();
            var forZone = $("#forZone").val();
            var phase = $("#phase").val();
            var partyno = $("#partyno").val();
            var assembly = $("#assembly").val();
            var poststatus = $("#poststatus").val();
            if(assembly== ""){
                $.alert({
                     title: 'Error!!',
                     type: 'red',
                     icon: 'fa fa-exclamation-triangle',
                     content: 'Assembly is required'
                 });
                 return false;
             }
             if(forZone==""){
                $.alert({
                     title: 'Error!!',
                     type: 'red',
                     icon: 'fa fa-exclamation-triangle',
                     content: 'For Zone is required'
                 });
                 return false;
             }
             if(phase==""){
                $.alert({
                     title: 'Error!!',
                     type: 'red',
                     icon: 'fa fa-exclamation-triangle',
                     content: 'Phase is required'
                 });
                 return false;
             }
              if(partyno== ""){
                $.alert({
                     title: 'Error!!',
                     type: 'red',
                     icon: 'fa fa-exclamation-triangle',
                     content: 'Polling Party No is required'
                 });
                 return false;
             }
             if(poststatus==""){
                $.alert({
                     title: 'Error!!',
                     type: 'red',
                     icon: 'fa fa-exclamation-triangle',
                     content: 'Post Status is required'
                 });
                 return false;
             }
            if (personnel_id == "") {
                $.alert({
                    title: 'Error!!',
                    type: 'red',
                    icon: 'fa fa-exclamation-triangle',
                    content: 'OLD Personnel ID is required'
                });
                return false;
            }
            
            //var token = $("input[name='_token']").val();
            var formData = new FormData($("#preGroupReplacement")[0]);
            $(".se-pre-con").fadeIn("slow");
            //var formData = {'zone': forzone,'forassembly': forassembly,'ofc_id': ofc_id,'booked': booked, 'gender': gender,'post_stat': post_stat, '_token': token};
            $.ajax({
                type: "post",
                url: "searchNewPPReverseData",
                data: formData,
                processData: false,
                contentType: false,
                dataType: 'json',
                success: function (data) {
                    $(".se-pre-con").fadeOut("slow");
                    if (data.status == 1)
                    {
                        $('#new_personnel_details').html(data.options);
                        $('#newpersonnel_id').val(data.perid);
                        document.getElementById('replace').disabled = false;

                    } else if (json.status == 0)
                    {
                        $('#new_personnel_details').html(data.options);
                        $('#newpersonnel_id').val(data.perid);
                        //$('#rw').hide();
                        document.getElementById('replace').disabled = true;
                    }
                    document.getElementById('print').disabled = true;
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $(".se-pre-con").fadeOut("slow");
                    var msg = "";
                    if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                        msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                    } else {
                        if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                            msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                        } else {
                            msg += "Error(s):<strong><ul>";
                            $.each(jqXHR.responseJSON, function (key, value) {
                                msg += "<li>" + value + "</li>";
                            });
                            msg += "</ul></strong>";
                        }
                    }
                    $.alert({
                        title: 'Error!!',
                        type: 'red',
                        icon: 'fa fa-exclamation-triangle',
                        content: msg
                    });
                }
            });
        });
        $("#replace").click(function () {

           var personnel_id = $("#personnel_id").val();
           var forZone = $("#forZone").val();
            var phase = $("#phase").val();
            var partyno = $("#partyno").val();
            var assembly = $("#assembly").val();
            var poststatus = $("#poststatus").val();
               var reason = $("#reason").val();
            if(assembly== ""){
                $.alert({
                     title: 'Error!!',
                     type: 'red',
                     icon: 'fa fa-exclamation-triangle',
                     content: 'Assembly is required'
                 });
                 return false;
             }
             if(forZone==""){
                $.alert({
                     title: 'Error!!',
                     type: 'red',
                     icon: 'fa fa-exclamation-triangle',
                     content: 'For Zone is required'
                 });
                 return false;
             }
             if(phase==""){
                $.alert({
                     title: 'Error!!',
                     type: 'red',
                     icon: 'fa fa-exclamation-triangle',
                     content: 'Phase is required'
                 });
                 return false;
             }
              if(partyno== ""){
                $.alert({
                     title: 'Error!!',
                     type: 'red',
                     icon: 'fa fa-exclamation-triangle',
                     content: 'Polling Party No is required'
                 });
                 return false;
             }
             if(poststatus==""){
                $.alert({
                     title: 'Error!!',
                     type: 'red',
                     icon: 'fa fa-exclamation-triangle',
                     content: 'Post Status is required'
                 });
                 return false;
             }
            if (personnel_id == "") {
                $.alert({
                    title: 'Error!!',
                    type: 'red',
                    icon: 'fa fa-exclamation-triangle',
                    content: 'OLD Personnel ID is required'
                });
                return false;
            }
            if (reason == "") {
                $.alert({
                    title: 'Error!!',
                    type: 'red',
                    icon: 'fa fa-exclamation-triangle',
                    content: 'Reason is required'
                });
                return false;
            }
            
            var formData = new FormData($("#preGroupReplacement")[0]);
            $(".se-pre-con").fadeIn("slow");
            //var formData = {'zone': forzone,'forassembly': forassembly,'ofc_id': ofc_id,'booked': booked, 'gender': gender,'post_stat': post_stat, '_token': token};
            $.ajax({
                type: "post",
                url: "postReplaceNewPPData",
                data: formData,
                processData: false,
                contentType: false,
                dataType: 'json',
                success: function (data) {
                    $(".se-pre-con").fadeOut("slow");
                    if (data.status == 1)
                    {
                        // $('#new_personnel_details').html(data.options);
                        //  $('#newpersonnel_id').val(data.perid);
                        document.getElementById('replace').disabled = true;
                        document.getElementById('search').disabled = true;
                        //document.getElementById('p_id').disabled=true;
                        document.getElementById('print').disabled = false;
                        $.confirm({
                            title: 'Success!',
                            type: 'green',
                            icon: 'fa fa-check',
                            content: data.options,
                            buttons: {
                                ok: function () {
                                    /*$("#swapping_personnel")[0].reset();
                                     $("#swapping").attr('disabled',true);
                                     $('.form-control-feedback').css('display', 'none');
                                     $('.has-success').removeClass('has-success');*/
                                    //location.reload(true);
                                    print_appt();
                                }

                            }
                        });
                    } else if (data.status == 0)
                    {
                        // $('#new_personnel_details').html(data.options);
                        // $('#newpersonnel_id').val(data.perid);
                        //$('#rw').hide();   
                        document.getElementById('search').disabled = false;
                        document.getElementById('replace').disabled = false;
                        //$("#venue_sch").hide();
                        //$("#difnt_sch").hide();
                        document.getElementById('print').disabled = true;
                        $.confirm({
                            title: 'Error!!',
                            type: 'red',
                            icon: 'fa fa-exclamation-triangle',
                            content: data.options,
                            buttons: {
                                ok: function () {
                                    /*$("#swapping_personnel")[0].reset();
                                     $("#swapping").attr('disabled',true);
                                     $('.form-control-feedback').css('display', 'none');
                                     $('.has-success').removeClass('has-success');*/
                                    // location.reload(true);
                                }

                            }
                        });
                    }

                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $(".se-pre-con").fadeOut("slow");
                    var msg = "";
                    if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                        msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                    } else {
                        if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                            msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                        } else {
                            msg += "Error(s):<strong><ul>";
                            $.each(jqXHR.responseJSON, function (key, value) {
                                msg += "<li>" + value + "</li>";
                            });
                            msg += "</ul></strong>";
                        }
                    }
                    $.alert({
                        title: 'Error!!',
                        type: 'red',
                        icon: 'fa fa-exclamation-triangle',
                        content: msg
                    });
                    document.getElementById('replace').disabled = false;
                }
            });
        });
        $('#print').click(function () {
            print_appt();

        });

    });
    function getZoneDetails() {
        var forDist = $("#districtcd").val();
        var token = $("input[name='_token']").val();
        $(".se-pre-con").fadeIn("slow");
        $.ajax({
            url: "getZoneDetails",
            method: 'POST',
            data: {forDist: forDist, _token: token},
            success: function (data) {//alert(data.options);
                $(".se-pre-con").fadeOut("slow");
                $("select[name='forZone'").html('');
                if (data.status == 1)
                {
                    $("select[name='forZone'").append('<option value>[Select]</option>');
                    $.each(data.options, function (k, v)
                    {
                        $("select[name='forZone'").append('<option value=' + k + '>' + v + '</option>');
                    });
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $(".se-pre-con").fadeOut("slow");
                var msg = "";
                if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                    msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                } else {
                    if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                        msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                    } else {
                        msg += "Error(s):<strong><ul>";
                        $.each(jqXHR.responseJSON, function (key, value) {
                            msg += "<li>" + value + "</li>";
                        });
                        msg += "</ul></strong>";
                    }
                }
                $.alert({
                    title: 'Error!!',
                    type: 'red',
                    icon: 'fa fa-exclamation-triangle',
                    content: msg
                });
            }
        });
    }
    function getAssemblyDetails() {
        var forZone = $("#forZone").val();
        var token = $("input[name='_token']").val();
        $(".se-pre-con").fadeIn("slow");
        $.ajax({
            url: "getAssemblyDetails",
            method: 'POST',
            data: {forZone: forZone, _token: token},
            success: function (data) {//alert(data.options);
                $(".se-pre-con").fadeOut("slow");
                $("select[name='assembly'").html('');
                if (data.status == 1)
                {
                    $("select[name='assembly'").html(data.options);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $(".se-pre-con").fadeOut("slow");
                var msg = "";
                if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                    msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                } else {
                    if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                        msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                    } else {
                        msg += "Error(s):<strong><ul>";
                        $.each(jqXHR.responseJSON, function (key, value) {
                            msg += "<li>" + value + "</li>";
                        });
                        msg += "</ul></strong>";
                    }
                }
                $.alert({
                    title: 'Error!!',
                    type: 'red',
                    icon: 'fa fa-exclamation-triangle',
                    content: msg
                });
            }
        });
    }
    function personnel_list() {
        var forZone = $("#forZone").val();
        var phase = $("#phase").val();
        var partyno = $("#partyno").val();
        var assembly = $("#assembly").val();
        var poststatus = $("#poststatus").val();
        if (forZone != "" && phase != "" && partyno != "" && assembly != "" && poststatus != "") {
            var bookedP ="P";
            var token = $("input[name='_token']").val();
           // $(".se-pre-con").fadeIn("slow");
            $.ajax({
                url: "getSelectedPersonnelID",
                method: 'POST',
                data: {forZone: forZone,phase:phase, partyno: partyno, assembly: assembly, poststatus: poststatus, bookedP : bookedP, _token: token},
                success: function (json) {//alert(data.options);
                 //   $(".se-pre-con").fadeOut("slow");
                    $("select[name='personnel_id'").html('');
                    if (json.status == 1)
                    {
                        $("select[name='personnel_id'").html(json.options);
                        // $('#rw').show();
                        // scrollToElement($('#rw'));
                    } else if (json.status == 0)
                    {
                        // $('#old_personnel_details').html(json.options);
                        //  $('#rw').hide();
                    }
                    //document.getElementById('replace').disabled=true;
                    // document.getElementById('print').disabled=true;
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $(".se-pre-con").fadeOut("slow");
                    var msg = "";
                    if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                        msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                    } else {
                        if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                            msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                        } else {
                            msg += "Error(s):<strong><ul>";
                            $.each(jqXHR.responseJSON, function (key, value) {
                                msg += "<li>" + value + "</li>";
                            });
                            msg += "</ul></strong>";
                        }
                    }
                    $.alert({
                        title: 'Error!!',
                        type: 'red',
                        icon: 'fa fa-warning',
                        content: msg
                    });
                }
            });
        }
    }
    function print_appt() {
        var party_reserve = "P";
        var forZone = $("#zone").val();
        var phase = $("#phase").val();
        var assembly = $("#forassembly").val();
        var group_id = $("#partyno").val();
        var no_of_member = "4";
        var from = "1";
        var to = "1";
        var datas = {party_reserve: party_reserve,forZone: forZone,phase: phase,assembly: assembly,group_id: group_id,no_of_member: no_of_member,from: from,to: to, '_token': $('input[name="_token"]').val()};
        redirectPost_newTab('{{url("getSecondAppointmentLetterPDF452019")}}', datas);
    }
    function get_personnel_details() {
        var personnel_id = $("#personnel_id").val();
        var zone = $("#forZone").val();
        var phase = $("#phase").val();
        if(zone == ""){
               $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-exclamation-triangle',
                  content: 'Zone is required'
              });
              return false;
          }
          if(phase == ""){
               $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-exclamation-triangle',
                  content: 'Phase is required'
              });
              return false;
          }
        var token = $("input[name='_token']").val();
        $(".se-pre-con").fadeIn("slow");
        $.ajax({
            url: "getOldPersonnelDetails",
            method: 'POST',
            data: {personnel_id: personnel_id, zone:zone,phase:phase, _token: token},
            success: function (json) {//alert(data.options);
                $(".se-pre-con").fadeOut("slow");
                //$("#memberDetails").html('');
                if (json.status == 1)
                {
                    $('#old_personnel_details').html(json.options);
                    $('#rw').show();
                    scrollToElement($('#rw'));
                } else if (json.status == 0)
                {
                    $('#old_personnel_details').html(json.options);
                    $('#rw').hide();
                }
                document.getElementById('replace').disabled = true;
                document.getElementById('print').disabled = true;
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $(".se-pre-con").fadeOut("slow");
                var msg = "";
                if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                    msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                } else {
                    if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                        msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                    } else {
                        msg += "Error(s):<strong><ul>";
                        $.each(jqXHR.responseJSON, function (key, value) {
                            msg += "<li>" + value + "</li>";
                        });
                        msg += "</ul></strong>";
                    }
                }
                $.alert({
                    title: 'Error!!',
                    type: 'red',
                    icon: 'fa fa-warning',
                    content: msg
                });
            }
        });
    }

    function scrollToElement(ele) {
        $('html, body').animate({
            scrollTop: ele.offset().top - 60
        }, 1000);
    }
</script>
@stop
