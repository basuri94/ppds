@extends('layouts.master')
@section('content')

 <div class="wrapper">
<!-- Sidebar Holder -->
@include('layouts.sidebar')

<!-- Page Content Holder -->
<div id="content">           
<section class="tables-section">
    <nav aria-label="breadcrumb" style="width:100%;">
        <div class="breadcrumb pagehead1">
           
                {!! Form::button('<i class="fa fa-desktop"></i> First Training Venue Active/Inactive',['class' => 'btn btn-primary-header add-new-button']) !!}
            
          
                <div class="clearfix"></div>
        </div>
    </nav>

    <!-- form -->
<div class="outer-w3-agile">
  {!! Form::open(['url' => '', 'name' => 'trainingActiveInactive', 'class' =>'request-info clearfix form-horizontal', 'id' => 'trainingActiveInactive', 'method' => 'post','role'=>'','files' => true]) !!}
  {!! Form::hidden('districtcd', session()->get('districtcd_ppds'),['id'=>'districtcd']) !!}
  
  
   <div class="panel-group" id="accordion5">                                                              
    <div class="panel panel-default">
     <div class="panel-heading1">
     <h6 class="panel-title">
         <a data-toggle="#" data-parent="#accordion5" href="#collapseUV"> <span class="fa fa-minus"></span> <span class="highlight">First Training Venue Active/Inactive</span></a>
     </h6>
     </div>
     <div id="collapseUV" class="panel-collapse collapse5">
      <div class="panel-body">                                   
        <div class="row">  
            <div class='col-sm-3'>
                {!! Form::label('zone', 'Zone:', ['class'=>'highlight required']) !!}
               <div class="form-group">
                   <div class=''>
                       {!! Form::select('zone',[''=>'[Select]'],Null,['id'=>'zone','class'=>'form-control','autocomplete'=>'off']) !!}
                   </div>
               </div>
           </div>  
          <div class='col-sm-3'>
             {!! Form::label('subdivision', 'Sub Division:', ['class'=>'highlight required']) !!}
               <!--<span class="highlight">Sub Division</span> -->
               <div class="form-group">
                   <div class=''>
                       {!! Form::select('subdivision',[''=>'[Select]'],null,['id'=>'subdivision','class'=>'form-control','autocomplete'=>'off']) !!}
                   </div>
               </div>
         </div>  
        </div>     
        <div class="row"> 
            <div class='col-sm-12'>
                <div id='firstTrainingActiveInactive'></div>
            </div>
        </div> 
        <div class="row" id="rw" style="display: none;">                                   
           <div class='col-sm-4'>
               {!! Form::label('status', 'Status:', ['class'=>'highlight required']) !!}
              <div class="form-group">
                  <div class=''>
                      {!! Form::select('status',[''=>'[Select]','1'=>'Active','0'=>'In Active'],Null,['id'=>'status','class'=>'form-control','autocomplete'=>'off']) !!}
                  </div>
              </div>
          </div>
       
           <div class='col-sm-4' > 
               {!! Form::label('', '', ['class'=>'highlight']) !!}
                <div class="form-group text-right permit" >                            	
                   {{ Form::button('Create', ['class' => 'btn btn-info', 'type' => 'button','id'=>'create']) }}
                   {{-- {{ Form::button('Delete', ['class' => 'btn btn-info', 'type' => 'button','id'=>'delete']) }} --}}
                </div>
           </div>
                
        </div>  
      
          
       </div>
      </div>
    </div>                              
  </div> 
 
 {!! Form::close() !!}             
</div>
<!--// form -->


</section>

</div>
</div>
<!-- Copyright -->
<script type="text/javascript">
$(document).ready(function () {
    getZoneDetails();
    $('select[name="zone"]').on('change', function () {
    change_subdivision(); 
 });
 $('select[name="subdivision"]').on('change', function () {
    getFirstTrainingAIDetails(); 
 });
  $("#create").click(function(){
        var formData = new FormData($("#trainingActiveInactive")[0]);
      
       
   
        $(".se-pre-con").fadeIn("slow");
        $.ajax({
        type: "post",
        url: "changeFirstTrainingStatus",
        data: formData,
        processData: false,
        contentType: false,
        dataType: 'json',
        success: function (data) { console.log(data)
           if(data.status==1)
           {
                $(".se-pre-con").fadeOut("slow"); 
               
               
                $.confirm({
                     title: 'Success!',
                     type: 'green',
                     icon: 'fa fa-check',
                     content: data.msg,
                     buttons: {
                         ok: function () {
                             //location.reload(true);
                             getFirstTrainingAIDetails();
                             $("#rw").hide();
                         }

                     }
                 });
           }
       },
        error: function (jqXHR, textStatus, errorThrown) {
            $(".se-pre-con").fadeOut("slow");
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-exclamation-triangle',
                  content: msg
              });
         }
     });
  });

  $("#delete").click(function(){
        var formData = new FormData($("#trainingActiveInactive")[0]);
        $(".se-pre-con").fadeIn("slow");
        $.ajax({
        type: "post",
        url: "deleteAssemblyZone",
        data: formData,
        processData: false,
        contentType: false,
        dataType: 'json',
        success: function (data) {
           if(data.status==1)
           {
                $(".se-pre-con").fadeOut("slow"); 
                var msg=data.options+" Assembly(s) deleted successfully for this zone</br>";
                if(data.options1>0){
                   msg+=data.options1+" Assembly(s) exist in personnela table";
                }
                $.confirm({
                     title: 'Success!',
                     type: 'green',
                     icon: 'fa fa-check',
                     content: msg,
                     buttons: {
                         ok: function () {
                             //location.reload(true);
                             getAssemblyDetails();
                             $("#rw").hide();
                         }

                     }
                 });
           }
       },
        error: function (jqXHR, textStatus, errorThrown) {
            $(".se-pre-con").fadeOut("slow");
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-exclamation-triangle',
                  content: msg
              });
         }
     });
  });
  
});
function getFirstTrainingAIDetails(){
    var zone = $("#zone").val();
    var subdivision = $("#subdivision").val();
    var token = $("input[name='_token']").val();
   // $(".se-pre-con").fadeIn("slow");
    $.ajax({
        type: 'POST',
        url: 'getfirstTrainingActiveInactive',
        data: {subdivision:subdivision,zone:zone, _token:token},
        dataType: 'json',
        success: function (response) {
       // $(".se-pre-con").fadeOut("slow");
        $("#firstTrainingActiveInactive").html("");

        $("#firstTrainingActiveInactive").append(response.options);
        
          $("#myCheckall").change(function(){ 
            var status = this.checked;
            if(status==true){
                $("#rw").show();
                scrollToElement($('#rw'));
            }else if(status==false){
                $("#rw").hide();
            }
            
            $('.submenu').each(function(){
                this.checked = status;     
            });
        });
        $('.submenu').change(function(){
            if($('.submenu:checked').length=='0'){
                $("#rw").hide();
            }else{
                $("#rw").show();
            }
            if(this.checked == false){
                $("#myCheckall")[0].checked = false; 
            }
            if ($('.submenu:checked').length == $('.submenu').length ){ 
                $("#myCheckall")[0].checked = true;
            }
        });
     
        },
        error: function (jqXHR, textStatus, errorThrown) {
           // $(".se-pre-con").fadeOut("slow");
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-exclamation-triangle',
                  content: msg
              });
         }
    });
}
 function getZoneDetails(){
    var forDist = $("#districtcd").val();
    var token = $("input[name='_token']").val();
    //$(".se-pre-con").fadeIn("slow");
    $.ajax({
       url: "getZoneDetails",
       method: 'POST',
       data: {forDist: forDist, _token: token},
       success: function (data) {//alert(data.options);
        // $(".se-pre-con").fadeOut("slow");
          $("select[name='zone'").html('');
          if(data.status==1)
          {
             $("select[name='zone'").append('<option value>[Select]</option>');
            $.each(data.options,function (k, v)
            {
                $("select[name='zone'").append('<option value='+k+'>'+v+'</option>');
            });
           // getMemberDetails();
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
           // $(".se-pre-con").fadeOut("slow");
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-exclamation-triangle',
                  content: msg
              });
         },

         complete:function(){
            getFirstTrainingAIDetails();
         }
   });
 }
 function change_subdivision(){
  var zone = $("#zone").val();
   var token = $("input[name='_token']").val();
  //$(".se-pre-con").fadeIn("slow");
   $.ajax({
       url: "getZonewiseSubdivisionData",
       method: 'POST',
       data: {zone: zone, _token: token},
       success: function (data) {//alert(data.options);
        //  $(".se-pre-con").fadeOut("slow");
          $("select[name='subdivision'").html('');
          
          if(data.status==1)
          {
            $("select[name='subdivision'").append('<option value>[Select]</option>');
            $.each(data.options,function (k, v)
            {
                $("select[name='subdivision'").append('<option value='+k+'>'+v+'</option>');
            });
            //$("select[name='subdivision'").html(data.options);
             // change_officeName();
            // getMemberDetails();
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
         //$(".se-pre-con").fadeOut("slow");
           var msg = "";
           if (jqXHR.status !== 422 && jqXHR.status !== 400) {
               msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
           } else {
               if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                   msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
               } else {
                   msg += "Error(s):<strong><ul>";
                   $.each(jqXHR.responseJSON, function (key, value) {
                       msg += "<li>" + value + "</li>";
                   });
                   msg += "</ul></strong>";
               }
           }
           $.alert({
               title: 'Error!!',
               type: 'red',
               icon: 'fa fa-exclamation-triangle',
               content: msg
           });
         },
        complete: function () {
            getFirstTrainingAIDetails();
        }
   });
}
 function scrollToElement(ele) {
    $('html, body').animate({
        scrollTop: ele.offset().top - 60
    }, 1000);
}
</script>
@stop
