
@extends('layouts.master')
@section('content')

 <!--// top-bar -->
 <div class="wrapper">
<!-- Sidebar Holder -->
@include('layouts.sidebar')

<!-- Page Content Holder -->
<div id="content">           
<section class="tables-section">
    <nav aria-label="breadcrumb" style="width:100%;">
        <div class="breadcrumb pagehead1">
                      <span class="headbrand"><i class="fa fa-desktop"></i>First Training Change
            <span class="scoop-mcaret1"></span>
           </span>&nbsp;&nbsp;&nbsp;         
              
               
                <div class="clearfix"></div>
        </div>
    </nav>

    <!-- form -->
<div class="outer-w3-agile">
  {!! Form::open(['url' => 'FirstTraingChange', 'name' => 'FirstTraingChange', 'class' =>'request-info clearfix form-horizontal', 'id' => 'FirstTraingChange', 'method' => 'post','role'=>'','files' => true]) !!}
   {!! Form::hidden('districtcd', session()->get('districtcd_ppds'),['id'=>'districtcd']) !!}
  <!-- OLD PWise -->
  <div class="panel-group" id="accordion5" style="">                                                              
    <div class="panel panel-default">
     <div class="panel-heading1">
     <h6 class="panel-title">
         <a data-toggle="#" data-parent="#accordion5" href="#collapsePV"> <span class="fa fa-minus"></span><span class="highlight"> OLD PERSONNEL </span></a>
     </h6>
     </div>
     <div id="collapsePV" class="panel-collapse collapse5">
      <div class="panel-body" style="min-height:75px;">                                   
        <div class="row"> 
         <div class='col-sm-4'>
               {!! Form::label('zone', 'Zone:', ['class'=>'highlight required']) !!}
              <div class="form-group">
                  <div class=''>
                      {!! Form::select('zone',[''=>'[Select]'],Null,['id'=>'zone','class'=>'form-control','autocomplete'=>'off']) !!}
                  </div>
              </div>
          </div>
         <div class='col-sm-4'>
              {!! Form::label('personnel_id', 'Personnel ID:', ['class'=>'highlight']) !!}
              <div class="form-group">
                  <div class=''>
                      {!! Form::text('personnel_id',null,['id'=>'personnel_id','class'=>'form-control','autocomplete'=>'off','maxlength'=>'11','minlength'=>'11']) !!}
                  </div>
              </div>
              <div style="display: none;">
                      {!! Form::text('newpersonnel_id',null,['id'=>'newpersonnel_id','class'=>'form-control','autocomplete'=>'off','maxlength'=>'11','minlength'=>'11','readonly'=>'false']) !!}
                  </div>
          </div>     
        </div>
        <div class="row"> 
            <div class='col-sm-12'> 
               <div id="old_personnel_details" >                            	
                   
               </div>
            </div>
         </div> 
         <div  id="rw" style="display: none;"> 
            <div class="row">
                                    <div class='col-sm-4'>
                                        {!! Form::label('subdivision', 'Sub Division:', ['class'=>'highlight required']) !!}
                                          <!--<span class="highlight">Sub Division</span> -->
                                        <div class="form-group">
                                            <div class=''>
                                                {!! Form::select('subdivision',[''=>'[Select]'],null,['id'=>'subdivision','class'=>'form-control','autocomplete'=>'off']) !!}
                                            </div>
                                        </div>
                                    </div>
                                
                                    {{-- <div class='col-sm-4'>
                                        {!! Form::label('phase', 'Phase:', ['class'=>'highlight required']) !!}
                                       <div class="form-group">
                                           <div class=''>
                                            
                                                <select id="phase" class="form-control" name="phase">
                                                            
                                                            <option value="">[Select]</option>
                                                            
                                            
                                                </select>
                                            </div>
                                       </div>
                                   </div> --}}
                            
             <div class='col-sm-4'>
              {!! Form::label('trainingtype', 'Training Type:', ['class'=>'highlight required']) !!}
              <div class="form-group">
                  <div class=''>
                       {!! Form::select('trainingtype',$training_type['desc'],null,['id'=>'trainingtype','class'=>'form-control','placeholder'=>'[Select]','autocomplete'=>'off']) !!}
                  </div>
              </div>
          </div>

          <div class='col-sm-4'>
            {!! Form::label('v_sub', 'Venue Subvenue:', ['class'=>'highlight required']) !!}
              <!--<span class="highlight">Sub Division</span> -->
            <div class="form-group">
                <div class=''>
                    {!! Form::select('v_sub',[''=>'[Select]'],null,['id'=>'v_sub','class'=>'form-control','autocomplete'=>'off']) !!}
                </div>
            </div>
        </div>
             
            </div>

            <div class="row">

               



                <div class='col-sm-4'>
                    {!! Form::label('orderno', 'Order Date & No:', ['class'=>'highlight required']) !!}
                    <div class="form-group">
                        <div class=''>
                            {!! Form::select('orderno',[''=>'[Select]'],Null,['id'=>'orderno','class'=>'form-control','autocomplete'=>'off']) !!}
                        </div>
                    </div>
                </div>

             <div class="col-sm-4" style="color:blue;font-size: 13px;">
                 {!! Form::label('', 'Training', ['class'=>'highlight required']) !!}
                 <div class="form-group">
                <input type="radio" name="trainging" value="1" checked>Give Both Training<br>
                <input type="radio" name="trainging" value="2">Give Single Training<br>
                 </div>
             </div>

             
            </div>
            <div class="row">
          <div class='col-sm-2'>  
                {!! Form::label('', '', ['class'=>'highlight']) !!}
                <div class="form-group text-right permit">                            	
                   {{ Form::button('Submit', ['class' => 'btn btn-info', 'type' => 'button','id'=>'save_change']) }}
                   {{ Form::button('Reset', ['class' => 'btn btn-success' ,'type' => 'reset','id'=>'reset']) }}
                </div>
           </div>
       
         </div> 
       </div>
      </div>
    </div>                              
  </div> 
  
    <!-- New PWise -->

                      
  

 {!! Form::close() !!}             
</div>
<!--// form -->


</section>

</div>
</div>
<!-- Copyright -->
<script type="text/javascript">

$(document).ready(function () {
    $("#show_po_add").click(function(){
       window.location.href = "preGroupCancellationExcelUpload"; 
  });
   getZoneDetails(); 
    $("#zone").change(function(){
        getOrderDetails();
    $("#personnel_id").val("");  
 });
   $("#reset").click(function () {
    location.reload(true);
  });
  char_num("#personnel_id");
  getZoneDetails(); 
  $('#personnel_id').on('change', function () {
    get_personnel_details();   
  });
  $("#trainingtype").change(function(){
      get_venue_and_subvenue();
  });
$('select[name="zone"]').on('change', function () {
            change_subdivision();
            getZonePhasedata();
           // other_subdivision();
        });
  $("#save_change").click(function(){
        
        var personnel_id = $("#personnel_id").val();
            if(personnel_id == ""){
                $.alert({
                   title: 'Error!!',
                   type: 'red',
                   icon: 'fa fa-exclamation-triangle',
                   content: 'OLD Personnel ID is required'
               });
               return false;
           }
           var zone = $("#zone").val();
        if(zone == ""){
               $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-exclamation-triangle',
                  content: 'Zone is required'
              });
              return false;
          }
          var subdivision = $("#subdivision").val();
            if(subdivision == ""){
                $.alert({
                   title: 'Error!!',
                   type: 'red',
                   icon: 'fa fa-exclamation-triangle',
                   content: 'Subdivision is required'
               });
               return false;
           }
           var trainingtype = $("#trainingtype").val();
            if(trainingtype == ""){
                $.alert({
                   title: 'Error!!',
                   type: 'red',
                   icon: 'fa fa-exclamation-triangle',
                   content: 'Training type is required'
               });
               return false;
           }
           var v_sub = $("#v_sub").val();
            if(v_sub == ""){
                $.alert({
                   title: 'Error!!',
                   type: 'red',
                   icon: 'fa fa-exclamation-triangle',
                   content: 'Venue is required'
               });
               return false;
           }
        var formData = new FormData($("#FirstTraingChange")[0]);
        $(".se-pre-con").fadeIn("slow");
        //var formData = {'zone': forzone,'forassembly': forassembly,'ofc_id': ofc_id,'booked': booked, 'gender': gender,'post_stat': post_stat, '_token': token};
        $.ajax({
            type: "post",
            url: "changeTrainingData",
            data: formData,
            processData: false,
            contentType: false,
            dataType: 'json',
            success: function (data) {
            $(".se-pre-con").fadeOut("slow"); 
            if(data.status==1)
            {
               // $('#new_personnel_details').html(data.options);
              //  $('#newpersonnel_id').val(data.perid);
                document.getElementById('save_change').disabled=true;
                $.confirm({
                        title: 'Success!',
                        type: 'green',
                        icon: 'fa fa-check',
                        content: "This Information Saved Successfully",
                        buttons: {
                            ok: function () {
                                /*$("#swapping_personnel")[0].reset();
                                $("#swapping").attr('disabled',true);
                                $('.form-control-feedback').css('display', 'none');
                                $('.has-success').removeClass('has-success');*/
                                // location.reload(true);
                                print_appt();
                            }

                        }
                    });
            }
            else if(data.status==0)
            {
               // $('#new_personnel_details').html(data.options);
               // $('#newpersonnel_id').val(data.perid);
                //$('#rw').hide();   
                document.getElementById('cancel').disabled=false;
                 $.confirm({
                        title: 'Error!!',
                        type: 'red',
                        icon: 'fa fa-exclamation-triangle',
                        content:"Please Check Your Details Are Correct",
                        buttons: {
                            ok: function () {
                                /*$("#swapping_personnel")[0].reset();
                                $("#swapping").attr('disabled',true);
                                $('.form-control-feedback').css('display', 'none');
                                $('.has-success').removeClass('has-success');*/
                               // location.reload(true);
                            }

                        }
                 });
            }
            
       },
        error: function (jqXHR, textStatus, errorThrown) {
            $(".se-pre-con").fadeOut("slow");
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-exclamation-triangle',
                  content: msg
              });
              document.getElementById('replace').disabled=false;
         }
     });
  });
 
});

function print_appt(){
 var districtcd = $("#districtcd").val();
     var subdivision = "";
     var blockMunicipality = "";
     var officeName = "";
     var PersonnelId = $("#personnel_id").val();
     var trainingtype = "";
     var from = "1";
     var to = "1";
     var datas = {'districtcd': districtcd,'subdivision': subdivision,'blockMunicipality': blockMunicipality,'officeName': officeName,'PersonnelId': PersonnelId,'trainingtype': trainingtype,from: from,to: to, '_token': $('input[name="_token"]').val()};
     redirectPost_newTab('{{url("getFirstAppointmentLetterPDF2019")}}', datas);
}



 function get_personnel_details(){
    var personnel_id = $("#personnel_id").val();
    var zone = $("#zone").val();
    if(zone == ""){
           $.alert({
              title: 'Error!!',
              type: 'red',
              icon: 'fa fa-exclamation-triangle',
              content: 'Zone is required'
          });
          return false;
      }
    var token = $("input[name='_token']").val();
    $(".se-pre-con").fadeIn("slow");
    $.ajax({
       url: "getOldPersonnelDetails_for_change",
       method: 'POST',
       data: {personnel_id: personnel_id, zone:zone, _token: token},
       success: function (json) {//alert(data.options);
          $(".se-pre-con").fadeOut("slow");
          //$("#memberDetails").html('');
          if(json.status==1)
          {
              $('#old_personnel_details').html(json.options);
               $('#rw').show();
               scrollToElement($('#rw'));
          }
          else if(json.status==0)
          {
              $('#old_personnel_details').html(json.options);
              $('#rw').hide();
          }
          document.getElementById('replace').disabled=true;
          document.getElementById('print').disabled=true;
       },
        error: function (jqXHR, textStatus, errorThrown) {
         $(".se-pre-con").fadeOut("slow");
           var msg = "";
           if (jqXHR.status !== 422 && jqXHR.status !== 400) {
               msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
           } else {
               if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                   msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
               } else {
                   msg += "Error(s):<strong><ul>";
                   $.each(jqXHR.responseJSON, function (key, value) {
                       msg += "<li>" + value + "</li>";
                   });
                   msg += "</ul></strong>";
               }
           }
           $.alert({
               title: 'Error!!',
               type: 'red',
               icon: 'fa fa-warning',
               content: msg
           });
        }
   });
 }


 function getZonePhasedata(){
  var zone = $("#zone").val();
//   $("#rw").hide();
   var token = $("input[name='_token']").val();
  // $(".se-pre-con").fadeIn("slow");
   $.ajax({
       url: "getZonePhasedata",
       method: 'POST',
       data: {zone: zone, _token: token},
       success: function (data) {//alert(data.options);
       //   $(".se-pre-con").fadeOut("slow");
          $("select[name='phase'").html('');
          
          if(data.status==1)
          {
            $("select[name='phase'").append('<option value>[Select]</option>');
            $.each(data.options,function (k, v)
            {
                $("select[name='phase'").append('<option value='+k+'>'+v+'</option>');
            });
            //$("select[name='subdivision'").html(data.options);
             // change_officeName();
             
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
       //  $(".se-pre-con").fadeOut("slow");
           var msg = "";
           if (jqXHR.status !== 422 && jqXHR.status !== 400) {
               msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
           } else {
               if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                   msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
               } else {
                   msg += "Error(s):<strong><ul>";
                   $.each(jqXHR.responseJSON, function (key, value) {
                       msg += "<li>" + value + "</li>";
                   });
                   msg += "</ul></strong>";
               }
           }
           $.alert({
               title: 'Error!!',
               type: 'red',
               icon: 'fa fa-warning',
               content: msg
           });
         }
        //  ,complete: function(){
        //   getZonePhaseWiseAssembly();
        //  }
   });
}

 function getOrderDetails(){
    var forDist = $("#districtcd").val();
    var token = $("input[name='_token']").val();
    //$(".se-pre-con").fadeIn("slow");
    $.ajax({
       url: "getOrderDetails",
       method: 'POST',
       data: {forDist: forDist, _token: token},
       success: function (data) {//alert(data.options);
        // $(".se-pre-con").fadeOut("slow");
          $("select[name='orderno'").html('');
          if(data.status==1)
          {
              $("select[name='orderno'").html(data.options);
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
            $(".se-pre-con").fadeOut("slow");
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-warning',
                  content: msg
              });
         }
   });
 }



  function getZoneDetails(){
    var forDist = $("#districtcd").val();
    var token = $("input[name='_token']").val();
    $(".se-pre-con").fadeIn("slow");
    $.ajax({
       url: "getZoneDetails",
       method: 'POST',
       data: {forDist: forDist, _token: token},
       success: function (data) {//alert(data.options);
         $(".se-pre-con").fadeOut("slow");
          $("select[name='zone'").html('');
          if(data.status==1)
          {
             $("select[name='zone'").append('<option value>[Select]</option>');
            $.each(data.options,function (k, v)
            {
                $("select[name='zone'").append('<option value='+k+'>'+v+'</option>');
            });
           // getMemberDetails();
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
            $(".se-pre-con").fadeOut("slow");
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-warning',
                  content: msg
              });
         }
   });
 }
 function scrollToElement(ele) {
    $('html, body').animate({
        scrollTop: ele.offset().top - 60
    }, 1000);
}
function change_subdivision(){
  var forDist = $("#districtcd").val();
  var zone = $("#zone").val();
   var token = $("input[name='_token']").val();
   $(".se-pre-con").fadeIn("slow");
   $.ajax({
    //    url: "getZonewiseSubBlockDataQuery",
        url: "getZonewiseSubdivisionall_Data",
       method: 'POST',
       data: {forDist: forDist, zone: zone, _token: token},
       success: function (data) {//alert(data.options);
          $(".se-pre-con").fadeOut("slow");
          $("select[name='subdivision'").html('');
          
          if(data.status==1)
          {
            $("select[name='subdivision'").append('<option value>[Select]</option>');
            $.each(data.options,function (k, v)
            {
                $("select[name='subdivision'").append('<option value='+k+'>'+v+'</option>');
            });
            //$("select[name='subdivision'").html(data.options);
             // change_officeName();
           //  getMemberDetails();
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
         $(".se-pre-con").fadeOut("slow");
           var msg = "";
           if (jqXHR.status !== 422 && jqXHR.status !== 400) {
               msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
           } else {
               if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                   msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
               } else {
                   msg += "Error(s):<strong><ul>";
                   $.each(jqXHR.responseJSON, function (key, value) {
                       msg += "<li>" + value + "</li>";
                   });
                   msg += "</ul></strong>";
               }
           }
           $.alert({
               title: 'Error!!',
               type: 'red',
               icon: 'fa fa-warning',
               content: msg
           });
         }
   });
}
function get_venue_and_subvenue(){
var subdivision = $("#subdivision").val();
var trainingtype = $("#trainingtype").val();
var post_stat=$("#post_stat").val();
    var token = $("input[name='_token']").val();
    $(".se-pre-con").fadeIn("slow");
    $.ajax({
       url: "get_venue_with_not_fill",
       method: 'POST',
       data: {subdivision:subdivision,post_stat:post_stat,trainingtype:trainingtype, _token: token},
       success: function (data) {//alert(data.options);
         $(".se-pre-con").fadeOut("slow");
          $("select[name='v_sub'").html('');
          if(data.status==1)
          {
             $("select[name='v_sub'").append('<option value>[Select]</option>');
            $.each(data.options,function (k, v)
            {
                $("select[name='v_sub'").append('<option value='+v.schedule_code+'>'+v.venuename+'('+v.subvenue+'['+v.training_dt+' '+v.training_time+'])</option>');
            });
           // getMemberDetails();
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
            $(".se-pre-con").fadeOut("slow");
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-warning',
                  content: msg
              });
         }
   });
}
</script>
@stop
