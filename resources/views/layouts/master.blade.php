﻿<!DOCTYPE html>
<html lang="en">
    <head>
   
        <?php
        header("Content-Security-Policy: default-src 'self' 'unsafe-inline' 'unsafe-eval'; img-src *; script-src 'self' 'unsafe-inline' 'unsafe-eval';  frame-src 'self' 'unsafe-inline' 'unsafe-eval'; object-src 'self' 'unsafe-inline' 'unsafe-eval'; style-src 'self' 'unsafe-inline'; connect-src 'self' 'unsafe-inline' 'unsafe-eval'; font-src 'self' 'unsafe-inline' 'unsafe-eval';");
        //header("X-XSS-Protection 1; mode=block");
        header("X-Content-Type-Options: nosniff");
        header("X-Frame-Options: SAMEORIGIN");
        header("Set-Cookie: name=value; httpOnly");
        header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");

//        $now = time(); // checking the time now when home page starts
//        //use Session;
//	if(session()->has('expire') != 1)
//	{
//		if($now > session()->get('expire'))
//		{
//			//Session::flush();
//			redirect('/login');
//			//exit;
//		}
//		else{
//			session(['expire' =>  $now + (1 * 60)]) ;
//                }
//	}
//	else{
//		session(['expire' =>  $now + (1 * 60)]) ;
//        }
        use App\Http\Controllers\LoginController;
        ?>

<?php if (session()->has('code_ppds') != 1) { ?>
            <script type="text/javascript">
                window.location = "login";
            </script>   
        <?php } else { ?>
            <?php
            $now = time(); // checking the time now when home page starts  
            if(session()->has('expire')==1){
                if($now > session()->get('expire')){
                   Session::flush();
                }else{
                   session(['expire' => $now + (60 * 60)]); 
                }
            }else{
                session(['expire' => $now + (60 * 60)]);
            }
            if (session()->has('code_ppds') != 1) { 
               echo "<body>\n";
			echo "Your session has expired ! <a href='logout'>Login Here</a>\n";
			echo "</body>\n";
			echo "</html>";
			exit;?>
           <?php }
                                       
//            echo $now." /";
//            echo session()->get('expire');
//             die();
            //die();
            $permission_ppds_all = \Session::get('permission_ppds_all');
            //echo $permission_ppds_all;die;

            $url_all = url()->current();
            $url = explode('/', $url_all);
            //echo $url[0];
            //echo $url[4];die;
            $url_count = sizeof($url);
            
            $get_page = '';
            if ($url_count > 5) {
                $curr_url = $url[5];
                //echo $curr_url;die;
                
                if($curr_url!='change_password' && $curr_url!='search_polling_personnel' && $curr_url!='queryreports_first_rand' && $curr_url!='index' && $curr_url!='dashboard' && $curr_url!='edit_personnel_new' && $curr_url!='sendsms' && $curr_url!='assembly_subdivision_wise_priority_list'){
                    $get_page_permission = LoginController::get_page_permission($curr_url, $permission_ppds_all);

                    $get_page_status = LoginController::get_page_status($url[5]);
                    $get_page = $get_page_status[0]['menu_menu_cd']['menu'] . ' / ' . $get_page_status[0]['submenu'];

                    //echo $get_page_permission;die;
                    if ($get_page_permission == 0) {
                        ?>
                        <script type="text/javascript">
                            window.location = "permission_not_found";
                        </script>
                        <?php
                    }
                }
            }
            ?>    
            <title>PPMS :: 2021</title>
            <!-- Meta Tags -->
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <meta charset="utf-8">
            <meta name="keywords" content="" />
            <script>
                addEventListener("load", function () {
                    setTimeout(hideURLbar, 0);
                }, false);

                function hideURLbar() {
                    window.scrollTo(0, 1);
                }
            </script>
            <!-- //Meta Tags -->

            <!-- Style-sheets -->
            <!-- Bootstrap Css -->
            <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
            <link rel="stylesheet" href="{{ asset('css/style4.css') }}">

            <!--<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" media="all" />-->
            <link href="css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" media="all" />
            <link href="css/bootstrapValidator.css" rel="stylesheet" type="text/css" media="all" />
            <!-- Bootstrap Css -->
            <!-- Bars Css -->
            <link rel="stylesheet" href="{{ asset('css/bar.css') }}">    
            <link rel="stylesheet" href="{{ asset('css/morris.css') }}">
            <!--// Bars Css -->
            <!-- Calender Css -->
            <link rel="stylesheet" type="text/css" href="{{ asset('css/pignose.calender.css') }}" />
            <!--// Calender Css -->
            <!-- Common Css -->
            <link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css" media="all" />
            <!--// Common Css -->
            <!-- Nav Css -->

            <!--// Nav Css -->
            <!-- Fontawesome Css -->
            <link href="{{ asset('css/fontawesome-all.css') }}" rel="stylesheet">

            <!--// Fontawesome Css -->
            <!--// Style-sheets -->

            <!--web-fonts-->    
<!--            <link href="fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">-->
            <!-- Bootstrap Validator -->

            <!-- Jquery Confirm -->
            <link href="css/jquery-confirm.min.css" rel="stylesheet" type="text/css" media="all" />
            <!--//web-fonts-->
            <script src="{{ asset('js/jquery-2.2.3.min.js') }}"></script>
            <script src="{{ asset('js/bootstrap-treeview.js') }}"></script>
            <script src="{{ asset('js/bootstrap.min.js') }}"></script>
            <script src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
            <script src="{{ asset('js/dataTables.bootstrap4.min.js') }}"></script>
            <!-- Required common Js -->
            <script src="{{ asset('js/moment.min.js') }}"></script>
            <script src="js/bootstrapValidator.js"></script>
            <script src="js/jquery-confirm.min.js"></script>
            <!-- //Required common Js -->    
            <!-- loading-gif Js -->
            <script src="{{ asset('js/modernizr.js') }}"></script> 
            <script src="{{ asset('js/raphael-min.js') }}"></script>
<!--            <script src="{{ asset('js/morris.js') }}"></script>-->
<!--            <script src="{{ asset('js/SimpleChart.js') }}"></script>-->
            <script src="{{ asset('js/rumcaJS.js') }}"></script>
            <script src="{{ asset('js/example.js') }}"></script>
            <!--// Bar-chart -->
            <!-- Calender -->
            <script src="{{ asset('js/highcharts.js') }}"></script>
            <script src="{{ asset('js/highcharts-3d.js') }}"></script>
            <script src="{{ asset('js/exporting.js') }}"></script>
            <script src="{{ asset('js/export-data.js') }}"></script>
            <script src="{{ asset('js/pignose.calender.js') }}"></script>
<!--            <script src='js/amcharts.js'></script>-->
            <script src="{{ asset('js/script.js') }}"></script>
            <script src="{{ asset('js/jquery-numeric.js') }}"></script>


            <link href="{{ asset('buttons/1.5.2/css/buttons.dataTables.min.css') }}" rel="stylesheet">

            <script type="text/javascript" src="{{ asset('buttons/1.5.2/js/dataTables.buttons.min.js') }}"></script>
            <script type="text/javascript" src="{{ asset('buttons/1.5.2/js/buttons.flash.min.js') }}"></script>
            <script type="text/javascript" src="{{ asset('buttons/1.5.2/js/jszip.min.js') }}"></script>
            <script type="text/javascript" src="{{ asset('buttons/1.5.2/js/pdfmake.min.js')}}"></script>
            <script type="text/javascript" src="{{ asset('buttons/1.5.2/js/vfs_fonts.js')}}"></script>
            <script type="text/javascript" src="{{ asset('buttons/1.5.2/js/buttons.html5.min.js') }}"></script>
            <script type="text/javascript" src="{{ asset('buttons/1.5.2/js/buttons.print.min.js') }}"></script>
            <script type="text/javascript" src="{{ asset('buttons/1.5.2/js/buttons.colVis.min.js') }}"></script>
            <link rel="stylesheet" href="{{ asset('css/bootstrap-datepicker.css') }}" />
            <script src="{{ asset('js/bootstrap-datepicker.js') }}"></script>
            
            <link rel="stylesheet" href="{{ asset('css/select2.min.css') }}" />
            <script src="{{ asset('js/select2.min.js') }}"></script>
        </head>

        <body>
            <div class="se-pre-con"></div> 
            <div class="se-pre-con1" style="display: none"></div> 
            <!-- top-bar -->
            <div class="top_header">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-5">
                            <ul class="phone-mail">
                                <li style="margin-left:0px;"><div class='date-part'></div><div class='time-part' style="margin-left:10px"></div></li>
                            </ul>
                        </div>
                        <div class="col-md-7">                	
                            <div class="top-bar-menu">
                                <ul id="menu_top_bar">
                                    <li style="border-left:none;">Welcome : <a href="#" class="active"><?php echo session()->get('user_id_ppds'); ?></a>&nbsp;[<?php $cat_array = config('constants.USER_CATEGORY');
                                         echo $cat_array[session()->get('category_ppds')]; ?>]</li>        
                                    <li><a href="change_password">Change Password</a></li>
                                    <li><a href="logout"><i class="fa fa-sign-out-alt"></i> Logout</a></li>  

                                </ul>                        
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="pageheader">
                <div class="container-fluid clearfix">
                    <div class="row">
                        <div class="col-xl-9 col-lg-9  col-md-9 text-left">
                             <div class="col-md-1"> 
                                <img src="images/Emblem_of_India.png" class="emblem" />
                            </div>
                            <h3 style="font-size: 24px;" class="emblem">General Election To The Bidhan Sabha - 2021 <br> Polling Personnel Management System <br>(PPMS)</h3> 
                            {{-- <div class="col-md-1">
                                <img src="images/Emblem_of_India.png" class="emblem" />
                            </div>
                            <h3 style="font-size: 24px;" class="emblem">General Election To The Bidhan Sabha - 2021 <br> Polling Personnel Management System <br>(PPMS)</h3> --}}
                        </div>
    <!--                    <div class="col-xl-6 col-lg-6 col-md-6 text-center  mt-3 mb-3"><h3>General Election To The Lok Sabha-2019</h3><h5><i>Polling Personnel Management System</i></h5><h5><i>(PPMS)</i></h5></div>-->
                        <div class="col-xl-3 col-lg-3  col-md-3 text-right"><img src="images/election-commission-of-india-seeklogo.png" class="img-fluid wbimg" /></div>
                    </div>
                </div>
            </div>
            <div class="container-fluid clearfix">
                <div class="row">
                    <nav class="navbar navbar-default" style="width:100%; background:#063b50;">
                        <div id="cssmenu">
                            <div id="head-mobile"></div>
                            <div class="button"></div>
                            <ul class="nav navbar-nav navbar-right">
<!--                                <li class=""><a href="{{config('constants.PROJECT_ROOT')}}"> <i class="fa fa-tachometer-alt"></i> Dashboard</a></li>-->
                                <?php
                                if (session()->has('code_ppds') == 1) {
                                    $permission = \Session::get('permission_ppds');
                                    //print_r($permission);die;
                                    $user_permission = $permission[0]->user_permission_code;
                                    foreach ($user_permission as $u_permission) {
                                        if ($u_permission->menu->menu_cd == 1) {
                                            $c_class = '<i class="fa fa-user"></i>';
                                        } else if ($u_permission->menu->menu_cd == 2) {
                                            $c_class = '<i class="fa fa-table"></i>';
                                        } else if ($u_permission->menu->menu_cd == 3) {
                                            $c_class = '<i class="fa fa-tasks"></i>';
                                        } else if ($u_permission->menu->menu_cd == 4) {
                                            $c_class = '<i class="fa fa-globe"></i>';
                                        } else if ($u_permission->menu->menu_cd == 5) {
                                            $c_class = '<i class="fa fa-table"></i>';
                                        } else if ($u_permission->menu->menu_cd == 6) {
                                            $c_class = '<i class="fa fa-database"></i>';
                                        } else if ($u_permission->menu->menu_cd == 7) {
                                            $c_class = '<i class="fa fa-print"></i>';
                                        }else if ($u_permission->menu->menu_cd == 8) {
                                            $c_class = '<i class="fa fa-mobile-alt"></i>';
                                        }
                                        else if ($u_permission->menu->menu_cd == 10) {
                                            // echo "fdvbdfh";
                                            $c_class = '<i class="fa fa-check-circle"></i>';
                                        }
                                        // echo "<pre>";
                                        
                                        ?>

                                        <li><a <?php  if ($u_permission->menu->link == '') { ?>href="#"<?php } else { ?>href="<?php echo $u_permission->menu->link; ?>" <?php } ?>><?php echo $c_class . " " . $u_permission->menu->menu; ?></a>
                                            <?php
                                            $get_sub_menu = $u_permission->menu->menu_wise_submenu;
                                            ?>
                                            <ul class="firstch">
                                                <?php foreach ($get_sub_menu as $submenu) {
                                                   //  print_r( $submenu);die;
                                                    ?>
                                                    @if($submenu->submenu_cd!=28)
                                                    <li><a <?php if ($submenu->link == '') { ?>href="#"<?php } else { ?>href="<?php echo $submenu->link; ?>" <?php } ?>><?php echo $submenu->submenu; ?></a></li>
                                                   
                                                    @endif
                                                    <?php } ?>
                                                    
                                            </ul>
                                        </li>
                                        <?php
                                    }
                                }
                                ?>


                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
            <!--         <div class="container-fluid clearfix">
                        <div class="row">
                            
                            <nav aria-label="breadcrumb" style="width:100%;">
                                <ol class="breadcrumb" style="width:100%; margin-bottom:0px; background-color: #fffffff; box-shadow: inset 0 -3px 8px rgba(0,0,0,0.1);border-radius:0px;padding-top: 5px;padding-bottom: 5px;font-size: 14px;">
                                  <div class="col-md-9">   
                                      <li class="breadcrumb-item">
                                        <a href="#"><span class="highlight">You are here :: 
            <?php
            if ($get_page != '') {
                echo $get_page;
            } else {
                echo "Home";
            }
            ?>
                                                </span>
                                        </a>
                                    </li>
                                  </div>
                                  <div class="col-md-3">
                                      <div style=" display: inline-block; float:right;">
            <?php
            if (session()->has('code_ppds') == 1) {
                if (session()->has('districtcd_ppds') == 1) {
                    $cat_array = config('constants.USER_CATEGORY');
                    echo "<span class='highlight'>[" . $cat_array[\Session::get('category_ppds')] . "]";
                }
            }
            ?>
                                          
                                      </div>
                                 </div>
                                </ol>
                               
                            </nav>
                            
                        </div>
                    </div>-->
            @yield('content')
            <!-- Copyright -->
            <div class="copyright-w3layouts py-xl-3 py-2 mt-xl-5 mt-4 text-center">
            <div class="row" style="margin-left:0px !important;margin-right: 0px !important;">
                <div class="col-xl-6 col-lg-6 col-md-6 text-left">
                    <p style="padding-left: 4px;text-align: left !important;letter-spacing: 1px !important;">© 2018 Election . All Rights Reserved | Design by
                        <b style="color:#FFF;">National Informatics Center </b>
                    </p>
                </div>
                <div class="col-xl-6 col-lg-6  col-md-6 text-left">
                    <p style="padding-right: 4px;text-align: right !important;letter-spacing: 1px !important;">
                        Site best viewed at 1024 x 768 resolution in I.E 10 +, Mozilla 35 +, Google Chrome 35 +
                    </p>
                </div>  
            </div>
        </div>
            <!--// Copyright -->

            <script>
                //paste this code under head tag or in a seperate js file.
                // Wait for window load
                $(window).load(function () {
                    // Animate loader off screen
                    $(".se-pre-con").fadeOut("slow");
                });
            </script>
            <!--// loading-gif Js -->
            <!-- Sidebar-nav Js -->
            <script>
                $(document).ready(function () {
                    $('#sidebarCollapse').on('click', function () {
                        $('#sidebar').toggleClass('active');
                    });
                });
            </script>
            <!--// Sidebar-nav Js -->
            <!-- Graph -->

            <script>
//                var graphdata4 = {
//                    linecolor: "Random",
//                    title: "Thursday",
//                    values: [{
//                            X: "6",
//                            Y: 300.00
//                        },
//                        {
//                            X: "7",
//                            Y: 101.98
//                        },
//                        {
//                            X: "8",
//                            Y: 140.00
//                        },
//                        {
//                            X: "9",
//                            Y: 340.00
//                        },
//                        {
//                            X: "10",
//                            Y: 470.25
//                        },
//                        {
//                            X: "11",
//                            Y: 180.56
//                        },
//                        {
//                            X: "12",
//                            Y: 680.57
//                        },
//                        {
//                            X: "13",
//                            Y: 740.00
//                        },
//                        {
//                            X: "14",
//                            Y: 800.89
//                        },
//                        {
//                            X: "15",
//                            Y: 420.57
//                        },
//                        {
//                            X: "16",
//                            Y: 980.24
//                        },
//                        {
//                            X: "17",
//                            Y: 1080.00
//                        },
//                        {
//                            X: "18",
//                            Y: 140.24
//                        },
//                        {
//                            X: "19",
//                            Y: 140.58
//                        },
//                        {
//                            X: "20",
//                            Y: 110.54
//                        },
//                        {
//                            X: "21",
//                            Y: 480.00
//                        },
//                        {
//                            X: "22",
//                            Y: 580.00
//                        },
//                        {
//                            X: "23",
//                            Y: 340.89
//                        },
//                        {
//                            X: "0",
//                            Y: 100.26
//                        },
//                        {
//                            X: "1",
//                            Y: 1480.89
//                        },
//                        {
//                            X: "2",
//                            Y: 1380.87
//                        },
//                        {
//                            X: "3",
//                            Y: 1640.00
//                        },
//                        {
//                            X: "4",
//                            Y: 1700.00
//                        }
//                    ]
//                };
//                $(function () {
//                    $("#Hybridgraph").SimpleChart({
//                        ChartType: "Hybrid",
//                        toolwidth: "50",
//                        toolheight: "25",
//                        axiscolor: "#E6E6E6",
//                        textcolor: "#6E6E6E",
//                        showlegends: false,
//                        data: [graphdata4],
//                        legendsize: "140",
//                        legendposition: 'bottom',
//                        xaxislabel: 'Hours',
//                        title: 'Weekly Profit',
//                        yaxislabel: 'Profit in $'
//                    });
//                });
            </script>
            <!--// Graph -->
            <!-- Bar-chart -->

            <script>
                //<![CDATA[
                $(function () {
                    $('.calender').pignoseCalender({
                        select: function (date, obj) {
                            obj.calender.parent().next().show().text('You selected ' +
                                    (date[0] === null ? 'null' : date[0].format('YYYY-MM-DD')) +
                                    '.');
                        }
                    });

                    $('.multi-select-calender').pignoseCalender({
                        multiple: true,
                        select: function (date, obj) {
                            obj.calender.parent().next().show().text('You selected ' +
                                    (date[0] === null ? 'null' : date[0].format('YYYY-MM-DD')) +
                                    '~' +
                                    (date[1] === null ? 'null' : date[1].format('YYYY-MM-DD')) +
                                    '.');
                        }
                    });
                });
                //]]>
            </script>
            <!--// Calender -->
            <!-- pie-chart -->

            <script>
//                var chart;
//                var legend;
//
//                var chartData = [{
//                        country: "Lithuania",
//                        value: 260
//                    },
//                    {
//                        country: "Ireland",
//                        value: 201
//                    },
//                    {
//                        country: "Germany",
//                        value: 65
//                    },
//                    {
//                        country: "Australia",
//                        value: 39
//                    },
//                    {
//                        country: "UK",
//                        value: 19
//                    },
//                    {
//                        country: "Latvia",
//                        value: 10
//                    }
//                ];

//                AmCharts.ready(function () {
//                    // PIE CHART
//                    chart = new AmCharts.AmPieChart();
//                    chart.dataProvider = chartData;
//                    chart.titleField = "country";
//                    chart.valueField = "value";
//                    chart.outlineColor = "";
//                    chart.outlineAlpha = 0.8;
//                    chart.outlineThickness = 2;
//                    // this makes the chart 3D
//                    chart.depth3D = 20;
//                    chart.angle = 30;
//
//                    // WRITE
//                    chart.write("chartdiv");
//                });
            </script>
            <!--// pie-chart -->
            <script>
//                Morris.Donut({
//                    element: 'graph4',
//                    data: [
//                        {value: 70, label: 'foo', formatted: 'at least 70%'},
//                        {value: 15, label: 'bar', formatted: 'approx. 15%'},
//                        {value: 10, label: 'baz', formatted: 'approx. 10%'},
//                        {value: 5, label: 'A really really long label', formatted: 'at most 5%'}
//                    ],
//                    formatter: function (x, data) {
//                        return data.formatted;
//                    }
//                });
            </script>   
            <!-- Js for bootstrap working-->

            <!-- //Js for bootstrap working -->

            <script type="text/javascript">
    //            var redirectPost = function (url, data = null, method = 'post') {
    //                var form = document.createElement('form');
    //                form.method = method;
    //                form.action = url;
    //                for (var name in data) {
    //                    var input = document.createElement('input');
    //                    input.type = 'hidden';
    //                    input.name = name;
    //                    input.value = data[name];
    //                    form.appendChild(input);
    //                }
    //                $('body').append(form);
    //                form.submit();
    //            };
                function redirectPost(url, data1) {
                    var $form = $("<form />");
                    $form.attr("action", url);
                    $form.attr("method", "post");
    //         $form.attr("target", "_blank");
                    for (var data in data1)
                        $form.append('<input type="hidden" name="' + data + '" value="' + data1[data] + '" />');
                    $("body").append($form);
                    $form.submit();
                }

function redirectPost_newTab(url, data1) {
                    var $form = $("<form />");
                    $form.attr("action", url);
                    $form.attr("method", "post");
             $form.attr("target", "_blank");
                    for (var data in data1)
                        $form.append('<input type="hidden" name="' + data + '" value="' + data1[data] + '" />');
                    $("body").append($form);
                    $form.submit();
                }

                $(document).ready(function () {
                    //  $.widget.bridge('uibutton', $.ui.button);
                    var interval = setInterval(function () {
                        var momentNow = moment();
                        $('.date-part').html(momentNow.format('YYYY MMMM DD') + ' '
                                + momentNow.format('dddd')
                                .substring(0, 3).toUpperCase());
                        $('.time-part').html(momentNow.format('hh:mm:ss A'));
                    }, 100);
                });
            </script>

        </body>
<?php } ?>
</html>
