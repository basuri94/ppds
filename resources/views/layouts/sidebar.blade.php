<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
use App\Http\Controllers\DashboardController;
if (session()->has('code_ppds') == 1) {
$dist = \Session::get('districtcd_ppds');
$cat = \Session::get('category_ppds');


if($dist>0){
$data_subdivision_array=DashboardController::Get_subdivision_all();
$subdivcode=$data_subdivision_array[0];
$subdivcodeid=$data_subdivision_array[1];

$total_office_ar=DashboardController::Get_total_office_all();
$total_pp=DashboardController::Get_total_pollingpp_all();

$distName = DashboardController::Get_distName();
$zoneName = DashboardController::Get_zoneName();
$subDivName = DashboardController::Get_subdivName();
$total_office = $total_office_ar[0]->cnt;
$total_polling_person = $total_pp[0]->cnt;

if($subdivcode!=""){
  $total_subdivision = count($data_subdivision_array[0]);

}else{
  $total_subdivision = 0;  
}

//echo($total_office_ar[0]->cnt);
}else{
  $total_office = 0; 
  $total_polling_person = 0;
  $total_subdivision = 0;
  $distName = "";
  $zoneName = "";
  $subDivName = "";
}
?>
{!! Form::open(['url' => 'sidebar', 'name' => 'sidebar', 'class' =>'', 'id' => 'sidebar', 'method' => 'post','role'=>'']) !!}
{!! Form::close() !!} 
<!-- <div class="widget widget_categories">
        	<ul>
                <li class="cat-item"><a href="#null">Report1</a></li>
                <li class="cat-item"><a href="#null">Report2</a></li>
                <li class="cat-item"><a href="#null">Report3</a></li>
                <li class="cat-item"><a href="#null">Report4</a></li>
                <li class="cat-item"><a href="#null">Report1</a></li>
                <li class="cat-item"><a href="#null">Report2</a></li>
                <li class="cat-item"><a href="#null">Report3</a></li>
                <li class="cat-item"><a href="#null">Report4</a></li>
                <li class="cat-item"><a href="#null">Report1</a></li>
                <li class="cat-item"><a href="#null">Report2</a></li>
                <li class="cat-item"><a href="#null">Report3</a></li>
                <li class="cat-item"><a href="#null">Report4</a></li>
            </ul>                
        </div>-->

<div class="nav-side-menu">
    <span class="brand0"><a href="{{config('constants.DASHBOARD_ROOT')}}"> <i class="fa fa-tachometer-alt"></i> DASHBOARD</a>
       <span class="scoop-mcaret"></span>
    </span>
    <span class="brand">District: <?php echo $distName;?>
       <span class="scoop-mcaret"></span>
    </span>
    <span class="brand1">
        <?php if($cat==6){ echo "Zone: ".$zoneName;}else{ echo "Subdivision: ".$subDivName;}?> 
       <span class="scoop-mcaret"></span>
    </span>
    <span class="brand2">Total Office: <?php echo $total_office; ?>
       <span class="scoop-mcaret"></span>
    </span>
    <span class="brand3">Total Polling Personnel: <?php echo $total_polling_person; ?>
       <span class="scoop-mcaret"></span>
    </span>
    <span class="brand6"><a href="search_polling_personnel"> <i class="fa fa-search"></i> Search Polling Personnel</a>
       <span class="scoop-mcaret"></span>
    </span>
    <span class="brand5"><a href="queryreports_first_rand"> <i class="fa fa-adjust"></i> Polling Personnel Query</a>
       <span class="scoop-mcaret"></span>
    </span>
<!--    <span class="brand6"><a href="pre_exemption_export"> <i class="fa fa-download"></i> Pre Exemption Export</a>
       <span class="scoop-mcaret"></span>
    </span>-->
    <i class="fa fa-bars fa-2x toggle-btn" data-toggle="collapse" data-target="#menu-content"></i>
  
        <div class="menu-list">
  
            <ul id="menu-content" class="menu-content collapse out">
<!--                <li>
                  <a href="#">
                   Downloads
                  </a>
                </li>-->

<!--                <li data-toggle="collapse" data-target="#products" class="collapsed active">
                  <a href="#"><i class="fa fa-globe fa-lg"></i> Personnel Reports<span class="arrow"></span></a>
                </li>
                <ul class="sub-menu collapse show" id="products">
                    <li><a href="#">CSS3 Animation<i class="fa fa-download"></i></a></li>
                    <li><a href="#">Bootstrap Model<i class="fa fa-download"></i></a></li>
                    <li><a href="#">Bootstrap Model<i class="fa fa-download"></i></a></li>
                    <li><a href="#">Bootstrap Model<i class="fa fa-download"></i></a></li>
            
                </ul>-->


                <li data-toggle="collapse" data-target="#service" class="collapsed active">
                  <a href="#"><i class="fa fa-globe fa-lg"></i> Personnel Exports <span class="arrow"></span></a>
                </li>  
                <ul class="sub-menu collapse" id="service">
                   <?php 
                    if($total_subdivision > 0){
                        for($j=0;$j<count($data_subdivision_array[0]);$j++){?> 
                    <li class="subd" id="<?php echo $subdivcodeid[$j];?>"><a href="javascript:"><?php echo $subdivcode[$j];?><i class="fa fa-download"></i></a></li>
                   <?php }
                    }?>
                    
                
                </ul>
               <!-- <li data-toggle="collapse" data-target="#personnel_list" class="collapsed active">
                  <a href="#"><i class="fa fa-globe fa-lg"></i> Personnel Details <span class="arrow"></span></a>
                </li>  
                <ul class="sub-menu collapse" id="personnel_list">
                <li><a href="personnel_add">Personnel Add <i class="fa fa-list"></i></a></li>
                <li><a href="getofficebydistrict">Personnel List <i class="fa fa-list"></i></a></li>
                    
                </ul> -->
<!--                <li data-toggle="collapse" data-target="#service1" class="collapsed active">
                  <a href="#"><i class="fa fa-globe fa-lg"></i> First Rand Reports <span class="arrow"></span></a>
                </li>  
                <ul class="sub-menu collapse" id="service1">
                    <li><a href="#">CSS3 Animation <i class="fa fa-download"></i></a></li>
                    <li><a href="#">Bootstrap Model<i class="fa fa-download"></i></a></li>
                    <li><a href="#">Bootstrap Model<i class="fa fa-download"></i></a></li>
                    <li><a href="#">Bootstrap Model <i class="fa fa-download"></i></a></li>
                </ul>
                <li data-toggle="collapse" data-target="#service2" class="collapsed active">
                  <a href="#"><i class="fa fa-globe fa-lg"></i> Second Rand Reports(Party) <span class="arrow"></span></a>
                </li>  
                <ul class="sub-menu collapse" id="service2">
                    <li><a href="#">CSS3 Animation <i class="fa fa-download"></i></a></li>
                    <li><a href="#">Bootstrap Model <i class="fa fa-download"></i></a></li>
                    <li><a href="#">Bootstrap Model <i class="fa fa-download"></i></a></li>
                    <li><a href="#">Bootstrap Model <i class="fa fa-download"></i></a></li>
                </ul>
                <li data-toggle="collapse" data-target="#service3" class="collapsed active">
                  <a href="#"><i class="fa fa-globe fa-lg"></i> Second Rand Reports(Reserve) <span class="arrow"></span></a>
                </li>  
                <ul class="sub-menu collapse" id="service3">
                  <li><a href="#">CSS3 Animation <i class="fa fa-download"></i></a></li>
                    <li><a href="#">Bootstrap Model <i class="fa fa-download"></i></a></li>
                    <li><a href="#">Bootstrap Model <i class="fa fa-download"></i></a></li>
                    <li><a href="#">Bootstrap Model <i class="fa fa-download"></i></a></li>
                </ul>-->

             <!--   <li data-toggle="collapse" data-target="#new" class="collapsed">
                  <a href="#"><i class="fa fa-car fa-lg"></i> New <span class="arrow"></span></a>
                </li>
                <ul class="sub-menu collapse" id="new">
                  <li>New New 1</li>
                  <li>New New 2</li>
                  <li>New New 3</li>
                </ul>-->


<!--                 <li class="active">
                  <a href="#">
                  <i class="fa fa-user fa-lg"></i> Profile
                  </a>
                  </li>-->
<!--
                 <li>
                  <a href="#">
                  <i class="fa fa-users fa-lg"></i> Users
                  </a>
                </li>-->
            </ul>
     </div>
    
</div>

<?php
  }
?>
<script type="text/javascript">
$(document).ready(function () {
 $(".subd").click(function(){
    var myId = $(this).attr('id');
    var datas = {'subdivid': myId,'_token': $('input[name="_token"]').val()};
    redirectPost('{{url("personnelaReport")}}', datas);

 });  
}); 
</script>    