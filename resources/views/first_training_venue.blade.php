@extends('layouts.master')
@section('content')

<!--// top-bar -->
<div class="wrapper">
    <!-- Sidebar Holder -->
    @include('layouts.sidebar')

    <!-- Page Content Holder -->
    <div id="content">           
        <section class="tables-section">
            <nav aria-label="breadcrumb" style="width:100%;">
                <div class="breadcrumb pagehead1">
                  <span class="headbrand"><i class="fa fa-desktop"></i> Add First Training Venue
                        <span class="scoop-mcaret1"></span>
                    </span>&nbsp;&nbsp;
                    &nbsp;{!! Form::button(' List of First Training Venue&nbsp;<i class="fa fa-arrow-circle-right"></i>',['id'=>'first_training_venue_list','class' => 'btn btn-primary-year add-new-button']) !!}  
                    <div class="clearfix"></div>
                </div>
            </nav>

            <!-- form -->
            <div class="outer-w3-agile">
                {!! Form::open(['url' => 'firstTrVenue', 'name' => 'firstTrVenue', 'class' =>'request-info clearfix form-horizontal', 'id' => 'firstTrVenue', 'method' => 'post','role'=>'','files' => true]) !!}
                {!! Form::hidden('districtcd', session()->get('districtcd_ppds'),['id'=>'districtcd']) !!}
                {!! Form::hidden('subvenue_array', '',['class'=>'form-control','id'=>'subvenue_array']) !!}
                {!! Form::hidden('capacity_array', '',['class'=>'form-control','id'=>'capacity_array']) !!}
                {!! Form::hidden('tysfs_code', '',['id'=>'tysfs_code']) !!}
                {!! Form::hidden('tysfm_code', '',['id'=>'tysfm_code']) !!}

                <div class="panel-group" id="accordion5">                                                              
                    <div class="panel panel-default">
                        <div class="panel-heading1">
                            <h6 class="panel-title">
                                <a data-toggle="#" data-parent="#accordion5" href="#collapsePV"> <span class="fa fa-minus"></span> <span class="highlight">Add First Training Venue</span></a>
                            </h6>
                        </div>
                        <div id="collapsePV" class="panel-collapse collapse5">
                            <div class="panel-body">                                   
                                <div class="row">  
                                    <div class='col-sm-3'>
                                        {!! Form::label('subdivision', 'Sub Division:', ['class'=>'highlight required']) !!}
                                          <!--<span class="highlight">Sub Division</span> -->
                                        <div class="form-group">
                                            <div class=''>
                                                {!! Form::select('subdivision',[''=>'[Select]'],null,['id'=>'subdivision','class'=>'form-control','autocomplete'=>'off']) !!}
                                            </div>
                                        </div>
                                    </div> 
                                    <div class="col-sm-3">
                                        <label for="assembly" class="highlight required">Assembly:</label>
                                        <div class="form-group">
                                            <div class="">
                                                <select id="assembly" class="form-control" autocomplete="off" name="assembly">
                                                    
                                                    <option value="">[Select]</option>
                                                
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class='col-sm-3'>
                                        {!! Form::label('blockMunicipality', 'Block/Municipality:', ['class'=>'highlight required']) !!}
                                        <div class="form-group">
                                            <div class=''>
                                                {!! Form::select('blockMunicipality',[''=>'[Select]'],null,['id'=>'blockMunicipality','class'=>'form-control','autocomplete'=>'off']) !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class='col-sm-3'>
                                        {!! Form::label('venuename', 'Venue Name:', ['class'=>'highlight required']) !!}
                                        <span style="color:red;font-size: 11px;">[Max:100]</span>
                                        <div class="form-group">
                                            <div class=''>
                                                {!! Form::text('venuename',null,['id'=>'venuename','class'=>'form-control','autocomplete'=>'off','maxlength'=>'100']) !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class='col-sm-3'>
                                        {!! Form::label('venueaddress', 'Venue Address:', ['class'=>'highlight required']) !!}
                                        <span style="color:red;font-size: 11px;">[Max:100]</span>
                                        <div class="form-group">
                                            <div class=''>
                                                {!! Form::text('venueaddress',null,['id'=>'venueaddress','class'=>'form-control','autocomplete'=>'off','maxlength'=>'100']) !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="row"> 
                                    <div class='col-sm-12'>
                                        <div class="form-group">
                                            <table class="table table-bordered table-striped" style='border-top: 2px solid #4cae4c;' id="sub_venue_allocation">
                                                <thead>
                                                    <tr style="background-color: #f5f8fa">
                                                        <th width='65%' class="highlight">Subvenue Name <span style="color: red;">* </span><span style="color:red;font-size: 11px;">[Max:30]</span></th>
                                                        <th width='30%' class="highlight">Maximum Capacity <span style="color: red;">* </span><span style="color:red;font-size: 11px;">[Max:4]</span></th>

                                                        <th width='20%' class="highlight">Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr style="background-color: #f5f8fa">
                                                        <td>
                                                            {!! Form::text('subvenuename', null, ['id'=>'subvenuename','class'=>'form-control ','placeholder'=>'Enter Name','maxlength'=>'30'])!!}
                                                        </td>


                                                        <td>   {!! Form::text('capacity', null, ['id'=>'capacity','class'=>'form-control','placeholder'=>'Enter Capacity', 'maxlength'=>'4']) !!}</td>
                                                        <td>   <a href='javascript:' class='add-row'><i class='fa fa-plus-circle' style='color: #17a2b8;cursor:pointer;font-size: 16.5px;padding-top: 10px;'></i>&nbsp;</a>  </td>

                                                    </tr> 
                                                </tbody>

                                            </table>
                                        </div>
                                    </div>

                                </div> 
                                <div class="row">  

                                    <div class='col-sm-3'>
                                        {!! Form::label('maxcapacity', 'Maximum Capacity:', ['class'=>'highlight']) !!}
                                        <div class="form-group">
                                            <div class=''>
                                                {!! Form::text('maxcapacity',null,['id'=>'maxcapacity','class'=>'form-control','autocomplete'=>'off','readonly'=>'true']) !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class='col-sm-9'>
                                        {!! Form::label('', '', ['class'=>'highlight']) !!}
                                        <div class="form-group text-right permit">                            	
                                            {{ Form::button('Submit', ['class' => 'btn btn-info', 'type' => 'submit','id'=>'Submit']) }}
                                            {{ Form::button('Reset', ['class' => 'btn btn-success' ,'type' => 'reset','id'=>'reset']) }}
                                        </div>
                                    </div>
                                </div>     

                            </div>                              
                        </div>
                    </div>  
                </div>
                {!! Form::close() !!}             
            </div>
            <!--// form -->

        </section>

    </div>
</div>
<!-- Copyright -->
<script type="text/javascript">
    $(document).ready(function () {
        $("#first_training_venue_list").click(function () {
            window.location.href = "first_training_venue_list";
        });
        char_num("#capacity");
        $("#reset").click(function () {
            location.reload(true);
        });
        array_subvenue = [];
        array_capacity = [];
        array_duplicate = [];
        change_subdivision();
        $('select[name="subdivision"]').on('change', function () {
            change_blockMuni();
        });
        
        $(".add-row").click(function () {
            var subvenuename = $("#subvenuename").val();
            var capacity = $("#capacity").val();
            if (subvenuename == "" || capacity == "") {
                $.alert({
                    title: 'Error!!',
                    type: 'red',
                    icon: 'fa fa-exclamation-triangle',
                    content: "Enter required field"
                });
                return false;
            }
            var asm_member = subvenuename;
            var duplicate_asm = jQuery.inArray(asm_member, array_duplicate);
            if (duplicate_asm >= 0) {
                $.alert({
                    title: 'Error!!',
                    type: 'red',
                    icon: 'fa fa-exclamation-triangle',
                    content: 'Duplicate Subvenue not allow'
                });
                return false;
            }
            array_duplicate.push(subvenuename);
            var markup = "<tr class='table_tr'><td>" + subvenuename + "</td><td>" + capacity + "</td><td><a href='javascript:' onclick='deleteRow(this);'><i class='fa fa-trash-alt' style='color:#f10418;cursor:pointer;font-size:16px;'></i></a></td></tr>";
            $("table tbody").append(markup);
            array_subvenue.push(subvenuename);
            array_capacity.push(capacity);
            var tot_sum = eval(array_capacity.join("+"));
            $("#maxcapacity").val(tot_sum);
            $("#subvenue_array").val(array_subvenue);
            $("#capacity_array").val(array_capacity);
            $("#capacity").val('');
        });
        $('#firstTrVenue').bootstrapValidator({

            message: 'This value is not valid',
            feedbackIcons: {
                valid: 'glyphicon',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {

                subdivision: {
                    validators: {
                        notEmpty: {
                            message: 'Subdivision is required'
                        }
                    }
                },
                blockMunicipality: {
                    validators: {
                        notEmpty: {
                            message: 'Block/Municipality is required'
                        }
                    }
                },
                assembly: {
                    validators: {
                        notEmpty: {
                            message: 'Assembly is required'
                        }
                    }
                },
                venuename: {
                    validators: {
                        notEmpty: {
                            message: 'Venue Name is required'
                        }
                    }
                },
                venueaddress: {
                    validators: {
                        notEmpty: {
                            message: 'Venue Address is required'
                        }
                    }
                }

            }

        }).on('success.form.bv', function (e) {
            e.preventDefault();
            var norowCount = $('#sub_venue_allocation tr').length;
            if (norowCount == 2) {
                $.alert({
                    title: 'Error!!',
                    type: 'red',
                    icon: 'fa fa-exclamation-triangle',
                    content: 'Add atleast one subvenue'
                });
                return false;
            }
            var action = $(this).attr('action');
            var type = $(this).attr('method');
            var districtcd = $("#districtcd").val();
            var subdivision = $("#subdivision").val();
            var venuename = $("#venuename").val();
            var venueaddress = $("#venueaddress").val();
            var blockMunicipality = $("#blockMunicipality").val();
            var maxcapacity = $("#maxcapacity").val();
            var subvenue_array = JSON.stringify(array_subvenue);
            var capacity_array = JSON.stringify(array_capacity);
            var token = $("input[name='_token']").val();
            var assembly = $("#assembly").val();
            var fd = new FormData();
            fd.append('districtcd', districtcd);
            fd.append('_token', token);
            fd.append('subdivision', subdivision);
            fd.append('subvenue_array', subvenue_array);
            fd.append('capacity_array', capacity_array);
            fd.append('venuename', venuename);
            fd.append('venueaddress', venueaddress);
            fd.append('blockMunicipality', blockMunicipality);
            fd.append('maxcapacity', maxcapacity);
            fd.append('assembly', assembly);
            $(".se-pre-con").fadeIn("slow");
            $.ajax({
                type: type,
                url: action,
                data: fd,
                processData: false,
                contentType: false,
                dataType: "json",
                success: function (data) {
                    $(".se-pre-con").fadeOut("slow");

                    if (data.status == 1) {
                        var msg = "Record(s) saved successfully";
                        $.confirm({
                            title: 'Success!',
                            type: 'green',
                            icon: 'fa fa-check',
                            content: msg,
                            buttons: {
                                ok: function () {
                                    location.reload(true);
                                }

                            }
                        });

                    } else {
                        var msg = "Record(s) updated successfully";
                        $.confirm({
                            title: 'Success!',
                            type: 'green',
                            icon: 'fa fa-check',
                            content: msg,
                            buttons: {
                                ok: function () {
                                    location.reload(true);
                                }

                            }
                        });

                    }
                    //$("#firstTrVenue")[0].reset();
                    // $('.table_tr').html("");


                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $(".se-pre-con").fadeOut("slow");
                    var msg = "";
                    if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                        msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                    } else {
                        if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                            msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                        } else {
                            msg += "Error(s):<strong><ul>";
                            $.each(jqXHR.responseJSON, function (key, value) {
                                msg += "<li>" + value + "</li>";
                            });
                            msg += "</ul></strong>";
                        }
                    }
                    $.alert({
                        title: 'Error!!',
                        type: 'red',
                        icon: 'fa fa-exclamation-triangle',
                        content: msg
                    });
                }
            });
        });
    });
    function deleteRow(row)
    {
        var i = row.parentNode.parentNode.rowIndex;

        document.getElementById('sub_venue_allocation').deleteRow(i);
        var index_arr = parseInt(i) - parseInt(2);
        array_subvenue.splice(index_arr, 1);
        array_capacity.splice(index_arr, 1);
        array_duplicate.splice(index_arr, 1);
        var tot_sum = eval(array_capacity.join("+"));
        $("#maxcapacity").val(tot_sum);
        $("#subvenue_array").val(array_subvenue);
        $("#capacity_array").val(array_capacity);

    }
    function change_subdivision() {
        var forDist = $("#districtcd").val();
        var token = $("input[name='_token']").val();
        // $(".se-pre-con").fadeIn("slow");
        $.ajax({
            url: "getSubdivisionData",
            method: 'POST',
            data: {forDist: forDist, _token: token},
            success: function (data) {//alert(data.options);
                //   $(".se-pre-con").fadeOut("slow");
                $("select[name='subdivision'").html('');

                if (data.status == 1)
                {
                    $("select[name='subdivision'").append('<option value>[Select]</option>');
                    $.each(data.options, function (k, v)
                    {
                        $("select[name='subdivision'").append('<option value=' + k + '>' + v + '</option>');
                    });
                    //$("select[name='subdivision'").html(data.options);
                    // change_officeName();
                    // getMemberDetails();
                }
            },
     
            error: function (jqXHR, textStatus, errorThrown) {
                $(".se-pre-con").fadeOut("slow");
                var msg = "";
                if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                    msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                } else {
                    if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                        msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                    } else {
                        msg += "Error(s):<strong><ul>";
                        $.each(jqXHR.responseJSON, function (key, value) {
                            msg += "<li>" + value + "</li>";
                        });
                        msg += "</ul></strong>";
                    }
                }
                $.alert({
                    title: 'Error!!',
                    type: 'red',
                    icon: 'fa fa-exclamation-triangle',
                    content: msg
                });
            }
        });
    }


function changeAssembly(){
    var forDist = $("#districtcd").val();
    var subdivision = $("#subdivision").val();
        var token = $("input[name='_token']").val();
        // $(".se-pre-con").fadeIn("slow");
        $.ajax({
            url: "getAssemblyData",
            method: 'POST',
            data: {forDist: forDist,subdivision:subdivision, _token: token},
            success: function (data) {
             $(".se-pre-con").fadeOut("slow");
                $("select[name='assembly'").html('');
          if(data.status==1)
          {
              $("select[name='assembly'").html(data.options);
          }
            },
         
            error: function (jqXHR, textStatus, errorThrown) {
                $(".se-pre-con").fadeOut("slow");
                var msg = "";
                if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                    msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                } else {
                    if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                        msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                    } else {
                        msg += "Error(s):<strong><ul>";
                        $.each(jqXHR.responseJSON, function (key, value) {
                            msg += "<li>" + value + "</li>";
                        });
                        msg += "</ul></strong>";
                    }
                }
                $.alert({
                    title: 'Error!!',
                    type: 'red',
                    icon: 'fa fa-exclamation-triangle',
                    content: msg
                });
            }
        });
}


    function change_blockMuni() {
        var subdivision = $("#subdivision").val();
        var token = $("input[name='_token']").val();
        $(".se-pre-con").fadeIn("slow");
        $.ajax({
            url: "getBlockMuni",
            method: 'POST',
            data: {subdivision: subdivision, _token: token},
            success: function (data) {//alert(data.options);
                $(".se-pre-con").fadeOut("slow");
                $("select[name='blockMunicipality'").html('');
                if (data.status == 1)
                {
                    $("select[name='blockMunicipality'").html(data.options);
                    // change_officeName();
                    //getMemberDetails();
                }
            },
            
            error: function (jqXHR, textStatus, errorThrown) {
                $(".se-pre-con").fadeOut("slow");
                var msg = "";
                if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                    msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                } else {
                    if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                        msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                    } else {
                        msg += "Error(s):<strong><ul>";
                        $.each(jqXHR.responseJSON, function (key, value) {
                            msg += "<li>" + value + "</li>";
                        });
                        msg += "</ul></strong>";
                    }
                }
                $.alert({
                    title: 'Error!!',
                    type: 'red',
                    icon: 'fa fa-exclamation-triangle',
                    content: msg
                });
            },
            complete:function(data){
            changeAssembly();
           
         }
        });
    }
</script>
@stop