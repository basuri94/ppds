
@extends('layouts.master')
@section('content')
<style>
    .dropdown-menu{
        /* right: 67%;*/
        /* left: auto; */
        min-width: 0px !important; 
        width:230px !important;
        padding: 1em !important;
    }

</style>
<div class="wrapper">
    <!-- Sidebar Holder -->
    @include('layouts.sidebar')

    <!-- Page Content Holder -->
    <div id="content">           
        <section class="tables-section">
            <nav aria-label="breadcrumb" style="width:100%;">
                <div class="breadcrumb pagehead1">

                    {!! Form::button('<i class="fa fa-desktop"></i> Order date & no',['class' => 'btn btn-primary-header add-new-button']) !!}

                    <div class="col-md-offset-8 pull-right">
                        <!--                {!! Form::button('Academic Year:&nbsp;',['id'=>'show_academic','class' => 'btn btn-primary-year']) !!}-->

                    </div>
                    <div class="clearfix"></div>
                </div>
            </nav>

            <!-- form -->
            <div class="outer-w3-agile">
                {!! Form::open(['url' => 'order_entry_details', 'name' => 'order_entry_details', 'class' =>'request-info clearfix form-horizontal', 'id' =>'order_entry_details', 'method' => 'post','role'=>'','files' => true]) !!}
                {!! Form::hidden('districtcd', session()->get('districtcd_ppds'),['id'=>'districtcd']) !!}
                {!! Form::hidden('order_cd_edit_code','',['class'=>'form-control','id'=>'order_cd_edit_code']) !!}

                <div class="panel-group" id="accordion5" style="padding-top: 0px;">                                                              
                    <div class="panel panel-default">
                        <div class="panel-heading1">
                            <h6 class="panel-title">
                                <a data-toggle="#" data-parent="#accordion5" href="#collapseUV"> <span class="fa fa-minus"></span> <span class="highlight">Order date & no </span></a>
                            </h6>
                        </div>
                        <div id="collapseUV" class="panel-collapse collapse5">
                            <div class="panel-body">                                   
                                <div class="row">                                   
                                    <div class='col-sm-4'>
                                        {!! Form::label('order_date', 'Order Date:', ['class'=>'highlight required']) !!}
                                        <div class="form-group">
                                            <div class=''>
                                                {!! Form::text('order_date',null,['id'=>'order_date','class'=>'form-control','autocomplete'=>'off','placeholder'=>'DD/MM/YYYY','maxlength'=>'10']) !!}

                                            </div>
                                        </div>
                                    </div>  
                                    <div class='col-sm-4'>
                                        {!! Form::label('order_no', 'Order No:', ['class'=>'highlight required']) !!} 
<!--                                        <span style="color:red;font-size: 11px;font-weight: bold;">[Max:15]</span>-->
                                        <div class="form-group">
                                            <div class=''>
                                                {!! Form::text('order_no',null,['id'=>'order_no','class'=>'form-control','autocomplete'=>'off','maxlength'=>'50']) !!}

                                            </div>
                                        </div>
                                    </div> 
                                </div>
                                <div class="row" > 
                                    <div class='col-sm-12'>                    
                                        <div class="form-group text-right permit">                            	
                                            {{ Form::button('Submit', ['class' => 'btn btn-info', 'type' => 'submit','id'=>'submit']) }}
                                            {{ Form::button('Reset', ['class' => 'btn btn-success', 'style'=>'display:none;' ,'type' => 'reset','id'=>'reset']) }}
                                        </div>
                                    </div>

                                </div>
                                {!! Form::close() !!}   
                                <div class="row"> 
                                    <div class='col-sm-12'>
                                        <div id='OrderDateNoDetails'></div>
                                    </div>
                                </div>  


                            </div>
                        </div>                              
                    </div> 


                </div>



            </div>
            <!--// form -->

        </section>
    </div>
</div>
<!-- Copyright -->
<script type="text/javascript">

    $(document).ready(function () {
        getOrderDateAndTimeRecords();
        $("#reset").click(function () {
            location.reload(true);
        });
        var date = new Date();
        var today = new Date(date.getFullYear(), date.getMonth(), date.getDate());

        $('#order_date').datepicker({
            format: "dd/mm/yyyy",
            todayHighlight: true,
//            endDate: new Date(),
            autoclose: true
        });
        $('#order_date').datepicker('setDate', today);

//        $("#first_train_dt").datepicker({
//            format: "dd/mm/yyyy",
//            autoclose: true,
//            todayHighlight: true,
//            endDate: new Date()})
//                .on('changeDate', function (ev) {
//                    if ($('#datepicker').valid()) {
//                        $('#datepicker').removeClass('invalid').addClass('success');
//                    }
//                });
        $('#order_entry_details')
                .bootstrapValidator({
                    message: 'This value is not valid',
                    feedbackIcons: {
                        valid: 'fa fa-check',
                        invalid: 'fa fa-times',
                        validating: 'fa fa-refresh'
                    },
                    fields: {
                        order_date: {
                            validators: {
                                notEmpty: {
                                    message: 'Date is required'
                                }
                            }
                        },
                        order_no: {
                            validators: {
                                notEmpty: {
                                    message: 'Order no is required'
                                },
                                regexp: {
                                    regexp: /^[A-Za-z0-9\s()/]+$/,
                                    message: 'Order no can consist of alphanumerical characters'
                                }
                            }
                        }
                    }
                }).on('success.form.bv', function (e) {
            // alert('HI');
            // Prevent form submission
            e.preventDefault();
            var action = $(this).attr('action');

            var districtcd = $("#districtcd").val();
            var order_date = $("#order_date").val();
            var order_no = $("#order_no").val();
            var type = $(this).attr('method');
            var token = $("input[name='_token']").val();
            var order_cd_edit_code = $('#order_cd_edit_code').val();
            var fd = new FormData();
            fd.append('districtcd', districtcd);
            fd.append('order_date', order_date);
            fd.append('order_no', order_no);
            fd.append('order_cd_edit_code', order_cd_edit_code);

            fd.append('_token', token);
            if (order_cd_edit_code != '') {
                action = "update_order_table"
                fd.append('order_cd_edit_code', order_cd_edit_code);
            }
            $(".se-pre-con").fadeIn("slow");
            $.ajax({
                type: type,
                url: action,
                data: fd,
                processData: false,
                contentType: false,
                dataType: "json",
                success: function (data) {

                    if (data.status == 1)
                    {

                        $(".se-pre-con").fadeOut("slow");

                        var msg = "Record(s) saved successfully";
                        $.confirm({
                            title: 'Success!',
                            type: 'green',
                            icon: 'fa fa-check',
                            content: msg,
                            buttons: {
                                ok: function () {

                                    //  document.getElementById("order_entry_details").reset();
                                    $('#order_date').val('');
                                    $('#order_no').val('');
                                    $('.form-control-feedback').css('display', 'none');
                                    $('.has-success').removeClass('has-success');
                                    getOrderDateAndTimeRecords();
                                }

                            }
                        });
                    } else if (data.status == 2) {
                        $(".se-pre-con").fadeOut("slow");
                        var msg = "Record(s) updated successfully";
                        $.confirm({
                            title: 'Success!',
                            type: 'green',
                            icon: 'fa fa-check',
                            content: msg,
                            buttons: {
                                ok: function () {
                                    location.reload(true);

                                }

                            }
                        });
                    }

                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $(".se-pre-con").fadeOut("slow");
                 
                      $("#submit").removeAttr("disabled");
                    var msg = "";
                    if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                        msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                    } else {
                        if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                            msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                        } else {
                            msg += "Error(s):<strong><ul>";
                            $.each(jqXHR.responseJSON, function (key, value) {
                                msg += "<li>" + value + "</li>";
                            });
                            msg += "</ul></strong>";
                        }
                    }
                    $.alert({
                        title: 'Error!!',
                        type: 'red',
                        icon: 'fa fa-warning',
                        content: msg
                    });
                }
            });


        });


    });
    function deleteOrderDateTime(order_cd) {

        var msg = "Are you sure to delete this record?";
        $.confirm({
            title: 'Confirm!',
            type: 'red',
            icon: 'fa fa-exclamation-triangle',
            content: msg,
            buttons: {
                ok: function () {
                    $.ajax({
                        type: 'post',
                        url: 'deleteOrderDateTime',
                        data: {'order_cd': order_cd, '_token': $('input[name="_token"]').val()},
                        dataType: 'json',
                        success: function (data) {
                            if (data.status == 1)
                            {
                                var msg = "<strong>SUCCESS: Record deleted successfully</strong>";
                                $.confirm({
                                    title: 'Success!',
                                    type: 'green',
                                    icon: 'fa fa-check',
                                    content: msg,
                                    buttons: {
                                        ok: function () {
                                            $('#order_entry_details').trigger("reset");
                                            getOrderDateAndTimeRecords();
                                        }

                                    }

                                });

                            } else if (data.status == 2) {
                                $.alert({
                                    title: 'Error!!',
                                    type: 'red',
                                    icon: 'fa fa-warning',
                                    content: data.options
                                });
                            }
                        },
                        error: function (jqXHR, textStatus, errorThrown) {
                            $(".se-pre-con").fadeOut("slow");
                            var msg = "";
                            if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                                msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                            } else {
                                if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                                    msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                                } else {
                                    msg += "Error(s):<strong><ul>";
                                    $.each(jqXHR.responseJSON, function (key, value) {
                                        msg += "<li>" + value + "</li>";
                                    });
                                    msg += "</ul></strong>";
                                }
                            }
                            $.alert({
                                title: 'Error!!',
                                type: 'red',
                                icon: 'fa fa-warning',
                                content: msg
                            });
                        }
                    });


                },
                cancel: function () {

                }

            }
        });


    }

    function editOrderDateTime(order_cd) {
        $("#reset").show();
        $(".se-pre-con").fadeIn("slow");
        $.ajax({
            type: 'post',
            url: 'editOrderDateTime',
            data: {'order_cd': order_cd, '_token': $('input[name="_token"]').val()},
            dataType: 'json',
            success: function (data) {
                $(".se-pre-con").fadeOut("slow");
                $('#order_date').val(data.options[0].odDaTi);
                $('#order_no').val(data.options[0].order_no);
                $('#order_cd_edit_code').val(data.options[0].order_cd);


            },
            error: function (jqXHR, textStatus, errorThrown) {
                $(".se-pre-con").fadeOut("slow");
                var msg = "";
                if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                    msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                } else {
                    if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                        msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                    } else {
                        msg += "Error(s):<strong><ul>";
                        $.each(jqXHR.responseJSON, function (key, value) {
                            msg += "<li>" + value + "</li>";
                        });
                        msg += "</ul></strong>";
                    }
                }
                $.alert({
                    title: 'Error!!',
                    type: 'red',
                    icon: 'fa fa-warning',
                    content: msg
                });
            }
        });
    }
    function getOrderDateAndTimeRecords() {
        var districtcd = $("#districtcd").val();
        var token = $("input[name='_token']").val();
        $(".se-pre-con").fadeIn("slow");
        $.ajax({
            url: "getOrderDateAndTimeRecords",
            method: 'POST',
            data: {forDist: districtcd, _token: token},
            success: function (json) {
                $(".se-pre-con").fadeOut("slow");
                $("#OrderDateNoDetails").html('');
                if (json.status == 1)
                {

                    $('#OrderDateNoDetails').html(json.options);


                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $(".se-pre-con").fadeOut("slow");
                var msg = "";
                if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                    msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                } else {
                    if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                        msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                    } else {
                        msg += "Error(s):<strong><ul>";
                        $.each(jqXHR.responseJSON, function (key, value) {
                            msg += "<li>" + value + "</li>";
                        });
                        msg += "</ul></strong>";
                    }
                }
                $.alert({
                    title: 'Error!!',
                    type: 'red',
                    icon: 'fa fa-warning',
                    content: msg
                });
            }
        });

    }


</script>
@stop

