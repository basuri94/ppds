@extends('layouts.master')
@section('content')
<style>
    .jconfirm-box{
        width: 600px;
        margin-left: -200px !important; 
    }

</style>
<!--// top-bar -->
<div class="wrapper">
    <!-- Sidebar Holder -->
    @include('layouts.sidebar')
    <!-- Page Content Holder -->
    <div id="content">           
        <section class="tables-section">

            <div class="breadcrumb pagehead1">
                <span class="headbrand"><i class="fa fa-desktop"></i> List of Group Training Allocation
                    <span class="scoop-mcaret1"></span>
                </span>&nbsp;&nbsp;
                &nbsp;{!! Form::button(' Add/Edit Group Training Allocation&nbsp;<i class="fa fa-plus-circle"></i>',['id'=>'second_training_allocation','class' => 'btn btn-primary-year add-new-button']) !!}          
                <div class="clearfix"></div>
            </div>
            </nav>
            <div class="outer-w3-agile">
                <div class="panel-group" id="accordion5">                                                              
                    <div class="panel panel-default">
                        <div class="panel-heading1">
                            <h6 class="panel-title">
                                <a data-toggle="#" data-parent="#accordion5" href="#collapsePV"> <span class="fa fa-minus"></span> <span class="highlight">List of Group Training Allocation</span></a>
                            </h6>
                        </div>
                        <div id="collapsePV" class="panel-collapse collapse5">
                            <div class="panel-body">                                   
                                <div class="row"> 
                                {{-- <div class='col-sm-3'>
                                        {!! Form::label('zone', 'Zone:', ['class'=>'highlight']) !!}
                                        <div class="form-group">
                                            <div class=''>
                                                {!! Form::select('zone',[''=>'[Select]'],Null,['id'=>'zone','class'=>'form-control','autocomplete'=>'off']) !!}
                                            </div>
                                        </div>
                                    </div>   --}}
                                 
                                    <div class='col-sm-3'>
                                        {!! Form::label('subdivision', 'Sub Division:', ['class'=>'highlight']) !!}
                                       <!--<span class="highlight">Sub Division</span> -->
                                        <div class="form-group">
                                            <div class=''>
                                                {!! Form::select('subdivision',[''=>'[Select]'],null,['id'=>'subdivision','class'=>'form-control','autocomplete'=>'off']) !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class='col-sm-3'>
                                        {!! Form::label('phase', 'Phase:', ['class'=>'highlight ']) !!}
                                       <div class="form-group">
                                           <div class=''>
                                                <select id="phase" class="form-control" name="phase">
                                                            <option value="">[Select]</option>
                                                </select>
                                            </div>
                                       </div>
                                   </div> 
                                    <div class='col-sm-3'>
                                        {!! Form::label('venuename', 'Venue Name:', ['class'=>'highlight']) !!}
                                        <div class="form-group">
                                            <div class=''>
                                                {!! Form::select('venuename',[''=>'[Select]'],null,['id'=>'venuename','class'=>'form-control','autocomplete'=>'off']) !!}
                                            </div>
                                        </div>
                                    </div>                    
                                    <div class='col-sm-3'>
                                        {!! Form::label('trainingdatetime', 'Training Date & Time:', ['class'=>'highlight']) !!}
                                        <div class="form-group">
                                            <div class=''>
                                                {!! Form::select('trainingdatetime',[''=>'[Select]'],null,['id'=>'trainingdatetime','class'=>'form-control','placeholder'=>'[Select]','autocomplete'=>'off']) !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class='col-sm-3'>
                                        {!! Form::label('assembly', 'Assembly:', ['class'=>'highlight']) !!}
                                        <div class="form-group">
                                            <div class=''>
                                                {!! Form::select('assembly',[''=>'[Select]'],null,['id'=>'assembly','class'=>'form-control','autocomplete'=>'off']) !!}
                                            </div>
                                        </div>
                                    </div>


                                </div>

                            </div>
                        
                        </div>
                        <div class="form-horizontal">
                            <div class="form-group row">
                                {{csrf_field()}}
                                <div class="datatbl table-responsive" style="width: 96%;margin-left: 20px;">
                                    <span style="font-size: .9rem;color: #f85a40;">NOTE:[A = PP allocated in Subvenue, U = PP Used in Subvenu (after Randomisation), M = Maximum capacity of Subvenue]</span>
                                    {!! Form::hidden('districtcd', session()->get('districtcd_ppds'),['id'=>'districtcd']) !!}
                                    <table class="table table-striped table-bordered table-hover notice-types-table" id="datatable-table" style="">
                                        <thead>
                                            <tr>
                                                <th>SL#</th>
                                                <th>Venue</th>
                                                <th>Phase</th>
                                                <th>P/R</th>
                                                <th>Group</th>
                                                <th>PR (A)</th><th>PR (U)</th>
                                                <th>P1 (A)</th><th>P1 (U)</th>
                                                <th>P2 (A)</th><th>P2 (U)</th>
                                                <th>P3 (A)</th><th>P3 (U)</th>
                                                <th>PA (A)</th><th>PA (U)</th>
                                                <th>PB (A)</th><th>PB (U)</th>
                                                <th>TOTAL (A)</th><th>TOTAL (U)</th><th>TOTAL (M)</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody></tbody>
                                        <!-- Table Footer -->

                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </section>
    </div>
</div>
<script>
    $(document).ready(function () {
        $("#second_training_allocation").click(function () {
            window.location.href = "second_training_allocation";
        });
      //  getZoneDetails();
        change_subdivision();
    //    $('select[name="zone"]').on('change', function () {
    //         var zone = $('#zone').val();
    //         if (zone == '') {
    //             create_table();
    //         } else {
    //             change_subdivision();
    //             getZonePhasedata(); 
    //         }
    //     });
        $('select[name="phase"]').on('change', function () {
            create_table();
        });
        $('select[name="subdivision"]').on('change', function () {
             var subdivision = $('#subdivision').val();
            if (subdivision == '') {
                create_table();
            } else {
                change_venueName();
                getSubdivisionPhasedata();
            }
        });
        $('select[name="trainingdatetime"]').on('change', function () {
            // get_SubvenueName();
            create_table();
        });
        $('select[name="venuename"]').on('change', function () {
            //get_SubvenueName(); 
            create_table();
        });
        $('select[name="assembly"]').on('change', function () {
            //get_SubvenueName(); 
            create_table();
        });
       
        var table = "";
        table = $('#datatable-table').DataTable();
        table.on('order.dt search.dt draw.dt', function () {
            $(".se-pre-con").fadeOut("slow");
            $('[data-toggle="tooltip"]').tooltip();
            table.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
                cell.innerHTML = table.page() * table.page.len() + (i + 1);
            });
        });
        table.on('draw.dt', function () {
            $(".se-pre-con").fadeOut("slow");

            $('.delete-button').click(function () {
//                var reply = $.confirm('Are you sure to delete the record?');
//                if (!reply) {
//                    return false;
//   
//                             }
                var value_all = this.id;
                if (value_all == 'NA') {
                     $.alert({
                        title: 'Error!!',
                        type: 'red',
                        icon: 'fa fa-exclamation-triangle',
                        content: "Record exist in another table.You can't delete this record."
                    });
                    return false;
                }
                $.confirm({
                    title: 'Attention!',
                    content: 'Do you want to delete this record?',
                    type: 'orange',
                    icon: 'fa fa-exclamation-triangle',
                    buttons: {
                        confirm: function () {
                            var data_new = value_all;
                            //alert(data_new);
                            $.ajax({
                                type: 'post',
                                url: 'delete_second_training_venue_alloc',
                                data: {'data': data_new, '_token': $('input[name="_token"]').val()},
                                dataType: 'json',
                                success: function (datam) {
                                    var msg = "<strong>Record deleted successfully</strong>";
                                    $.confirm({
                                        title: 'Success!',
                                        type: 'green',
                                        icon: 'fa fa-check',
                                        content: msg,
                                        buttons: {
                                            ok: function () {  
                                                //table.ajax.reload();
                                                create_table();
                                            }
                                        }
                                    });
                                },
                                error: function (jqXHR, textStatus, errorThrown) {
                                    $(".se-pre-con").fadeOut("slow");
                                    var msg = "";
                                    if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                                        msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                                    } else {
                                        if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                                            if (jqXHR.responseJSON.exception_code == 23000) {
                                                msg += "Record exist in another table.You can't delete this record.";
                                            } else {
                                                msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                                            }
                                        } else {
                                            msg += "Error(s):<strong><ul>";
                                            $.each(jqXHR.responseJSON, function (key, value) {
                                                msg += "<li>" + value + "</li>";
                                            });
                                            msg += "</ul></strong>";
                                        }
                                    }
                                    $.alert({
                                        title: 'Error!!',
                                        type: 'red',
                                        icon: 'fa fa-warning',
                                        content: msg,
                                    });
                                }
                            });
                        },
                        cancel: function () {

                        }
                    }
                });


            });

        });
    });
    
    function getSubdivisionPhasedata(){
 // var zone = $("#zone").val();
  var subdivision = $("#subdivision").val();
   var token = $("input[name='_token']").val();
  // $(".se-pre-con").fadeIn("slow");
   $.ajax({
       url: "getSubdivisionPhasedata",
       method: 'POST',
       data: {subdivision:subdivision, _token: token},
       success: function (data) {//alert(data.options);
       //   $(".se-pre-con").fadeOut("slow");
          $("select[name='phase'").html('');
          
          if(data.status==1)
          {
            $("select[name='phase'").append('<option value>[Select]</option>');
            $.each(data.options,function (k, v)
            {
                $("select[name='phase'").append('<option value='+k+'>'+v+'</option>');
            });
            //$("select[name='subdivision'").html(data.options);
             // change_officeName();
             
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
       //  $(".se-pre-con").fadeOut("slow");
           var msg = "";
           if (jqXHR.status !== 422 && jqXHR.status !== 400) {
               msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
           } else {
               if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                   msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
               } else {
                   msg += "Error(s):<strong><ul>";
                   $.each(jqXHR.responseJSON, function (key, value) {
                       msg += "<li>" + value + "</li>";
                   });
                   msg += "</ul></strong>";
               }
           }
           $.alert({
               title: 'Error!!',
               type: 'red',
               icon: 'fa fa-warning',
               content: msg
           });
           
         },
       
   });
}
    function change_subdivision() {
            var forDist = $("#districtcd").val();
            var token = $("input[name='_token']").val();
            // $(".se-pre-con").fadeIn("slow");
            $.ajax({
                url: "getSubdivisionData",
                method: 'POST',
                data: {forDist: forDist, _token: token},
                success: function (data) {//alert(data.options);
                    //   $(".se-pre-con").fadeOut("slow");
                    $("select[name='subdivision'").html('');

                    if (data.status == 1)
                    {
                        $("select[name='subdivision'").append('<option value>[Select]</option>');
                        $.each(data.options, function (k, v)
                        {
                            $("select[name='subdivision'").append('<option value=' + k + '>' + v + '</option>');
                        });
                        //$("select[name='subdivision'").html(data.options);
                        // change_officeName();
                        // getMemberDetails();
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $(".se-pre-con").fadeOut("slow");
                    var msg = "";
                    if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                        msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                    } else {
                        if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                            msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                        } else {
                            msg += "Error(s):<strong><ul>";
                            $.each(jqXHR.responseJSON, function (key, value) {
                                msg += "<li>" + value + "</li>";
                            });
                            msg += "</ul></strong>";
                        }
                    }
                    $.alert({
                        title: 'Error!!',
                        type: 'red',
                        icon: 'fa fa-exclamation-triangle',
                        content: msg
                    });
                },
                complete: function () {
                    getTrainingDateTime();
                }
            });
        }
    function getAssemblyDetails() {
        var subdivision = $("#subdivision").val();
        var token = $("input[name='_token']").val();
      //  $(".se-pre-con").fadeIn("slow");
        $.ajax({
            url: "getSubAssemblyDetails",
            method: 'POST',
            data: {subdivision: subdivision, _token: token},
            success: function (data) {//alert(data.options);
                $(".se-pre-con").fadeOut("slow");
                $("select[name='assembly'").html('');
                if (data.status == 1)
                {
                    $("select[name='assembly'").html(data.options);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $(".se-pre-con").fadeOut("slow");
                var msg = "";
                if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                    msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                } else {
                    if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                        msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                    } else {
                        msg += "Error(s):<strong><ul>";
                        $.each(jqXHR.responseJSON, function (key, value) {
                            msg += "<li>" + value + "</li>";
                        });
                        msg += "</ul></strong>";
                    }
                }
                $.alert({
                    title: 'Error!!',
                    type: 'red',
                    icon: 'fa fa-exclamation-triangle',
                    content: msg
                });
            },
            complete: function () {
                create_table();
            }
        });
    }
    function getTrainingDateTime() {
        var forDist = $("#districtcd").val();
        var token = $("input[name='_token']").val();
        //$(".se-pre-con").fadeIn("slow");
        $.ajax({
            url: "get2ndTrainingDateTime",
            method: 'POST',
            data: {forDist: forDist, _token: token},
            success: function (data) {//alert(data.options);
                $(".se-pre-con").fadeOut("slow");
                $("select[name='trainingdatetime'").html('');
                if (data.status == 1)
                {
                    $("select[name='trainingdatetime'").html(data.options);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $(".se-pre-con").fadeOut("slow");
                var msg = "";
                if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                    msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                } else {
                    if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                        msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                    } else {
                        msg += "Error(s):<strong><ul>";
                        $.each(jqXHR.responseJSON, function (key, value) {
                            msg += "<li>" + value + "</li>";
                        });
                        msg += "</ul></strong>";
                    }
                }
                $.alert({
                    title: 'Error!!',
                    type: 'red',
                    icon: 'fa fa-warning',
                    content: msg
                });
            },
            complete: function () {
                create_table();
            }
        });
    }
    function change_venueName() {
        var subdivision = $("#subdivision").val();
        var token = $("input[name='_token']").val();
        //$(".se-pre-con").fadeIn("slow");
        $.ajax({
            url: "get2ndVenueName",
            method: 'POST',
            data: {subdivision: subdivision, _token: token},
            success: function (data) {//alert(data.options);
                //$(".se-pre-con").fadeOut("slow");
                $("select[name='venuename'").html('');

                if (data.status == 1)
                {
                    $("select[name='venuename'").append('<option value>[Select]</option>');
                    $.each(data.options, function (k, v)
                    {
                        $("select[name='venuename'").append('<option value=' + k + '>' + v + '</option>');
                    });
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $(".se-pre-con").fadeOut("slow");
                var msg = "";
                if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                    msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                } else {
                    if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                        msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                    } else {
                        msg += "Error(s):<strong><ul>";
                        $.each(jqXHR.responseJSON, function (key, value) {
                            msg += "<li>" + value + "</li>";
                        });
                        msg += "</ul></strong>";
                    }
                }
                $.alert({
                    title: 'Error!!',
                    type: 'red',
                    icon: 'fa fa-exclamation-triangle',
                    content: msg
                });
            },
            complete: function () {
                getAssemblyDetails();
            }
        });
    }
    function create_table() {
        var table = "";
        var token = $('input[name="_token"]').val();
       // $(".se-pre-con").fadeIn("slow");

        $("#datatable-table").dataTable().fnDestroy();
        table = $('#datatable-table').DataTable({
            "processing": true,
            "serverSide": true,
            "dom": 'lBfrtip',
            "buttons": [
                //'copy', 'csv', 'excel', 'pdf'
                {
                    extend: 'collection',
                    text: 'Export',
                    buttons: [
                        {
                            extend: 'copyHtml5',
                            exportOptions: {
                                 columns: ':visible'
                            }
                        },
                        {
                            extend: 'excelHtml5',

                            exportOptions: {
                                columns: ':visible'
                            }
                        },
                        {
                            extend: 'pdfHtml5',
                            exportOptions: {
                                columns: ':visible'
                            }
                        },
                        {
                            extend: 'print',
                            exportOptions: {
                                columns: ':visible'
                            }
                        },
                        {
                            extend: 'csvHtml5',
                            exportOptions: {
                                columns: ':visible'
                            }
                        }

                    ]

                },
                'colvis'
            ],
            "ajax": {
                url: "second_training_venue_allocation_datatable",
                type: "post",
                data: {'_token': $('input[name="_token"]').val(), 'subdivision': $("#subdivision").val(), 'venuename': $("#venuename").val(), 'trainingdatetime': $("#trainingdatetime").val(), 'assembly': $("#assembly").val(),'phase': $("#phase").val()},
                dataSrc: "fisrt_training_venue_alloc",
                error: function (jqXHR, textStatus, errorThrown) {
                    var msg = "";
                    if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                        msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                    } else {
                        if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                            if (jqXHR.responseJSON.exception_code == 23000) {
                                msg += "Some Sql Exception Occured";
                            } else {
                                msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                            }
                        } else {
                            msg += "Error(s):<strong><ul>";
                            $.each(jqXHR.responseJSON, function (key, value) {
                                msg += "<li>" + value + "</li>";
                            });
                            msg += "</ul></strong>";
                        }
                    }
                    $.alert({
                        title: 'Error!!',
                        type: 'red',
                        icon: 'fa fa-warning',
                        content: msg,
                    });
                }
            },

            
            "dataType": 'json',
            "columnDefs":
                    [
                        {className: "table-text", "targets": "_all"},
                        {
                            "targets": 0,
                            "data": "code",
                            "searchable": false,
                            "sortable": false
                        },
                        {
                            "targets": 1,
                            "data": "venue",
                            "searchable": false,
                            "sortable": false
                        },
                        {
                            "targets": 2,
                            "data": "name",
                            "searchable": false,
                            "sortable": false
                        },
                        {
                            "targets": 3,
                            "data": "party",
                            "searchable": false,
                            "sortable": false,
                        },
                        {
                            "targets": 4,
                            "data": "group",
                            "searchable": false,
                            "sortable": false,
                        },
                        {
                            "targets": 5,
                            "data": "PR_A",
                            "searchable": false,
                            "sortable": false,
                        },
                        {
                            "targets": 6,
                            "data": "PR_U",
                            "searchable": false,
                            "sortable": false,
                        },
                        {
                            "targets": 7,
                            "data": "P1_A",
                            "searchable": false,
                            "sortable": false,
                        },

                        {
                            "targets": 8,
                            "data": "P1_U",
                            "searchable": false,
                            "sortable": false,
                        },
                        {
                            "targets": 9,
                            "data": "P2_A",
                            "searchable": false,
                            "sortable": false,
                        },
                        {
                            "targets": 10,
                            "data": "P2_U",
                            "searchable": false,
                            "sortable": false,
                        },
                        {
                            "targets": 11,
                            "data": "P3_A",
                            "searchable": false,
                            "sortable": false,
                        },
                        {
                            "targets": 12,
                            "data": "P3_U",
                            "searchable": false,
                            "sortable": false,
                        },
                        {
                            "targets": 13,
                            "data": "PA_A",
                            "searchable": false,
                            "sortable": false,
                        },
                        {
                            "targets": 14,
                            "data": "PA_U",
                            "searchable": false,
                            "sortable": false,
                        },
                        {
                            "targets": 15,
                            "data": "PB_A",
                            "searchable": false,
                            "sortable": false,
                        },
                        {
                            "targets": 16,
                            "data": "PB_U",
                            "searchable": false,
                            "sortable": false,
                        },
                        {
                            "targets": 17,
                            "data": "TOT_A",
                            "searchable": false,
                            "sortable": false,
                        },
                        {
                            "targets": 18,
                            "data": "TOT_U",
                            "searchable": false,
                            "sortable": false,
                        },

                        {
                            "targets": 19,
                            "data": "TOT_m",
                            "searchable": false,
                            "sortable": false,
                        },

                        {
                            "targets": -1,
                            "data": 'action',
                            "searchable": false,
                            "sortable": false,
                            "render": function (data, type, full, meta) {
                                var str_btns = "";

                                str_btns += '<button type="button"  class="btn btn-danger  delete-button btn_new1" id="' + data.d + '" title="Delete"><i class="fa fa-trash"></i></button>';

                                return str_btns;
                            }
                        }
                    ],

            "order": [[1, 'asc']]
        });

        // table.columns( [-4,-3,-2,-1] ).visible( false );
    }
</script>
<!-- Copyright -->
@stop