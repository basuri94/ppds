@extends('layouts.master')
@section('content')

<!--// top-bar -->
<div class="wrapper">
    <!-- Sidebar Holder -->
    @include('layouts.sidebar')

    <!-- Page Content Holder -->
    <div id="content">           
        <section class="tables-section">
            <nav aria-label="breadcrumb" style="width:100%;">
                <div class="breadcrumb pagehead1">
                    <span class="headbrand"><i class="fa fa-desktop"></i> Add Assembly Subdivision Wise Priority
                        <span class="scoop-mcaret1"></span>
                    </span>&nbsp;&nbsp;
                    &nbsp;{!! Form::button(' List of Assembly Subdivision Wise Priority&nbsp;<i class="fa fa-arrow-circle-right"></i>',['id'=>'assembly_subdivision_wise_priority_list','class' => 'btn btn-primary-year add-new-button']) !!}  

                    <div class="clearfix"></div>
                </div>
            </nav>

            <!-- form -->
            <div class="outer-w3-agile">
                {!! Form::open(['url' => 'saveAssemblySubdivisionPriority', 'name' => 'saveAssemblySubdivisionPriority', 'class' =>'request-info clearfix form-horizontal', 'id' => 'saveAssemblySubdivisionPriority', 'method' => 'post','role'=>'','files' => true]) !!}
                {!! Form::hidden('districtcd', session()->get('districtcd_ppds'),['id'=>'districtcd']) !!}
                {!! Form::hidden('assembly_array', '',['class'=>'form-control','id'=>'assembly_array']) !!}
                {!! Form::hidden('subdivision_array', '',['class'=>'form-control','id'=>'subdivision_array']) !!}
                {!! Form::hidden('priority_array', '',['class'=>'form-control','id'=>'priority_array']) !!}
                
                <div class="panel-group" id="accordion5">                                                              
                    <div class="panel panel-default">
                        <div class="panel-heading1">
                            <h6 class="panel-title">
                                <a data-toggle="#" data-parent="#accordion5" href="#collapsePV"> <span class="fa fa-minus"></span> <span class="highlight">Add Group Training Venue</span></a>
                            </h6>
                        </div>
                        <div id="collapsePV" class="panel-collapse collapse5">
                            <div class="panel-body">                                   
                               


                                <div class="row"> 
                                    <div class='col-sm-12'>
                                        <div class="form-group">
                                            <table class="table table-bordered table-striped" style='border-top: 2px solid #4cae4c;' id="priority_allocation">
                                                <thead>
                                                    <tr style="background-color: #f5f8fa">
                                                        <th width='30%' class="highlight">Assembly Name <span style="color: red;">* </span><span style="color:red;font-size: 11px;"></span></th>
                                                        <th width='30%' class="highlight">Subdivision <span style="color: red;">* </span><span style="color:red;font-size: 11px;"></span></th>
                                                        <th width='30%' class="highlight">Priority <span style="color: red;">* </span><span style="color:red;font-size: 11px;"></span></th>
                                                        <th width='20%' class="highlight">Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr style="background-color: #f5f8fa">
                                                        <td>
                                                            @php
                                                                $assemblydata=App\tbl_assembly::select('assemblyname','assemblycd')->where('districtcd',session()->get('districtcd_ppds'))->get();
                                                            $subdivisiondata=App\tbl_subdivision::select('subdivision','subdivisioncd')->where('districtcd',session()->get('districtcd_ppds'))->get();
                                                           
                                                           @endphp
                                                            <select class="form-control" id="assembly" name="assembly">  
                                                                <option value="">[Select]</option>
                                                            @foreach ($assemblydata as $item)
                                                                <option value="{{$item->assemblycd}}">{{$item->assemblycd}} - {{$item->assemblyname}}</option>
                                                            @endforeach

                                                            </select>
                                                            
                                                        </td>
                                                        <td>
                                                            <select class="form-control" id="subdivision" name="subdivision">  
                                                                <option value="">[Select]</option>
                                                            @foreach ($subdivisiondata as $subdivisiondata)
                                                                <option value="{{$subdivisiondata->subdivisioncd}}">{{$subdivisiondata->subdivisioncd}} - {{$subdivisiondata->subdivision}}</option>
                                                            @endforeach

                                                            </select>
                                                            
                                                           </td>

                                                          <td> <select class="form-control" id="priorityset" name="priorityset">  
                                                            <option value="">[Select]</option>
                                                        @for ($i=1;$i<=9;$i++)
                                                            <option value="{{$i}}">{{$i}}</option>
                                                        @endfor

                                                        </select>
                                                    </td>
                                            
                                                        <td>   <a href='javascript:' class='add-row'><i class='fa fa-plus-circle' style='color: #17a2b8;cursor:pointer;font-size: 16.5px;padding-top: 10px;'></i>&nbsp;</a>  </td>

                                                    </tr> 
                                                </tbody>

                                            </table>
                                        </div>
                                    </div>

                                </div> 
                                <div class="row">  

                                   
                                    <div class='col-sm-12'>
                                        {!! Form::label('', '', ['class'=>'highlight']) !!}
                                        <div class="form-group text-right permit">                            	
                                            {{ Form::button('Submit', ['class' => 'btn btn-info', 'type' => 'submit','id'=>'Submit']) }}
                                            {{ Form::button('Reset', ['class' => 'btn btn-success' ,'type' => 'reset','id'=>'reset']) }}
                                        </div>
                                    </div>
                                </div> 

                            </div>                              
                        </div>
                    </div>  
                </div>
                {!! Form::close() !!}             
            </div>
            <!--// form -->

        </section>

    </div>
</div>
<!-- Copyright -->
<script type="text/javascript">
    $(document).ready(function () {
        $("#assembly_subdivision_wise_priority_list").click(function () {
            window.location.href = "assembly_subdivision_wise_priority_list";
        });
        char_num("#priorityset");
        $("#reset").click(function () {
            location.reload(true);
        });

        
        array_assembly = [];
        array_subdivision = [];
        array_duplicate = [];
        array_priority = [];

        change_subdivision();

        $(".add-row").click(function () {
            var assembly = $("#assembly").val();
            var priorityset = $("#priorityset").val();
            var subdivision=$('#subdivision').val();
            if (assembly == "" || priorityset == "" || subdivision=="") {
                $.alert({
                    title: 'Error!!',
                    type: 'red',
                    icon: 'fa fa-exclamation-triangle',
                    content: "Enter required field"
                });
                return false;
            }
            var asm_member = assembly;
            var sub_member = subdivision;
            var duplicate_asm = jQuery.inArray(asm_member, array_duplicate);
            var duplicate_subdiv = jQuery.inArray(sub_member, array_duplicate);
            console.log(duplicate_asm)
            console.log(duplicate_subdiv)
            if (duplicate_asm >= 0 && duplicate_subdiv>=0) {
                $.alert({
                    title: 'Error!!',
                    type: 'red',
                    icon: 'fa fa-exclamation-triangle',
                    content: 'Assembly and subdivison both duplicate not allow'
                });
                return false;
            }
            array_duplicate.push(subdivision,assembly);
            var sub_text_assembly = $("#assembly option:selected").text();
            var sub_text_subdivision = $("#subdivision option:selected").text();

            var markup = "<tr class='table_tr'><td>" + sub_text_assembly + "</td><td>" + sub_text_subdivision + "</td><td>" + priorityset + "</td><td><a href='javascript:' onclick='deleteRow(this);'><i class='fa fa-trash-alt' style='color:#f10418;cursor:pointer;font-size:16px;'></i></a></td></tr>";
            $("table tbody").append(markup);
            array_assembly.push(assembly);
            array_subdivision.push(subdivision);
            array_priority.push(priorityset);
            
            
            $("#priority_array").val(array_priority);
            $("#assembly_array").val(array_assembly);
            $("#subdivision_array").val(array_subdivision);
            $("#priorityset").val('');
        });
        $('#saveAssemblySubdivisionPriority').bootstrapValidator({

            message: 'This value is not valid',
            feedbackIcons: {
                valid: 'glyphicon',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {

                // subdivision: {
                //     validators: {
                //         notEmpty: {
                //             message: 'Subdivision is required'
                //         }
                //     }
                // },
                // venuename: {
                //     validators: {
                //         notEmpty: {
                //             message: 'Venue Name is required'
                //         }
                //     }
                // },
                // venueaddress: {
                //     validators: {
                //         notEmpty: {
                //             message: 'Venue Address is required'
                //         }
                //     }
                // }

            }

        }).on('success.form.bv', function (e) {
            e.preventDefault();
            var norowCount = $('#priority_allocation tr').length;
            if (norowCount == 2) {
                $.alert({
                    title: 'Error!!',
                    type: 'red',
                    icon: 'fa fa-exclamation-triangle',
                    content: 'Add atleast one record'
                });
                return false;
            }
            var action = $(this).attr('action');
            var type = $(this).attr('method');
            var districtcd = $("#districtcd").val();
           
            var priority_array = JSON.stringify(array_priority);
            var assembly_array = JSON.stringify(array_assembly);
            var subdivision_array = JSON.stringify(array_subdivision);
            var token = $("input[name='_token']").val();
            var fd = new FormData();
            fd.append('districtcd', districtcd);
            fd.append('_token', token);
            fd.append('priority_array', priority_array);
            fd.append('assembly_array', assembly_array);
            fd.append('subdivision_array', subdivision_array);
          
           
            
            $(".se-pre-con").fadeIn("slow");
            $.ajax({
                type: type,
                url: action,
                data: fd,
                processData: false,
                contentType: false,
                dataType: "json",
                success: function (data) {
                    $(".se-pre-con").fadeOut("slow");

                    if (data.status == 1) {
                        var msg = data.options+" Record(s) saved successfully";
                        $.confirm({
                            title: 'Success!',
                            type: 'green',
                            icon: 'fa fa-check',
                            content: msg,
                            buttons: {
                                ok: function () {
                                    location.reload(true);
                                }

                            }
                        });

                    } else {
                        var msg = data.options+" Record(s) updated successfully";
                        $.confirm({
                            title: 'Success!',
                            type: 'green',
                            icon: 'fa fa-check',
                            content: msg,
                            buttons: {
                                ok: function () {
                                    location.reload(true);
                                }

                            }
                        });

                    }
                    //$("#firstTrVenue")[0].reset();
                    // $('.table_tr').html("");


                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $(".se-pre-con").fadeOut("slow");
                    $('#Submit').removeAttr('disabled','true');
                    var msg = "";
                    if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                        msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                    } else {
                        if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                            msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                        } else {
                            msg += "Error(s):<strong><ul>";
                            $.each(jqXHR.responseJSON, function (key, value) {
                                msg += "<li>" + value + "</li>";
                            });
                            msg += "</ul></strong>";
                        }
                    }
                    $.alert({
                        title: 'Error!!',
                        type: 'red',
                        icon: 'fa fa-exclamation-triangle',
                        content: msg
                    });
                }
            });
        });
    });
    function deleteRow(row)
    {
        var i = row.parentNode.parentNode.rowIndex;

        document.getElementById('priority_allocation').deleteRow(i);
        var index_arr = parseInt(i) - parseInt(2);
        array_assembly.splice(index_arr, 1);
        array_priority.splice(index_arr, 1);
        array_subdivision.splice(index_arr, 1);
        array_duplicate.splice(index_arr, 1);
        var tot_sum = eval(array_subdivision.join("+"));
      
        $("#assembly_array").val(array_assembly);
        $("#subdivision_array").val(array_subdivision);
        $("#priority_array").val(array_priority);

    }
    function change_subdivision() {
        var forDist = $("#districtcd").val();
        var token = $("input[name='_token']").val();
        // $(".se-pre-con").fadeIn("slow");
        $.ajax({
            url: "getSubdivisionData",
            method: 'POST',
            data: {forDist: forDist, _token: token},
            success: function (data) {//alert(data.options);
                //   $(".se-pre-con").fadeOut("slow");
                $("select[name='subdivision'").html('');

                if (data.status == 1)
                {
                    $("select[name='subdivision'").append('<option value>[Select]</option>');
                    $.each(data.options, function (k, v)
                    {
                        $("select[name='subdivision'").append('<option value=' + k + '>' + v + '</option>');
                    });
                    //$("select[name='subdivision'").html(data.options);
                    // change_officeName();
                    // getMemberDetails();
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $(".se-pre-con").fadeOut("slow");
                var msg = "";
                if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                    msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                } else {
                    if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                        msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                    } else {
                        msg += "Error(s):<strong><ul>";
                        $.each(jqXHR.responseJSON, function (key, value) {
                            msg += "<li>" + value + "</li>";
                        });
                        msg += "</ul></strong>";
                    }
                }
                $.alert({
                    title: 'Error!!',
                    type: 'red',
                    icon: 'fa fa-exclamation-triangle',
                    content: msg
                });
            }
        });
    }

    
</script>
@stop