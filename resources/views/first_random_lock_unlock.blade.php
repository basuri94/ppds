@extends('layouts.master')
@section('content')

    <div class="wrapper">
        <!-- Sidebar Holder -->
        @include('layouts.sidebar')

        <!-- Page Content Holder -->
        <div id="content">
            <section class="tables-section">
                <nav aria-label="breadcrumb" style="width:100%;">
                    <div class="breadcrumb pagehead1">

                        {!! Form::button('<i class="fa fa-desktop"></i> First Randomisation (Lock/Unlock)', ['class' => 'btn
                        btn-primary-header add-new-button']) !!}

                        <div class="col-md-offset-8 pull-right">
                            <!--                {!!  Form::button('Academic Year:&nbsp;', ['id' => 'show_academic', 'class' => 'btn btn-primary-year']) !!}-->

                        </div>
                        <div class="clearfix"></div>
                    </div>
                </nav>

                <!-- form -->
                <div class="outer-w3-agile">
                    {!! Form::open(['url' => 'firstrand_lock_unlock', 'name' => 'firstrand_lock_unlock', 'class' =>
                    'request-info clearfix form-horizontal', 'id' => 'firstrand_lock_unlock', 'method' => 'post', 'role' =>
                    '', 'files' => true]) !!}
                    {!! Form::hidden('districtcd', session()->get('districtcd_ppds'), ['id' => 'districtcd']) !!}

                    {!! Form::hidden('status', 0, ['id' => 'status']) !!}
                    <div class="panel-group" id="accordion5">
                        <div class="panel panel-default">
                            <div class="panel-heading1">
                                <h6 class="panel-title">
                                    <a data-toggle="#" data-parent="#accordion5" href="#collapseUV"> <span
                                            class="fa fa-minus"></span> <span class="highlight">Zone-wise
                                            Assembly</span></a>
                                </h6>
                            </div>
                            <div id="collapseUV" class="panel-collapse collapse5">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class='col-sm-4'>
                                            {!! Form::label('zone', 'Zone:', ['class' => 'highlight required']) !!}
                                            <div class="form-group">
                                                <div class=''>
                                                    {!! Form::select('zone', ['' => '[Select]'], null, ['id' => 'zone',
                                                    'class' => 'form-control', 'autocomplete' => 'off']) !!}
                                                </div>
                                            </div>
                                        </div>
                                        {{-- <div class='col-sm-4'>
                                            {!! Form::label('pc', 'Parliament Constituency:', ['class' => 'highlight
                                            required']) !!}
                                            <!--<span class="highlight">Sub Division</span> -->
                                            <div class="form-group">
                                                <div class=''>
                                                    {!! Form::select('pc', ['' => '[Select]'], null, ['id' => 'pc', 'class'
                                                    => 'form-control', 'autocomplete' => 'off']) !!}
                                                </div>
                                            </div>
                                        </div> --}}
                                        <div class='col-sm-4'>
                                            {!! Form::label('phase', 'Phase:', ['class' => 'highlight ']) !!}
                                            <div class="form-group">
                                                <div class=''>

                                                    <select id="phase" class="form-control" name="phase">

                                                        <option value="">[Select]</option>


                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class='col-sm-12'>
                                            <div id='assemblyDetails'></div>
                                        </div>
                                    </div>

                                    <div class="row" id="rw" style="display: none;">
                                        <div class='col-sm-12'>
                                            <div class="form-group text-right permit">
                                                {{ Form::button('Submit', ['class' => 'btn btn-info', 'type' => 'submit', 'id' => 'submit']) }}
                                                {{ Form::button('Reset', ['class' => 'btn btn-success', 'type' => 'reset', 'id' => 'reset']) }}
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                    {!! Form::close() !!}
                </div>
                <!--// form -->


            </section>

        </div>
    </div>
    <!-- Copyright -->
    <script type="text/javascript">
        $(document).ready(function() {
            getZoneDetails();
            $("#reset").click(function() {
                location.reload(true);
            });
            $("#zone").change(function() {

                getZonePhasedata();
            });
            $("#phase").change(function() {
                getZonePhaseWiseAssembly();
            });
            // $("#pc").change(function() {
            //     getZoneWiseAssembly();
            // });
            $('#firstrand_lock_unlock')
                .bootstrapValidator({
                    message: 'This value is not valid',
                    feedbackIcons: {
                        valid: 'fa fa-check',
                        invalid: 'fa fa-times',
                        validating: 'fa fa-refresh'
                    },
                    fields: {
                        zone: {
                            validators: {
                                notEmpty: {
                                    message: 'Zone is required'
                                }
                            }
                        }
                    }
                }).on('success.form.bv', function(e) {
                    // Prevent form submission
                    e.preventDefault();
                    var action = $(this).attr('action');
                    var type = $(this).attr('method');
                    var formData = new FormData($(this)[0]);
                    // var operation = "save";
                    //  var operated = "saved";
                    // $(".se-pre-con").fadeIn("slow");
                    var ostatus = $("#status").val();
                    if (ostatus == 1) {
                        var table_edit =
                            '<form id="firstrand_lock_unlock" action="firstrand_lock_unlock" method="post" class="form-horizontal">';
                        table_edit += '<div class="form-group">';
                        // table_edit += '<label class="control-label col-md-4 required" style="font-size:12px;">Enter Password:</label>';
                        table_edit +=
                            '<div class="col-md-11"><input type="password" class="form-control" id="password" name="password" style="margin-bottom: 2px;" /></div>';
                        table_edit += '</div>';
                        table_edit += '</form>';
                        var jc = $.confirm({
                            title: '<span style="font-size: 17px;">Enter Lock/Unlock Password</span>',
                            content: table_edit,
                            type: 'green',
                            icon: 'fa fa-edit',
                            typeAnimated: true,
                            buttons: {
                                proceed: {
                                    btnClass: 'btn-primary',
                                    action: function() {
                                        var flag = 0;
                                        var check = $("#password").val();
                                        if (check == '') {
                                            $.alert({
                                                title: 'Error!!',
                                                type: 'red',
                                                icon: 'fa fa-exclamation-triangle',
                                                content: "Please enter password"
                                            });
                                            flag = 1;
                                            return false;
                                        }
                                        if (flag > 0) {
                                            return false;
                                        }
                                        jc.showLoading(true);
                                        var password = $("#password").val();

                                        $.ajax({
                                            type: 'post',
                                            url: 'check_for_lu_password',
                                            data: {
                                                'password': password,
                                                'type': 1,
                                                '_token': $('input[name="_token"]').val()
                                            },
                                            dataType: 'json',
                                            success: function(datam) {
                                                //  $(".se-pre-con").fadeOut("slow");
                                                if (datam.options > 0) {
                                                    return $.ajax({
                                                        type: type,
                                                        url: action,
                                                        data: formData,
                                                        processData: false,
                                                        contentType: false

                                                    }).done(function(response) {
                                                        if (response.status ==
                                                            1) {
                                                            jc.hideLoading(
                                                            true);
                                                            var msg = response
                                                                .options +
                                                                " Assembly(s) changed successfully";
                                                            if (response
                                                                .totalC > 0) {
                                                                msg +=
                                                                    "<br>Please do Randomisation before locking <span style='color:red;'>" +
                                                                    response
                                                                    .totalC +
                                                                    "</span> Assembly(s)";
                                                            }
                                                            $.confirm({
                                                                title: 'Success!',
                                                                type: 'green',
                                                                icon: 'fa fa-check',
                                                                content: msg,
                                                                buttons: {
                                                                    ok: function() {
                                                                        // location.reload(true);
                                                                        getZonePhaseWiseAssembly
                                                                            ();
                                                                        $("#submit")
                                                                            .attr(
                                                                                'disabled',
                                                                                false
                                                                                );
                                                                        $("#rw")
                                                                            .hide();

                                                                    }

                                                                }
                                                            });
                                                        }
                                                    }).fail(function(jqXHR,
                                                        textStatus, errorThrown
                                                        ) {
                                                        var msg = "";
                                                        if (jqXHR.status !==
                                                            422 && jqXHR
                                                            .status !== 400) {
                                                            msg += "<strong>" +
                                                                jqXHR.status +
                                                                ": " +
                                                                errorThrown +
                                                                "</strong>";
                                                        } else {
                                                            if (jqXHR
                                                                .responseJSON
                                                                .hasOwnProperty(
                                                                    'exception')
                                                                ) {
                                                                if (jqXHR
                                                                    .responseJSON
                                                                    .exception_code ==
                                                                    23000) {
                                                                    msg +=
                                                                        "Check Password.";
                                                                } else {
                                                                    msg +=
                                                                        "Exception: <strong>" +
                                                                        jqXHR
                                                                        .responseJSON
                                                                        .exception_message +
                                                                        "</strong>";
                                                                }
                                                            } else {
                                                                msg +=
                                                                    "Error(s):<strong><ul>";
                                                                $.each(jqXHR
                                                                    .responseJSON,
                                                                    function(
                                                                        key,
                                                                        value
                                                                        ) {
                                                                        msg +=
                                                                            "<li>" +
                                                                            value +
                                                                            "</li>";
                                                                    });
                                                                msg +=
                                                                    "</ul></strong>";
                                                            }
                                                        }
                                                        $.alert({
                                                            title: 'Error!!',
                                                            type: 'red',
                                                            icon: 'fa fa-exclamation-triangle',
                                                            content: msg,
                                                        });
                                                    });
                                                } else {
                                                    $("#submit").attr('disabled',
                                                    false);
                                                    $.alert({
                                                        title: 'Error!!',
                                                        type: 'red',
                                                        icon: 'fa fa-exclamation-triangle',
                                                        content: "Please enter correct password"
                                                    });

                                                }
                                            },
                                            error: function(jqXHR, textStatus,
                                            errorThrown) {
                                                var msg = "";
                                                if (jqXHR.status !== 422 && jqXHR
                                                    .status !== 400) {
                                                    msg += "<strong>" + jqXHR.status +
                                                        ": " + errorThrown +
                                                        "</strong>";
                                                } else {
                                                    if (jqXHR.responseJSON
                                                        .hasOwnProperty('exception')) {
                                                        msg += "Exception: <strong>" +
                                                            jqXHR.responseJSON
                                                            .exception_message +
                                                            "</strong>";
                                                    } else {
                                                        msg += "Error(s):<strong><ul>";
                                                        $.each(jqXHR.responseJSON,
                                                            function(key, value) {
                                                                msg += "<li>" +
                                                                    value + "</li>";
                                                            });
                                                        msg += "</ul></strong>";
                                                    }
                                                }
                                                $.alert({
                                                    title: 'Error!!',
                                                    type: 'red',
                                                    icon: 'fa fa-warning',
                                                    content: msg
                                                });
                                                $("#submit").attr('disabled', false);
                                            }
                                        });


                                    }
                                },
                                close: function() {
                                    $("#submit").attr('disabled', false);
                                }
                            },
                            onContentReady: function() {
                                // bind to events

                            },

                            onOpen: function() {
                                // after the modal is displayed.
                                //startTimer();
                            }
                        });
                    } else {
                        $(".se-pre-con").fadeIn("slow");
                        $.ajax({
                            type: 'post',
                            url: 'send_otp_password',
                            data: {
                                'otp_status': 'lock',
                                'type': 1,
                                '_token': $('input[name="_token"]').val()
                            },
                            dataType: 'json',
                            success: function(datam) {
                                $(".se-pre-con").fadeOut("slow");
                                if (datam.status > 0) {
                                    if (datam.options > 0) {
                                        var jc = $.confirm({
                                            title: '<span style="font-size: 17px;">Enter Lock/Unlock OTP</span>',
                                            content: '<div class="form-group"><div class="col-md-11"><input type="text" class="form-control" id="otp" name="otp"  autocomplete="off" maxlength="4"></div></div>',
                                            type: 'green',
                                            icon: 'fa fa-mobile',
                                            typeAnimated: true,
                                            buttons: {
                                                resend: {
                                                    btnClass: 'btn-danger',
                                                    action: function() {
                                                        $(".se-pre-con").fadeIn("slow");
                                                        $.ajax({
                                                            type: 'post',
                                                            url: 'resend_otp_password',
                                                            data: {
                                                                'otp_status': 'lock',
                                                                'type': 1,
                                                                '_token': $(
                                                                    'input[name="_token"]'
                                                                    ).val()
                                                            },
                                                            dataType: 'json',
                                                            success: function(
                                                                data) {
                                                                $(".se-pre-con")
                                                                    .fadeOut(
                                                                        "slow"
                                                                        );
                                                                jc.open(
                                                                    true);
                                                            },
                                                            error: function(
                                                                jqXHR,
                                                                textStatus,
                                                                errorThrown
                                                                ) {
                                                                $(".se-pre-con")
                                                                    .fadeOut(
                                                                        "slow"
                                                                        );
                                                                var msg =
                                                                "";
                                                                if (jqXHR
                                                                    .status !==
                                                                    422 &&
                                                                    jqXHR
                                                                    .status !==
                                                                    400) {
                                                                    msg +=
                                                                        "<strong>" +
                                                                        jqXHR
                                                                        .status +
                                                                        ": " +
                                                                        errorThrown +
                                                                        "</strong>";
                                                                } else {
                                                                    if (jqXHR
                                                                        .responseJSON
                                                                        .hasOwnProperty(
                                                                            'exception'
                                                                            )
                                                                        ) {
                                                                        msg +=
                                                                            "Exception: <strong>" +
                                                                            jqXHR
                                                                            .responseJSON
                                                                            .exception_message +
                                                                            "</strong>";
                                                                    } else {
                                                                        msg +=
                                                                            "Error(s):<strong><ul>";
                                                                        $.each(jqXHR
                                                                            .responseJSON,
                                                                            function(
                                                                                key,
                                                                                value
                                                                                ) {
                                                                                msg +=
                                                                                    "<li>" +
                                                                                    value +
                                                                                    "</li>";
                                                                            }
                                                                            );
                                                                        msg +=
                                                                            "</ul></strong>";
                                                                    }
                                                                }
                                                                $.alert({
                                                                    title: 'Error!!',
                                                                    type: 'red',
                                                                    icon: 'fa fa-warning',
                                                                    content: msg,
                                                                });
                                                                $("#submit")
                                                                    .attr(
                                                                        'disabled',
                                                                        false
                                                                        );
                                                            }
                                                        });
                                                    }
                                                },
                                                next: {
                                                    btnClass: 'btn-primary',
                                                    action: function() {
                                                        var flag = 0;
                                                        var check = $("#otp").val();
                                                        if (check == '') {
                                                            $.alert({
                                                                title: 'Error!!',
                                                                type: 'red',
                                                                icon: 'fa fa-exclamation-triangle',
                                                                content: "Please enter OTP"
                                                            });
                                                            flag = 1;
                                                            return false;
                                                        }
                                                        if (flag > 0) {
                                                            return false;
                                                        }
                                                        jc.showLoading(true);
                                                        var otp = $("#otp").val();

                                                        $.ajax({
                                                            type: 'post',
                                                            url: 'check_for_lu_rand_otp',
                                                            data: {
                                                                'otp_status': 'lock',
                                                                'otp': otp,
                                                                'type': 1,
                                                                '_token': $(
                                                                    'input[name="_token"]'
                                                                    ).val()
                                                            },
                                                            dataType: 'json',
                                                            success: function(
                                                                datam) {
                                                                //  $(".se-pre-con").fadeOut("slow");
                                                                if (datam
                                                                    .options >
                                                                    0) {
                                                                    return $
                                                                        .ajax({
                                                                            type: type,
                                                                            url: action,
                                                                            data: formData,
                                                                            processData: false,
                                                                            contentType: false

                                                                        })
                                                                        .done(
                                                                            function(
                                                                                response
                                                                                ) {
                                                                                if (response
                                                                                    .status ==
                                                                                    1
                                                                                    ) {
                                                                                    jc.hideLoading(
                                                                                        true
                                                                                        );
                                                                                    var msg =
                                                                                        response
                                                                                        .options +
                                                                                        " Assembly(s) changed successfully";
                                                                                    if (response
                                                                                        .totalC >
                                                                                        0
                                                                                        ) {
                                                                                        msg +=
                                                                                            "<br>Please do Randomisation before locking <span style='color:red;'>" +
                                                                                            response
                                                                                            .totalC +
                                                                                            "</span> Assembly(s)";
                                                                                    }
                                                                                    $.confirm({
                                                                                        title: 'Success!',
                                                                                        type: 'green',
                                                                                        icon: 'fa fa-check',
                                                                                        content: msg,
                                                                                        buttons: {
                                                                                            ok: function() {
                                                                                                // location.reload(true);
                                                                                                getZonePhaseWiseAssembly
                                                                                                    ();
                                                                                                $("#submit")
                                                                                                    .attr(
                                                                                                        'disabled',
                                                                                                        false
                                                                                                        );
                                                                                                $("#rw")
                                                                                                    .hide();

                                                                                            }

                                                                                        }
                                                                                    });
                                                                                }
                                                                            }
                                                                            )
                                                                        .fail(
                                                                            function(
                                                                                jqXHR,
                                                                                textStatus,
                                                                                errorThrown
                                                                                ) {
                                                                                var msg =
                                                                                    "";
                                                                                if (jqXHR
                                                                                    .status !==
                                                                                    422 &&
                                                                                    jqXHR
                                                                                    .status !==
                                                                                    400
                                                                                    ) {
                                                                                    msg +=
                                                                                        "<strong>" +
                                                                                        jqXHR
                                                                                        .status +
                                                                                        ": " +
                                                                                        errorThrown +
                                                                                        "</strong>";
                                                                                } else {
                                                                                    if (jqXHR
                                                                                        .responseJSON
                                                                                        .hasOwnProperty(
                                                                                            'exception'
                                                                                            )
                                                                                        ) {
                                                                                        if (jqXHR
                                                                                            .responseJSON
                                                                                            .exception_code ==
                                                                                            23000
                                                                                            ) {
                                                                                            msg +=
                                                                                                "Check Password.";
                                                                                        } else {
                                                                                            msg +=
                                                                                                "Exception: <strong>" +
                                                                                                jqXHR
                                                                                                .responseJSON
                                                                                                .exception_message +
                                                                                                "</strong>";
                                                                                        }
                                                                                    } else {
                                                                                        msg +=
                                                                                            "Error(s):<strong><ul>";
                                                                                        $.each(jqXHR
                                                                                            .responseJSON,
                                                                                            function(
                                                                                                key,
                                                                                                value
                                                                                                ) {
                                                                                                msg +=
                                                                                                    "<li>" +
                                                                                                    value +
                                                                                                    "</li>";
                                                                                            }
                                                                                            );
                                                                                        msg +=
                                                                                            "</ul></strong>";
                                                                                    }
                                                                                }
                                                                                $.alert({
                                                                                    title: 'Error!!',
                                                                                    type: 'red',
                                                                                    icon: 'fa fa-exclamation-triangle',
                                                                                    content: msg,
                                                                                });
                                                                            }
                                                                            );
                                                                } else {
                                                                    $("#submit")
                                                                        .attr(
                                                                            'disabled',
                                                                            false
                                                                            );
                                                                    $.alert({
                                                                        title: 'Error!!',
                                                                        type: 'red',
                                                                        icon: 'fa fa-exclamation-triangle',
                                                                        content: "Please enter correct OTP"
                                                                    });

                                                                }
                                                            },
                                                            error: function(
                                                                jqXHR,
                                                                textStatus,
                                                                errorThrown
                                                                ) {
                                                                var msg =
                                                                "";
                                                                if (jqXHR
                                                                    .status !==
                                                                    422 &&
                                                                    jqXHR
                                                                    .status !==
                                                                    400) {
                                                                    msg +=
                                                                        "<strong>" +
                                                                        jqXHR
                                                                        .status +
                                                                        ": " +
                                                                        errorThrown +
                                                                        "</strong>";
                                                                } else {
                                                                    if (jqXHR
                                                                        .responseJSON
                                                                        .hasOwnProperty(
                                                                            'exception'
                                                                            )
                                                                        ) {
                                                                        msg +=
                                                                            "Exception: <strong>" +
                                                                            jqXHR
                                                                            .responseJSON
                                                                            .exception_message +
                                                                            "</strong>";
                                                                    } else {
                                                                        msg +=
                                                                            "Error(s):<strong><ul>";
                                                                        $.each(jqXHR
                                                                            .responseJSON,
                                                                            function(
                                                                                key,
                                                                                value
                                                                                ) {
                                                                                msg +=
                                                                                    "<li>" +
                                                                                    value +
                                                                                    "</li>";
                                                                            }
                                                                            );
                                                                        msg +=
                                                                            "</ul></strong>";
                                                                    }
                                                                }
                                                                $.alert({
                                                                    title: 'Error!!',
                                                                    type: 'red',
                                                                    icon: 'fa fa-warning',
                                                                    content: msg
                                                                });
                                                                $("#submit")
                                                                    .attr(
                                                                        'disabled',
                                                                        false
                                                                        );
                                                            }
                                                        });
                                                    }
                                                },
                                                close: function() {
                                                    $("#submit").attr('disabled',
                                                    false);
                                                }
                                            },
                                            onOpen: function() {
                                                // after the modal is displayed.
                                                startTimer();
                                            }
                                        });

                                        function startTimer() {
                                            var counter = 30;
                                            setInterval(function() {
                                                counter--;
                                                if (counter >= 0) {
                                                    jc.buttons.resend.disable();
                                                    jc.buttons.resend.setText(counter +
                                                        " Sec Remaining");
                                                }
                                                if (counter === 0) {
                                                    jc.buttons.resend.enable();
                                                    jc.buttons.resend.removeClass(
                                                        "btn-danger");
                                                    jc.buttons.resend.setText("Resend OTP");
                                                    jc.buttons.resend.addClass(
                                                        "btn-success");
                                                }
                                            }, 1000);
                                        }
                                    }
                                } else {
                                    $("#submit").attr('disabled', false);
                                    $.alert({
                                        title: 'Error!!',
                                        type: 'red',
                                        icon: 'fa fa-exclamation-triangle',
                                        content: "Please reload the page.Check database"
                                    });

                                }
                            },
                            error: function(jqXHR, textStatus, errorThrown) {
                                var msg = "";
                                if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                                    msg += "<strong>" + jqXHR.status + ": " + errorThrown +
                                        "</strong>";
                                } else {
                                    if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                                        msg += "Exception: <strong>" + jqXHR.responseJSON
                                            .exception_message + "</strong>";
                                    } else {
                                        msg += "Error(s):<strong><ul>";
                                        $.each(jqXHR.responseJSON, function(key, value) {
                                            msg += "<li>" + value + "</li>";
                                        });
                                        msg += "</ul></strong>";
                                    }
                                }
                                $.alert({
                                    title: 'Error!!',
                                    type: 'red',
                                    icon: 'fa fa-warning',
                                    content: msg
                                });
                                $("#submit").attr('disabled', false);
                            }
                        });

                    }

                    //         $.ajax({
                    //            type: type,
                    //                    url: action,
                    //                    data: formData,
                    //                    processData: false,
                    //                    contentType: false,
                    //                    dataType: "json",
                    //                    success: function (data) {
                    //
                    //                        if(data.status==1)
                    //                        {
                    //                          $(".se-pre-con").fadeOut("slow");            
                    //                          var msg=data.options+" Record(s) changed successfully";
                    //                           $.confirm({
                    //                                title: 'Success!',
                    //                                type: 'green',
                    //                                icon: 'fa fa-check',
                    //                                content: msg,
                    //                                buttons: {
                    //                                    ok: function () {
                    //                                       // location.reload(true);
                    //                                       getZonePhaseWiseAssembly();
                    //                                       $("#submit").attr('disabled',false);
                    //                                        $("#rw").hide();
                    //                                        
                    //                                    }
                    //
                    //                                }
                    //                            });
                    //                          }              
                    //
                    //                    },
                    //                    error: function (jqXHR, textStatus, errorThrown) {
                    //                        $(".se-pre-con").fadeOut("slow");
                    //                        var msg = "";
                    //                        if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                    //                            msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                    //                        } else {
                    //                            if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                    //                                msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                    //                            } else {
                    //                                msg += "Error(s):<strong><ul>";
                    //                                $.each(jqXHR.responseJSON, function (key, value) {
                    //                                    msg += "<li>" + value + "</li>";
                    //                                });
                    //                                msg += "</ul></strong>";
                    //                            }
                    //                        }
                    //                        $.alert({
                    //                            title: 'Error!!',
                    //                            type: 'red',
                    //                            icon: 'fa fa-warning',
                    //                            content: msg
                    //                        });
                    //                    }
                    ////                    complete: function() {
                    ////                         getZonePhaseWiseAssembly();
                    ////                    }
                    //            });

                });

        });

        function getZonePhaseWiseAssembly() {
            var forDist = $("#districtcd").val();
            var zone = $('#zone').val();
            var phase = $('#phase').val();
            var token = $("input[name='_token']").val();
            $("#rw").hide();
            $(".se-pre-con").fadeIn("slow");
            $.ajax({
                type: 'POST',
                url: 'firstzone_phase_wise_assembly',
                data: {
                    forDist: forDist,
                    zone: zone,
                    phase: phase,
                    _token: token
                },
                dataType: 'json',
                success: function(response) {
                    $(".se-pre-con").fadeOut("slow");
                    $("#assemblyDetails").html("");
                    var str = '<input type="hidden" id="row_count" name="row_count" value="' + response.options
                        .length + '"> '
                    str +=
                        "<table class='table table-bordered table-striped' width='100%' style='border-top: 2px solid #4cae4c;'>";
                    str +=
                        ' <tr style="background-color: #f5f8fa"><th align="left">&nbsp;<input type="checkbox" id="myCheckall" name="myCheckall"  value="1"  class="myCheckall">&nbsp;</th><th align="left">&nbsp;Assembly Code</th><th>&nbsp;Assembly Name </th><th>&nbsp;Action Taken </th></tr>';
                    //         str += '<tr style=" background-color: #1ABB9C; color: #FFFFFF; font-weight: bold;"><td align="left"><input type="checkbox" id="myCheckall" name="myCheckall"  value="1"  class="submenu">&nbsp; Check All</td><td align="center" width="30%"><input type="checkbox" id="view_all" class="flat"></td></tr>';
                    var sub_length = response.options.length;
                    // alert(sub_length);
                    for (var i = 0; i < sub_length; i++) {
                        var c = parseInt(i) + 1;
                        str += '<tr><td align="left" width="5%">&nbsp;';

                        str += '<input type="checkbox" id="myCheck' + c + '" name="myCheck' + c + '"  value="' +
                            response.options[i].assemblycd + "-" + response.options[i].rand_status1 +
                            '"  class="submenu" >';
                        str += '</td><td align="left" width="20%">&nbsp;' + response.options[i].assemblycd +
                            '</td>';
                        str += '</td><td align="left" width="60%">&nbsp;' + response.options[i].assemblyname +
                            '</td>';
                        str += '<td align="left" width="15%">&nbsp;';
                        if (response.options[i].rand_status1 == 'Y') {
                            str += "<span style='color:red;'>Locked</span>";

                        } else if (response.options[i].rand_status1 == 'A') {
                            str += "<span style='color:green;'>Done</span>";
                        } else {
                            str += "<span style='color:green;'>Unlocked</span>";
                        }
                        str += '</td>';

                        /* str+='<td align="left" width="15%">&nbsp;';
                         var status_l=response.options[i].status.length;
                         for(var j=0;j<status_l;j++){
                         str+=response.options[i].status[j].total;
                         }
                         str+='</td>';*/
                        str += '</tr>'; // return empty
                    }
                    str += "</table>";
                    $("#assemblyDetails").append(str);

                    $("#myCheckall").change(function() {
                        var status = this.checked;
                        if (status == true) {
                            $("#rw").show();
                        } else if (status == false) {
                            $("#rw").hide();
                        }

                        $('.submenu').each(function() {
                            this.checked = status;
                        });
                    });
                    $('.submenu').change(function() {
                        if ($('.submenu:checked').length == '0') {
                            $("#rw").hide();
                        } else {
                            $("#rw").show();

                        }
                        if (this.checked == false) {
                            $("#myCheckall")[0].checked = false;
                        }
                        if ($('.submenu:checked').length == $('.submenu').length) {
                            $("#myCheckall")[0].checked = true;
                        }
                    });
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $(".se-pre-con").fadeOut("slow");
                    var msg = "";
                    if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                        msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                    } else {
                        if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                            msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                        } else {
                            msg += "Error(s):<strong><ul>";
                            $.each(jqXHR.responseJSON, function(key, value) {
                                msg += "<li>" + value + "</li>";
                            });
                            msg += "</ul></strong>";
                        }
                    }
                    $.alert({
                        title: 'Error!!',
                        type: 'red',
                        icon: 'fa fa-warning',
                        content: msg
                    });
                }
            });
        }

        function getZoneWiseAssembly() {
            var forDist = $("#districtcd").val();
            var zone = $('#zone').val();
            var pc = $('#pc').val();
            var token = $("input[name='_token']").val();
            $("#rw").hide();
            $(".se-pre-con").fadeIn("slow");
            $.ajax({
                type: 'POST',
                url: 'firstzone_wise_assembly',
                data: {
                    forDist: forDist,
                    zone: zone,
                    pc: pc,
                    _token: token
                },
                dataType: 'json',
                success: function(response) {
                    $(".se-pre-con").fadeOut("slow");
                    $("#assemblyDetails").html("");
                    var str = '<input type="hidden" id="row_count" name="row_count" value="' + response.options
                        .length + '"> '
                    str +=
                        "<table class='table table-bordered table-striped' width='100%' style='border-top: 2px solid #4cae4c;'>";
                    str +=
                        ' <tr style="background-color: #f5f8fa"><th align="left">&nbsp;<input type="checkbox" id="myCheckall" name="myCheckall"  value="1"  class="myCheckall">&nbsp;</th><th align="left">&nbsp;Assembly Code</th><th>&nbsp;Assembly Name </th><th>&nbsp;Action Taken </th></tr>';
                    //         str += '<tr style=" background-color: #1ABB9C; color: #FFFFFF; font-weight: bold;"><td align="left"><input type="checkbox" id="myCheckall" name="myCheckall"  value="1"  class="submenu">&nbsp; Check All</td><td align="center" width="30%"><input type="checkbox" id="view_all" class="flat"></td></tr>';
                    var sub_length = response.options.length;
                    // alert(sub_length);
                    for (var i = 0; i < sub_length; i++) {
                        var c = parseInt(i) + 1;
                        str += '<tr><td align="left" width="5%">&nbsp;';

                        str += '<input type="checkbox" id="myCheck' + c + '" name="myCheck' + c + '"  value="' +
                            response.options[i].assemblycd + "-" + response.options[i].rand_status1 +
                            '"  class="submenu" >';
                        str += '</td><td align="left" width="20%">&nbsp;' + response.options[i].assemblycd +
                            '</td>';
                        str += '</td><td align="left" width="60%">&nbsp;' + response.options[i].assemblyname +
                            '</td>';
                        str += '<td align="left" width="15%">&nbsp;';
                        if (response.options[i].rand_status1 == 'Y') {
                            str += "<span style='color:red;'>Locked</span>";

                        } else if (response.options[i].rand_status1 == 'A') {
                            str += "<span style='color:green;'>Done</span>";
                        } else {
                            str += "<span style='color:green;'>Unlocked</span>";
                        }
                        str += '</td>';

                        /* str+='<td align="left" width="15%">&nbsp;';
                         var status_l=response.options[i].status.length;
                         for(var j=0;j<status_l;j++){
                         str+=response.options[i].status[j].total;
                         }
                         str+='</td>';*/
                        str += '</tr>'; // return empty
                    }
                    str += "</table>";
                    $("#assemblyDetails").append(str);

                    $("#myCheckall").change(function() {
                        var status = this.checked;
                        if (status == true) {
                            $("#rw").show();
                        } else if (status == false) {
                            $("#rw").hide();
                        }

                        $('.submenu').each(function() {
                            this.checked = status;
                        });
                    });
                    $('.submenu').change(function() {
                        if ($('.submenu:checked').length == '0') {
                            $("#rw").hide();
                        } else {
                            $("#rw").show();

                        }
                        if (this.checked == false) {
                            $("#myCheckall")[0].checked = false;
                        }
                        if ($('.submenu:checked').length == $('.submenu').length) {
                            $("#myCheckall")[0].checked = true;
                        }
                    });
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $(".se-pre-con").fadeOut("slow");
                    var msg = "";
                    if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                        msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                    } else {
                        if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                            msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                        } else {
                            msg += "Error(s):<strong><ul>";
                            $.each(jqXHR.responseJSON, function(key, value) {
                                msg += "<li>" + value + "</li>";
                            });
                            msg += "</ul></strong>";
                        }
                    }
                    $.alert({
                        title: 'Error!!',
                        type: 'red',
                        icon: 'fa fa-warning',
                        content: msg
                    });
                }
            });
        }

        function getZoneDetails() {
            var forDist = $("#districtcd").val();
            var token = $("input[name='_token']").val();
            $(".se-pre-con").fadeIn("slow");
            $.ajax({
                url: "getZoneDetails",
                method: 'POST',
                data: {
                    forDist: forDist,
                    _token: token
                },
                success: function(data) { //alert(data.options);
                    $(".se-pre-con").fadeOut("slow");
                    $("select[name='zone'").html('');
                    if (data.status == 1) {
                        $("select[name='zone'").append('<option value>[Select]</option>');
                        $.each(data.options, function(k, v) {
                            $("select[name='zone'").append('<option value=' + k + '>' + v +
                            '</option>');
                        });
                        // getMemberDetails();
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $(".se-pre-con").fadeOut("slow");
                    var msg = "";
                    if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                        msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                    } else {
                        if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                            msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                        } else {
                            msg += "Error(s):<strong><ul>";
                            $.each(jqXHR.responseJSON, function(key, value) {
                                msg += "<li>" + value + "</li>";
                            });
                            msg += "</ul></strong>";
                        }
                    }
                    $.alert({
                        title: 'Error!!',
                        type: 'red',
                        icon: 'fa fa-warning',
                        content: msg
                    });
                },
                complete: function() {
                    check_for_status_password();
                }
            });
        }

        function check_for_status_password() {
            var districtcd = $("#districtcd").val();
            var type = '1';
            var token = $("input[name='_token']").val();
            $.ajax({
                url: "check_for_status_password",
                method: 'POST',
                data: {
                    districtcd: districtcd,
                    type: type,
                    _token: token
                },
                success: function(data) { //alert(data.options);
                    if (data.status == 1) {
                        $("#status").val(data.options);

                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    $(".se-pre-con").fadeOut("slow");
                    var msg = "";
                    if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                        msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                    } else {
                        if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                            msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                        } else {
                            msg += "Error(s):<strong><ul>";
                            $.each(jqXHR.responseJSON, function(key, value) {
                                msg += "<li>" + value + "</li>";
                            });
                            msg += "</ul></strong>";
                        }
                    }
                    $.alert({
                        title: 'Error!!',
                        type: 'red',
                        icon: 'fa fa-warning',
                        content: msg
                    });
                }
            });
        }

        function getZoneWisePC() {
            var zone = $("#zone").val();
            $("#rw").hide();
            var token = $("input[name='_token']").val();
            // $(".se-pre-con").fadeIn("slow");
            $.ajax({
                url: "getZonePCdata",
                method: 'POST',
                data: {
                    zone: zone,
                    _token: token
                },
                success: function(data) { //alert(data.options);
                    //   $(".se-pre-con").fadeOut("slow");
                    $("select[name='pc'").html('');

                    if (data.status == 1) {
                        $("select[name='pc'").append('<option value>[Select]</option>');
                        $.each(data.options, function(k, v) {
                            $("select[name='pc'").append('<option value=' + k + '>' + v + '</option>');
                        });
                        //$("select[name='subdivision'").html(data.options);
                        // change_officeName();

                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    //  $(".se-pre-con").fadeOut("slow");
                    var msg = "";
                    if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                        msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                    } else {
                        if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                            msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                        } else {
                            msg += "Error(s):<strong><ul>";
                            $.each(jqXHR.responseJSON, function(key, value) {
                                msg += "<li>" + value + "</li>";
                            });
                            msg += "</ul></strong>";
                        }
                    }
                    $.alert({
                        title: 'Error!!',
                        type: 'red',
                        icon: 'fa fa-warning',
                        content: msg
                    });
                },
                complete: function() {
                    getZoneWiseAssembly();
                }
            });
        }

        function getZonePhasedata() {
            var zone = $("#zone").val();
            $("#rw").hide();
            var token = $("input[name='_token']").val();
            // $(".se-pre-con").fadeIn("slow");
            $.ajax({
                url: "getZonePhasedata",
                method: 'POST',
                data: {
                    zone: zone,
                    _token: token
                },
                success: function(data) { //alert(data.options);
                    //   $(".se-pre-con").fadeOut("slow");
                    $("select[name='phase'").html('');

                    if (data.status == 1) {
                        $("select[name='phase'").append('<option value>[Select]</option>');
                        $.each(data.options, function(k, v) {
                            $("select[name='phase'").append('<option value=' + k + '>' + v +
                                '</option>');
                        });
                        //$("select[name='subdivision'").html(data.options);
                        // change_officeName();

                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    //  $(".se-pre-con").fadeOut("slow");
                    var msg = "";
                    if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                        msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                    } else {
                        if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                            msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                        } else {
                            msg += "Error(s):<strong><ul>";
                            $.each(jqXHR.responseJSON, function(key, value) {
                                msg += "<li>" + value + "</li>";
                            });
                            msg += "</ul></strong>";
                        }
                    }
                    $.alert({
                        title: 'Error!!',
                        type: 'red',
                        icon: 'fa fa-warning',
                        content: msg
                    });
                },
                complete: function() {
                    getZonePhaseWiseAssembly();
                }
            });
        }

    </script>
@stop
