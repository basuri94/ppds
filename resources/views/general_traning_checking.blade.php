@extends('layouts.master')
@section('content')
<div class="wrapper">
    @include('layouts.sidebar')
    <div id="content">           
        <section class="tables-section">
            <nav aria-label="breadcrumb" style="width:100%;">
                <div class="breadcrumb pagehead1">
                        {!! Form::button('<i class="fa fa-desktop"></i> Zone And Phase Wise General Training Allocation  Checking',['class' => 'btn btn-primary-header add-new-button']) !!}
                        <div class="clearfix"></div>
                </div>
            </nav>
            <div class="outer-w3-agile">
                {!! Form::open(['url' => 'general_traning_checking', 'name' => 'general_traning_checking', 'class' =>'request-info clearfix form-horizontal', 'id' => 'general_traning_checking', 'method' => 'post','role'=>'','files' => true]) !!}
                {!! Form::hidden('districtcd', session()->get('districtcd_ppds'),['id'=>'districtcd']) !!}
                <div class="panel-group" id="accordion5">                                                              
                    <div class="panel panel-default">
                     <div class="panel-heading1">
                     <h6 class="panel-title">
                         <a data-toggle="#" data-parent="#accordion5" href="#collapseUV"> <span class="fa fa-minus"></span> <span class="highlight">Zone And Phase Wise General Training Allocation  Checking</span> </a>
                     </h6>
                     </div>
                     <div id="collapseUV" class="panel-collapse collapse5">
                      <div class="panel-body">                                   
                        <div class="row">                                   
                           {{-- <div class='col-sm-4'> --}}
                           <div class='col-sm-3'>
                               {!! Form::label('zone', 'Zone:', ['class'=>'highlight required']) !!}
                              <div class="form-group">
                                  <div class=''>
                                      {!! Form::select('zone',[''=>'[Select]'],Null,['id'=>'zone','class'=>'form-control','autocomplete'=>'off']) !!}
                                  </div>
                              </div>
                          </div>
                          {{-- <div class='col-sm-4'> --}}
                          {{-- <div class='col-sm-3'>
                            {!! Form::label('training_head', 'Training Head:', ['class'=>'highlight required']) !!}
                            <div class="form-group">
                                <div class=''>
                                     {!! Form::select('training_head',$training_type['desc'],null,['id'=>'training_head','class'=>'form-control','placeholder'=>'[Select]','autocomplete'=>'off']) !!}
                                </div>
                            </div>
                        </div> --}}
                          <div class='col-sm-3'>
                            {!! Form::label('phase', 'Phase:', ['class'=>'highlight required']) !!}
                           <div class="form-group">
                               <div class=''>
                                    <select id="phase" class="form-control" name="phase">
                                                <option value="">[Select]</option>
                                    </select>
                                </div>
                           </div>
                       </div>

                       {{-- <div class='col-sm-3'>
                        {!! Form::label('subdivision', 'Sub Division:', ['class'=>'highlight required']) !!}
                          <!--<span class="highlight">Sub Division</span> -->
                          <div class="form-group">
                              <div class=''>
                                  {!! Form::select('subdivision',[''=>'[Select]'],null,['id'=>'subdivision','class'=>'form-control','autocomplete'=>'off']) !!}
                              </div>
                          </div>
                    </div>  --}}

                       {{-- <div class='col-sm-4'> --}}
                       {{-- <div class='col-sm-3'>
                        {!! Form::label('subdivision', 'Subdivision:', ['class'=>'highlight required']) !!}
                       <div class="form-group">
                         
                            <div class=''>
                                {!! Form::select('subdivision',[''=>'[Select]'],Null,['id'=>'subdivision','class'=>'form-control','autocomplete'=>'off']) !!}
                           
                            </div>
                       </div>
                   </div> --}}
                    </div> 
                     
                       <div class="row"> 
                        <div class='col-sm-12'>
                            <div id='assemblyDetails'></div>
                        </div>
                    </div>

                    <div class="row"> 
                        <div class='col-sm-12'>                    
                        <div class="form-group text-right permit">                            	
{{--                            
                            {{ Form::button('EXCEL', ['class' => 'btn btn-info', 'type' => 'button','id'=>'EXCEL']) }}
                           {{ Form::button('Reset', ['class' => 'btn btn-success' ,'type' => 'reset','id'=>'reset']) }} --}}
                        </div>
                        </div>
                  </div> 
                    {!! Form::close() !!}         
                </div>
            </div>
        </div>                              
    </div> 
                           
                </div>
            </section>
</div>
</div>
<script>
    $(document).ready(function () {
        getZoneDetails();
        $("#zone").change(function(){
            getZonePhasedata();
        //    change_subdivision();
        });
        // $("#subdivision").change(function(){
        //     var zone = $("#zone").val();
        //     var phase = $("#phase").val();
        //     var subdivison = $("#subdivision").val();
          
        //     if(zone!='' && phase!='' && subdivison!='' ){
        //         subdivision_wise_traning_checking();
        //     } else{
        //         $("#assemblyDetails").html("");
          
        //     }

        // });
        // $("#training_head").change(function(){
        //     var zone = $("#zone").val();
        //     var phase = $("#phase").val();
        //     var subdivison = $("#subdivision").val();
        //     var training_head = $("#training_head").val();
        //     if(zone!='' && phase!='' && subdivison!='' && training_head!=''){
        //         subdivision_wise_traning_checking();
        //     } else{
        //         $("#assemblyDetails").html("");
        //     //     $.alert({
        //     //       title: 'Error!!',
        //     //       type: 'red',
        //     //       icon: 'fa fa-warning',
        //     //       content: "Please Select Subdivision"
        //     //   });
        //     }
        // });
        $("#phase").change(function(){
            var zone = $("#zone").val();
            var phase = $("#phase").val();
            var subdivison = $("#subdivision").val();
          
            if(zone!='' && phase!='' && subdivison!='' ){
                subdivision_wise_traning_checking();
            } else{
                $("#assemblyDetails").html("");
          
            }
        });
        // $("#subdivision").change(function(){
        //     var zone = $("#zone").val();
        //     var phase = $("#phase").val();
        //     var subdivison = $("#subdivision").val();
          
        //     if(zone!='' && phase!='' && subdivison!='' ){
        //         subdivision_wise_traning_checking();
        //     } else{
        //         $("#assemblyDetails").html("");
        //     //     $.alert({
        //     //       title: 'Error!!',
        //     //       type: 'red',
        //     //       icon: 'fa fa-warning',
        //     //       content: "Please Select Zone, Traning Head and Phase"
        //     //   });
        //     }
        // });

        // $("#EXCEL").click(function (e) {
        //     window.open('data:application/vnd.ms-excel,' + $('#dvData').html());
        //     e.preventDefault();
        // });






        $('#EXCEL1').click(function () {
            var zone = $("#zone").val();
            var phase = $("#phase").val();
            // alert(phase);
            if(zone=="" || phase==''){
                $.alert({
                    title: 'Error!!',
                    type: 'red',
                    icon: 'fa fa-exclamation-triangle',
                    content: 'Please Select Both Zone And Phase'
                });
                return false;
            }
             var datas = {'zone': zone,'phase': phase, '_token': $('input[name="_token"]').val()};
             redirectPost('{{url("generaltraningallocationPPExcel")}}', datas);
        });

    });
    function getZoneDetails(){
    var forDist = $("#districtcd").val();
    var token = $("input[name='_token']").val();
    $(".se-pre-con").fadeIn("slow");
    $.ajax({
       url: "getZoneDetails",
       method: 'POST',
       data: {forDist: forDist, _token: token},
       success: function (data) {//alert(data.options);
         $(".se-pre-con").fadeOut("slow");
          $("select[name='zone'").html('');
          if(data.status==1)
          {
             $("select[name='zone'").append('<option value>[Select]</option>');
            $.each(data.options,function (k, v)
            {
                $("select[name='zone'").append('<option value='+k+'>'+v+'</option>');
            });
           // getMemberDetails();
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
            $(".se-pre-con").fadeOut("slow");
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-warning',
                  content: msg
              });
         },
   });
 }
 function getZonePhasedata(){
  var zone = $("#zone").val();
  $("#rw").hide();
   var token = $("input[name='_token']").val();
  // $(".se-pre-con").fadeIn("slow");
   $.ajax({
       url: "getZonePhasedata",
       method: 'POST',
       data: {zone: zone, _token: token},
       success: function (data) {//alert(data.options);
       //   $(".se-pre-con").fadeOut("slow");
          $("select[name='phase'").html('');
          
          if(data.status==1)
          {
            $("select[name='phase'").append('<option value>[Select]</option>');
            $.each(data.options,function (k, v)
            {
                $("select[name='phase'").append('<option value='+k+'>'+v+'</option>');
            });
            //$("select[name='subdivision'").html(data.options);
             // change_officeName();
             
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
       //  $(".se-pre-con").fadeOut("slow");
           var msg = "";
           if (jqXHR.status !== 422 && jqXHR.status !== 400) {
               msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
           } else {
               if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                   msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
               } else {
                   msg += "Error(s):<strong><ul>";
                   $.each(jqXHR.responseJSON, function (key, value) {
                       msg += "<li>" + value + "</li>";
                   });
                   msg += "</ul></strong>";
               }
           }
           $.alert({
               title: 'Error!!',
               type: 'red',
               icon: 'fa fa-warning',
               content: msg
           });
         },
   });
}
function change_subdivision(){
  var zone = $("#zone").val();
  var forDist = $("#districtcd").val();
   var token = $("input[name='_token']").val();
  $(".se-pre-con").fadeIn("slow");
   $.ajax({
       url: "getZonewiseSubdivisionall_Data",
       method: 'POST',
       data: {zone: zone,forDist: forDist, _token: token},
       success: function (data) {//alert(data.options);
          $(".se-pre-con").fadeOut("slow");
          $("select[name='subdivision'").html('');
          
          if(data.status==1)
          {
            $("select[name='subdivision'").append('<option value>[Select]</option>');
            $.each(data.options,function (k, v)
            {
                $("select[name='subdivision'").append('<option value='+k+'>'+v+'</option>');
            });
            //$("select[name='subdivision'").html(data.options);
             // change_officeName();
            // getMemberDetails();
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
         $(".se-pre-con").fadeOut("slow");
           var msg = "";
           if (jqXHR.status !== 422 && jqXHR.status !== 400) {
               msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
           } else {
               if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                   msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
               } else {
                   msg += "Error(s):<strong><ul>";
                   $.each(jqXHR.responseJSON, function (key, value) {
                       msg += "<li>" + value + "</li>";
                   });
                   msg += "</ul></strong>";
               }
           }
           $.alert({
               title: 'Error!!',
               type: 'red',
               icon: 'fa fa-exclamation-triangle',
               content: msg
           });
         },
        complete: function () {
            other_subdivision();
            getZonePhasedata(); 
        }
   });
}

function subdivision_wise_traning_checking() {
   
    var zone = $("#zone").val();
    var phase = $("#phase").val();
    // var subdivison = $("#subdivision").val();
   
    
    var token = $("input[name='_token']").val();
    $(".se-pre-con").fadeIn("slow");
    $.ajax({
        type: 'POST',
        url: 'get_subdivison_wise_traning',
      
        data: {zone:zone,phase:phase,_token:token},
        dataType: 'json',
        success: function (response) {
        $(".se-pre-con").fadeOut("slow");
        $("#assemblyDetails").html("");
        var str = "";
        str+= "<table class='table table-bordered table-striped' width='100%' style='border-top: 2px solid #4cae4c;' id='dvData'>";
       
        str += ' <tr style="background-color: #f5f8fa"><th rowspan="2" style="width: 15%;">&nbsp;Subdivision </th><th colspan="4" scope="colgroup">&nbsp;Without 1st/2nd Training</th><th colspan="4">&nbsp;1st Traning Allocation</th><th colspan="4">&nbsp;2nd Traning Allocation</th></tr>';
        str += '<tr style="background-color: #f5f8fa"><th>P1</th><th>P2</th><th>P3</th><th>PR</th><th>P1</th><th>P2</th><th>P3</th><th>PR</th><th>P1</th><th>P2</th><th>P3</th><th>PR</th></tr>';
        str +=response.requirement
      
        $("#assemblyDetails").append(str);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $(".se-pre-con").fadeOut("slow");
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-warning',
                  content: msg
              });
         }
   
    });
}

</script>

@stop
