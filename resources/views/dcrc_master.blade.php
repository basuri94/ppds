
@extends('layouts.master')
@section('content')
<style>
    .dropdown-menu{
        /* right: 67%;*/
        /* left: auto; */
        min-width: 0px !important; 
        width:230px !important;
        padding: 1em !important;
    }

</style>
<!--// top-bar -->
<div class="wrapper">
    <!-- Sidebar Holder -->
    @include('layouts.sidebar')

    <!-- Page Content Holder -->
    <div id="content">           
        <section class="tables-section">
            <nav aria-label="breadcrumb" style="width:100%;">
                <div class="breadcrumb pagehead1">
                    <span class="headbrand"><i class="fa fa-desktop"></i> Add DCRC Master
                        <span class="scoop-mcaret1"></span>
                    </span>&nbsp;&nbsp;
                    &nbsp;{!! Form::button(' List of DCRC Master&nbsp;<i class="fa fa-arrow-circle-right"></i>',['id'=>'show_dcrc_master_list','class' => 'btn btn-primary-year add-new-button']) !!}  

<!--                    {!! Form::button('<i class="fa fa-desktop"></i> Add DCRC Master',['class' => 'btn btn-primary-header add-new-button']) !!}

     <div class="col-md-offset-8 pull-right">
         {!! Form::button('List of DCRC Master &nbsp;<i class="fa fa-arrow-circle-right"></i>',['id'=>'show_dcrc_master_list','class' => 'btn btn-primary-year add-new-button']) !!}  

     </div>-->
                    <div class="clearfix"></div>
                </div>
            </nav>

            <!-- form -->
            <div class="outer-w3-agile">
                {!! Form::open(['url' => 'DCRCMasterForm', 'name' => 'DCRCMasterForm', 'class' =>'request-info clearfix form-horizontal', 'id' => 'DCRCMasterForm', 'method' => 'post','role'=>'','files' => true]) !!}
                {!! Form::hidden('districtcd', session()->get('districtcd_ppds'),['id'=>'districtcd']) !!}
                {!! Form::hidden('dc_venue_array', '',['class'=>'form-control','id'=>'dc_venue_array']) !!}
                {!! Form::hidden('dc_address_array', '',['class'=>'form-control','id'=>'dc_address_array']) !!}
                {!! Form::hidden('rc_venue_array', '',['class'=>'form-control','id'=>'rc_venue_array']) !!}
                {!! Form::hidden('rc_address_array', '',['class'=>'form-control','id'=>'rc_address_array']) !!}
                {!! Form::hidden('tysfs_code', '',['id'=>'tysfs_code']) !!}
                {!! Form::hidden('tysfm_code', '',['id'=>'tysfm_code']) !!}

                <div class="panel-group" id="accordion5">                                                              
                    <div class="panel panel-default">
                        <div class="panel-heading1">
                            <h6 class="panel-title">
                                <a data-toggle="#" data-parent="#accordion5" href="#collapsePV"> <span class="fa fa-minus"></span> <span class="highlight">Zonewise DCRC Master </span></a>
                            </h6>
                        </div>
                        <div id="collapsePV" class="panel-collapse collapse5">
                            <div class="panel-body">                                   
                                <div class="row">  
                                    <!--         <div class='col-sm-4'>
                                                {!! Form::label('subdivision', 'Sub Division:', ['class'=>'highlight required']) !!}
                                                  <span class="highlight">Sub Division</span> 
                                                  <div class="form-group">
                                                      <div class=''>
                                                          {!! Form::select('subdivision',[''=>'[Select]'],null,['id'=>'subdivision','class'=>'form-control','autocomplete'=>'off']) !!}
                                                      </div>
                                                  </div>
                                            </div> -->

                                    <div class='col-sm-4'>
                                        {!! Form::label('forZone', 'For Zone:', ['class'=>'highlight required']) !!}
                                        <div class="form-group">
                                            <div class=''>
                                                {!! Form::select('forZone',[''=>'[Select]'],Null,['id'=>'forZone','class'=>'form-control','autocomplete'=>'off']) !!}
                                            </div>
                                        </div>
                                    </div> 
                                    <div class='col-sm-4'>
                                        {!! Form::label('dcdate', 'DC Date:', ['class'=>'highlight required']) !!}
                                        <div class="form-group">
                                            <div class=''>
                                                {!! Form::text('dcdate',null,['id'=>'dcdate','class'=>'form-control','autocomplete'=>'off','maxlength'=>'10','placeholder'=>'DD/MM/YYYY']) !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class='col-sm-4'>
                                        {!! Form::label('dctime', 'DC Time:', ['class'=>'highlight required']) !!} <span style="color:red;font-size: 11px;font-weight: bold;">[Max:15]</span>
                                        <div class="form-group">
                                            <div class=''>
                                                {!! Form::text('dctime',null,['id'=>'dctime','class'=>'form-control','autocomplete'=>'off','maxlength'=>'15']) !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="row"> 
                                    <div class='col-sm-12'>
                                        <div class="form-group">
                                            <table class="table table-bordered table-striped" style='border-top: 2px solid #4cae4c;' id="assembly_party">
                                                <thead>
                                                    <tr style="background-color: #f5f8fa" >
                                                        <th width='23%' class="highlight">DC Venue  <span style="color: red;">* </span><span style="color:red;font-size: 11px;">[Max:60]</span></th>
                                                        <th width='24%' class="highlight">DC Address <span style="color: red;">* </span><span style="color:red;font-size: 11px;">[Max:60]</span></th>
                                                        <th width='23%' class="highlight">RC Venue <span style="color: red;">* </span><span style="color:red;font-size: 11px;">[Max:60]</span></th>
                                                        <th width='24%' class="highlight">RC Address <span style="color: red;">* </span><span style="color:red;font-size: 11px;">[Max:60]</span></th>
                                                        <th width='6%' class="highlight">Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr style="background-color: #FDF5E6">
                                                        <td>
                                                            {!! Form::text('dc_venue', null, ['id'=>'dc_venue','class'=>'form-control','maxlength'=>'60'])!!}
                                                        </td>
                                                        <td>   {!! Form::text('dc_address', null, ['id'=>'dc_address','class'=>'form-control','maxlength'=>'60'])!!}</td>
                                                        <td>
                                                            {!! Form::text('rc_venue', null, ['id'=>'rc_venue','class'=>'form-control','maxlength'=>'60'])!!}
                                                        </td>

                                                        <td>   {!! Form::text('rc_address', null, ['id'=>'rc_address','class'=>'form-control','maxlength'=>'60'])!!}</td>
                                                        <td>   <a href='javascript:' class='add-row'><i class='fa fa-plus-circle' style='color: #17a2b8;cursor:pointer;font-size: 16.5px;padding-top: 10px;'></i>&nbsp;</a>  </td>

                                                    </tr> 
                                                </tbody>

                                            </table>

                                        </div>
                                    </div>

                                </div> 
                                <div class="row">  
                                    <div class='col-sm-12'>
                                        <div class="form-group text-right permit">                            	
                                            {{ Form::button('Submit', ['class' => 'btn btn-info', 'type' => 'submit','id'=>'Submit']) }}
                                            {{ Form::button('Reset', ['class' => 'btn btn-success' ,'type' => 'reset','id'=>'reset']) }}
                                        </div>
                                    </div>
                                </div>     

                            </div>                              
                        </div>
                    </div>  
                </div>
                {!! Form::close() !!}             
            </div>
            <!--// form -->

        </section>

    </div>
</div>
<!-- Copyright -->
<script type="text/javascript">
    $(document).ready(function () {
        getZoneDetails();
        $('#dc_venue').on('change', function () {
            var dc_venue = $("#dc_venue").val();
            $("#rc_venue").val(dc_venue);
        });
        $('#dc_address').on('change', function () {
            var dc_address = $("#dc_address").val();
            $("#rc_address").val(dc_address);
        });
        $("#reset").click(function () {
            location.reload(true);
        });
        char_num("#no_of_party");
        $("#show_dcrc_master_list").click(function () {
            window.location.href = "dcrc_master_list";
        });
        var date = new Date();
        var today = new Date(date.getFullYear(), date.getMonth(), date.getDate());

        $('#dcdate').datepicker({
            format: "dd/mm/yyyy",
            todayHighlight: true,
//            endDate: new Date(),
            autoclose: true
        });
        $('#dcdate').datepicker('setDate', today);
//        $('#dcdate').datepicker({
//            format: "dd/mm/yyyy",
//            autoclose: true,
//            todayHighlight: true,
//            endDate: new Date(),
//            startDate: today
//        });
       
        array_dc_venue = [];
        array_dc_address = [];
        array_rc_venue = [];
        array_rc_address = [];
        array_duplicate = [];
        $(".add-row").click(function () {
            var dc_venue = $("#dc_venue").val();
            var dc_address = $("#dc_address").val();
            var rc_venue = $("#rc_venue").val();
            var rc_address = $("#rc_address").val();

            if (dc_venue == "" || dc_address == "" || rc_venue == "" || rc_address == "") {
                $.alert({
                    title: 'Error!!',
                    type: 'red',
                    icon: 'fa fa-exclamation-triangle',
                    content: "Enter required field"
                });
                return false;
            }
            var dc_venue_address = dc_venue + "-" + dc_address;
            var duplicate_asm = jQuery.inArray(dc_venue_address, array_duplicate);
            if (duplicate_asm >= 0) {
                $.alert({
                    title: 'Error!!',
                    type: 'red',
                    icon: 'fa fa-exclamation-triangle',
                    content: 'Duplicate record not allow'
                });
                return false;
            }
            array_duplicate.push(dc_venue + "-" + dc_address);
            var markup = "<tr class='table_tr'><td>&nbsp;" + dc_venue + "</td><td>&nbsp;" + dc_address + "</td><td>&nbsp;" + rc_venue + "</td><td>&nbsp;" + rc_address + "&nbsp;</td><td><a href='javascript:' onclick='deleteRow(this);'><i class='fa fa-trash-alt' style='color:#f10418;cursor:pointer;font-size:16px;'></i></a></td></tr>";
            $("table tbody").append(markup);
            array_dc_venue.push(dc_venue);
            array_dc_address.push(dc_address);
            array_rc_venue.push(rc_venue);
            array_rc_address.push(rc_address);
            $("#dc_venue_array").val(array_dc_venue);
            $("#dc_address_array").val(array_dc_address);
            $("#rc_venue_array").val(array_rc_venue);
            $("#rc_address_array").val(array_rc_address);
            $("#dc_venue").val("");
            $("#dc_address").val("");
            $("#rc_venue").val("");
            $("#rc_address").val("");
        });
        $('#DCRCMasterForm').bootstrapValidator({

            message: 'This value is not valid',
            feedbackIcons: {
                valid: 'glyphicon',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {

                forZone: {
                    validators: {
                        notEmpty: {
                            message: 'Zone is required'
                        }
                    }
                },
                dcdate: {
                    validators: {
                        notEmpty: {
                            message: 'DC Date is required'
                        }
                    }
                },
                dctime: {
                    validators: {
                        notEmpty: {
                            message: 'DC Time is required'
                        }
                    }
                }

            }

        }).on('success.form.bv', function (e) {
            e.preventDefault();
            var norowCount = $('#assembly_party tr').length;
            if (norowCount == 2) {
                $.alert({
                    title: 'Error!!',
                    type: 'red',
                    icon: 'fa fa-exclamation-triangle',
                    content: 'Add atleast one DCRC'
                });
                return false;
            }
            var action = $(this).attr('action');
            //alert(action);
            var type = $(this).attr('method');
            var districtcd = $("#districtcd").val();
            var forZone = $("#forZone").val();
            var dcdate = $("#dcdate").val();
            var dctime = $("#dctime").val();
            var dc_venue_array = JSON.stringify(array_dc_venue);
            var dc_address_array = JSON.stringify(array_dc_address);
            var rc_venue_array = JSON.stringify(array_rc_venue);
            var rc_address_array = JSON.stringify(array_rc_address);
            var token = $("input[name='_token']").val();
            var fd = new FormData();
            fd.append('districtcd', districtcd);
            fd.append('_token', token);
            fd.append('forZone', forZone);
            fd.append('dc_venue_array', dc_venue_array);
            fd.append('dc_address_array', dc_address_array);
            fd.append('rc_venue_array', rc_venue_array);
            fd.append('rc_address_array', rc_address_array);
            fd.append('dcdate', dcdate);
            fd.append('dctime', dctime);

            $(".se-pre-con").fadeIn("slow");
            $.ajax({
                type: type,
                url: action,
                data: fd,
                processData: false,
                contentType: false,
                dataType: "json",
                success: function (data) {
                    $(".se-pre-con").fadeOut("slow");

                    if (data.status == 1) {
                        var msg = data.options + " Record(s) saved successfully";
                        $.confirm({
                            title: 'Success!',
                            type: 'green',
                            icon: 'fa fa-check',
                            content: msg,
                            buttons: {
                                ok: function () {
                                    location.reload(true);
                                }
                            }
                        });

                    } else {
                        var msg = "Record(s) updated successfully";
                        $.confirm({
                            title: 'Success!',
                            type: 'green',
                            icon: 'fa fa-check',
                            content: msg,
                            buttons: {
                                ok: function () {
                                    location.reload(true);
                                }

                            }
                        });

                    }
                    //$("#firstTrVenue")[0].reset();
                    // $('.table_tr').html("");


                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $(".se-pre-con").fadeOut("slow");
                    var msg = "";
                    if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                        msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                    } else {
                        if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                            msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                        } else {
                            msg += "Error(s):<strong><ul>";
                            $.each(jqXHR.responseJSON, function (key, value) {
                                msg += "<li>" + value + "</li>";
                            });
                            msg += "</ul></strong>";
                        }
                    }
                    $.alert({
                        title: 'Error!!',
                        type: 'red',
                        icon: 'fa fa-exclamation-triangle',
                        content: msg
                    });
                }
            });
        });
    });
    function deleteRow(row) {
        var i = row.parentNode.parentNode.rowIndex;

        document.getElementById('assembly_party').deleteRow(i);
        var index_arr = parseInt(i) - parseInt(2);
        array_dc_venue.splice(index_arr, 1);
        array_dc_address.splice(index_arr, 1);
        array_rc_venue.splice(index_arr, 1);
        array_rc_address.splice(index_arr, 1);
        array_duplicate.splice(index_arr, 1);
        $("#dc_venue_array").val(array_dc_venue);
        $("#dc_address_array").val(array_dc_address);
        $("#rc_venue_array").val(array_rc_venue);
        $("#rc_address_array").val(array_rc_address);

    }
    function getZoneDetails() {
        var forDist = $("#districtcd").val();
        var token = $("input[name='_token']").val();
        $(".se-pre-con").fadeIn("slow");
        $.ajax({
            url: "getZoneDetails",
            method: 'POST',
            data: {forDist: forDist, _token: token},
            success: function (data) {//alert(data.options);
                $(".se-pre-con").fadeOut("slow");
                $("select[name='forZone'").html('');
                if (data.status == 1)
                {
                    $("select[name='forZone'").append('<option value>[Select]</option>');
                    $.each(data.options, function (k, v)
                    {
                        $("select[name='forZone'").append('<option value=' + k + '>' + v + '</option>');
                    });
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $(".se-pre-con").fadeOut("slow");
                var msg = "";
                if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                    msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                } else {
                    if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                        msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                    } else {
                        msg += "Error(s):<strong><ul>";
                        $.each(jqXHR.responseJSON, function (key, value) {
                            msg += "<li>" + value + "</li>";
                        });
                        msg += "</ul></strong>";
                    }
                }
                $.alert({
                    title: 'Error!!',
                    type: 'red',
                    icon: 'fa fa-exclamation-triangle',
                    content: msg
                });
            }
        });
    }
</script>
@stop
