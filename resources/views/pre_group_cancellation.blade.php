
@extends('layouts.master')
@section('content')

 <!--// top-bar -->
 <div class="wrapper">
<!-- Sidebar Holder -->
@include('layouts.sidebar')

<!-- Page Content Holder -->
<div id="content">           
<section class="tables-section">
    <nav aria-label="breadcrumb" style="width:100%;">
        <div class="breadcrumb pagehead1">
                      <span class="headbrand"><i class="fa fa-desktop"></i> Pre-group Cancellation
            <span class="scoop-mcaret1"></span>
           </span>&nbsp;&nbsp;&nbsp;         
                {!! Form::button(' Upload Excel &nbsp;<i class="fa fa-plus-circle"></i>',['id'=>'show_po_add','class' => 'btn btn-primary-year add-new-button']) !!}  
                
               
                <div class="clearfix"></div>
        </div>
    </nav>

    <!-- form -->
<div class="outer-w3-agile">
  {!! Form::open(['url' => 'preGroupCancellation', 'name' => 'preGroupCancellation', 'class' =>'request-info clearfix form-horizontal', 'id' => 'preGroupCancellation', 'method' => 'post','role'=>'','files' => true]) !!}
   {!! Form::hidden('districtcd', session()->get('districtcd_ppds'),['id'=>'districtcd']) !!}
  <!-- OLD PWise -->
  <div class="panel-group" id="accordion5" style="">                                                              
    <div class="panel panel-default">
     <div class="panel-heading1">
     <h6 class="panel-title">
         <a data-toggle="#" data-parent="#accordion5" href="#collapsePV"> <span class="fa fa-minus"></span><span class="highlight"> OLD PERSONNEL </span></a>
     </h6>
     </div>
     <div id="collapsePV" class="panel-collapse collapse5">
      <div class="panel-body" style="min-height:75px;">                                   
        <div class="row"> 
         <div class='col-sm-4'>
               {!! Form::label('zone', 'Zone:', ['class'=>'highlight required']) !!}
              <div class="form-group">
                  <div class=''>
                      {!! Form::select('zone',[''=>'[Select]'],Null,['id'=>'zone','class'=>'form-control','autocomplete'=>'off']) !!}
                  </div>
              </div>
          </div>
          <div class='col-sm-4'>
            {!! Form::label('phase', 'Phase:', ['class'=>'highlight required']) !!}
           <div class="form-group">
               <div class=''>
                
    <select id="phase" class="form-control" name="phase">
                  @php
                      $phase_data=App\Phase::get();
                  @endphp
                  <option value="">[Select]</option>
                  @foreach ($phase_data as $item)
                  <option value="{{ $item->code}}">{{$item->name}}</option>
                  @endforeach
                   
                  

    </select>
                </div>
           </div>
       </div>
         <div class='col-sm-4'>
              {!! Form::label('personnel_id', 'Personnel ID:', ['class'=>'highlight']) !!}
              <div class="form-group">
                  <div class=''>
                      {!! Form::text('personnel_id',null,['id'=>'personnel_id','class'=>'form-control','autocomplete'=>'off','maxlength'=>'11','minlength'=>'11']) !!}
                  </div>
              </div>
              <div style="display: none;">
                      {!! Form::text('newpersonnel_id',null,['id'=>'newpersonnel_id','class'=>'form-control','autocomplete'=>'off','maxlength'=>'11','minlength'=>'11','readonly'=>'false']) !!}
                  </div>
          </div>     
        </div>
        <div class="row"> 
            <div class='col-sm-12'> 
               <div id="old_personnel_details" >                            	
                   
               </div>
            </div>
         </div> 
         <div class="row" id="rw" style="display: none;"> 
           <div class='col-sm-3'>
               {!! Form::label('reason', 'Reason for Cancellation:', ['class'=>'highlight required']) !!}
              <div class="form-group">
                  <div class=''>
                      {!! Form::text('reason',null,['id'=>'reason','class'=>'form-control','autocomplete'=>'off','maxlength'=>'30']) !!}
                  </div>
              </div>
          </div> 
          <div class='col-sm-9'>  
                {!! Form::label('', '', ['class'=>'highlight']) !!}
                <div class="form-group text-right permit">                            	
                   {{ Form::button('Submit', ['class' => 'btn btn-info', 'type' => 'button','id'=>'cancel']) }}
                   {{ Form::button('Reset', ['class' => 'btn btn-success' ,'type' => 'reset','id'=>'reset']) }}
                </div>
           </div>
         </div> 
       </div>
      </div>
    </div>                              
  </div> 
  
    <!-- New PWise -->

                      
  

 {!! Form::close() !!}             
</div>
<!--// form -->


</section>

</div>
</div>
<!-- Copyright -->
<script type="text/javascript">

$(document).ready(function () {
    $("#show_po_add").click(function(){
       window.location.href = "preGroupCancellationExcelUpload"; 
  });
   getZoneDetails(); 
    $("#zone").change(function(){
    $("#personnel_id").val("");  
 });
   $("#reset").click(function () {
    location.reload(true);
  });
  char_num("#personnel_id");
  //getZoneDetails(); 
  $('#personnel_id').on('change', function () {
    var personnel_id = $("#personnel_id").val();
    var zone = $("#zone").val();
    var phase = $("#phase").val();
   if(personnel_id!="" && zone!="" && phase!=""){
    get_personnel_details(); 
   }
    
  });
  $('#phase').on('change', function () {
    var personnel_id = $("#personnel_id").val();
    var zone = $("#zone").val();
    var phase = $("#phase").val();
   if(personnel_id!="" && zone!="" && phase!=""){
    get_personnel_details(); 
   }
  });
  $("#cancel").click(function(){
        
        var personnel_id = $("#personnel_id").val();
            if(personnel_id == ""){
                $.alert({
                   title: 'Error!!',
                   type: 'red',
                   icon: 'fa fa-exclamation-triangle',
                   content: 'OLD Personnel ID is required'
               });
               return false;
           }
           var zone = $("#zone").val();
        if(zone == ""){
               $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-exclamation-triangle',
                  content: 'Zone is required'
              });
              return false;
          }
          var reason = $("#reason").val();
            if(reason == ""){
                $.alert({
                   title: 'Error!!',
                   type: 'red',
                   icon: 'fa fa-exclamation-triangle',
                   content: 'Reason is required'
               });
               return false;
           }
        var formData = new FormData($("#preGroupCancellation")[0]);
        $(".se-pre-con").fadeIn("slow");
        //var formData = {'zone': forzone,'forassembly': forassembly,'ofc_id': ofc_id,'booked': booked, 'gender': gender,'post_stat': post_stat, '_token': token};
        $.ajax({
            type: "post",
            url: "preCancelPPData",
            data: formData,
            processData: false,
            contentType: false,
            dataType: 'json',
            success: function (data) {
            $(".se-pre-con").fadeOut("slow"); 
            if(data.status==1)
            {
               // $('#new_personnel_details').html(data.options);
              //  $('#newpersonnel_id').val(data.perid);
                document.getElementById('cancel').disabled=true;
                $.confirm({
                        title: 'Success!',
                        type: 'green',
                        icon: 'fa fa-check',
                        content: data.options,
                        buttons: {
                            ok: function () {
                                /*$("#swapping_personnel")[0].reset();
                                $("#swapping").attr('disabled',true);
                                $('.form-control-feedback').css('display', 'none');
                                $('.has-success').removeClass('has-success');*/
                                location.reload(true);
                            }

                        }
                    });
            }
            else if(data.status==0)
            {
               // $('#new_personnel_details').html(data.options);
               // $('#newpersonnel_id').val(data.perid);
                //$('#rw').hide();   
                document.getElementById('cancel').disabled=false;
                 $.confirm({
                        title: 'Error!!',
                        type: 'red',
                        icon: 'fa fa-exclamation-triangle',
                        content: data.options,
                        buttons: {
                            ok: function () {
                                /*$("#swapping_personnel")[0].reset();
                                $("#swapping").attr('disabled',true);
                                $('.form-control-feedback').css('display', 'none');
                                $('.has-success').removeClass('has-success');*/
                               // location.reload(true);
                            }

                        }
                 });
            }
            
       },
        error: function (jqXHR, textStatus, errorThrown) {
            $(".se-pre-con").fadeOut("slow");
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-exclamation-triangle',
                  content: msg
              });
              document.getElementById('replace').disabled=false;
         }
     });
  });
 
});
 function get_personnel_details(){
    var personnel_id = $("#personnel_id").val();
    var zone = $("#zone").val();
    var phase = $("#phase").val();
    if(zone == ""){
           $.alert({
              title: 'Error!!',
              type: 'red',
              icon: 'fa fa-exclamation-triangle',
              content: 'Zone is required'
          });
          return false;
      }
      if(phase == ""){
           $.alert({
              title: 'Error!!',
              type: 'red',
              icon: 'fa fa-exclamation-triangle',
              content: 'Phase is required'
          });
          return false;
      }
    var token = $("input[name='_token']").val();
    $(".se-pre-con").fadeIn("slow");
    $.ajax({
       url: "getOldPersonnelDetails",
       method: 'POST',
       data: {personnel_id: personnel_id, zone:zone,phase:phase, _token: token},
       success: function (json) {//alert(data.options);
          $(".se-pre-con").fadeOut("slow");
          //$("#memberDetails").html('');
          if(json.status==1)
          {
              $('#old_personnel_details').html(json.options);
               $('#rw').show();
               scrollToElement($('#rw'));
          }
          else if(json.status==0)
          {
              $('#old_personnel_details').html(json.options);
              $('#rw').hide();
          }
          document.getElementById('replace').disabled=true;
          document.getElementById('print').disabled=true;
       },
        error: function (jqXHR, textStatus, errorThrown) {
         $(".se-pre-con").fadeOut("slow");
           var msg = "";
           if (jqXHR.status !== 422 && jqXHR.status !== 400) {
               msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
           } else {
               if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                   msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
               } else {
                   msg += "Error(s):<strong><ul>";
                   $.each(jqXHR.responseJSON, function (key, value) {
                       msg += "<li>" + value + "</li>";
                   });
                   msg += "</ul></strong>";
               }
           }
           $.alert({
               title: 'Error!!',
               type: 'red',
               icon: 'fa fa-warning',
               content: msg
           });
        }
   });
 }
  function getZoneDetails(){
    var forDist = $("#districtcd").val();
    var token = $("input[name='_token']").val();
    $(".se-pre-con").fadeIn("slow");
    $.ajax({
       url: "getZoneDetails",
       method: 'POST',
       data: {forDist: forDist, _token: token},
       success: function (data) {//alert(data.options);
         $(".se-pre-con").fadeOut("slow");
          $("select[name='zone'").html('');
          if(data.status==1)
          {
             $("select[name='zone'").append('<option value>[Select]</option>');
            $.each(data.options,function (k, v)
            {
                $("select[name='zone'").append('<option value='+k+'>'+v+'</option>');
            });
           // getMemberDetails();
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
            $(".se-pre-con").fadeOut("slow");
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-warning',
                  content: msg
              });
         }
   });
 }
 function scrollToElement(ele) {
    $('html, body').animate({
        scrollTop: ele.offset().top - 60
    }, 1000);
}
</script>
@stop
