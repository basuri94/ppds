
@extends('layouts.master')
@section('content')

 <!--// top-bar -->
 <div class="wrapper">
<!-- Sidebar Holder -->
@include('layouts.sidebar')

<!-- Page Content Holder -->
<div id="content">           
<section class="tables-section">
    <nav aria-label="breadcrumb" style="width:100%;">
        <div class="breadcrumb pagehead1">
           
                {!! Form::button('<i class="fa fa-desktop"></i> Second Appointment Letter',['class' => 'btn btn-primary-header add-new-button']) !!}
            
            <div class="col-md-offset-8 pull-right">
<!--                {!! Form::button('Academic Year:&nbsp;',['id'=>'show_academic','class' => 'btn btn-primary-year']) !!}-->
                
            </div>
                <div class="clearfix"></div>
        </div>
    </nav>

    <!-- form -->
<div class="outer-w3-agile">
  {!! Form::open(['url' => 'getFirstAppointmentLetterPDF', 'name' => 'getFirstAppointmentLetterPDF', 'class' =>'request-info clearfix form-horizontal', 'id' => 'getFirstAppointmentLetterPDF', 'method' => 'post','role'=>'','files' => true]) !!}
  {!! Form::hidden('districtcd', session()->get('districtcd_ppds'),['id'=>'districtcd']) !!}
  {!! Form::hidden('subdivcd', session()->get('subdivisioncd_ppds'),['id'=>'subdivcd']) !!}
  {!! Form::hidden('blockcd', session()->get('blockmunicd_ppds'),['id'=>'blockcd']) !!}
  
<!--  <span class="highlight">[Note: Record(s) transfered from Personnel to Zonewise]</span>-->
   <div class="panel-group" id="accordion5">                                                              
    <div class="panel panel-default">
     <div class="panel-heading1">
     <h6 class="panel-title">
         <a data-toggle="#" data-parent="#accordion5" href="#collapseUV"> <span class="fa fa-minus"></span><span class="highlight"> Zone-wise,Assembly-wise </span></a>
     </h6>
     </div>
     <div id="collapseUV" class="panel-collapse collapse5">
      <div class="panel-body">                                   
        <div class="row"> 
           <div class='col-sm-4'>
               {!! Form::label('party_reserve', 'Party/Reserve:', ['class'=>'highlight required']) !!}
                <div class="form-group">
                    <div class=''>
                        {!! Form::select('party_reserve',[''=>'[Select]'],Null,['id'=>'party_reserve','class'=>'form-control','autocomplete'=>'off']) !!}
                    </div>
                </div>
          </div>
           <div class='col-sm-4'>
               {!! Form::label('forZone', 'For Zone:', ['class'=>'highlight required']) !!}
                <div class="form-group">
                    <div class=''>
                        {!! Form::select('forZone',[''=>'[Select]'],Null,['id'=>'forZone','class'=>'form-control','autocomplete'=>'off']) !!}
                    </div>
                </div>
          </div>
            
          <div class='col-sm-4'>
              {!! Form::label('assembly', 'Assembly:', ['class'=>'highlight']) !!}
              <div class="form-group">
                  <div class=''>
                      {!! Form::select('assembly',[''=>'[Select]'],null,['id'=>'assembly','class'=>'form-control','autocomplete'=>'off']) !!}
                  </div>
              </div>
          </div>                      
       </div>
          
         <div class="row" id='PR_panel' style="display:none;"> 
         <div class='col-sm-4'>
               {!! Form::label('group_id', 'Group ID:', ['class'=>'highlight']) !!}
                <div class="form-group">
                    <div class=''>
                        {!! Form::text('group_id',Null,['id'=>'group_id','class'=>'form-control','autocomplete'=>'off','maxlength'=>'3']) !!}
                    </div>
                </div>
          </div>
           <div class='col-sm-4'>
               {!! Form::label('no_of_member', 'No of Member:', ['class'=>'highlight required']) !!}
                <div class="form-group">
                    <div class=''>
                        {!! Form::select('no_of_member',['4'=>'4 or 5'],Null,['id'=>'no_of_member','class'=>'form-control','autocomplete'=>'off']) !!}
                    </div>
                </div>
          </div>
                                 
        </div>
                       
       </div>
      </div>
    </div>                              
  </div> 
  
   <div class="panel-group" id="accordion6">                                                              
    <div class="panel panel-default">
     <div class="panel-heading1">
     <h6 class="panel-title">
         <a data-toggle="#" data-parent="#accordion6" href="#collapsePDF"> <span class="fa fa-minus"></span> <span class="highlight"> PDF / EXCEL </span></a>
     </h6>
     </div>
     <div id="collapsePDF" class="panel-collapse collapse5">
      <div class="panel-body">                                   
        <div class="row">                     
          <div class='col-sm-4'>
            {!! Form::label('recordsav', 'Records Available:', ['class'=>'highlight']) !!}
            <div class="form-group">
               <div class=''>
                     {!! Form::text('recordsav',null,['id'=>'recordsav','class'=>'form-control','autocomplete'=>'off', 'readonly'=>'true']) !!}
               </div>
            </div>
          </div>
         <div class='col-sm-4'>
            {!! Form::label('from', 'Records From:', ['class'=>'highlight required']) !!}
            <div class="form-group">
               <div class=''>
                     {!! Form::text('from',null,['id'=>'from','class'=>'form-control','autocomplete'=>'off']) !!}
               </div>
            </div>
          </div>
          <div class='col-sm-4'>
            {!! Form::label('to', 'Records To:', ['class'=>'highlight required']) !!}
            <div class="form-group">
               <div class=''>
                     {!! Form::text('to',null,['id'=>'to','class'=>'form-control','autocomplete'=>'off']) !!}
               </div>
            </div>
          </div>            
        </div>
          
          
          
        <div class="row"> 
           <div class='col-sm-12'>                    
            <div class="form-group text-right permit">  
               {{ Form::button('EXCEL', ['class' => 'btn btn-info', 'type' => 'button','id'=>'EXCEL']) }}
               {{ Form::button('PDF', ['class' => 'btn btn-info', 'type' => 'button','id'=>'PDF']) }}
               {{ Form::button('Reset', ['class' => 'btn btn-success' ,'type' => 'reset','id'=>'reset']) }}
            </div>
          </div>
        </div>
        
       </div>
      </div>
    </div>                              
  </div> 
  
   
  

 {!! Form::close() !!}             
</div>
<!--// form -->


</section>

</div>
</div>
<!-- Copyright -->
<script type="text/javascript">
$(document).ready(function () {
 $("#reset").click(function () {
  location.reload(true);  
});

 $("select[name='party_reserve'").append('<option value="P">Party</option>','<option value="R">Reserve</option>');
 $("select[name='no_of_member'").append('<option value="6">6</option>');
 $("#party_reserve").on('change', function () {
   var party_reserve = $("#party_reserve").val(); 
   if(party_reserve=="P"){
      $("#PR_panel").show();
   }else if(party_reserve=="R"){
     $("#PR_panel").hide();   
   }else{
     $("#PR_panel").hide();  
   }
    var forZone = $("#forZone").val();
    if(forZone!=""){
       getSecondRecordAvailable();
   }else{
       $("#recordsav").val("");
   }
 });
getZoneDetails(); 
 $('select[name="forZone"]').on('change', function () {   
    $("#recordsav").val("");
    getAssemblyDetails(); 
    getSecondRecordAvailable();
 });
  $('select[name="assembly"]').on('change', function () {
    getSecondRecordAvailable();
 }); 
 $('#group_id').on('change', function () {
    getSecondRecordAvailable();
 });
 $('select[name="no_of_member"]').on('change', function () {
    getSecondRecordAvailable();
 }); 
  $('#PDF').click(function () {
    var party_reserve = $("#party_reserve").val();
    var forZone = $("#forZone").val();
    var assembly = $("#assembly").val();
    var group_id = $("#group_id").val();
    var no_of_member = $("#no_of_member").val();
    var from = $("#from").val();
    var to = $("#to").val();
//    if(party_reserve== ""){
//        $.alert({
//             title: 'Error!!',
//             type: 'red',
//             icon: 'fa fa-exclamation-triangle',
//             content: 'Party/Reserve is required'
//         });
//         return false;
//     }
     if(forZone==""){
        $.alert({
             title: 'Error!!',
             type: 'red',
             icon: 'fa fa-exclamation-triangle',
             content: 'For Zone is required'
         });
         return false;
     }
     if(from== ""){
        $.alert({
             title: 'Error!!',
             type: 'red',
             icon: 'fa fa-exclamation-triangle',
             content: 'Records From is required'
         });
         return false;
     }
     if(to==""){
        $.alert({
             title: 'Error!!',
             type: 'red',
             icon: 'fa fa-exclamation-triangle',
             content: 'Records To is required'
         });
         return false;
     }
    var datas = {party_reserve: party_reserve,forZone: forZone,assembly: assembly,group_id: group_id,no_of_member: no_of_member,from: from,to: to,'_token': $('input[name="_token"]').val()};
    //$(".se-pre-con").fadeIn("slow");
    if(party_reserve=="P" && no_of_member=="4"){
       redirectPost_newTab('{{url("getSecondAppointmentLetterPDF452019")}}', datas);
    }else if(party_reserve=="P" && no_of_member=="6"){
       redirectPost_newTab('{{url("getSecondAppointmentLetterPDF62019")}}', datas); 
    }else if(party_reserve=="R"){
      redirectPost_newTab('{{url("getSecondAppointmentLetterReservePDF2019")}}', datas);   
    }else{
          redirectPost_newTab('{{url("getSecondAppointmentLetterPDF452019")}}', datas);
    }


 });
  $('#EXCEL').click(function () {
    var party_reserve = $("#party_reserve").val();
    var forZone = $("#forZone").val();
    var assembly = $("#assembly").val();
    var group_id = $("#group_id").val();
    var no_of_member = $("#no_of_member").val();
    var from = $("#from").val();
    var to = $("#to").val();
//    if(party_reserve== ""){
//        $.alert({
//             title: 'Error!!',
//             type: 'red',
//             icon: 'fa fa-exclamation-triangle',
//             content: 'Party/Reserve is required'
//         });
//         return false;
//     }
     if(forZone==""){
        $.alert({
             title: 'Error!!',
             type: 'red',
             icon: 'fa fa-exclamation-triangle',
             content: 'For Zone is required'
         });
         return false;
     }
     if(from== ""){
        $.alert({
             title: 'Error!!',
             type: 'red',
             icon: 'fa fa-exclamation-triangle',
             content: 'Records From is required'
         });
         return false;
     }
     if(to==""){
        $.alert({
             title: 'Error!!',
             type: 'red',
             icon: 'fa fa-exclamation-triangle',
             content: 'Records To is required'
         });
         return false;
     }
    var datas = {party_reserve: party_reserve,forZone: forZone,assembly: assembly,group_id: group_id,no_of_member: no_of_member,from: from,to: to,'_token': $('input[name="_token"]').val()};
    $(".se-pre-con").fadeIn("slow");
    if(party_reserve=="P" && no_of_member=="4"){
       redirectPost('{{url("getSecondAppointmentLetterEXCEL")}}', datas);
    }else if(party_reserve=="P" && no_of_member=="6"){
       redirectPost('{{url("getSecondAppointmentLetterEXCEL")}}', datas); 
    }else if(party_reserve=="R"){
      redirectPost('{{url("getSecondAppointmentLetterEXCEL")}}', datas);   
    }else{
        redirectPost('{{url("getSecondAppointmentLetterEXCEL")}}', datas);   
    }

     $(".se-pre-con").fadeOut("slow");
 }); 
});
function getZoneDetails(){
     var forDist = $("#districtcd").val();
     var token = $("input[name='_token']").val();
     $(".se-pre-con").fadeIn("slow");
     $.ajax({
       url: "getZoneDetails",
       method: 'POST',
       data: {forDist: forDist, _token: token},
       success: function (data) {//alert(data.options);
         $(".se-pre-con").fadeOut("slow");
          $("select[name='forZone'").html('');
          if(data.status==1)
          {
             $("select[name='forZone'").append('<option value>[Select]</option>');
            $.each(data.options,function (k, v)
            {
                $("select[name='forZone'").append('<option value='+k+'>'+v+'</option>');
            });
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
            $(".se-pre-con").fadeOut("slow");
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-exclamation-triangle',
                  content: msg
              });
         }
   });
}
function getAssemblyDetails(){
    var forZone = $("#forZone").val();
     var token = $("input[name='_token']").val();
     $(".se-pre-con").fadeIn("slow");
     $.ajax({
       url: "getAssemblyDetails",
       method: 'POST',
       data: {forZone: forZone, _token: token},
       success: function (data) {//alert(data.options);
         $(".se-pre-con").fadeOut("slow");
          $("select[name='assembly'").html('');
          if(data.status==1)
          {
              $("select[name='assembly'").html(data.options);
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
            $(".se-pre-con").fadeOut("slow");
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-exclamation-triangle',
                  content: msg
              });
         }
   });
}
function getSecondRecordAvailable(){
    var forZone = $("#forZone").val();
    var party_reserve = $("#party_reserve").val();
    var assembly = $("#assembly").val();
    var group_id = $("#group_id").val();
    var no_of_member = $("#no_of_member").val();
    
    var token = $("input[name='_token']").val();
    // $(".se-pre-con").fadeIn("slow");
     $.ajax({
       url: "getSecondRecordAvailable",
       method: 'POST',
       data: {forZone: forZone,party_reserve:party_reserve,assembly:assembly,group_id:group_id,no_of_member:no_of_member, _token: token},
       success: function (data) {//alert(data.options);
        // $(".se-pre-con").fadeOut("slow");
         $("#recordsav").val("");
          if(data.status==1)
          {
              $("#recordsav").val(data.options);
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-exclamation-triangle',
                  content: msg
              });
         }
   });
}

function change_blockMuni() {
        var subdivision = $("#subdivision").val();
        var token = $("input[name='_token']").val();
        $(".se-pre-con").fadeIn("slow");
        $.ajax({
            url: "getBlockMuni",
            method: 'POST',
            data: {subdivision: subdivision, _token: token},
            success: function (data) {//alert(data.options);
                $(".se-pre-con").fadeOut("slow");
                $("select[name='blockMunicipality'").html('');
                if (data.status == 1)
                {
                    $("select[name='blockMunicipality'").html(data.options);
                    // change_officeName();
                    //getMemberDetails();
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $(".se-pre-con").fadeOut("slow");
                var msg = "";
                if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                    msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                } else {
                    if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                        msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                    } else {
                        msg += "Error(s):<strong><ul>";
                        $.each(jqXHR.responseJSON, function (key, value) {
                            msg += "<li>" + value + "</li>";
                        });
                        msg += "</ul></strong>";
                    }
                }
                $.alert({
                    title: 'Error!!',
                    type: 'red',
                    icon: 'fa fa-exclamation-triangle',
                    content: msg
                });
            }
        });
    }
</script>
@stop
