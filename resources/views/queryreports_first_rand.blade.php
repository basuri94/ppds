
@extends('layouts.master')
@section('content')

 <!--// top-bar -->
 <div class="wrapper">
<!-- Sidebar Holder -->
@include('layouts.sidebar')

<!-- Page Content Holder -->
<div id="content">           
<section class="tables-section">
    <nav aria-label="breadcrumb" style="width:100%;">
        <div class="breadcrumb pagehead1">
           
                {!! Form::button('<i class="fa fa-desktop"></i> Polling Personnel Query',['class' => 'btn btn-primary-header add-new-button']) !!}
            
            <div class="col-md-offset-8 pull-right">
<!--                {!! Form::button('Academic Year:&nbsp;',['id'=>'show_academic','class' => 'btn btn-primary-year']) !!}-->
                
            </div>
                <div class="clearfix"></div>
        </div>
    </nav>

    <!-- form -->
<div class="outer-w3-agile">
  {!! Form::open(['url' => 'getFirstAppointmentLetterPDF', 'name' => 'getFirstAppointmentLetterPDF', 'class' =>'request-info clearfix form-horizontal', 'id' => 'getFirstAppointmentLetterPDF', 'method' => 'post','role'=>'','files' => true]) !!}
  {!! Form::hidden('districtcd', session()->get('districtcd_ppds'),['id'=>'districtcd']) !!}
  {!! Form::hidden('subdivcd', session()->get('subdivisioncd_ppds'),['id'=>'subdivcd']) !!}
  {!! Form::hidden('blockcd', session()->get('blockmunicd_ppds'),['id'=>'blockcd']) !!}
  
<!--  <span class="highlight">[Note: Record(s) transfered from Personnel to Zonewise]</span>-->
   <div class="panel-group" id="accordion5" style="width: 100%;float: left;">                                                              
    <div class="panel panel-default">
     <div class="panel-heading1">
     <h6 class="panel-title">
         <a data-toggle="#" data-parent="#accordion5" href="#collapseUV"> <span class="fa fa-minus"></span><span class="highlight"> Zone-wise, Subdivision-wise, Block/Municipality-wise, Category-wise, Institute-wise, Posting Status-wise PP data </span></a>
     </h6>
     </div>
     <div id="collapseUV" class="panel-collapse collapse5">
      <div class="panel-body">                                   
        <div class="row"> 
          <div class='col-sm-3'>
               {!! Form::label('zone', 'Zone:', ['class'=>'highlight required']) !!}
              <div class="form-group">
                  <div class=''>
                      {!! Form::select('zone',[''=>'[Select]'],Null,['id'=>'zone','class'=>'form-control','autocomplete'=>'off']) !!}
                  </div>
              </div>
          </div>
           <div class='col-sm-3'>
               {!! Form::label('subdivision', 'Sub Division:', ['class'=>'highlight required']) !!}
              <div class="form-group">
                  <div class=''>
                      {!! Form::select('subdivision',[''=>'[Select]'],null,['id'=>'subdivision','class'=>'form-control','autocomplete'=>'off']) !!}
                  </div>
              </div>
          </div>         
           <div class='col-sm-3'>
               {!! Form::label('type', 'Type:', ['class'=>'highlight required']) !!}
              <div class="form-group">
                  <div class=''>
                      {!! Form::select('type',['A'=>'[ALL]'],null,['id'=>'type','class'=>'form-control','autocomplete'=>'off']) !!}
                  </div>
              </div>
          </div>
          <div class='col-sm-3'>
            {!! Form::label('phase', 'Phase:', ['class'=>'highlight ']) !!}
           <div class="form-group">
               <div class=''>
                
    <select id="phase" class="form-control" name="phase">
                  @php
                      $phase_data=App\Phase::get();
                  @endphp
                  <option value="">[Select]</option>
                  @foreach ($phase_data as $item)
                  <option value="{{ $item->code}}">{{$item->name}}</option>
                  @endforeach
                   
                  

    </select>
                </div>
           </div>
       </div> 
         </div> 
             
       </div>
      </div>
    </div>                              
  </div> 
  <!-- Block Wise -->
  <div class="panel-group" id="accordion5" style="width: 25%;float: left;">                                                              
    <div class="panel panel-default">
     <div class="panel-heading1">
     <h6 class="panel-title">
         <a data-toggle="#" data-parent="#accordion5" href="#collapsePV"> <span class="fa fa-minus"></span><span class="highlight"> Block/Municipality-wise </span></a>
     </h6>
     </div>
     <div id="collapsePV" class="panel-collapse collapse5">
      <div class="panel-body">                                   
        <div class="row">      
         <div class='col-sm-11'>
              {!! Form::label('blockMunicipality', 'Block/Municipality:', ['class'=>'highlight']) !!}
              <div class="form-group">
                  <div class=''>
                      {!! Form::select('blockMunicipality',[''=>'[Select]'],null,['id'=>'blockMunicipality','class'=>'form-control','autocomplete'=>'off']) !!}
                  </div>
              </div>
          </div>     
        </div>
        <div class="row"> 
            <div class='col-sm-8'> 
               <div class="form-group text-right permit">                            	
                  {{ Form::button('EXCEL', ['class' => 'btn btn-info', 'type' => 'button','id'=>'EXCEL']) }}
                  {{ Form::button('Reset', ['class' => 'btn btn-success' ,'type' => 'reset','id'=>'reset']) }}
               </div>
            </div>
         </div>                
       </div>
      </div>
    </div>                              
  </div> 
  
    <!-- Category Wise -->
  <div class="panel-group" id="accordion5" style="width: 25%;float: left;margin-left: 3px;margin-right: 3px;">                                                              
    <div class="panel panel-default">
     <div class="panel-heading1">
     <h6 class="panel-title">
         <a data-toggle="#" data-parent="#accordion5" href="#collapseCV"> <span class="fa fa-minus"></span><span class="highlight"> Category-wise </span></a>
     </h6>
     </div>
     <div id="collapseCV" class="panel-collapse collapse5">
      <div class="panel-body">                                   
        <div class="row">          
          <div class='col-sm-11'>
              {!! Form::label('category', 'Category:', ['class'=>'highlight']) !!}
              <div class="form-group">
                  <div class=''>
                      {!! Form::select('category',[''=>'[Select]'],null,['id'=>'category','class'=>'form-control','autocomplete'=>'off']) !!}
                  </div>
              </div>
          </div>  
         
       </div>
        <div class="row"> 
            <div class='col-sm-8'> 
               <div class="form-group text-right permit">                            	
                  {{ Form::button('EXCEL', ['class' => 'btn btn-info', 'type' => 'button','id'=>'EXCEL1']) }}
                  {{ Form::button('Reset', ['class' => 'btn btn-success' ,'type' => 'reset','id'=>'reset']) }}
               </div>
            </div>
         </div>  
         </div> 
               
       </div>
      </div>
    </div>                              
 
  <!--- Institute Wise -->
  <div class="panel-group" id="accordion5" style="width: 24%;float: left;margin-right: 3px;">                                                              
    <div class="panel panel-default">
     <div class="panel-heading1">
     <h6 class="panel-title">
         <a data-toggle="#" data-parent="#accordion5" href="#collapseIV"> <span class="fa fa-minus"></span><span class="highlight"> Institute-wise</span></a>
     </h6>
     </div>
     <div id="collapseIV" class="panel-collapse collapse5">
      <div class="panel-body">                                   
        <div class="row"> 
          <div class='col-sm-11'>
              {!! Form::label('institute', 'Institute:', ['class'=>'highlight']) !!}
              <div class="form-group">
                  <div class=''>
                      {!! Form::select('institute',[''=>'[Select]'],null,['id'=>'institute','class'=>'form-control','autocomplete'=>'off']) !!}
                  </div>
              </div>
          </div>
            
        </div>
        <div class="row"> 
            <div class='col-sm-8'> 
               <div class="form-group text-right permit">                            	
                  {{ Form::button('EXCEL', ['class' => 'btn btn-info', 'type' => 'button','id'=>'EXCEL2']) }}
                  {{ Form::button('Reset', ['class' => 'btn btn-success' ,'type' => 'reset','id'=>'reset']) }}
               </div>
            </div>
         </div>  
        </div> 
        
       </div>
      </div>
    </div>    
  
  <!--- Posting Status Wise -->
  <div class="panel-group" id="accordion5" style="width: 25%;float: left;">                                                              
    <div class="panel panel-default">
     <div class="panel-heading1">
     <h6 class="panel-title">
         <a data-toggle="#" data-parent="#accordion5" href="#collapseIV"> <span class="fa fa-minus"></span><span class="highlight"> Posting Status-wise</span></a>
     </h6>
     </div>
     <div id="collapseIV" class="panel-collapse collapse5">
      <div class="panel-body">                                   
        <div class="row"> 
          <div class='col-sm-11'>
              {!! Form::label('poststatus', 'Posting Status:', ['class'=>'highlight']) !!}
               <div class="form-group">
                   <div class=''>
                       {!! Form::select('poststatus',$poststat['poststatus'],null,['id'=>'poststatus','class'=>'form-control','placeholder'=>'[Select]','autocomplete'=>'off']) !!}
                   </div>
               </div>
          </div>
            
        </div>
        <div class="row"> 
            <div class='col-sm-8'> 
               <div class="form-group text-right permit">                            	
                  {{ Form::button('EXCEL', ['class' => 'btn btn-info', 'type' => 'button','id'=>'EXCEL3']) }}
                  {{ Form::button('Reset', ['class' => 'btn btn-success' ,'type' => 'reset','id'=>'reset']) }}
               </div>
            </div>
         </div>  
        </div> 
        
       </div>
      </div>
    </div>  
  

 {!! Form::close() !!}             
</div>
<!--// form -->


</section>

</div>
</div>
<!-- Copyright -->
<script type="text/javascript">
$(document).ready(function () {
   $("select[name='type'").append('<option value="B">Selected (Without Individual Training)</option>','<option value="D">Selected (With Individual Training)</option>','<option value="F">Selected (Without Group Training)</option>','<option value="G">Selected (With Group Training)</option>','<option value="E">Not Selected</option>','<option value="C">Cancelled</option>');
   $("#reset").click(function () {
    location.reload(true);
  });
  getZoneDetails(); 
   $('select[name="zone"]').on('change', function () {
    change_subdivision();   
  });
  $('select[name="subdivision"]').on('change', function () {
    change_blockMuni();    
  });

  $('#EXCEL').click(function () {
     var districtcd = $("#districtcd").val();
     var zone = $("#zone").val();
     var subdivision = $("#subdivision").val();
     var blockMunicipality = $("#blockMunicipality").val();
     var type = $("#type").val();
     var phase = $("#phase").val();

     if(zone==""){
         $.alert({
            title: 'Error!!',
            type: 'red',
            icon: 'fa fa-exclamation-triangle',
            content: 'Zone is required'
        });
        return false;
     }
     if(subdivision==""){
         $.alert({
            title: 'Error!!',
            type: 'red',
            icon: 'fa fa-exclamation-triangle',
            content: 'Subdivision is required'
        });
        return false;
     }
     if(phase==""){
         $.alert({
            title: 'Error!!',
            type: 'red',
            icon: 'fa fa-exclamation-triangle',
            content: 'Phase is required'
        });
        return false;
     }
    // $(".se-pre-con").fadeIn("slow");
     var datas = {'type':type,'zone': zone,'districtcd': districtcd,'subdivision': subdivision,'blockMunicipality': blockMunicipality, 'phase':phase, '_token': $('input[name="_token"]').val()};
     redirectPost('{{url("getFirstSubBlockQueryReport")}}', datas);


 });
  $('#EXCEL1').click(function () {
     var districtcd = $("#districtcd").val();
     var zone = $("#zone").val();
     var subdivision = $("#subdivision").val();
     var category = $("#category").val();
     var type = $("#type").val();
     var phase = $("#phase").val();
     if(zone==""){
         $.alert({
            title: 'Error!!',
            type: 'red',
            icon: 'fa fa-exclamation-triangle',
            content: 'Zone is required'
        });
        return false;
     }
     if(subdivision==""){
         $.alert({
            title: 'Error!!',
            type: 'red',
            icon: 'fa fa-exclamation-triangle',
            content: 'Subdivision is required'
        });
        return false;
     }
     if(phase==""){
         $.alert({
            title: 'Error!!',
            type: 'red',
            icon: 'fa fa-exclamation-triangle',
            content: 'Phase is required'
        });
        return false;
     }
     
    // $(".se-pre-con").fadeIn("slow");
     var datas = {'type':type,'zone': zone,'districtcd': districtcd,'subdivision': subdivision,'category': category,'phase':phase, '_token': $('input[name="_token"]').val()};
     redirectPost('{{url("getFirstSubCatQueryReport")}}', datas);


  });
  $('#EXCEL2').click(function () {
     var districtcd = $("#districtcd").val();
     var zone = $("#zone").val();
     var subdivision = $("#subdivision").val();
     var institute = $("#institute").val();
     var type = $("#type").val();
     var phase = $("#phase").val();
     if(zone==""){
         $.alert({
            title: 'Error!!',
            type: 'red',
            icon: 'fa fa-exclamation-triangle',
            content: 'Zone is required'
        });
        return false;
     }
     if(subdivision==""){
         $.alert({
            title: 'Error!!',
            type: 'red',
            icon: 'fa fa-exclamation-triangle',
            content: 'Subdivision is required'
        });
        return false;
     }
     if(phase==""){
         $.alert({
            title: 'Error!!',
            type: 'red',
            icon: 'fa fa-exclamation-triangle',
            content: 'Phase is required'
        });
        return false;
     }
    // $(".se-pre-con").fadeIn("slow");
     var datas = {'type':type,'zone': zone,'districtcd': districtcd,'subdivision': subdivision,'institute': institute,'phase':phase,  '_token': $('input[name="_token"]').val()};
     redirectPost('{{url("getFirstSubInstiQueryReport")}}', datas);

 });
  $('#EXCEL3').click(function () {
     var districtcd = $("#districtcd").val();
     var zone = $("#zone").val();
     var subdivision = $("#subdivision").val();
     var poststatus = $("#poststatus").val();
     var type = $("#type").val();
     var phase = $("#phase").val();
     if(zone==""){
         $.alert({
            title: 'Error!!',
            type: 'red',
            icon: 'fa fa-exclamation-triangle',
            content: 'Zone is required'
        });
        return false;
     }
     if(subdivision==""){
         $.alert({
            title: 'Error!!',
            type: 'red',
            icon: 'fa fa-exclamation-triangle',
            content: 'Subdivision is required'
        });
        return false;
     }
     if(phase==""){
         $.alert({
            title: 'Error!!',
            type: 'red',
            icon: 'fa fa-exclamation-triangle',
            content: 'Phase is required'
        });
        return false;
     }
    // $(".se-pre-con").fadeIn("slow");
     var datas = {'type':type,'zone': zone,'districtcd': districtcd,'subdivision': subdivision,'poststatus': poststatus,'phase':phase, '_token': $('input[name="_token"]').val()};
     redirectPost('{{url("getFirstSubPoststatusQueryReport")}}', datas);
     // $(".se-pre-con").fadeOut("slow");
 });
   
});
function change_subdivision(){
  var forDist = $("#districtcd").val();
  var zone = $("#zone").val();
   var token = $("input[name='_token']").val();
   $(".se-pre-con").fadeIn("slow");
   $.ajax({
       url: "getZonewiseSubBlockDataQuery",
       method: 'POST',
       data: {forDist: forDist, zone: zone, _token: token},
       success: function (data) {//alert(data.options);
          $(".se-pre-con").fadeOut("slow");
          $("select[name='subdivision'").html('');
          
          if(data.status==1)
          {
            $("select[name='subdivision'").append('<option value>[Select]</option>');
            $.each(data.options,function (k, v)
            {
                $("select[name='subdivision'").append('<option value='+k+'>'+v+'</option>');
            });
            //$("select[name='subdivision'").html(data.options);
             // change_officeName();
           //  getMemberDetails();
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
         $(".se-pre-con").fadeOut("slow");
           var msg = "";
           if (jqXHR.status !== 422 && jqXHR.status !== 400) {
               msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
           } else {
               if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                   msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
               } else {
                   msg += "Error(s):<strong><ul>";
                   $.each(jqXHR.responseJSON, function (key, value) {
                       msg += "<li>" + value + "</li>";
                   });
                   msg += "</ul></strong>";
               }
           }
           $.alert({
               title: 'Error!!',
               type: 'red',
               icon: 'fa fa-warning',
               content: msg
           });
         }
   });
}
function getZoneDetails(){
     var forDist = $("#districtcd").val();
     var token = $("input[name='_token']").val();
     $(".se-pre-con").fadeIn("slow");
     $.ajax({
       url: "getZoneDetails",
       method: 'POST',
       data: {forDist: forDist, _token: token},
       success: function (data) {//alert(data.options);
         $(".se-pre-con").fadeOut("slow");
          $("select[name='zone'").html('');
          if(data.status==1)
          {
             $("select[name='zone'").append('<option value>[Select]</option>');
            $.each(data.options,function (k, v)
            {
                $("select[name='zone'").append('<option value='+k+'>'+v+'</option>');
            });
           // getMemberDetails();
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
            $(".se-pre-con").fadeOut("slow");
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-warning',
                  content: msg
              });
         }
   });
 }
function change_blockMuni() {
   var subdivision = $("#subdivision").val();
   var forDist = $("#districtcd").val();
   var zone = $("#zone").val();
   var token = $("input[name='_token']").val();
   $(".se-pre-con").fadeIn("slow");
   $.ajax({
       url: "getZoneBlockMuniUserwiseQuery",
       method: 'POST',
       data: {subdivision: subdivision,forDist:forDist,zone : zone, _token: token},
       success: function (data) {//alert(data.options);
          $(".se-pre-con").fadeOut("slow");
          $("select[name='blockMunicipality'").html('');
          if(data.status==1)
          {
            $("select[name='blockMunicipality'").html(data.options);
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
            $(".se-pre-con").fadeOut("slow");
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-warning',
                  content: msg
              });
         },complete: function(){
           getCategoryDetails();
         }
   });
 }
function getCategoryDetails(){
     var subdivision = $("#subdivision").val();
     var forDist = $("#districtcd").val();
     var zone = $("#zone").val();     
     var token = $("input[name='_token']").val();
     //$(".se-pre-con").fadeIn("slow");
     $.ajax({
       url: "getCategoryDetails",
       method: 'POST',
       data: {forDist: forDist, subdivision:subdivision, zone:zone,_token: token},
       success: function (data) {//alert(data.options);
        // $(".se-pre-con").fadeOut("slow");
          $("select[name='category'").html('');
          if(data.status==1)
          {
            $("select[name='category'").append('<option value>[Select]</option>');
            $.each(data.options,function (k, v)
            {
                $("select[name='category'").append('<option value='+k+'>'+v+'</option>');
            });
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
           // $(".se-pre-con").fadeOut("slow");
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-warning',
                  content: msg
              });
         },complete: function(){
           getInstituteDetails();
         }
   });
 }
function getInstituteDetails(){
     var subdivision = $("#subdivision").val();
     var forDist = $("#districtcd").val();
     var zone = $("#zone").val();     
     var token = $("input[name='_token']").val();
     //$(".se-pre-con").fadeIn("slow");
     $.ajax({
       url: "getInstituteDetails",
       method: 'POST',
       data: {forDist: forDist, subdivision:subdivision, zone:zone,_token: token},
       success: function (data) {//alert(data.options);
        // $(".se-pre-con").fadeOut("slow");
          $("select[name='institute'").html('');
          if(data.status==1)
          {
            $("select[name='institute'").append('<option value>[Select]</option>');
            $.each(data.options,function (k, v)
            {
                $("select[name='institute'").append('<option value='+k+'>'+v+'</option>');
            });
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
           // $(".se-pre-con").fadeOut("slow");
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-warning',
                  content: msg
              });
         }
   });
 }
</script>
@stop
