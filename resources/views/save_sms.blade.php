
@extends('layouts.master')
@section('content')
<style>
    .dropdown-menu{
        /* right: 67%;*/
        /* left: auto; */
        min-width: 0px !important; 
        width:230px !important;
        padding: 1em !important;
    }

</style>
<div class="wrapper">
    <!-- Sidebar Holder -->
    @include('layouts.sidebar')

    <!-- Page Content Holder -->
    <div id="content">           
        <section class="tables-section">
            <nav aria-label="breadcrumb" style="width:100%;">
                
                
                 <div class="breadcrumb pagehead1">
                      <span class="headbrand"><i class="fa fa-desktop"></i> Save SMS For General Training
            <span class="scoop-mcaret1"></span>
           </span>&nbsp;&nbsp;&nbsp;         
                {!! Form::button(' Send SMS For General Training &nbsp;<i class="fa fa-plus-circle"></i>',['id'=>'show_send_sms','class' => 'btn btn-primary-year add-new-button']) !!}  
                
               
                <div class="clearfix"></div>
        </div>
            </nav>

            <!-- form -->
            <div class="outer-w3-agile">
                {!! Form::open(['url' => 'sms_entry_details', 'name' => 'sms_entry_details', 'class' =>'request-info clearfix form-horizontal', 'id' =>'sms_entry_details', 'method' => 'post','role'=>'','files' => true]) !!}
                {!! Form::hidden('districtcd', session()->get('districtcd_ppds'),['id'=>'districtcd']) !!}


                <div class="panel-group" id="accordion5" style="padding-top: 0px;">                                                              
                    <div class="panel panel-default">
                        <div class="panel-heading1">
                            <h6 class="panel-title">
                                <a data-toggle="#" data-parent="#accordion5" href="#collapseUV"> <span class="fa fa-minus"></span> <span class="highlight">Save SMS </span></a>
                            </h6>
                        </div>
                        <div id="collapseUV" class="panel-collapse collapse5">
                            <div class="panel-body">                                   
                                <div class="row">                                   
                                    <div class='col-sm-4'>
                                        {!! Form::label('forZone', 'Zone:', ['class'=>'highlight required']) !!}
                                        <div class="form-group">
                                            <div class=''>
                                                {!! Form::select('forZone',[''=>'[Select]'],Null,['id'=>'forZone','class'=>'form-control','autocomplete'=>'off']) !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class='col-sm-4'>
                                        {!! Form::label('phase', 'Phase:', ['class'=>'highlight required']) !!}
                                       <div class="form-group">
                                           <div class=''>
                                            
                                <select id="phase" class="form-control" name="phase">
                                              
                                               <option value="">[Select]</option>
                                              
                            
                                </select>
                                            </div>
                                       </div>
                                   </div>
                                    <!--                                   <div class='col-sm-4'>
                                                  {!! Form::label('poststatus', 'Posting Status:', ['class'=>'highlight required']) !!}
                                                   <div class="form-group">
                                                       <div class=''>
                                                           {!! Form::select('poststatus',$dataAllforSMS['poststatus'],Null,['id'=>'poststatus','class'=>'form-control','placeholder'=>'[Select]','autocomplete'=>'off']) !!}
                                                       </div>
                                                   </div>
                                              </div>-->
                                    <div class='col-sm-4'>
                                        {!! Form::label('training_type', 'Type of Training:', ['class'=>'highlight required']) !!}
                                        <div class="form-group">
                                            <div class=''>
                                                @php
                                                    $training_type=App\tbl_training_type::where('status',1)->get();
                                                @endphp
                                                  <select name="training_type" id="training_type" class='form-control'> 
                                                <option value="">[Select]</option>
                                                @foreach ($training_type as $item)
                                                <option value="{{$item->training_code}}">{{$item->training_desc}}</option>
                                                @endforeach
                                                  </select>
                                               
                                            </div>
                                        </div>
                                    </div>           
                                   
                                </div>



                                <div class="row" > 

                                    {{-- <div class='col-sm-4 col-md-offset-2'>
                                        {!! Form::label('user_message_box', 'Your Message Box:', ['class'=>'highlight required']) !!}
                                          <span style="color:red;font-size: 12px;">[characters only: ,-.]</span>
                                        <div class="form-group">
                                            <div class=''>

                                                {!! Form::textarea('user_message_box',null,['class'=>'form-control', 'rows' => 5,'id'=>'user_message_box','cols' => 40]) !!}

                                            </div>
                                        </div>
                                    </div>  --}}
                                    <div class='col-sm-4' id='final_message'>
                                        {!! Form::label('final_message_box', 'Final Message Box:', ['class'=>'highlight']) !!}
                                        <div class="form-group">
                                            <div class=''>

                                                {!! Form::textarea('final_message_box',null,['class'=>'form-control','id'=>'final_message_box','rows' => 5, 'cols' => 40, 'readonly']) !!}

                                            </div>
                                        </div>
                                    </div> 
                                </div>



                                <div class="row" > 
                                    <div class='col-sm-12'>                    
                                        <div class="form-group text-right permit">                            	
                                            {{ Form::button('Submit', ['class' => 'btn btn-info', 'type' => 'submit','id'=>'submit']) }}
                                            {{ Form::button('Reset', ['class' => 'btn btn-success','type' => 'reset','id'=>'reset']) }}
                                        </div>
                                    </div>

                                </div>
                                {!! Form::close() !!}   
                                <div class="row"> 
                                    <div class='col-sm-12'>
                                        <div id='OrderDateNoDetails'></div>
                                    </div>
                                </div>  


                            </div>
                        </div>                              
                    </div> 


                </div>



            </div>
            <!--// form -->

        </section>
    </div>
</div>
<!-- Copyright -->
<script type="text/javascript">

    $(document).ready(function () {

        $("#reset").click(function () {
            location.reload(true);
        });
        getZoneDetails();
      
            $('#forZone').on('change', function () {
                var forZone=$('#forZone').val();
                if(forZone!=""){
                    getZonePhasedata();
                }
               
        });
       

        // $('#user_message_box').on('change', function () {
        //     getMessageShow();
        // });
$('#training_type').on('change', function () {
    var forZone=$('#forZone').val();
           var phase=$('#phase').val();
            var training_type=$('#training_type').val();
            if(phase!="" && training_type!="" && forZone!=""){
                getMessageShow();
        }
           
        });
        $('#phase').on('change', function () {
            var forZone=$('#forZone').val();
            var phase=$('#phase').val();
            var training_type=$('#training_type').val();
            if(phase!="" && training_type!="" && forZone!=""){
                getMessageShow();
        }
           
        });
        
$("#show_send_sms").click(function(){
       window.location.href = "send_sms"; 
  });
        $('#sms_entry_details')
                .bootstrapValidator({
                    message: 'This value is not valid',
                    feedbackIcons: {
                        valid: 'fa fa-check',
                        invalid: 'fa fa-times',
                        validating: 'fa fa-refresh'
                    },
                    fields: {
                        forZone: {
                            validators: {
                                notEmpty: {
                                    message: 'Training type is required'
                                }
                            }
                        },
                        poststatus: {
                            validators: {
                                notEmpty: {
                                    message: 'Training date is required'
                                }
                            }
                        },
                        user_message_box: {
                            validators: {
                                notEmpty: {
                                    message: 'Message is required'
                                }
                            }
                        },
                        training_type: {
                            validators: {
                                notEmpty: {
                                    message: 'Training Type is required'
                                }
                            }
                        },
                        phase: {
                            validators: {
                                notEmpty: {
                                    message: 'Phase is required'
                                }
                            }
                        }
                    }
                }).on('success.form.bv', function (e) {
            // alert('HI');
            // Prevent form submission
            e.preventDefault();
            var action = $(this).attr('action');

            var forZone = $("#forZone").val();
            var poststatus = $("#poststatus").val();
            var user_message_box = $("#user_message_box").val();
            var training_type = $('#training_type').val();
            var training_type_text = $("#training_type option:selected").text();
            var type = $(this).attr('method');
            var token = $("input[name='_token']").val();
                    var phase=$("#phase").val();
            var fd = new FormData();
            fd.append('forZone', forZone);
            fd.append('poststatus', poststatus);
            fd.append('user_message_box', user_message_box);
            fd.append('training_type', training_type);
            fd.append('training_type_text', training_type_text);

            fd.append('phase', phase);
            fd.append('_token', token);

             $(".se-pre-con1").show();
            $.ajax({
                type: type,
                url: action,
                data: fd,
                processData: false,
                contentType: false,
                dataType: "json",
                success: function (data) {

                    if (data.status == 1)
                    {

                         $(".se-pre-con1").hide();

                        var msg = data.countTotal + " Record(s) saved successfully";
                        $.confirm({
                            title: 'Success!',
                            type: 'green',
                            icon: 'fa fa-check',
                            content: msg,
                            buttons: {
                                ok: function () {
                                    location.reload(true);
                                }

                            }
                        });
                    } 

                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $(".se-pre-con1").hide();

                    $("#submit").removeAttr("disabled");
                    var msg = "";
                    if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                        msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                    } else {
                        if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                            msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                        } else {
                            msg += "Error(s):<strong><ul>";
                            $.each(jqXHR.responseJSON, function (key, value) {
                                msg += "<li>" + value + "</li>";
                            });
                            msg += "</ul></strong>";
                        }
                    }
                    $.alert({
                        title: 'Error!!',
                        type: 'red',
                        icon: 'fa fa-warning',
                        content: msg
                    });
                }
            });


        });


    });

    function getZoneDetails() {
        var forDist = $("#districtcd").val();
        var token = $("input[name='_token']").val();
        $(".se-pre-con").fadeIn("slow");
        $.ajax({
            url: "getZoneDetails",
            method: 'POST',
            data: {forDist: forDist, _token: token},
            success: function (data) {//alert(data.options);
                $(".se-pre-con").fadeOut("slow");
                $("select[name='forZone'").html('');
                if (data.status == 1)
                {
                    $("select[name='forZone'").append('<option value>[Select]</option>');
                    $.each(data.options, function (k, v)
                    {
                        $("select[name='forZone'").append('<option value=' + k + '>' + v + '</option>');
                    });
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $(".se-pre-con").fadeOut("slow");
                var msg = "";
                if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                    msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                } else {
                    if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                        msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                    } else {
                        msg += "Error(s):<strong><ul>";
                        $.each(jqXHR.responseJSON, function (key, value) {
                            msg += "<li>" + value + "</li>";
                        });
                        msg += "</ul></strong>";
                    }
                }
                $.alert({
                    title: 'Error!!',
                    type: 'red',
                    icon: 'fa fa-exclamation-triangle',
                    content: msg
                });
            }
        });
    }

    function getMessageShow() {
        var forZone = $("#forZone").val();
        var user_message_box = $("#user_message_box").val();
        var training_type = $('#training_type').val();
        var training_type_text = $("#training_type option:selected").text();
        var token = $("input[name='_token']").val();
        var phase=$("#phase").val();
        if ( forZone == "" || training_type == "" || phase=="") {
            return false;
        }
        $(".se-pre-con").fadeIn("slow");
        $.ajax({
            url: "getMessageShow",
            method: 'POST',
            data: {phase:phase,forZone: forZone, user_message_box: user_message_box, training_type: training_type, training_type_text: training_type_text, _token: token},
            success: function (data) {//alert(data.options);
                $(".se-pre-con").fadeOut("slow");
                $('#final_message_box').html('');
                if (data.status == 1)
                {
                    $('#final_message_box').html(data.options);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $(".se-pre-con").fadeOut("slow");
                var msg = "";
                if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                    msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                } else {
                    if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                        msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                    } else {
                        msg += "Error(s):<strong><ul>";
                        $.each(jqXHR.responseJSON, function (key, value) {
                            msg += "<li>" + value + "</li>";
                        });
                        msg += "</ul></strong>";
                    }
                }
                $.alert({
                    title: 'Error!!',
                    type: 'red',
                    icon: 'fa fa-exclamation-triangle',
                    content: msg
                });
            }
        });
    }

    function getZonePhasedata(){
  var zone = $("#forZone").val();
  $("#rw").hide();
   var token = $("input[name='_token']").val();
  // $(".se-pre-con").fadeIn("slow");
   $.ajax({
       url: "getZonePhasedata",
       method: 'POST',
       data: {zone: zone, _token: token},
       success: function (data) {//alert(data.options);
       //   $(".se-pre-con").fadeOut("slow");
          $("select[name='phase'").html('');
          
          if(data.status==1)
          {
            $("select[name='phase'").append('<option value>[Select]</option>');
            $.each(data.options,function (k, v)
            {
                $("select[name='phase'").append('<option value='+k+'>'+v+'</option>');
            });
            //$("select[name='subdivision'").html(data.options);
             // change_officeName();
             
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
       //  $(".se-pre-con").fadeOut("slow");
           var msg = "";
           if (jqXHR.status !== 422 && jqXHR.status !== 400) {
               msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
           } else {
               if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                   msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
               } else {
                   msg += "Error(s):<strong><ul>";
                   $.each(jqXHR.responseJSON, function (key, value) {
                       msg += "<li>" + value + "</li>";
                   });
                   msg += "</ul></strong>";
               }
           }
           $.alert({
               title: 'Error!!',
               type: 'red',
               icon: 'fa fa-warning',
               content: msg
           });
         },complete: function(){
         
         }
   });
}

</script>
@stop

