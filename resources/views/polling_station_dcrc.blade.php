@extends('layouts.master')
@section('content')
<!--// top-bar -->
 <div class="wrapper">
<!-- Sidebar Holder -->
@include('layouts.sidebar')

<!-- Page Content Holder -->
<div id="content">           
<section class="tables-section">
    <nav aria-label="breadcrumb" style="width:100%;">
        <div class="breadcrumb pagehead1">
           
                {!! Form::button('<i class="fa fa-desktop"></i> Add Polling Station-wise DCRC Venue',['class' => 'btn btn-primary-header add-new-button']) !!}
            
            <div class="col-md-offset-8 pull-right">
<!--                {!! Form::button('Academic Year:&nbsp;',['id'=>'show_academic','class' => 'btn btn-primary-year']) !!}-->
                
            </div>
                <div class="clearfix"></div>
        </div>
    </nav>

    <!-- form -->
<div class="outer-w3-agile">
  {!! Form::open(['url' => 'PSDCRCUpdate', 'name' => 'PSDCRCUpdate', 'class' =>'request-info clearfix form-horizontal', 'id' => 'PSDCRCUpdate', 'method' => 'post','role'=>'','files' => true]) !!}
  {!! Form::hidden('districtcd', session()->get('districtcd_ppds'),['id'=>'districtcd']) !!}
  
  <div class="panel-group" id="accordion5">                                                              
   <div class="panel panel-default">
    <div class="panel-heading1">
     <h6 class="panel-title">
         <a data-toggle="#" data-parent="#accordion5" href="#collapsePV"> <span class="fa fa-minus"></span> <span class="highlight">Add Polling Station-wise DCRC Venue </span></a>
     </h6>
    </div>
    <div id="collapsePV" class="panel-collapse collapse5">
      <div class="panel-body">                                   
       <div class="row">  
         <div class='col-sm-4'>
            {!! Form::label('forZone', 'For Zone:', ['class'=>'highlight required']) !!}
           <div class="form-group">
               <div class=''>
                   {!! Form::select('forZone',[''=>'[Select]'],Null,['id'=>'forZone','class'=>'form-control','autocomplete'=>'off']) !!}
               </div>
           </div>          
        </div> 

        <div class='col-sm-4'>
            {!! Form::label('phase', 'Phase:', ['class'=>'highlight required']) !!}
           <div class="form-group">
               <div class=''>
                    <select id="phase" class="form-control" name="phase">
                        <option value="">[Select]</option>
                    </select>
                </div>
           </div>
        </div> 


        <div class='col-sm-4'>
              {!! Form::label('assembly', 'Assembly:', ['class'=>'highlight required']) !!}
              <div class="form-group">
                  <div class=''>
                      {!! Form::select('assembly',[''=>'[Select]'],null,['id'=>'assembly','class'=>'form-control','autocomplete'=>'off']) !!}
                  </div>
              </div>
          </div> 
        <div class='col-sm-4'>
               {!! Form::label('dcvenue', 'DCRC Venue:', ['class'=>'highlight required']) !!}
              <div class="form-group">
                  <div class=''>
                      {!! Form::select('dcvenue',[''=>'[Select]'],Null,['id'=>'dcvenue','class'=>'form-control','autocomplete'=>'off']) !!}
                  </div>
              </div>
          </div>
                 
       {{-- / </div> --}}
          
       {{-- <div class="row">                                    --}}
           <div class='col-sm-4'>
               {!! Form::label('frompsno', 'From PS No:', ['class'=>'highlight']) !!}
              <div class="form-group">
                  <div class=''>           
                  {!! Form::text('frompsno',null,['id'=>'frompsno','class'=>'form-control','autocomplete'=>'off','maxlength'=>'4']) !!}
                 </div>
              </div>
          </div>
            
          <div class='col-sm-4'>
              {!! Form::label('topsno', 'To PS No:', ['class'=>'highlight']) !!}
              <div class="form-group">
                  <div class=''>
                      {!! Form::text('topsno',null,['id'=>'topsno','class'=>'form-control','autocomplete'=>'off','maxlength'=>'4']) !!}
                  </div>
              </div>
          </div>
       </div>
        
                  
      <div class="row"> 
            <div class='col-sm-12'>
                <div id='assemblyDetails'></div>
            </div>
         
      </div> 
      <div class="row" id="rw" style="display: none;">                                   
           
           <div class='col-sm-12' > 
<!--               {!! Form::label('', '', ['class'=>'highlight']) !!}-->
                <div class="form-group text-right permit" >                            	
                    {{ Form::button('Submit', ['class' => 'btn btn-info', 'type' => 'submit','id'=>'Submit']) }}
                    {{ Form::button('Reset', ['class' => 'btn btn-success' ,'type' => 'reset','id'=>'reset']) }}
                </div>
           </div>
                
       </div>     
    </div>                              
   </div>
  </div>  
  </div>
 {!! Form::close() !!}             
</div>
<!--// form -->

</section>
</div>
</div>
<!-- Copyright -->
<script type="text/javascript">
$(document).ready(function () {
char_num("#frompsno");
char_num("#topsno");

 $("#reset").click(function () {
  location.reload(true);  
});
 getZoneDetails(); 
 $('select[name="forZone"]').on('change', function () {
    getZonePhasedata();
    // getAssemblyDetails();
    var assembly = $("#assembly").val();
    if(assembly != ""){
     // get_DcrcPSDetails();
    }
 });

 $('select[name="phase"]').on('change', function () {
    getAssemblyDetails();
 });
 $('select[name="assembly"]').on('change', function () {
    getDCVenueDetails();
        
 });
  $('#frompsno').on('change', function () {
      var forZone = $("#forZone").val();
      var assembly = $("#assembly").val();
      if(assembly != "" && forZone!= ""){
        get_DcrcPSDetails();  
      }
 });
 $('#topsno').on('change', function () {
    var forZone = $("#forZone").val();
      var assembly = $("#assembly").val();
      if(assembly != "" && forZone!= ""){
        get_DcrcPSDetails();  
      }
 });
 $('#PSDCRCUpdate').bootstrapValidator({

        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {

            forZone: {
                validators: {
                    notEmpty: {
                        message: 'Zone is required'
                    }
                }
            },
            phase: {
                validators: {
                    notEmpty: {
                        message: 'Phase is required'
                    }
                }
            },
            dcvenue: {
                validators: {
                    notEmpty: {
                        message: 'DC Venue is required'
                    }
                }
            },
            assembly: {
                validators: {
                    notEmpty: {
                        message: 'Assembly is required'
                    }
                }
            }
            
        }

    }).on('success.form.bv', function (e) {
        e.preventDefault();
        // alert("hi");
        var action = $(this).attr('action');
        var type = $(this).attr('method');
        var formData = new FormData($(this)[0]);
       $(".se-pre-con").fadeIn("slow");
        $.ajax({
            type: type,
            url: action,
            data: formData,
            processData: false,
            contentType: false,
            dataType: "json",
            success: function (data) {
                $(".se-pre-con").fadeOut("slow");

                if (data.status == 1) { 
                    var msg="DCRC saved in "+data.options+" Polling Station";
                    $.confirm({
                       title: 'Success!',
                       type: 'green',
                       icon: 'fa fa-check',
                       content: msg,
                       buttons: {
                           ok: function () {     
                               location.reload(true);
                           }
                       }
                   });
                } 
            },
            error: function (jqXHR, textStatus, errorThrown) {
            $(".se-pre-con").fadeOut("slow");
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-exclamation-triangle',
                  content: msg
              });
              $("#Submit").attr('disabled',false);
           }
        });
    });
});

function getZonePhasedata() {
            var zone = $("#forZone").val();
            $("#rw").hide();
            var token = $("input[name='_token']").val();
            // $(".se-pre-con").fadeIn("slow");
            $.ajax({
                url: "getZonePhasedata",
                method: 'POST',
                data: {
                    zone: zone,
                    _token: token
                },
                success: function(data) { //alert(data.options);
                    //   $(".se-pre-con").fadeOut("slow");
                    $("select[name='phase'").html('');

                    if (data.status == 1) {
                        $("select[name='phase'").append('<option value>[Select]</option>');
                        $.each(data.options, function(k, v) {
                            $("select[name='phase'").append('<option value=' + k + '>' + v +
                                '</option>');
                        });
                        //$("select[name='subdivision'").html(data.options);
                        // change_officeName();

                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    //  $(".se-pre-con").fadeOut("slow");
                    var msg = "";
                    if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                        msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                    } else {
                        if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                            msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                        } else {
                            msg += "Error(s):<strong><ul>";
                            $.each(jqXHR.responseJSON, function(key, value) {
                                msg += "<li>" + value + "</li>";
                            });
                            msg += "</ul></strong>";
                        }
                    }
                    $.alert({
                        title: 'Error!!',
                        type: 'red',
                        icon: 'fa fa-warning',
                        content: msg
                    });
                }
                // complete: function() {
                //     getZonePhaseWiseAssembly();
                // }
            });
        }





function get_DcrcPSDetails(){
   var forDist = $("#districtcd").val();
   var assembly = $("#assembly").val(); 
   var forZone = $("#forZone").val();
   var phase = $("#phase").val();
   var frompsno = $("#frompsno").val();
   var topsno = $("#topsno").val();
   var token = $("input[name='_token']").val();
   $("#rw").hide();
    $(".se-pre-con").fadeIn("slow");
    $.ajax({
        type: 'POST',
        url: 'getPollingStationDetailsForDCRC',
        data: {forDist:forDist,forZone:forZone,phase:phase,assembly:assembly,frompsno:frompsno,topsno:topsno, _token:token},
        dataType: 'json',
        success: function (response) {
        $(".se-pre-con").fadeOut("slow");
        $("#assemblyDetails").html("");
        var str='<div style="height:400px;overflow:scroll;">';
        str+='<input type="hidden" id="row_count" name="row_count" value="'+response.options.length+'"> '
        str+= "<table class='table table-bordered table-striped' width='100%' style='border-top: 2px solid #4cae4c;'>";
        str += ' <tr style="background-color: #f5f8fa"><th align="left">&nbsp;<input type="checkbox" id="myCheckall" name="myCheckall"  value="1"  class="myCheckall">&nbsp;</th><th align="left">&nbsp;Assembly </th><th>&nbsp;Member </th><th>&nbsp;Gender </th><th>&nbsp;PS No </th><th>&nbsp;PS Name </th><th>&nbsp;DCRC</th></tr>';
//         str += '<tr style=" background-color: #1ABB9C; color: #FFFFFF; font-weight: bold;"><td align="left"><input type="checkbox" id="myCheckall" name="myCheckall"  value="1"  class="submenu">&nbsp; Check All</td><td align="center" width="30%"><input type="checkbox" id="view_all" class="flat"></td></tr>';
         var sub_length = response.options.length;
        // alert(sub_length);
         for (var i = 0; i < sub_length; i++)
         {
            var c = parseInt(i) + 1;
            var gender;
            if(response.options[i].gender=='M'){
                gender="Male";
            }else{
                gender="Female";
            }
             str += '<tr><td align="left" width="5%">&nbsp;';

             str+='<input type="checkbox" id="myCheck' + c + '" name="myCheck' + c + '"  value="'+ response.options[i].forassembly+'-'+response.options[i].member+'-'+response.options[i].gender+'-'+ response.options[i].psno+'-'+ response.options[i].psfix+'"  class="submenu" >';                             
             str+='</td><td align="left" width="8%">&nbsp;' + response.options[i].forassembly +'</td>';
             str+='</td><td align="left" width="7%">&nbsp;' + response.options[i].member + '</td>';
             str+='</td><td align="left" width="10%">&nbsp;' + gender + '</td>';
             str+='</td><td align="left" width="8%">&nbsp;' + response.options[i].psno + ''+response.options[i].psfix +'</td>';
             str+='</td><td align="left" width="54%">&nbsp;' + response.options[i].psname  + '</td>';
             str+='<td align="left" width="8%">';
             if(response.options[i].dcrccd==null)
             {
                str+='&nbsp;<span class="fa fa-exclamation-triangle" style="color: red;"></span></td>';
             }
             else{
               str+='&nbsp;<span class="fa fa-check" style="color: green;"></span></td>';  

             }
             str+='</td></tr>'; // return empty
        }
        str += "</table>";
        str += "</div>";
        $("#assemblyDetails").append(str);
        
          $("#myCheckall").change(function(){ 
            var status = this.checked;
            if(status==true){
                $("#rw").show();
                scrollToElement($('#rw'));
            }else if(status==false){
                $("#rw").hide();
            }
            
            $('.submenu').each(function(){
                this.checked = status;     
            });
        });
        $('.submenu').change(function(){
            if($('.submenu:checked').length=='0'){
                $("#rw").hide();
            }else{
                $("#rw").show();
            }
            if(this.checked == false){
                $("#myCheckall")[0].checked = false; 
            }
            if ($('.submenu:checked').length == $('.submenu').length ){ 
                $("#myCheckall")[0].checked = true;
            }
        });
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $(".se-pre-con").fadeOut("slow");
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-exclamation-triangle',
                  content: msg
              });
         }
    });
}

function getZoneDetails(){
     var forDist = $("#districtcd").val();
     var token = $("input[name='_token']").val();
     $(".se-pre-con").fadeIn("slow");
     $.ajax({
       url: "getZoneDetails",
       method: 'POST',
       data: {forDist: forDist, _token: token},
       success: function (data) {//alert(data.options);
         $(".se-pre-con").fadeOut("slow");
          $("select[name='forZone'").html('');
          if(data.status==1)
          {
             $("select[name='forZone'").append('<option value>[Select]</option>');
            $.each(data.options,function (k, v)
            {
                $("select[name='forZone'").append('<option value='+k+'>'+v+'</option>');
            });
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
            $(".se-pre-con").fadeOut("slow");
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-exclamation-triangle',
                  content: msg
              });
         }
   });
}
function getAssemblyDetails(){
    var forZone = $("#forZone").val();
    var phase = $("#phase").val();
     var token = $("input[name='_token']").val();
     $(".se-pre-con").fadeIn("slow");
     $.ajax({
       url: "get_boothtaging_AssemblyDetails",
       method: 'POST',
       data: {forZone: forZone,phase:phase, _token: token},
       success: function (data) {//alert(data.options);
         $(".se-pre-con").fadeOut("slow");
          $("select[name='assembly'").html('');
          if(data.status==1)
          {
              $("select[name='assembly'").html(data.options);
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
            $(".se-pre-con").fadeOut("slow");
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-exclamation-triangle',
                  content: msg
              });
         }
   });
}
function getDCVenueDetails(){
    var forZone = $("#forZone").val();
     var phase = $("#phase").val();
     var assembly = $("#assembly").val();
     var token = $("input[name='_token']").val();
     $("#rw").hide();
    // $(".se-pre-con").fadeIn("slow");
     $.ajax({
       url: "getDCAssemblyVenueDetails",
       method: 'POST',
       data: {forZone: forZone,phase:phase,  assembly:assembly, _token: token},
       success: function (data) {//alert(data.options);
        // $(".se-pre-con").fadeOut("slow");
          $("select[name='dcvenue'").html('');
          if(data.status==1)
          {
             $("select[name='dcvenue'").append('<option value>[Select]</option>');
            $.each(data.options,function (k, v)
            {
                $("select[name='dcvenue'").append('<option value='+k+'>'+v+'</option>');
            });
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
            $(".se-pre-con").fadeOut("slow");
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-exclamation-triangle',
                  content: msg
              });
         },complete: function () {
            get_DcrcPSDetails();
         }
   });
}
function scrollToElement(ele) {
    $('html, body').animate({
        scrollTop: ele.offset().top - 60
    }, 1000);
}
</script>
@stop