
@extends('layouts.master')
@section('content')

 <div class="wrapper">
<!-- Sidebar Holder -->
@include('layouts.sidebar')

<!-- Page Content Holder -->
<div id="content">           
<section class="tables-section">
    <nav aria-label="breadcrumb" style="width:100%;">
        <div class="breadcrumb pagehead1">
           
                {!! Form::button('<i class="fa fa-desktop"></i> Epic No Search',['class' => 'btn btn-primary-header add-new-button']) !!}
            
            <div class="col-md-offset-8 pull-right">
<!--                {!! Form::button('Academic Year:&nbsp;',['id'=>'show_academic','class' => 'btn btn-primary-year']) !!}-->
                
            </div>
            <div class="clearfix"></div>
        </div>
    </nav>

    <!-- form -->
<div class="outer-w3-agile">
{!! Form::open(['url' => 'save_personnel_new', 'name' => 'personnel_save', 'class' =>'request-info clearfix form-horizontal', 'id' => 'personnel_save', 'method' => 'post','role'=>'','files' => true]) !!}
  {!! Form::hidden('districtcd', session()->get('districtcd_ppds'),['id'=>'districtcd']) !!}
  
  
  <div class="panel-group" id="accordion5" style="padding-top: 0px;">                                                              
    <div class="panel panel-default">
     <div class="panel-heading1">
     <h6 class="panel-title">
         <a data-toggle="#" data-parent="#accordion5" href="#collapseUV"> <span class="fa fa-minus"></span> <span class="highlight">Epic No Search</span> </a>
     </h6>
     </div>
     <div id="collapseUV" class="panel-collapse collapse5">
      <div class="panel-body">                                   
        
      
         <div class="row"> 
            <div class='col-sm-12'>
          
          <div >
                <table class="table table-striped table-bordered table-hover " width="100%">
                <thead>
                <tr>
                <td ><b>Field</b></td>
                <td ><b>Details</b></td>

                </tr></thead>
                
                <tbody>
                
               
                    
                
			   
               <tr style="background-color:yellowgreen;color:white;"><td>Type EPIC No. and Press TAB<br>to get details from Search Engine </td><td><div class="form-group"><input type="text" class="form-control" name="epic" value="" onblur="return show_Det(this.value)"></div></td></tr>
             <tr><td colspan="2"><div class="form-group" style="background-color: #210252;color: white;font-size: 18px;display: none;" id="change_div2"></div></td></tr>
               
               
                
                
                </tbody>
                </table>
                </div>
            </div>
        </div>  
        
         
      </div>
    </div>                              
  </div> 
 
  
  </div>

 
 {!! Form::close() !!}             
</div>
<!--// form -->

</section>
</div>
</div>
<!-- Copyright -->
<script type="text/javascript">
function show_Det(epic){
      $.ajax({
                  type: 'post',
                  url: 'get_epic_det',
                  data: {'epic': epic, '_token': $('input[name="_token"]').val()},
                  dataType: 'json',
                  success: function (data) {
                   if(data.length!=0){
                      var str=" Name:"+data[0].Fm_NameEn+" "+data[0].LastNameEn+", AC NO:"+data[0].AC_NO+", Part No:"+data[0].PART_NO+", SL NO:"+data[0].SLNOINPART+"." 
                  // $("#change_div1").hide();
                   $("#change_div2").show();
                   $("#change_div2").html(str);
        }else{
             //$("#change_div1").hide();
                  // $("#change_div2").hide();
                   $("#change_div2").show();
                   var str="No Record Found"
                   $("#change_div2").html(str);
        }
                  }
                });  
    }





   

        
</script>
@stop

