@extends('layouts.master')
@section('content')
<style>
    .jconfirm-box{
        width: 600px;
        margin-left: -200px !important; 
    }
</style>
<!--// top-bar -->
<div class="wrapper">
    <!-- Sidebar Holder -->
    @include('layouts.sidebar')
    <!-- Page Content Holder -->
    <div id="content">           
        <section class="tables-section">

            <div class="breadcrumb pagehead1">
               <span class="headbrand"><i class="fa fa-desktop"></i> List of Assembly Subdivision Wise Priority
                    <span class="scoop-mcaret1"></span>
                </span>&nbsp;&nbsp;
                &nbsp;{!! Form::button(' Add Assembly Subdivision Wise Priority&nbsp;<i class="fa fa-plus-circle"></i>',['id'=>'add_second_training_venue','class' => 'btn btn-primary-year add-new-button']) !!}  
          
                <div class="clearfix"></div>
            </div>
            </nav>
            <div class="outer-w3-agile">
                <div class="panel-group" id="accordion5">                                                              
                    <div class="panel panel-default">
                        <div class="panel-heading1">
                            <h6 class="panel-title">
                                <a data-toggle="#" data-parent="#accordion5" href="#collapsePV"> <span class="fa fa-minus"></span> <span class="highlight">List of Group Training Venue</span></a>
                            </h6>
                        </div>
                        <div id="collapsePV" class="panel-collapse collapse5">
                            <div class="panel-body">                                   
                                <div class="row">

                                    {{-- <div class='col-sm-4'>
                                        {!! Form::label('subdivision', 'Sub Division:', ['class'=>'highlight ']) !!}
                                        <div class="form-group">
                                            <div class=''>
                                                {!! Form::select('subdivision',[''=>'[ALL]'],Null,['id'=>'subdivision','class'=>'form-control','autocomplete'=>'off']) !!}
                                            </div>
                                        </div>
                                    </div> 
                                    <div class='col-sm-4'>
                                        {!! Form::label('venuename', 'Venue:', ['class'=>'highlight ']) !!}
                                        <div class="form-group">
                                            <div class=''>
                                                {!! Form::select('venuename',[''=>'[ALL]'],Null,['id'=>'venuename','class'=>'form-control','autocomplete'=>'off']) !!}
                                            </div>
                                        </div>
                                    </div> --}}
                                </div>
                            </div>

                        </div>
                        <div class="form-horizontal">
                            <div class="form-group row">
                                {{csrf_field()}}
                                <div class="datatbl table-responsive" style="width: 96%;margin-left: 20px;">
                                    {!! Form::hidden('districtcd', session()->get('districtcd_ppds'),['id'=>'districtcd']) !!}
                                    <table class="table table-striped table-bordered table-hover notice-types-table" id="datatable-table" style="">
                                        <thead>
                                            <tr>
                                                <th>SL#</th>
                                                <th>Assembly Code</th>
                                                <th>Assembly Name</th>
                                                <th>Subdivision</th>
                                                <th>Priority</th>
                                                <th>Action</th>
                                            </tr>

                                        </thead>
                                        <tbody></tbody>
                                        <!-- Table Footer -->

                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </section>
    </div>
</div>
<script>
    $(document).ready(function () {
        $("#add_second_training_venue").click(function () {
            window.location.href = "assembly_subdivision_wise_priority";
        });

        create_table();
        // change_subdivision();
        // $('select[name="subdivision"]').on('change', function () {
        //      var subdivision = $('#subdivision').val();
        //     if (subdivision == '') {
        //         create_table();
        //     } else {
        //         change_venueName();
        //     }
        // });
        // $('select[name="venuename"]').on('change', function () {

        //     create_table();
        // });
        // $('#add-new-button').click(function () {
        //     //$(".se-pre-con").fadeIn("slow"); 
        // });
        
        var table = "";
        table = $('#datatable-table').DataTable();
       
        table.on('draw.dt', function () {
    // table.fnSetColumnVis( 1, false, false ); 
            $('.edit-button').click(function () {
                var value_all = this.id;
                // alert(value_all);
                $.ajax({
                    type: 'post',
                    url: 'editAssemblySubdivisionWisePriority',
                    data: {'data': value_all, '_token': $('input[name="_token"]').val()},
                    dataType: 'json',
                    success: function (datam) {
                        var edit_val = datam.rec;
                        // getAll_Assembly();
                        // console.log(edit_val);
                        // console.log(datam.assemblydata[0].assemblycd);
                        var table_edit = '<form id="edit_first" action="" method="post" class="form-horizontal">';
                            table_edit += '<input type="hidden" value="' + edit_val.id + '" id="edit_code">';
                            table_edit += '<div class="form-group">';
                            table_edit += '<label class="control-label col-md-4 required">Assembly Code:</label>';
                            table_edit += '<div class="col-md-6"><input type="text" class="form-control"  id="assembly_code" name="assembly_code" style="margin-bottom: 2px;" value='+edit_val.assemblycd+' disabled></div>';
                            

                            table_edit += '<label class="control-label col-md-4 required">Subdivision:</label>';
                            table_edit += '<div class="col-md-6"><input type="text" class="form-control"  id="subdivision" name="subdivision" style="margin-bottom: 2px;" value='+edit_val.subdivision+' disabled></div>';
                            
                            table_edit += '<label class="control-label col-md-4 required">Priority:</label>';

                            
                            table_edit += '<div class="col-md-6"><select class="form-control"  id="priority" name="priority" style="margin-bottom: 2px;">';
                                table_edit += "<option value=''>[Select]</option>";
                                var selected="";
                                for (var i=1;i<9;i++){
                                if(i==edit_val.priority){
                                    selected='selected';
                                }
                                else{
                                    selected="";
                                }
                                table_edit += '<option value="'+i+'"'+selected+' >'+i+'</option>';
                    }
                                
                                // += "<option>" + edit_val.priority + "</option>";
                                table_edit += "</select>"
                            table_edit += '</div>';
                            table_edit += '</form>';

                            var jc = $.confirm({
                                title: '<span style="font-size: 17px;">Assembly Subdivision Wise Priority :</span>',
                                content: table_edit,
                                type: 'green',
                                icon: 'fa fa-edit',
                                typeAnimated: true,
                                buttons: {
                                    edit: {
                                        btnClass: 'btn-primary',
                                        action: function () {
                                            return $.ajax({
                                                url: 'update_assmbly_eise_prioriy',
                                                dataType: 'json',
                                                method: 'post',
                                                data: {'edit_code':$("#edit_code").val(),'priority': $("#priority").val(),'_token': $("input[name='_token']").val()},

                                            }).done(function (response) {
                                                jc.hideLoading(true);
                                                if (response.status == 1) {
                                               var msg_suc = "<strong>Record edited successfuly</strong>";
                                                 $.confirm({
                                                    title: 'Success!',
                                                    type: 'green',
                                                    icon: 'fa fa-check',
                                                    content: msg_suc,
                                                    buttons: {
                                                        ok: function () {  
                                                            //table.ajax.reload();
                                                            create_table();
                                                        }

                                                    }
                                                });
                                            }
                                            }).fail(function (jqXHR, textStatus, errorThrown) {
                                            var msg = "";
                                            if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                                                msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                                            } else {
                                                if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                                                    if (jqXHR.responseJSON.exception_code == 23000) {
                                                        msg += "Record exist in another table.You can't change this record.";
                                                    } else {
                                                        msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                                                    }
                                                } else {
                                                    msg += "Error(s):<strong><ul>";
                                                    $.each(jqXHR.responseJSON, function (key, value) {
                                                        msg += "<li>" + value + "</li>";
                                                    });
                                                    msg += "</ul></strong>";
                                                }
                                            }
                                            $.alert({
                                                title: 'Error!!',
                                                type: 'red',
                                                icon: 'fa fa-warning',
                                                content: msg,
                                            });
                                        });
                                        }
                                        
                                    },
                                    close: function () {
                                }
                                }
                            });
                    }
                });
            });
            $('.delete-button').click(function () {
                // alert("hi");
            //    var reply = $.confirm('Are you sure to delete the record?');
            //    if (!reply) {
            //        return false;
            //            }
                var value_all = this.id;
                // alert(value_all);
                // var res = value_all.split("/");
                $.ajax({
                    type: 'post',
                    url: 'deleteAssemblysubdivisionWisPriority',
                    data: {'data': value_all,'_token': $('input[name="_token"]').val()},
                    dataType: 'json',
                    success: function (datam) {
                        $(".se-pre-con").fadeOut("slow");
                        //alert(datam.count);
                        // if (datam.count > 0) {
                        //     $.alert({
                        //         title: 'Error!!',
                        //         type: 'red',
                        //         icon: 'fa fa-exclamation-triangle',
                        //         content: "Record exist in another table.You can't delete this record."
                        //     });
                        //     return false;
                        // }
                        $.confirm({
                            title: 'Attention!',
                            content: 'Do you want to delete this record?',
                            type: 'orange',
                            icon: 'fa fa-exclamation-triangle',
                            buttons: {
                                confirm: function () {
                                    location.reload();
                                    create_table();
                                    // var data_new = value_all;
                                    // //alert(data_new);
                                    // $.ajax({
                                    //     type: 'post',
                                    //     url: 'delete_second_training_venue',
                                    //     // data: {'data': '_token': $('input[name="_token"]').val()},
                                    //     dataType: 'json',
                                    //     success: function (datam) {
                                    //         var msg = "<strong>Record deleted successfully</strong>";
                                    //         $.confirm({
                                    //             title: 'Success!',
                                    //             type: 'green',
                                    //             icon: 'fa fa-check',
                                    //             content: msg,
                                    //             buttons: {
                                    //                 ok: function () {  
                                    //                     //table.ajax.reload();
                                    //                     create_table();
                                    //                 }
                                    //             }
                                    //         });
                                    //     }
                                        // error: function (jqXHR, textStatus, errorThrown) {
                                        //     $(".se-pre-con").fadeOut("slow");
                                        //     var msg = "";
                                        //     if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                                        //         msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                                        //     } else {
                                        //         if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                                        //             if (jqXHR.responseJSON.exception_code == 23000) {
                                        //                 msg += "Record exist in another table.You can't delete this record.";
                                        //             } else {
                                        //                 msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                                        //             }
                                        //         } else {
                                        //             msg += "Error(s):<strong><ul>";
                                        //             $.each(jqXHR.responseJSON, function (key, value) {
                                        //                 msg += "<li>" + value + "</li>";
                                        //             });
                                        //             msg += "</ul></strong>";
                                        //         }
                                        //     }
                                        //     $.alert({
                                        //         title: 'Error!!',
                                        //         type: 'red',
                                        //         icon: 'fa fa-warning',
                                        //         content: msg,
                                        //     });
                                        // }
                                    // });
                                },
                                cancel: function () {

                                }
                            }
                        });
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        $(".se-pre-con").fadeOut("slow");
                        var msg = "";
                        if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                            msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                        } else {
                            if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                                if (jqXHR.responseJSON.exception_code == 23000) {
                                    msg += "Record exist in another table.You can't delete this record.";
                                } else {
                                    msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                                }
                            } else {
                                msg += "Error(s):<strong><ul>";
                                $.each(jqXHR.responseJSON, function (key, value) {
                                    msg += "<li>" + value + "</li>";
                                });
                                msg += "</ul></strong>";
                            }
                        }
                        $.alert({
                            title: 'Error!!',
                            type: 'red',
                            icon: 'fa fa-warning',
                            content: msg,
                        });
                    }
                });


            // });

        });
    });
    // function change_subdivision() {
    //         var forDist = $("#districtcd").val();
    //         var token = $("input[name='_token']").val();
    //         // $(".se-pre-con").fadeIn("slow");
    //         $.ajax({
    //             url: "getSubdivisionData",
    //             method: 'POST',
    //             data: {forDist: forDist, _token: token},
    //             success: function (data) {//alert(data.options);
    //                 //   $(".se-pre-con").fadeOut("slow");
    //                 $("select[name='subdivision'").html('');

    //                 if (data.status == 1)
    //                 {
    //                     $("select[name='subdivision'").append('<option value>[Select]</option>');
    //                     $.each(data.options, function (k, v)
    //                     {
    //                         $("select[name='subdivision'").append('<option value=' + k + '>' + v + '</option>');
    //                     });
    //                     //$("select[name='subdivision'").html(data.options);
    //                     // change_officeName();
    //                     // getMemberDetails();
    //                 }
    //             },
    //             error: function (jqXHR, textStatus, errorThrown) {
    //                 $(".se-pre-con").fadeOut("slow");
    //                 var msg = "";
    //                 if (jqXHR.status !== 422 && jqXHR.status !== 400) {
    //                     msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
    //                 } else {
    //                     if (jqXHR.responseJSON.hasOwnProperty('exception')) {
    //                         msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
    //                     } else {
    //                         msg += "Error(s):<strong><ul>";
    //                         $.each(jqXHR.responseJSON, function (key, value) {
    //                             msg += "<li>" + value + "</li>";
    //                         });
    //                         msg += "</ul></strong>";
    //                     }
    //                 }
    //                 $.alert({
    //                     title: 'Error!!',
    //                     type: 'red',
    //                     icon: 'fa fa-exclamation-triangle',
    //                     content: msg
    //                 });
    //             },
    //             complete: function () {
    //                 create_table();
    //             }
    //         });
    //     }
    //     function change_venueName() {
    //         var subdivision = $("#subdivision").val();
    //         var token = $("input[name='_token']").val();
    //         //$(".se-pre-con").fadeIn("slow");
    //         $.ajax({
    //             url: "getVenueName_second",
    //             method: 'POST',
    //             data: {subdivision: subdivision, _token: token},
    //             success: function (data) {//alert(data.options);
    //                 $(".se-pre-con").fadeOut("slow");
    //                 $("select[name='venuename'").html('');

    //                 if (data.status == 1)
    //                 {
    //                     $("select[name='venuename'").append('<option value>[Select]</option>');
    //                     $.each(data.options, function (k, v)
    //                     {
    //                         $("select[name='venuename'").append('<option value=' + k + '>' + v + '</option>');
    //                     });
    //                 }
    //             },
    //             error: function (jqXHR, textStatus, errorThrown) {
    //                 $(".se-pre-con").fadeOut("slow");
    //                 var msg = "";
    //                 if (jqXHR.status !== 422 && jqXHR.status !== 400) {
    //                     msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
    //                 } else {
    //                     if (jqXHR.responseJSON.hasOwnProperty('exception')) {
    //                         msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
    //                     } else {
    //                         msg += "Error(s):<strong><ul>";
    //                         $.each(jqXHR.responseJSON, function (key, value) {
    //                             msg += "<li>" + value + "</li>";
    //                         });
    //                         msg += "</ul></strong>";
    //                     }
    //                 }
    //                 $.alert({
    //                     title: 'Error!!',
    //                     type: 'red',
    //                     icon: 'fa fa-exclamation-triangle',
    //                     content: msg
    //                 });
    //             },
    //             complete: function () {
    //                 create_table();
    //             }
    //         });
    //     }

     });
        function create_table() {
            var table = "";
            var token = $('input[name="_token"]').val();


            $("#datatable-table").dataTable().fnDestroy();
            table = $('#datatable-table').DataTable({
                "processing": true,
                "serverSide": true,
                "dom": 'lBfrtip',
                "buttons": [
                //'copy', 'csv', 'excel', 'pdf'
                {
                    extend: 'collection',
                    text: 'Export',
                    buttons: [
                        {
                            extend: 'copyHtml5',
                            exportOptions: {
                                columns: ':visible'
                            }
                        },
                        {
                            extend: 'excelHtml5',
                            exportOptions: {
                                columns: ':visible'
                            }
                        },
                        {
                            extend: 'pdfHtml5',
                            exportOptions: {
                                columns: ':visible'
                            }
                        },
                        {
                            extend: 'print',
                            exportOptions: {
                                columns: ':visible'
                            }
                        },
                        {
                            extend: 'csvHtml5',
                            exportOptions: {
                                columns: ':visible'
                            }
                        }

                    ]

                },
                'colvis'
            ],
                "ajax": {
                    url: "assemblySubdivisionWisePriorityDatatable",
                    type: "post",
                    data: {'_token': $('input[name="_token"]').val()},
                    dataSrc: "record_details",
                    error: function (jqXHR, textStatus, errorThrown) {
                        var msg = "";
                        if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                            msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                        } else {
                            if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                                if (jqXHR.responseJSON.exception_code == 23000) {
                                    msg += "Some Sql Exception Occured";
                                } else {
                                    msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                                }
                            } else {
                                msg += "Error(s):<strong><ul>";
                                $.each(jqXHR.responseJSON, function (key, value) {
                                    msg += "<li>" + value + "</li>";
                                });
                                msg += "</ul></strong>";
                            }
                        }
                        $.alert({
                            title: 'Error!!',
                            type: 'red',
                            icon: 'fa fa-warning',
                            content: msg,
                        });
                    }
                },
                "dataType": 'json',
                "columnDefs":
                        [
                            {className: "table-text", "targets": "_all"},
                            {
                                "targets": 0,
                                "data": "code",
                                "searchable": false,
                                "sortable": false
                            },
                            {
                                "targets": 1,
                                "data": "assemblycd",
                            },
                            {
                                "targets": 2,
                                "data": "assemblyname",
                            },
                            {
                                "targets": 3,
                                "data": "subdivision",
                            },
                            {
                                "targets": 4,
                                "data": "priority",
                            },

                            {
                                "targets": -1,
                                "data": 'action',
                                "searchable": false,
                                "sortable": false,
                                "render": function (data, type, full, meta) {
                                    var str_btns = "";

                                    str_btns += '<button type="button"  class="btn btn-success  edit-button btn_new1" id="'+data.e+'" title="Edit"><i class="fa fa-edit"></i></button>&nbsp&nbsp';



                                    str_btns += '<button type="button"  class="btn btn-danger  delete-button btn_new1" id="'+data.d+'" title="Delete"><i class="fa fa-trash"></i></button>';

                                    return str_btns;
                                }
                            }
                        ],

                "order": [[1, 'asc']]
            });
        //  table.on('order.dt search.dt draw.dt', function () {
        //     $('[data-toggle="tooltip"]').tooltip();
        //     table.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
        //         cell.innerHTML = table.page() * table.page.len() + (i + 1);
        //     });
        // });
            // table.columns( [-4,-3,-2,-1] ).visible( false );
        }


        function getAll_Assembly() {
            // alert("hi");
            var token = $('input[name="_token"]').val();
            var forDist = $("#districtcd").val();
            $.ajax({
            url: "getAllAssembly",
            method: 'POST',
            data: {forDist: forDist, _token: token},
            success: function (data) {//alert(data.options);
                //   $(".se-pre-con").fadeOut("slow");
                $("select[name='assembly_details'").html('');

                if (data.status == 1)
                {
                    // console.log(data.options);
                    $("select[name='assembly_details'").append('<option value>[Select]</option>');
                    $.each(data.options, function (k, v)
                    {
                        $("select[name='assembly_details'").append('<option value=' + k.assemblycd + '>' + v.assemblycd + '-' + v.assemblyname + '</option>');
                    });
                    //$("select[name='subdivision'").html(data.options);
                    // change_officeName();
                    // getMemberDetails();
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $(".se-pre-con").fadeOut("slow");
                var msg = "";
                if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                    msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                } else {
                    if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                        msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                    } else {
                        msg += "Error(s):<strong><ul>";
                        $.each(jqXHR.responseJSON, function (key, value) {
                            msg += "<li>" + value + "</li>";
                        });
                        msg += "</ul></strong>";
                    }
                }
                $.alert({
                    title: 'Error!!',
                    type: 'red',
                    icon: 'fa fa-exclamation-triangle',
                    content: msg
                });
            }
        });
        }
</script>

@stop