
@extends('layouts.master')
@section('content')

 <!--// top-bar -->
 <div class="wrapper">
<!-- Sidebar Holder -->
@include('layouts.sidebar')

<!-- Page Content Holder -->
<div id="content">           
<section class="tables-section">
    <nav aria-label="breadcrumb" style="width:100%;">
        <div class="breadcrumb pagehead1">
           
                {!! Form::button('<i class="fa fa-desktop"></i> Second Master Roll/Scroll/Venue Report',['class' => 'btn btn-primary-header add-new-button']) !!}
            
            <div class="col-md-offset-8 pull-right">
<!--                {!! Form::button('Academic Year:&nbsp;',['id'=>'show_academic','class' => 'btn btn-primary-year']) !!}-->
                
            </div>
                <div class="clearfix"></div>
        </div>
    </nav>

    <!-- form -->
<div class="outer-w3-agile">
  {!! Form::open(['url' => 'getFirstAppointmentLetterPDF', 'name' => 'getFirstAppointmentLetterPDF', 'class' =>'request-info clearfix form-horizontal', 'id' => 'getFirstAppointmentLetterPDF', 'method' => 'post','role'=>'','files' => true]) !!}
  {!! Form::hidden('districtcd', session()->get('districtcd_ppds'),['id'=>'districtcd']) !!}
<!--  {!! Form::hidden('subdivcd', session()->get('subdivisioncd_ppds'),['id'=>'subdivcd']) !!}
  {!! Form::hidden('blockcd', session()->get('blockmunicd_ppds'),['id'=>'blockcd']) !!}-->
  
<!--  <span class="highlight">[Note: Record(s) transfered from Personnel to Zonewise]</span>-->
   <div class="panel-group" id="accordion5">                                                              
    <div class="panel panel-default">
     <div class="panel-heading1">
     <h6 class="panel-title">
         <a data-toggle="#" data-parent="#accordion5" href="#collapseUV"> <span class="fa fa-minus"></span><span class="highlight"> Second Master Roll/Scroll/Venue Report </span></a>
     </h6>
     </div>
     <div id="collapseUV" class="panel-collapse collapse5">
      <div class="panel-body">                                   
        <div class="row"> 
        <div class='col-sm-4'>
               {!! Form::label('forZone', 'For Zone:', ['class'=>'highlight required']) !!}
                <div class="form-group">
                    <div class=''>
                        {!! Form::select('forZone',[''=>'[Select]'],Null,['id'=>'forZone','class'=>'form-control','autocomplete'=>'off']) !!}
                    </div>
                </div>
          </div>
          <div class='col-sm-4'>
            {!! Form::label('phase', 'Phase:', ['class'=>'highlight required']) !!}
           <div class="form-group">
               <div class=''>
                    <select id="phase" class="form-control" name="phase">
                                <option value="">[Select]</option>
                    </select>
                </div>
           </div>
       </div>
           <div class='col-sm-4'>
               {!! Form::label('party_reserve', 'Party/Reserve:', ['class'=>'highlight required']) !!}
                <div class="form-group">
                    <div class=''>
                        {!! Form::select('party_reserve',[''=>'[Select]'],Null,['id'=>'party_reserve','class'=>'form-control','autocomplete'=>'off']) !!}
                    </div>
                </div>
          </div>
           
          <div class='col-sm-4'>
              {!! Form::label('assembly', 'Assembly:', ['class'=>'highlight required']) !!}
              <div class="form-group">
                  <div class=''>
                      {!! Form::select('assembly',[''=>'[Select]'],null,['id'=>'assembly','class'=>'form-control','autocomplete'=>'off']) !!}
                  </div>
              </div>
          </div>  
          <div class='col-sm-4'>
               {!! Form::label('Type_of_Report', 'Type of Report:', ['class'=>'highlight required']) !!}
                <div class="form-group">
                    <div class=''>
                        {!! Form::select('Type_of_Report',[''=>'[Select]'],Null,['id'=>'Type_of_Report','class'=>'form-control','autocomplete'=>'off']) !!}
                    </div>
                </div>
          </div>
       </div>
          <div class="row" id="rw1Show" style="display:none;"> 
           <div class='col-sm-3'>
               {!! Form::label('subdivision', 'Sub Division:', ['class'=>'highlight']) !!}
              <!--<span class="highlight">Sub Division</span> -->
              <div class="form-group">
                  <div class=''>
                      {!! Form::select('subdivision',[''=>'[Select]'],null,['id'=>'subdivision','class'=>'form-control','autocomplete'=>'off']) !!}
                  </div>
              </div>
          </div>
           <div class='col-sm-3'>
              {!! Form::label('venuename', 'Venue Name:', ['class'=>'highlight']) !!}
              <div class="form-group">
                  <div class=''>
                       {!! Form::select('venuename',[''=>'[Select]'],null,['id'=>'venuename','class'=>'form-control','autocomplete'=>'off']) !!}
                  </div>
              </div>
          </div>        
             <div class='col-sm-3'>
               {!! Form::label('trainingdatetime', 'Training Date & Time:', ['class'=>'highlight']) !!}
              <div class="form-group">
                  <div class=''>
                     {!! Form::select('trainingdatetime',[''=>'[Select]'],null,['id'=>'trainingdatetime','class'=>'form-control','placeholder'=>'[Select]','autocomplete'=>'off']) !!}
                  </div>
             </div>
          </div>
          </div>
         <div class="row"> 
           <div class='col-sm-12'>                    
            <div class="form-group text-right permit">  
               {{ Form::button('EXCEL', ['class' => 'btn btn-info', 'type' => 'button','id'=>'EXCEL']) }}
               {{ Form::button('PDF', ['class' => 'btn btn-info', 'type' => 'button','id'=>'PDF']) }}
               {{ Form::button('Reset', ['class' => 'btn btn-success' ,'type' => 'reset','id'=>'reset']) }}
            </div>
          </div>
        </div>  
      </div>
    </div>                              
  </div> 
                             
</div> 
  
   
  

 {!! Form::close() !!}             
</div>
<!--// form -->


</section>

</div>
</div>
<!-- Copyright -->
<script type="text/javascript">
$(document).ready(function () {
 $("#reset").click(function () {
  location.reload(true);  
});
 $("select[name='party_reserve'").append('<option value="P">Party</option>','<option value="R">Reserve</option>');
 $("select[name='Type_of_Report'").append('<option value="M">Master Roll</option>','<option value="S">Scroll</option>','<option value="V">Venue</option>');
getZoneDetails(); 
 $('select[name="forZone"]').on('change', function () {   
    getZonePhasedata();
    getAssemblyDetails(); 
 });
 $('select[name="subdivision"]').on('change', function () {   
      $("select[name='venuename'").html('');
       $("select[name='venuename'").html('<option value>[Select]</option>');
    change_venueName();
 });
  $('select[name="Type_of_Report"]').on('change', function () { 
      var Type_of_Report = $("#Type_of_Report").val();
      if(Type_of_Report=="V"){
           $('#rw1Show').show();
      }
      else{
           $('#rw1Show').hide();
      }
   
 });

//  $('select[name="assembly"]').on('change', function () {     
//    get_SubvenueName();    
// });

  $('#PDF').click(function () {
    var party_reserve = $("#party_reserve").val();
    var forZone = $("#forZone").val();
    var phase = $("#phase").val();
   // alert(phase);
    var assembly = $("#assembly").val();
    var subdivision=$('#subdivision').val();
    var venuename=$('#venuename').val();
    var trainingdatetime=$('#trainingdatetime').val();
    var Type_of_Report = $("#Type_of_Report").val();
    if(party_reserve== ""){
        $.alert({
             title: 'Error!!',
             type: 'red',
             icon: 'fa fa-exclamation-triangle',
             content: 'Party/Reserve is required'
         });
         return false;
     }
     if(forZone==""){
        $.alert({
             title: 'Error!!',
             type: 'red',
             icon: 'fa fa-exclamation-triangle',
             content: 'For Zone is required'
         });
         return false;
     }
     if(phase==""){
        $.alert({
             title: 'Error!!',
             type: 'red',
             icon: 'fa fa-exclamation-triangle',
             content: 'Phase is required'
         });
         return false;
     }
     if(assembly== ""){
        $.alert({
             title: 'Error!!',
             type: 'red',
             icon: 'fa fa-exclamation-triangle',
             content: 'Assembly is required'
         });
         return false;
     }
     if(Type_of_Report== ""){
        $.alert({
             title: 'Error!!',
             type: 'red',
             icon: 'fa fa-exclamation-triangle',
             content: 'Type of Report is required'
         });
         return false;
     }

    var datas = {subdivision:subdivision,venuename:venuename,trainingdatetime:trainingdatetime,party_reserve: party_reserve,forZone: forZone,assembly: assembly,phase:phase,'_token': $('input[name="_token"]').val()};
   // $(".se-pre-con").fadeIn("slow");
    if(party_reserve=="P" && Type_of_Report=="M"){
       redirectPost_newTab('{{url("getSecondMasterRollPDF")}}', datas);
    }else if(party_reserve=="P" && Type_of_Report=="S"){
      redirectPost_newTab('{{url("getSecondPartyScrollPDF")}}', datas);  
    }else if(party_reserve=="P" && Type_of_Report=="V"){
      redirectPost_newTab('{{url("getSecondPartyVenuePDF")}}', datas);  
    }else if(party_reserve=="R" && Type_of_Report=="M"){
      redirectPost_newTab('{{url("getSecondReserveMasterPDF")}}', datas);  
    }else if(party_reserve=="R" && Type_of_Report=="V"){
      redirectPost_newTab('{{url("getSecondReserveVenuePDF")}}', datas);  
    }else if(party_reserve=="R" && Type_of_Report=="S"){
      redirectPost_newTab('{{url("getSecondReserveScrollPDF")}}', datas);
    }


 });
 $('#EXCEL').click(function () {
    var party_reserve = $("#party_reserve").val();
    var forZone = $("#forZone").val();
    var assembly = $("#assembly").val();
     var subdivision=$('#subdivision').val();
    var venuename=$('#venuename').val();
    var trainingdatetime=$('#trainingdatetime').val();
    var Type_of_Report = $("#Type_of_Report").val();
    var phase = $("#phase").val();
    if(party_reserve== ""){
        $.alert({
             title: 'Error!!',
             type: 'red',
             icon: 'fa fa-exclamation-triangle',
             content: 'Party/Reserve is required'
         });
         return false;
     }
     if(forZone==""){
        $.alert({
             title: 'Error!!',
             type: 'red',
             icon: 'fa fa-exclamation-triangle',
             content: 'For Zone is required'
         });
         return false;
     }
     if(phase==""){
        $.alert({
             title: 'Error!!',
             type: 'red',
             icon: 'fa fa-exclamation-triangle',
             content: 'Phase is required'
         });
         return false;
     }
     if(assembly== ""){
        $.alert({
             title: 'Error!!',
             type: 'red',
             icon: 'fa fa-exclamation-triangle',
             content: 'Assembly is required'
         });
         return false;
     }
     if(Type_of_Report== ""){
        $.alert({
             title: 'Error!!',
             type: 'red',
             icon: 'fa fa-exclamation-triangle',
             content: 'Type of Report is required'
         });
         return false;
     }
     
    

    var datas = {party_reserve: party_reserve,forZone: forZone,assembly: assembly,phase:phase,'_token': $('input[name="_token"]').val()};
   // $(".se-pre-con").fadeIn("slow");
    if(party_reserve=="P" && Type_of_Report=="M"){
       redirectPost('{{url("getSecondMasterRollEXCEL")}}', datas);
    }else if(party_reserve=="P" && Type_of_Report=="S"){
       redirectPost('{{url("getSecondPartyScrollRollEXCEL")}}', datas);
    }
    else if(party_reserve=="P" && Type_of_Report=="V"){
        var datas = {subdivision:subdivision,venuename:venuename,trainingdatetime:trainingdatetime,party_reserve: party_reserve,forZone: forZone,assembly: assembly,phase:phase,'_token': $('input[name="_token"]').val()};
        redirectPost('{{url("getSecondVenueEXCEL")}}', datas);
    }
    else if(party_reserve=="R" && Type_of_Report=="V"){
        var datas = {subdivision:subdivision,venuename:venuename,trainingdatetime:trainingdatetime,party_reserve: party_reserve,forZone: forZone,assembly: assembly,phase:phase,'_token': $('input[name="_token"]').val()};
        redirectPost('{{url("getSecondVenueReserveEXCEL")}}', datas);
    }
    
//    else if(party_reserve=="P" && Type_of_Report=="V"){
//       $.alert({
//             title: 'Error!!',
//             type: 'red',
//             icon: 'fa fa-exclamation-triangle',
//             content: 'Not available'
//         });
//         return false;
//    }
    else if(party_reserve=="R" && Type_of_Report=="M"){
      redirectPost('{{url("getSecondMasterRollEXCEL")}}', datas);  
    }
//    else if(party_reserve=="R" && Type_of_Report=="V"){
//       $.alert({
//             title: 'Error!!',
//             type: 'red',
//             icon: 'fa fa-exclamation-triangle',
//             content: 'Not available'
//         });
//         return false;
//    }
    else if(party_reserve=="R" && Type_of_Report=="S"){
        redirectPost('{{url("getSecondReserveScrollRollEXCEL")}}', datas);  
    }
    
 }); 
   
});
function getZoneDetails(){
     var forDist = $("#districtcd").val();
     var token = $("input[name='_token']").val();
     $(".se-pre-con").fadeIn("slow");
     $.ajax({
       url: "getZoneDetails",
       method: 'POST',
       data: {forDist: forDist, _token: token},
       success: function (data) {//alert(data.options);
         $(".se-pre-con").fadeOut("slow");
          $("select[name='forZone'").html('');
          if(data.status==1)
          {
             $("select[name='forZone'").append('<option value>[Select]</option>');
            $.each(data.options,function (k, v)
            {
                $("select[name='forZone'").append('<option value='+k+'>'+v+'</option>');
            });
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
            $(".se-pre-con").fadeOut("slow");
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-exclamation-triangle',
                  content: msg
              });
         },
          complete: function () {
            change_subdivision();
        }
   });
}
function getZonePhasedata(){
  //  alert("hi");
  var zone = $("#forZone").val();
 // alert(zone);
  $("#rw").hide();
   var token = $("input[name='_token']").val();
  // $(".se-pre-con").fadeIn("slow");
   $.ajax({
       url: "getZonePhasedata",
       method: 'POST',
       data: {zone: zone, _token: token},
       success: function (data) {//alert(data.options);
       //   $(".se-pre-con").fadeOut("slow");
          $("select[name='phase'").html('');
          
          if(data.status==1)
          {
            $("select[name='phase'").append('<option value>[Select]</option>');
            $.each(data.options,function (k, v)
            {
                $("select[name='phase'").append('<option value='+k+'>'+v+'</option>');
            });
            //$("select[name='subdivision'").html(data.options);
             // change_officeName();
             
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
       //  $(".se-pre-con").fadeOut("slow");
           var msg = "";
           if (jqXHR.status !== 422 && jqXHR.status !== 400) {
               msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
           } else {
               if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                   msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
               } else {
                   msg += "Error(s):<strong><ul>";
                   $.each(jqXHR.responseJSON, function (key, value) {
                       msg += "<li>" + value + "</li>";
                   });
                   msg += "</ul></strong>";
               }
           }
           $.alert({
               title: 'Error!!',
               type: 'red',
               icon: 'fa fa-warning',
               content: msg
           });
         },
        //  complete: function(){
        //   getZonePhaseWiseAssembly();
        //  }
   });
}
function getAssemblyDetails(){
    var forZone = $("#forZone").val();
     var token = $("input[name='_token']").val();
     $(".se-pre-con").fadeIn("slow");
     $.ajax({
       url: "getAssemblyDetails",
       method: 'POST',
       data: {forZone: forZone, _token: token},
       success: function (data) {//alert(data.options);
         $(".se-pre-con").fadeOut("slow");
          $("select[name='assembly'").html('');
          if(data.status==1)
          {
              $("select[name='assembly'").html(data.options);
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
            $(".se-pre-con").fadeOut("slow");
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-exclamation-triangle',
                  content: msg
              });
         },
        
   });
}
function getSecondRecordAvailable(){
    var forZone = $("#forZone").val();
    var party_reserve = $("#party_reserve").val();
    var assembly = $("#assembly").val();
    var group_id = $("#group_id").val();
    var no_of_member = $("#no_of_member").val();
    
    var token = $("input[name='_token']").val();
    // $(".se-pre-con").fadeIn("slow");
     $.ajax({
       url: "getSecondRecordAvailable",
       method: 'POST',
       data: {forZone: forZone,party_reserve:party_reserve,assembly:assembly,group_id:group_id,no_of_member:no_of_member, _token: token},
       success: function (data) {//alert(data.options);
        // $(".se-pre-con").fadeOut("slow");
         $("#recordsav").val("");
          if(data.status==1)
          {
              $("#recordsav").val(data.options);
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-exclamation-triangle',
                  content: msg
              });
         },
          
         
   });
}

function getTrainingDateTime(){
     var forDist = $("#districtcd").val();
     var token = $("input[name='_token']").val();
     //$(".se-pre-con").fadeIn("slow");
     $.ajax({
       url: "get2ndTrainingDateTime",
       method: 'POST',
       data: {forDist: forDist, _token: token},
       success: function (data) {//alert(data.options);
         $(".se-pre-con").fadeOut("slow");
          $("select[name='trainingdatetime'").html('');
          if(data.status==1)
          {
              $("select[name='trainingdatetime'").html(data.options);
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
            $(".se-pre-con").fadeOut("slow");
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-warning',
                  content: msg
              });
         }
   });
 }
 
 function change_venueName(){
  var subdivision = $("#subdivision").val();
   var token = $("input[name='_token']").val();
   //$(".se-pre-con").fadeIn("slow");
   $.ajax({
       url: "get2ndVenueName",
       method: 'POST',
       data: {subdivision: subdivision, _token: token},
       success: function (data) {//alert(data.options);
          //$(".se-pre-con").fadeOut("slow");
          $("select[name='venuename'").html('');
          
          if(data.status==1)
          {
            $("select[name='venuename'").append('<option value>[Select]</option>');
            $.each(data.options,function (k, v)
            {
                $("select[name='venuename'").append('<option value='+k+'>'+v+'</option>');
            });
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
         $(".se-pre-con").fadeOut("slow");
           var msg = "";
           if (jqXHR.status !== 422 && jqXHR.status !== 400) {
               msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
           } else {
               if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                   msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
               } else {
                   msg += "Error(s):<strong><ul>";
                   $.each(jqXHR.responseJSON, function (key, value) {
                       msg += "<li>" + value + "</li>";
                   });
                   msg += "</ul></strong>";
               }
           }
           $.alert({
               title: 'Error!!',
               type: 'red',
               icon: 'fa fa-exclamation-triangle',
               content: msg
           });
         },
        complete: function () {
            //getAssemblyDetails();
        }
   });
}

function change_subdivision(){
  var forDist = $("#districtcd").val();
   var token = $("input[name='_token']").val();
  // $(".se-pre-con").fadeIn("slow");
   $.ajax({
       url: "getSubdivisionData",
       method: 'POST',
       data: {forDist: forDist, _token: token},
       success: function (data) {//alert(data.options);
       //   $(".se-pre-con").fadeOut("slow");
          $("select[name='subdivision'").html('');
          
          if(data.status==1)
          {
            $("select[name='subdivision'").append('<option value>[Select]</option>');
            $.each(data.options,function (k, v)
            {
                $("select[name='subdivision'").append('<option value='+k+'>'+v+'</option>');
            });
            //$("select[name='subdivision'").html(data.options);
             // change_officeName();
            // getMemberDetails();
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
         $(".se-pre-con").fadeOut("slow");
           var msg = "";
           if (jqXHR.status !== 422 && jqXHR.status !== 400) {
               msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
           } else {
               if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                   msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
               } else {
                   msg += "Error(s):<strong><ul>";
                   $.each(jqXHR.responseJSON, function (key, value) {
                       msg += "<li>" + value + "</li>";
                   });
                   msg += "</ul></strong>";
               }
           }
           $.alert({
               title: 'Error!!',
               type: 'red',
               icon: 'fa fa-exclamation-triangle',
               content: msg
           });
         },
        complete: function () {
            getTrainingDateTime();
        }
   });
}
</script>
@stop
