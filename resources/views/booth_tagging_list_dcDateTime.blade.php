
@extends('layouts.master')
@section('content')

 <!--// top-bar -->
 <div class="wrapper">
<!-- Sidebar Holder -->
@include('layouts.sidebar')

<!-- Page Content Holder -->
<div id="content">           
<section class="tables-section">
    <nav aria-label="breadcrumb" style="width:100%;">
        <div class="breadcrumb pagehead1">
           
                {!! Form::button('<i class="fa fa-desktop"></i> Booth Tagging List',['class' => 'btn btn-primary-header add-new-button']) !!}
            
            <div class="col-md-offset-8 pull-right">
<!--                {!! Form::button('Academic Year:&nbsp;',['id'=>'show_academic','class' => 'btn btn-primary-year']) !!}-->
                
            </div>
                <div class="clearfix"></div>
        </div>
    </nav>

    <!-- form -->
<div class="outer-w3-agile">
  {!! Form::open(['url' => 'getBoothTaggingPDF1', 'name' => 'getBoothTaggingPDF1', 'class' =>'request-info clearfix form-horizontal', 'id' => 'getBoothTaggingPDF1', 'method' => 'post','role'=>'','files' => true]) !!}
  {!! Form::hidden('districtcd', session()->get('districtcd_ppds'),['id'=>'districtcd']) !!}
<!--  {!! Form::hidden('subdivcd', session()->get('subdivisioncd_ppds'),['id'=>'subdivcd']) !!}
  {!! Form::hidden('blockcd', session()->get('blockmunicd_ppds'),['id'=>'blockcd']) !!}-->
  
<!--  <span class="highlight">[Note: Record(s) transfered from Personnel to Zonewise]</span>-->
   <div class="panel-group" id="accordion5">                                                              
    <div class="panel panel-default">
     <div class="panel-heading1">
     <h6 class="panel-title">
         <a data-toggle="#" data-parent="#accordion5" href="#collapseUV"> <span class="fa fa-minus"></span><span class="highlight"> Zone-wise,Assembly-wise(Polling Party vs PS No)</span></a>
     </h6>
     </div>
     <div id="collapseUV" class="panel-collapse collapse5">
      <div class="panel-body">                                   
        <div class="row"> 
           <div class='col-sm-3'>
               {!! Form::label('forZone', 'For Zone:', ['class'=>'highlight required']) !!}
                <div class="form-group">
                    <div class=''>
                        {!! Form::select('forZone',[''=>'[Select]'],Null,['id'=>'forZone','class'=>'form-control','autocomplete'=>'off']) !!}
                    </div>
                </div>
          </div>
            
          <div class='col-sm-3'>
              {!! Form::label('assembly', 'Assembly:', ['class'=>'highlight required']) !!}
              <div class="form-group">
                  <div class=''>
                      {!! Form::select('assembly',[''=>'[Select]'],null,['id'=>'assembly','class'=>'form-control','autocomplete'=>'off']) !!}
                  </div>
              </div>
          </div>  
          <div class='col-sm-3'>
              {!! Form::label('dc_data_time_party_vs_ps', 'DC Venue,Date & Time:', ['class'=>'highlight']) !!}
              <div class="form-group">
                  <div class=''>
                      {!! Form::select('dc_data_time_party_vs_ps',[''=>'[Select]'],null,['id'=>'dc_data_time_party_vs_ps','class'=>'form-control','autocomplete'=>'off']) !!}
                  </div>
              </div>
          </div>  
       </div>
         <div class="row"> 
           <div class='col-sm-12'>                    
            <div class="form-group text-right permit">                            	
               {{ Form::button('PDF', ['class' => 'btn btn-info', 'type' => 'button','id'=>'PDF']) }}
               {{ Form::button('Reset', ['class' => 'btn btn-success' ,'type' => 'reset','id'=>'reset']) }}
            </div>
          </div>
        </div>  
      </div>
    </div>                              
  </div> 
                             
</div> 
  <div class="panel-group" id="accordion5">                                                              
    <div class="panel panel-default">
     <div class="panel-heading1">
     <h6 class="panel-title">
         <a data-toggle="#" data-parent="#accordion5" href="#collapseUV"> <span class="fa fa-minus"></span><span class="highlight"> Zone-wise,Assembly-wise(PS No vs Polling Party)</span></a>
     </h6>
     </div>
     <div id="collapseUV" class="panel-collapse collapse5">
      <div class="panel-body">                                   
        <div class="row"> 
           <div class='col-sm-3'>
               {!! Form::label('forZonepsvspolling', 'For Zone:', ['class'=>'highlight required']) !!}
                <div class="form-group">
                    <div class=''>
                        {!! Form::select('forZonepsvspolling',[''=>'[Select]'],Null,['id'=>'forZonepsvspolling','class'=>'form-control','autocomplete'=>'off']) !!}
                    </div>
                </div>
          </div>
            
          <div class='col-sm-3'>
              {!! Form::label('assemblypsvspolling', 'Assembly:', ['class'=>'highlight required']) !!}
              <div class="form-group">
                  <div class=''>
                      {!! Form::select('assemblypsvspolling',[''=>'[Select]'],null,['id'=>'assemblypsvspolling','class'=>'form-control','autocomplete'=>'off']) !!}
                  </div>
              </div>
          </div>  
            <div class='col-sm-3'>
              {!! Form::label('dc_date_time_ps_vs_party', 'DC Venue,Date & Time:', ['class'=>'highlight']) !!}
              <div class="form-group">
                  <div class=''>
                      {!! Form::select('dc_date_time_ps_vs_party',[''=>'[Select]'],null,['id'=>'dc_date_time_ps_vs_party','class'=>'form-control','autocomplete'=>'off']) !!}
                  </div>
              </div>
          </div>
          
       </div>
         <div class="row"> 
           <div class='col-sm-12'>                    
            <div class="form-group text-right permit">                            	
               {{ Form::button('PDF', ['class' => 'btn btn-info', 'type' => 'button','id'=>'PDFpsvspolling']) }}
               {{ Form::button('Reset', ['class' => 'btn btn-success' ,'type' => 'reset','id'=>'reset']) }}
            </div>
          </div>
        </div>  
      </div>
    </div>                              
  </div> 
                             
</div> 
   
  

 {!! Form::close() !!}             
</div>
<!--// form -->


</section>

</div>
</div>
<!-- Copyright -->
<script type="text/javascript">
$(document).ready(function () {
 $("#reset").click(function () {
  location.reload(true);  
});
getZoneDetails(); 
 $('select[name="forZone"]').on('change', function () {   
    getAssemblyDetails(); 
 });
  $('select[name="forZonepsvspolling"]').on('change', function () {   
    getAssemblyDetailsForPsvspolling(); 
 });
 $('select[name="assembly"]').on('change', function () {
    getDCVenueDetails();
        
 });
  $('select[name="assemblypsvspolling"]').on('change', function () {
    getDCVenueDetailsPsVsParty();
        
 });
  $('#PDF').click(function () {
    var forZone = $("#forZone").val();
    var assembly = $("#assembly").val();
     var dc_data_time_party_vs_ps=$('#dc_data_time_party_vs_ps').val();
     if(forZone==""){
        $.alert({
             title: 'Error!!',
             type: 'red',
             icon: 'fa fa-exclamation-triangle',
             content: 'For Zone is required'
         });
         return false;
     }
     if(assembly== ""){
        $.alert({
             title: 'Error!!',
             type: 'red',
             icon: 'fa fa-exclamation-triangle',
             content: 'Assembly is required'
         });
         return false;
     }

    var datas = {forZone: forZone,assembly: assembly,dc_data_time_party_vs_ps:dc_data_time_party_vs_ps,'_token': $('input[name="_token"]').val()};
    //$(".se-pre-con").fadeIn("slow");
    redirectPost_newTab('{{url("getBoothTaggingPDF")}}', datas);
 });
   $('#PDFpsvspolling').click(function () {
    var forZonepsvspolling = $("#forZonepsvspolling").val();
    var assemblypsvspolling = $("#assemblypsvspolling").val();
    var dc_date_time_ps_vs_party = $("#dc_date_time_ps_vs_party").val();
   
     if(forZonepsvspolling==""){
        $.alert({
             title: 'Error!!',
             type: 'red',
             icon: 'fa fa-exclamation-triangle',
             content: 'For Zone is required'
         });
         return false;
     }
     if(assemblypsvspolling== ""){
        $.alert({
             title: 'Error!!',
             type: 'red',
             icon: 'fa fa-exclamation-triangle',
             content: 'Assembly is required'
         });
         return false;
     }

    var datas = {forZone: forZonepsvspolling,assembly: assemblypsvspolling,dc_date_time_ps_vs_party:dc_date_time_ps_vs_party,'_token': $('input[name="_token"]').val()};
    //$(".se-pre-con").fadeIn("slow");
    redirectPost_newTab('{{url("getBoothTaggingPDFpsvspolling")}}', datas);
 });
});
function getZoneDetails(){
     var forDist = $("#districtcd").val();
     var token = $("input[name='_token']").val();
     $(".se-pre-con").fadeIn("slow");
     $.ajax({
       url: "getZoneDetails",
       method: 'POST',
       data: {forDist: forDist, _token: token},
       success: function (data) {//alert(data.options);
         $(".se-pre-con").fadeOut("slow");
          $("select[name='forZone'").html('');
          if(data.status==1)
          {
             $("select[name='forZone'").append('<option value>[Select]</option>');
            //  $("select[name='forZonepsvspolling'").append('<option value>[Select]</option>');
            $.each(data.options,function (k, v)
            {
                $("select[name='forZone'").append('<option value='+k+'>'+v+'</option>');
                $("select[name='forZonepsvspolling'").append('<option value='+k+'>'+v+'</option>');
            });
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
            $(".se-pre-con").fadeOut("slow");
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-exclamation-triangle',
                  content: msg
              });
         }
   });
}
function getAssemblyDetails(){
    var forZone = $("#forZone").val();
     var token = $("input[name='_token']").val();
     $(".se-pre-con").fadeIn("slow");
     $.ajax({
       url: "getAssemblyDetails",
       method: 'POST',
       data: {forZone: forZone, _token: token},
       success: function (data) {//alert(data.options);
         $(".se-pre-con").fadeOut("slow");
          $("select[name='assembly'").html('');
          if(data.status==1)
          {
              $("select[name='assembly'").html(data.options);
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
            $(".se-pre-con").fadeOut("slow");
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-exclamation-triangle',
                  content: msg
              });
         }
   });
}

function getAssemblyDetailsForPsvspolling(){
    var forZone = $("#forZonepsvspolling").val();
     var token = $("input[name='_token']").val();
     $(".se-pre-con").fadeIn("slow");
     $.ajax({
       url: "getAssemblyDetailsForPsvspolling",
       method: 'POST',
       data: {forZone: forZone, _token: token},
       success: function (data) {//alert(data.options);
         $(".se-pre-con").fadeOut("slow");
          $("select[name='assemblypsvspolling'").html('');
          if(data.status==1)
          {
              $("select[name='assemblypsvspolling'").html(data.options);
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
            $(".se-pre-con").fadeOut("slow");
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-exclamation-triangle',
                  content: msg
              });
         }
   });
}
function getDCVenueDetails(){
     var forZone = $("#forZone").val();
     var assembly = $("#assembly").val();
     var token = $("input[name='_token']").val();
     $("#rw").hide();
    // $(".se-pre-con").fadeIn("slow");
     $.ajax({
       url: "getDCAssemblyVenueDetails",
       method: 'POST',
       data: {forZone: forZone,  assembly:assembly, _token: token},
       success: function (data) {//alert(data.options);
        // $(".se-pre-con").fadeOut("slow");
          $("select[name='dc_data_time_party_vs_ps'").html('');
          if(data.status==1)
          {
             $("select[name='dc_data_time_party_vs_ps'").append('<option value>[Select]</option>');
            $.each(data.options,function (k, v)
            {
                $("select[name='dc_data_time_party_vs_ps'").append('<option value='+k+'>'+v+'</option>');
            });
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
            $(".se-pre-con").fadeOut("slow");
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-exclamation-triangle',
                  content: msg
              });
         },complete: function () {
            get_DcrcPSDetails();
         }
   });
}

function getDCVenueDetailsPsVsParty(){
     var forZone = $("#forZonepsvspolling").val();
     var assembly = $("#assemblypsvspolling").val();
     var token = $("input[name='_token']").val();
     $("#rw").hide();
    // $(".se-pre-con").fadeIn("slow");
     $.ajax({
       url: "getDCAssemblyVenueDetails",
       method: 'POST',
       data: {forZone: forZone,  assembly:assembly, _token: token},
       success: function (data) {//alert(data.options);
        // $(".se-pre-con").fadeOut("slow");
          $("select[name='dc_date_time_ps_vs_party'").html('');
          if(data.status==1)
          {
             $("select[name='dc_date_time_ps_vs_party'").append('<option value>[Select]</option>');
            $.each(data.options,function (k, v)
            {
                $("select[name='dc_date_time_ps_vs_party'").append('<option value='+k+'>'+v+'</option>');
            });
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
            $(".se-pre-con").fadeOut("slow");
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-exclamation-triangle',
                  content: msg
              });
         },complete: function () {
            get_DcrcPSDetails();
         }
   });
}
</script>
@stop
