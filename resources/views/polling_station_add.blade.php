@extends('layouts.master')
@section('content')
<style>
.p_note{
 padding-left: 10px;
 padding-right: 10px;
 padding-top: 10px;
  font-size: .9rem;
  line-height: 1.5;
  color: #f85a40;
  background-color: #fff;
}
</style>
<!--// top-bar -->
 <div class="wrapper">
<!-- Sidebar Holder -->
@include('layouts.sidebar')

<!-- Page Content Holder -->
<div id="content">           
<section class="tables-section">
    <nav aria-label="breadcrumb" style="width:100%;">
         <div class="breadcrumb pagehead1">
           <span class="headbrand"><i class="fa fa-desktop"></i> Add Polling Station (Excel Upload)
            <span class="scoop-mcaret1"></span>
           </span>&nbsp;&nbsp;&nbsp;         
                {!! Form::button(' Add Polling Station-wise DCRC Venue&nbsp;<i class="fa fa-plus-circle"></i>',['id'=>'show_po_add','class' => 'btn btn-primary-year add-new-button']) !!}  
                &nbsp;{!! Form::button(' List of Polling Station&nbsp;<i class="fa fa-arrow-circle-right"></i>',['id'=>'show_po_list','class' => 'btn btn-primary-year add-new-button']) !!}  
           <div class="clearfix"></div>
        </div>
    </nav>

    <!-- form -->
<div class="outer-w3-agile">
  {!! Form::open(['url' => 'pollingStationAdd', 'name' => 'pollingStationAdd', 'class' =>'request-info clearfix form-horizontal', 'id' => 'pollingStationAdd', 'method' => 'post','role'=>'','files' => true]) !!}
  {!! Form::hidden('districtcd', session()->get('districtcd_ppds'),['id'=>'districtcd']) !!}
  
  <div class="panel-group" id="accordion5">                                                              
   <div class="panel panel-default">
    <div class="panel-heading1">
     <h6 class="panel-title">
         <a data-toggle="#" data-parent="#accordion5" href="#collapsePV"> <span class="fa fa-minus"></span> <span class="highlight">Add Polling Station (Excel Upload)</span></a>
     </h6>
    </div>
    <div class="row"> 
        <div class='col-sm-12'>
            <div class="form-group">
                <div class="p_note">Note:<br> 1. File should be an excel format(.xlsx).<br>
                           2. File column value should follow the following format:<br> &nbsp;&nbsp;&nbsp;a)PS No(mandatory), b)PSfix (not mandatory), c)Assembly Code(mandatory), d)Member (mandatory), e)PS Name(mandatory), f)Gender (mandatory)[M,F]<br>
                           3. File column header name should be same.<br>
                           4. <span style="font-size:14px;cursor: pointer;color: blue;"><a id="excel-download"><i class="fa fa-download"></i> Download</a></span> excel format to upload the file .
                </div>
              </div>
        </div>        
    </div>
    <div id="collapsePV" class="panel-collapse collapse5">
      <div class="panel-body">                                   
       <div class="row"> 
         <div class='col-sm-4'>
               {!! Form::label('exceldata', 'Upload file:', ['class'=>'highlight required']) !!}
              <div class="form-group">
                  <div class=''>
                      {!! Form::file('excelup',['id'=>'excelup','class'=>'form-control','autocomplete'=>'off']) !!}
                  </div>
              </div>
          </div>  
         
      </div>
          
 
          
<!--      <div class="row"> 
            <div class='col-sm-12'>
                <div class="form-group">
                     <div id='subVenueDetails'></div>
                  </div>
            </div>
         
      </div> -->
      <div class="row">                  
         <div class='col-sm-12'>
            <div class="form-group text-right permit">                            	
               {{ Form::button('Submit', ['class' => 'btn btn-info', 'type' => 'submit','id'=>'Submit']) }}
               {{ Form::button('Reset', ['class' => 'btn btn-success' ,'type' => 'reset','id'=>'reset']) }}
            </div>
          </div>
      </div>     
       
    </div>                              
   </div>
  </div>  
  </div>
 {!! Form::close() !!}             
</div>
<!--// form -->

</section>

</div>
</div>
<!-- Copyright -->
<script type="text/javascript">
$(document).ready(function () {
$("#show_po_add").click(function(){
       window.location.href = "polling_station_dcrc"; 
});
$("#show_po_list").click(function(){
       window.location.href = "pollin_station_list"; 
});
$("#reset").click(function () {
  location.reload(true);  
});
$("#excel-download").click(function(){
    var datas = {'_token': $('input[name="_token"]').val()};
    redirectPost('{{url("excelformat")}}', datas);

});

$('#pollingStationAdd').bootstrapValidator({

        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            excelup: {
                validators: {
                    notEmpty: {
                        message: 'Please Select File'
                    },
                    file: {
                        extension: 'xlsx',
                        message: 'Excel file should be in EXCEL format'
                    }
                }
            }
        }

    }).on('success.form.bv', function (e) {
        e.preventDefault();
        var action = $(this).attr('action');
        var type = $(this).attr('method');
        var formData = new FormData($(this)[0]);
        $(".se-pre-con").fadeIn("slow");
        $.ajax({
            type: type,
            url: action,
            data: formData,
            processData: false,
            contentType: false,
            dataType: "json",
            success: function (data) {
                $(".se-pre-con").fadeOut("slow");

                if (data.status == 1) {        
                     var msg=data.options+" Record(s) saved successfully";
                     $.confirm({
                        title: 'Success!',
                        type: 'green',
                        icon: 'fa fa-check',
                        content: msg,
                        buttons: {
                            ok: function () {     
                                location.reload(true);
                            }

                        }
                    });

                } else {
                   $.alert({
                        title: 'Error!!',
                        type: 'red',
                        icon: 'fa fa-exclamation-triangle',
                        content: data.options
                    });
                    $("#Submit").attr('disabled',false);  
                }
                $("#Submit").attr('disabled',false);
            },
            error: function (jqXHR, textStatus, errorThrown) {
            $(".se-pre-con").fadeOut("slow");
           // var arr = $.map(jqXHR.responseJSON, function(el) { return el });
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-exclamation-triangle',
                  content: msg
              });
              $("#Submit").attr('disabled',false);
           }
        });
    });
});

</script>
@stop