
@extends('layouts.master')
@section('content')

 <div class="wrapper">
<!-- Sidebar Holder -->
@include('layouts.sidebar')

<!-- Page Content Holder -->
<div id="content">           
<section class="tables-section">
    <nav aria-label="breadcrumb" style="width:100%;">
        <div class="breadcrumb pagehead1">
           
                {!! Form::button('<i class="fa fa-desktop"></i> Set Priority AC',['class' => 'btn btn-primary-header add-new-button']) !!}
            
            <div class="col-md-offset-8 pull-right">
<!--                {!! Form::button('Academic Year:&nbsp;',['id'=>'show_academic','class' => 'btn btn-primary-year']) !!}-->
                
            </div>
            <div class="clearfix"></div>
        </div>
    </nav>

    <!-- form -->
<div class="outer-w3-agile">
  {!! Form::open(['url' => 'save_priority_ac', 'name' => 'save_priority_ac', 'class' =>'request-info clearfix form-horizontal', 'id' => 'save_priority_ac', 'method' => 'post','role'=>'','files' => true]) !!}
  {!! Form::hidden('districtcd', session()->get('districtcd_ppds'),['id'=>'districtcd']) !!}
  
  <div class="panel-group" id="accordion5" style="padding-top: 0px;">                                                              
    <div class="panel panel-default">
     <div class="panel-heading1">
     <h6 class="panel-title">
         <a data-toggle="#" data-parent="#accordion5" href="#collapseUV"> <span class="fa fa-minus"></span> <span class="highlight">Set Priority AC</span> </a>
     </h6>
     </div>
     <div id="collapseUV" class="panel-collapse collapse5">
      <div class="panel-body">                                   
        <div class="row">                                   
           <div class='col-sm-4'>
               {!! Form::label('pc', 'Parliament Constituency:', ['class'=>'highlight']) !!}
              <!--<span class="highlight">Sub Division</span> -->
              <div class="form-group">
                  <div class=''>
                      {!! Form::select('pc',[''=>'[Select]'],null,['id'=>'pc','class'=>'form-control','autocomplete'=>'off']) !!}
                  </div>
              </div>
          </div> 
          <div class='col-sm-4'>
               {!! Form::label('ac_priority', 'Assembly Constituency (Priority):', ['class'=>'highlight']) !!}
              <!--<span class="highlight">Sub Division</span> -->
              <div class="form-group">
                  <div class=''>
                      {!! Form::select('ac_priority',[''=>'[Select]'],null,['id'=>'ac_priority','class'=>'form-control','autocomplete'=>'off']) !!}
                  </div>
              </div>
          </div>
            <div class='col-sm-4'>
               {!! Form::label('priority_no', 'Priority No:', ['class'=>'highlight required']) !!}
                <div class="form-group">
                    <div class=''>
                        {!! Form::select('priority_no',['1'=>'1'],Null,['id'=>'priority_no','class'=>'form-control','autocomplete'=>'off']) !!}
                    </div>
                </div>
          </div>
         </div>
          <div class="row" > 
          <div class='col-sm-12'>                    
            <div class="form-group text-right permit">                            	
                   {{ Form::button('Submit', ['class' => 'btn btn-info', 'type' => 'submit','id'=>'submit']) }}
                   {{ Form::button('Reset', ['class' => 'btn btn-success' ,'type' => 'reset','id'=>'reset']) }}
            </div>
          </div>
          
         </div>
          
         <div class="row"> 
            <div class='col-sm-12'>
                <div id='memberDetails'></div>
            </div>
        </div>  
        
         
      </div>
    </div>                              
  </div> 
 
  
  </div>

 
 {!! Form::close() !!}             
</div>
<!--// form -->

</section>
</div>
</div>
<!-- Copyright -->
<script type="text/javascript">
$(document).ready(function () {
 $("select[name='priority_no'").append('<option value="2">2</option>','<option value="3">3</option>','<option value="4">4</option>','<option value="5">5</option>');
getDistrictWisePC(); 
$("#reset").click(function () {
  location.reload(true);  
});
$("#pc").change(function(){
  getPCWiseAC();  
});
  $('#save_priority_ac')
            .bootstrapValidator({
            message: 'This value is not valid',
            feedbackIcons: {
            valid: 'fa fa-check',
                    invalid: 'fa fa-times',
                    validating: 'fa fa-refresh'
            },
            fields: {
                pc: {
                    validators: {
                        notEmpty: {
                            message: 'PC is required'
                        }
                    }
                },
                ac_priority: {
                    validators: {
                        notEmpty: {
                            message: 'Priority AC is required'
                        }
                    }
                },
                priority_no: {
                    validators: {
                        notEmpty: {
                            message: 'Priority No is required'
                        }
                    }
                }
              }
            }).on('success.form.bv', function (e) {
           // Prevent form submission
            e.preventDefault();
            var action = $(this).attr('action');
//            var zone_code = $('#zone_code').val();
            var districtcd = $("#districtcd").val();
            var pc = $("#pc").val();
            var ac_priority = $("#ac_priority").val();
            var priority_no = $("#priority_no").val();
            var type = $(this).attr('method');
            var token = $("input[name='_token']").val();
            var fd = new FormData();
            fd.append('districtcd', districtcd);
            fd.append('pc', pc);
            fd.append('ac_priority', ac_priority);
            fd.append('priority_no', priority_no);
            fd.append('_token', token);
//            if (zone_code != '') {
//             action = "update_zone"
//             fd.append('edit_zone', zone_code);
//            }
          $(".se-pre-con").fadeIn("slow");
            $.ajax({
            type: type,
                    url: action,
                    data: fd,
                    processData: false,
                    contentType: false,
                    dataType: "json",
                    success: function (data) {

                    if(data.status==1)
                    {
                      $(".se-pre-con").fadeOut("slow");            
                      var msg="Record(s) saved successfully";
                       $.confirm({
                            title: 'Success!',
                            type: 'green',
                            icon: 'fa fa-check',
                            content: msg,
                            buttons: {
                                ok: function () {
                                   // location.reload(true);
                                 //  getZoneWiseAssembly();
                                  getDistrctRecords();

                                }

                            }
                        });
                      }else if (data.status==0){
                         $(".se-pre-con").fadeOut("slow");            
                        var msg="Duplicate record not allowed";
                         $.confirm({
                               title: 'Error!!',
                                type: 'red',
                                icon: 'fa fa-exclamation-triangle',
                                content: msg,
                                 buttons: {
                                  ok: function () {
                                    $("#submit").attr('disabled',false);
                                  }

                              }
                          });  
                      }
                      else if (data.status==2){
                         $(".se-pre-con").fadeOut("slow");            
                        var msg="Duplicate Priority No is not allowed for this PC";
                         $.confirm({
                               title: 'Error!!',
                                type: 'red',
                                icon: 'fa fa-exclamation-triangle',
                                content: msg,
                                 buttons: {
                                  ok: function () {
                                    $("#submit").attr('disabled',false);
                                  }

                              }
                          }); 
                      }

                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        $(".se-pre-con").fadeOut("slow");
                        var msg = "";
                        if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                            msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                        } else {
                            if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                                msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                            } else {
                                msg += "Error(s):<strong><ul>";
                                $.each(jqXHR.responseJSON, function (key, value) {
                                    msg += "<li>" + value + "</li>";
                                });
                                msg += "</ul></strong>";
                            }
                        }
                        $.alert({
                            title: 'Error!!',
                            type: 'red',
                            icon: 'fa fa-warning',
                            content: msg
                        });
                    }
            });
 
     });
 

});
function delete_priority_pc(code) {

//    var reply=confirm('Are you sure to delete this record?');
//                 if(!reply){
//                     return false;
//                 }
    var msg="Are you sure to delete this record?";
    $.confirm({
         title: 'Confirm!',
         type: 'red',
         icon: 'fa fa-exclamation-triangle',
         content: msg,
         buttons: {
             ok: function () {
                 $.ajax({
                  type: 'post',
                  url: 'delete_priority_ac',
                  data: {'data': code, '_token': $('input[name="_token"]').val()},
                  dataType: 'json',
                  success: function (data) {
                    if(data.status==1)
                    {
                     location.reload(true);
                    }
                    else if (data.status==2){
                       $.alert({
                            title: 'Error!!',
                            type: 'red',
                            icon: 'fa fa-warning',
                            content: data.options
                        }); 
                    }
                  }
                });
              

             },
             cancel:function () {
                 
             }

         }
     }); 
    

}

function edit_zone(code) {
    $("#reset").show();
    $.ajax({
        type: 'post',
        url: 'zone_edit',
        data: {'zone': code, '_token': $('input[name="_token"]').val()},
        dataType: 'json',
        success: function (data) {
            $('#zone').val(data.options[0].zonename);
            $('#zone_code').val(data.options[0].zone);
        }
    });
 }
function getDistrictWisePC(){
   var districtcd = $("#districtcd").val();   
   var token = $("input[name='_token']").val();
   // $(".se-pre-con").fadeIn("slow");
   $.ajax({
       url: "getDistrictWisePCdata",
       method: 'POST',
       data: {districtcd: districtcd, _token: token},
       success: function (data) {//alert(data.options);
       //   $(".se-pre-con").fadeOut("slow");
          $("select[name='pc'").html('');
          
          if(data.status==1)
          {
            $("select[name='pc'").append('<option value>[Select]</option>');
            $.each(data.options,function (k, v)
            {
                $("select[name='pc'").append('<option value='+k+'>'+v+'</option>');
            });
//            $("select[name='pc_priority'").append('<option value>[Select]</option>');
//            $.each(data.options,function (k, v)
//            {
//                $("select[name='pc_priority'").append('<option value='+k+'>'+v+'</option>');
//            });
//            $("select[name='pc_priority'").append('<option value="99">Others</option>');
            //$("select[name='subdivision'").html(data.options);
             // change_officeName();            
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
       //  $(".se-pre-con").fadeOut("slow");
           var msg = "";
           if (jqXHR.status !== 422 && jqXHR.status !== 400) {
               msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
           } else {
               if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                   msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
               } else {
                   msg += "Error(s):<strong><ul>";
                   $.each(jqXHR.responseJSON, function (key, value) {
                       msg += "<li>" + value + "</li>";
                   });
                   msg += "</ul></strong>";
               }
           }
           $.alert({
               title: 'Error!!',
               type: 'red',
               icon: 'fa fa-warning',
               content: msg
           });
         },complete: function(){
          getDistrctRecords();
         }
   });
}
function getPCWiseAC(){
var districtcd = $("#districtcd").val(); 
var pccd = $("#pc").val();  
   var token = $("input[name='_token']").val();
   // $(".se-pre-con").fadeIn("slow");
   $.ajax({
       url: "getDistrictWisePCwiseACdata",
       method: 'POST',
       data: {districtcd: districtcd,pccd:pccd, _token: token},
       success: function (data) {//alert(data.options);
       //   $(".se-pre-con").fadeOut("slow");
         
           $("select[name='ac_priority'").html('');
          if(data.status==1)
          {
            
            $("select[name='ac_priority'").append('<option value>[Select]</option>');
            $.each(data.options,function (k, v)
            {
                $("select[name='ac_priority'").append('<option value='+k+'>'+v+'</option>');
            });
            $("select[name='ac_priority'").append('<option value="99">Others</option>');
            //$("select[name='subdivision'").html(data.options);
             // change_officeName();            
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
       //  $(".se-pre-con").fadeOut("slow");
           var msg = "";
           if (jqXHR.status !== 422 && jqXHR.status !== 400) {
               msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
           } else {
               if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                   msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
               } else {
                   msg += "Error(s):<strong><ul>";
                   $.each(jqXHR.responseJSON, function (key, value) {
                       msg += "<li>" + value + "</li>";
                   });
                   msg += "</ul></strong>";
               }
           }
           $.alert({
               title: 'Error!!',
               type: 'red',
               icon: 'fa fa-warning',
               content: msg
           });
         },complete: function(){
          getDistrctRecords();
         }
   });
}
function getDistrctRecords(){
   var districtcd = $("#districtcd").val();   
   var token = $("input[name='_token']").val();
   //$(".se-pre-con").fadeIn("slow");
   $.ajax({
       url: "getDistrictACPriorityRecords",
       method: 'POST',
       data: {districtcd:districtcd, _token: token},
       success: function (json) {//alert(data.options);
          $(".se-pre-con").fadeOut("slow");
          $("#memberDetails").html('');
          if(json.status==1)
          {
              //alert(json.options.length);
//              var tr;
//              tr="<table class='table table-bordered table-striped1' width='100%' style='border-top: 2px solid #009fe8;'>";
//              tr+="<tr style='background-color: #f5f8fa'>";
//              tr+="<th colspan='4'>List of Zone";
//              tr+="</th>";
//              tr+="</tr>";
//              tr+="<tr style='background-color: #f5f8fa'>";
//              tr+="<td width='10%'><b>SL#</b></td><td><b>Zone</b></td><td><b>Name of Zone </b></td><td width='10%'><b>Action</b></td>";
//              tr+="</tr>";
//               for (var i = 0; i < json.options.length; i++) {
//                    tr+="<tr><td>" + (i+1) + "</td>";
//                    tr+="<td>" + json.options[i].zone + "</td>";
//                    tr+="<td>" + json.options[i].zonename + "</td>";
//                    tr+="<td><a class='edit_button' title='Edit'  onclick='edit_zone("+json.options[i].zone+");'><i class='fa fa-pencil-alt' aria-hidden='true' value="+json.options[i].zone+"></i></a>";
//                    tr+="</td></tr>"; 
//                } 
//              
//              tr+="</table>";
//              $('#memberDetails').append(tr);
              $('#memberDetails').html(json.options);
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
         $(".se-pre-con").fadeOut("slow");
           var msg = "";
           if (jqXHR.status !== 422 && jqXHR.status !== 400) {
               msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
           } else {
               if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                   msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
               } else {
                   msg += "Error(s):<strong><ul>";
                   $.each(jqXHR.responseJSON, function (key, value) {
                       msg += "<li>" + value + "</li>";
                   });
                   msg += "</ul></strong>";
               }
           }
           $.alert({
               title: 'Error!!',
               type: 'red',
               icon: 'fa fa-warning',
               content: msg
           });
        }
   });
    
 }
</script>
@stop

