
@extends('layouts.master')
@section('content')

 <!--// top-bar -->
 <div class="wrapper">
<!-- Sidebar Holder -->
@include('layouts.sidebar')

<!-- Page Content Holder -->
<div id="content">           
<section class="tables-section">
    <nav aria-label="breadcrumb" style="width:100%;">
        <div class="breadcrumb pagehead1">
           
                {!! Form::button('<i class="fa fa-desktop"></i> Reserve Replacement',['class' => 'btn btn-primary-header add-new-button']) !!}
            
            <div class="col-md-offset-8 pull-right">
<!--                {!! Form::button('Academic Year:&nbsp;',['id'=>'show_academic','class' => 'btn btn-primary-year']) !!}-->
                
            </div>
                <div class="clearfix"></div>
        </div>
    </nav>

    <!-- form -->
<div class="outer-w3-agile">
  {!! Form::open(['url' => 'preGroupReplacement', 'name' => 'preGroupReplacement', 'class' =>'request-info clearfix form-horizontal', 'id' => 'preGroupReplacement', 'method' => 'post','role'=>'','files' => true]) !!}
   {!! Form::hidden('districtcd', session()->get('districtcd_ppds'),['id'=>'districtcd']) !!}
    {!! Form::hidden('poststat', '',['id'=>'poststat']) !!}
  <!-- OLD PWise -->
  <div class="panel-group" id="accordion5" style="width: 49%;float: left;">                                                              
    <div class="panel panel-default">
     <div class="panel-heading1">
     <h6 class="panel-title">
         <a data-toggle="#" data-parent="#accordion5" href="#collapsePV"> <span class="fa fa-minus"></span><span class="highlight"> OLD PERSONNEL </span></a>
     </h6>
     </div>
     <div id="collapsePV" class="panel-collapse collapse5">
      <div class="panel-body" style="min-height: 365px;">                                   
        <div class="row"> 
         <div class='col-sm-6'>
               {!! Form::label('zone', 'Zone:', ['class'=>'highlight required']) !!}
              <div class="form-group">
                  <div class=''>
                      {!! Form::select('zone',[''=>'[Select]'],Null,['id'=>'zone','class'=>'form-control','autocomplete'=>'off']) !!}
                  </div>
              </div>
          </div>


          <div class='col-sm-6'>
            {!! Form::label('phase', 'Phase:', ['class'=>'highlight required']) !!}
           <div class="form-group">
               <div class=''>
                    <select id="phase" class="form-control" name="phase">
                                @php
                                    $phase_data=App\Phase::get();
                                @endphp
                                <option value="">[Select]</option>
                                @foreach ($phase_data as $item)
                                <option value="{{ $item->code}}">{{$item->name}}</option>
                                @endforeach
                    </select>
                </div>
           </div>
       </div>






         <div class='col-sm-12'>
              {!! Form::label('personnel_id', 'Personnel ID:', ['class'=>'highlight required']) !!}
              <div class="form-group">
                  <div class=''>
                      {!! Form::text('personnel_id',null,['id'=>'personnel_id','class'=>'form-control','autocomplete'=>'off','maxlength'=>'11','minlength'=>'11']) !!}
                  </div>
              </div>
          </div>     
        </div>
        <div class="row"> 
            <div class='col-sm-12'> 
               <div id="old_personnel_details" >                            	
                   
               </div>
            </div>
         </div>                
       </div>
      </div>
    </div>                              
  </div> 
  
    <!-- New PWise -->
  <div class="panel-group" id="accordion5" style="width: 50%;float: left;margin-left: 3px;">                                                              
    <div class="panel panel-default">
     <div class="panel-heading1">
     <h6 class="panel-title">
         <a data-toggle="#" data-parent="#accordion5" href="#collapseCV"> <span class="fa fa-minus"></span><span class="highlight"> NEW PERSONNEL </span></a>
     </h6>
     </div>
        <div id="collapseCV" class="panel-collapse collapse5" >
      <div class="panel-body" style="min-height: 365px;">                                   
        <div class="row">      
         <div class='col-sm-11'>
              {!! Form::label('newpersonnel_id', 'New Personnel ID:', ['class'=>'highlight']) !!}
              <div class="form-group">
                  <div class=''>
                      {!! Form::text('newpersonnel_id',null,['id'=>'newpersonnel_id','class'=>'form-control','autocomplete'=>'off','maxlength'=>'11','minlength'=>'11','readonly'=>'true']) !!}
                  </div>
              </div>
          </div>     
        </div>
        <div class="row"> 
            <div class='col-sm-12'> 
                <div id="new_personnel_details">                            	
                   
               </div>
            </div>
         </div>  
         </div> 
               
       </div>
      </div>
    </div>                              
 
<!--  <div class="panel-group" id="accordion5" style="width: 100%;float: left;">                                                              
    <div class="panel panel-default">
     <div class="panel-heading1">
     <h6 class="panel-title">
         <a data-toggle="#" data-parent="#accordion5" href="#collapse"> <span class="fa fa-minus"></span><span class="highlight"> Training, Reason </span></a>
     </h6>
     </div>
     <div id="collapse" class="panel-collapse collapse5">
      <div class="panel-body">                                   
        <div class="row"> 
          <div class='col-sm-3'>
               {!! Form::label('reason', 'Reason for Replacement:', ['class'=>'highlight']) !!}
              <div class="form-group">
                  <div class=''>
                      {!! Form::text('reason',null,['id'=>'reason','class'=>'form-control','autocomplete'=>'off','maxlength'=>'10']) !!}
                  </div>
              </div>
          </div>  
          <div class='col-sm-4'>
              <span style="display:none;" id="venue_sch"><input type="checkbox" id="chkSameVenueTraining" name="chkSameVenueTraining" checked value="true" onclick="return chkSameVenueTraining_change();" />
                {!! Form::label('chkSameVenueTraining', 'Training at Same Venue for New Personnel', ['class'=>'highlight']) !!} </span>
              <div class="form-group">
                  <div class='' style="display:none;" id="difnt_sch">
                       {!! Form::label('training_sch', 'Training Schedule Code:', ['class'=>'highlight required']) !!}
                      {!! Form::select('training_sch',[''=>'[Select]'],Null,['id'=>'training_sch','class'=>'form-control','autocomplete'=>'off']) !!}
                  </div>
              </div>
          </div>
                        
        
        
         </div> 
             
       </div>
      </div>
    </div>                              
  </div> -->
 
  <div class='panel-body'> 
            <div class="form-group text-right permit" id="rw" style="display: none;">                            	
                   {{ Form::button('Search', ['class' => 'btn btn-info', 'type' => 'button','id'=>'search']) }}
                   {{ Form::button('Replace', ['class' => 'btn btn-info', 'type' => 'button','id'=>'replace','disabled'=>'true']) }}
                    {{ Form::button('Print', ['class' => 'btn btn-info', 'type' => 'button','id'=>'print','disabled'=>'true']) }}
                   {{ Form::button('Reset', ['class' => 'btn btn-success' ,'type' => 'reset','id'=>'reset']) }}
            </div>

  </div>

 {!! Form::close() !!}             
</div>
<!--// form -->


</section>

</div>
</div>
<!-- Copyright -->
<script type="text/javascript">
//function chkSameVenueTraining_change()
//{
//    if(document.getElementById('chkSameVenueTraining').checked==true)
//    {
//        $("#difnt_sch").hide();
//        document.getElementById('training_sch').value='';
//    }
//    else
//    {
//        $("#difnt_sch").show();
//    }
//}
$(document).ready(function () {
    getZoneDetails(); 
    $("#zone").change(function(){
    $("#personnel_id").val("");  
 });
   $("#reset").click(function () {
    location.reload(true);
  });
  char_num("#personnel_id");
  //getZoneDetails(); 
  $('#personnel_id').on('change', function () {
    get_personnel_details();   
  });
  $("#search").click(function(){
        document.getElementById('replace').disabled=true;
         var personnel_id = $("#personnel_id").val();
            if(personnel_id == ""){
                $.alert({
                   title: 'Error!!',
                   type: 'red',
                   icon: 'fa fa-exclamation-triangle',
                   content: 'OLD Personnel ID is required'
               });
               return false;
           }
          var zone = $("#zone").val();
         if(zone == ""){
                $.alert({
                   title: 'Error!!',
                   type: 'red',
                   icon: 'fa fa-exclamation-triangle',
                   content: 'Zone is required'
               });
               return false;
           }
           var phase = $("#phase").val();

           if(phase == ""){
                $.alert({
                   title: 'Error!!',
                   type: 'red',
                   icon: 'fa fa-exclamation-triangle',
                   content: 'Phase is required'
               });
               return false;
           }
        var formData = new FormData($("#preGroupReplacement")[0]);
        $(".se-pre-con").fadeIn("slow");
        //var formData = {'zone': forzone,'forassembly': forassembly,'ofc_id': ofc_id,'booked': booked, 'gender': gender,'post_stat': post_stat, '_token': token};
        $.ajax({
            type: "post",
            url: "searchNewReserveDataFR",
            data: formData,
            processData: false,
            contentType: false,
            dataType: 'json',
            success: function (data) {
            $(".se-pre-con").fadeOut("slow"); 
            if(data.status==1)
            {
                $('#new_personnel_details').html(data.options);
                $('#newpersonnel_id').val(data.perid);
                 var poststat=$('#poststat').val(data.poststat);
                document.getElementById('replace').disabled=false;	
            }
            else if(json.status==0)
            {
                $('#new_personnel_details').html(data.options);
                $('#newpersonnel_id').val(data.perid);
                //$('#rw').hide();
                document.getElementById('replace').disabled=true;	
            }
            document.getElementById('print').disabled=true;
       },
        error: function (jqXHR, textStatus, errorThrown) {
            $(".se-pre-con").fadeOut("slow");
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-exclamation-triangle',
                  content: msg
              });
         }
     });
  });
  $("#replace").click(function(){
        
        var personnel_id = $("#personnel_id").val();
            if(personnel_id == ""){
                $.alert({
                   title: 'Error!!',
                   type: 'red',
                   icon: 'fa fa-exclamation-triangle',
                   content: 'OLD Personnel ID is required'
               });
               return false;
           }
         var zone = $("#zone").val();
         if(zone == ""){
                $.alert({
                   title: 'Error!!',
                   type: 'red',
                   icon: 'fa fa-exclamation-triangle',
                   content: 'Zone is required'
               });
               return false;
           }
           var phase = $("#phase").val();
           if(phase == ""){
                $.alert({
                   title: 'Error!!',
                   type: 'red',
                   icon: 'fa fa-exclamation-triangle',
                   content: 'Phase is required'
               });
               return false;
           }
        var formData = new FormData($("#preGroupReplacement")[0]);
        $(".se-pre-con").fadeIn("slow");
        //var formData = {'zone': forzone,'forassembly': forassembly,'ofc_id': ofc_id,'booked': booked, 'gender': gender,'post_stat': post_stat, '_token': token};
        $.ajax({
            type: "post",
            url: "reserveReplaceNewPPData",
            data: formData,
            processData: false,
            contentType: false,
            dataType: 'json',
            success: function (data) {
            $(".se-pre-con").fadeOut("slow"); 
            if(data.status==1)
            {
               // $('#new_personnel_details').html(data.options);
                //$('#newpersonnel_id').val(data.perid);
		document.getElementById('replace').disabled=true;
                document.getElementById('search').disabled=true;
                //document.getElementById('p_id').disabled=true;
                document.getElementById('print').disabled=false;
                $.confirm({
                        title: 'Success!',
                        type: 'green',
                        icon: 'fa fa-check',
                        content: data.options,
                        buttons: {
                            ok: function () {
                                /*$("#swapping_personnel")[0].reset();
                                $("#swapping").attr('disabled',true);
                                $('.form-control-feedback').css('display', 'none');
                                $('.has-success').removeClass('has-success');*/
                                //location.reload(true);
                                print_appt();
                            }

                        }
                    });
            }
            else if(data.status==0)
            {
               // $('#new_personnel_details').html(data.options);
               // $('#newpersonnel_id').val(data.perid);
                //$('#rw').hide();   
                document.getElementById('search').disabled=false;
                document.getElementById('replace').disabled=false;	
	        //$("#venue_sch").hide();
		//$("#difnt_sch").hide();
                document.getElementById('print').disabled=true;
                 $.confirm({
                        title: 'Error!!',
                        type: 'red',
                        icon: 'fa fa-exclamation-triangle',
                        content: data.options,
                        buttons: {
                            ok: function () {
                                /*$("#swapping_personnel")[0].reset();
                                $("#swapping").attr('disabled',true);
                                $('.form-control-feedback').css('display', 'none');
                                $('.has-success').removeClass('has-success');*/
                               // location.reload(true);
                            }

                        }
                 });
            }
            
       },
        error: function (jqXHR, textStatus, errorThrown) {
            $(".se-pre-con").fadeOut("slow");
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-exclamation-triangle',
                  content: msg
              });
              document.getElementById('replace').disabled=false;
         }
     });
  });
   $('#print').click(function () { 
     print_appt();

 });
 
});
 function print_appt(){
    var party_reserve = "R";
    var forZone = $("#zone").val();
    var phase = $("#phase").val();
    var assembly = $("#forassembly").val();
    var group_id = $("#groupid").val();
    var poststat=$('#poststat').val();
    //alert(poststat);
     //var newpersonnel_id = $("#newpersonnel_id").val();
    var no_of_member = "";
    var from = "1";
    var to = "1";
    var datas = {party_reserve: party_reserve,forZone: forZone,phase: phase,poststat:poststat,assembly: assembly,group_id: group_id,no_of_member: no_of_member,from: from,to: to, '_token': $('input[name="_token"]').val()};
    redirectPost_newTab('{{url("getSecondAppointmentLetterReservePDF2019")}}', datas);
}
 function get_personnel_details(){
     var personnel_id = $("#personnel_id").val();
    var zone = $("#zone").val();
    var phase = $("#phase").val();
    if(zone == ""){
           $.alert({
              title: 'Error!!',
              type: 'red',
              icon: 'fa fa-exclamation-triangle',
              content: 'Zone is required'
          });
          return false;
      }
      if(phase == ""){
           $.alert({
              title: 'Error!!',
              type: 'red',
              icon: 'fa fa-exclamation-triangle',
              content: 'Phase is required'
          });
          return false;
      }
    var token = $("input[name='_token']").val();
    $(".se-pre-con").fadeIn("slow");
    $.ajax({
       url: "getOldPersonnelReserveDetails",
       method: 'POST',
       data: {personnel_id: personnel_id, zone:zone,phase:phase, _token: token},
       success: function (json) {//alert(data.options);
          $(".se-pre-con").fadeOut("slow");
          //$("#memberDetails").html('');
          if(json.status==1)
          {
              $('#old_personnel_details').html(json.options);
               $('#rw').show();
               scrollToElement($('#rw'));
          }
          else if(json.status==0)
          {
              $('#old_personnel_details').html(json.options);
              $('#rw').hide();
          }
          document.getElementById('replace').disabled=true;
          document.getElementById('print').disabled=true;
       },
        error: function (jqXHR, textStatus, errorThrown) {
         $(".se-pre-con").fadeOut("slow");
           var msg = "";
           if (jqXHR.status !== 422 && jqXHR.status !== 400) {
               msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
           } else {
               if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                   msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
               } else {
                   msg += "Error(s):<strong><ul>";
                   $.each(jqXHR.responseJSON, function (key, value) {
                       msg += "<li>" + value + "</li>";
                   });
                   msg += "</ul></strong>";
               }
           }
           $.alert({
               title: 'Error!!',
               type: 'red',
               icon: 'fa fa-warning',
               content: msg
           });
        }
   });
 }
  function getZoneDetails(){
    var forDist = $("#districtcd").val();
    var token = $("input[name='_token']").val();
    $(".se-pre-con").fadeIn("slow");
    $.ajax({
       url: "getZoneDetails",
       method: 'POST',
       data: {forDist: forDist, _token: token},
       success: function (data) {//alert(data.options);
         $(".se-pre-con").fadeOut("slow");
          $("select[name='zone'").html('');
          if(data.status==1)
          {
             $("select[name='zone'").append('<option value>[Select]</option>');
            $.each(data.options,function (k, v)
            {
                $("select[name='zone'").append('<option value='+k+'>'+v+'</option>');
            });
           // getMemberDetails();
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
            $(".se-pre-con").fadeOut("slow");
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-warning',
                  content: msg
              });
         }
   });
 }
 function scrollToElement(ele) {
    $('html, body').animate({
        scrollTop: ele.offset().top - 60
    }, 1000);
}
</script>
@stop
