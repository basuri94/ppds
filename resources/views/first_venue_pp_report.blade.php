@extends('layouts.master')
@section('content')
<style>
.p_small_box{
 width: 40px;
 padding: 2px;
  font-size: .9rem;
  line-height: 1.5;
  color: #495057;
  background-color: #fff;
  background-clip: padding-box;
  border: 1px solid #ccc;
  border-radius: 0.15rem;
  transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
  height: calc(1.40rem + 1px);
}
.p_big_box{
 width: 55px;
 padding: 2px;
 font-size: .95rem;
  line-height: 1.5;
  color: #495057;
  background-color: #fff;
  background-clip: padding-box;
  border: 1px solid #ccc;
  border-radius: 0.15rem;
  transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
  height: calc(1.40rem + 1px);
}
</style>
<!--// top-bar -->
 <div class="wrapper">
<!-- Sidebar Holder -->
@include('layouts.sidebar')

<!-- Page Content Holder -->
<div id="content">           
<section class="tables-section">
    <nav aria-label="breadcrumb" style="width:100%;">
        <div class="breadcrumb pagehead1">
           
                {!! Form::button('<i class="fa fa-desktop"></i> First Venue-wise PP Report',['class' => 'btn btn-primary-header add-new-button']) !!}
            
            <div class="col-md-offset-8 pull-right">
<!--  {!! Form::button('Academic Year:&nbsp;',['id'=>'show_academic','class' => 'btn btn-primary-year']) !!}-->
                
            </div>
            <div class="clearfix"></div>
        </div>
    </nav>

    <!-- form -->
<div class="outer-w3-agile">
  {!! Form::open(['url' => 'firstTrAllocation', 'name' => 'firstTrAllocation', 'class' =>'request-info clearfix form-horizontal', 'id' => 'firstTrAllocation', 'method' => 'post','role'=>'','files' => true]) !!}
  {!! Form::hidden('districtcd', session()->get('districtcd_ppds'),['id'=>'districtcd']) !!}
  
  <div class="panel-group" id="accordion5">                                                              
   <div class="panel panel-default">
    <div class="panel-heading1">
     <h6 class="panel-title">
         <a data-toggle="#" data-parent="#accordion5" href="#collapsePV"> <span class="fa fa-minus"></span> <span class="highlight">First Venue-wise PP Report</span></a>
     </h6>
    </div>
    <div id="collapsePV" class="panel-collapse collapse5">
      <div class="panel-body">                                   
       <div class="row"> 
         <div class='col-sm-3'>
            {!! Form::label('subdivision', 'Sub Division:', ['class'=>'highlight required']) !!}
              <!--<span class="highlight">Sub Division</span> -->
              <div class="form-group">
                  <div class=''>
                      {!! Form::select('subdivision',[''=>'[Select]'],null,['id'=>'subdivision','class'=>'form-control','autocomplete'=>'off']) !!}
                  </div>
              </div>
        </div> 

          <div class='col-sm-3'>
              {!! Form::label('venuename', 'Venue Name:', ['class'=>'highlight required']) !!}
              <div class="form-group">
                  <div class=''>
                       {!! Form::select('venuename',[''=>'[Select]'],null,['id'=>'venuename','class'=>'form-control','autocomplete'=>'off']) !!}
                  </div>
              </div>
          </div>
           <div class='col-sm-3'>
              {!! Form::label('trainingtype', 'Training Type:', ['class'=>'highlight required']) !!}
              <div class="form-group">
                  <div class=''>
                       {!! Form::select('trainingtype',$training_type['desc'],null,['id'=>'trainingtype','class'=>'form-control','placeholder'=>'[Select]','autocomplete'=>'off']) !!}
                  </div>
              </div>
          </div>
          <div class='col-sm-3'>
            {!! Form::label('trainingdatetime', 'Training Date & Time:', ['class'=>'highlight required']) !!}
              <div class="form-group">
                  <div class=''>
                     {!! Form::select('trainingdatetime',[''=>'[Select]'],null,['id'=>'trainingdatetime','class'=>'form-control','placeholder'=>'[Select]','autocomplete'=>'off']) !!}
                  </div>
             </div>
        </div>
      </div>
           
          
      <div class="row">                  
         <div class='col-sm-12'>
            <div class="form-group text-right permit">   
               {{ Form::button('EXCEL', ['class' => 'btn btn-info', 'type' => 'button','id'=>'EXCEL']) }}
               {{ Form::button('PDF', ['class' => 'btn btn-info', 'type' => 'button','id'=>'PDF']) }}
               {{ Form::button('Reset', ['class' => 'btn btn-success' ,'type' => 'reset','id'=>'reset']) }}
            </div>
          </div>
      </div>     
       
    </div>                              
   </div>
  </div>  
  </div>
 {!! Form::close() !!}             
</div>
<!--// form -->

</section>

</div>
</div>
<!-- Copyright -->
<script type="text/javascript">
$(document).ready(function () {
change_subdivision(); 
$("#reset").click(function () {
  location.reload(true);  
});
 $('select[name="subdivision"]').on('change', function () {
    change_venueName();    
 });

 $('#PDF').click(function () {
    var subdivision = $("#subdivision").val();
    var venuename = $("#venuename").val();
    var trainingtype = $("#trainingtype").val();
    var trainingdatetime = $("#trainingdatetime").val();
    if(subdivision== ""){
        $.alert({
             title: 'Error!!',
             type: 'red',
             icon: 'fa fa-exclamation-triangle',
             content: 'Subdivision is required'
         });
         return false;
     }
    if(venuename== ""){
        $.alert({
             title: 'Error!!',
             type: 'red',
             icon: 'fa fa-exclamation-triangle',
             content: 'Venue Name is required'
         });
         return false;
     }
      if(trainingtype== ""){
        $.alert({
             title: 'Error!!',
             type: 'red',
             icon: 'fa fa-exclamation-triangle',
             content: 'Training Type is required'
         });
         return false;
     }
      if(trainingdatetime== ""){
        $.alert({
             title: 'Error!!',
             type: 'red',
             icon: 'fa fa-exclamation-triangle',
             content: 'Training Date & Time is required'
         });
         return false;
     }
    var datas = {subdivision: subdivision,venuename: venuename,trainingtype: trainingtype,trainingdatetime: trainingdatetime,'_token': $('input[name="_token"]').val()};
    
    //$(".se-pre-con").fadeIn("slow");
      redirectPost_newTab('{{url("getFirstVenuePPReport")}}', datas);   


 });
   $('#EXCEL').click(function () {
     var subdivision = $("#subdivision").val();
    var venuename = $("#venuename").val();
    var trainingtype = $("#trainingtype").val();
    var trainingdatetime = $("#trainingdatetime").val();
    if(subdivision== ""){
        $.alert({
             title: 'Error!!',
             type: 'red',
             icon: 'fa fa-exclamation-triangle',
             content: 'Subdivision is required'
         });
         return false;
     }
    if(venuename== ""){
        $.alert({
             title: 'Error!!',
             type: 'red',
             icon: 'fa fa-exclamation-triangle',
             content: 'Venue Name is required'
         });
         return false;
     }
      if(trainingtype== ""){
        $.alert({
             title: 'Error!!',
             type: 'red',
             icon: 'fa fa-exclamation-triangle',
             content: 'Training Type is required'
         });
         return false;
     }
      if(trainingdatetime== ""){
        $.alert({
             title: 'Error!!',
             type: 'red',
             icon: 'fa fa-exclamation-triangle',
             content: 'Training Date & Time is required'
         });
         return false;
     }
    var datas = {subdivision: subdivision,venuename: venuename,trainingtype: trainingtype,trainingdatetime: trainingdatetime,'_token': $('input[name="_token"]').val()};
    
    $(".se-pre-con").fadeIn("slow");
      redirectPost('{{url("getFirstVenuePPReportexcel")}}', datas);  
        $(".se-pre-con").fadeOut("slow");


 }); 
});

function change_subdivision(){
  var forDist = $("#districtcd").val();
   var token = $("input[name='_token']").val();
  // $(".se-pre-con").fadeIn("slow");
   $.ajax({
       url: "getSubdivisionData",
       method: 'POST',
       data: {forDist: forDist, _token: token},
       success: function (data) {//alert(data.options);
       //   $(".se-pre-con").fadeOut("slow");
          $("select[name='subdivision'").html('');
          
          if(data.status==1)
          {
            $("select[name='subdivision'").append('<option value>[Select]</option>');
            $.each(data.options,function (k, v)
            {
                $("select[name='subdivision'").append('<option value='+k+'>'+v+'</option>');
            });
            //$("select[name='subdivision'").html(data.options);
             // change_officeName();
            // getMemberDetails();
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
         $(".se-pre-con").fadeOut("slow");
           var msg = "";
           if (jqXHR.status !== 422 && jqXHR.status !== 400) {
               msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
           } else {
               if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                   msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
               } else {
                   msg += "Error(s):<strong><ul>";
                   $.each(jqXHR.responseJSON, function (key, value) {
                       msg += "<li>" + value + "</li>";
                   });
                   msg += "</ul></strong>";
               }
           }
           $.alert({
               title: 'Error!!',
               type: 'red',
               icon: 'fa fa-exclamation-triangle',
               content: msg
           });
         },complete: function(){
          getTrainingDateTime();
         }
   });
}
function getTrainingDateTime(){
     var forDist = $("#districtcd").val();
     var token = $("input[name='_token']").val();
    // $(".se-pre-con").fadeIn("slow");
     $.ajax({
       url: "getTrainingDateTime",
       method: 'POST',
       data: {forDist: forDist, _token: token},
       success: function (data) {//alert(data.options);
       //  $(".se-pre-con").fadeOut("slow");
          $("select[name='trainingdatetime'").html('');
          if(data.status==1)
          {
              $("select[name='trainingdatetime'").html(data.options);
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
            $(".se-pre-con").fadeOut("slow");
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-warning',
                  content: msg
              });
         }
   });
 }
function change_venueName(){
  var subdivision = $("#subdivision").val();
   var token = $("input[name='_token']").val();
   $(".se-pre-con").fadeIn("slow");
   $.ajax({
       url: "getVenueName",
       method: 'POST',
       data: {subdivision: subdivision, _token: token},
       success: function (data) {//alert(data.options);
          $(".se-pre-con").fadeOut("slow");
          $("select[name='venuename'").html('');
          
          if(data.status==1)
          {
            $("select[name='venuename'").append('<option value>[Select]</option>');
            $.each(data.options,function (k, v)
            {
                $("select[name='venuename'").append('<option value='+k+'>'+v+'</option>');
            });
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
         $(".se-pre-con").fadeOut("slow");
           var msg = "";
           if (jqXHR.status !== 422 && jqXHR.status !== 400) {
               msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
           } else {
               if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                   msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
               } else {
                   msg += "Error(s):<strong><ul>";
                   $.each(jqXHR.responseJSON, function (key, value) {
                       msg += "<li>" + value + "</li>";
                   });
                   msg += "</ul></strong>";
               }
           }
           $.alert({
               title: 'Error!!',
               type: 'red',
               icon: 'fa fa-exclamation-triangle',
               content: msg
           });
         }
   });
}
</script>
@stop