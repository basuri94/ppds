@extends('layouts.master')
@section('content')

<!--// top-bar -->
<div class="wrapper">
    <!-- Sidebar Holder -->
    @include('layouts.sidebar')
    <!-- Page Content Holder -->
    <div id="content">           
        <section class="tables-section">
           
                <div class="breadcrumb pagehead1">
                    <span class="headbrand"><i class="fa fa-desktop"></i> List of User
                        <span class="scoop-mcaret1"></span>
                    </span>&nbsp;&nbsp;
                    &nbsp;{!! Form::button(' Create User&nbsp;<i class="fa fa-plus-circle"></i>',['id'=>'add-new-button','class' => 'btn btn-primary-year add-new-button']) !!}  

                </div>
            <div class="outer-w3-agile">
                <div class="form-horizontal">
                    <div class="form-group row">
                              {{csrf_field()}}
                               <div class="datatbl table-responsive" style="">
                              <table class="table table-striped table-bordered table-hover notice-types-table" id="datatable-table" style="width:100%;">
                                <thead>
                                    <tr>
                                        <th>SL#</th>
                                        <th>User ID</th>
                                        <th>Category</th>
                                        <th>Creation Date</th>
                                        <th>Action</th>
                                    </tr>

                                </thead>
                                <tbody></tbody>
                                <!-- Table Footer -->

                            </table>
                               </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<script>
    $(document).ready(function () {
       
        $('#add-new-button').click(function () {
            window.location.href="user_create";
            //$(".se-pre-con").fadeIn("slow"); 
            //var datas = {'user_code': 0,'_token': $('input[name="_token"]').val()};
            //redirectPost('{{url("user_create")}}',datas); 
        });
        create_table();
        
});
function create_table() {
        var table = "";
        var token = $('input[name="_token"]').val();


        $("#datatable-table").dataTable().fnDestroy();
        table = $('#datatable-table').DataTable({
            "processing": true,
            "serverSide": true,
            "ajax": {
                url: "user_list_datatable",
                type: "post",
                data: {'_token': $('input[name="_token"]').val(),'page_permission':$('#page_permission').val()},
                dataSrc: "user_masters",
                error: function (jqXHR, textStatus, errorThrown) {
                    var msg = "";
                    if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                        msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                    } else {
                        if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                            if (jqXHR.responseJSON.exception_code == 23000) {
                                msg += "Some Sql Exception Occured";
                            } else {
                                msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                            }
                        } else {
                            msg += "Error(s):<strong><ul>";
                            $.each(jqXHR.responseJSON, function (key, value) {
                                msg += "<li>" + value + "</li>";
                            });
                            msg += "</ul></strong>";
                        }
                    }
                    $.alert({
                        title: 'Error!!',
                        type: 'red',
                        icon: 'fa fa-warning',
                        content: msg
                    });
                }
            },
            "dataType": 'json',
            "columnDefs":
                    [
                        {className: "table-text", "targets": "_all"},
                        {
                            "targets": 0,
                            "data": "code",
                            "defaultContent": "",
                        },
                        {
                            "targets": 1,
                            "data": "user_id",
                        },
                        {
                            "targets": 2,
                            "data": "category",
                        },
                       {
                            "targets": 3,
                            "data": "creation_date",
                        },
                        {
                            "targets": -1,
                            "data":'action',
                            "searchable": false,
                            "sortable": false,
                            "render": function (data, type, full, meta) {
                                var str_btns="";
                                 str_btns+='<button type="submit"  class="btn btn-warning permission-button btn_new1" id="' +data.p+ '" title="Permission"><i class="fa fa-lock"></i></button>&nbsp&nbsp';
                                   
                                    str_btns+='<button type="submit"  class="btn btn-success  edit-button btn_new1" id="' +data.e+ '" title="Edit"><i class="fa fa-edit"></i></button>&nbsp&nbsp';
                                    
                                      
                                   if(data.d!=''){
                                    str_btns+='<button type="submit"  class="btn btn-danger  delete-button btn_new1" id="' +data.d+ '" title="Delete"><i class="fa fa-trash"></i></button>';
                                }
                                return str_btns;
                            }
                        }
                    ],

            "order": [[1, 'asc']]
        });
        table.on('order.dt search.dt draw.dt', function () {
            $('[data-toggle="tooltip"]').tooltip();
            table.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
                cell.innerHTML = table.page() * table.page.len() + (i + 1);
            });
        });
        table.on('draw.dt', function () {
//table.fnSetColumnVis( 1, false, false ); 
            $('.edit-button').click(function () {

               //$(".se-pre-con").fadeIn("slow");
                var data = this.id;
                var uid=<?php echo \Session::get('code_ppds'); ?>;
                if(data==uid){
                $.alert({
                        title: 'Error!!',
                        type: 'red',
                        icon: 'fa fa-warning',
                        content: "To edit your own password click on change password link"
                    });
                   
                    return false;
                  }
             $(".se-pre-con").fadeIn("slow");
                var datas = {'user_code': data, '_token': $('input[name="_token"]').val()};
                redirectPost('{{url("user_create")}}', datas);
           
            });
            $('.delete-button').click(function () {
                var reply = confirm('Are you sure to delete the record?');
                if (!reply) {
                    return false;
                }
                var data_new = this.id;
                //alert(data_new);
                $.ajax({
                    type: 'post',
                    url: 'user_list_delete',
                    data: {'user_code': data_new, '_token': $('input[name="_token"]').val()},
                    dataType: 'json',
                    success: function (datam) {
                        table.ajax.reload();
                        var msg = "<strong>Record deleted successfully</strong>";
                        $.alert({
                            title: 'Sucuccess!!',
                            type: 'green',
                            icon: 'fa fa-check',
                            content: msg
                        });
                    },
                error: function (jqXHR, textStatus, errorThrown) {
                    $(".se-pre-con").fadeOut("slow");
                    var msg = "";
                    if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                        msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                    } else {
                        if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                            if(jqXHR.responseJSON.exception_code==23000){
                              msg +="You can't delete this record.User is exist in another table."; 
                            }else{
                            msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                        }
                        } else {
                            msg += "Error(s):<strong><ul>";
                            $.each(jqXHR.responseJSON, function (key, value) {
                                msg += "<li>" + value + "</li>";
                            });
                            msg += "</ul></strong>";
                        }
                    }
                    $.alert({
                        title: 'Error!!',
                        type: 'red',
                        icon: 'fa fa-warning',
                        content: msg
                    });
                }
                });
            });
            $('.active_status').click(function () {
                //var data = table.row( $(this).parents('tr') ).data();
                var reply = confirm('Are you sure to Change Status');
                if (!reply) {
                    return false;
                }
                var status_code = this.id;
                //alert(status_code);
                $.ajax({
                    type: 'post',
                    url: 'status_change',
                    data: {'code': status_code, '_token': $('input[name="_token"]').val()},
                    dataType: 'json',
                    success: function (datam) {
                        table.ajax.reload();
                        var msg = "<strong>SUCCESS: Status Changed</strong>";
                        show_message_div("success", msg);
                        scrollToElement($('#message-div'));
                    }
                });

            });
            $('.permission-button').click(function () {
                var data = this.id;
                //alert(data);
                var datas = {'user_code': data, '_token': $('input[name="_token"]').val()};
                redirectPost('{{url("user_permission")}}', datas);
            });
        });
        // table.columns( [-4,-3,-2,-1] ).visible( false );
    }
</script>
<!-- Copyright -->
@stop