
<!DOCTYPE html>
<html>
    <head>
        <?php
        header("Content-Security-Policy: default-src 'self' 'unsafe-inline' 'unsafe-eval'; img-src *; script-src 'self' 'unsafe-inline' 'unsafe-eval';  frame-src 'self' 'unsafe-inline' 'unsafe-eval'; object-src 'self' 'unsafe-inline' 'unsafe-eval'; style-src 'self' 'unsafe-inline'; connect-src 'self' 'unsafe-inline' 'unsafe-eval'; font-src 'self' 'unsafe-inline' 'unsafe-eval';");
       // header("X-XSS-Protection 1; mode=block");
        header("X-Content-Type-Options: nosniff");
        header("X-Frame-Options: SAMEORIGIN");
        header("Set-Cookie: name=value; httpOnly");
        header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");
        ?>
        <?php if (session()->has('code_ppds') == 1) { 
            ?>
            <script type="text/javascript">
                window.location = "/";
            </script>   
        <?php } ?>
        <title>Login :: Election</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="keywords" content="" />
        <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
        <!-- Bootstrap Css -->
        <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
        <!-- Bootstrap Css -->
        <!-- Bars Css -->
        <link rel="stylesheet" href="css/bar.css">    
        <link rel="stylesheet" href="css/morris.css">
        <!--// Bars Css -->
        <!-- Calender Css -->
        <link rel="stylesheet" type="text/css" href="css/pignose.calender.css" />
        <!--// Calender Css -->
        <!-- Common Css -->
        <link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
        <!--// Common Css -->
        <!-- Nav Css -->
        <link rel="stylesheet" href="css/style4.css">
        <!--// Nav Css -->
        <!-- Fontawesome Css -->
        <link href="css/fontawesome-all.css" rel="stylesheet">
        <!--// Fontawesome Css -->
        <!-- web font -->
        <link href='css/googlefont.css' rel='stylesheet' type='text/css'><!--web font-->
        <!-- //web font --> 
        <!-- Bootstrap Validator -->
        <link href="css/bootstrapValidator.css" rel="stylesheet" type="text/css" media="all" />
        <!-- Jquery Confirm -->
        <link href="css/jquery-confirm.min.css" rel="stylesheet" type="text/css" media="all" />
        <style>
            body {
                font-family: 'Open Sans', sans-serif;

                /*background: #ffffff;
                background: -moz-linear-gradient(top, #ffffff 0%, #f6f6f6 47%, #ededed 100%);
                background: -webkit-gradient(left top, left bottom, color-stop(0%, #ffffff), color-stop(47%, #f6f6f6), color-stop(100%, #ededed));
                background: -webkit-linear-gradient(top, #ffffff 0%, #f6f6f6 47%, #ededed 100%);
                background: -o-linear-gradient(top, #ffffff 0%, #f6f6f6 47%, #ededed 100%);
                background: -ms-linear-gradient(top, #ffffff 0%, #f6f6f6 47%, #ededed 100%);
                background: linear-gradient(to bottom, #ffffff 0%, #f6f6f6 47%, #ededed 100%);
                filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#ededed', GradientType=0 );
                */
                background-attachment: fixed;
                background:url(images/loginbg.jpg) no-repeat;
                background-position:center center;
                background-size:cover;	 
            }
            .pageheader{background:#3f96c9 !important; color:#fff;}
            .copyright-w3layouts{background:#3f96c9 !important;border-top: 1px solid #3f96c9;}
        </style>
    </head>
    <body>
        <div class="pageheader">
            <div class="container-fluid clearfix">
                <div class="row">
                    <div class="col-xl-9 col-lg-9  col-md-9 text-left">
                        <div class="col-md-1">
                            <img src="images/Emblem_of_India.png" class="emblem" />
                        </div>
                        <h3 style="font-size: 24px;" class="emblem">General Election To The Bidhan Sabha - 2021 <br> Polling Personnel Management System <br>(PPMS)</h3>
                    </div>
        <!--                    <div class="col-xl-6 col-lg-6 col-md-6 text-center  mt-3 mb-3"><h3>General Election To The Lok Sabha-2019</h3><h5><i>Polling Personnel Management System</i></h5><h5><i>(PPMS)</i></h5></div>-->
                    <div class="col-xl-3 col-lg-3  col-md-3 text-right"><img src="images/election-commission-of-india-seeklogo.png" class="img-fluid wbimg" /></div>
                </div>
            </div>
        </div>
        <!-- main-agileits -->
        <div class="agileits">
            <div class="w3-agileits-info">
                <p class="w3agileits">Login Panel</p>
                {!! Form::open(['url' => 'login_action', 'name' => 'login_form', 'id' => 'login_form', 'method' => 'post' ,'class'=>'login-form animate-form form-horizontal','role'=>'form']) !!}
                <div class='form-group row'>
                    {!! Form::label('username', 'Username',['class'=>'control-label']) !!}
                    {!! Form::text('username', null, ['id'=>'username','class'=>'form-control','placeholder'=>'Enter User Name','autocomplete'=>'off']) !!}
                </div>

                <div class='form-group row'>
                    {!! Form::label('password', 'Password',['class'=>'control-label']) !!}
                    {!! Form::password('password',['id'=>'password','class' => 'form-control', 'placeholder' => 'Password', 'type' => 'password','autocomplete'=>'off']) !!}
                </div>
                <div class='form-group row submit w3-agile'>
                    <input class='btn btn-lg' type='submit' value='SUBMIT'>
                </div>
                {!! Form::close() !!}
            </div>	
        </div>	
        <!-- //agileits -->
        <!-- Copyright -->
        <div class="copyright-w3layouts py-xl-3 py-2 mt-xl-5 mt-4 text-center">
            <div class="row" style="margin-left:0px !important;margin-right: 0px !important;">
                <div class="col-xl-6 col-lg-6 col-md-6 text-left">
                    <p style="padding-left: 4px;text-align: left !important;letter-spacing: 1px !important;">© 2018 Election . All Rights Reserved | Design by
                        <b style="color:#FFF;">National Informatics Center </b>
                    </p>
                </div>
                <div class="col-xl-6 col-lg-6  col-md-6 text-left">
                    <p style="padding-right: 4px;text-align: right !important;letter-spacing: 1px !important;">
                        Site best viewed at 1024 x 768 resolution in I.E 10 +, Mozilla 35 +, Google Chrome 35 +
                    </p>
                </div>  
            </div>
        </div>
        <!--// Copyright --> 
        <!-- js -->
        <script src="js/jquery-2.2.3.min.js"></script>
        <script src="js/bootstrapValidator.js"></script>
        <script src="js/jquery-confirm.min.js"></script>
        
<!--        <script src="{{ asset('js/jquery.validate.min.js')}}"></script>
        <script src="{{ asset('js/formAnimation.js')}}"></script>
        
        <script src="{{ asset('js/shake.js')}}"></script> -->
        <!-- js -->
        <script>
                $(document).ready(function () {
                    var er_new = '';
<?php
if (session()->has('login_error') == 1) {
    $err = session()->get('login_error');

    foreach ($err as $error) {
        ?>
                            //var aa="</n>";

                            er_new += "<div><?php echo $error; ?></div>";
                            //er_new+=aa;
    <?php }
    ?>

                        $.confirm({
                            type: 'red',
                            title: '<span style="color:red;">ERROR!</span>',
                            content: '<span style="color:red;">' + er_new + '</span>',
                            width: '200px',
                            buttons: {
                                ok: function () {
    <?php session()->forget('login_error'); ?>
                                },

                            }
                        });

<?php } ?>
                    $('#login_form')
                            .bootstrapValidator({
                                message: 'This value is not valid',
                                feedbackIcons: {
                                    valid: 'fa fa-check',
                                    invalid: 'fa fa-times',
                                    validating: 'fa fa-refresh'
                                },

                                fields: {
                                    username: {
                                        err: 'tooltip',
                                        validators: {
                                            notEmpty: {
                                                message: 'User Name Is Required'
                                            },
//                        regexp: {
//                        regexp:/^[^*|\":<>[\]{}`\\()',;@&$_]+$/,
//                        message: 'Special charecter not Allowed'
//                        },
                                            tooltip_options: {
                                                username: {trigger: 'focus'}

                                            }

                                        }
                                    },
                                    password: {
                                        err: 'tooltip',
                                        validators: {
                                            notEmpty: {
                                                message: 'Password Is Required'
                                            },
//                                                        regexp: {
//                        regexp:/^[^|\":<>[\]{}`\\()',;]+$/,
//                        message: 'Special charecter not Allowed'
//                        },

                                        }
                                    }

                                }
                            });

                });
        </script>
    </body>
</html>