
@extends('layouts.master')
@section('content')

 <div class="wrapper">
<!-- Sidebar Holder -->
@include('layouts.sidebar')

<!-- Page Content Holder -->
<div id="content">           
<section class="tables-section">
    <nav aria-label="breadcrumb" style="width:100%;">
        <div class="breadcrumb pagehead1">
           
                {!! Form::button('<i class="fa fa-desktop"></i> Personnel Add',['class' => 'btn btn-primary-header add-new-button']) !!}
            
            <div class="col-md-offset-8 pull-right">
<!--                {!! Form::button('Academic Year:&nbsp;',['id'=>'show_academic','class' => 'btn btn-primary-year']) !!}-->
                
            </div>
            <div class="clearfix"></div>
        </div>
    </nav>

    <!-- form -->
<div class="outer-w3-agile">
  {!! Form::open(['url' => 'store_personnel', 'name' => 'personnel_store', 'class' =>'request-info clearfix form-horizontal', 'id' => 'personnel_store', 'method' => 'post','role'=>'','files' => true]) !!}
  {!! Form::hidden('districtcd', session()->get('districtcd_ppds'),['id'=>'districtcd']) !!}
  
  
  <div class="panel-group" id="accordion5" style="padding-top: 0px;">                                                              
    <div class="panel panel-default">
     <div class="panel-heading1">
     <h6 class="panel-title">
         <a data-toggle="#" data-parent="#accordion5" href="#collapseUV"> <span class="fa fa-minus"></span> <span class="highlight">Personnel Details</span> </a>
     </h6>
     </div>
     <div id="collapseUV" class="panel-collapse collapse5">
      <div class="panel-body">                                   
        
      
         <div class="row"> 
            <div class='col-sm-12'>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div >
                <table class="table table-striped table-bordered table-hover " width="100%">
                <thead>
                <tr>
                <td ><b>Field</b></td>
                <td ><b>Details</b></td>

                </tr></thead>
                
                <tbody>
                
                <tr>
                    <td>Select Office</td>
                    <td>         
                        <select class="office_id form-control"  name="office_id" id='office_id' autocomplete="off"></select>

                   
                      </td></tr>
                <tr><td>Name</td><td><div class="form-group"><input type="text" class="form-control" name="officer_name" value="{{ old('officer_name') }}"></div></td></tr>
                <tr><td>Designation</td><td><div class="form-group"><input type="text" class="form-control" name="off_desg" value="{{ old('off_desg') }}"></div></td></tr>
                <tr><td>Present Address1</td><td><div class="form-group"><textarea class="form-control" name="present_addr1">{{ old('present_addr1') }}</textarea></div></td></tr>
                <tr><td>Present Address2</td><td><div class="form-group"><textarea class="form-control" name="present_addr2">{{ old('present_addr2') }}</textarea></div></td></tr>
                <tr><td>Permanent Address1</td><td><div class="form-group"><textarea class="form-control" name="perm_addr1">{{ old('perm_addr1') }}</textarea></div></td></tr>
                <tr><td>Permanent Address2</td><td><div class="form-group"><textarea class="form-control" name="perm_addr2">{{ old('perm_addr2') }}</textarea></div></td></tr>
                <tr><td>Birth Date</td><td><div class="form-group"><input type="text" class="form-control" name="dateofbirth" id="dob" value="{{ old('dateofbirth') }}" placeholder="YYYY-MM-DD 00:00:00"></div></td></tr>
                <tr><td>Gender</td><td><div class="form-group"><select name="gender" class="form-control"><option <?php if(old('gender')=='M') echo 'selected';?> value="M">Male</option><option <?php if(old('gender')=='F') echo 'selected';?> value="F">Female</option></select></div></td></tr>
                <tr><td>Pay Scale</td><td><div class="form-group"><input type="text" class="form-control" name="scale" value="{{ old('scale') }}"></div></td></tr>
                <tr><td>Basic Pay</td><td><div class="form-group"><input type="text" class="form-control" name="basic_pay" value="{{ old('basic_pay') }}"></div></td></tr>
                <tr><td>Group</td><td><div class="form-group"><select name="pgroup" class="form-control"><option <?php if(old('pgroup')=='A') echo 'selected';?> value="A">A</option><option <?php if(old('pgroup')=='B') echo 'selected';?> value="B">B</option><option <?php if(old('pgroup')=='C') echo 'selected';?> value="C">C</option><option <?php if(old('pgroup')=='D') echo 'selected';?> value="D">D</option></select></div></td></tr>
                <tr><td>Garde Pay</td><td><div class="form-group"><input type="text" class="form-control" name="grade_pay" value="{{ old('grade_pay') }}"></div></td></tr>
                <tr><td>Working Status</td><td><div class="form-group"><select name="workingstatus" class="form-control"><option <?php if(old('workingstatus')=='Y') echo 'selected';?> value="Y">Yes</option><option <?php if(old('workingstatus')=='N') echo 'selected';?> value="N">No</option></select></div></td></tr>
                <tr><td>Email</td><td><div class="form-group"><input type="text" class="form-control" name="email" value="{{ old('email') }}"></div></td></tr>
                <tr><td>Phone (Residencial)</td><td><div class="form-group"><input type="text" class="form-control" maxlength="10" name="resi_no" value="{{ old('resi_no') }}"></div></td></tr>
                <tr><td>Mobile</td><td><div class="form-group"><input type="text" name="mob_no" maxlength="10" class="form-control" value="{{ old('mob_no') }}"></div></td></tr>
                <tr><td>Qualification</td><td><div class="form-group"><select class="form-control" name="qualificationcd"><?php foreach($qualification as $q){if(old('qualificationcd')==$q->qualificationcd){echo '<option selected  value="'.$q->qualificationcd.'">'.$q->qualification.'</option>';}else{echo '<option  value="'.$q->qualificationcd.'">'.$q->qualification.'</option>';}} ?></select></div></td></tr>
                <tr><td>Language</td><td><div class="form-group"><select name="languagecd" class="form-control"><?php foreach($language as $l){if(old('languagecd')==$l->languagecd){echo '<option selected  value="'.$l->languagecd.'">'.$l->language.'</option>';}else{echo '<option  value="'.$l->languagecd.'">'.$l->language.'</option>';}}?></select></div></td></tr>
                <tr><td>EPIC</td><td><div class="form-group"><input type="text" class="form-control" name="epic" value="{{ old('epic') }}"></div></td></tr>
               

                <tr><td>Assembly (Permanent)</td><td><div class="form-group"> <select class="assembly_perm form-control"  name="assembly_perm" id='assembly_perm' ></select></div></td></tr>
                <tr><td>Assembly (Temporary)</td><td><div class="form-group">  <select class="assembly_temp form-control"  name="assembly_temp" id='assembly_temp' ></select></td></div></tr>
                <tr><td>Assembly (Office)</td><td> <div class="form-group"> <select class="assembly_off form-control"  name="assembly_off" id='assembly_off' ></select></td></div></tr>


                <tr><td>Serial No</td><td><div class="form-group"><input type="text" class="form-control" name="slno" value="{{ old('slno') }}"></div></td></tr>
                <tr><td>Part No</td><td><div class="form-group"><input type="text" class="form-control" name="partno" value="{{ old('partno') }}"></div></td></tr>
                <tr><td>Post Status</td><td><div class="form-group"><select class="form-control" name="poststat"><?php foreach($poststatus as $ps){if(old('poststat')==$ps->post_stat){echo '<option selected value="'.$ps->post_stat.'">'.$ps->poststatus.'</option>';}{echo '<option  value="'.$ps->post_stat.'">'.$ps->poststatus.'</option>';}} ?></select></div></td></tr>
                <tr><td>Branch IFSC</td><td><div class="form-group"><input class="form-control" type="text" name="branch_ifsc" value="{{ old('branch_ifsc') }}"></div></td></tr>
                <tr><td>Bank Account Number</td><td><div class="form-group"><input class="form-control" type="text" name="bank_acc_no" value="{{ old('bank_acc_no') }}"></div></td></tr>
                <tr><td>Remark</td><td><div class="form-group"><select class="form-control" name="remarks"><?php foreach($remark as $r){if(old('remarks')==$r->remarks_cd){echo '<option selected value="'.$r->remarks_cd.'">'.$r->remarks.'</option>';}else{echo '<option  value="'.$r->remarks_cd.'">'.$r->remarks.'</option>';}} ?></select></div></td></tr>
<!--                <tr><td><div class="form-group"><input type="submit" name="save" value="Save" class="btn btn-info"></div></td></tr>-->
                
                
               <tr><td><?php if(!empty($_GET['save']) && $_GET['save']==1) echo '<p style="color:green;">Personnel added with code - '.$_GET['id'].'.</p>';?></td><td><div class="form-group"><input type="submit" name="save" value="Save" class="btn btn-info" id="save"></div></td></tr>
<!--                <div class="row">  
         <div class='col-sm-12'>
            <div class="form-group text-right permit">                            	
               {{ Form::button('save', ['class' => 'btn btn-info', 'type' => 'submit','id'=>'save']) }}
               
            </div>
          </div>
      </div> -->
                </tbody>
                </table>
                </div>
            </div>
        </div>  
        
         
      </div>
    </div>                              
  </div> 
 
  
  </div>

 
 {!! Form::close() !!}             
</div>
<!--// form -->

</section>
</div>
</div>
<!-- Copyright -->
<script type="text/javascript">
$(document).ready(function () {
//  getoffice();
// var table = "";
//         table = $('#datatable-table').DataTable();
$("#reset").click(function () {
  location.reload(true);  
});
var token = $("input[name='_token']").val();

$("#reset").click(function () {
  location.reload(true);  
});
var token = $("input[name='_token']").val();
$('.assembly_perm,.assembly_temp,.assembly_off').select2({
            placeholder: "Enter Assembly Name Or Code",
            minimumInputLength: 3,
            maximumInputLength: 11,
             
            ajax: {
                url: 'assemblysearch',
                dataType: 'json',
                method: 'POST',
                data: function (params) {
                    return {
                        q: $.trim(params.term),
                         _token: token
                    };
                },
                processResults: function (data) {
                    
                    return {
                        results:  $.map(data.options, function (item) {
                    return {
                        text: item.assemblycd + '-' + item.assemblyname,
                        id: item.assemblycd
                    }
                })
                    };

                },
                cache: true
            }
        }).on("select2:select", function(event) {

}); 

$('.office_id').select2({
            placeholder: "Enter Office Code",
            minimumInputLength: 5,
            maximumInputLength: 11,
             
            ajax: {
                url: 'officeSearch',
                dataType: 'json',
                method: 'POST',
                data: function (params) {
                    return {
                        q: $.trim(params.term),
                         _token: token
                    };
                },
                processResults: function (data) {
                    
                    return {
                        results:  $.map(data.options, function (item) {
                    return {
                        text: item.officecd + '-' + item.office,
                        id: item.officecd
                    }
                })
                    };

                },
                cache: true
            }
        }).on("select2:select", function(event) {
}); 






  $('#personnel_store')
            .bootstrapValidator({
            message: 'This value is not valid',
            feedbackIcons: {
            valid: 'fa fa-check',
                    invalid: 'fa fa-times',
                    validating: 'fa fa-refresh'
            },
            fields: {
                zone: {
                    validators: {
                        notEmpty: {
                            message: 'Zone is required'
                        }
                    }
                }
              }
            }).on('success.form.bv', function (e) {
           // Prevent form submission
            e.preventDefault();
            var action = $(this).attr('action');
            var formData = new FormData($('#personnel_store')[0]);
            var type = $(this).attr('method');
            var token = $("input[name='_token']").val();
            var fd = new FormData();
           
            fd.append('_token', token);
         
          $(".se-pre-con").fadeIn("slow");
            $.ajax({
            type: type,
                    url: action,
                    data: formData,
                    processData: false,
                    contentType: false,
                    dataType: "json",
                    success: function (data) {
//alert(data.status);  

                        $("#save").removeAttr("disabled", true);
                    
                    if(data.status==1)
                    {
                      $(".se-pre-con").fadeOut("slow");            
                      var msg="Record(s) saved successfully";
                       $.confirm({
                            title: 'Success!',
                            type: 'green',
                            icon: 'fa fa-check',
                            content: msg,
                            buttons: {
                                ok: function () {
                                   // location.reload(true);
                                 //  getZoneWiseAssembly();
                                  location.reload(true);
                                 
                                }

                            }
                        });
                      }
                      else if (data.status==2){
                         $(".se-pre-con").fadeOut("slow");            
                        var msg="Record(s) updated successfully";
                         $.confirm({
                              title: 'Success!',
                              type: 'green',
                              icon: 'fa fa-check',
                              content: msg,
                              buttons: {
                                  ok: function () {
                                     // location.reload(true);
                                   //  getZoneWiseAssembly();
                                    location.reload(true);

                                  }

                              }
                          });  
                      }

                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        $(".se-pre-con").fadeOut("slow");
                        $("#save").removeAttr("disabled", true);
                        var msg = "";
                        if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                            msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                        } else {
                            if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                                msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                            } else {
                                msg += "Error(s):<strong><ul>";
                                $.each(jqXHR.responseJSON, function (key, value) {
                                    msg += "<li>" + value + "</li>";
                                });
                                msg += "</ul></strong>";
                            }
                        }
                        $.alert({
                            title: 'Error!!',
                            type: 'red',
                            icon: 'fa fa-warning',
                            content: msg
                        });
                    }
            });
 
     });
 

});



 


        

        
</script>
@stop

