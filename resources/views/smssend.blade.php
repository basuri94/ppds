@extends('layouts.master')
@section('content')
<style>
    .dropdown-menu {
        /* right: 67%;*/
        /* left: auto; */
        min-width: 0px !important;
        width: 230px !important;
        padding: 1em !important;
    }
</style>
<div class="wrapper">
    <!-- Sidebar Holder -->
    @include('layouts.sidebar')

    <!-- Page Content Holder -->
    <div id="content">
        <section class="tables-section">
            <nav aria-label="breadcrumb" style="width:100%;">


                <div class="breadcrumb pagehead1">
                    
                    {!! Form::button(' Send SMS  &nbsp;<i
                        class="fa fa-plus-circle"></i>',['id'=>'show_save_sms','class' => 'btn btn-primary-year
                    add-new-button']) !!}


                    <div class="clearfix"></div>
                </div>
            </nav>

            <!-- form -->
            <div class="outer-w3-agile">
                {!! Form::open(['url' => 'smssent', 'name' => 'smssent', 'class' =>'request-info clearfix
                form-horizontal', 'id' =>'smssent', 'method' => 'post','role'=>'','files' => true]) !!}
                {!! Form::hidden('districtcd', session()->get('districtcd_ppds'),['id'=>'districtcd']) !!}
                <div class="panel-group" id="accordion5">
                    <div class="panel panel-default">
                        <div class="panel-heading1">
                            <h6 class="panel-title">
                                <a data-toggle="#" data-parent="#accordion5" href="#collapsePV"> <span
                                        class="fa fa-minus"></span> <span class="highlight">Training Records
                                        Available</span> </a>
                            </h6>
                        </div>
                        <div id="collapsePV" class="panel-collapse collapse5">
                            <div class="panel-body">
                                <div class="row">


                                    <div class='col-sm-3'>
                                        {!! Form::label('smsrecord', 'Records:', ['class'=>'highlight']) !!}
                                        <div class="form-group">
                                            <div class=''>
                                                {!!
                                                Form::text('smsrecord',null,['id'=>'smsrecord','class'=>'form-control','autocomplete'=>'off','readonly'])
                                                !!}
                                            </div>
                                        </div>
                                    </div>


                                </div>



                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel-group" id="accordion5" style="padding-top: 0px;">
                    <div class="panel panel-default">
                        <div class="panel-heading1">
                            <h6 class="panel-title">
                                <a data-toggle="#" data-parent="#accordion5" href="#collapseUV"> <span
                                        class="fa fa-minus"></span> <span class="highlight">Send SMS </span></a>
                            </h6>
                        </div>
                        <div id="collapseUV" class="panel-collapse collapse5">
                            <div class="panel-body">
                                <div class="row">



                                    <div class='col-sm-3'>
                                        {!! Form::label('from', 'From Sms:', ['class'=>'highlight required']) !!}
                                        <div class="form-group">
                                            <div class=''>
                                                {!!
                                                Form::text('from',null,['id'=>'from','class'=>'form-control','autocomplete'=>'off','readonly','maxlength'=>'4'
                                                ]) !!}
                                            </div>
                                        </div>
                                    </div>

                                    <div class='col-sm-3'>
                                        {!! Form::label('to', 'To Sms:', ['class'=>'highlight required']) !!}
                                        {{-- <span style="color:red;font-size: 12px;">[Count should not be greater than 999]</span> --}}
                                        <div class="form-group">
                                            <div class=''>
                                                {!!
                                                Form::text('to',null,['id'=>'to','class'=>'form-control','autocomplete'=>'off','readonly','maxlength'=>'4'])
                                                !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class='col-sm-12'>
                                        <div class="form-group text-right permit">
                                            {{ Form::button('Send', ['class' => 'btn btn-info', 'type' => 'submit','id'=>'Send']) }}
                                            {{ Form::button('Reset', ['class' => 'btn btn-success' ,'type' => 'reset','id'=>'reset']) }}
                                        </div>
                                    </div>
                                </div>
                                {!! Form::close() !!}
                                <div class="row">
                                    <div class='col-sm-12'>
                                        <div id='OrderDateNoDetails'></div>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>


                </div>



            </div>
            <!--// form -->

        </section>
    </div>
</div>
<!-- Copyright -->
<script type="text/javascript">
    $(document).ready(function () {
        getSmsRecordAvailable('');

        $("#reset").click(function () {
            location.reload(true);
        });
char_num("#from");
char_num("#to");
   

        $('#smssent')
                .bootstrapValidator({
                    message: 'This value is not valid',
                    feedbackIcons: {
                        valid: 'fa fa-check',
                        invalid: 'fa fa-times',
                        validating: 'fa fa-refresh'
                    },
                    fields: {
                        from: {
                            validators: {
                                notEmpty: {
                                    message: 'From PP is required'
                                }
                            }
                        },
                        to: {
                            validators: {
                                notEmpty: {
                                    message: 'PP count is required'
                                }
                            }
                        }
                       
                    }
                }).on('success.form.bv', function (e) {
            // alert('HI');
            // Prevent form submission
            e.preventDefault();
            var action = $(this).attr('action');

            var from = $("#from").val();
           var to = $("#to").val();
         
            var type = $(this).attr('method');
            var token = $("input[name='_token']").val();
          
            var fd = new FormData();
           fd.append('from', from);
           fd.append('to', to);
         

            fd.append('_token', token);

              $(".se-pre-con1").show();
            $.ajax({
                type: type,
                url: action,
                data: fd,
                processData: false,
                contentType: false,
                dataType: "json",
                success: function (data) {
                    $(".se-pre-con1").fadeOut("slow");
                    if (data.status == 1)
                    {

                          $(".se-pre-con1").hide();

                        //var msg = data.sendSms + " Venue Record(s) send successfully.<br> ";
                      

                        var  msg = data.mobileUser + " user message send successfully";
                        $.confirm({
                            title: 'Success!',
                            type: 'green',
                            icon: 'fa fa-check',
                            content: msg,
                            buttons: {
                                ok: function () {
                                    getSmsRecordAvailable();
                                 
                                }

                            }
                        });
                    } 
$("#Send").removeAttr("disabled");
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $(".se-pre-con1").fadeOut("slow");

                    $("#Send").removeAttr("disabled");
                    var msg = "";
                    if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                        msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                    } else {
                        if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                            msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                        } else {
                            msg += "Error(s):<strong><ul>";
                            $.each(jqXHR.responseJSON, function (key, value) {
                                msg += "<li>" + value + "</li>";
                            });
                            msg += "</ul></strong>";
                        }
                    }
                    $.alert({
                        title: 'Error!!',
                        type: 'red',
                        icon: 'fa fa-warning',
                        content: msg
                    });
                }
            });


        });


    });

    

    function getSmsRecordAvailable(phase){
  
   
    var token = $("input[name='_token']").val();
    // $(".se-pre-con").fadeIn("slow");
     $.ajax({
       url: "getSmsRecordAvailable",
       method: 'POST',
       data: { _token: token,phase:phase},
       success: function (data) {
          $("#smsrecord").val("");
              
               
          if(data.status==1)
          {
            
           
              $("#smsrecord").val(data.phonedata);
             
          }
       
       },
        error: function (jqXHR, textStatus, errorThrown) {
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-exclamation-triangle',
                  content: msg
              });
         },
         complete:function(){
            SMSRecordssendSpecial();
         }
   });
}
function SMSRecordssendSpecial(){
var training_type=$('#training_type').val();
var phase= $('#phase').val();
  var token = $("input[name='_token']").val();
    // $(".se-pre-con").fadeIn("slow");
     $.ajax({
       url: "SMSRecordssendSpecial",
       method: 'POST',
       data: { _token: token},
       success: function (data) {
          $("#from").val("");
             
          if(data.status==1)
          {
              $("#from").val(data.SmsSendAlready);
              $("#to").val(data.nextSmsLot);
          }
         
       },
        error: function (jqXHR, textStatus, errorThrown) {
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-exclamation-triangle',
                  content: msg
              });
         }
   });
}

</script>
@stop