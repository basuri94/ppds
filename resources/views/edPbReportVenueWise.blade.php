
@extends('layouts.master')
@section('content')

 <!--// top-bar -->
 <div class="wrapper">
<!-- Sidebar Holder -->
@include('layouts.sidebar')

<!-- Page Content Holder -->
<div id="content">           
<section class="tables-section">
    <nav aria-label="breadcrumb" style="width:100%;">
        <div class="breadcrumb pagehead1">
           
                {!! Form::button('<i class="fa fa-desktop"></i> ED & PB Report Venue Wise',['class' => 'btn btn-primary-header add-new-button']) !!}
            
            <div class="col-md-offset-8 pull-right">
<!--                {!! Form::button('Academic Year:&nbsp;',['id'=>'show_academic','class' => 'btn btn-primary-year']) !!}-->
                
            </div>
                <div class="clearfix"></div>
        </div>
    </nav>

    <!-- form -->
<div class="outer-w3-agile">
  {!! Form::open(['url' => 'getedpbReportVenueWise', 'name' => 'getedpbReportVenueWise', 'class' =>'request-info clearfix form-horizontal', 'id' => 'getedpbReportVenueWise', 'method' => 'post','role'=>'','files' => true]) !!}
  {!! Form::hidden('districtcd', session()->get('districtcd_ppds'),['id'=>'districtcd']) !!}
<!--  {!! Form::hidden('subdivcd', session()->get('subdivisioncd_ppds'),['id'=>'subdivcd']) !!}
  {!! Form::hidden('blockcd', session()->get('blockmunicd_ppds'),['id'=>'blockcd']) !!}-->
  
<!--  <span class="highlight">[Note: Record(s) transfered from Personnel to Zonewise]</span>-->
   <div class="panel-group" id="accordion5">                                                              
    <div class="panel panel-default">
     <div class="panel-heading1">
     <h6 class="panel-title">
         <a data-toggle="#" data-parent="#accordion5" href="#collapseUV"> <span class="fa fa-minus"></span><span class="highlight"> ED & PB Report Venue Wise </span></a>
     </h6>
     </div>
     <div id="collapseUV" class="panel-collapse collapse5">
      <div class="panel-body">                                   
        <div class="row"> 
            <div class='col-sm-3'>
               {!! Form::label('forZone', 'For Zone:', ['class'=>'highlight required']) !!}
                <div class="form-group">
                    <div class=''>
                        {!! Form::select('forZone',[''=>'[Select]'],Null,['id'=>'forZone','class'=>'form-control','autocomplete'=>'off']) !!}
                    </div>
                </div>
          </div>
            <div class='col-sm-3'>
                                        {!! Form::label('typeOfEdPd', 'Type Of ED & PD:', ['class'=>'highlight required']) !!}
                                        <div class="form-group">
                                            <div class=''>
                                                {!! Form::select('typeOfEdPd',[''=>'[Select]','E'=>'Election Duty','P'=>'Postal Ballot'],Null,['id'=>'typeOfEdPd','class'=>'form-control','autocomplete'=>'off']) !!}
                                            </div>
                                        </div>
                                    </div>
           <div class='col-sm-3'>
               {!! Form::label('subdivision', 'Sub Division:', ['class'=>'highlight']) !!}
              <!--<span class="highlight">Sub Division</span> -->
              <div class="form-group">
                  <div class=''>
                      {!! Form::select('subdivision',[''=>'[Select]'],null,['id'=>'subdivision','class'=>'form-control','autocomplete'=>'off']) !!}
                  </div>
              </div>
          </div>
           <div class='col-sm-3'>
              {!! Form::label('venuename', 'Venue Name:', ['class'=>'highlight']) !!}
              <div class="form-group">
                  <div class=''>
                       {!! Form::select('venuename',[''=>'[Select]'],null,['id'=>'venuename','class'=>'form-control','autocomplete'=>'off']) !!}
                  </div>
              </div>
          </div>
             
           
        </div>
          <div class="row"> 
              <div class='col-sm-3'>
              {!! Form::label('subvenuename', 'Sub-venue Name:', ['class'=>'highlight']) !!}
              <div class="form-group">
                  <div class=''>
                       {!! Form::select('subvenuename',[''=>'[Select]'],null,['id'=>'subvenuename','class'=>'form-control','autocomplete'=>'off']) !!}
                  </div>
              </div>
          </div>
              <div class='col-sm-3'>
               {!! Form::label('trainingdatetime', 'Training Date & Time:', ['class'=>'highlight']) !!}
              <div class="form-group">
                  <div class=''>
                     {!! Form::select('trainingdatetime',[''=>'[Select]'],null,['id'=>'trainingdatetime','class'=>'form-control','placeholder'=>'[Select]','autocomplete'=>'off']) !!}
                  </div>
             </div>
          </div>
              <div class='col-sm-3'>
              {!! Form::label('assembly', 'Assembly:', ['class'=>'highlight']) !!}
              <div class="form-group">
                  <div class=''>
                      {!! Form::select('assembly',[''=>'[Select]'],null,['id'=>'assembly','class'=>'form-control','autocomplete'=>'off']) !!}
                  </div>
              </div>
          </div> 
              
       </div>
          
         <div class="row"> 
           <div class='col-sm-12'>                    
            <div class="form-group text-right permit">  
               {{ Form::button('EXCEL', ['class' => 'btn btn-info', 'type' => 'button','id'=>'EXCEL']) }}
              
               {{ Form::button('Reset', ['class' => 'btn btn-success' ,'type' => 'reset','id'=>'reset']) }}
            </div>
          </div>
        </div>  
      </div>
    </div>                              
  </div> 
                             
</div> 
  
   
  

 {!! Form::close() !!}             
</div>
<!--// form -->


</section>

</div>
</div>
<!-- Copyright -->
<script type="text/javascript">
$(document).ready(function () {
 $("#reset").click(function () {
  location.reload(true);  
});
 
getZoneDetails(); 
 
 $('select[name="subdivision"]').on('change', function () {    
    change_venueName();
 });

$('select[name="venuename"]').on('change', function () {
    get_SubvenueName();    
 });
//  $('select[name="assembly"]').on('change', function () {     
//    get_SubvenueName();    
// });

  $('#EXCEL').click(function () {
  
    var forZone = $("#forZone").val();
    var assembly = $("#assembly").val();
    var subdivision=$('#subdivision').val();
    var venuename=$('#venuename').val();
     var subvenuename=$('#subvenuename').val();
    var trainingdatetime=$('#trainingdatetime').val();
   var typeOfEdPd=$('#typeOfEdPd').val();
 
     if(forZone==""){
        $.alert({
             title: 'Error!!',
             type: 'red',
             icon: 'fa fa-exclamation-triangle',
             content: 'For Zone is required'
         });
         return false;
     }
    if(typeOfEdPd==""){
        $.alert({
             title: 'Error!!',
             type: 'red',
             icon: 'fa fa-exclamation-triangle',
             content: 'Type is required'
         });
         return false;
     }
   

    var datas = {subdivision:subdivision,venuename:venuename,trainingdatetime:trainingdatetime,forZone: forZone,assembly: assembly,subvenuename:subvenuename,typeOfEdPd:typeOfEdPd,'_token': $('input[name="_token"]').val()};
    redirectPost('{{url("getEDPBEXCEL")}}', datas);
   


 });
  
   
});
function getZoneDetails(){
     var forDist = $("#districtcd").val();
     var token = $("input[name='_token']").val();
     $(".se-pre-con").fadeIn("slow");
     $.ajax({
       url: "getZoneDetails",
       method: 'POST',
       data: {forDist: forDist, _token: token},
       success: function (data) {//alert(data.options);
         $(".se-pre-con").fadeOut("slow");
          $("select[name='forZone'").html('');
          if(data.status==1)
          {
             $("select[name='forZone'").append('<option value>[Select]</option>');
            $.each(data.options,function (k, v)
            {
                $("select[name='forZone'").append('<option value='+k+'>'+v+'</option>');
            });
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
            $(".se-pre-con").fadeOut("slow");
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-exclamation-triangle',
                  content: msg
              });
         },
          complete: function () {
            change_subdivision();
        }
   });
}
function getAssemblyDetails(){
    var subdivision = $("#subdivision").val();
     var token = $("input[name='_token']").val();
     $(".se-pre-con").fadeIn("slow");
     $.ajax({
       url: "getSubAssemblyDetails",
       method: 'POST',
       data: {subdivision: subdivision, _token: token},
       success: function (data) {//alert(data.options);
         $(".se-pre-con").fadeOut("slow");
          $("select[name='assembly'").html('');
          if(data.status==1)
          {
              $("select[name='assembly'").html(data.options);
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
            $(".se-pre-con").fadeOut("slow");
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-exclamation-triangle',
                  content: msg
              });
         }
   });
}


function getTrainingDateTime(){
     var forDist = $("#districtcd").val();
     var token = $("input[name='_token']").val();
     //$(".se-pre-con").fadeIn("slow");
     $.ajax({
       url: "get2ndTrainingDateTime",
       method: 'POST',
       data: {forDist: forDist, _token: token},
       success: function (data) {//alert(data.options);
         $(".se-pre-con").fadeOut("slow");
          $("select[name='trainingdatetime'").html('');
          if(data.status==1)
          {
              $("select[name='trainingdatetime'").html(data.options);
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
            $(".se-pre-con").fadeOut("slow");
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-warning',
                  content: msg
              });
         }
   });
 }
 
 

function change_subdivision(){
  var forDist = $("#districtcd").val();
   var token = $("input[name='_token']").val();
  // $(".se-pre-con").fadeIn("slow");
   $.ajax({
       url: "getSubdivisionData",
       method: 'POST',
       data: {forDist: forDist, _token: token},
       success: function (data) {//alert(data.options);
       //   $(".se-pre-con").fadeOut("slow");
          $("select[name='subdivision'").html('');
          
          if(data.status==1)
          {
            $("select[name='subdivision'").append('<option value>[Select]</option>');
            $.each(data.options,function (k, v)
            {
                $("select[name='subdivision'").append('<option value='+k+'>'+v+'</option>');
            });
            //$("select[name='subdivision'").html(data.options);
             // change_officeName();
            // getMemberDetails();
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
         $(".se-pre-con").fadeOut("slow");
           var msg = "";
           if (jqXHR.status !== 422 && jqXHR.status !== 400) {
               msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
           } else {
               if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                   msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
               } else {
                   msg += "Error(s):<strong><ul>";
                   $.each(jqXHR.responseJSON, function (key, value) {
                       msg += "<li>" + value + "</li>";
                   });
                   msg += "</ul></strong>";
               }
           }
           $.alert({
               title: 'Error!!',
               type: 'red',
               icon: 'fa fa-exclamation-triangle',
               content: msg
           });
         },
        complete: function () {
            getTrainingDateTime();
        }
   });
}

function change_venueName(){
  var subdivision = $("#subdivision").val();
   var token = $("input[name='_token']").val();
   //$(".se-pre-con").fadeIn("slow");
   $.ajax({
       url: "get2ndVenueName",
       method: 'POST',
       data: {subdivision: subdivision, _token: token},
       success: function (data) {//alert(data.options);
          //$(".se-pre-con").fadeOut("slow");
          $("select[name='venuename'").html('');
          
          if(data.status==1)
          {
            $("select[name='venuename'").append('<option value>[Select]</option>');
            $.each(data.options,function (k, v)
            {
                $("select[name='venuename'").append('<option value='+k+'>'+v+'</option>');
            });
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
         $(".se-pre-con").fadeOut("slow");
           var msg = "";
           if (jqXHR.status !== 422 && jqXHR.status !== 400) {
               msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
           } else {
               if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                   msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
               } else {
                   msg += "Error(s):<strong><ul>";
                   $.each(jqXHR.responseJSON, function (key, value) {
                       msg += "<li>" + value + "</li>";
                   });
                   msg += "</ul></strong>";
               }
           }
           $.alert({
               title: 'Error!!',
               type: 'red',
               icon: 'fa fa-exclamation-triangle',
               content: msg
           });
         },
        complete: function () {
            getAssemblyDetails();
        }
   });
}

function get_SubvenueName(){
//alert('hi');
    var venuename = $("#venuename").val();
    var trainingdatetime = $("#trainingdatetime").val();
//    if(venuename=="" || trainingdatetime==""){
//        return false;
//    }
    var token = $("input[name='_token']").val();
    $(".se-pre-con").fadeIn("slow");
    $.ajax({
        type: 'POST',
        url: 'get2ndSubVenueName',
        data: {venuename:venuename,trainingdatetime:trainingdatetime, _token:token},
        dataType: 'json',
        success: function (data) {
        $(".se-pre-con").fadeOut("slow");
        $("select[name='subvenuename'").html('');
        if(data.status==1)
        {
         $("select[name='subvenuename'").append('<option value>[Select]</option>');
            $.each(data.options,function (k, v)
            {
                $("select[name='subvenuename'").append('<option value='+k+'>'+v+'</option>');
            });
        }
        else{
            var str="<span style='color: red;'>Sub Venue is required</span>";
        }
      
      
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $(".se-pre-con").fadeOut("slow");
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-exclamation-triangle',
                  content: msg
              });
         }
    });
}
</script>
@stop
