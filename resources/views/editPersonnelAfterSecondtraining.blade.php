
@extends('layouts.master')
@section('content')

 <div class="wrapper">
<!-- Sidebar Holder -->
@include('layouts.sidebar')

<!-- Page Content Holder -->
<div id="content">           
<section class="tables-section">
    <nav aria-label="breadcrumb" style="width:100%;">
        <div class="breadcrumb pagehead1">
           
                {!! Form::button('<i class="fa fa-desktop"></i> Personnel Bank Details Update',['class' => 'btn btn-primary-header add-new-button']) !!}
            
            <div class="col-md-offset-8 pull-right">
<!--                {!! Form::button('Academic Year:&nbsp;',['id'=>'show_academic','class' => 'btn btn-primary-year']) !!}-->
                
            </div>
            <div class="clearfix"></div>
        </div>
    </nav>

    <!-- form -->
<div class="outer-w3-agile">
{!! Form::open(['url' => 'update_personnel_new', 'name' => 'update_personnel_new', 'class' =>'request-info clearfix form-horizontal', 'id' => 'update_personnel_new', 'method' => 'post','role'=>'','files' => true]) !!}
  {!! Form::hidden('districtcd', session()->get('districtcd_ppds'),['id'=>'districtcd']) !!}
  {!! Form::hidden('poststat_personnel', '',['id'=>'poststat_personnel']) !!}
  
  
  <div class="panel-group" id="accordion5" style="padding-top: 0px;">                                                              
    <div class="panel panel-default">
     <div class="panel-heading1">
     <h6 class="panel-title">
         <a data-toggle="#" data-parent="#accordion5" href="#collapseUV"> <span class="fa fa-minus"></span> <span class="highlight">Personnel Bank Details Update</span> </a>
     </h6>
     </div>
     <div id="collapseUV" class="panel-collapse collapse5">
      <div class="panel-body">                                   
        
      
         <div class="row"> 
             <div class='col-sm-4'>
              {!! Form::label('personnel_id', 'Personnel ID:', ['class'=>'highlight']) !!}
              <div class="form-group">
                  <div class=''>
                      {!! Form::text('personnel_id',null,['id'=>'personnel_id','class'=>'form-control','autocomplete'=>'off','maxlength'=>'11']) !!}
                  </div>
              </div>
              
          </div>
              </div>
              <div class="row"> 
            <div class='col-sm-12'>
          
          <div >
                <table class="table table-striped table-bordered table-hover " width="100%">
                <thead>
                <tr>
                <td ><b>Field</b></td>
                <td ><b>Details</b></td>

                </tr></thead>
                
                <tbody>
                
               
                    
                
			   
               <tr><td>Name</td><td><div class="form-group"><input type="text" class="form-control" name="officer_name" id="officer_name" value="" ></div></td></tr>
           <tr><td colspan="2"><div class="form-group" style="background-color: #210252;color: white;font-size: 18px;display: none;" id="change_div2"></div></td></tr>
               <tr><td>Mobile</td><td><div class="form-group"><input type="text" name="mob_no" class="form-control" id="mob_no" value=""></div></td></tr>
              <tr><td>Branch IFSC</td><td><div class="form-group"><input class="form-control" type="text" name="branch_ifsc" id="branch_ifsc" value=""></div></td></tr>
                <tr><td>Bank Account Number</td><td><div class="form-group"><input class="form-control" type="text" name="bank_acc_no" id="bank_acc_no" value=""></div></td></tr> 
               
               
                
                </tbody>
                </table>
                </div>
            </div>
        </div>  
        
           <div class="row" > 
          <div class='col-md-6 col-md-offset-5'>                    
            <div class="form-group">                            	
                   {{ Form::button('Update', ['class' => 'btn btn-success', 'type' => 'submit','id'=>'submit']) }}

            </div>
          </div>
          
         </div>
      </div>
    </div>                              
  </div> 
 
  
  </div>

 
 {!! Form::close() !!}             
</div>
<!--// form -->

</section>
</div>
</div>
<!-- Copyright -->
<script type="text/javascript">
    $(document).ready(function () {
      $('#personnel_id').on('change', function () {
    get_personnel_details();   
  });
   $('#update_personnel_new').bootstrapValidator({

        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {

            
            mob_no: {
                validators: {
                    notEmpty: {
                        message: 'Mobile Number is required'
                    },
                      digits: {
                                    message: 'Mobile Number is not valid'
                                },
                                stringLength: {
                                    min: 10,
                                    max: 10,
                                    message: 'Mobile Number must have 10 digit'
                                }
                            }
                
            },
            branch_ifsc: {
                validators: {
                    notEmpty: {
                        message: 'Branch IFSC is required'
                    }
                }
            },
            bank_acc_no: {
                validators: {
                    notEmpty: {
                        message: 'Bank Account No is required'
                    }
                }
            },
         
            
        }

    }).on('success.form.bv', function (e) {
        e.preventDefault();
        var action = $(this).attr('action');
        var type = $(this).attr('method');
        
        var formData = new FormData($(this)[0]);
        $(".se-pre-con").fadeIn("slow");
        $.ajax({
            type: type,
            url: action,
            data: formData,
            processData: false,
            contentType: false,
            dataType: "json",
            success: function (data) {
                $(".se-pre-con").fadeOut("slow");

                if (data.status == 1) { 
                    var msg="Record updated successfully";
                    $.confirm({
                       title: 'Success!',
                       type: 'green',
                       icon: 'fa fa-check',
                       content: msg,
                       buttons: {
                           ok: function () {     
                               $("#submit").attr('disabled',true);
                                location.reload(true);
                             
                           }
                       }
                   });
                } 
            },
            error: function (jqXHR, textStatus, errorThrown) {
            $(".se-pre-con").fadeOut("slow");
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-exclamation-triangle',
                  content: msg
              });
              $("#Submit").attr('disabled',false);
           }
        });
    });
    });

function get_personnel_details(){
    var personnel_id = $("#personnel_id").val();
   
  
    var token = $("input[name='_token']").val();
    $(".se-pre-con").fadeIn("slow");
    $.ajax({
       url: "getPersonnelDetailsAfterGroupTraining",
       method: 'POST',
       data: {personnel_id: personnel_id, _token: token},
       success: function (json) {//alert(data.options);
          $(".se-pre-con").fadeOut("slow");
          //$("#memberDetails").html('');
          if(json.status==1)
          {
            //  $('#old_personnel_details').html(json.options);
                $('#officer_name').val(json.options[0].officer_name);
                 $('#mob_no').val(json.options[0].mob_no);
                  $('#branch_ifsc').val(json.options[0].branch_ifsc);
                   $('#bank_acc_no').val(json.options[0].bank_acc_no);
                    $('#poststat_personnel').val(json.options[0].poststat);
                   
            
              
          }
          else if(json.status==0)
          {
             $('#officer_name').val('');
                 $('#mob_no').val('');
                  $('#branch_ifsc').val('');
                   $('#bank_acc_no').val('');
                   $('#poststat_personnel').val('');
                   $.alert({
                    title: 'Not Found!!',
                    type: 'red',
                    icon: 'fa fa-exclamation-triangle',
                    content: 'No record found'
                });
          }
        show_Det();
       },
        error: function (jqXHR, textStatus, errorThrown) {
         $(".se-pre-con").fadeOut("slow");
           var msg = "";
           if (jqXHR.status !== 422 && jqXHR.status !== 400) {
               msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
           } else {
               if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                   msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
               } else {
                   msg += "Error(s):<strong><ul>";
                   $.each(jqXHR.responseJSON, function (key, value) {
                       msg += "<li>" + value + "</li>";
                   });
                   msg += "</ul></strong>";
               }
           }
           $.alert({
               title: 'Error!!',
               type: 'red',
               icon: 'fa fa-warning',
               content: msg
           });
        }
   });
 }
 
 
function show_Det(){
var personnel_id = $("#personnel_id").val();
      $.ajax({
                  type: 'post',
                  url: 'get_personnel_details',
                  data: {'personnel_id': personnel_id, '_token': $('input[name="_token"]').val()},
                  dataType: 'json',
                  success: function (data) {
                   if(data.length!=0){
                      var str=" Poststat:"+data[0].poststat+", For Assembly "+data[0].forassembly+", GroupId:"+data[0].groupid+",Booked:"+data[0].booked+"." 
                   //$("#change_div1").hide();
                   $("#change_div2").show();
                   $("#change_div2").html(str);
        }else{
             $("#change_div1").hide();
                   $("#change_div2").hide();
                   $("#change_div2").html('');
        }
                  }
                });  
    }




   

        
</script>
@stop

