@extends('layouts.master')
@section('content')
<div class="wrapper">
    @include('layouts.sidebar')
    <div id="content">           
        <section class="tables-section">
            <nav aria-label="breadcrumb" style="width:100%;">
                <div class="breadcrumb pagehead1">
                        {!! Form::button('<i class="fa fa-desktop"></i>After First Randomisation Assembly Wise  Checking',['class' => 'btn btn-primary-header add-new-button']) !!}
                        <div class="clearfix"></div>
                </div>
            </nav>
            <div class="outer-w3-agile">
                {{-- {!! Form::open(['url' => 'first_random_checking', 'name' => 'first_random_checking', 'class' =>'request-info clearfix form-horizontal', 'id' => 'first_random_checking', 'method' => 'post','role'=>'','files' => true]) !!} --}}
                {!! Form::open(['url' => 'first_randomisation_checking', 'name' => 'first_randomisation_checking', 'class' =>'request-info clearfix form-horizontal', 'id' => 'first_randomisation_checking', 'method' => 'post','role'=>'','files' => true]) !!}
                {!! Form::hidden('districtcd', session()->get('districtcd_ppds'),['id'=>'districtcd']) !!}
                <div class="panel-group" id="accordion5">                                                              
                    <div class="panel panel-default">
                     <div class="panel-heading1">
                     <h6 class="panel-title">
                         <a data-toggle="#" data-parent="#accordion5" href="#collapseUV"> <span class="fa fa-minus"></span> <span class="highlight">After First Randomisation Assembly Wise  Checking</span> </a>
                         {{-- <a data-toggle="#" data-parent="#accordion5" href="#collapseUV"> <span class="fa fa-minus"></span> <span class="highlight">Zone And Phase Wise General Traning Checking</span> </a> --}}
                     </h6>
                     </div>
                     <div id="collapseUV" class="panel-collapse collapse5">
                      <div class="panel-body">                                   
                        <div class="row">                                   
                           <div class='col-sm-4'>
                               {!! Form::label('zone', 'Zone:', ['class'=>'highlight required']) !!}
                              <div class="form-group">
                                  <div class=''>
                                      {!! Form::select('zone',[''=>'[Select]'],Null,['id'=>'zone','class'=>'form-control','autocomplete'=>'off']) !!}
                                  </div>
                              </div>
                          </div>
                          <div class='col-sm-4'>
                            {!! Form::label('assembly', 'Assembly:', ['class'=>'highlight']) !!}
                           <div class="form-group">
                               <div class=''>
                                   {!! Form::select('assembly',[''=>'[Select]'],Null,['id'=>'assembly','class'=>'form-control','autocomplete'=>'off']) !!}
                               </div>
                           </div>
                       </div>
                          <div class='col-sm-4'>
                            {!! Form::label('phase', 'Phase:', ['class'=>'highlight']) !!}
                           <div class="form-group">
                               <div class=''>
                                    <select id="phase" class="form-control" name="phase">
                                                <option value="">[Select]</option>
                                    </select>
                                </div>
                           </div>
                       </div>
                    </div> 
                     
                       <div class="row"> 
                        <div class='col-sm-12'>
                            <div id='assemblyDetails'></div>
                        </div>
                    </div>
                    <div class="row"> 
                        <div class='col-sm-12'>                    
                        <div class="form-group text-right permit">                            	
{{--                            
                            {{ Form::button('EXCEL', ['class' => 'btn btn-info', 'type' => 'button','id'=>'EXCEL']) }}
                           {{ Form::button('Reset', ['class' => 'btn btn-success' ,'type' => 'reset','id'=>'reset']) }} --}}
                        </div>
                        </div>
                  </div> 
                    {!! Form::close() !!}         
                </div>
            </div>
        </div>                              
    </div> 
                           
                </div>
            </section>
</div>
</div>

<script>
    $(document).ready(function () {
        getZoneDetails();
        $("#zone").change(function(){
            getallassembly();
           
        });
        $("#phase").change(function(){
            
        getphasewisecheking();

           
        });
        $("#assembly").change(function(){
           getphasewisecheking();

           
        });

        $('#EXCEL').click(function () {
            var zone = $("#zone").val();
            var phase = $("#phase").val();
            var assembly = $("#assembly").val();
            // alert(phase);
            if(zone==""){
                $.alert({
                    title: 'Error!!',
                    type: 'red',
                    icon: 'fa fa-exclamation-triangle',
                    content: 'Please Select Zone'
                });
                return false;
            }
             var datas = {'zone': zone,'phase': phase, 'assembly':assembly,'_token': $('input[name="_token"]').val()};
             redirectPost('{{url("checkingAssemblyWiseRPTEXcel")}}', datas);
        });

      
    });
    function getZoneDetails(){
    var forDist = $("#districtcd").val();
    var token = $("input[name='_token']").val();
    $(".se-pre-con").fadeIn("slow");
    $.ajax({
       url: "getZoneDetails",
       method: 'POST',
       data: {forDist: forDist, _token: token},
       success: function (data) {//alert(data.options);
         $(".se-pre-con").fadeOut("slow");
          $("select[name='zone'").html('');
          if(data.status==1)
          {
             $("select[name='zone'").append('<option value>[Select]</option>');
            $.each(data.options,function (k, v)
            {
                $("select[name='zone'").append('<option value='+k+'>'+v+'</option>');
            });
           // getMemberDetails();
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
            $(".se-pre-con").fadeOut("slow");
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-warning',
                  content: msg
              });
         },
   });
 }
 function getallassembly() {
    var forZone = $("#zone").val();
     var token = $("input[name='_token']").val();
     $(".se-pre-con").fadeIn("slow");
     $.ajax({
       url: "getAssemblyDetails",
       method: 'POST',
       data: {forZone: forZone, _token: token},
       success: function (data) {//alert(data.options);
         $(".se-pre-con").fadeOut("slow");
          $("select[name='assembly'").html('');
          if(data.status==1)
          {
              $("select[name='assembly'").html(data.options);
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
            $(".se-pre-con").fadeOut("slow");
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-exclamation-triangle',
                  content: msg
              });
         },
         complete:function(){
            getZonePhasedata();
         }
   });
 }
 function getZonePhasedata(){
  var zone = $("#zone").val();
 
   var token = $("input[name='_token']").val();
 
   $.ajax({
       url: "getZonePhasedata",
       method: 'POST',
       data: {zone: zone, _token: token},
       success: function (data) {
      
          $("select[name='phase'").html('');
          
          if(data.status==1)
          {
            $("select[name='phase'").append('<option value>[Select]</option>');
            $.each(data.options,function (k, v)
            {
                $("select[name='phase'").append('<option value='+k+'>'+v+'</option>');
            });
           
             
          }
       },
       complete:function(){
        getphasewisecheking();
       }
   });
}

function getphasewisecheking() {
    var zone = $("#zone").val();
    var phase = $("#phase").val();
    var assembly = $("#assembly").val();
   
    //alert(subdivison);
    var token = $("input[name='_token']").val();
  //  $(".se-pre-con").fadeIn("slow");
    $.ajax({
        
        method: 'POST',
        url: "getAssemblyWiserecord",
        data: {zone:zone,phase:phase,assembly:assembly,_token:token},
        dataType: 'json',
        success: function (response) {
       // $(".se-pre-con").fadeOut("slow");
       var totalfinalp1=0;
       var totalfinalp2=0;
       var totalfinalp3=0;
       var totalfinalpr=0;

       var totalfinalafterp1=0;
       var totalfinalafterp2=0;
       var totalfinalafterp3=0;
       var totalfinalafterpr=0;
        $("#assemblyDetails").html("");
        var str = "";
        str+= "<table class='table table-bordered table-striped' width='100%' style='border-top: 2px solid #4cae4c;'>";
        str += ' <tr style="background-color: #f5f8fa"><th rowspan="2" style="width: 15%;">&nbsp;Assembly Name </th><th colspan="4" scope="colgroup">&nbsp;PP Requirement Before 1st Randomisation</th><th colspan="4">&nbsp; PP Assigned After 1st Randomisation</th></tr>';
        str += '<tr style="background-color: #f5f8fa"><th>P1</th><th>P2</th><th>P3</th><th>PR</th><th>P1</th><th>P2</th><th>P3</th><th>PR</th></tr>';
       // console.log(response.options);
         $.each( response.options, function( key, value ) {
         
            str += '<tr>';
            str+='<td align="left" style="width: 15%;">&nbsp;'+ response.options[key].assemblycd+'-'+ response.options[key].assemblyname+ '</td>';
         
            $.each( response.options[key].reserve_arr, function( key1, value1 ) {
                str+='<td> '+ value1.sum +'</td>';
                if(value1.poststat=="P1"){
                    totalfinalp1 =totalfinalp1+value1.sum;
                }
                else if(value1.poststat=="P2"){
                    totalfinalp2 =totalfinalp2+value1.sum;
                }
                else if(value1.poststat=="P3"){
                    totalfinalp3 =totalfinalp3+value1.sum;
                }
                else if(value1.poststat=="PR"){
                    totalfinalpr =totalfinalpr+value1.sum;
                }
               
              
                 
                
              
            });
           $.each( response.options[key].current_arr, function( key2, value2 ) {
               str+='<td> '+ value2.total +'</td>';
                 if(value2.poststat=="P1"){
                    totalfinalafterp1 =totalfinalafterp1+value2.total;
                }
                else if(value2.poststat=="P2"){
                    totalfinalafterp2 =totalfinalafterp2+value2.total;
                }
                else if(value2.poststat=="P3"){
                    totalfinalafterp3 =totalfinalafterp3+value2.total;
                }
                else if(value2.poststat=="PR"){
                    totalfinalafterpr =totalfinalafterpr+value2.total;
                }
      
        });
      
            // str+=response.current_arr;
            str+='</tr>';
         });
         str += '<tr><td colspan="1">Total</td><td>'+totalfinalp1+'</td><td>'+totalfinalp2+'</td><td>'+totalfinalp3+'</td><td>'+totalfinalpr+'</td>' ;
         str += '<td>'+totalfinalafterp1+'</td><td>'+totalfinalafterp2+'</td><td>'+totalfinalafterp3+'</td><td>'+totalfinalafterpr+'</td></tr>';
      
        $("#assemblyDetails").html(str);
        }
   
    });

}

</script>
@stop
