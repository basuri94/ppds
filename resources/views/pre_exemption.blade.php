@extends('layouts.master')
@section('content')
<!--// top-bar -->

 <div class="wrapper">
<!-- Sidebar Holder -->
@include('layouts.sidebar')

<!-- Page Content Holder -->
<div id="content">           
<section class="tables-section">
    <nav aria-label="breadcrumb" style="width:100%;">
        <div class="breadcrumb pagehead1">
            <span class="headbrand"><i class="fa fa-desktop"></i> Pre Exemption (Add)
            <span class="scoop-mcaret1"></span>
           </span>         
                <div class="clearfix"></div>
        </div>
    </nav>

    <!-- form -->
<div class="outer-w3-agile">
  {!! Form::open(['url' => 'save_pre_exemption', 'name' => 'save_pre_exemption', 'class' =>'request-info clearfix form-horizontal', 'id' => 'save_pre_exemption', 'method' => 'post','role'=>'','files' => true]) !!}
  {!! Form::hidden('districtcd', session()->get('districtcd_ppds'),['id'=>'districtcd']) !!}
  {!! Form::hidden('searchOfficeCodeValue', '',['id'=>'searchOfficeCodeValue']) !!}
  {!! Form::hidden('searchDesignationValue', '',['id'=>'searchDesignationValue']) !!}
  
  
  
  <div class="panel-group" id="accordion5">                                                              
   <div class="panel panel-default">
    <div class="panel-heading1">
     <h6 class="panel-title">
         <a data-toggle="#" data-parent="#accordion5" href="#collapsePV"> <span class="fa fa-minus"></span> <span class="highlight">Pre Exemption (Before Swapping) </span></a>
     </h6>
    </div>
    <div id="collapsePV" class="panel-collapse collapse5">
      <div class="panel-body">                                                      
        <div class="row"> 
            <div class='col-sm-4'>
              {!! Form::label('office_cd', 'Office Code:', ['class'=>'highlight']) !!}
              <div class="form-group">
                  <div class=''>

<select class="office_cd form-control"  name="office_cd" maxlength="10" minlength="10" id='office_cd' autocomplete="off"></select>
                  </div>
              </div>
              <div style="display: none;">
                      {!! Form::text('newpersonnel_id',null,['id'=>'newpersonnel_id','class'=>'form-control','autocomplete'=>'off','maxlength'=>'11','minlength'=>'11','readonly'=>'false']) !!}
                  </div>
          </div> 
            <div class='col-sm-4'>
              {!! Form::label('designation_search', 'Designation:', ['class'=>'highlight']) !!}
              <div class="form-group">
                  <div class=''>
<!--                      {!! Form::text('designation_search',null,['id'=>'designation_search','class'=>'form-control typeahead','autocomplete'=>'off']) !!}-->
               <select class="designation_search form-control"  name="designation_search" id='designation_search' autocomplete="off"></select>
                  </div>
              </div>
              <div style="display: none;">
                      {!! Form::text('newpersonnel_id',null,['id'=>'newpersonnel_id','class'=>'form-control','autocomplete'=>'off','maxlength'=>'11','minlength'=>'11','readonly'=>'false']) !!}
                  </div>
          </div> 
            
            <div class='col-sm-4'>
               {!! Form::label('age_filter', 'Age:', ['class'=>'highlight']) !!}
              <div class="form-group">
                  <div class=''>
                      {!! Form::select('age_filter',[''=>'[Select]','1'=>'Above 59 Years'],Null,['id'=>'age_filter','class'=>'form-control','autocomplete'=>'off']) !!}
                  </div>
              </div>
          </div>
        </div>   
    
        <div class="row"> 
            <div class='col-sm-12'>
                <div id='assemblyDetails'>
                    
                </div>
            </div>       
        </div> 
           
        <div class="row" id="rw" style="display: none;" > 
           <div class='col-sm-4'>
               {!! Form::label('reason', 'Reason for Pre Exemption:', ['class'=>'highlight required']) !!}
              <div class="form-group">
                  <div class=''>
                      {!! Form::text('reason',null,['id'=>'reason','class'=>'form-control','autocomplete'=>'off','maxlength'=>'30']) !!}
                  </div>
              </div>
          </div> 
           <div class='col-sm-8' > 
               {!! Form::label('', '', ['class'=>'highlight']) !!}
                <div class="form-group text-right permit" > 
                    {{ Form::button('Submit', ['class' => 'btn btn-info', 'type' => 'button','id'=>'Submit']) }}
                    {{ Form::button('Reset', ['class' => 'btn btn-success' ,'type' => 'reset','id'=>'reset']) }}
                </div>
           </div>              
       </div> 
    </div>                              
   </div>
  </div>  
  </div>
 {!! Form::close() !!}             
</div>
<!--// form -->

</section>
</div>
</div>
<!-- Copyright -->
<script type="text/javascript">
$(document).ready(function () {

 $("#reset").click(function () {
  location.reload(true);  
});
$('#office_cd').on('change', function () {
   // get_personnel_details();   
  
});
$('#age_filter').on('change', function () {
    //alert('hi');
     get_personnel_details();
  
});

//$("#loader").hide();
//getZoneDetails(); 
char_num(".office_cd");
var token = $("input[name='_token']").val();
  $('.office_cd').select2({
            placeholder: "Enter office code",
            minimumInputLength: 6,
            maximumInputLength: 10,
             
            ajax: {
                url: 'officeCodeSearch',
                dataType: 'json',
                method: 'POST',
                data: function (params) {
                    return {
                        q: $.trim(params.term),
                         _token: token
                    };
                },
                processResults: function (data) {
                    
                    return {
                        results:  $.map(data.options, function (item) {
                    return {
                        text: item.officecd,
                        id: item.officecd
                    }
                })
                    };

                },
                
                cache: true,
//                error: function (jqXHR, textStatus, errorThrown) {
//            $(".se-pre-con").fadeOut("slow");
//              var msg = "";
//              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
//                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
//              } else {
//                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
//                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
//                  } else {
//                      msg += "Error(s):<strong><ul>";
//                      $.each(jqXHR.responseJSON, function (key, value) {
//                          msg += "<li>" + value + "</li>";
//                      });
//                      msg += "</ul></strong>";
//                  }
//              }
//              
//              $.alert({
//                  title: 'Error!!',
//                  type: 'red',
//                  icon: 'fa fa-exclamation-triangle',
//                  content: msg
//              });
//              $("#Submit").attr('disabled',false);
//           }
           
            }
        }).on("select2:select", function(event) {
  var value = $(event.currentTarget).find("option:selected").val();
         $('#searchOfficeCodeValue').val(value);
           get_personnel_details();
});
$('.designation_search').select2({
            placeholder: "Enter designation",
            minimumInputLength: 2,
            maximumInputLength: 10,
             
            ajax: {
                url: 'designationSearch',
                dataType: 'json',
                method: 'POST',
                data: function (params) {
                    return {
                        q: $.trim(params.term),
                         _token: token
                    };
                },
                processResults: function (data) {
                    
                    return {
                        results:  $.map(data.options, function (item) {
                    return {
                        text: item.off_desg,
                        id: item.off_desg
                    }
                })
                    };

                },
                cache: true
            }
        }).on("select2:select", function(event) {
  var value = $(event.currentTarget).find("option:selected").val();
//alert(value);
         $('#searchDesignationValue').val(value);
           get_personnel_details();
});

//                                    var array_keys = new Array();
//var array_values = new Array();
//
//for (var key in item) {
//    array_keys.push(key);
//    array_values.push(item[key]);
//}
//
//alert(array_keys);
//alert(array_values);

        
      
        

 $('#Submit').click(function(){
        var office_cd = $("#office_cd").val();
       // alert(office_cd);
//            if(office_cd == ""){
//                $.alert({
//                   title: 'Error!!',
//                   type: 'red',
//                   icon: 'fa fa-exclamation-triangle',
//                   content: 'Office Code is required'
//               });
//               return false;
//           }
          var districtcd = $("#districtcd").val();
         if(districtcd == ""){
               $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-exclamation-triangle',
                  content: 'Districtcd is required'
              });
              return false;
          }
          var reason = $("#reason").val();
         if(reason == ""){
               $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-exclamation-triangle',
                  content: 'Reason is required'
              });
              return false;
          }
        var formData = new FormData($("#save_pre_exemption")[0]);

        $(".se-pre-con").fadeIn("slow");
        $.ajax({
            type: "post",
            url: "save_pre_exemption",
            data: formData,
            processData: false,
            contentType: false,
            dataType: "json",
            success: function (data) {
                $(".se-pre-con").fadeOut("slow");

                if (data.status == 1) { 
                    var msg=data.options+" Record(s) exempted successfully";
                    $.confirm({
                       title: 'Success!',
                       type: 'green',
                       icon: 'fa fa-check',
                       content: msg,
                       buttons: {
                           ok: function () { 
                               get_personnel_details();
                               $("#Submit").attr('disabled',false);
                           }
                       }
                   });
                } 
            },
            error: function (jqXHR, textStatus, errorThrown) {
            $(".se-pre-con").fadeOut("slow");
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-exclamation-triangle',
                  content: msg
              });
              $("#Submit").attr('disabled',false);
           }
        });
    });
});

function get_personnel_details(){

        var office_cd=$('#searchOfficeCodeValue').val();
        var searchDesignationValue=$('#searchDesignationValue').val();
        var age_filter=$('#age_filter').val();
   
   var districtcd = $("#districtcd").val();
   $("#rw").hide();
   if (office_cd != "" || searchDesignationValue!="" || age_filter!='') {
        var token = $("input[name='_token']").val();
        $(".se-pre-con").fadeIn("slow");
        $.ajax({
            type: 'POST',
            url: 'getPPDetailsForExempt',
            data: {office_cd:office_cd,searchDesignationValue:searchDesignationValue,age_filter:age_filter, districtcd:districtcd, _token:token},
            dataType: 'json',
            success: function (response) {
            $(".se-pre-con").fadeOut("slow");
            $("#assemblyDetails").html("");
               if(response.status==1)
               {
                    var str='<div style="height:450px;overflow:scroll;">';
                    str+='<input type="hidden" id="row_count" name="row_count" value="'+response.options.length+'"> '
                    str+= "<table class='table table-bordered table-striped' width='100%' style='border-top: 2px solid #4cae4c;'>";
                    str+= ' <tr style="background-color: #f5f8fa"><th align="left">&nbsp;<input type="checkbox" id="myCheckall" name="myCheckall"  value="1"  class="myCheckall">&nbsp;</th><th align="left">&nbsp;Personnel ID </th><th>&nbsp;Name </th><th>&nbsp;Designation </th><th>&nbsp;Basic Pay </th><th>&nbsp;Grade Pay </th></tr>';
            //      str += '<tr style=" background-color: #1ABB9C; color: #FFFFFF; font-weight: bold;"><td align="left"><input type="checkbox" id="myCheckall" name="myCheckall"  value="1"  class="submenu">&nbsp; Check All</td><td align="center" width="30%"><input type="checkbox" id="view_all" class="flat"></td></tr>';
                     var sub_length = response.options.length;
                     for (var i = 0; i < sub_length; i++)
                     {
                            var c = parseInt(i) + 1;                 
                            str += '<tr><td align="left" width="6%">&nbsp;';

                            if(response.options[i].pStatus!='Y'){
                                str+='<input type="checkbox" id="myCheck' + c + '" name="myCheck' + c + '"  value="'+ response.options[i].personcd+'" class="submenu" >';   
                            }else{
                                str+='<span class="fa fa-check" style="color: green;"></span>';   
                            }                         
                            str+='</td><td align="left" width="10%">&nbsp;' + response.options[i].personcd +'</td>';
                            str+='<td align="left" width="32%">&nbsp;' + response.options[i].officer_name +'</td>';
                             str+='<td align="left" width="32%">&nbsp;' + response.options[i].off_desg +'</td>';
                              str+='<td align="left" width="10%">&nbsp;' + response.options[i].basic_pay +'</td>';
                               str+='<td align="left" width="10%">&nbsp;' + response.options[i].grade_pay +'</td>';

        //                    str+='<td align="left" width="10%">';
        //                    if(response.options[i].pStatus==0)
        //                    {
        //                       str+='&nbsp;<span class="fa fa-exclamation-triangle" style="color: red;"></span></td>';
        //                    }
        //                    else{
        //                      str+='&nbsp;<span class="fa fa-check" style="color: green;"></span></td>';  
        //
        //                    }
                           str+='</tr>'; // return empty

                    }
                     str += "</table>";
                     str += "</div>";
                     $("#assemblyDetails").append(str);

                     $("#myCheckall").change(function(){ 
                        var status = this.checked;
                        if(status==true){
                            $("#rw").show();
                            scrollToElement($('#rw'));
                        }else if(status==false){
                            $("#rw").hide();
                        }

                        $('.submenu').each(function(){
                            this.checked = status;     
                        });
                    });
                     $('.submenu').change(function(){
                        if($('.submenu:checked').length=='0'){
                            $("#rw").hide();
                        }else{
                            $("#rw").show();
                        }
                        if(this.checked == false){
                            $("#myCheckall")[0].checked = false; 
                        }
                        if ($('.submenu:checked').length == $('.submenu').length ){ 
                            $("#myCheckall")[0].checked = true;
                        }
                    });
                }else{
                    $.confirm({
                        title: 'Error!!',
                        type: 'red',
                        icon: 'fa fa-exclamation-triangle',
                        content: response.options,
                        buttons: {
                            ok: function () {
                               location.reload(true);
                            }
                        }
                    });
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $(".se-pre-con").fadeOut("slow");
                  var msg = "";
                  if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                      msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                  } else {
                      if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                          msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                      } else {
                          msg += "Error(s):<strong><ul>";
                          $.each(jqXHR.responseJSON, function (key, value) {
                              msg += "<li>" + value + "</li>";
                          });
                          msg += "</ul></strong>";
                      }
                  }
                  $.alert({
                      title: 'Error!!',
                      type: 'red',
                      icon: 'fa fa-exclamation-triangle',
                      content: msg
                  });
             }
        });
    }
}

function scrollToElement(ele) {
    $('html, body').animate({
        scrollTop: ele.offset().top - 60
    }, 1000);
}
</script>
@stop