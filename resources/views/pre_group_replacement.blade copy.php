
@extends('layouts.master')
@section('content')

 <!--// top-bar -->
 <div class="wrapper">
<!-- Sidebar Holder -->
@include('layouts.sidebar')

<!-- Page Content Holder -->
<div id="content">           
<section class="tables-section">
    <nav aria-label="breadcrumb" style="width:100%;">
        <div class="breadcrumb pagehead1">
           
                {!! Form::button('<i class="fa fa-desktop"></i> Pre-group Replacement',['class' => 'btn btn-primary-header add-new-button']) !!}
            
            <div class="col-md-offset-8 pull-right">
<!--                {!! Form::button('Academic Year:&nbsp;',['id'=>'show_academic','class' => 'btn btn-primary-year']) !!}-->
                
            </div>
                <div class="clearfix"></div>
        </div>
    </nav>

    <!-- form -->
<div class="outer-w3-agile">
  {!! Form::open(['url' => 'preGroupReplacement', 'name' => 'preGroupReplacement', 'class' =>'request-info clearfix form-horizontal', 'id' => 'preGroupReplacement', 'method' => 'post','role'=>'','files' => true]) !!}
   {!! Form::hidden('districtcd', session()->get('districtcd_ppds'),['id'=>'districtcd']) !!}
  <!-- OLD PWise -->
  <div class="panel-group" id="accordion5" style="width: 49%;float: left;">                                                              
    <div class="panel panel-default">
     <div class="panel-heading1">
     <h6 class="panel-title">
         <a data-toggle="#" data-parent="#accordion5" href="#collapsePV"> <span class="fa fa-minus"></span><span class="highlight"> OLD PERSONNEL </span></a>
     </h6>
     </div>
     <div id="collapsePV" class="panel-collapse collapse5">
      <div class="panel-body" style="min-height:75px;">                                   
        <div class="row"> 
         <div class='col-sm-6'>
               {!! Form::label('zone', 'Zone:', ['class'=>'highlight required']) !!}
              <div class="form-group">
                  <div class=''>
                      {!! Form::select('zone',[''=>'[Select]'],Null,['id'=>'zone','class'=>'form-control','autocomplete'=>'off']) !!}
                  </div>
              </div>
          </div>
          <div class='col-sm-6'>
            {!! Form::label('phase', 'Phase:', ['class'=>'highlight required']) !!}
           <div class="form-group">
               <div class=''>
                
                    <select id="phase" class="form-control" name="phase">
                                @php
                                    $phase_data=App\Phase::get();
                                @endphp
                                <option value="">[Select]</option>
                                @foreach ($phase_data as $item)
                                <option value="{{ $item->code}}">{{$item->name}}</option>
                                @endforeach
                                
                                

                    </select>
                </div>
           </div>
       </div>

    </div>

    <div class="row"> 
         <div class='col-sm-6'>
              {!! Form::label('personnel_id', 'Personnel ID:', ['class'=>'highlight required']) !!}
              <div class="form-group">
                  <div class=''>
                      {!! Form::text('personnel_id',null,['id'=>'personnel_id','class'=>'form-control','autocomplete'=>'off','maxlength'=>'11','minlength'=>'11']) !!}
                  </div>
              </div>
          </div> 
          <div class='col-sm-6'>
            {!! Form::label('pin_cd', 'Your Wish Personnel Id:', ['class'=>'highlight']) !!}
            <div class="form-group">
                <div class=''>
                    {!! Form::text('pin_cd',null,['id'=>'pin_cd','class'=>'form-control','autocomplete'=>'off','maxlength'=>'11','minlength'=>'11']) !!}
                </div>
            </div>
        </div>     
    </div>
        <div class="row"> 
            <div class='col-sm-12'> 
               <div id="old_personnel_details" >                            	
                   
               </div>
            </div>
         </div>                
       </div>
      </div>
    </div>                              
  </div> 
  
    <!-- New PWise -->
  <div class="panel-group" id="accordion5" style="width: 50%;float: left;margin-left: 3px;">                                                              
    <div class="panel panel-default">
     <div class="panel-heading1">
     <h6 class="panel-title">
         <a data-toggle="#" data-parent="#accordion5" href="#collapseCV"> <span class="fa fa-minus"></span><span class="highlight"> NEW PERSONNEL </span></a>
     </h6>
     </div>
     <div id="collapseCV" class="panel-collapse collapse5">
      <div class="panel-body">                                   
        <div class="row">      
         <div class='col-sm-11'>
              {!! Form::label('newpersonnel_id', 'New Personnel ID:', ['class'=>'highlight']) !!}
              <div class="form-group">
                  <div class=''>
                      {!! Form::text('newpersonnel_id',null,['id'=>'newpersonnel_id','class'=>'form-control','autocomplete'=>'off','maxlength'=>'11','minlength'=>'11','readonly'=>'true']) !!}
                  </div>
              </div>
          </div>     
        </div>
        <div class="row"> 
            <div class='col-sm-12'> 
                <div id="new_personnel_details">                            	
                   
               </div>
            </div>
         </div>  
         </div> 
               
       </div>
      </div>
    </div>                              
 
  <div class="panel-group" id="accordion5" style="width: 100%;float: left;">                                                              
    <div class="panel panel-default">
     <div class="panel-heading1">
     <h6 class="panel-title">
         <a data-toggle="#" data-parent="#accordion5" href="#collapse"> <span class="fa fa-minus"></span><span class="highlight"> Training, Reason </span></a>
     </h6>
     </div>
     <div id="collapse" class="panel-collapse collapse5">
      <div class="panel-body">                                   
        <div class="row"> 
          <div class='col-sm-3'>
               {!! Form::label('reason', 'Reason for Replacement:', ['class'=>'highlight required']) !!}
              <div class="form-group">
                  <div class=''>
                      {!! Form::text('reason',null,['id'=>'reason','class'=>'form-control','autocomplete'=>'off','maxlength'=>'30']) !!}
                  </div>
              </div>
          </div>  
          <div class='col-sm-4'>
              <span style="display:none;" id="venue_sch"><input type="checkbox" id="chkSameVenueTraining" name="chkSameVenueTraining" checked value="true"  />
                {!! Form::label('chkSameVenueTraining', 'Training at Same Venue for New Personnel', ['class'=>'highlight']) !!} </span>
<!--              <div class="form-group">
                  <div class='' style="display:none;" id="difnt_sch">
                       {!! Form::label('training_sch', 'Training Schedule Code:', ['class'=>'highlight required']) !!}
                      {!! Form::select('training_sch',[''=>'[Select]'],Null,['id'=>'training_sch','class'=>'form-control','autocomplete'=>'off']) !!}
                  </div>
              </div>-->
          </div>
                  
           
         </div> 
          <div id="collapsePV" class="panel-collapse collapse5">
      <div class="panel-body" style="min-height:75px;">                                   
        
          <div class="row" id="rw1" style="display: none;"> 
            <div class='col-sm-3'>
              {!! Form::label('trainingtype', 'Training Head:', ['class'=>'highlight']) !!}
              <div class="form-group">
                  <div class=''>
                       {!! Form::select('trainingtype',$training_type['desc'],null,['id'=>'trainingtype','class'=>'form-control','placeholder'=>'[Select]','autocomplete'=>'off']) !!}
                  </div>
              </div>
          </div>
                                    <div class='col-sm-3'>
                                        {!! Form::label('subdivision', 'Sub Division:', ['class'=>'highlight']) !!}
                                          <!--<span class="highlight">Sub Division</span> -->
                                        <div class="form-group">
                                            <div class=''>
                                                {!! Form::select('subdivision',[''=>'[Select]'],null,['id'=>'subdivision','class'=>'form-control','autocomplete'=>'off']) !!}
                                            </div>
                                        </div>
                                    </div>
             <div class='col-sm-3'>
                                        {!! Form::label('v_sub', '1st Training Venue Subvenue:', ['class'=>'highlight']) !!}
                                          <!--<span class="highlight">Sub Division</span> -->
                                        <div class="form-group">
                                            <div class=''>
                                                {!! Form::select('v_sub',[''=>'[Select]'],null,['id'=>'v_sub','class'=>'form-control','autocomplete'=>'off']) !!}
                                            </div>
                                        </div>
                                    </div>
              <div class='col-sm-3'>
                                        {!! Form::label('v_sub2', '2nd Training Venue Subvenue:', ['class'=>'highlight']) !!}
                                          <!--<span class="highlight">Sub Division</span> -->
                                        <div class="form-group">
                                            <div class=''>
                                                {!! Form::select('v_sub2',[''=>'[Select]'],null,['id'=>'v_sub2','class'=>'form-control','autocomplete'=>'off']) !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class='col-sm-3'>
                                        {!! Form::label('orderno', 'Order Date & No:', ['class'=>'highlight required']) !!}
                                       <div class="form-group">
                                           <div class=''>
                                               {!! Form::select('orderno',[''=>'[Select]'],Null,['id'=>'orderno','class'=>'form-control','autocomplete'=>'off']) !!}
                                           </div>
                                       </div>
                                   </div>
             
          
         </div> 
       </div>
      </div>
         <div class="row" id="rw" style="display: none;"> 
          <div class='col-sm-12'>                    
                <div class="form-group text-right permit">                            	
                   {{ Form::button('Search', ['class' => 'btn btn-info', 'type' => 'button','id'=>'search']) }}
                   {{ Form::button('Replace', ['class' => 'btn btn-info', 'type' => 'button','id'=>'replace','disabled'=>'true']) }}
                    {{ Form::button('Print', ['class' => 'btn btn-info', 'type' => 'button','id'=>'print','disabled'=>'true']) }}
                   {{ Form::button('Reset', ['class' => 'btn btn-success' ,'type' => 'reset','id'=>'reset']) }}
                </div>
           </div>
         </div> 
             
       </div>
      </div>
    </div>                              
  </div> 
                      
  

 {!! Form::close() !!}             
</div>
<!--// form -->


</section>

</div>
</div>
<!-- Copyright -->
<script type="text/javascript">
function chkSameVenueTraining_change()
{
//    if(document.getElementById('chkSameVenueTraining').checked==true)
//    {
//        $("#difnt_sch").hide();
//        document.getElementById('training_sch').value='';
//    }
//    else
//    {
//        $("#difnt_sch").show();
//    }
}
$(document).ready(function () {
    
    getZoneDetails(); 
    $("#zone").change(function(){
    $("#personnel_id").val(""); 
    change_subdivision();
 });
   $("#reset").click(function () {
    location.reload(true);
  });
  char_num("#personnel_id");
  //getZoneDetails(); 
  $('#personnel_id').on('change', function () {
    var personnel_id = $("#personnel_id").val();
    var zone = $("#zone").val();
    var phase = $("#phase").val();
   if(personnel_id!="" && zone!="" && phase!=""){
    get_personnel_details(); 
   }
    //get_personnel_details();   
  });
  $('#phase').on('change', function () {
    var personnel_id = $("#personnel_id").val();
    var zone = $("#zone").val();
    var phase = $("#phase").val();
   if(personnel_id!="" && zone!="" && phase!=""){
    get_personnel_details(); 
   }
      
  });
  $("#trainingtype").change(function(){
     $("select[name='v_sub'").html('<option value>[Select]</option>');
             $("select[name='v_sub2'").html('<option value>[Select]</option>');
             $("#subdivision").val('');
  });
  $("#subdivision").change(function(){
      get_venue_and_subvenue();
  });
  $("#search").click(function(){
        document.getElementById('replace').disabled=true;
        
         var personnel_id = $("#personnel_id").val();
         var zone = $("#zone").val();
         var phase = $("#phase").val();
         var pin_cd = $("#pin_cd").val();
         if(zone == ""){
                $.alert({
                   title: 'Error!!',
                   type: 'red',
                   icon: 'fa fa-exclamation-triangle',
                   content: 'Zone is required'
               });
               return false;
           }
            if(personnel_id == ""){
                $.alert({
                   title: 'Error!!',
                   type: 'red',
                   icon: 'fa fa-exclamation-triangle',
                   content: 'OLD Personnel ID is required'
               });
               return false;
           }
           if(phase == ""){
                $.alert({
                   title: 'Error!!',
                   type: 'red',
                   icon: 'fa fa-exclamation-triangle',
                   content: 'Phase is required'
               });
               return false;
           }
//        var forassembly = $("#forassembly").val();
//      alert(forassembly);
//	var forzone=document.getElementById('hid_for_zone').innerHTML;
//	//var for_subdiv=document.getElementById('hid_for_subdiv').innerHTML;
//	//var forassembly=document.getElementById('hid_forassembly').innerHTML;
//	var ofc_id=document.getElementById('hid_ofc_cd').innerHTML;
//	var booked=document.getElementById('hid_booked').innerHTML;
//	var gender=document.getElementById('hid_gender').innerHTML;
//	var post_stat=document.getElementById('hid_post_stat').innerHTML;  
        //var token = $("input[name='_token']").val();
        var formData = new FormData($("#preGroupReplacement")[0]);
        $(".se-pre-con").fadeIn("slow");
        //var formData = {'zone': forzone,'forassembly': forassembly,'ofc_id': ofc_id,'booked': booked, 'gender': gender,'post_stat': post_stat, '_token': token};
        $.ajax({
            type: "post",
            url: "searchNewPPData",
            data: formData,
            processData: false,
            contentType: false,
            dataType: 'json',
            success: function (data) {
            $(".se-pre-con").fadeOut("slow"); 
            if(data.status==1)
            {
                $('#new_personnel_details').html(data.options);
                $('#newpersonnel_id').val(data.perid);
                document.getElementById('replace').disabled=false;
		$("#venue_sch").show();      
                $("select[name='training_sch'").html("");
                $("select[name='training_sch'").append('<option value>[Select]</option>');
                $.each(data.trschedule,function (k, v)
                {
                    $("select[name='training_sch'").append('<option value='+v['schedule_code']+'>'+v['vs_name']+'</option>');
                });
            }
            else if(data.status==0)
            {
                $('#new_personnel_details').html(data.options);
                $('#newpersonnel_id').val(data.perid);
                //$('#rw').hide();
                document.getElementById('replace').disabled=true;	
	        $("#venue_sch").hide();
//		$("#difnt_sch").hide();
            }
            document.getElementById('print').disabled=true;
       },
        error: function (jqXHR, textStatus, errorThrown) {
            $(".se-pre-con").fadeOut("slow");
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-exclamation-triangle',
                  content: msg
              });
         }
     });
  });
  $("#chkSameVenueTraining").change(function(){
  var chkSameVenueTraining = $("#chkSameVenueTraining").is(":checked");
  if(chkSameVenueTraining==true){
    $("#rw1").hide(); 
  }else{
     $("#rw1").show();  
  }
  });
  $("#replace").click(function(){
        
        var chkSameVenueTraining = $("#chkSameVenueTraining").is(":checked");
       
        var personnel_id = $("#personnel_id").val();
            if(personnel_id == ""){
                $.alert({
                   title: 'Error!!',
                   type: 'red',
                   icon: 'fa fa-exclamation-triangle',
                   content: 'OLD Personnel ID is required'
               });
               return false;
           }
           var zone = $("#zone").val();
         if(zone == ""){
                $.alert({
                   title: 'Error!!',
                   type: 'red',
                   icon: 'fa fa-exclamation-triangle',
                   content: 'Zone is required'
               });
               return false;
           }
           if(chkSameVenueTraining==false){
                var newpersonnel_id = $("#newpersonnel_id").val();
            if(newpersonnel_id == ""){
                $.alert({
                   title: 'Error!!',
                   type: 'red',
                   icon: 'fa fa-exclamation-triangle',
                   content: 'NEW Personnel ID is required'
               });
               return false;
           }
           var training_sch = $("#training_sch").val();
            if(training_sch ==""){
                $.alert({
                   title: 'Error!!',
                   type: 'red',
                   icon: 'fa fa-exclamation-triangle',
                   content: 'Training is required'
               });
               return false;
           }
          var subdivision = $("#subdivision").val();
           if(subdivision == ""){
                $.alert({
                   title: 'Error!!',
                   type: 'red',
                   icon: 'fa fa-exclamation-triangle',
                   content: 'Subdivision is required'
               });
               return false;
           }
        //    var v_sub2 = $("#v_sub2").val();
        //     if(v_sub2 == ""){
        //         $.alert({
        //            title: 'Error!!',
        //            type: 'red',
        //            icon: 'fa fa-exclamation-triangle',
        //            content: '2nd Training  is required'
        //        });
        //        return false;
        //    }
           var v_sub = $("#v_sub").val();
            if(v_sub == ""){
                $.alert({
                   title: 'Error!!',
                   type: 'red',
                   icon: 'fa fa-exclamation-triangle',
                   content: '1st Training Venue is required'
               });
               return false;
           }
           var orderno = $("#orderno").val();
            if(orderno == ""){
                $.alert({
                   title: 'Error!!',
                   type: 'red',
                   icon: 'fa fa-exclamation-triangle',
                   content: 'Please select order date & no'
               });
               return false;
           }
           }
       
        var reason = $("#reason").val();
            if(reason == ""){
                $.alert({
                   title: 'Error!!',
                   type: 'red',
                   icon: 'fa fa-exclamation-triangle',
                   content: 'Reason is required'
               });
               return false;
           }
        var formData = new FormData($("#preGroupReplacement")[0]);
        $(".se-pre-con").fadeIn("slow");
        //var formData = {'zone': forzone,'forassembly': forassembly,'ofc_id': ofc_id,'booked': booked, 'gender': gender,'post_stat': post_stat, '_token': token};
        $.ajax({
            type: "post",
            url: "preReplacePPData",
            data: formData,
            processData: false,
            contentType: false,
            dataType: 'json',
            success: function (data) {
            $(".se-pre-con").fadeOut("slow"); 
            if(data.status==1)
            {
               // $('#new_personnel_details').html(data.options);
              //  $('#newpersonnel_id').val(data.perid);
		document.getElementById('replace').disabled=true;
                document.getElementById('search').disabled=true;
                //document.getElementById('p_id').disabled=true;
                document.getElementById('print').disabled=false;
                $.confirm({
                        title: 'Success!',
                        type: 'green',
                        icon: 'fa fa-check',
                        content: data.options,
                        buttons: {
                            ok: function () {
                                /*$("#swapping_personnel")[0].reset();
                                $("#swapping").attr('disabled',true);
                                $('.form-control-feedback').css('display', 'none');
                                $('.has-success').removeClass('has-success');*/
                                //location.reload(true);
                                print_appt();
                            }

                        }
                    });
            }
            else if(data.status==0)
            {
               // $('#new_personnel_details').html(data.options);
               // $('#newpersonnel_id').val(data.perid);
                //$('#rw').hide();   
                document.getElementById('search').disabled=false;
                document.getElementById('replace').disabled=false;	
	        //$("#venue_sch").hide();
		//$("#difnt_sch").hide();
                document.getElementById('print').disabled=true;
                 $.confirm({
                        title: 'Error!!',
                        type: 'red',
                        icon: 'fa fa-exclamation-triangle',
                        content: data.options,
                        buttons: {
                            ok: function () {
                                /*$("#swapping_personnel")[0].reset();
                                $("#swapping").attr('disabled',true);
                                $('.form-control-feedback').css('display', 'none');
                                $('.has-success').removeClass('has-success');*/
                               // location.reload(true);
                            }

                        }
                 });
            }else if(data.status==2){
                document.getElementById('replace').disabled=true;
                document.getElementById('search').disabled=true;
                //document.getElementById('p_id').disabled=true;
                document.getElementById('print').disabled=false;
                $.confirm({
                        title: 'Success!',
                        type: 'green',
                        icon: 'fa fa-check',
                        content: data.options,
                        buttons: {
                            ok: function () {
                                /*$("#swapping_personnel")[0].reset();
                                $("#swapping").attr('disabled',true);
                                $('.form-control-feedback').css('display', 'none');
                                $('.has-success').removeClass('has-success');*/
                              //  location.reload(true);
                              print_appt();
                            }

                        }
                    });
            }
            
       },
        error: function (jqXHR, textStatus, errorThrown) {
            $(".se-pre-con").fadeOut("slow");
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-exclamation-triangle',
                  content: msg
              });
              document.getElementById('replace').disabled=false;
         }
     });
  });
  $('#print').click(function () { 
     print_appt();

 });
 
});
function print_appt(){
 var districtcd = $("#districtcd").val();
     var subdivision = "";
     var blockMunicipality = "";
     var officeName = "";
     var PersonnelId = $("#newpersonnel_id").val();
     var trainingtype = "";
     var from = "1";
     var to = "1";
     var datas = {'districtcd': districtcd,'subdivision': subdivision,'blockMunicipality': blockMunicipality,'officeName': officeName,'PersonnelId': PersonnelId,'trainingtype': trainingtype,from: from,to: to, '_token': $('input[name="_token"]').val()};
     redirectPost_newTab('{{url("getFirstAppointmentLetterPDF2019")}}', datas);
}
 function get_personnel_details(){
    var personnel_id = $("#personnel_id").val();
    var zone = $("#zone").val();
    var phase = $("#phase").val();
    if(zone == ""){
           $.alert({
              title: 'Error!!',
              type: 'red',
              icon: 'fa fa-exclamation-triangle',
              content: 'Zone is required'
          });
          return false;
      }
      if(phase == ""){
           $.alert({
              title: 'Error!!',
              type: 'red',
              icon: 'fa fa-exclamation-triangle',
              content: 'Phase is required'
          });
          return false;
      }
      
    var token = $("input[name='_token']").val();
    $(".se-pre-con").fadeIn("slow");
    $.ajax({
       url: "getOldPersonnelDetails",
       method: 'POST',
       data: {personnel_id: personnel_id, zone:zone,phase:phase, _token: token},
       success: function (json) {//alert(data.options);
          $(".se-pre-con").fadeOut("slow");
          //$("#memberDetails").html('');
          if(json.status==1)
          {
              $('#old_personnel_details').html(json.options);
               $('#rw').show();
               scrollToElement($('#rw'));
          }
          else if(json.status==0)
          {
              $('#old_personnel_details').html(json.options);
              $('#rw').hide();
          }
          document.getElementById('replace').disabled=true;
          document.getElementById('print').disabled=true;
       },
        error: function (jqXHR, textStatus, errorThrown) {
         $(".se-pre-con").fadeOut("slow");
           var msg = "";
           if (jqXHR.status !== 422 && jqXHR.status !== 400) {
               msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
           } else {
               if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                   msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
               } else {
                   msg += "Error(s):<strong><ul>";
                   $.each(jqXHR.responseJSON, function (key, value) {
                       msg += "<li>" + value + "</li>";
                   });
                   msg += "</ul></strong>";
               }
           }
           $.alert({
               title: 'Error!!',
               type: 'red',
               icon: 'fa fa-warning',
               content: msg
           });
        }
   });
 }
 function getZoneDetails(){
    var forDist = $("#districtcd").val();
    var token = $("input[name='_token']").val();
    $(".se-pre-con").fadeIn("slow");
    $.ajax({
       url: "getZoneDetails",
       method: 'POST',
       data: {forDist: forDist, _token: token},
       success: function (data) {//alert(data.options);
         $(".se-pre-con").fadeOut("slow");
          $("select[name='zone'").html('');
          if(data.status==1)
          {
             $("select[name='zone'").append('<option value>[Select]</option>');
            $.each(data.options,function (k, v)
            {
                $("select[name='zone'").append('<option value='+k+'>'+v+'</option>');
            });
           // getMemberDetails();
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
            $(".se-pre-con").fadeOut("slow");
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-warning',
                  content: msg
              });
         },
         complete:function(){
            getOrderDetails();
         }
   });
 }
 function scrollToElement(ele) {
    $('html, body').animate({
        scrollTop: ele.offset().top - 60
    }, 1000);
}
function change_subdivision(){
  var forDist = $("#districtcd").val();
  var zone = $("#zone").val();
   var token = $("input[name='_token']").val();
   $(".se-pre-con").fadeIn("slow");
   $.ajax({
       url: "getZonewiseSubBlockDataQuery",
       method: 'POST',
       data: {forDist: forDist, zone: zone, _token: token},
       success: function (data) {//alert(data.options);
          $(".se-pre-con").fadeOut("slow");
          $("select[name='subdivision'").html('');
          
          if(data.status==1)
          {
            $("select[name='subdivision'").append('<option value>[Select]</option>');
            $.each(data.options,function (k, v)
            {
                $("select[name='subdivision'").append('<option value='+k+'>'+v+'</option>');
            });
            //$("select[name='subdivision'").html(data.options);
             // change_officeName();
           //  getMemberDetails();
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
         $(".se-pre-con").fadeOut("slow");
           var msg = "";
           if (jqXHR.status !== 422 && jqXHR.status !== 400) {
               msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
           } else {
               if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                   msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
               } else {
                   msg += "Error(s):<strong><ul>";
                   $.each(jqXHR.responseJSON, function (key, value) {
                       msg += "<li>" + value + "</li>";
                   });
                   msg += "</ul></strong>";
               }
           }
           $.alert({
               title: 'Error!!',
               type: 'red',
               icon: 'fa fa-warning',
               content: msg
           });
         }
   });
}
function get_venue_and_subvenue(){
var subdivision = $("#subdivision").val();
var hid_post_stat=$("#hid_post_stat").html();
// alert(hid_post_stat);
var trainingtype=$("#trainingtype").val();
// if(subdivision == ""){
//            $.alert({
//               title: 'Error!!',
//               type: 'red',
//               icon: 'fa fa-exclamation-triangle',
//               content: 'Subdivision is required'
//           });
//           return false;
//       }
//       if(trainingtype == ""){
//            $.alert({
//               title: 'Error!!',
//               type: 'red',
//               icon: 'fa fa-exclamation-triangle',
//               content: 'Training Type is required'
//           });
//           return false;
//       }
    var token = $("input[name='_token']").val();
    $(".se-pre-con").fadeIn("slow");
    $.ajax({
       url: "get_venue_with_not_fill_for_replace",
       method: 'POST',
       data: {subdivision:subdivision,trainingtype:trainingtype,post_stat:hid_post_stat, _token: token},
       success: function (data) {//alert(data.options);
         $(".se-pre-con").fadeOut("slow");
          $("select[name='v_sub'").html('');
          $("select[name='v_sub2'").html('');
          if(data.status==1)
          {
             $("select[name='v_sub'").append('<option value>[Select]</option>');
             $("select[name='v_sub2'").append('<option value>[Select]</option>');
            $.each(data.options,function (k, v)
            { 
                if(v.training_type=='01' || v.training_type=='03' || v.training_type=='05'){
                 $("select[name='v_sub'").append('<option value='+v.schedule_code+'>'+v.venuename+'('+v.subvenue+'['+v.training_dt+' '+v.training_time+'])</option>');   
                }else{
                 $("select[name='v_sub2'").append('<option value='+v.schedule_code+'>'+v.venuename+'('+v.subvenue+'['+v.training_dt+' '+v.training_time+'])</option>');   
                }
                
            });
           // getMemberDetails();
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
            $(".se-pre-con").fadeOut("slow");
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-warning',
                  content: msg
              });
         }
   });
}
function getOrderDetails(){
    var forDist = $("#districtcd").val();
    var token = $("input[name='_token']").val();
    //$(".se-pre-con").fadeIn("slow");
    $.ajax({
       url: "getOrderDetails",
       method: 'POST',
       data: {forDist: forDist, _token: token},
       success: function (data) {//alert(data.options);
        // $(".se-pre-con").fadeOut("slow");
          $("select[name='orderno'").html('');
          if(data.status==1)
          {
              $("select[name='orderno'").html(data.options);
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
            $(".se-pre-con").fadeOut("slow");
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-warning',
                  content: msg
              });
         }
   });
 }
</script>
@stop
