@extends('layouts.master')
@section('content')
<style>
.p_small_box{
 width: 40px;
 padding: 2px;
  font-size: .9rem;
  line-height: 1.5;
  color: #495057;
  background-color: #fff;
  background-clip: padding-box;
  border: 1px solid #ccc;
  border-radius: 0.15rem;
  transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
  height: calc(1.40rem + 1px);
}
.p_big_box{
 width: 55px;
 padding: 2px;
 font-size: .95rem;
  line-height: 1.5;
  color: #495057;
  background-color: #fff;
  background-clip: padding-box;
  border: 1px solid #ccc;
  border-radius: 0.15rem;
  transition: border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
  height: calc(1.40rem + 1px);
}
</style>
<!--// top-bar -->
 <div class="wrapper">
<!-- Sidebar Holder -->
@include('layouts.sidebar')

<!-- Page Content Holder -->
<div id="content">           
<section class="tables-section">
    <nav aria-label="breadcrumb" style="width:100%;">
        <div class="breadcrumb pagehead1">
            <span class="headbrand"><i class="fa fa-desktop"></i> Add/Edit Group Training Allocation
                <span class="scoop-mcaret1"></span>
            </span>&nbsp;&nbsp;
            &nbsp;{!! Form::button('List of Group Training Allocation&nbsp;<i class="fa fa-arrow-circle-right"></i>',['id'=>'second_training_venue_allocation_list','class' => 'btn btn-primary-year add-new-button']) !!}  
            
           <div class="clearfix"></div>
        </div>
    </nav>

    <!-- form -->
<div class="outer-w3-agile">
  {!! Form::open(['url' => 'secondTrAllocation', 'name' => 'secondTrAllocation', 'class' =>'request-info clearfix form-horizontal', 'id' => 'secondTrAllocation', 'method' => 'post','role'=>'','files' => true]) !!}
  {!! Form::hidden('districtcd', session()->get('districtcd_ppds'),['id'=>'districtcd']) !!}
  
  <div class="panel-group" id="accordion5">                                                              
   <div class="panel panel-default">
    <div class="panel-heading1">
     <h6 class="panel-title">
         <a data-toggle="#" data-parent="#accordion5" href="#collapsePV"> <span class="fa fa-minus"></span><span class="highlight"> Add/Edit Group Training Allocation</span></a>
     </h6>
    </div>
    <div id="collapsePV" class="panel-collapse collapse5">
      <div class="panel-body">                                   
       <div class="row"> 
       {{-- <div class='col-sm-3'>
               {!! Form::label('zone', 'Zone:', ['class'=>'highlight required']) !!}
              <div class="form-group">
                  <div class=''>
                      {!! Form::select('zone',[''=>'[Select]'],Null,['id'=>'zone','class'=>'form-control','autocomplete'=>'off']) !!}
                  </div>
              </div>
          </div>   --}}
          <div class='col-sm-3'>
            {!! Form::label('subdivision', 'Sub Division:', ['class'=>'highlight required']) !!}
           <!--<span class="highlight">Sub Division</span> -->
           <div class="form-group">
               <div class=''>
                   {!! Form::select('subdivision',[''=>'[Select]'],null,['id'=>'subdivision','class'=>'form-control','autocomplete'=>'off']) !!}
               </div>
           </div>
       </div>
          <div class='col-sm-3'>
            {!! Form::label('phase', 'Phase:', ['class'=>'highlight required']) !!}
           <div class="form-group">
               <div class=''>
                    <select id="phase" class="form-control" name="phase">
                        <option value="">[Select]</option>
                    </select>
                </div>
           </div>
        </div> 
        
           <div class='col-sm-3'>
              {!! Form::label('venuename', 'Venue Name:', ['class'=>'highlight required']) !!}
              <div class="form-group">
                  <div class=''>
                       {!! Form::select('venuename',[''=>'[Select]'],null,['id'=>'venuename','class'=>'form-control','autocomplete'=>'off']) !!}
                  </div>
              </div>
          </div>                    
           <div class='col-sm-3'>
               {!! Form::label('trainingdatetime', 'Training Date & Time:', ['class'=>'highlight required']) !!}
              <div class="form-group">
                  <div class=''>
                     {!! Form::select('trainingdatetime',[''=>'[Select]'],null,['id'=>'trainingdatetime','class'=>'form-control','placeholder'=>'[Select]','autocomplete'=>'off']) !!}
                  </div>
             </div>
          </div>
          <div class='col-sm-3'>
              {!! Form::label('assembly', 'Assembly:', ['class'=>'highlight required']) !!}
              <div class="form-group">
                  <div class=''>
                      {!! Form::select('assembly',[''=>'[Select]'],null,['id'=>'assembly','class'=>'form-control','autocomplete'=>'off']) !!}
                  </div>
              </div>
          </div> 
      </div>
          
          
      <div class="row"> 
            <div class='col-sm-12'>
                <div class="form-group">
                     <div id='subVenueDetails'></div>
                  </div>
            </div>
         
      </div> 
      <div class="row">                  
         <div class='col-sm-12'>
            <div class="form-group text-right permit">                            	
               {{ Form::button('Submit', ['class' => 'btn btn-info', 'type' => 'submit','id'=>'Submit']) }}
               {{ Form::button('Reset', ['class' => 'btn btn-success' ,'type' => 'reset','id'=>'reset']) }}
            </div>
          </div>
      </div>     
       
    </div>                              
   </div>
  </div>  
  </div>
 {!! Form::close() !!}             
</div>
<!--// form -->

</section>

</div>
</div>
<!-- Copyright -->
<script type="text/javascript">
$(document).ready(function () {
$("#second_training_venue_allocation_list").click(function () {
    window.location.href = "second_training_venue_allocation_list";
});
//getZoneDetails();
change_subdivision(); 
// $('select[name="zone"]').on('change', function () {
//     change_subdivision(); 

//  });
 $('select[name="phase"]').on('change', function () {
    getAssemblyDetails(); 

 });
$("#reset").click(function () {
  location.reload(true);  
});
 $('select[name="subdivision"]').on('change', function () {    
    change_venueName();
    getSubdivisionPhasedata(); 
 });
  $('select[name="trainingdatetime"]').on('change', function () {   
    get_SubvenueName(); 
 });
 $('select[name="venuename"]').on('change', function () {
    get_SubvenueName();    
 });
  $('select[name="assembly"]').on('change', function () {     
    get_SubvenueName();    
 });

$('#secondTrAllocation').bootstrapValidator({

        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            zone: {
                validators: {
                    notEmpty: {
                        message: 'Zone is required'
                    }
                }
            },
            subdivision: {
                validators: {
                    notEmpty: {
                        message: 'Subdivision is required'
                    }
                }
            },
            phase: {
                validators: {
                    notEmpty: {
                        message: 'Phase is required'
                    }
                }
            },
            venuename: {
                validators: {
                    notEmpty: {
                        message: 'Venue Name is required'
                    }
                }
            },
            assembly: {
                validators: {
                    notEmpty: {
                        message: 'Assembly is required'
                    }
                }
            },
            trainingdatetime: {
                validators: {
                    notEmpty: {
                        message: 'Training Date & Time is required'
                    }
                }
            }
            
        }

    }).on('success.form.bv', function (e) {
        e.preventDefault();
        var action = $(this).attr('action');
        var type = $(this).attr('method');
        var formData = new FormData($(this)[0]);
       $(".se-pre-con").fadeIn("slow");
        $.ajax({
            type: type,
            url: action,
            data: formData,
            processData: false,
            contentType: false,
            dataType: "json",
            success: function (data) {
                $(".se-pre-con").fadeOut("slow");

                if (data.status == 1) {        
                     var msg=data.options+"Record(s) saved successfully";
                     $.confirm({
                        title: 'Success!',
                        type: 'green',
                        icon: 'fa fa-check',
                        content: msg,
                        buttons: {
                            ok: function () {     
                               // location.reload(true);
                            }

                        }
                    });

                } else if (data.status == 0){
                   $.alert({
                        title: 'Error!!',
                        type: 'red',
                        icon: 'fa fa-exclamation-triangle',
                        content: 'Assign value does not match'
                    });
                    $("#Submit").attr('disabled',false);  
                }else{
                    //var msg="Record(s) updated successfully";
                    var msg="Venue can not be same in same Training Date & Time";
                     $.confirm({
                        title: 'Error!!',
                        type: 'red',
                        icon: 'fa fa-exclamation-triangle',
                        content: msg,
                        buttons: {
                            ok: function () {     
                                location.reload(true);
                            }

                        }
                    });

                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
            $(".se-pre-con").fadeOut("slow");
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-exclamation-triangle',
                  content: msg
              });
              $("#Submit").attr('disabled',false);
           }
        });
    });
});

function get_SubvenueName(){
    var venuename = $("#venuename").val();
    var trainingdatetime = $("#trainingdatetime").val();
    if(venuename=="" || trainingdatetime==""){
        return false;
    }
    var token = $("input[name='_token']").val();
    $(".se-pre-con").fadeIn("slow");
    $.ajax({
        type: 'POST',
        url: 'get2ndSubVenue',
        data: {venuename:venuename,trainingdatetime:trainingdatetime, _token:token},
        dataType: 'json',
        success: function (data) {
        $(".se-pre-con").fadeOut("slow");
        $("#subVenueDetails").html("");
        if(data.status==1)
        {
        if(data.options.length==1){
       

        var str='<input type="hidden" id="row_count" name="row_count" value="'+data.options[0].sub_venue_detail_brief.length+'"> ';
        str+= "<table class='table table-bordered table-striped' width='100%' style='border-top: 2px solid #4cae4c;'>";
        str += ' <tr style="background-color: #f5f8fa"><th align="left" width="22%">Sub venue</th><th align="left" width="6%">&nbsp;P/R</th><th align="left" width="6%">&nbsp;From &nbsp;Group</th><th align="left" width="6%">&nbsp;To &nbsp;Group</th><th align="left" width="6%">&nbsp;PR</th><th align="left" width="6%">&nbsp;P1</th><th align="left" width="6%">&nbsp;P2</th><th align="left" width="6%">&nbsp;P3</th><th align="left" width="6%">&nbsp;PA</th><th align="left" width="6%">&nbsp;PB</th><th align="left" width="8%">&nbsp;Max CP</th><th align="left" width="8%">&nbsp;Assigned</th><th align="left" width="8%">&nbsp;Left</th></tr>';
        str += ' <tr style="background-color: #FDF5E6"><th align="left">&nbsp;Total</th><th align="left">&nbsp;</th><th align="left">&nbsp;</th><th align="left">&nbsp;</th><th align="left">&nbsp;</th><th align="left">&nbsp;</th><th align="left">&nbsp;</th><th align="left">&nbsp;</th><th align="left">&nbsp;</th><th align="left">&nbsp;</th><th align="left">&nbsp;<input type="text" id="maxcp" class="p_big_box" readonly="true" value="'+data.options[0].maximumcapacity+'"/></th><th align="left">&nbsp;<input type="text" id="assign" name="assign" class="p_big_box" readonly="true"/></th><th align="left">&nbsp;<input type="text" id="left" class="p_big_box" readonly="true"  value="'+data.options[0].maximumcapacity+'"/></th></tr>';
        for(var i=0;i<data.options[0].sub_venue_detail_brief.length;i++){ 
            var c = parseInt(i) + 1;
            var fetch_pa_val="";
            var fetch_pb_val="";
            if(data.options[0].sub_venue_detail_brief[i].pa==null || data.options[0].sub_venue_detail_brief[i].pa=="" ){
              fetch_pa_val="";
            }else{
              fetch_pa_val=  data.options[0].sub_venue_detail_brief[i].pa;
            }
             if(data.options[0].sub_venue_detail_brief[i].pb==null || data.options[0].sub_venue_detail_brief[i].pb==""){
               fetch_pb_val="";
            }else{
               fetch_pb_val=  data.options[0].sub_venue_detail_brief[i].pb;
            }
             str+='<tr>';
             str+='<input type="hidden" id="subVenue' + c + '" name="subVenue' + c + '"  value="'+ data.options[0].sub_venue_detail_brief[i].subvenue_cd+'" >';   
               var asm_code="";
          if(data.options[0].sub_venue_detail_brief[i].assemblycd!="")
          {
              asm_code=' [Assembly code - <span style="color: red;">'+data.options[0].sub_venue_detail_brief[i].assemblycd+'</span>]';
          }
    str+='<td align="left">&nbsp;'+data.options[0].sub_venue_detail_brief[i].subvenue+asm_code+' </td>';
             str+='<input type="hidden" id="hid_PartyRes' + c + '" name="hid_PartyRes' + c + '" value="'+data.options[0].sub_venue_detail_brief[i].party_reserve+'"/>';
             str+='<input type="hidden" id="assembly_code_done' + c + '" name="assembly_code_done' + c + '" value="'+data.options[0].sub_venue_detail_brief[i].assemblycd+'"/>';
    str+='<td align="left">&nbsp;<select id="s_party_re' + c + '" name="s_party_re' + c + '" class="p_small_box" onChange="SubFromToPR('+ c + ');"><option value="P">P</option><option value="R">R</option</select></td>';
             str+='<td align="left">&nbsp;<input type="text" id="f_gr' + c + '" name="f_gr' + c + '" class="p_small_box" value="'+ data.options[0].sub_venue_detail_brief[i].group_from+'" maxlength="4" onChange="SubFromToPR('+ c + ');"/></td>';
             str+='<td align="left">&nbsp;<input type="text" id="e_gr' + c + '" name="e_gr' + c + '" class="p_small_box" value="'+ data.options[0].sub_venue_detail_brief[i].group_to+'" maxlength="4" onChange="SubFromToPR('+ c + ');"/></td>';
             str+='<td align="left">&nbsp;<input type="text" id="s_pr' + c + '" name="s_pr' + c + '" class="p_small_box" value="'+ data.options[0].sub_venue_detail_brief[i].pr+'" maxlength="4" readOnly="true"/></td>';
             str+='<td align="left">&nbsp;<input type="text" id="s_p1' + c + '" name="s_p1' + c + '" class="p_small_box" value="'+ data.options[0].sub_venue_detail_brief[i].p1+'" maxlength="4" readOnly="true"/></td>';
             str+='<td align="left">&nbsp;<input type="text" id="s_p2' + c + '" name="s_p2' + c + '" class="p_small_box" value="'+ data.options[0].sub_venue_detail_brief[i].p2+'" maxlength="4" readOnly="true"/></td>';
             str+='<td align="left">&nbsp;<input type="text" id="s_p3' + c + '" name="s_p3' + c + '" class="p_small_box" value="'+ data.options[0].sub_venue_detail_brief[i].p3+'" maxlength="4" readOnly="true"/></td>';
             str+='<td align="left">&nbsp;<input type="text" id="s_pa' + c + '" name="s_pa' + c + '" class="p_small_box" value="'+ fetch_pa_val+'" maxlength="4" readOnly="true"/></td>';
             str+='<td align="left">&nbsp;<input type="text" id="s_pb' + c + '" name="s_pb' + c + '" class="p_small_box" value="'+ fetch_pb_val+'" maxlength="4" readOnly="true"/></td>';
             
             str+='<td align="left">&nbsp;<input type="text" id="s_maxcp' + c + '" name="s_maxcp' + c + '" class="p_big_box" value="'+data.options[0].sub_venue_detail_brief[i].maxcapacity+'" readOnly="true"/></td>';
             str+='<td align="left">&nbsp;<input type="text" id="s_assign' + c + '" name="s_assign' + c + '" class="p_big_box" value="" readOnly="true"/></td>';
             str+='<td align="left">&nbsp;<input type="text" id="s_left' + c + '" name="s_left' + c + '" class="p_big_box" value="'+data.options[0].sub_venue_detail_brief[i].maxcapacity+'" readOnly="true"/></td>';
             str+='</tr>';
             
                     
        }
        str += "</table>";
         }else{
                var str="<span style='color: red;'>Sub Venue is required</span>";
          }
        }
        else{
            var str="<span style='color: red;'>Sub Venue is required</span>";
        }
        $("#subVenueDetails").append(str);
        
        PP_Recordmanage();
      
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $(".se-pre-con").fadeOut("slow");
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-exclamation-triangle',
                  content: msg
              });
         }
    });
}

function change_subdivision(){
  var forDist = $("#districtcd").val();
   var token = $("input[name='_token']").val();
  // $(".se-pre-con").fadeIn("slow");
   $.ajax({
       url: "getSubdivisionData",
       method: 'POST',
       data: {forDist: forDist, _token: token},
       success: function (data) {//alert(data.options);
       //   $(".se-pre-con").fadeOut("slow");
          $("select[name='subdivision'").html('');
          
          if(data.status==1)
          {
            $("select[name='subdivision'").append('<option value>[Select]</option>');
            $.each(data.options,function (k, v)
            {
                $("select[name='subdivision'").append('<option value='+k+'>'+v+'</option>');
            });
            //$("select[name='subdivision'").html(data.options);
             // change_officeName();
            // getMemberDetails();
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
         $(".se-pre-con").fadeOut("slow");
           var msg = "";
           if (jqXHR.status !== 422 && jqXHR.status !== 400) {
               msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
           } else {
               if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                   msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
               } else {
                   msg += "Error(s):<strong><ul>";
                   $.each(jqXHR.responseJSON, function (key, value) {
                       msg += "<li>" + value + "</li>";
                   });
                   msg += "</ul></strong>";
               }
           }
           $.alert({
               title: 'Error!!',
               type: 'red',
               icon: 'fa fa-exclamation-triangle',
               content: msg
           });
         },
         complete: function () {
            getTrainingDateTime();
            
        }
   });
}

function getSubdivisionPhasedata(){
 // var zone = $("#zone").val();
  var subdivision = $("#subdivision").val();
   var token = $("input[name='_token']").val();
  // $(".se-pre-con").fadeIn("slow");
   $.ajax({
       url: "getSubdivisionPhasedata",
       method: 'POST',
       data: {subdivision:subdivision, _token: token},
       success: function (data) {//alert(data.options);
       //   $(".se-pre-con").fadeOut("slow");
          $("select[name='phase'").html('');
          
          if(data.status==1)
          {
            $("select[name='phase'").append('<option value>[Select]</option>');
            $.each(data.options,function (k, v)
            {
                $("select[name='phase'").append('<option value='+k+'>'+v+'</option>');
            });
            //$("select[name='subdivision'").html(data.options);
             // change_officeName();
             
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
       //  $(".se-pre-con").fadeOut("slow");
           var msg = "";
           if (jqXHR.status !== 422 && jqXHR.status !== 400) {
               msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
           } else {
               if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                   msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
               } else {
                   msg += "Error(s):<strong><ul>";
                   $.each(jqXHR.responseJSON, function (key, value) {
                       msg += "<li>" + value + "</li>";
                   });
                   msg += "</ul></strong>";
               }
           }
           $.alert({
               title: 'Error!!',
               type: 'red',
               icon: 'fa fa-warning',
               content: msg
           });
           
         },
       
   });
}
function getAssemblyDetails(){
    var subdivision = $("#subdivision").val();
    var phase = $("#phase").val();
     var token = $("input[name='_token']").val();
     $(".se-pre-con").fadeIn("slow");
     $.ajax({
       url: "getSubAssemblyDetails",
       method: 'POST',
       data: {subdivision: subdivision,phase:phase, _token: token},
       success: function (data) {//alert(data.options);
         $(".se-pre-con").fadeOut("slow");
          $("select[name='assembly'").html('');
          if(data.status==1)
          {
              $("select[name='assembly'").html(data.options);
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
            $(".se-pre-con").fadeOut("slow");
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-exclamation-triangle',
                  content: msg
              });
         }
   });
}
function getTrainingDateTime(){
     var forDist = $("#districtcd").val();
     var token = $("input[name='_token']").val();
     //$(".se-pre-con").fadeIn("slow");
     $.ajax({
       url: "get2ndTrainingDateTime",
       method: 'POST',
       data: {forDist: forDist, _token: token},
       success: function (data) {//alert(data.options);
         $(".se-pre-con").fadeOut("slow");
          $("select[name='trainingdatetime'").html('');
          if(data.status==1)
          {
              $("select[name='trainingdatetime'").html(data.options);
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
            $(".se-pre-con").fadeOut("slow");
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-warning',
                  content: msg
              });
         }
   });
 }
function change_venueName(){
  var subdivision = $("#subdivision").val();
   var token = $("input[name='_token']").val();
   //$(".se-pre-con").fadeIn("slow");
   $.ajax({
       url: "get2ndVenueName",
       method: 'POST',
       data: {subdivision: subdivision, _token: token},
       success: function (data) {//alert(data.options);
          //$(".se-pre-con").fadeOut("slow");
          $("select[name='venuename'").html('');
          
          if(data.status==1)
          {
            $("select[name='venuename'").append('<option value>[Select]</option>');
            $.each(data.options,function (k, v)
            {
                $("select[name='venuename'").append('<option value='+k+'>'+v+'</option>');
            });
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
         $(".se-pre-con").fadeOut("slow");
           var msg = "";
           if (jqXHR.status !== 422 && jqXHR.status !== 400) {
               msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
           } else {
               if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                   msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
               } else {
                   msg += "Error(s):<strong><ul>";
                   $.each(jqXHR.responseJSON, function (key, value) {
                       msg += "<li>" + value + "</li>";
                   });
                   msg += "</ul></strong>";
               }
           }
           $.alert({
               title: 'Error!!',
               type: 'red',
               icon: 'fa fa-exclamation-triangle',
               content: msg
           });
         },
        complete: function () {
            getAssemblyDetails();
        }
   });
}
function SubFromToPR(m){
//alert(m);
    var s_party_re=$("#s_party_re"+m).val();
    var f_gr=$("#f_gr"+m).val();
    var e_gr=$("#e_gr"+m).val();
    var phase=$("#phase").val();
    //alert(f_gr);
        //alert(e_gr);
    var assembly=$("#assembly").val();
    if(s_party_re=="" || f_gr=="" || e_gr=="" || assembly==""){
        if(assembly==""){
            $.alert({
            title: 'Error!!',
            type: 'red',
            icon: 'fa fa-exclamation-triangle',
            content: 'Assembly is required'
           }); 
        }
        if(parseInt(f_gr)=="" && parseInt(e_gr)=="")
        {          
            $("#f_gr"+m).val("");
            $("#e_gr"+m).val("");
            $("#s_pr"+m).val("");
            $("#s_p1"+m).val("");
            $("#s_p2"+m).val("");
            $("#s_p3"+m).val("");
            $("#s_pa"+m).val("");
            $("#s_pb"+m).val("");
            $("#hid_PartyRes"+m).val("");
            $("#s_assign"+m).val("");
            var s_maxcp=$("#s_maxcp"+m).val();
           // alert(s_maxcp);
            $("#s_left"+m).val(s_maxcp);
            PP_Recordmanage();
        }       
        return false;
    }
    if(parseInt(f_gr)> parseInt(e_gr))
    {
         $.alert({
               title: 'Error!!',
               type: 'red',
               icon: 'fa fa-exclamation-triangle',
               content: 'From Group ["'+f_gr+'"] should not be greater than To Group ["'+e_gr+'"]'
              }); 
           $("#f_gr"+m).val("");
           $("#e_gr"+m).val("");
           $("#s_pr"+m).val("");
           $("#s_p1"+m).val("");
           $("#s_p2"+m).val("");
           $("#s_p3"+m).val("");
           $("#s_pa"+m).val("");
           $("#s_pb"+m).val("");
           $("#hid_PartyRes"+m).val("");
           $("#s_assign"+m).val("");
           var s_maxcp=$("#s_maxcp"+m).val();
           $("#s_left"+m).val(s_maxcp);
           PP_Recordmanage();
           return false; 
    }
    else
    {
       var token = $("input[name='_token']").val();
       $(".se-pre-con").fadeIn("slow");
       $.ajax({
        url: "get2ndVenuePPTotal",
        method: 'POST',
        data: {s_party_re: s_party_re,f_gr: f_gr,e_gr: e_gr,assembly: assembly,phase:phase, _token: token},
        success: function (data) {//alert(data.options);
           $(".se-pre-con").fadeOut("slow");
          // $("select[name='venuename'").html('');

           if(data.status==1)
           {
               if(data.options.length>0){
                 for(var i=0;i<data.options.length;i++){ 
                     var c = parseInt(i) + 1;
                     if(data.options[i].poststat=="PR"){
                         $("#s_pr"+m).val(data.options[i].cnt);
                     }
                     if(data.options[i].poststat=="P1"){
                         $("#s_p1"+m).val(data.options[i].cnt);
                     }
                     if(data.options[i].poststat=="P2"){
                         $("#s_p2"+m).val(data.options[i].cnt);
                     }
                     if(data.options[i].poststat=="P3"){
                         $("#s_p3"+m).val(data.options[i].cnt);
                     }
                     if(data.options[i].poststat=="PA"){
                         $("#s_pa"+m).val(data.options[i].cnt);
                     }
                     if(data.options[i].poststat=="PB"){
                         $("#s_pb"+m).val(data.options[i].cnt);
                     }

                 }
                var s_maxcp=$("#s_maxcp"+m).val();
                var s_pr=$("#s_pr"+m).val();
                var s_p1=$("#s_p1"+m).val();
                var s_p2=$("#s_p2"+m).val();
                var s_p3=$("#s_p3"+m).val();
                var s_pa=$("#s_pa"+m).val();
                var s_pb=$("#s_pb"+m).val();
                var new_assign=Math.round((+s_p1)+(+s_p2)+(+s_p3)+(+s_pa)+(+s_pb)+(+s_pr));
                if(parseInt(new_assign)<=parseInt(s_maxcp))
                {
                   $("#hid_PartyRes"+m).val(s_party_re);
                }
                else
                {
                     $.alert({
                        title: 'Error!!',
                        type: 'red',
                        icon: 'fa fa-exclamation-triangle',
                        content: 'Assigned value ["'+new_assign+'"] should not be greater than Maximum Capacty ["'+s_maxcp+'"]'
                       }); 
                    $("#f_gr"+m).val("");
                    $("#e_gr"+m).val("");
                    $("#s_pr"+m).val("");
                    $("#s_p1"+m).val("");
                    $("#s_p2"+m).val("");
                    $("#s_p3"+m).val("");
                    $("#s_pa"+m).val("");
                    $("#s_pb"+m).val("");
                    $("#hid_PartyRes"+m).val("");
                    $("#s_assign"+m).val("");
                    var s_maxcp=$("#s_maxcp"+m).val();
                    $("#s_left"+m).val(s_maxcp);
                }
              }else{
                 $("#f_gr"+m).val("");
                 $("#e_gr"+m).val("");
                 $("#s_pr"+m).val("");
                 $("#s_p1"+m).val("");
                 $("#s_p2"+m).val("");
                 $("#s_p3"+m).val("");
                 $("#s_pa"+m).val("");
                 $("#s_pb"+m).val("");
                 $("#hid_PartyRes"+m).val("");
                 $("#s_assign"+m).val("");
                 var s_maxcp=$("#s_maxcp"+m).val();
                 $("#s_left"+m).val(s_maxcp);
              }
             PP_Recordmanage();
           }
        },
         error: function (jqXHR, textStatus, errorThrown) {
          $(".se-pre-con").fadeOut("slow");
            var msg = "";
            if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
            } else {
                if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                    msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                } else {
                    msg += "Error(s):<strong><ul>";
                    $.each(jqXHR.responseJSON, function (key, value) {
                        msg += "<li>" + value + "</li>";
                    });
                    msg += "</ul></strong>";
                }
            }
            $.alert({
                title: 'Error!!',
                type: 'red',
                icon: 'fa fa-exclamation-triangle',
                content: msg
            });
          }
        });

    }
}
function PP_Recordmanage(){
    var TotalrwCnt=$("#row_count").val();
    //alert(TotalrwCnt);
    var totalpp=0;
    for(var l=0;l<TotalrwCnt;l++){          
        var m = parseInt(l) + 1;
        char_num("#f_gr"+m);
        char_num("#e_gr"+m);
        char_num("#s_pr"+m);
        char_num("#s_p1"+m);
        char_num("#s_p2"+m);
        char_num("#s_p3"+m);
        char_num("#s_pa"+m);
        char_num("#s_pb"+m);
        var s_party_reserve=$("#hid_PartyRes"+m).val();
        if(s_party_reserve!="")
        {
           $('#s_party_re' + m).val(s_party_reserve);

            var s_maxcp=$("#s_maxcp"+m).val();
            var s_pr=$("#s_pr"+m).val();
            var s_p1=$("#s_p1"+m).val();
            var s_p2=$("#s_p2"+m).val();
            var s_p3=$("#s_p3"+m).val();
            var s_pa=$("#s_pa"+m).val();
            var s_pb=$("#s_pb"+m).val();
            var new_assign=Math.round((+s_p1)+(+s_p2)+(+s_p3)+(+s_pa)+(+s_pb)+(+s_pr));
            $("#s_assign"+m).val(new_assign);
            var new_left=Math.round((+s_maxcp)-(+new_assign));
            $("#s_left"+m).val(new_left);
            totalpp=totalpp+new_assign;
        }           
    }
    if(totalpp>0){
        var maxcp=$("#maxcp" ).val();
        $("#assign").val(totalpp);
        var to_left=Math.round((+maxcp)-(+totalpp));
        $("#left").val(to_left);
    }
    else
    {
        var maxcp = $("#maxcp").val();
        $("#assign").val(totalpp);
        var to_left = Math.round((+maxcp) - (+totalpp));
        $("#left").val(to_left);
    }
}
</script>
@stop