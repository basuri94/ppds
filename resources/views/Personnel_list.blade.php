
@extends('layouts.master')
@section('content')
<style>
    .jconfirm-box{
        width: 650px;
        margin-left: -200px !important; 
    }

    /*    table.dataTable thead>tr>th.sorting_asc,table.dataTable thead>tr>th.sorting_desc,table.dataTable thead>tr>th.sorting,table.dataTable thead>tr>td.sorting_asc,table.dataTable thead>tr>td.sorting_desc,table.dataTable thead>tr>td.sorting{
            padding-right:0px !important;
        }*/

    .dropdown-menu{
        /* right: 67%;*/
        /* left: auto; */
        min-width: 0px !important; 
        width:230px !important;
        padding: 1em !important;
    }
</style>
<!--// top-bar -->
<div class="wrapper">
    <!-- Sidebar Holder -->
    @include('layouts.sidebar')
    <!-- Page Content Holder -->
    <div id="content">           
        <section class="tables-section">

            <div class="breadcrumb pagehead1">
                <span class="headbrand"><i class="fa fa-desktop"></i> Personnel List
                    
                </span>


                <div class="clearfix"></div>
            </div>
            </nav>
            <div class="outer-w3-agile">
                 {!! Form::hidden('searchOfficeValue', '',['id'=>'searchOfficeValue']) !!}
                <div class="panel-group" id="accordion5">                                                              
                    <div class="panel panel-default">
                        <div class="panel-heading1">
                            <h6 class="panel-title">
                                <a data-toggle="#" data-parent="#accordion5" href="#collapsePV"> <span class="fa fa-minus"></span> <span class="highlight">Office Wise Personnel List</span></a>
                            </h6>
                        </div>
                        <div id="collapsePV" class="panel-collapse collapse5">
                            <div class="panel-body">                                   
                                <div class="row">
<!--                                    <div class='col-sm-4'>
              {!! Form::label('office_id', 'Office Name:', ['class'=>'highlight']) !!}
              <div class="form-group">
                  <div class=''>

               <select class="office_id form-control"  name="office_id" id='office_id' autocomplete="off"></select>
                  </div>
              </div>
              <div style="display: none;">
                      {!! Form::text('office_id',null,['id'=>'office_id','class'=>'form-control','autocomplete'=>'off','readonly'=>'false']) !!}
                  </div>
          </div> -->
                                                                        <div class='col-sm-4'>
              {!! Form::label('officer_id', 'Officer Name:', ['class'=>'highlight']) !!}
              <div class="form-group">
                  <div class=''>

               <select class="officer_id form-control"  name="officer_id" id='officer_id' autocomplete="off"></select>
                  </div>
              </div>
              <div style="display: none;">
                      {!! Form::text('officer_id',null,['id'=>'officer_id','class'=>'form-control','autocomplete'=>'off','readonly'=>'false']) !!}
                  </div>
          </div>
                                    
                                    <div class='col-sm-4'>
              {!! Form::label('personnel_id', 'Personnel Id:', ['class'=>'highlight']) !!}
              <div class="form-group">
                  <div class=''>
{{-- <!--                      {!! Form::text('designation_search',null,['id'=>'designation_search','class'=>'form-control typeahead','autocomplete'=>'off']) !!}--> --}}
               <select class="personnel_id form-control"  name="personnel_id" id='personnel_id' autocomplete="off"></select>
                  </div>
              </div>
              <div style="display: none;">
                      {!! Form::text('personnel_id',null,['id'=>'personnel_id','class'=>'form-control','autocomplete'=>'off','readonly'=>'false']) !!}
                  </div>
          </div> 
                                    

                                </div>
                            </div>

                        </div>
                        <div class="form-horizontal">
                            <div class="form-group row">
                                {{csrf_field()}}
                                <div class="datatbl table-responsive" style="width: 96%;margin-left: 20px;">
                                    {!! Form::hidden('districtcd', session()->get('districtcd_ppds'),['id'=>'districtcd']) !!}
                                    <table class="table table-striped table-bordered table-hover notice-types-table" id="datatable-table" style="">
                                        <thead>
                                            <tr>
                                                <th>SL#</th>
                                                <th>Personnel id</th>
                                                <th>Name</th>
                                                <th>Designation</th>
                                                 <th>Office Code</th>
                                               <th>Mobile No</th>
                                                <th>Action</th>
                                            </tr>

                                        </thead>
                                        <tbody></tbody>
                                        <!-- Table Footer -->

                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </section>
    </div>
</div>
<!-- Copyright -->
<script type="text/javascript">
    $(document).ready(function () {

        $("#reset").click(function () {
            location.reload(true);
        });

//        $('select[name="office_id"]').on('change', function () {
//            create_table();
//        });

        var table = "";
        table = $('#datatable-table').DataTable();
        table.on('draw.dt', function () {
            $('.edit-button').click(function () {
                var data = this.id;
             // alert(data);
                var datas = {'edit_code': data, '_token': $('input[name="_token"]').val()};
                //alert(data.code);
                redirectPost('{{url("/edit_personnel")}}', datas);

            });
        });
        
        var token = $("input[name='_token']").val();
       $('.office_id').select2({
            placeholder: "Enter Office Name",
            minimumInputLength: 2,
            maximumInputLength: 10,
             
            ajax: {
                url: 'officeSearch',
                dataType: 'json',
                method: 'POST',
                data: function (params) {
                    return {
                        q: $.trim(params.term),
                         _token: token
                    };
                },
                processResults: function (data) {
                    
                    return {
                        results:  $.map(data.options, function (item) {
                    return {
                        text: item.office,
                        id: item.officecd
                    }
                })
                    };

                },
                cache: true
            }
        }).on("select2:select", function(event) {
  var value = $(event.currentTarget).find("option:selected").val();
//alert(value);
         //$('#searchOfficeValue').val(value);
         create_table();
}); 



$('.personnel_id').select2({
        // alert("hi");
        
            placeholder: "Enter Personnel Id",
            minimumInputLength: 5,
            maximumInputLength: 11,
             
            ajax: {
                url: 'personnelIdSearch',
                dataType: 'json',
                method: 'POST',
                data: function (params) {
                    return {
                        q: $.trim(params.term),
                         _token: token
                    };
                },
                processResults: function (data) {
                    
                    return {
                        results:  $.map(data.options, function (item) {
                    return {
                        text: item.personcd,
                        id: item.personcd
                    }
                })
                    };

                },
                cache: true
            }
        }).on("select2:select", function(event) {
  var value = $(event.currentTarget).find("option:selected").val();
//alert(value);
         //$('#searchOfficeValue').val(value);
         create_table();
}); 
      
      $('.officer_id').select2({
            placeholder: "Enter Officer Name",
            minimumInputLength: 2,
            maximumInputLength: 10,
             
            ajax: {
                url: 'officerSearch',
                dataType: 'json',
                method: 'POST',
                data: function (params) {
                    return {
                        q: $.trim(params.term),
                         _token: token
                    };
                },
                processResults: function (data) {
                    
                    return {
                        results:  $.map(data.options, function (item) {
                    return {
                        text: item.officer_name,
                        id: item.personcd
                    }
                })
                    };

                },
                cache: true
            }
        }).on("select2:select", function(event) {
  var value = $(event.currentTarget).find("option:selected").val();
//alert(value);
         //$('#searchOfficeValue').val(value);
         create_table();
}); 
    });



    function create_table() {
        var table = "";
        table = $('#datatable-table').DataTable();
        var token = $('input[name="_token"]').val();
        var districtcd = $("#districtcd").val();
      //  var office_id = $('#office_id').val();
        var personnel_id=$('#personnel_id').val();
        //alert(personnel_id);
          var officer_id=$('#officer_id').val();
      //  alert(officer_id);
        $("#datatable-table").dataTable().fnDestroy();
        table = $('#datatable-table').DataTable({
            "processing": true,
            "serverSide": true,
            "dom": 'lBfrtip',
            "buttons": [
                //'copy', 'csv', 'excel', 'pdf'
                {
                    extend: 'collection',
                    text: 'Export',
                    buttons: [
                        {
                            extend: 'copyHtml5',
                            exportOptions: {
                                columns: ':visible'
                            }
                        },
                        {
                            extend: 'excelHtml5',
                            exportOptions: {
                                columns: ':visible'
                            }
                        },
                        {
                            extend: 'pdfHtml5',
                            exportOptions: {
                                columns: ':visible'
                            }
                        },
                        {
                            extend: 'print',
                            exportOptions: {
                                columns: ':visible'
                            }
                        },
                        {
                            extend: 'csvHtml5',
                            exportOptions: {
                                columns: ':visible'
                            }
                        }

                    ]

                },
                'colvis'
            ],
            "ajax": {
                url: "su_personnel_list" ,
                method: 'POST',
                data: {district_id: districtcd,personnel_id:personnel_id,officer_id:officer_id, _token: token},
                dataSrc: "personnel_masters",
                error: function (jqXHR, textStatus, errorThrown) {
                    var msg = "";
                    if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                        msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                    } else {
                        if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                            if (jqXHR.responseJSON.exception_code == 23000) {
                                msg += "Some Sql Exception Occured";
                            } else {
                                msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                            }
                        } else {
                            msg += "Error(s):<strong><ul>";
                            $.each(jqXHR.responseJSON, function (key, value) {
                                msg += "<li>" + value + "</li>";
                            });
                            msg += "</ul></strong>";
                        }
                    }
                    $.alert({
                        title: 'Error!!',
                        type: 'red',
                        icon: 'fa fa-warning',
                        content: msg,
                    });
                }
            },
            "dataType": 'json',
            "columnDefs":
                    [
                        {className: "table-text", "targets": "_all"},
                        {
                            "targets": 0,
                            "data": "mob_no",
                            "defaultContent": "",
                            "searchable": false,
                            "sortable": false
                        },
                        {
                            "targets": 1,
                            "data": "personcd",
                        },
                        {
                            "targets": 2,
                            "data": "officer_name",
                        },
                        {
                            "targets": 3,
                            "data": "off_desg",
                        },
                        {
                            "targets": 4,
                            "data": "officecd",
                        },
                        {
                            "targets": 5,
                            "data": "mob_no",
                        },
                        {
                            "targets": 6,
                            "data": 'action',
                            "searchable": false,
                            "sortable": false,
                            "render": function (data, type, full, meta) {
                                var str_btns = "";
                              // alert(data);
                                str_btns += '<button type="button"  class="btn btn-success  edit-button btn_new1" id="' + data.e + '" title="Edit"><i class="fa fa-edit"></i></button>&nbsp&nbsp';
                                return str_btns;
                            }
                        }
                    ],

            "order": [[1, 'asc']]
        });
        table.on('order.dt search.dt draw.dt', function () {
            $('[data-toggle="tooltip"]').tooltip();
            table.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
                cell.innerHTML = table.page() * table.page.len() + (i + 1);
            });
        });
        // table.columns( [-4,-3,-2,-1] ).visible( false );
    }
</script>
@stop

