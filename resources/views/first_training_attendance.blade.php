@extends('layouts.master')
@section('content')
<!--// top-bar -->
 <div class="wrapper">
<!-- Sidebar Holder -->
@include('layouts.sidebar')

<!-- Page Content Holder -->
<div id="content">           
<section class="tables-section">
    <nav aria-label="breadcrumb" style="width:100%;">
        <div class="breadcrumb pagehead1">
            <span class="headbrand"><i class="fa fa-desktop"></i> First Training Attendance
            <span class="scoop-mcaret1"></span>
           </span>         
                <div class="clearfix"></div>
        </div>
    </nav>

    <!-- form -->
<div class="outer-w3-agile">
  {!! Form::open(['url' => 'first_tr_attendance', 'name' => 'first_tr_attendance', 'class' =>'request-info clearfix form-horizontal', 'id' => 'first_tr_attendance', 'method' => 'post','role'=>'','files' => true]) !!}
  {!! Form::hidden('districtcd', session()->get('districtcd_ppds'),['id'=>'districtcd']) !!}
  
  <div class="panel-group" id="accordion5">                                                              
   <div class="panel panel-default">
    <div class="panel-heading1">
     <h6 class="panel-title">
         <a data-toggle="#" data-parent="#accordion5" href="#collapsePV"> <span class="fa fa-minus"></span> <span class="highlight">First Training Attendance </span></a>
     </h6>
    </div>
    <div id="collapsePV" class="panel-collapse collapse5">
      <div class="panel-body">                                   
       <div class="row">  
            <div class='col-sm-4'>
               {!! Form::label('subdivision', 'Sub Division:', ['class'=>'highlight required']) !!}
                 <!--<span class="highlight">Sub Division</span> -->
                 <div class="form-group">
                     <div class=''>
                         {!! Form::select('subdivision',[''=>'[Select]'],null,['id'=>'subdivision','class'=>'form-control','autocomplete'=>'off']) !!}
                     </div>
                 </div>
           </div> 

             <div class='col-sm-4'>
                 {!! Form::label('venuename', 'Venue Name:', ['class'=>'highlight required']) !!}
                 <div class="form-group">
                     <div class=''>
                          {!! Form::select('venuename',[''=>'[Select]'],null,['id'=>'venuename','class'=>'form-control','autocomplete'=>'off']) !!}
                     </div>
                 </div>
             </div>
           <div class='col-sm-4'>
                 {!! Form::label('subvenuename', 'Sub-venue Name:', ['class'=>'highlight required']) !!}
                 <div class="form-group">
                     <div class=''>
                          {!! Form::select('subvenuename',[''=>'[Select]'],null,['id'=>'subvenuename','class'=>'form-control','autocomplete'=>'off']) !!}
                     </div>
                 </div>
             </div>
        </div>
               
          
        <div class="row"> 
              <div class='col-sm-4'>
                 {!! Form::label('trainingtype', 'Training Type:', ['class'=>'highlight required']) !!}
                 <div class="form-group">
                     <div class=''>
                          {!! Form::select('trainingtype',$training_type['desc'],null,['id'=>'trainingtype','class'=>'form-control','placeholder'=>'[Select]','autocomplete'=>'off']) !!}
                     </div>
                 </div>
             </div>
             <div class='col-sm-4'>
               {!! Form::label('trainingdatetime', 'Training Date & Time:', ['class'=>'highlight required']) !!}
                 <div class="form-group">
                     <div class=''>
                        {!! Form::select('trainingdatetime',[''=>'[Select]'],null,['id'=>'trainingdatetime','class'=>'form-control','placeholder'=>'[Select]','autocomplete'=>'off']) !!}
                     </div>
                </div>
           </div>
        </div>
        
          
          
      <div class="row"> 
            <div class='col-sm-12'>
                <div id='assemblyDetails'></div>
            </div>
         
      </div> 
      <div class="row" id="rw" style="display: none;" > 
           <div class='col-sm-12' > 
               {!! Form::label('', '', ['class'=>'highlight']) !!}
                <div class="form-group text-right permit" > 
                    {{ Form::button('Submit', ['class' => 'btn btn-info', 'type' => 'submit','id'=>'Submit']) }}
                    {{ Form::button('Reset', ['class' => 'btn btn-success' ,'type' => 'reset','id'=>'reset']) }}
                </div>
           </div>
                
       </div> 
        <div class="row" id="rw1" style="display: none;" > 
          <div class='col-sm-4'>
               {!! Form::label('records', 'Type of Records (For Downloading):', ['class'=>'highlight']) !!}
              <div class="form-group">
                  <div class=''>
                      {!! Form::select('records',['A'=>'ALL Submitted'],Null,['id'=>'records','class'=>'form-control','autocomplete'=>'off']) !!}
                  </div>
              </div>
          </div>
           <div class='col-sm-8' > 
               {!! Form::label('', '', ['class'=>'highlight']) !!}
                <div class="form-group text-right permit" > 
                    {{ Form::button('EXCEL', ['class' => 'btn btn-info', 'type' => 'button','id'=>'EXCEL']) }}
                    {{ Form::button('Reset', ['class' => 'btn btn-success' ,'type' => 'reset','id'=>'reset']) }}
                </div>
           </div>
                
       </div> 
    </div>                              
   </div>
  </div>  
  </div>
 {!! Form::close() !!}             
</div>
<!--// form -->

</section>
</div>
</div>
<!-- Copyright -->
<script type="text/javascript">
$(document).ready(function () {
 change_subdivision();
 $("select[name='records'").append('<option value="Y">Prev Submitted</option>');
 $("select[name='records'").append('<option value="C">Last Submitted</option>');
 $("#reset").click(function () {
  location.reload(true);  
});
 $('select[name="subdivision"]').on('change', function () {
    change_venueName();    
 });
//getZoneDetails(); 
//char_num("#pr");

 $('select[name="venuename"]').on('change', function () {
    change_subvenueName();
       
 });
 $('select[name="subvenuename"]').on('change', function () {
    getAttendanceVenueDetails();
       
 });
  $('select[name="trainingtype"]').on('change', function () {
    getAttendanceVenueDetails();
       
 });
 $('select[name="trainingdatetime"]').on('change', function () {
    getAttendanceVenueDetails();
       
 });
  $('#EXCEL').click(function () {
     var subdivision = $("#subdivision").val();
    var venuename = $("#venuename").val();
    var trainingtype = $("#trainingtype").val();
    var trainingdatetime = $("#trainingdatetime").val();
    var subvenuename = $("#subvenuename").val();
    var records = $("#records").val();
    if(subdivision== ""){
        $.alert({
             title: 'Error!!',
             type: 'red',
             icon: 'fa fa-exclamation-triangle',
             content: 'Subdivision is required'
         });
         return false;
     }
    if(venuename== ""){
        $.alert({
             title: 'Error!!',
             type: 'red',
             icon: 'fa fa-exclamation-triangle',
             content: 'Venue Name is required'
         });
         return false;
     }
      if(trainingtype== ""){
        $.alert({
             title: 'Error!!',
             type: 'red',
             icon: 'fa fa-exclamation-triangle',
             content: 'Training Type is required'
         });
         return false;
     }
      if(trainingdatetime== ""){
        $.alert({
             title: 'Error!!',
             type: 'red',
             icon: 'fa fa-exclamation-triangle',
             content: 'Training Date & Time is required'
         });
         return false;
     }
      if(subvenuename== ""){
        $.alert({
             title: 'Error!!',
             type: 'red',
             icon: 'fa fa-exclamation-triangle',
             content: 'Sub-venue is required'
         });
         return false;
     }
    var datas = {subdivision: subdivision,subvenuename:subvenuename,records:records,venuename: venuename,trainingtype: trainingtype,trainingdatetime: trainingdatetime,'_token': $('input[name="_token"]').val()};
     $(".se-pre-con").fadeIn("slow");
     redirectPost('{{url("getFirstTrAttenExcel")}}', datas);
     $(".se-pre-con").fadeOut("slow");


 }); 
 $('#first_tr_attendance').bootstrapValidator({

        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {

            subdivision: {
                validators: {
                    notEmpty: {
                        message: 'Subdivision is required'
                    }
                }
            },
            venuename: {
                validators: {
                    notEmpty: {
                        message: 'Venue is required'
                    }
                }
            },
            subvenuename: {
                validators: {
                    notEmpty: {
                        message: 'Sub-venue is required'
                    }
                }
            },
            trainingtype: {
                validators: {
                    notEmpty: {
                        message: 'Training Type is required'
                    }
                }
            },
            trainingdatetime: {
                validators: {
                    notEmpty: {
                        message: 'Training Date & Time is required'
                    }
                }
            }
            
        }

    }).on('success.form.bv', function (e) {
        e.preventDefault();
        var action = $(this).attr('action');
        var type = $(this).attr('method');
        
        var formData = new FormData($(this)[0]);
        $(".se-pre-con").fadeIn("slow");
        $.ajax({
            type: type,
            url: action,
            data: formData,
            processData: false,
            contentType: false,
            dataType: "json",
            success: function (data) {
                $(".se-pre-con").fadeOut("slow");

                if (data.status == 1) { 
                    var msg=data.options+" Record(s) saved successfully";
                    $.confirm({
                       title: 'Success!',
                       type: 'green',
                       icon: 'fa fa-check',
                       content: msg,
                       buttons: {
                           ok: function () {     
                               $("#Submit").attr('disabled',false);
                               getAttendanceVenueDetails();
                               $("#rw1").show();
                           }
                       }
                   });
                } 
            },
            error: function (jqXHR, textStatus, errorThrown) {
            $(".se-pre-con").fadeOut("slow");
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-exclamation-triangle',
                  content: msg
              });
              $("#Submit").attr('disabled',false);
           }
        });
    });
});

function getAttendanceVenueDetails(){
   var subdivision = $("#subdivision").val();
   var venuename = $("#venuename").val();
   var subvenuename = $("#subvenuename").val();
   var trainingtype = $("#trainingtype").val();
   var trainingdatetime = $("#trainingdatetime").val();
   $("#rw").hide();
   if (subdivision != "" && subvenuename !="" && venuename != "" && trainingtype != "" && trainingdatetime != "") {
        var token = $("input[name='_token']").val();
        $(".se-pre-con").fadeIn("slow");
        $.ajax({
            type: 'POST',
            url: 'getPPForTrAttendance',
            data: {subdivision:subdivision,venuename:venuename, subvenuename:subvenuename, trainingtype:trainingtype, trainingdatetime:trainingdatetime, _token:token},
            dataType: 'json',
            success: function (response) {
            $(".se-pre-con").fadeOut("slow");
            $("#assemblyDetails").html("");
            var str='<div style="height:450px;overflow:scroll;">';
            str+='<input type="hidden" id="row_count" name="row_count" value="'+response.options.length+'"> '
            str+= "<table class='table table-bordered table-striped' width='100%' style='border-top: 2px solid #4cae4c;'>";
            str+= ' <tr style="background-color: #f5f8fa"><th align="left">&nbsp;<input type="checkbox" id="myCheckall" name="myCheckall"  value="1"  class="myCheckall">&nbsp;</th><th align="left">&nbsp;Personnel ID </th><th>&nbsp;Personnel Name </th><th>&nbsp;Absent </th></tr>';
    //         str += '<tr style=" background-color: #1ABB9C; color: #FFFFFF; font-weight: bold;"><td align="left"><input type="checkbox" id="myCheckall" name="myCheckall"  value="1"  class="submenu">&nbsp; Check All</td><td align="center" width="30%"><input type="checkbox" id="view_all" class="flat"></td></tr>';
             var sub_length = response.options.length;
            // alert(sub_length);
             for (var i = 0; i < sub_length; i++)
             {
                   var c = parseInt(i) + 1;
                   
                    str += '<tr><td align="left" width="10%">&nbsp;<input type="hidden" id="schedule_code" name="schedule_code" value="'+ response.options[i].schedule_code+'" >';
                    
                    if(response.options[i].pStatus==0){
                        str+='<input type="checkbox" id="myCheck' + c + '" name="myCheck' + c + '"  value="'+ response.options[i].personcd+'" class="submenu" >';   
                    }else{
                        str+='<input type="checkbox" id="myCheck' + c + '" name="myCheck' + c + '"  value="'+ response.options[i].personcd+'" checked class="submenu" >';   
                    }                         
                    str+='</td><td align="left" width="30%">&nbsp;' + response.options[i].personcd +'</td>';
                    str+='<td align="left" width="50%">&nbsp;' + response.options[i].officer_name +'</td>';

                       
                    str+='<td align="left" width="10%">';
                    if(response.options[i].pStatus==0)
                    {
                       str+='&nbsp;<span class="fa fa-exclamation-triangle" style="color: red;"></span></td>';
                    }
                    else{
                      str+='&nbsp;<span class="fa fa-check" style="color: green;"></span></td>';  

                    }
                    str+='</td></tr>'; // return empty
                    
            }
            str += "</table>";
            str += "</div>";
            $("#assemblyDetails").append(str);

              $("#myCheckall").change(function(){ 
                var status = this.checked;
                if(status==true){
                    $("#rw").show();
                    $("#rw1").hide();
                    scrollToElement($('#Submit'));
                }else if(status==false){
                    $("#rw").hide();
                }

                $('.submenu').each(function(){
                    this.checked = status;     
                });
            });
            $('.submenu').change(function(){
                if($('.submenu:checked').length=='0'){
                    $("#rw").hide();
                }else{
                    $("#rw").show();
                    $("#rw1").hide();
                }
                if(this.checked == false){
                    $("#myCheckall")[0].checked = false; 
                }
                if ($('.submenu:checked').length == $('.submenu').length ){ 
                    $("#myCheckall")[0].checked = true;
                }
            });
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $(".se-pre-con").fadeOut("slow");
                  var msg = "";
                  if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                      msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                  } else {
                      if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                          msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                      } else {
                          msg += "Error(s):<strong><ul>";
                          $.each(jqXHR.responseJSON, function (key, value) {
                              msg += "<li>" + value + "</li>";
                          });
                          msg += "</ul></strong>";
                      }
                  }
                  $.alert({
                      title: 'Error!!',
                      type: 'red',
                      icon: 'fa fa-exclamation-triangle',
                      content: msg
                  });
             }
        });
    }
}

function getZoneDetails(){
     var forDist = $("#districtcd").val();
     var token = $("input[name='_token']").val();
     $(".se-pre-con").fadeIn("slow");
     $.ajax({
       url: "getZoneDetails",
       method: 'POST',
       data: {forDist: forDist, _token: token},
       success: function (data) {//alert(data.options);
         $(".se-pre-con").fadeOut("slow");
          $("select[name='forZone'").html('');
          if(data.status==1)
          {
             $("select[name='forZone'").append('<option value>[Select]</option>');
            $.each(data.options,function (k, v)
            {
                $("select[name='forZone'").append('<option value='+k+'>'+v+'</option>');
            });
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
            $(".se-pre-con").fadeOut("slow");
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-exclamation-triangle',
                  content: msg
              });
         }
   });
}
function change_subvenueName(){
  var subdivision = $("#subdivision").val();
  var venuename = $("#venuename").val();
   var token = $("input[name='_token']").val();
   $(".se-pre-con").fadeIn("slow");
   $.ajax({
       url: "getSubVenueName",
       method: 'POST',
       data: {subdivision: subdivision, venuename:venuename, _token: token},
       success: function (data) {//alert(data.options);
          $(".se-pre-con").fadeOut("slow");
          $("select[name='subvenuename'").html('');
          
          if(data.status==1)
          {
            $("select[name='subvenuename'").append('<option value>[Select]</option>');
            $.each(data.options,function (k, v)
            {
                $("select[name='subvenuename'").append('<option value='+k+'>'+v+'</option>');
            });
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
         $(".se-pre-con").fadeOut("slow");
           var msg = "";
           if (jqXHR.status !== 422 && jqXHR.status !== 400) {
               msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
           } else {
               if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                   msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
               } else {
                   msg += "Error(s):<strong><ul>";
                   $.each(jqXHR.responseJSON, function (key, value) {
                       msg += "<li>" + value + "</li>";
                   });
                   msg += "</ul></strong>";
               }
           }
           $.alert({
               title: 'Error!!',
               type: 'red',
               icon: 'fa fa-exclamation-triangle',
               content: msg
           });
         }
   });
}
function change_venueName(){
  var subdivision = $("#subdivision").val();
   var token = $("input[name='_token']").val();
   $(".se-pre-con").fadeIn("slow");
   $.ajax({
       url: "getVenueName",
       method: 'POST',
       data: {subdivision: subdivision, _token: token},
       success: function (data) {//alert(data.options);
          $(".se-pre-con").fadeOut("slow");
          $("select[name='venuename'").html('');
          
          if(data.status==1)
          {
            $("select[name='venuename'").append('<option value>[Select]</option>');
            $.each(data.options,function (k, v)
            {
                $("select[name='venuename'").append('<option value='+k+'>'+v+'</option>');
            });
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
         $(".se-pre-con").fadeOut("slow");
           var msg = "";
           if (jqXHR.status !== 422 && jqXHR.status !== 400) {
               msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
           } else {
               if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                   msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
               } else {
                   msg += "Error(s):<strong><ul>";
                   $.each(jqXHR.responseJSON, function (key, value) {
                       msg += "<li>" + value + "</li>";
                   });
                   msg += "</ul></strong>";
               }
           }
           $.alert({
               title: 'Error!!',
               type: 'red',
               icon: 'fa fa-exclamation-triangle',
               content: msg
           });
         }
   });
}
function change_subdivision(){
  var forDist = $("#districtcd").val();
   var token = $("input[name='_token']").val();
  // $(".se-pre-con").fadeIn("slow");
   $.ajax({
       url: "getSubdivisionData",
       method: 'POST',
       data: {forDist: forDist, _token: token},
       success: function (data) {//alert(data.options);
       //   $(".se-pre-con").fadeOut("slow");
          $("select[name='subdivision'").html('');
          
          if(data.status==1)
          {
            $("select[name='subdivision'").append('<option value>[Select]</option>');
            $.each(data.options,function (k, v)
            {
                $("select[name='subdivision'").append('<option value='+k+'>'+v+'</option>');
            });
            //$("select[name='subdivision'").html(data.options);
             // change_officeName();
            // getMemberDetails();
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
         $(".se-pre-con").fadeOut("slow");
           var msg = "";
           if (jqXHR.status !== 422 && jqXHR.status !== 400) {
               msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
           } else {
               if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                   msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
               } else {
                   msg += "Error(s):<strong><ul>";
                   $.each(jqXHR.responseJSON, function (key, value) {
                       msg += "<li>" + value + "</li>";
                   });
                   msg += "</ul></strong>";
               }
           }
           $.alert({
               title: 'Error!!',
               type: 'red',
               icon: 'fa fa-exclamation-triangle',
               content: msg
           });
         },complete: function(){
          getTrainingDateTime();
         }
   });
}
function getTrainingDateTime(){
     var forDist = $("#districtcd").val();
     var token = $("input[name='_token']").val();
    // $(".se-pre-con").fadeIn("slow");
     $.ajax({
       url: "getTrainingDateTime",
       method: 'POST',
       data: {forDist: forDist, _token: token},
       success: function (data) {//alert(data.options);
         $(".se-pre-con").fadeOut("slow");
          $("select[name='trainingdatetime'").html('');
          if(data.status==1)
          {
              $("select[name='trainingdatetime'").html(data.options);
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
            $(".se-pre-con").fadeOut("slow");
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-warning',
                  content: msg
              });
         }
   });
 }
function scrollToElement(ele) {
    $('html, body').animate({
        scrollTop: ele.offset().top - 60
    }, 1000);
}
</script>
@stop