
@extends('layouts.master')
@section('content')
<style>
    .dropdown-menu{
        /* right: 67%;*/
        /* left: auto; */
        min-width: 0px !important; 
        width:230px !important;
        padding: 1em !important;
    }

</style>
<div class="wrapper">
    <!-- Sidebar Holder -->
    @include('layouts.sidebar')

    <!-- Page Content Holder -->
    <div id="content">           
        <section class="tables-section">
            <nav aria-label="breadcrumb" style="width:100%;">
                <div class="breadcrumb pagehead1">

                    {!! Form::button('<i class="fa fa-desktop"></i> Change Password',['class' => 'btn btn-primary-header add-new-button']) !!}

                    <div class="col-md-offset-8 pull-right">
                        <!--                {!! Form::button('Academic Year:&nbsp;',['id'=>'show_academic','class' => 'btn btn-primary-year']) !!}-->

                    </div>
                    <div class="clearfix"></div>
                </div>
            </nav>

            <!-- form -->
            <div class="outer-w3-agile">
                {!! Form::open(['url' => 'change_password_create', 'name' => 'change_password_create', 'class' =>'request-info clearfix form-horizontal', 'id' => 'change_password_create', 'method' => 'post','role'=>'','files' => true]) !!}

                <div class="panel-group" id="accordion5" style="padding-top: 0px;">                                                              
                    <div class="panel panel-default">
                        <div class="panel-heading1">
                            <h6 class="panel-title">
                                <a data-toggle="#" data-parent="#accordion5" href="#collapseUV"> <span class="fa fa-minus"></span> <span class="highlight">Change Pssword </span></a>
                            </h6>
                        </div>
                        <div id="collapseUV" class="panel-collapse collapse5">
                            <div class="panel-body">                                   
                                <div class="row">                                   
                                    <div class='col-sm-6'>
                                        {!! Form::label('pass', 'New Password:', ['class'=>'highlight required']) !!} <span style="color:red;font-size: 11px;font-weight: bold;">[Max:10]</span>
                                        <div class="form-group">
                                            <div class="">
                                            {!! Form::password('pass',['id'=>'pass','class' => 'form-control', 'placeholder' => 'New Password', 'type' => 'password','autocomplete'=>'off','maxlength'=>'10']) !!}
                                           </div>
                                        </div>
                                    </div> 
                                    <div class='col-sm-6'>
                                        {!! Form::label('con_pass', 'Confirm Password:', ['class'=>'highlight required']) !!} <span style="color:red;font-size: 11px;font-weight: bold;">[Max:10]</span>
                                        <div class="form-group">
                                            <div class="">
                                            {!! Form::password('con_pass',['id'=>'con_pass','class' => 'form-control', 'placeholder' => 'Confirm Password', 'type' => 'password','autocomplete'=>'off','maxlength'=>'10']) !!}
                                           </div>
                                        </div>
                                    </div> 
                                    
                                </div>
                                <div class="row" > 
                                    <div class='col-sm-12'>                    
                                        <div class="form-group text-right permit">                            	
                                            {{ Form::button('Submit', ['class' => 'btn btn-info', 'type' => 'submit','id'=>'Submit']) }}
                                            {{ Form::button('Reset', ['class' => 'btn btn-success' ,'type' => 'reset','id'=>'reset']) }}
                                         </div>
                                    </div>

                                </div>
                        
      
                            </div>
                        </div>                              
                    </div> 


                </div>


                {!! Form::close() !!}  
            </div>
            <!--// form -->

        </section>
    </div>
</div>
<!-- Copyright -->
<script type="text/javascript">
    $(document).ready(function () {
        $("#reset").click(function () {
            location.reload(true);
        });
        $('#change_password_create')
                .bootstrapValidator({
                    message: 'This value is not valid',
                    feedbackIcons: {
                        valid: 'fa fa-check',
                        invalid: 'fa fa-times',
                        validating: 'fa fa-refresh'
                    },
                    fields: {
                         pass: {
                            validators: {
                                notEmpty: {
                                    message: 'Password is required'
                                },
                                regexp: {
                                    regexp: /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@#$!%*?&_])[A-Za-z\d$@#$!%*?&_]{6,}[a-zA-Z0-9!$@#%_]+$/,
                                    message: 'Password contain alteast six characters, one uppercase letter, one lowercase letter, one number and one special character[!@#$%^&*_-].'
                                }
                            }
                        },
                        con_pass: {
                            validators: {
                                notEmpty: {
                                    message: 'Confirm Password is required'
                                },
                                identical: {
                                    field: 'pass',
                                    message: 'Password does not match the confirm password'
                                },
                                regexp: {
                                    regexp: /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@#$!%*?&_])[A-Za-z\d$@#$!%*?&_]{6,}[a-zA-Z0-9!$@#%_]+$/,
                                    message: 'Confirm Password contain alteast six characters, one uppercase letter, one lowercase letter, one number and one special character[!@#$%^&*_-].'
                                }
                            }
                        }
                    }
                }).on('success.form.bv', function (e) {
            // Prevent form submission
            e.preventDefault();
            var action = $(this).attr('action');

            var pass = $("#pass").val();
            var con_pass = $("#con_pass").val();
            var type = $(this).attr('method');
            var token = $("input[name='_token']").val();
            var fd = new FormData();
            fd.append('pass', pass);
            fd.append('con_pass', con_pass);


            fd.append('_token', token);
            $(".se-pre-con").fadeIn("slow");
            $.ajax({
                type: type,
                url: action,
                data: fd,
                processData: false,
                contentType: false,
                dataType: "json",
                success: function (data) {

                    if (data.status == 1)
                    {

                        $(".se-pre-con").fadeOut("slow");

                        var msg = "Password changed successfully";
                        $.confirm({
                            title: 'Success!',
                            type: 'green',
                            icon: 'fa fa-check',
                            content: msg,
                            buttons: {
                                ok: function () {

                                    //  document.getElementById("first_training_date_entry").reset();
                                   location.reload(true);
                                }

                            }
                        });
                    } 

                },
                error: function (jqXHR, textStatus, errorThrown) {
                    $(".se-pre-con").fadeOut("slow");
                    var msg = "";
                    if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                        msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                    } else {
                        if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                            msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                        } else {
                            msg += "Error(s):<strong><ul>";
                            $.each(jqXHR.responseJSON, function (key, value) {
                                msg += "<li>" + value + "</li>";
                            });
                            msg += "</ul></strong>";
                        }
                    }
                    $.alert({
                        title: 'Error!!',
                        type: 'red',
                        icon: 'fa fa-warning',
                        content: msg
                    });
                }
            });

        });


    });
    
</script>
@stop

