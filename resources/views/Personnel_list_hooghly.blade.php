
@extends('layouts.master')
@section('content')

 <div class="wrapper">
<!-- Sidebar Holder -->
@include('layouts.sidebar')

<!-- Page Content Holder -->
<div id="content">           
<section class="tables-section">
    <nav aria-label="breadcrumb" style="width:100%;">
        <div class="breadcrumb pagehead1">
           
                {!! Form::button('<i class="fa fa-desktop"></i> Personnel List',['class' => 'btn btn-primary-header add-new-button']) !!}
            
            <div class="col-md-offset-8 pull-right">
<!--                {!! Form::button('Academic Year:&nbsp;',['id'=>'show_academic','class' => 'btn btn-primary-year']) !!}-->
                
            </div>
            <div class="clearfix"></div>
        </div>
    </nav>

    <!-- form -->
<div class="outer-w3-agile">
  {!! Form::open(['url' => 'su_personnel_list', 'name' => 'su_personnel_list', 'class' =>'request-info clearfix form-horizontal', 'id' => 'su_personnel_list', 'method' => 'post','role'=>'','files' => true]) !!}
  {!! Form::hidden('districtcd', session()->get('districtcd_ppds'),['id'=>'districtcd']) !!}
  {!! Form::hidden('office_list','',['class'=>'form-control','id'=>'office_list']) !!}
  
  <div class="panel-group" id="accordion5" style="padding-top: 0px;">                                                              
    <div class="panel panel-default">
     <div class="panel-heading1">
     <h6 class="panel-title">
         <a data-toggle="#" data-parent="#accordion5" href="#collapseUV"> <span class="fa fa-minus"></span> <span class="highlight">Office List</span> </a>
     </h6>
     </div>
     <div id="collapseUV" class="panel-collapse collapse5">
      <div class="panel-body">                                   
        <div class="row">                                   
           <div class='col-sm-10'>
               {!! Form::label('Select Office', 'Select Office:', ['class'=>'highlight required']) !!}
               <span style="color:red;font-size: 11px;">[*]</span>
              <div class="form-group">
                  <div class=''>
                      <select id="ofice_list" class="form-control" name="office_id">
                        <?php foreach($ofc as $off){ ?>
                        <option value="<?php echo $off->officecd; ?>"><?php echo $off->officecd.'-'.$off->office; ?></option>
                        <?php } ?>
                      </select>
    
                  </div>
              </div>
          </div>  
          <div class="col-sm-2">
          <div class="form-group">                            	
            <button class="btn btn-info" type="button" id="submit" onclick="getpersonnel()">Submit</button>
            </div>
          </div>     
         </div>
          <div class="row" > 
          <div class='col-sm-10'>
               {!! Form::label('Serach Personnel', '(OR) Serach Personnel:', ['class'=>'highlight required']) !!}
               <span style="color:red;font-size: 11px;">[*]</span>
              <div class="form-group">
                  <div class=''>
                      <input type="text" class="form-control" name="personnel_id" id="personnel_id" vallue="" placeholder="Serach Personnel by ID">
    
                  </div>
              </div>
          </div>  
          <div class="col-sm-2">
          <div class="form-group">                            	
            <button class="btn btn-info" type="button" id="submit" onclick="getpersonnelbyid()">Search</button>
            </div>
          </div>   
          </div>
          
         </div>
          
         <div class="row"> 
            <div class='col-sm-12'>
                <div >
                <table class="table table-striped table-bordered table-hover notice-types-table" id="datatable-table">
                <thead>
                <tr>
                <td ><b>ID</b></td>
                <td><b>Name</b></td>
                <td><b>Designation</b></td>
                <td><b>Mobile</b></td>
                <td><b>Email</b></td>
                <td><b>Bank Ac No</b></td>
                <td><b>Gender</b></td>
                <td ><b>Action</b></td>
                </tr></thead>
                
                <tbody id='memberDetails'></tbody>
                </table>
                </div>
            </div>
        </div>  
        
         
      </div>
    </div>                              
  </div> 
 
  
  </div>

 
 {!! Form::close() !!}             
</div>
<!--// form -->

</section>
</div>
</div>
<!-- Copyright -->
<script type="text/javascript">
$(document).ready(function () {
 
$("#reset").click(function () {
  location.reload(true);  
});
 


  $('#su_zone_entry')
            .bootstrapValidator({
            message: 'This value is not valid',
            feedbackIcons: {
            valid: 'fa fa-check',
                    invalid: 'fa fa-times',
                    validating: 'fa fa-refresh'
            },
            fields: {
                zone: {
                    validators: {
                        notEmpty: {
                            message: 'Zone is required'
                        }
                    }
                }
              }
            }).on('success.form.bv', function (e) {
           // Prevent form submission
            e.preventDefault();
            var action = $(this).attr('action');
            var zone_code = $('#zone_code').val();
            var districtcd = $("#districtcd").val();
            var zone = $("#zone").val();
            var type = $(this).attr('method');
            var token = $("input[name='_token']").val();
            var fd = new FormData();
            fd.append('districtcd', districtcd);
            fd.append('zone', zone);
            fd.append('_token', token);
            if (zone_code != '') {
             action = "update_zone"
             fd.append('edit_zone', zone_code);
            }
          $(".se-pre-con").fadeIn("slow");
            $.ajax({
            type: type,
                    url: action,
                    data: fd,
                    processData: false,
                    contentType: false,
                    dataType: "json",
                    success: function (data) {

                    if(data.status==1)
                    {
                      $(".se-pre-con").fadeOut("slow");            
                      var msg="Record(s) saved successfully";
                       $.confirm({
                            title: 'Success!',
                            type: 'green',
                            icon: 'fa fa-check',
                            content: msg,
                            buttons: {
                                ok: function () {
                                   // location.reload(true);
                                 //  getZoneWiseAssembly();
                                  location.reload(true);

                                }

                            }
                        });
                      }
                      else if (data.status==2){
                         $(".se-pre-con").fadeOut("slow");            
                        var msg="Record(s) updated successfully";
                         $.confirm({
                              title: 'Success!',
                              type: 'green',
                              icon: 'fa fa-check',
                              content: msg,
                              buttons: {
                                  ok: function () {
                                     // location.reload(true);
                                   //  getZoneWiseAssembly();
                                    location.reload(true);

                                  }

                              }
                          });  
                      }

                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        $(".se-pre-con").fadeOut("slow");
                        var msg = "";
                        if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                            msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                        } else {
                            if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                                msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                            } else {
                                msg += "Error(s):<strong><ul>";
                                $.each(jqXHR.responseJSON, function (key, value) {
                                    msg += "<li>" + value + "</li>";
                                });
                                msg += "</ul></strong>";
                            }
                        }
                        $.alert({
                            title: 'Error!!',
                            type: 'red',
                            icon: 'fa fa-warning',
                            content: msg
                        });
                    }
            });
 
     });
 

});
function delete_zone(code) {

//    var reply=confirm('Are you sure to delete this record?');
//                 if(!reply){
//                     return false;
//                 }
    var msg="Are you sure to delete this record?";
    $.confirm({
         title: 'Confirm!',
         type: 'red',
         icon: 'fa fa-exclamation-triangle',
         content: msg,
         buttons: {
             ok: function () {
                 $.ajax({
                  type: 'post',
                  url: 'zone_delete',
                  data: {'zone': code, '_token': $('input[name="_token"]').val()},
                  dataType: 'json',
                  success: function (data) {
                    if(data.status==1)
                    {
                     location.reload(true);
                    }
                    else if (data.status==2){
                       $.alert({
                            title: 'Error!!',
                            type: 'red',
                            icon: 'fa fa-warning',
                            content: data.options
                        }); 
                    }
                  }
                });
              

             },
             cancel:function () {
                 
             }

         }
     }); 
    

}

function edit_zone(code) {
    $("#reset").show();
    $.ajax({
        type: 'post',
        url: 'zone_edit',
        data: {'zone': code, '_token': $('input[name="_token"]').val()},
        dataType: 'json',
        success: function (data) {
            $('#zone').val(data.options[0].zonename);
            $('#zone_code').val(data.options[0].zone);
        }
    });
 }
 

function getpersonnel(){
    $('#personnel_id').val('')
    var token = $('input[name="_token"]').val();
        var districtcd = $("#districtcd").val();   
        var office_id=$('#ofice_list').children("option:selected"). val();
   $(".se-pre-con").fadeIn("slow");
   $.ajax({
       url: "su_personnel_list/"+office_id,
       method: 'GET',
       data: {district_id:districtcd, _token: token},
       success: function (json) {//alert(json);
          $(".se-pre-con").fadeOut("slow");
          $("#memberDetails").html('');
          $('#memberDetails').html(json);
          $('.editpersonnelId').click(function () {
                var data = this.id;
             // alert(data);
                var datas = {'personnel_id': data, '_token': $('input[name="_token"]').val()};
                //alert(data.code);
                redirectPost('{{url("/edit_personnel")}}', datas);

            });
       },
        error: function (jqXHR, textStatus, errorThrown) {
         $(".se-pre-con").fadeOut("slow");
           var msg = "";
           if (jqXHR.status !== 422 && jqXHR.status !== 400) {
               msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
           } else {
               if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                   msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
               } else {
                   msg += "Error(s):<strong><ul>";
                   $.each(jqXHR.responseJSON, function (key, value) {
                       msg += "<li>" + value + "</li>";
                   });
                   msg += "</ul></strong>";
               }
           }
           $.alert({
               title: 'Error!!',
               type: 'red',
               icon: 'fa fa-warning',
               content: msg
           });
        }
   });
    
 }
 function getpersonnelbyid(){
    var token = $('input[name="_token"]').val();
        var districtcd = $("#districtcd").val();   
        var personnel_id=$('#personnel_id').val();
   
   $(".se-pre-con").fadeIn("slow");
   $.ajax({
       url: "searchpersonnel/"+personnel_id,
       method: 'GET',
       data: {district_id:districtcd, _token: token},
       success: function (json) { 
        $(".se-pre-con").fadeOut("slow");
          $("#memberDetails").html('');
           if(!json || json==''){
            $('#memberDetails').html('<tr><td colspan="8">Personnel not found by this ID</td></tr>');

           }else{
            $('#memberDetails').html(json);

           }
           $('.editpersonnelId').click(function () {
                var data = this.id;
             // alert(data);
                var datas = {'personnel_id': data, '_token': $('input[name="_token"]').val()};
                //alert(data.code);
                redirectPost('{{url("/edit_personnel")}}', datas);

            });
          
       },
        error: function (jqXHR, textStatus, errorThrown) {
         $(".se-pre-con").fadeOut("slow");
           var msg = "";
           if (jqXHR.status !== 422 && jqXHR.status !== 400) {
               msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
           } else {
               if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                   msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
               } else {
                   msg += "Error(s):<strong><ul>";
                   $.each(jqXHR.responseJSON, function (key, value) {
                       msg += "<li>" + value + "</li>";
                   });
                   msg += "</ul></strong>";
               }
           }
           $.alert({
               title: 'Error!!',
               type: 'red',
               icon: 'fa fa-warning',
               content: msg
           });
        }
   });
    
 }
        

        function create_table() {
        var table = "";
        table = $('#datatable-table').DataTable();
        var token = $('input[name="_token"]').val();
        var districtcd = $("#districtcd").val();   
        var office_id=$('#ofice_list').children("option:selected"). val();

        $("#datatable-table").dataTable().fnDestroy();
        table = $('#datatable-table').DataTable({
            "processing": true,
            "serverSide": true,
            "dom": 'lBfrtip',
            "buttons": [
                //'copy', 'csv', 'excel', 'pdf'
                {
                    extend: 'collection',
                    text: 'Export',
                    buttons: [
                        {
                            extend: 'copyHtml5',
                            exportOptions: {
                                columns: ':visible'
                            }
                        },
                        {
                            extend: 'excelHtml5',
                            exportOptions: {
                                columns: ':visible'
                            }
                        },
                        {
                            extend: 'pdfHtml5',
                            exportOptions: {
                                columns: ':visible'
                            }
                        },
                        {
                            extend: 'print',
                            exportOptions: {
                                columns: ':visible'
                            }
                        },
                        {
                            extend: 'csvHtml5',
                            exportOptions: {
                                columns: ':visible'
                            }
                        }

                    ]

                },
                'colvis'
            ],
            "ajax": {
                url: "su_personnel_list",
                type: "post",
                data: {district_id:districtcd, _token: token,office_id:office_id},
                dataSrc: "personnel_list",
                error: function (jqXHR, textStatus, errorThrown) {
                    var msg = "";
                    if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                        msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                    } else {
                        if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                            if (jqXHR.responseJSON.exception_code == 23000) {
                                msg += "Some Sql Exception Occured";
                            } else {
                                msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                            }
                        } else {
                            msg += "Error(s):<strong><ul>";
                            $.each(jqXHR.responseJSON, function (key, value) {
                                msg += "<li>" + value + "</li>";
                            });
                            msg += "</ul></strong>";
                        }
                    }
                    $.alert({
                        title: 'Error!!',
                        type: 'red',
                        icon: 'fa fa-warning',
                        content: msg,
                    });
                }
            },
            "dataType": 'json',
            "columnDefs":
                    [
                        {className: "table-text", "targets": "_all"},
                        {
                            "targets": 0,
                            "data": "personcd",
                        },
                        {
                            "targets": 1,
                            "data": "officer_name",
                        },
                        {
                            "targets": 2,
                            "data": "off_desg",
                        },
                        {
                            "targets": 3,
                            "data": "mob_no",
                        },
                        {
                            "targets": 4,
                            "data": "email",
                        },
                        {
                            "targets": 5,
                            "data": "bank_acc_no",
                        },
                        {
                            "targets": 6,
                            "data": "gender",
                        },

                        {
                            "targets": -1,
                            "data": 'action',
                            "searchable": false,
                            "sortable": false,
                            "render": function (data, type, full, meta) {
                                var str_btns = "";

                                str_btns += '<button type="button"  class="btn btn-success  edit-button btn_new1" id="' + data.personcd + '" title="Edit"><i class="fa fa-edit"></i></button>&nbsp&nbsp';



                               // str_btns += '<button type="button"  class="btn btn-danger  delete-button btn_new1" id="' + data.d + '" title="Delete"><i class="fa fa-trash"></i></button>';

                                return str_btns;
                            }
                        }
                    ],

            "order": [[1, 'asc']]
        });
        table.on('order.dt search.dt draw.dt', function () {
            $('[data-toggle="tooltip"]').tooltip();
            table.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
                cell.innerHTML = table.page() * table.page.len() + (i + 1);
            });
        });
        // table.columns( [-4,-3,-2,-1] ).visible( false );
        }
</script>
@stop

