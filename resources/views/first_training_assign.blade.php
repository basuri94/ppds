@extends('layouts.master')
@section('content')
<!--// top-bar -->
 <div class="wrapper">
<!-- Sidebar Holder -->
@include('layouts.sidebar')

<!-- Page Content Holder -->
<div id="content">           
<section class="tables-section">
    <nav aria-label="breadcrumb" style="width:100%;">
        <div class="breadcrumb pagehead1">
           
                {!! Form::button('<i class="fa fa-desktop"></i> First Training Assign',['class' => 'btn btn-primary-header add-new-button']) !!}
            
            <div class="col-md-offset-8 pull-right">
<!--                {!! Form::button('Academic Year:&nbsp;',['id'=>'show_academic','class' => 'btn btn-primary-year']) !!}-->
                
            </div>
                <div class="clearfix"></div>
        </div>
    </nav>

    <!-- form -->
<div class="outer-w3-agile">
  {!! Form::open(['url' => 'firstTrAssign', 'name' => 'firstTrAssign', 'class' =>'request-info clearfix form-horizontal', 'id' => 'firstTrAssign', 'method' => 'post','role'=>'','files' => true]) !!}
  {!! Form::hidden('districtcd', session()->get('districtcd_ppds'),['id'=>'districtcd']) !!}
  
  <div class="panel-group" id="accordion5">                                                              
   <div class="panel panel-default">
    <div class="panel-heading1">
     <h6 class="panel-title">
         <a data-toggle="#" data-parent="#accordion5" href="#collapsePV"> <span class="fa fa-minus"></span> <span class="highlight">First Training Assign</span></a>
     </h6>
    </div>
    <div id="collapsePV" class="panel-collapse collapse5">
      <div class="panel-body">                                   
       <div class="row"> 
         <div class='col-sm-4'>
               {!! Form::label('zone', 'Zone:', ['class'=>'highlight required']) !!}
              <div class="form-group">
                  <div class=''>
                      {!! Form::select('zone',[''=>'[Select]'],Null,['id'=>'zone','class'=>'form-control','autocomplete'=>'off']) !!}
                  </div>
              </div>
          </div>  
         <div class='col-sm-4'>
            {!! Form::label('subdivision', 'Sub Division:', ['class'=>'highlight required']) !!}
              <!--<span class="highlight">Sub Division</span> -->
              <div class="form-group">
                  <div class=''>
                      {!! Form::select('subdivision',[''=>'[Select]'],null,['id'=>'subdivision','class'=>'form-control','autocomplete'=>'off']) !!}
                  </div>
              </div>
        </div> 
        <div class='col-sm-4'>
            {!! Form::label('phase', 'Phase:', ['class'=>'highlight required']) !!}
           <div class="form-group">
               <div class=''>
                
    <select id="phase" class="form-control" name="phase">
                  
                   <option value="">[Select]</option>
                  

    </select>
                </div>
           </div>
       </div>

           <div class='col-sm-4'>
              {!! Form::label('trainingtype', 'Training Head:', ['class'=>'highlight required']) !!}
              <div class="form-group">
                  <div class=''>
                       {!! Form::select('trainingtype',$training_type['desc'],null,['id'=>'trainingtype','class'=>'form-control','placeholder'=>'[Select]','autocomplete'=>'off']) !!}
                  </div>
              </div>
          </div>
      </div>
          
 
          
<!--      <div class="row"> 
            <div class='col-sm-12'>
                <div class="form-group">
                     <div id='subVenueDetails'></div>
                  </div>
            </div>
         
      </div> -->
      <div class="row">                  
         <div class='col-sm-12'>
            <div class="form-group text-right permit">                            	
               {{ Form::button('Submit', ['class' => 'btn btn-info', 'type' => 'submit','id'=>'Submit']) }}
               {{ Form::button('Reset', ['class' => 'btn btn-success' ,'type' => 'reset','id'=>'reset']) }}
            </div>
          </div>
      </div>     
       
    </div>                              
   </div>
  </div>  
  </div>
 {!! Form::close() !!}             
</div>
<!--// form -->

</section>

</div>
</div>
<!-- Copyright -->
<script type="text/javascript">
$(document).ready(function () {
  getZoneDetails();

$('select[name="zone"]').on('change', function () {
    change_subdivision(); 
    getZonePhasedata(); 
 });

$("#reset").click(function () {
  location.reload(true);  
});

$('#firstTrAssign').bootstrapValidator({

        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            zone: {
                validators: {
                    notEmpty: {
                        message: 'Zone is required'
                    }
                }
            },
            subdivision: {
                validators: {
                    notEmpty: {
                        message: 'Subdivision is required'
                    }
                }
            },
            
            trainingtype: {
                validators: {
                    notEmpty: {
                        message: 'Training Head is required'
                    }
                }
            },
            
                    phase: {
                        validators: {
                            notEmpty: {
                                message: 'Phase is required'
                            }
                        }
                    }         
        }

    }).on('success.form.bv', function (e) {
        e.preventDefault();
        var action = $(this).attr('action');
        var type = $(this).attr('method');
        var formData = new FormData($(this)[0]);
       $(".se-pre-con1").fadeIn("slow");
        $.ajax({
            type: type,
            url: action,
            data: formData,
            processData: false,
            contentType: false,
            dataType: "json",
            success: function (data) {
                $(".se-pre-con1").fadeOut("slow");

                if (data.status == 1) {        
                     var msg="<span style='font-size:13.5px;'>Record(s) assigned successfully</br>";
                     msg+="Total Number of PP => "+data.totalPP;
                     msg+="</br>Total No of (1st)Training assigned => "+data.totalPostAsPP;
                     msg+="</br>Total No of (2nd)Training assigned => "+data.totalPostAs2PP;
                     msg+="</br>No of (1st)Training assigned in ["+data.subd+"] => "+data.totalAsPP;
                     msg+="</br>No of (2nd)Training assigned in ["+data.subd+"] => "+data.totalAs2PP;
                     msg+="</span>";
                     $.confirm({
                        title: 'Success!',
                        type: 'green',
                        icon: 'fa fa-check',
                        content: msg,
                        buttons: {
                            ok: function () {     
                              location.reload(true);
                            }

                        }
                    });

                } else if (data.status == 0){
                   $.alert({
                        title: 'Error!!',
                        type: 'red',
                        icon: 'fa fa-exclamation-triangle',
                        content: 'First Training allocation is empty'
                    });
                    $("#Submit").attr('disabled',false);  
                }
//                }else{
//                    //var msg="Record(s) updated successfully";
//                    var msg="Venue can not be same in same Training Date & Time";
//                     $.confirm({
//                        title: 'Error!!',
//                        type: 'red',
//                        icon: 'fa fa-exclamation-triangle',
//                        content: msg,
//                        buttons: {
//                            ok: function () {     
//                                location.reload(true);
//                            }
//
//                        }
//                    });
//
//                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
             $(".se-pre-con1").fadeOut("slow");
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-exclamation-triangle',
                  content: msg
              });
              $("#Submit").attr('disabled',false);
           }
        });
    });
});
function getZoneDetails(){
    var forDist = $("#districtcd").val();
    var token = $("input[name='_token']").val();
    $(".se-pre-con").fadeIn("slow");
    $.ajax({
       url: "getZoneDetails",
       method: 'POST',
       data: {forDist: forDist, _token: token},
       success: function (data) {//alert(data.options);
         $(".se-pre-con").fadeOut("slow");
          $("select[name='zone'").html('');
          if(data.status==1)
          {
             $("select[name='zone'").append('<option value>[Select]</option>');
            $.each(data.options,function (k, v)
            {
                $("select[name='zone'").append('<option value='+k+'>'+v+'</option>');
            });
           // getMemberDetails();
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
            $(".se-pre-con").fadeOut("slow");
              var msg = "";
              if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                  msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
              } else {
                  if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                      msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                  } else {
                      msg += "Error(s):<strong><ul>";
                      $.each(jqXHR.responseJSON, function (key, value) {
                          msg += "<li>" + value + "</li>";
                      });
                      msg += "</ul></strong>";
                  }
              }
              $.alert({
                  title: 'Error!!',
                  type: 'red',
                  icon: 'fa fa-warning',
                  content: msg
              });
         }
   });
 }
 function change_subdivision(){
  var zone = $("#zone").val();
  var forDist = $("#districtcd").val();
   var token = $("input[name='_token']").val();
  // $(".se-pre-con").fadeIn("slow");
   $.ajax({
       url: "getZonewiseSubdivisionall_Data",
       method: 'POST',
       data: {zone: zone,forDist:forDist, _token: token},
       success: function (data) {//alert(data.options);
       //   $(".se-pre-con").fadeOut("slow");
          $("select[name='subdivision'").html('');
          
          if(data.status==1)
          {
            $("select[name='subdivision'").append('<option value>[Select]</option>');
            $.each(data.options,function (k, v)
            {
                $("select[name='subdivision'").append('<option value='+k+'>'+v+'</option>');
            });
            //$("select[name='subdivision'").html(data.options);
             // change_officeName();
            // getMemberDetails();
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
         $(".se-pre-con").fadeOut("slow");
           var msg = "";
           if (jqXHR.status !== 422 && jqXHR.status !== 400) {
               msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
           } else {
               if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                   msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
               } else {
                   msg += "Error(s):<strong><ul>";
                   $.each(jqXHR.responseJSON, function (key, value) {
                       msg += "<li>" + value + "</li>";
                   });
                   msg += "</ul></strong>";
               }
           }
           $.alert({
               title: 'Error!!',
               type: 'red',
               icon: 'fa fa-exclamation-triangle',
               content: msg
           });
         }
   });
}
function getZonePhasedata(){
  var zone = $("#zone").val();

   var token = $("input[name='_token']").val();
  // $(".se-pre-con").fadeIn("slow");
   $.ajax({
       url: "getZonePhasedata",
       method: 'POST',
       data: {zone: zone, _token: token},
       success: function (data) {//alert(data.options);
       //   $(".se-pre-con").fadeOut("slow");
          $("select[name='phase'").html('');
          
          if(data.status==1)
          {
            $("select[name='phase'").append('<option value>[Select]</option>');
            $.each(data.options,function (k, v)
            {
                $("select[name='phase'").append('<option value='+k+'>'+v+'</option>');
            });
            //$("select[name='subdivision'").html(data.options);
             // change_officeName();
             
          }
       },
        error: function (jqXHR, textStatus, errorThrown) {
       //  $(".se-pre-con").fadeOut("slow");
           var msg = "";
           if (jqXHR.status !== 422 && jqXHR.status !== 400) {
               msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
           } else {
               if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                   msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
               } else {
                   msg += "Error(s):<strong><ul>";
                   $.each(jqXHR.responseJSON, function (key, value) {
                       msg += "<li>" + value + "</li>";
                   });
                   msg += "</ul></strong>";
               }
           }
           $.alert({
               title: 'Error!!',
               type: 'red',
               icon: 'fa fa-warning',
               content: msg
           });
         }
   });
}
</script>
@stop