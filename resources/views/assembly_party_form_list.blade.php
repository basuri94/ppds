@extends('layouts.master')
@section('content')
<style>
    .jconfirm-box{
        width: 650px;
        margin-left: -200px !important; 
    }
</style>
<!--// top-bar -->
<div class="wrapper">
    <!-- Sidebar Holder -->
    @include('layouts.sidebar')
    <!-- Page Content Holder -->
    <div id="content">           
        <section class="tables-section">

            <div class="breadcrumb pagehead1">
                <span class="headbrand"><i class="fa fa-desktop"></i> List of Assembly Party
                    <span class="scoop-mcaret1"></span>
                </span>&nbsp;&nbsp;
                &nbsp;{!! Form::button(' Add Assembly Party&nbsp;<i class="fa fa-plus-circle"></i>',['id'=>'add_asm_party','class' => 'btn btn-primary-year add-new-button']) !!}  

                <div class="clearfix"></div>
            </div>
            </nav>
            <div class="outer-w3-agile">
                <div class="panel-group" id="accordion5">                                                              
                    <div class="panel panel-default">
                        <div class="panel-heading1">
                            <h6 class="panel-title">
                                <a data-toggle="#" data-parent="#accordion5" href="#collapsePV"> <span class="fa fa-minus"></span> <span class="highlight">Zonewise Assembly Party List</span></a>
                            </h6>
                        </div>
                        <div id="collapsePV" class="panel-collapse collapse5">
                            <div class="panel-body">                                   
                                <div class="row">

                                    <div class='col-sm-4'>
                                        {!! Form::label('forZone', 'For Zone:', ['class'=>'highlight ']) !!}
                                        <div class="form-group">
                                            <div class=''>
                                                {!! Form::select('forZone',[''=>'[ALL]'],Null,['id'=>'forZone','class'=>'form-control','autocomplete'=>'off']) !!}
                                            </div>
                                        </div>
                                    </div> 
                                    <div class='col-sm-4'>
                                        {!! Form::label('assembly', 'Assembly:', ['class'=>'highlight ']) !!}
                                        <div class="form-group">
                                            <div class=''>
                                                {!! Form::select('assembly',[''=>'[ALL]'],Null,['id'=>'assembly','class'=>'form-control','autocomplete'=>'off']) !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="form-horizontal">
                            <div class="form-group row">
                                {{csrf_field()}}
                                <div class="datatbl table-responsive" style="width: 96%;margin-left: 20px;">
                                    {!! Form::hidden('districtcd', session()->get('districtcd_ppds'),['id'=>'districtcd']) !!}
                                    <table class="table table-striped table-bordered table-hover notice-types-table" id="datatable-table" style="">
                                        <thead>
                                            <tr>
                                                <th>SL#</th>
                                                <th>Zone</th>
                                                <th>Assembly Code</th>
                                                <th>Assembly Name</th>
                                                <th>No of Member</th>
                                                <th>Gender</th>
                                                <th>No of Party</th>
                                                <th>Action</th>
                                            </tr>

                                        </thead>
                                        <tbody></tbody>
                                        <!-- Table Footer -->

                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </section>
    </div>
</div>
<script>
    $(document).ready(function () {
        $("#add_asm_party").click(function () {
            window.location.href = "assemblyparty_formation";
        });
        getZoneDetails();
        $('select[name="forZone"]').on('change', function () {
            var zone = $("#forZone").val();
            if (zone == "") {
                create_table();
            } else {
                getAssemblyDetails();
            }
        });
        $('select[name="assembly"]').on('change', function () {

            create_table();
        });
        

        var table = $('#datatable-table').DataTable();
        table.on('draw.dt', function () {
//table.fnSetColumnVis( 1, false, false ); 
            $('.edit-button').click(function () {
                var value_all = this.id;
                var split_val = value_all.split("/");
                 var value_all1 = split_val[0]+'/'+split_val[1]+'/'+split_val[2]+'/'+split_val[3];
                $.ajax({
                    type: 'post',
                    url: 'check_for_edit_asmparty',
                    data: {'data': value_all1, '_token': $('input[name="_token"]').val()},
                    dataType: 'json',
                    success: function (datam) {
                        $(".se-pre-con").fadeOut("slow");
                        //alert(datam.count);
                        if (datam.count > 0) {
                            $.alert({
                                title: 'Error!!',
                                type: 'red',
                                icon: 'fa fa-exclamation-triangle',
                                content: "Record exist in personnela table.You can't change this record."
                            });
                            return false;
                        }
                        
                        var table_edit = '<table class="table table-bordered table-striped" style="border-top: 2px solid #4cae4c;" id="assembly_party">';
                        table_edit += '<thead>';
                        table_edit += '<tr style="background-color: #f5f8fa" >';
                        table_edit += '<th width="40%" class="highlight">Assembly Name <span style="color: red;">*</span></th>';
                        table_edit += '<th width="20%" class="highlight">No of Member <span style="color: red;">*</span></th>';
                        table_edit += '<th width="20%" class="highlight">Gender <span style="color: red;">*</span></th>';
                        table_edit += '<th width="20%" class="highlight">No of Party <span style="color: red;">*</span></th>';

                        table_edit += '</tr>';
                        table_edit += '</thead>';
                        table_edit += '<tbody>';
                        table_edit += '<tr style="background-color: #FDF5E6">';
                        table_edit += '<td>' + split_val[4] + '-' + split_val[0] + '</td>';
                        table_edit += '<td>' + split_val[1] + '</td>';
                        table_edit += '<td>' + split_val[2] + '</td>';
                        table_edit += '<td><input class="form-control" type="text" id="edit_party" maxlength="4" name="edit_party" value="' + split_val[3] + '"></td>';

                        table_edit += '</tr>';
                        table_edit += '</tbody>';
                        table_edit += '</table>';
                       
                        var jc = $.confirm({
                            title: '<span style="font-size: 17px;">Zonewise Assembly Party Formation</span>',
                            content: table_edit,
                            type: 'green',
                            icon: 'fa fa-edit',
                            typeAnimated: true,
                            buttons: {
                                 
                                edit: {
                                    btnClass: 'btn-primary',
                                    action: function () {
                                        jc.showLoading(true);
                                        var edit_party = $("#edit_party").val();
                                        return $.ajax({
                                            url: 'edit_assembly_party_form',
                                            dataType: 'json',
                                            data: {'data': value_all1, 'edit_party': edit_party, '_token': $("input[name='_token']").val()},
                                            method: 'post'
                                        }).done(function (response) {
                                            //alert('hi');
                                            jc.hideLoading(true);
                                            if (response.status == 1) {
                                                 var msg_suc = "<strong>SUCCESS: Record edited successfuly</strong>";
                                                $.confirm({
                                                    title: 'Success!',
                                                    type: 'green',
                                                    icon: 'fa fa-check',
                                                    content: msg_suc,
                                                    buttons: {
                                                        ok: function () {  
                                                            //table.ajax.reload();
                                                            create_table();
                                                        }

                                                    }
                                                });
                                            }
                                        }).fail(function (jqXHR, textStatus, errorThrown) {
                                            var msg = "";
                                            if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                                                msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                                            } else {
                                                if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                                                    if (jqXHR.responseJSON.exception_code == 23000) {
                                                        msg += "Record exist in another table.You can't change this record.";
                                                    } else {
                                                        msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                                                    }
                                                } else {
                                                    msg += "Error(s):<strong><ul>";
                                                    $.each(jqXHR.responseJSON, function (key, value) {
                                                        msg += "<li>" + value + "</li>";
                                                    });
                                                    msg += "</ul></strong>";
                                                }
                                            }
                                            $.alert({
                                                title: 'Error!!',
                                                type: 'red',
                                                icon: 'fa fa-warning',
                                                content: msg,
                                            });
                                        });
                                    }
                                },
                                close: function () {
                                }
                            },
                            onOpen: function () {
                                // after the modal is displayed.
                                //startTimer();
                            }
                        });
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        $(".se-pre-con").fadeOut("slow");
                        var msg = "";
                        if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                            msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                        } else {
                            if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                                if (jqXHR.responseJSON.exception_code == 23000) {
                                    msg += "Record exist in another table.You can't change this record.";
                                } else {
                                    msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                                }
                            } else {
                                msg += "Error(s):<strong><ul>";
                                $.each(jqXHR.responseJSON, function (key, value) {
                                    msg += "<li>" + value + "</li>";
                                });
                                msg += "</ul></strong>";
                            }
                        }
                        $.alert({
                            title: 'Error!!',
                            type: 'red',
                            icon: 'fa fa-warning',
                            content: msg,
                        });
                    }
                });

            });
            $('.delete-button').click(function () {
//                var reply = $.confirm('Are you sure to delete the record?');
//                if (!reply) {
//                    return false;
//   
//                             }
                var value_all = this.id;
                var split_val = value_all.split("/");
                var data_new = split_val[0]+'/'+split_val[1]+'/'+split_val[2]+'/'+split_val[3];
                $.ajax({
                    type: 'post',
                    url: 'check_for_delete_asmparty',
                    data: {'data': data_new, '_token': $('input[name="_token"]').val()},
                    dataType: 'json',
                    success: function (datam) {
                        $(".se-pre-con").fadeOut("slow");
                        //alert(datam.count);
                        if (datam.count > 0) {
                            $.alert({
                                title: 'Error!!',
                                type: 'red',
                                icon: 'fa fa-exclamation-triangle',
                                content: "Record exist in personnela table.You can't delete this record."
                            });
                            return false;
                        }
                        $.confirm({
                            title: 'Attention!',
                            content: 'Do you want to delete this record?',
                            type: 'orange',
                            icon: 'fa fa-exclamation-triangle',
                            buttons: {
                                confirm: function () {
                                    
                                    //alert(data_new);
                                    $.ajax({
                                        type: 'post',
                                        url: 'delete_assembly_party_form',
                                        data: {'data': data_new, '_token': $('input[name="_token"]').val()},
                                        dataType: 'json',
                                        success: function (datam) {
                                            
                                             var msg = "<strong>SUCCESS: Record deleted successfully</strong>";
                                            $.confirm({
                                                title: 'Success!',
                                                type: 'green',
                                                icon: 'fa fa-check',
                                                content: msg,
                                                buttons: {
                                                    ok: function () {  
                                                        //table.ajax.reload();
                                                        create_table();
                                                    }
                                                }
                                            });
                                        },
                                        error: function (jqXHR, textStatus, errorThrown) {
                                            $(".se-pre-con").fadeOut("slow");
                                            var msg = "";
                                            if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                                                msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                                            } else {
                                                if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                                                    if (jqXHR.responseJSON.exception_code == 23000) {
                                                        msg += "Record exist in another table.You can't delete this record.";
                                                    } else {
                                                        msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                                                    }
                                                } else {
                                                    msg += "Error(s):<strong><ul>";
                                                    $.each(jqXHR.responseJSON, function (key, value) {
                                                        msg += "<li>" + value + "</li>";
                                                    });
                                                    msg += "</ul></strong>";
                                                }
                                            }
                                            $.alert({
                                                title: 'Error!!',
                                                type: 'red',
                                                icon: 'fa fa-warning',
                                                content: msg,
                                            });
                                        }
                                    });
                                },
                                cancel: function () {

                                }
                            }
                        });
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        $(".se-pre-con").fadeOut("slow");
                        var msg = "";
                        if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                            msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                        } else {
                            if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                                if (jqXHR.responseJSON.exception_code == 23000) {
                                    msg += "Record exist in another table.You can't delete this record.";
                                } else {
                                    msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                                }
                            } else {
                                msg += "Error(s):<strong><ul>";
                                $.each(jqXHR.responseJSON, function (key, value) {
                                    msg += "<li>" + value + "</li>";
                                });
                                msg += "</ul></strong>";
                            }
                        }
                        $.alert({
                            title: 'Error!!',
                            type: 'red',
                            icon: 'fa fa-warning',
                            content: msg,
                        });
                    }
                });


            });

        });
    });
    function getZoneDetails() {
        var forDist = $("#districtcd").val();
        var token = $("input[name='_token']").val();
        $(".se-pre-con").fadeIn("slow");
        $.ajax({
            url: "getZoneDetails",
            method: 'POST',
            data: {forDist: forDist, _token: token},
            success: function (data) {//alert(data.options);
                $(".se-pre-con").fadeOut("slow");
                $("select[name='forZone'").html('');
                if (data.status == 1)
                {
                    $("select[name='forZone'").append('<option value="">[ALL]</option>');
                    $.each(data.options, function (k, v)
                    {
                        $("select[name='forZone'").append('<option value=' + k + '>' + v + '</option>');
                    });
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $(".se-pre-con").fadeOut("slow");
                var msg = "";
                if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                    msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                } else {
                    if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                        msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                    } else {
                        msg += "Error(s):<strong><ul>";
                        $.each(jqXHR.responseJSON, function (key, value) {
                            msg += "<li>" + value + "</li>";
                        });
                        msg += "</ul></strong>";
                    }
                }
                $.alert({
                    title: 'Error!!',
                    type: 'red',
                    icon: 'fa fa-exclamation-triangle',
                    content: msg
                });
            },
            complete: function () {
                create_table();
            }
        });
    }
    function getAssemblyDetails() {
        var forZone = $("#forZone").val();
        var token = $("input[name='_token']").val();
        $(".se-pre-con").fadeIn("slow");
        $.ajax({
            url: "getAssemblyDetails",
            method: 'POST',
            data: {forZone: forZone, _token: token},
            success: function (data) {//alert(data.options);
                $(".se-pre-con").fadeOut("slow");
                $("select[name='assembly'").html('');
                if (data.status == 1)
                {
                    $("select[name='assembly'").html(data.options);
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                $(".se-pre-con").fadeOut("slow");
                var msg = "";
                if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                    msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                } else {
                    if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                        msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                    } else {
                        msg += "Error(s):<strong><ul>";
                        $.each(jqXHR.responseJSON, function (key, value) {
                            msg += "<li>" + value + "</li>";
                        });
                        msg += "</ul></strong>";
                    }
                }
                $.alert({
                    title: 'Error!!',
                    type: 'red',
                    icon: 'fa fa-exclamation-triangle',
                    content: msg
                });
            },
            complete: function () {
                create_table();
            }
        });
    }
    function create_table() {
        var table = "";
        var token = $('input[name="_token"]').val();


        $("#datatable-table").dataTable().fnDestroy();
        table = $('#datatable-table').DataTable({
            "processing": true,
            "serverSide": true,
            "dom": 'lBfrtip',
            "buttons": [
                //'copy', 'csv', 'excel', 'pdf'
                {
                    extend: 'collection',
                    text: 'Export',
                    buttons: [
                        {
                            extend: 'copyHtml5',
                            exportOptions: {
                                columns: ':visible'
                            }
                        },
                        {
                            extend: 'excelHtml5',
                            exportOptions: {
                                columns: ':visible'
                            }
                        },
                        {
                            extend: 'pdfHtml5',
                            exportOptions: {
                                columns: ':visible'
                            }
                        },
                        {
                            extend: 'print',
                            exportOptions: {
                                columns: ':visible'
                            }
                        },
                        {
                            extend: 'csvHtml5',
                            exportOptions: {
                                columns: ':visible'
                            }
                        }

                    ]

                },
                'colvis'
            ],
            "ajax": {
                url: "assem_party_formation_list_datatable",
                type: "post",
                data: {'_token': $('input[name="_token"]').val(), 'zone': $("#forZone").val(), 'assembly': $("#assembly").val()},
                dataSrc: "assembly_form",
                error: function (jqXHR, textStatus, errorThrown) {
                    var msg = "";
                    if (jqXHR.status !== 422 && jqXHR.status !== 400) {
                        msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
                    } else {
                        if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                            if (jqXHR.responseJSON.exception_code == 23000) {
                                msg += "Some Sql Exception Occured";
                            } else {
                                msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
                            }
                        } else {
                            msg += "Error(s):<strong><ul>";
                            $.each(jqXHR.responseJSON, function (key, value) {
                                msg += "<li>" + value + "</li>";
                            });
                            msg += "</ul></strong>";
                        }
                    }
                    $.alert({
                        title: 'Error!!',
                        type: 'red',
                        icon: 'fa fa-warning',
                        content: msg,
                    });
                }
            },
            "dataType": 'json',
            "columnDefs":
                    [
                        {className: "table-text", "targets": "_all"},
                        {
                            "targets": 0,
                            "data": "code",
                            "defaultContent": "",
                            "searchable": false,
                            "sortable": false
                        },
                        {
                            "targets": 1,
                            "data": "zonename"
                        },
                        {
                            "targets": 2,
                            "data": "assemblycd"
                        },
                        {
                            "targets": 3,
                            "data": "assemblyname"
                        },
                        {
                            "targets": 4,
                            "data": "no_of_member"
                        },
                        {
                            "targets": 5,
                            "data": "gender"
                        },
                        {
                            "targets": 6,
                            "data": "no_party"
                        },
                        {
                            "targets": -1,
                            "data": 'action',
                            "searchable": false,
                            "sortable": false,
                            "render": function (data, type, full, meta) {
                                var str_btns = "";

                                str_btns += '<button type="button"  class="btn btn-success  edit-button btn_new1" id="' + data.e + '" title="Edit"><i class="fa fa-edit"></i></button>&nbsp&nbsp';



                                str_btns += '<button type="button"  class="btn btn-danger  delete-button btn_new1" id="' + data.d + '" title="Delete"><i class="fa fa-trash"></i></button>';

                                return str_btns;
                            }
                        }
                    ],

            "order": [[1, 'asc']]
        });
        table.on('order.dt search.dt draw.dt', function () {
            $('[data-toggle="tooltip"]').tooltip();
            table.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
                cell.innerHTML = table.page() * table.page.len() + (i + 1);
            });
        });
        // table.columns( [-4,-3,-2,-1] ).visible( false );
    }
</script>
<!-- Copyright -->
@stop