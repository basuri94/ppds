
@extends('layouts.master')
@section('content')

 <!--// top-bar -->
 <div class="wrapper">
<!-- Sidebar Holder -->
@include('layouts.sidebar')

<!-- Page Content Holder -->
<div id="content">           
<section class="tables-section">
    <nav aria-label="breadcrumb" style="width:100%;">
        <div class="breadcrumb pagehead1">
           
                {!! Form::button('<i class="fa fa-desktop"></i> Search Polling Personnel',['class' => 'btn btn-primary-header add-new-button']) !!}
            
            <div class="col-md-offset-8 pull-right">
<!--                {!! Form::button('Academic Year:&nbsp;',['id'=>'show_academic','class' => 'btn btn-primary-year']) !!}-->
                
            </div>
                <div class="clearfix"></div>
        </div>
    </nav>

    <!-- form -->
<div class="outer-w3-agile">
  {!! Form::open(['url' => 'ppSearch', 'name' => 'ppSearch', 'class' =>'request-info clearfix form-horizontal', 'id' => 'ppSearch', 'method' => 'post','role'=>'','files' => true]) !!}
   {!! Form::hidden('districtcd', session()->get('districtcd_ppds'),['id'=>'districtcd']) !!}
  <!-- OLD PWise -->
  <div class="panel-group" id="accordion5" style="">                                                              
    <div class="panel panel-default">
     <div class="panel-heading1">
     <h6 class="panel-title">
         <a data-toggle="#" data-parent="#accordion5" href="#collapsePV"> <span class="fa fa-minus"></span><span class="highlight"> OLD PERSONNEL </span></a>
     </h6>
     </div>
     <div id="collapsePV" class="panel-collapse collapse5">
      <div class="panel-body" style="min-height:75px;">                                   
        <div class="row">      
         <div class='col-sm-4'>
              {!! Form::label('personnel_id', 'Personnel ID:', ['class'=>'highlight']) !!}
              <div class="form-group">
                  <div class=''>
                      {!! Form::text('personnel_id',null,['id'=>'personnel_id','class'=>'form-control','autocomplete'=>'off','maxlength'=>'11','minlength'=>'11']) !!}
                  </div>
              </div>
              <div style="display: none;">
                      {!! Form::text('newpersonnel_id',null,['id'=>'newpersonnel_id','class'=>'form-control','autocomplete'=>'off','maxlength'=>'11','minlength'=>'11','readonly'=>'false']) !!}
                  </div>
          </div>     
        </div>
        <div class="row"> 
            <div class='col-sm-12'> 
               <div id="old_personnel_details" >                            	
                   
               </div>
            </div>
         </div> 
         <div class="row" id="rw" style="display: none;"> 
          <div class='col-sm-12'>                    
                <div class="form-group text-right permit">                            	
          
                   {{ Form::button('Reset', ['class' => 'btn btn-success' ,'type' => 'reset','id'=>'reset']) }}
                </div>
           </div>
         </div> 
       </div>
      </div>
    </div>                              
  </div>                    
 {!! Form::close() !!}             
</div>
<!--// form -->


</section>

</div>
</div>
<!-- Copyright -->
<script type="text/javascript">
$(document).ready(function () {
   
   $("#reset").click(function () {
    location.reload(true);
  });
  char_num("#personnel_id");
  //getZoneDetails(); 
  $('#personnel_id').on('change', function () {
    get_personnel_details();   
  });
 
});
 function get_personnel_details(){
    var personnel_id = $("#personnel_id").val();
    
    var token = $("input[name='_token']").val();
    $(".se-pre-con").fadeIn("slow");
    $.ajax({
       url: "searchPPDetails",
       method: 'POST',
       data: {personnel_id: personnel_id, _token: token},
       success: function (json) {//alert(data.options);
          $(".se-pre-con").fadeOut("slow");
          //$("#memberDetails").html('');
          if(json.status==1)
          {
              $('#old_personnel_details').html(json.options);
               $('#rw').show();
               scrollToElement($('#rw'));
          }
          else if(json.status==0)
          {
              $('#old_personnel_details').html(json.options);
              $('#rw').hide();
          }
          document.getElementById('replace').disabled=true;
          document.getElementById('print').disabled=true;
       },
        error: function (jqXHR, textStatus, errorThrown) {
         $(".se-pre-con").fadeOut("slow");
           var msg = "";
           if (jqXHR.status !== 422 && jqXHR.status !== 400) {
               msg += "<strong>" + jqXHR.status + ": " + errorThrown + "</strong>";
           } else {
               if (jqXHR.responseJSON.hasOwnProperty('exception')) {
                   msg += "Exception: <strong>" + jqXHR.responseJSON.exception_message + "</strong>";
               } else {
                   msg += "Error(s):<strong><ul>";
                   $.each(jqXHR.responseJSON, function (key, value) {
                       msg += "<li>" + value + "</li>";
                   });
                   msg += "</ul></strong>";
               }
           }
           $.alert({
               title: 'Error!!',
               type: 'red',
               icon: 'fa fa-warning',
               content: msg
           });
        }
   });
 }
 function scrollToElement(ele) {
    $('html, body').animate({
        scrollTop: ele.offset().top - 60
    }, 1000);
}
</script>
@stop
